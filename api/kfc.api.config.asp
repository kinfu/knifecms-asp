<%
'**
'系统api配置
'filename: kfc.api.config.asp
'copyright: KFC
'**

Const Api_Enable	= False        '是否打开系统整合（默认闭关: False ,打开：True ）
Const Api_SysKey	= "API_TEST"   '设置系统密钥 (系统整合，必须保证与其它系统设置的密钥一致。)
Const Api_Urls	= ""               'Api_Urls :整合的其它程序的接口文件路径。多个程序接口之间用半角"|"分隔。

%>