/*
* 疑问：为什么不把这些代码直接放去goods.detail.asp页面呢？
* 解答：因为本系统的模板引擎的执行速度和页面代码的多少成反比，页面代码越少页面执行的速度就越快！别小看这点代码，这样已经大约提高了5%的执行速度
*/

/***商品详细页**/
function Goods_Init(){
	ListOverExpand("leftHotGoods");
	KnifeCMS.C.Shop.GoodsID = goodsid;
	KnifeCMS.C.Shop.BuyGoodsInit();
	//搭配销售
	set_adjunctoodslist_width();
	KnifeCMS.C.Shop.AdjunctGoodsSet();
	//判断商品购买数量是否超出库存
	KnifeCMS.C.Cart.GoodsBuyNumCheck("goodsnum","goodsbuynumcheck_cue");
	//商品浏览量+1
	KnifeCMS.C.Shop.AddGoodsHits(goodsid);
	if($.trim($("#relatedgoods_bd").html())==""){$("#relatedgoods_bd").html('<p class="none_related">'+Lang_Js_Shop[16]+'</p>');}
}

//设置商品配件组-商品显示的ul.adjunctgoodslist的宽度
function set_adjunctoodslist_width(){
	if(objExist(KnifeCMS.$id("adjunctarea"))){
		var uls=getElementsByTagNameAndAttribute(KnifeCMS.$id("adjunctarea"),"ul","adjunct","true");
		for(var i=0;i<uls.length;i++){
			var lis=uls[i].getElementsByTagName("li");
			uls[i].style.width=120 * parseInt(lis.length)+"px";
		}
	}
}

//显示商品咨询
function PrintGoodsConsult(PrintArea,GlobalConsultSet,ID,Lang){
	var html = '<div class="heading"><span class="heading_title">'+Lang[0]+'</span></div>'+
	'<div class="consult">'+
	'<div id="goodsconsult_tiearea"></div>'+
	'<div class="send">'+
	'<div class="cue">'+Lang[1]+'</div>'+
	'<table border="0" cellpadding="0" cellspacing="0" width="100%">'+
	'<tr><td class="td01"><div>'+Lang[2]+'</div></td>'+
		'<td class="td02"><div><textarea class="text" name="goodsconsulttextarea" id="goodsconsulttextarea" style="width:485px; height:86px;"></textarea></div></td>'+
	'</tr>'+
	'<tr><td class="td01"><div></div></td><td class="td02"><div id="submitBtn_Consult"><input type="submit" class="btn btn_sendconsult" onclick=KnifeCMS.C.Tie.Post(this,"goodsconsult","'+ID+'"); value="" /><input type="hidden" name="goodsconsultcheck" id="goodsconsultcheck" value="{knifecms:isgoodsconsultcheck}" /></div></td></tr>'+
	'</table>'+
	'</div>'+
	'</div>';
	var Area = KnifeCMS.$id(PrintArea);
	if(objExist(Area)){
		if(parseInt(GlobalConsultSet)!=1){
			Area.innerHTML=html;
			KnifeCMS.C.Tie.Show("goodsconsult",goodsid);
		}
	}
}

//显示商品评论
function PrintGoodsComment(PrintArea,GlobalCommentSet,ID,Lang){
	var html = '<div class="heading"><span class="heading_title">'+Lang[0]+'</span></div>'+
	'<div class="comment">'+
	'<div id="goodscomment_tiearea"></div>'+
	'<div class="send">'+
	'<div class="cue">'+Lang[1]+'</div>'+
	'<table border="0" cellpadding="0" cellspacing="0" width="100%">'+
	'<tr><td class="td01"><div>'+Lang[2]+'</div></td>'+
		'<td class="td02"><div><textarea class="text" name="goodscommenttextarea" id="goodscommenttextarea" style="width:485px; height:86px;"></textarea></div></td></tr>'+
	'<tr><td class="td01"><div></div></td><td class="td02"><div id="submitBtn_Comment"><input type="submit" class="btn btn_sendcomment" onclick=KnifeCMS.C.Tie.Post(this,"goodscomment","'+ID+'") value="" /><input type="hidden" name="goodscommentcheck" id="goodscommentcheck" value="{knifecms:isgoodscommentcheck}" /></div></td></tr>'+
	'</table>'+
	'</div>'+
	'</div>';			  
	var Area = KnifeCMS.$id(PrintArea);
	if(objExist(Area)){
		if(parseInt(GlobalCommentSet)!=1){
			Area.innerHTML=html;
			KnifeCMS.C.Tie.Show("goodscomment",goodsid);
		}
	}
}
