/***购物车**/
function SetOrderAddress(){
	var Check     = true;
	if(!KnifeCMS.CheckSelectArea("ship_area","d_ship_area",Array("Normal",""),Array("d_err",Lang_Js[17]))){Check=false;}
	if(!KnifeCMS.CheckItem("ship_address","d_ship_address",Array("Normal",""),Array("d_err",Lang_Js[18]))){Check=false;}
	if(!KnifeCMS.CheckItem("ship_zip","d_ship_zip",Array("Normal",""),Array("d_err",Lang_Js[19]))){Check=false;}
	if(!KnifeCMS.CheckItem("ship_name","d_ship_name",Array("Normal",""),Array("d_err",Lang_Js[20]))){Check=false;}
	if(!KnifeCMS.CheckItem("ship_mobile|ship_tel","d_ship_tel",Array("Normal",""),Array("d_err",Lang_Js[21]))){Check=false;}
	if(Check){
		//电话号码和手机合法性判断
		var fn_tel    = KnifeCMS.$id("ship_tel").value;
		var fn_mobile = KnifeCMS.$id("ship_mobile").value;
		fn_tel    = fn_tel.replace(/[^0-9]*/g,"");
		fn_mobile = fn_mobile.replace(/[^0-9]*/g,"");
		if(fn_tel.length<6 && fn_mobile.length<6){
			Check=false;
			KnifeCMS.$id("d_ship_tel").className="d_err";
			KnifeCMS.$id("d_ship_tel").innerHTML=Lang_Js[41];
		}
		if(Check){
			$("#btn_setorderaddress").fadeOut();
			$("#order_delivery").fadeIn();
			KnifeCMS.ExpandShrink(KnifeCMS.$id("es_address"),"myaddress","[-"+Lang_Js_DoShrink+"]","[+"+ Lang_Js_DoExpand +"]");
		}
	}
}
function SetOrderDelivery(){
	var Check = false;
	var delivery_radio = document.getElementsByName("delivery[shipping_id]");
	if(delivery_radio.length>=1){
		for(var j=0;j<delivery_radio.length; j++){
			if(delivery_radio[j].checked == true){
				Check = true;
			}
		}
	}else{
		Check = false;
		alert(Lang_Js[43]);
	}
	if(Check){
		KnifeCMS.$id("d_delivery").className="";
		KnifeCMS.$id("d_delivery").innerHTML="";
		$("#btn_setorderdelivery").fadeOut();
		$("#order_goods").fadeIn();
		$("#order_amount").fadeIn();
		$("#order_place").fadeIn();
		KnifeCMS.ExpandShrink(KnifeCMS.$id("es_delivery"),"delivery_and_date","[-"+Lang_Js_DoShrink+"]","[+"+ Lang_Js_DoExpand +"]");
	}else{
		KnifeCMS.$id("d_delivery").className="d_err";
		KnifeCMS.$id("d_delivery").innerHTML=Lang_Js[42];
	}
}
