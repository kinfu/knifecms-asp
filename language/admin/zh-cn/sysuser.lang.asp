<%
'=========================================================
' File     : sysuser.lang.asp
' 功能注释 : 系统后台语言文档(系统用户管理)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
Dim Lang_SysUser_ColLine(10)
    Lang_SysUser_ColLine(0)  = "特别组"
	Lang_SysUser_ColLine(1)  = "系统用户组名"
	Lang_SysUser_ColLine(2)  = "等级 ( 数值越大等级越高 )"
	Lang_SysUser_ColLine(3)  = "用户帐号"
	Lang_SysUser_ColLine(4)  = "真实姓名"
	Lang_SysUser_ColLine(5)  = "电子邮件"
	Lang_SysUser_ColLine(6)  = "状态"
	Lang_SysUser_ColLine(7)  = "登陆次数"
	Lang_SysUser_ColLine(8)  = "上次登陆时间"
	Lang_SysUser_ColLine(9)  = "前台会员"
	Lang_SysUser_ColLine(10) = "IP"

Dim Lang_SysUserGroup(11)
	Lang_SysUserGroup(0) = "成功添加系统用户组."
	Lang_SysUserGroup(1) = "成功编辑系统用户组."
	Lang_SysUserGroup(2) = "系统用户组名不能为空,且等级不能为负数."
	Lang_SysUserGroup(3) = "成功删除系统用户组."
	Lang_SysUserGroup(4) = "不能删除创始人组."
	Lang_SysUserGroup(5) = "系统用户组名"
	Lang_SysUserGroup(6) = "等级"
	Lang_SysUserGroup(7) = "数值越大等级越高"
	Lang_SysUserGroup(8) = "备注"
	Lang_SysUserGroup(9) = "系统权限设置"
	Lang_SysUserGroup(10) = "创始人组不可编辑"
	Lang_SysUserGroup(11) = "不存在系统用户组."
	
Dim Lang_SysUser(22)
    Lang_SysUser(0)  = "用户成功登陆"
	Lang_SysUser(1)  = "成功添加系统用户"
	Lang_SysUser(2)  = "成功编辑系统用户"
	Lang_SysUser(3)  = "成功删除系统用户"
	Lang_SysUser(4)  = "成功彻底删除系统用户"
	Lang_SysUser(5)  = "成功还原系统用户"
	Lang_SysUser(6)  = "不存在该用户;"
	Lang_SysUser(7)  = "密码错误;"
	Lang_SysUser(8)  = "验证码错误;"
	Lang_SysUser(9)  = "该用户已被禁用;"
	Lang_SysUser(10) = "该用户已被删至回收站;"
	Lang_SysUser(11) = "用户名或密码不能为空;"
	Lang_SysUser(12) = "用户安全退出;"
	
	Lang_SysUser(14) = "前台会员ID"
	Lang_SysUser(15) = "用户帐号"
	Lang_SysUser(16) = "真实姓名"
	Lang_SysUser(17) = "Email"
	Lang_SysUser(18) = "设置密码"
	Lang_SysUser(19) = "密码确认"
	Lang_SysUser(20) = "所属用户组"
	Lang_SysUser(21) = "是否禁用"
	Lang_SysUser(22) = "备注"
	
	
Dim Lang_SysUserCue(18)
    Lang_SysUserCue(0)  = "用户名不能为空"
	Lang_SysUserCue(1)  = "真实姓名不能为空."
	Lang_SysUserCue(2)  = "密码长度不能小于6."
	Lang_SysUserCue(3)  = "两次填写的密码不一致."
	Lang_SysUserCue(4)  = "Email格式错误."
	Lang_SysUserCue(5)  = "未选择所属用户组."
	Lang_SysUserCue(6)  = "已经存在该用户帐号"
	Lang_SysUserCue(7)  = "不能添加、编辑、删除比您所在用户组级别更高的用户."
	Lang_SysUserCue(8)  = "不能对自己进行删除、禁用、启用操作"
	Lang_SysUserCue(9)  = "成功还原系统用户."
	Lang_SysUserCue(10) = "成功禁用系统用户."
	Lang_SysUserCue(11) = "成功启用系统用户."
	Lang_SysUserCue(12) = "前台会员不存在."
	Lang_SysUserCue(13) = "密码由""英文数字_-""组成，最小长度:6 最大长度:20."
	Lang_SysUserCue(14) = "必须为没有和其他系统用户绑定的前台会员，注意是填写<a href='user.asp?ctl=user&act=view' target=_blank>会员ID</a>，不是会员用户名！"
	Lang_SysUserCue(15) = "前台会员已经和其他系统用户绑定."
	Lang_SysUserCue(16) = "留空时表示不修改密码."
	Lang_SysUserCue(17) = "您无权操作创始人组成员."
	
%>
