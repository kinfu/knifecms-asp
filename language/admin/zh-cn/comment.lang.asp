<%
'=========================================================
' File     : comment.lang.asp
' 功能注释 : 系统后台语言文档(评论管理系统)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
	
Dim Lang_Comment_ListTableColLine(1)
    Lang_Comment_ListTableColLine(0) = "排序"
	
Dim Lang_Comment(15)
    Lang_Comment(0)   = ""
	Lang_Comment(1)   = "会员ID"
	Lang_Comment(2)   = "会员帐号"
	Lang_Comment(3)   = "会员头像"
	Lang_Comment(4)   = "内容ID"
	Lang_Comment(5)   = "内容标题"
	Lang_Comment(6)   = "咨询/评论内容"
	Lang_Comment(7)   = "发表时间"
	Lang_Comment(8)   = "审核"
	Lang_Comment(9)   = "回复数"
	Lang_Comment(10)  = "IP"
	Lang_Comment(11)  = "上级评论ID"
	Lang_Comment(12)  = "回复"
	
Dim Lang_Comment_Cue(12)
    Lang_Comment_Cue(0)   = ""
	Lang_Comment_Cue(1)   = "成功回复."
	Lang_Comment_Cue(2)   = "成功编辑."
	Lang_Comment_Cue(3)   = "成功删除."
	Lang_Comment_Cue(4)   = "成功彻底删除评论/咨询及其回复."
	Lang_Comment_Cue(5)   = "成功还原."
	Lang_Comment_Cue(6)   = ""
	Lang_Comment_Cue(7)   = "不存在."
	Lang_Comment_Cue(8)   = "名称不能为空."
	Lang_Comment_Cue(9)   = "已经超出回复级数，不能再回复."
	Lang_Comment_Cue(10)  = "通过审核"
	Lang_Comment_Cue(11)  = "未通过审核"

%>
