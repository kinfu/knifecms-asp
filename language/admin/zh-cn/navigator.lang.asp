<%
'=========================================================
' File     : navigator.lang.asp
' 功能注释 : 系统后台语言文档(站点导航管理)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
	
Dim Lang_Navigator_ListTableColLine(10)
    Lang_Navigator_ListTableColLine(0) = ""
	Lang_Navigator_ListTableColLine(1) = "位置"
	Lang_Navigator_ListTableColLine(2) = "排序|站点栏目名称"
	Lang_Navigator_ListTableColLine(3) = "显示"
	Lang_Navigator_ListTableColLine(4) = "添加"
	Lang_Navigator_ListTableColLine(5) = "编辑"
	Lang_Navigator_ListTableColLine(6) = "删除"
	Lang_Navigator_ListTableColLine(7) = "预览"
	Lang_Navigator_ListTableColLine(8) = ""
	
Dim Lang_NavigatorType(5)
    Lang_NavigatorType(1) = "顶部导航"
	Lang_NavigatorType(2) = "网站主导航"
	Lang_NavigatorType(3) = "底部分类导航"
	Lang_NavigatorType(4) = "底部导航"
	
Dim Lang_Navigator(15)
    Lang_Navigator(0)   = "新导航"
	Lang_Navigator(1)   = "子导航"
	Lang_Navigator(2)   = "导航名称"
	Lang_Navigator(3)   = "位置"
	Lang_Navigator(4)   = "上级导航"
	Lang_Navigator(5)   = "导航内容URL"
	Lang_Navigator(6)   = "是否隐藏"
	Lang_Navigator(7)   = "是否新窗口打开"
	Lang_Navigator(8)   = "排序"
	Lang_Navigator(9)   = ""
	Lang_Navigator(10)   = "选择内容可自动生成下面的导航URL."
	Lang_Navigator(11)   = "站外地址必须以 "" http:// 或 https:// "" 开头."
	Lang_Navigator(12)   = "隐藏即不显示该导航."
	Lang_Navigator(13)   = "数字越小越靠前."
	Lang_Navigator(14)   = "新窗口"
	
Dim Lang_Navigator_Cue(12)
    Lang_Navigator_Cue(0)   = "成功添加新站点导航"
	Lang_Navigator_Cue(1)   = "成功编辑站点导航"
	Lang_Navigator_Cue(2)   = "成功批量编辑."
	Lang_Navigator_Cue(3)   = "成功删除站点导航栏目."
	Lang_Navigator_Cue(4)   = ""
	Lang_Navigator_Cue(5)   = "系统默认的导航不能删除."
	Lang_Navigator_Cue(6)   = ""
	Lang_Navigator_Cue(7)   = "支付方式不存在."
	Lang_Navigator_Cue(8)   = "导航名称不能为空."
	Lang_Navigator_Cue(9)   = "导航内容URL不能为空."
	Lang_Navigator_Cue(10)  = "上级导航不存在."
	Lang_Navigator_Cue(11)  = "请选择导航位置."

%>
