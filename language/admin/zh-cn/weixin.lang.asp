<%
'=========================================================
' File     : weixin.lang.asp
' 功能注释 : 系统后台语言文档(微信管理)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

Dim Lang_Weixin_ListTableColLine(10)
    Lang_Weixin_ListTableColLine(0)  = ""
	Lang_Weixin_ListTableColLine(1)  = "会员ID"
	Lang_Weixin_ListTableColLine(2)  = "用户名"
	Lang_Weixin_ListTableColLine(3)  = "会员头像"
	Lang_Weixin_ListTableColLine(4)  = "微博内容"
	Lang_Weixin_ListTableColLine(5)  = "发表时间"
	Lang_Weixin_ListTableColLine(6)  = "回复数"
	Lang_Weixin_ListTableColLine(7)  = "类别"
	Lang_Weixin_ListTableColLine(8)  = ""
	Lang_Weixin_ListTableColLine(9)  = ""
	
Dim Lang_Weixin(5)
    Lang_Weixin(0)   = ""
	Lang_Weixin(1)   = "会员ID"
	Lang_Weixin(2)   = "用户名"
	Lang_Weixin(3)   = "上级内容ID"
	Lang_Weixin(4)   = "IP"
	
	
Dim Lang_Weixin_Cue(5)
    Lang_Weixin_Cue(0)   = ""
	Lang_Weixin_Cue(1)   = "成功发表微博."
	Lang_Weixin_Cue(2)   = ""
	Lang_Weixin_Cue(3)   = ""
	Lang_Weixin_Cue(4)   = "成功彻底删除微博."

%>
