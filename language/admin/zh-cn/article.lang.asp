<%
'=========================================================
' File     : syslog.lang.asp
' 功能注释 : 系统后台语言文档(系统日志)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

'信息列表表格栏
Const Lang_ListTableColLine_LogTime   = "日志时间"
Const Lang_ListTableColLine_Username  = "用户"
Const Lang_ListTableColLine_UserID    = "用户ID"
Const Lang_ListTableColLine_UserIP    = "来源IP"
Const Lang_ListTableColLine_SystemCtl = "功能系统"
Const Lang_ListTableColLine_SystemAction = "功能操作"
Const Lang_ListTableColLine_LogContent = "日志内容"
	
Dim Lang_SysLogCue(12)
	Lang_SysLogCue(0) = "成功还原系统日志."
	Lang_SysLogCue(1) = "成功删除系统日志."
	Lang_SysLogCue(2) = "成功彻底删除系统日志."
%>
