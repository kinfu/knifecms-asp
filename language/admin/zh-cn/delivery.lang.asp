<%
'=========================================================
' File     : delivery.lang.asp
' 功能注释 : 系统后台语言文档(配送方式)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
	
Dim Lang_Delivery_ListTableColLine(5)
    Lang_Delivery_ListTableColLine(0) = "配送方式名称"
	Lang_Delivery_ListTableColLine(1) = "状态"
	Lang_Delivery_ListTableColLine(2) = "物流保价"
	Lang_Delivery_ListTableColLine(3) = "货到付款"
	Lang_Delivery_ListTableColLine(4) = "排序"
	
Dim Lang_Delivery(29)
    Lang_Delivery(0)   = "配送方式"
	Lang_Delivery(1)   = "配送方式名称"
	Lang_Delivery(2)   = "类型"
	Lang_Delivery(3)   = "先收款后发货"
	Lang_Delivery(4)   = "支持货到付款"
	Lang_Delivery(5)   = "支持货到付款后顾客可以选择所有选择支付方式"
	Lang_Delivery(6)   = "重量设置"
	Lang_Delivery(7)   = "首重重量"
	Lang_Delivery(8)   = "续重单位"
	Lang_Delivery(9)   = "保价"
	Lang_Delivery(10)  = "支持物流保价"
	Lang_Delivery(11)  = "费率"
	Lang_Delivery(12)  = "最低保价费"
	Lang_Delivery(13)  = "配送地区费用设置"
	Lang_Delivery(14)  = "统一设置"
	Lang_Delivery(15)  = "指定配送地区和费用"
	Lang_Delivery(16)  = "配送费用"
	Lang_Delivery(17)  = "首重费用"
	Lang_Delivery(18)  = "续重费用"
	Lang_Delivery(19)  = "启用默认费用"
	Lang_Delivery(20)  = "注意：未启用默认费用时，不在指定配送地区的顾客不能使用本配送方式下订单"
	Lang_Delivery(21)  = "指定配送地区"
	Lang_Delivery(22)  = "配送地区"
	Lang_Delivery(23)  = "费用"
	Lang_Delivery(24)  = "为指定的地区设置运费"
	Lang_Delivery(25)  = "排序"
	Lang_Delivery(26)  = "数字越小越靠前"
	Lang_Delivery(27)  = "说明"
	Lang_Delivery(28)  = ""
	
Dim Lang_Delivery_Cue(12)
    Lang_Delivery_Cue(0)   = ""
	Lang_Delivery_Cue(1)   = "成功添加配送方式."
	Lang_Delivery_Cue(2)   = "成功编辑配送方式."
	Lang_Delivery_Cue(3)   = "成功删除配送方式."
	Lang_Delivery_Cue(4)   = "成功彻底删除配送方式."
	Lang_Delivery_Cue(5)   = "成功启动使用配送方式"
	Lang_Delivery_Cue(6)   = "成功停用配送方式"
	Lang_Delivery_Cue(7)   = "配送方式不存在."
	Lang_Delivery_Cue(8)   = "配送名称不能为空."
	Lang_Delivery_Cue(9)   = ""
	Lang_Delivery_Cue(10)  = "."
	Lang_Delivery_Cue(11)  = ""

%>
