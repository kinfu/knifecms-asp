<%
'=========================================================
' File     : cmodule.lang.asp
' 功能注释 : 系统后台语言文档
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

Dim Lang_CModule(18)
	Lang_CModule(0) = "自定义标签"
	Lang_CModule(1) = "标签名称"
	Lang_CModule(2) = "模版标签"
	Lang_CModule(3) = "标签内容"
	Lang_CModule(4) = "备注"
	Lang_CModule(5) = "排序"
	Lang_CModule(6) = "状态"
	Lang_CModule(7) = ""
	Lang_CModule(8) = "标签只能由英文字母、数字和下划线组成，并且仅能字母开头，不以下划线结尾"
	Lang_CModule(9) = "标签名称不能为空"
	Lang_CModule(10)= "模版标签不能为空"
	Lang_CModule(11)= "成功添加自定义标签"
	Lang_CModule(12)= "成功编辑自定义标签"
	Lang_CModule(13)= "自定义标签不存在"
	Lang_CModule(14)= ""
	Lang_CModule(15)= "成功删除自定义标签"
	Lang_CModule(16)= "自定义标签删除失败"
%>
