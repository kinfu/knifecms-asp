<%
'=========================================================
' File     : paymentbill.lang.asp
' 功能注释 : 系统后台语言文档(收款单)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
	
Dim Lang_PaymentBill_ListTableColLine(12)
    Lang_PaymentBill_ListTableColLine(0)  = ""
	Lang_PaymentBill_ListTableColLine(1)  = "支付单号"
	Lang_PaymentBill_ListTableColLine(2)  = "支付金额"
	Lang_PaymentBill_ListTableColLine(3)  = "货币"
	Lang_PaymentBill_ListTableColLine(4)  = "订单号"
	Lang_PaymentBill_ListTableColLine(5)  = "支付方式"
	Lang_PaymentBill_ListTableColLine(6)  = "收款银行"
	Lang_PaymentBill_ListTableColLine(7)  = "收款账号"
	Lang_PaymentBill_ListTableColLine(8)  = "支付完成时间"
	Lang_PaymentBill_ListTableColLine(9)  = "会员用户名"
	Lang_PaymentBill_ListTableColLine(10) = "支付账户"
	Lang_PaymentBill_ListTableColLine(11) = "操作员"
	
	
Dim Lang_PaymentBill(21)
    Lang_PaymentBill(0)   = "收款单"
	Lang_PaymentBill(1)   = "收款单信息"
	Lang_PaymentBill(2)   = "支付单号"
	Lang_PaymentBill(3)   = "订单号"
	Lang_PaymentBill(4)   = "收款银行"
	Lang_PaymentBill(5)   = "收款帐号"
	Lang_PaymentBill(6)   = "付款金额"
	Lang_PaymentBill(7)   = "付款人"
	Lang_PaymentBill(8)   = "收款方式"
	Lang_PaymentBill(9)   = "支付方式"
	Lang_PaymentBill(10)   = "生成时间"
	Lang_PaymentBill(11)   = "付款时间"
	Lang_PaymentBill(12)   = "会员用户名"
	Lang_PaymentBill(13)   = "操作员"
	Lang_PaymentBill(14)   = "备注"
	Lang_PaymentBill(15)   = "货币"
	Lang_PaymentBill(16)   = "货币金额"
	Lang_PaymentBill(17)   = "支付手续费"
	Lang_PaymentBill(18)   = "支付状态"
	Lang_PaymentBill(19)   = "IP"
	Lang_PaymentBill(20)   = "是否有效"
	
Dim Lang_PaymentBill_Cue(10)
    Lang_PaymentBill_Cue(0)   = ""
	Lang_PaymentBill_Cue(1)   = "成功添加新支付单."
	Lang_PaymentBill_Cue(2)   = "成功编辑支付单."
	Lang_PaymentBill_Cue(3)   = "成功删除支付单."
	Lang_PaymentBill_Cue(4)   = "成功彻底删除支付单."
	Lang_PaymentBill_Cue(5)   = "成功还原支付单."
	Lang_PaymentBill_Cue(6)   = ""
	Lang_PaymentBill_Cue(7)   = "支付单不存在."
	Lang_PaymentBill_Cue(8)   = "名称不能为空."

%>
