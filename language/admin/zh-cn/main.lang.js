/*
'=========================================================
' File     : main.lang.js
' 功能注释 : 系统后台语言文档(共用)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : linx
'=========================================================
*/

//前后台都必须并且一样的:
var Lang_Js_DiaTitle = [];
     Lang_Js_DiaTitle[0] = "信息提示";
	 
var Lang_Js_DiaText = [];
	 Lang_Js_DiaText[0] = "正在执行操作,请稍后...";
	 Lang_Js_DiaText[1] = "秒后自动关闭";
	 
var Lang_Js_PayTradeType = []
	 Lang_Js_PayTradeType[0] = "交易类型"
	 Lang_Js_PayTradeType[1] = "其他"
	 Lang_Js_PayTradeType[2] = "订单支付"
	 Lang_Js_PayTradeType[3] = "充值"
	 
var Lang_Js_PayTradeStatus = []
     Lang_Js_PayTradeStatus[0] = "交易状态"
	 Lang_Js_PayTradeStatus[1] = "支付完成"
	 Lang_Js_PayTradeStatus[2] = "支付失败"
	 Lang_Js_PayTradeStatus[3] = "支付取消"
	 Lang_Js_PayTradeStatus[4] = "支付错误"
	 Lang_Js_PayTradeStatus[5] = "支付无效"
	 Lang_Js_PayTradeStatus[6] = "支付处理中"
	 Lang_Js_PayTradeStatus[7] = "支付超时"
	 Lang_Js_PayTradeStatus[8] = "支付准备中"
	 Lang_Js_PayTradeStatus[9] = "等待支付"
	 Lang_Js_PayTradeStatus[10]= "未知状态"
	 
var Lang_Js_PayStatus = []
     Lang_Js_PayStatus[0] = "未支付"
	 Lang_Js_PayStatus[1] = "已支付"
	 Lang_Js_PayStatus[2] = "处理中"
	 Lang_Js_PayStatus[3] = "部分付款"
	 Lang_Js_PayStatus[4] = "部分退款"
	 Lang_Js_PayStatus[5] = "全额退款"

//共用;
	var Lang_Js_Btn_Close          = "关闭";
	var Lang_Js_Btn_Submit         = "提交保存";
	var Lang_Js_Btn_Confirm        = "确定";
	var Lang_Js_Btn_Select         = "选择";
	
	var Lang_Js_Add                = "添加";
	var Lang_Js_Edit               = "编辑";
	var Lang_Js_Del                = "删除";
	
	var Lang_Js_Year               = "年";
	var Lang_Js_Month              = "月";
	var Lang_Js_Day                = "日";
	var Lang_Js_PleaseSelect       = "请选择...";
	
	var Lang_Js_DataUploading      = "数据上传中...";
	var Lang_Js_ThisCannotEmpty    = "此项不能为空";
	var Lang_Js_HaveNotSelect      = "请选择";
	var Lang_Js_PasswordNotAccord  = "两次输入的密码不一致";
	var Lang_Js_AdvancedSearch     = "高级搜索";
	var Lang_Js_RadioSelect        = "单选";
	var Lang_Js_CheckBoxSelect     = "多选";
	var Lang_Js_Option             = "选项";
	var Lang_Js_Select             = "选择";
	var Lang_Js_SelectAll          = "全选";
	var Lang_Js_MaxCanSelectNOption= "最多选{tpl:num}项";
	var Lang_Js_ReplaceFor         = "替换为";
	var Lang_Js_Help               = "帮助";
	var Lang_Js_DoConfirmLeave     = "确定要离开本页面吗?";
	var Lang_Js_Yes                = "是";
	var Lang_Js_No                 = "否";
	var Lang_Js_OnUse              = "正常使用";
	var Lang_Js_HaveStopUse        = "已停用";
	var Lang_Js_OnRunning          = "正常运行";
	var Lang_Js_HaveStopRunning    = "停止";
	var Lang_Js_AreaSelect         = "地区选择";
	var Lang_Js_Image              = "图片"
	var Lang_Js_UploadImage        = "上传图片";
	var Lang_Js_ImageViewAndGetUrl = "查看图片和路径";
	var Lang_Js_ImageURL           = "图片路径";
	var Lang_Js_ImageWidth         = "图片宽";
	var Lang_Js_ImageHeight        = "图片高";
	var Lang_Js_PleaseSelect       = "请选择...";
	var Lang_Js_SetAsCover         = "选择设为首图";
	var Lang_Js_DelThisImage       = "删除此图";
	var Lang_Js_GetImageUrl        = "查看图片路径";
	
	
	var Lang_Js_ListTableColLine_ID = "主键ID"
	
//操作
	var Lang_Js_DoDel              = "删除";
	

//提示信息
	var Lang_Js_Cue_DataSubmiting     = "正在提交传输数据......";
	var Lang_Js_Cue_DataLoading       = "正在加载数据......";
	var Lang_Js_Cue_LoadingFail       = "数据加载失败.";
	var Lang_Js_Cue_ServerErrorAndTryLatter        = "服务器请求失败,请稍后再试试.";
	var Lang_Js_Cue_ServerErrorAndTryRefreshLatter = "服务器请求失败,请稍后再刷新试试.";
	var Lang_Js_Cue_CheckPermission   = "检查是否有权限.";
	var Lang_Js_Cue_HaveSelectNothing = "未选择任何内容!";
	var Lang_Js_Cue_CanbeUse          = "可以使用";
	var Lang_Js_Cue_GoodsBNHasBeenUsed= "商品编号已被使用,请检查.";
	
	var Lang_Js_ChangeCheckcodeTip    = "看不清楚可点击换一张";
	var Lang_Js_ChangeCheckcodeTipPre = "看不清";
	var Lang_Js_ChangeCheckcodeTipSub = "换一张";
	
	var Lang_Js      = [];
	     Lang_Js[1]  = "点击查看详细内容";
		 Lang_Js[2]  = "点击查看评论/咨询列表";
		 Lang_Js[3]  = "点击查看回复列表";
		 Lang_Js[5]  = "未审核";
		 Lang_Js[6]  = "已审核";
		 Lang_Js[7]  = "已审核";
		 Lang_Js[8]  = "条回复";
		 Lang_Js[9]  = "失败! 原因:";
		 Lang_Js[10] = "未明.";
		 Lang_Js[11] = "首播";
		 Lang_Js[12] = "留言";
		 Lang_Js[13] = "级回复";//1级回复、2级回复
		 Lang_Js[14] = "未回复";
	
	var Lang_Js_Cue       = [];
		 Lang_Js_Cue[1]   = "密码不能为空";
		 Lang_Js_Cue[2]   = "密码不能包含 ; = & < > % # / \ \" \' 等字符";
		 Lang_Js_Cue[3]   = "两次密码不一致";
		 Lang_Js_Cue[4]   = "您确定要导出选中内容吗?";
		 Lang_Js_Cue[5]   = "请选择数据所要导出到的内容模型及分类!"
		 Lang_Js_Cue[6]   = "您确定要将选中内容导出到其默认系统吗?"
		 Lang_Js_Cue[7]   = ""
		 Lang_Js_Cue[8]   = ""
		 Lang_Js_Cue[9]   = ""
		 
	var Lang_Js_ChangePassword          = "修改密码";
	var Lang_Js_InputNewPassword        = "输入新密码";
	var Lang_Js_NewPasswordConfirm      = "确认新密码";
	var Lang_Js_ChangePasswordSuccess   = "修改密码成功.";
	var Lang_Js_ChangePasswordFail      = "修改密码失败.";
	var Lang_Js_TwoPasswordsIsNotAccord = "两次密码不一致.";
	var Lang_Js_PasswordLengthWrong     = "密码由\"英文数字_-\"组成，最小长度:6 最大长度:20。"
	
//Ajax图片上传
	var Lang_Js_AjaxUp_Cue = [];
		 Lang_Js_AjaxUp_Cue[0] = ""
		 Lang_Js_AjaxUp_Cue[1] = "本次上传数量:"
		 Lang_Js_AjaxUp_Cue[2] = "名称:"
		 Lang_Js_AjaxUp_Cue[3] = "大小:"
		 Lang_Js_AjaxUp_Cue[4] = "失败原因:"
	 
//内容系统
	var Lang_Js_Content = [];
		 Lang_Js_Content[0]  = "自由评论";
		 Lang_Js_Content[1]  = "禁止评论";
		 Lang_Js_Content[2]  = "只允许会员评论";
	 
/**投票系统**/
	var Lang_Js_Cue_NotWriteVoteOptions            = "请填写投票选项";
	var Lang_Js_Cue_VoteOptionsCannotLessThanTwo   = "投票选项不能少于两个";

	
	
//帮助信息
	var Js_HelpMsg = []
	     Js_HelpMsg[1]  = "";
	     Js_HelpMsg[2]  = "";
	     Js_HelpMsg[3]  = "首次访问时，系统会自动生成静态页面保存起来，下次访问时，ASP不做运算，直接读取静态页面，以加快页面的访问速度(<b>相当于静态页面</b>)，缓解服务器压力，对SEO优化和用户体验都有极大的作用。<br>您可以进入[<a href=\"cache.asp?ctl=cache&act=view\" onclick=\"return confirm('"+Lang_Js_DoConfirmLeave+"');\">静态缓存管理</a>]页面进行缓存的管理";
	     Js_HelpMsg[4]  = "即最大采集量.<br />当采集网页的文章数量大于最大采集总数时，程序会在采集量达到最大采集总数时自动停止采集，以防止无限制采集。<br />"
	     Js_HelpMsg[5]  = "如采集页面里文章列表的html源码为:<br /><img src=\"Image/cction_01.gif\" class=\"code\" /><br />则列表区域识别规则为:<br /><img src=\"Image/cction_02.gif\" class=\"code\" />"
	     Js_HelpMsg[6]  = "若要采集页面里文章列表的html源码为:<br /><img src=\"Image/cction_01.gif\" class=\"code\" /><br />则文章链接URL识别规则为:<br />&lt;a href=\"[url]\"&gt;或是&lt;li&gt;&lt;a href=\"[url]\"&gt;"
	     Js_HelpMsg[7]  = "当采集的文章列表存在缩略图(预览图)时即可开启."
	     Js_HelpMsg[8]  = "此操作将去除网页多余代码如样式、脚本及其他html标签.<br />格式化的过程为程序自动分析,会存在一些误差."
	     Js_HelpMsg[9]  = "<b>活动定单: </b>即没完成也没作废的订单.<br><b>待付款订单: </b>所有未付款的活动定单.<br><b>已付款订单: </b>所有已付款的活动定单.<br><b>待发货订单: </b>所有还未发货的活动定单.<br><b>已发货订单: </b>所有已经发货的活动定单.<br><b>已退款订单: </b>所有已经退款的活动定单.<br><b>已退货订单: </b>所有已经退货的活动定单.<br><b>待付款待发货: </b>所有未付款也未发货的活动定单.<br><b>已发货待付款: </b>所有已发货但还未付款的活动定单.<br><b>已付款待发货: </b>所有已付款但还未发货的活动定单.<br><b>已退货待退款: </b>所有已退货但还未退清款的活动定单(支付状态为未支付和处理中的订单不用退款).<br><b>已作废订单: </b>已作废订的所有定单.<br><b>已完成订单: </b>已完成的所有定单.<br>";
	     Js_HelpMsg[10] = ""
	
	//内容
	var Js_Content_HelpMsg = []
	Js_Content_HelpMsg[1]  = "若使用转向链接，则点击内容标题时将直接跳转到该地址。"
	Js_Content_HelpMsg[2]  = "通常为在文章中\"最重要\"、\"最常出现\"、\"与该文章最为关联\"的关键词。"
	Js_Content_HelpMsg[3]  = "启用此功能后，系统会在保存内容时自动把相关图片下载保存到服务器上。<br>启用图片采集会影响内容保存效率，请慎重使用。"
	Js_Content_HelpMsg[4]  = ""
	Js_Content_HelpMsg[5]  = ""
	Js_Content_HelpMsg[6]  = ""
	Js_Content_HelpMsg[7]  = ""
	Js_Content_HelpMsg[8]  = ""
	
	//商品
	var Js_Goods     = [];
	    Js_Goods[1]  = "配件组名称";
		Js_Goods[2]  = "删除此组配件";
		Js_Goods[3]  = "最小购买量";
		Js_Goods[4]  = "最大购买量";
		Js_Goods[5]  = "0表示不限制";
		Js_Goods[6]  = "优惠方式";
		Js_Goods[7]  = "优惠一定金额";
		Js_Goods[8]  = "优惠价为某个折扣";
		Js_Goods[9]  = "优惠额度";
		Js_Goods[10] = "(优惠100元就输入100，优惠折扣价为原来的8.8折就输入0.88)(无优惠的设置方式：优惠方式选择优惠一定金额、优惠额度填写0)";
		Js_Goods[11] = "添加配件商品";
		Js_Goods[12] = "填加配件";
		Js_Goods[13] = "商品编号";
		Js_Goods[14] = "商品名称";
		Js_Goods[15] = "";
		Js_Goods[16] = "唯一不可选";
		Js_Goods[17] = "单选属性";
		Js_Goods[18] = "复选属性";
		Js_Goods[19] = "手工录入";
		Js_Goods[20] = "列表中选择";
		Js_Goods[21] = "多行文本框";
		Js_Goods[22] = "不存在id=GoodsAttr的表单文本域input,商品扩展属性功能无法启用!";
		Js_Goods[23] = "不存在id=GoodsAdjunct的表单文本域input,商品搭配销售功能无法启用!";
		
	var Js_Order     = [];
	    Js_Order[0]  = "";
		Js_Order[1]  = "订单支付";
		Js_Order[2]  = "订单发货";
	
	
	
	var Js_Goods_HelpMsg = [] 
	Js_Goods_HelpMsg[0]  = "<b>商品(直接销售型):</b>普通销售的商品.<br><b>配件(非直接销售型):</b>不可以单独销售，只可以作为配件搭配销售的商品.<br><b>赠品:</b> 非买品，作为购买商品时赠送的物品."
	Js_Goods_HelpMsg[1]  = "商品类型属性请进入[<a href=\"goodsmodel.asp?ctl=goodsmodel&act=view\" onclick=\"return confirm('"+Lang_Js_DoConfirmLeave+"');\">商品类型</a>]栏目添加"
	Js_Goods_HelpMsg[2]  = "选择[<b>唯一属性</b>]时，商品的该属性值只能设置一个值，商品前台用户只能查看该值。<br /><br />选择[<b>单选属性</b>]时，可以对商品该属性设置多个值，同时还能对不同属性值指定不同的价格加价，商品前台用户购买商品时只能选择一个属性值。<br /><br />选择[<b>复选属性</b>]时，可以对商品该属性设置多个值，同时还能对不同属性值指定不同的价格加价，商品前台用户购买商品时可以选择多个属性值。"
	Js_Goods_HelpMsg[3]  = ""
	Js_Goods_HelpMsg[4]  = ""
	Js_Goods_HelpMsg[5]  = ""
	Js_Goods_HelpMsg[6]  = ""
	Js_Goods_HelpMsg[7]  = ""
	Js_Goods_HelpMsg[8]  = ""
	
	var Lang_Js_Region =[];
	Lang_Js_Region[0]  = "[无上级]";
	Lang_Js_Region[1]  = "上级地区";
	Lang_Js_Region[2]  = "新地区名称";
	Lang_Js_Region[3]  = "排序";
	Lang_Js_Region[4]  = "数字越小越靠前";
	Lang_Js_Region[5]  = "添加地区";
	Lang_Js_Region[6]  = "编辑地区";
	Lang_Js_Region[7]  = "删除地区";
	Lang_Js_Region[8]  = "地区名称";
	Lang_Js_Region[9]  = "失败! 原因:";
	Lang_Js_Region[10] = "未明.";
	Lang_Js_Region[11] = "成功添加新地区";
	Lang_Js_Region[12] = "成功编辑地区";
	Lang_Js_Region[13] = "成功删除地区";
	Lang_Js_Region[14] = "您确定要删除[{$:Local_Name}]吗？";
	Lang_Js_Region[15] = "上级地区";
	
	
	var Lang_Js_Delivery =[];
	Lang_Js_Delivery[0]  = "";
	Lang_Js_Delivery[1]  = "请填写配送方式名称.";
	Lang_Js_Delivery[2]  = "请选择配送地区费用的设置方式.";
	Lang_Js_Delivery[3]  = "请指定配送地区.";
	Lang_Js_Delivery[4]  = "请选择首重重量.";
	Lang_Js_Delivery[5]  = "请选择续重单位.";
	
	Lang_Js_Delivery[6]  = "配送地区";
	Lang_Js_Delivery[7]  = "费用";
	Lang_Js_Delivery[8]  = "首重费用";
	Lang_Js_Delivery[9]  = "续重费用";
	Lang_Js_Delivery[10] = "";
	
	var Lang_Js_DeliveryCorp =[];
	Lang_Js_DeliveryCorp[0]  = "";
	Lang_Js_DeliveryCorp[1]  = "请填写物流公司名称.";
	
	var Lang_Js_Payment = [];
	Lang_Js_Payment[0] = "支持交易货币";
	Lang_Js_Payment[1] = "人民币";
	Lang_Js_Payment[2] = "合作者身份(parter ID)";
	Lang_Js_Payment[3] = "交易安全校验码(key)";
	Lang_Js_Payment[4] = "选择接口类型";
	Lang_Js_Payment[5] = "标准双接口";
	Lang_Js_Payment[6] = "即时到帐交易接口";
	Lang_Js_Payment[7] = "担保交易接口";
	Lang_Js_Payment[8] = "签约支付宝账号(seller_email)";
	
var Lang_Js_FileManage = [];
    Lang_Js_FileManage[0]  = "文件管理系统";
	Lang_Js_FileManage[1]  = "名称";
	Lang_Js_FileManage[2]  = "日期";
	Lang_Js_FileManage[3]  = "大小";
	Lang_Js_FileManage[4]  = "该文件夹为空";
	Lang_Js_FileManage[5]  = "附件目录";
	Lang_Js_FileManage[6]  = "请先选择要删除的文件夹和文件！";
	Lang_Js_FileManage[7]  = "正在删除，请稍候 ...";
	Lang_Js_FileManage[8]  = "删除完成";
	Lang_Js_FileManage[9]  = "正在扫描冗余文件，请稍候 ...";
	Lang_Js_FileManage[10] = "在 {tpl:folderpath} 中共扫描到 {tpl:folderscount} 个冗余文件夹和 {tpl:filescount} 个冗余文件，本次检测耗时 {tpl:runtime} 秒。";
	Lang_Js_FileManage[11] = "在 {tpl:folderpath} 中无冗余文件，本次检测耗时 {tpl:runtime} 秒。"
	Lang_Js_FileManage[12] = "您确定要删除选中的文件吗?";
	
	
	
	