<%
'=========================================================
' File     : systembackup.lang.asp
' 功能注释 : 系统后台语言文档(系统备份)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
Dim Lang_SystemBackup(20)
	Lang_SystemBackup(0)  = "系统备份"
	Lang_SystemBackup(1)  = "备份成功."
	Lang_SystemBackup(2)  = "备份失败."
	Lang_SystemBackup(3)  = "系统数据库还原成功"
	Lang_SystemBackup(4)  = "系统数据库还原失败"
	Lang_SystemBackup(5)  = "成功删除备份"
	Lang_SystemBackup(6)  = "删除备份失败"
	Lang_SystemBackup(7)  = "开始备份"
	Lang_SystemBackup(8)  = "删除备份"
	Lang_SystemBackup(9)  = "备份文件名称"
	Lang_SystemBackup(10) = "备份日期"
	Lang_SystemBackup(11) = "大小"
	Lang_SystemBackup(12) = "暂无备份"
	Lang_SystemBackup(13) = "请选择要删除的备份文件"
	Lang_SystemBackup(14) = "正在删除备份文件"
	Lang_SystemBackup(15) = "您确认要删除所选择的备份文件吗？"
	Lang_SystemBackup(16) = "系统还原"
	Lang_SystemBackup(17) = "请选择一个要还原的备份文件"
	Lang_SystemBackup(18) = "仅可以选择一个备份文件"
	Lang_SystemBackup(19) = "您确定要将系统数据库还原为{tpl:filepath}吗？"
	Lang_SystemBackup(20) = "很抱歉, KnifeCMS目前还不支持 Microsoft SQL Server 数据库备份。"
%>
