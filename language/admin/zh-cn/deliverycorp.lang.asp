<%
'=========================================================
' File     : deliverycorp.lang.asp
' 功能注释 : 系统后台语言文档(物流公司)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
	
Dim Lang_DeliveryCorp_ListTableColLine(5)
    Lang_DeliveryCorp_ListTableColLine(0) = "物流公司"
	Lang_DeliveryCorp_ListTableColLine(1) = "LOGO"
	Lang_DeliveryCorp_ListTableColLine(2) = "网址"
	Lang_DeliveryCorp_ListTableColLine(3) = "状态"
	Lang_DeliveryCorp_ListTableColLine(4) = "排序"
	Lang_DeliveryCorp_ListTableColLine(5) = "备注"
	
Dim Lang_DeliveryCorp(8)
    Lang_DeliveryCorp(0)   = "物流公司"
	Lang_DeliveryCorp(1)   = "物流公司名称"
	Lang_DeliveryCorp(2)   = "排序"
	Lang_DeliveryCorp(3)   = "网址"
	Lang_DeliveryCorp(4)   = "LOGO"
	Lang_DeliveryCorp(5)   = "详细介绍"
	Lang_DeliveryCorp(6)   = ""
	
Dim Lang_DeliveryCorp_Cue(12)
    Lang_DeliveryCorp_Cue(0)   = ""
	Lang_DeliveryCorp_Cue(1)   = "成功添加物流公司."
	Lang_DeliveryCorp_Cue(2)   = "成功编辑物流公司."
	Lang_DeliveryCorp_Cue(3)   = "成功删除物流公司."
	Lang_DeliveryCorp_Cue(4)   = "成功彻底删除物流公司."
	Lang_DeliveryCorp_Cue(5)   = "成功启动使用物流公司."
	Lang_DeliveryCorp_Cue(6)   = "成功停用物流公司."
	Lang_DeliveryCorp_Cue(7)   = "物流公司不存在."
	Lang_DeliveryCorp_Cue(8)   = "物流公司名称不能为空."
	Lang_DeliveryCorp_Cue(9)   = "数字越小越靠前."

%>
