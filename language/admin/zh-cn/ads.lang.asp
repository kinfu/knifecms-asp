<%
'=========================================================
' File     : ads.lang.asp
' 功能注释 : 系统后台语言文档(广告系统)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
	
Dim Lang_Ads_ListTableColLine(20)
	Lang_Ads_ListTableColLine(1) = "广告位名称"
	Lang_Ads_ListTableColLine(2) = "标识"
	Lang_Ads_ListTableColLine(3) = "系统默认"
	Lang_Ads_ListTableColLine(4) = "状态"
	Lang_Ads_ListTableColLine(5) = "开始时间"
	Lang_Ads_ListTableColLine(6) = "结束时间"
	Lang_Ads_ListTableColLine(7) = "宽"
	Lang_Ads_ListTableColLine(8) = "高"
	Lang_Ads_ListTableColLine(9) = "排序"
	
	Lang_Ads_ListTableColLine(10) = "内容管理"
	Lang_Ads_ListTableColLine(11) = "广告标题"
	Lang_Ads_ListTableColLine(12) = "广告链接地址"
	Lang_Ads_ListTableColLine(13) = "广告图片"
	Lang_Ads_ListTableColLine(14) = "广告简介"
	Lang_Ads_ListTableColLine(15) = "排序"
	Lang_Ads_ListTableColLine(16) = ""
	Lang_Ads_ListTableColLine(17) = ""
	Lang_Ads_ListTableColLine(18) = ""
	
Dim Lang_Ads(22)
    Lang_Ads(0)   = "广告"
	Lang_Ads(1)   = "广告位"
	Lang_Ads(2)   = "广告位名称"
	Lang_Ads(3)   = "广告标识"
	Lang_Ads(4)   = "开始时间"
	Lang_Ads(5)   = "结束时间"
	Lang_Ads(6)   = "广告位宽度"
	Lang_Ads(7)   = "广告位高度"
	Lang_Ads(8)   = "排序"
	Lang_Ads(9)   = "描述"
	Lang_Ads(10)  = "数字越小越靠前."
	Lang_Ads(11)  = "广告标题"
	Lang_Ads(12)  = "广告链接地址"
	Lang_Ads(13)  = "广告简介"
	Lang_Ads(14)  = "广告图片URL"
	Lang_Ads(15)  = ""
	Lang_Ads(16)  = "广告标题"
	Lang_Ads(17)  = ""
	Lang_Ads(18)  = ""
	Lang_Ads(19)  = ""
	Lang_Ads(20)  = ""
	Lang_Ads(21)  = ""
	
	
Dim Lang_Ads_Cue(30)
    Lang_Ads_Cue(0)   = ""
	Lang_Ads_Cue(1)   = "成功添加广告位."
	Lang_Ads_Cue(2)   = "成功编辑广告位."
	Lang_Ads_Cue(3)   = "成功删除广告位."
	Lang_Ads_Cue(4)   = "成功彻底删除广告位."
	Lang_Ads_Cue(5)   = "成功还原广告位."
	Lang_Ads_Cue(6)   = ""
	Lang_Ads_Cue(7)   = "广告位不存在."
	Lang_Ads_Cue(8)   = "广告位名称不能为空."
	Lang_Ads_Cue(9)   = "广告标识不能为空."
	Lang_Ads_Cue(10)  = "开始时间不能为空."
	Lang_Ads_Cue(11)  = "结束时间不能为空."
	Lang_Ads_Cue(12)  = "广告位宽度不能为空."
	Lang_Ads_Cue(13)  = "广告位高度不能为空."
	Lang_Ads_Cue(14)  = "已经存在一样标识的广告位."
	Lang_Ads_Cue(15)  = "结束时间不能小于开始时间."
	Lang_Ads_Cue(16)  = "系统默认广告位不能删除."
	Lang_Ads_Cue(17)  = "成功启动运行广告位"
	Lang_Ads_Cue(18)  = "成功停止广告位"
	Lang_Ads_Cue(19)  = """英文数字-_""的组合,如: index_ads_01"
	Lang_Ads_Cue(20)  = ""
	
Dim Lang_Ads_Detail_Cue(30)
    Lang_Ads_Detail_Cue(0)   = ""
	Lang_Ads_Detail_Cue(1)   = "成功添加广告."
	Lang_Ads_Detail_Cue(2)   = "成功编辑广告."
	Lang_Ads_Detail_Cue(3)   = "成功删除广告."
	Lang_Ads_Detail_Cue(4)   = ""
	Lang_Ads_Detail_Cue(5)   = ""
	Lang_Ads_Detail_Cue(6)   = ""
	Lang_Ads_Detail_Cue(7)   = "广告不存在."
	Lang_Ads_Detail_Cue(8)   = "广告图片URL不能为空."
	Lang_Ads_Detail_Cue(9)   = "请选择广告."
	Lang_Ads_Detail_Cue(10)  = ""
	Lang_Ads_Detail_Cue(11)  = ""
	Lang_Ads_Detail_Cue(12)  = ""
	Lang_Ads_Detail_Cue(13)  = ""
	Lang_Ads_Detail_Cue(14)  = ""
	Lang_Ads_Detail_Cue(15)  = ""
	Lang_Ads_Detail_Cue(16)  = ""
	

%>
