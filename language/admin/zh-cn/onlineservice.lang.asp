<%
'=========================================================
' File     : onlineservice.lang.asp
' 功能注释 : 系统后台语言文档(在线客服)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

Dim Lang_OnlineService(15)
    Lang_OnlineService(0)  = "在线客服"
	Lang_OnlineService(1)  = "显示"
	Lang_OnlineService(2)  = "隐藏"
	Lang_OnlineService(3)  = "再添加一个客服"
	Lang_OnlineService(4)  = "删除此客服"
	Lang_OnlineService(5)  = "名称"
	Lang_OnlineService(6)  = "电话"
	Lang_OnlineService(7)  = "QQ号"
	Lang_OnlineService(8)  = "旺旺"
	Lang_OnlineService(9)  = "其他客服代码"
	Lang_OnlineService(10) = "调用标签"
	Lang_OnlineService(11) = "成功保存在线客服设置"
	Lang_OnlineService(12) = "保存在线客服设置失败"
	Lang_OnlineService(13) = "skype"
	Lang_OnlineService(14) = "文本"

%>
