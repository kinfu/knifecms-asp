<%
'=========================================================
' File     : index.lang.asp
' 功能注释 : 系统首页主界面语言文档
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
Dim Lang_Index(6)
	Lang_Index(0)   = "桌面"	'desktop
	Lang_Index(1)   = "设置"	'account setting
	Lang_Index(2)   = "首页"	'view website
	Lang_Index(3)   = "帮助"	'help
	Lang_Index(4)   = "退出"	'logout
	Lang_Index(5)   = "刷新"	'refresh

	
Const Lang_Menu_SystemSet         = "系统管理"     'system set
Const Lang_Menu_SystemInfo        = "系统信息"     'system info
Const Lang_Menu_SystemBaseSet     = "系统基本设置" 'base setting
Const Lang_Menu_WebsiteNavigation = "网站导航管理" 'website navigation
Const Lang_Menu_Template          = "模板风格管理" 'template set
Const Lang_Menu_DistrictSet       = "地区管理"     'district set
Const Lang_Menu_Cache             = "静态缓存管理" 'cache

Const Lang_Menu_SysUserManage     = "系统用户"     'system user manage
Const Lang_Menu_SysUserGroups     = "系统用户组"   'system user group
Const Lang_Menu_SysUsers          = "系统用户"     'system user
Const Lang_Menu_SysUsersRecycle   = "用户回收站"   'system user recycle

Const Lang_Menu_SysLog            = "系统日志"     'system log manage
Const Lang_Menu_SysLogView        = "查看日志"     'system log
Const Lang_Menu_SysLogRecycle     = "日志回收站"   'system log recycle

Const Lang_Menu_SystemEManage     = "常规管理"     'system expand
Const Lang_Menu_SystemBackup      = "系统备份"     'system backup
Const Lang_Menu_OnlineService     = "在线客服"     'online service
Const Lang_Menu_FileManage        = "上传文件管理" 'file manage

Const Lang_Menu_ContentSystem     = "内容系统"     'content system
Const Lang_Menu_SubSystemManage   = "内容模型管理"   'sub system manage
Const Lang_Menu_FieldSet          = "自定义字段"   'system fieldset
Const Lang_Menu_CModule           = "自定义标签" 'content label

Const Lang_Menu_VisionSystem 	 = "视觉系统"     'vision system
Const Lang_Menu_VisualDesign 	 = "可视化设计"     'visual design
Const Lang_Menu_PageList 		 = "页面列表"     'page list

Const Lang_Menu_AddNewContent     = "添加新内容"   'add new content
Const Lang_Menu_ContentMange      = "内容管理"     'conent manage
Const Lang_Menu_ContentCategory   = "分类管理"     'category
Const Lang_Menu_ContentDraft      = "草稿箱"       'draft
Const Lang_Menu_ContentComment    = "评论管理"     'comment
Const Lang_Menu_ContentRecycle    = "回收站"       'recycle

Const Lang_Menu_AssistSystem      		= "功能模块"    'assist system
Const Lang_Menu_ModuleAdmin				= "模块管理"	'module admin
Const Lang_Menu_CollectSystem           = "采集系统"         'collection system
Const Lang_Menu_AddNewCollection        = "添加采集器"       'add new collection
Const Lang_Menu_CollectionManage        = "采集器管理"       'collection manage
Const Lang_Menu_CollectionResultManage  = "采集结果管理"     'collect result
Const Lang_Menu_VoteStstem     = "投票系统"       'vote system
Const Lang_Menu_AddNewVote     = "添加投票"       'add new vote
Const Lang_Menu_VoteManage     = "投票管理"       'vote manage
Const Lang_Menu_VoteCategory   = "分类管理"       'category
Const Lang_Menu_VoteRecycle    = "回收站"         'recycle

Const Lang_Menu_Guestbook      = "在线留言"       'feedback system
Const Lang_Menu_Link           = "友情链接"       'link


Const Lang_Menu_ShopSystem  = "网店系统"       'shop system
Const Lang_Menu_GoodsSystem     = "商品系统"      'goods system
Const Lang_Menu_AddNewGoods     = "添加新商品"    'add goods
Const Lang_Menu_GoodsManage     = "商品列表"      'goods manage
Const Lang_Menu_GoodsRecycle    = "商品回收站"    'recycle
Const Lang_Menu_GoodsFieldSet   = "自定商品字段"   'system fieldset
Const Lang_Menu_GoodsCommentAndConsult = "商品咨询/评论"  'comment and consult
Const Lang_Menu_GoodsConsult           = "商品咨询"       'comment
Const Lang_Menu_GoodsComment           = "商品评论"       'consult
Const Lang_Menu_GoodsModelAndCategory  = "商品类型/分类"  'model and category
Const Lang_Menu_GoodsModel             = "商品类型"       'goods model
Const Lang_Menu_GoodsCategory          = "商品分类"       'category
Const Lang_Menu_GoodsVirtualCategory   = "虚拟分类"       'virtual category
Const Lang_Menu_BrandSystem   = "品牌管理系统"        'brand system
Const Lang_Menu_BrandManage   = "品牌列表"            'brand manage
Const Lang_Menu_BrandRecycle  = "品牌回收站"          'recycle

Const Lang_Menu_OrderSystem           = "订单系统"       'order system
Const Lang_Menu_AddNewOrder           = "添加新订单"     'add new order
Const Lang_Menu_OrderManage           = "订单列表"       'order manage
Const Lang_Menu_OrderUnShipUnPay      = "待发货待付款"   'unship and unpayed
Const Lang_Menu_OrderShipedUnPay      = "已发货待付款"   'shiped but unpayed
Const Lang_Menu_OrderUnShipPay        = "已付款待发货"   'unship but payed
Const Lang_Menu_OrderReShipUnRefund   = "已退货待退款"   'reship but unrefund
Const Lang_Menu_OrderInvalid          = "已作废订单"     'invalid order
Const Lang_Menu_OrderFinish           = "已完成订单"     'finished order
Const Lang_Menu_OrderRecycle          = "订单回收站"     'recycle

Const Lang_Menu_BillManage            = "单据管理"     'bill manage
Const Lang_Menu_PaymentBill           = "收款单"       'payment bill
Const Lang_Menu_RefundBill            = "退款单"       'refund bill
Const Lang_Menu_ShipBill              = "发货单"       'ship bill
Const Lang_Menu_ReshipBill            = "退货单"       'reship bill

Const Lang_Menu_TradeRecord       = "费用交易记录" 'trade record
Const Lang_Menu_PaytradeRecord    = "支付交易记录" 'paytrade record

Const Lang_Menu_PaymentAndDelivery = "支付/配送管理"  'payment and delivery
Const Lang_Menu_PaymentManage      = "支付方式"       'payment manage
Const Lang_Menu_DeliveryManage     = "配送方式"       'delivery manage
Const Lang_Menu_DeliveryCorp       = "物流公司"       'delivery corp

Const Lang_Menu_MemberSystem           = "会员系统"    'member system
Const Lang_Menu_MemberManage           = "会员列表"    'member manage
Const Lang_Menu_MemberRecycle          = "会员回收站"  'recycle
Const Lang_Menu_MemberTwriter          = "微博列表"    'twriter
Const Lang_Menu_MemberGuestbook        = "会员留言板"  'guestbook

Const Lang_Menu_AdSystem        = "广告系统"         'ad system
Const Lang_Menu_AddNewAd        = "添加广告位"       'add new ad
Const Lang_Menu_AdManage        = "广告位列表"       'ad manage
Const Lang_Menu_AdRecycle       = "广告回收站"       'recycle

Const Lang_Menu_WeiXin        		= "微信系统"         'weixin system
Const Lang_Menu_AddNewWeiXin		= "添加微信号"
Const Lang_Menu_WeiXinManage		= "微信号管理"
Const Lang_Menu_WeiXinMenu			= "菜单管理"
Const Lang_Menu_WeiXinMenuConfig 	= "微信菜单设置"
Const Lang_Menu_WeiXinMsg			= "消息管理"
Const Lang_Menu_WeiXinMsgManage		= "微信消息管理"
Const Lang_Menu_WeiXinUser			= "用户管理"
Const Lang_Menu_WeiXinUserManage	= "微信用户管理"
%>
