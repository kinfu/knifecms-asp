<%
'=========================================================
' File     : userguestbook.lang.asp
' 功能注释 : 系统后台语言文档(留言管理)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

Dim Lang_Guestbook_ListTableColLine(14)
    Lang_Guestbook_ListTableColLine(0)  = ""
	Lang_Guestbook_ListTableColLine(1)  = "会员ID"
    Lang_Guestbook_ListTableColLine(2)  = "昵称"
	Lang_Guestbook_ListTableColLine(3)  = "头像"
	Lang_Guestbook_ListTableColLine(4)  = "审核"
	Lang_Guestbook_ListTableColLine(5)  = "回复"
	Lang_Guestbook_ListTableColLine(6)  = "留言内容"
	Lang_Guestbook_ListTableColLine(7)  = "联系电话"
	Lang_Guestbook_ListTableColLine(8)  = "联系邮箱"
	Lang_Guestbook_ListTableColLine(9)  = "时间"
	Lang_Guestbook_ListTableColLine(10) = "IP"
	Lang_Guestbook_ListTableColLine(11) = "类别"
	Lang_Guestbook_ListTableColLine(12) = "回复"
	Lang_Guestbook_ListTableColLine(13) = "链接网址"
	
Dim Lang_Guestbook(5)
    Lang_Guestbook(0)   = ""
	Lang_Guestbook(1)   = "会员ID"
	Lang_Guestbook(2)   = "昵称"
	Lang_Guestbook(3)   = "IP"
	
	
Dim Lang_Guestbook_Cue(10)
    Lang_Guestbook_Cue(0)   = "留言不存在."
	Lang_Guestbook_Cue(1)   = "成功回复."
	Lang_Guestbook_Cue(2)   = "成功编辑."
	Lang_Guestbook_Cue(3)   = "成功删除."
	Lang_Guestbook_Cue(4)   = "成功彻底删除留言."
	Lang_Guestbook_Cue(5)   = "成功还原."
	Lang_Guestbook_Cue(6)   = "回复不能为空."
	Lang_Guestbook_Cue(7)   = "通过审核"
	Lang_Guestbook_Cue(8)   = "未通过审核"

%>
