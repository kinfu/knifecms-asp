<%
'=========================================================
' File     : fieldset.lang.asp
' 功能注释 : 系统后台[自定义内容/商品字段]语言文档
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

Dim Lang_Field(30)
	Lang_Field(0) = "自定义字段"
	Lang_Field(1) = "字段"
	Lang_Field(2) = "字段名称"
	Lang_Field(3) = "字段类型"
	Lang_Field(4) = "所属模型"
	Lang_Field(5) = "是否为必填项"
	Lang_Field(6) = "必填"
	Lang_Field(7) = "排序"
	Lang_Field(8) = "模板标签"
	Lang_Field(9) = "字段不能为空"
	Lang_Field(10)= "字段名称不能为空"
	Lang_Field(11)= "所属模型不能为空"
	Lang_Field(12)= "字段类型不能为空"
	Lang_Field(13)= "成功添加自定义字段"
	Lang_Field(14)= "添加自定义字段失败"
	Lang_Field(15)= "成功编辑自定义字段"
	Lang_Field(16)= "编辑自定义字段失败"
	Lang_Field(17)= "成功删除自定义字段"
	Lang_Field(18)= "删除自定义字段失败"
	Lang_Field(19)= "自定义字段不存在"
	Lang_Field(20)= "备选内容"
	Lang_Field(21)= "一行代表一个选项"
	Lang_Field(22)= "请选择字段类型..."
	Lang_Field(23)= "已经存在此字段，不可重复"
	Lang_Field(24)= "只能由英文字母、数字和下划线组成，并且仅能字母开头，不以下划线结尾"
	Lang_Field(25)= "字段提示"
	Lang_Field(26)= "填写该字段表单信息时的提示文字"
	Lang_Field(27)= "列表页使用"
	Lang_Field(28)= "详细页使用"
%>
