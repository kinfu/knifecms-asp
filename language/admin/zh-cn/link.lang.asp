<%
'=========================================================
' File     : link.lang.asp
' 功能注释 : 系统后台语言文档
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

Dim Lang_Link(18)
	Lang_Link(0) = "友情链接"
	Lang_Link(1) = "链接类型"
	Lang_Link(2) = "网站名称"
	Lang_Link(3) = "网站LOGO"
	Lang_Link(4) = "链接网址"
	Lang_Link(5) = "简介"
	Lang_Link(6) = "是否推荐"
	Lang_Link(7) = "排序"
	Lang_Link(8) = "审核状态"
	Lang_Link(9) = "网站名称不能为空"
	Lang_Link(10)= "链接网址不能为空"
	Lang_Link(11)= "成功添加友情链接"
	Lang_Link(12)= "成功编辑友情链接"
	Lang_Link(13)= "友情链接不存在"
	Lang_Link(14)= "点击量"
	Lang_Link(15)= "成功删除友情链接"
	Lang_Link(16)= "删除友情链接失败"
%>
