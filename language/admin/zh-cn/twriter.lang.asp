<%
'=========================================================
' File     : twriter.lang.asp
' 功能注释 : 系统后台语言文档(会员微博管理)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

Dim Lang_Twriter_ListTableColLine(10)
    Lang_Twriter_ListTableColLine(0)  = ""
	Lang_Twriter_ListTableColLine(1)  = "会员ID"
	Lang_Twriter_ListTableColLine(2)  = "用户名"
	Lang_Twriter_ListTableColLine(3)  = "会员头像"
	Lang_Twriter_ListTableColLine(4)  = "微博内容"
	Lang_Twriter_ListTableColLine(5)  = "发表时间"
	Lang_Twriter_ListTableColLine(6)  = "回复数"
	Lang_Twriter_ListTableColLine(7)  = "类别"
	Lang_Twriter_ListTableColLine(8)  = ""
	Lang_Twriter_ListTableColLine(9)  = ""
	
Dim Lang_Twriter(5)
    Lang_Twriter(0)   = ""
	Lang_Twriter(1)   = "会员ID"
	Lang_Twriter(2)   = "用户名"
	Lang_Twriter(3)   = "上级内容ID"
	Lang_Twriter(4)   = "IP"
	
	
Dim Lang_Twriter_Cue(5)
    Lang_Twriter_Cue(0)   = ""
	Lang_Twriter_Cue(1)   = "成功发表微博."
	Lang_Twriter_Cue(2)   = ""
	Lang_Twriter_Cue(3)   = ""
	Lang_Twriter_Cue(4)   = "成功彻底删除微博."

%>
