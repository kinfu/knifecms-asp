<%
'=========================================================
' File     : vote.lang.asp
' 功能注释 : 系统后台语言文档(投票系统)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

Const Lang_ListTableColLine_VoteMode  = "投票方式"
Const Lang_ListTableColLine_Joins     = "参与量"

Const Lang_SeoTitleCue             = "不填写的情况下则自动复制投票主题"
Const Lang_SeoUrlCue               = "可不填"
Const Lang_MetaKeywordsCue         = "一般不超过100个字符"
Const Lang_MetaDescriptionCue      = "一般不超过200个字符"

Dim Lang_Vote(22)
    Lang_Vote(0)  = ""
	Lang_Vote(1)  = "投票主题"
	Lang_Vote(2)  = "所属分类"
	Lang_Vote(3)  = "描述"
	Lang_Vote(4)  = "选项"
	Lang_Vote(5)  = "+增加一项"
	Lang_Vote(6)  = "+增加三项"
	Lang_Vote(7)  = "投票方式"
	Lang_Vote(8)  = ""
	Lang_Vote(9)  = ""
	Lang_Vote(10) = "投票结果"
	Lang_Vote(11) = "最多选"
	Lang_Vote(12) = "项"

Dim Lang_VoteCue(15)
	Lang_VoteCue(0)  = "成功还原投票."
	Lang_VoteCue(1)  = "成功删除投票."
	Lang_VoteCue(2)  = "成功彻底删除投票."
	Lang_VoteCue(3)  = "投票主题不能为空."
	Lang_VoteCue(4)  = "投票方式-多选-最大选项不能小于2个."
	Lang_VoteCue(5)  = "请填写投票选项."
	Lang_VoteCue(6)  = "投票选项不能少于2个."
	Lang_VoteCue(7)  = "不存在该投票."
	Lang_VoteCue(8)  = "成功添加投票."
	Lang_VoteCue(9)  = "成功编辑投票."
	Lang_VoteCue(10) = "添加投票失败."
	Lang_VoteCue(11) = "请选择所属分类."
	Lang_VoteCue(12) = "投票选项不能操过100个."
	Lang_VoteCue(13) = "无投票选项."
	Lang_VoteCue(14) = "投票选项不能修改."
%>
