<%
'=========================================================
' File     : brand.lang.asp
' 功能注释 : 系统后台语言文档(品牌管理系统)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
Const Lang_SeoTitleCue             = "不填写的情况下则自动复制品牌名称"
Const Lang_SeoUrlCue               = "可不填"
Const Lang_MetaKeywordsCue         = "一般不超过100个字符"
Const Lang_MetaDescriptionCue      = "一般不超过200个字符"

Dim Lang_Brand_ListTableColLine(5)
    Lang_Brand_ListTableColLine(0) = "排序"
	Lang_Brand_ListTableColLine(1) = "品牌名称"
	Lang_Brand_ListTableColLine(2) = "商品数量"
	Lang_Brand_ListTableColLine(3) = "品牌LOGO"
	Lang_Brand_ListTableColLine(4) = "品牌URL"
	
Dim Lang_Brand(8)
    Lang_Brand(0)   = ""
	Lang_Brand(1)   = "品牌名称"
	Lang_Brand(2)   = "排序"
	Lang_Brand(3)   = "品牌网址"
	Lang_Brand(4)   = "品牌LOGO"
	Lang_Brand(5)   = "摘要介绍"
	Lang_Brand(6)   = "详细介绍"
	Lang_Brand(7)   = "查看商品列表"
	
Dim Lang_Brand_Cue(12)
    Lang_Brand_Cue(0)   = ""
	Lang_Brand_Cue(1)   = "成功添加新品牌."
	Lang_Brand_Cue(2)   = "成功编辑品牌."
	Lang_Brand_Cue(3)   = "成功删除品牌."
	Lang_Brand_Cue(4)   = "成功彻底删除品牌."
	Lang_Brand_Cue(5)   = "成功还原品牌."
	Lang_Brand_Cue(6)   = ""
	Lang_Brand_Cue(7)   = "品牌不存在."
	Lang_Brand_Cue(8)   = "品牌名称不能为空."
	Lang_Brand_Cue(9)   = "数字越小越靠前."
	Lang_Brand_Cue(10)  = "自动获取详细介绍的前200个字作为摘要介绍."
	Lang_Brand_Cue(11)  = "建议在一百个字之内"

%>
