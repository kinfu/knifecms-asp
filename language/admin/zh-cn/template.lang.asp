<%
'=========================================================
' File     : template.lang.asp
' 功能注释 : 系统后台语言文档
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

'信息列表表格栏
	
Dim Lang_Template(20)
	Lang_Template(0) = "模板"
	Lang_Template(1) = "当前使用模板"
	Lang_Template(2) = "其他可用模板"
	Lang_Template(3) = "启用此模板"
	Lang_Template(4) = "编辑模板文件"
	Lang_Template(5) = "详细信息"
	Lang_Template(6) = "删除"
	Lang_Template(7) = "查看模板文件"
	Lang_Template(8) = "作者为"
	Lang_Template(9) = "访问作者主页"
	Lang_Template(10)= "查看演示"
	Lang_Template(11)= "该模板下不存在模板信息文件template.xml"
	Lang_Template(12)= "无详细信息"
	Lang_Template(13)= "无效的模板文件夹"
	Lang_Template(14)= "成功启用模板[{ptl:templatename}]"
	Lang_Template(15)= "启用模板[{ptl:templatename}]失败"
	Lang_Template(16)= "成功删除模板[{ptl:templatename}]"
	Lang_Template(17)= "删除模板[{ptl:templatename}]失败"
	Lang_Template(18)= "您确定要删除模板“{tpl:templatename}”吗？"
	Lang_Template(19)= "预览"

%>
