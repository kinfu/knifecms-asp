<%
'=========================================================
' File     : cache.lang.asp
' 功能注释 : 系统后台语言文档(缓存系统)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

Const Lang_Btn_CacheList           = "缓存列表"
Const Lang_Btn_ClearCache          = "清除选中缓存"
Const Lang_Btn_UpdateCache         = "重新生成整站静态缓存页"
Const Lang_UpdateCacheFinished     = "更新缓存完毕"

Dim Lang_Cache(25)
	Lang_Cache(0) = "缓存"
	Lang_Cache(1) = "正在清除缓存..."
	Lang_Cache(2) = "成功清除缓存文件"
	Lang_Cache(3) = "清除缓存文件失败"
	Lang_Cache(4) = "成功清除缓存目录"
	Lang_Cache(5) = "清除缓存目录失败"
	Lang_Cache(6) = "该文件夹下无缓存文件."
	Lang_Cache(7) = "天前"
	Lang_Cache(8) = "小时前"
	Lang_Cache(9) = "秒前"
	Lang_Cache(10)= "开始生成内容系统缓存..."
	Lang_Cache(11)= "正在生成{tpl:systemname}缓存..."
	Lang_Cache(12)= "{tpl:systemname}缓存生成完毕."
	Lang_Cache(13)= "正在生成投票系统缓存..."
	Lang_Cache(14)= "投票系统缓存生成完毕."
	Lang_Cache(15)= "正在生成网店系统缓存..."
	Lang_Cache(16)= "网店商品系统缓存生成完毕."
	Lang_Cache(17)= "生成首页缓存."
	Lang_Cache(18)= "生成登陆页缓存."
	Lang_Cache(19)= "生成注册页缓存."
	Lang_Cache(20)= "生成购物车页缓存."
	Lang_Cache(21)= "生成在线留言页缓存."
	Lang_Cache(22)= "生成404错误页缓存."
	Lang_Cache(23)= "缓存生成时间跟系统内容的多少成正比，如果你的系统内容比较多，则生成缓存的时间会比较长，请耐心等候。"
	
	

%>

