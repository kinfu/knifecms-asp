<%
'=========================================================
' File     : refundbill.lang.asp
' 功能注释 : 系统后台语言文档(退款单)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
	
Dim Lang_RefundBill_ListTableColLine(12)
    Lang_RefundBill_ListTableColLine(0)  = ""
	Lang_RefundBill_ListTableColLine(1)  = "退款单号"
	Lang_RefundBill_ListTableColLine(2)  = "退款金额"
	Lang_RefundBill_ListTableColLine(3)  = "货币"
	Lang_RefundBill_ListTableColLine(4)  = "订单号"
	Lang_RefundBill_ListTableColLine(5)  = "支付方式"
	Lang_RefundBill_ListTableColLine(6)  = "退款银行"
	Lang_RefundBill_ListTableColLine(7)  = "退款账号"
	Lang_RefundBill_ListTableColLine(8)  = "支付完成时间"
	Lang_RefundBill_ListTableColLine(9)  = "会员用户名"
	Lang_RefundBill_ListTableColLine(10) = "收款账户"
	Lang_RefundBill_ListTableColLine(11) = "操作员"
	
	
Dim Lang_RefundBill(21)
    Lang_RefundBill(0)   = "退款单"
	Lang_RefundBill(1)   = "退款单信息"
	Lang_RefundBill(2)   = "退款单号"
	Lang_RefundBill(3)   = "订单号"
	Lang_RefundBill(4)   = "退款银行"
	Lang_RefundBill(5)   = "退款帐号"
	Lang_RefundBill(6)   = "退款金额"
	Lang_RefundBill(7)   = "收款账户"
	Lang_RefundBill(8)   = "退款方式"
	Lang_RefundBill(9)   = "支付方式"
	Lang_RefundBill(10)  = "生成时间"
	Lang_RefundBill(11)  = "退款时间"
	Lang_RefundBill(12)  = "会员用户名"
	Lang_RefundBill(13)  = "操作员"
	Lang_RefundBill(14)  = "备注"
	Lang_RefundBill(15)  = "货币"
	Lang_RefundBill(16)  = "货币金额"
	Lang_RefundBill(17)  = "支付手续费"
	Lang_RefundBill(18)  = "支付状态"
	Lang_RefundBill(19)  = "IP"
	Lang_RefundBill(20)  = "是否有效"
	
Dim Lang_RefundBill_Cue(10)
    Lang_RefundBill_Cue(0)   = ""
	Lang_RefundBill_Cue(1)   = "成功添加新退款单."
	Lang_RefundBill_Cue(2)   = "成功编辑退款单."
	Lang_RefundBill_Cue(3)   = "成功删除退款单."
	Lang_RefundBill_Cue(4)   = "成功彻底删除退款单."
	Lang_RefundBill_Cue(5)   = "成功还原退款单."
	Lang_RefundBill_Cue(6)   = ""
	Lang_RefundBill_Cue(7)   = "退款单不存在."
	Lang_RefundBill_Cue(8)   = "名称不能为空."

%>
