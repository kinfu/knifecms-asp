<%
'=========================================================
' File     : payment.lang.asp
' 功能注释 : 系统后台语言文档(支付方式)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
	
Dim Lang_Payment_ListTableColLine(5)
    Lang_Payment_ListTableColLine(0) = "支付方式名称"
	Lang_Payment_ListTableColLine(1) = "状态"
	Lang_Payment_ListTableColLine(2) = "排序"
	Lang_Payment_ListTableColLine(3) = "手续费"
	Lang_Payment_ListTableColLine(4) = "手续费设置"
	
Dim Lang_Payment(15)
    Lang_Payment(0)   = "支付方式"
	Lang_Payment(1)   = "支付方式名称"
	Lang_Payment(2)   = "支付手续费设置"
	Lang_Payment(3)   = "支付手续费设置"
	Lang_Payment(4)   = "按比例收费"
	Lang_Payment(5)   = "固定费用"
	Lang_Payment(6)   = "费率"
	Lang_Payment(7)   = "说明：顾客将支付订单总金额乘以此费率作为手续费."
	Lang_Payment(8)   = "金额"
	Lang_Payment(9)   = "说明：顾客每笔订单需要支付的手续费."
	Lang_Payment(10)  = "排序"
	Lang_Payment(11)  = "数字越小越靠前."
	Lang_Payment(12)  = "说明"
	Lang_Payment(13)  = ""
	
	
Dim Lang_Payment_Cue(12)
    Lang_Payment_Cue(0)   = ""
	Lang_Payment_Cue(1)   = "成功添加支付方式."
	Lang_Payment_Cue(2)   = "成功编辑支付方式."
	Lang_Payment_Cue(3)   = "成功删除支付方式."
	Lang_Payment_Cue(4)   = "成功彻底删除支付方式."
	Lang_Payment_Cue(5)   = "成功启动使用支付方式."
	Lang_Payment_Cue(6)   = "成功停用支付方式."
	Lang_Payment_Cue(7)   = "支付方式不存在."
	Lang_Payment_Cue(8)   = "请填写支付方式名称.."
	Lang_Payment_Cue(9)   = "请选择支付方式."
	Lang_Payment_Cue(10)  = "."
	Lang_Payment_Cue(11)  = ""

%>
