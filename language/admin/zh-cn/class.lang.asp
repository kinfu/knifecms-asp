<%
'=========================================================
' File     : class.lang.asp
' 功能注释 : 系统后台语言文档(分类系统)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

Const Lang_Class_GoodsManage = "商品管理"
Const Lang_SeoTitleCue             = "不填写的情况下则自动复制分类名称"
Const Lang_SeoUrlCue               = "可不填"
Const Lang_MetaKeywordsCue         = "一般不超过100个字符"
Const Lang_MetaDescriptionCue      = "一般不超过200个字符"

Dim Lang_Class(22)
    Lang_Class(0)  = "添加子类"
	Lang_Class(1)  = "名称"
	Lang_Class(2)  = "上级分类"
	Lang_Class(3)  = "排序"
	Lang_Class(4)  = "显示"
	Lang_Class(5)  = "单页内容"
	Lang_Class(6)  = "META_KEYWORDS<br />(页面关键词)"
	Lang_Class(7)  = "META_DESCRIPTION<br />(页面描述)"
	Lang_Class(8)  = "优先显示单页内容"
	Lang_Class(9)  = "隐藏"
	Lang_Class(10) = "投票结果"
	Lang_Class(11) = "最多选"
	Lang_Class(12) = "项"
	Lang_Class(13) = "前台筛选类型"
	Lang_Class(14) = "模版设置"
	Lang_Class(15) = "此分类页模板文件"
	Lang_Class(16) = "内容页模板文件"
	Lang_Class(17) = "图片设置"
	Lang_Class(18) = "Icon标识图"
	Lang_Class(19) = "分类图片"
	Lang_Class(20) = "+展开"
	Lang_Class(21) = "-收起"
	

Dim Lang_ClassCue(20)
	Lang_ClassCue(0)  = "成功添加分类."
	Lang_ClassCue(1)  = "成功编辑分类."
	Lang_ClassCue(2)  = "成功删除分类."
	Lang_ClassCue(3)  = "成功批量编辑分类."
	Lang_ClassCue(4)  = "分类名称不能为空."
	Lang_ClassCue(5)  = "数字越小越靠前"
	Lang_ClassCue(6)  = "隐藏即表示不显示该分类"
	Lang_ClassCue(7)  = "不存在该分类."
	Lang_ClassCue(8)  = ""
	Lang_ClassCue(9)  = ""
	Lang_ClassCue(10) = "本模型允许最大分类为{$:allowDepth}级,请选择级数小于{$:allowDepth}的上级分类."'改为其他语言时"{$:allowDepth}"不可修改.
	Lang_ClassCue(11) = "请选择所属分类."
	Lang_ClassCue(12) = "不能选择自己为上一级分类."
	Lang_ClassCue(13) = "不能移到自己的分类下."
	Lang_ClassCue(14) = "该分类下有子分类,请先删除子分类."
	Lang_ClassCue(15) = "排序只在同一父类下的同级间比较,数字越小越靠前."
	Lang_ClassCue(16) = "提交的数据不完整,无法进行批量编辑操作!"
	Lang_ClassCue(17) = "打勾时优先显示下面的单页内容,不打勾则显示内容列表."
	Lang_ClassCue(18) = "继承上级模版设置"

%>