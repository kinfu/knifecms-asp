<%
'=========================================================
' File     : userguestbook.lang.asp
' 功能注释 : 系统后台语言文档(会员留言板管理)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

Dim Lang_Guestbook_ListTableColLine(10)
    Lang_Guestbook_ListTableColLine(0)  = ""
	Lang_Guestbook_ListTableColLine(1)  = "会员ID"
    Lang_Guestbook_ListTableColLine(2)  = "用户名"
	Lang_Guestbook_ListTableColLine(3)  = "会员头像"
	Lang_Guestbook_ListTableColLine(4)  = "留言内容"
	Lang_Guestbook_ListTableColLine(5)  = "发表时间"
	Lang_Guestbook_ListTableColLine(6)  = "回复数"
	Lang_Guestbook_ListTableColLine(7)  = "类别"
	Lang_Guestbook_ListTableColLine(8)  = ""
	
Dim Lang_Guestbook(5)
    Lang_Guestbook(0)   = ""
	Lang_Guestbook(1)   = "会员ID"
	Lang_Guestbook(2)   = "用户名"
	Lang_Guestbook(3)   = "上级内容ID"
	Lang_Guestbook(4)   = "IP"
	
	
Dim Lang_Guestbook_Cue(5)
    Lang_Guestbook_Cue(0)   = "留言不存在."
	Lang_Guestbook_Cue(1)   = "成功留言."
	Lang_Guestbook_Cue(2)   = ""
	Lang_Guestbook_Cue(3)   = ""
	Lang_Guestbook_Cue(4)   = "成功彻底删除留言."

%>
