<%
'=========================================================
' File     : user.lang.asp
' 功能注释 : 系统后台语言文档(会员用户管理)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

Dim Lang_User_ListTableColLine(18)
    Lang_User_ListTableColLine(0)  = "会员ID"
	Lang_User_ListTableColLine(1)  = "用户名"
	Lang_User_ListTableColLine(2)  = "会员等级"
	Lang_User_ListTableColLine(3)  = "姓名"
	Lang_User_ListTableColLine(4)  = "性别"
	Lang_User_ListTableColLine(5)  = "Email"
	Lang_User_ListTableColLine(6)  = "积分"
	Lang_User_ListTableColLine(7)  = "手机号码"
	Lang_User_ListTableColLine(8)  = "注册时间"
	Lang_User_ListTableColLine(9)  = "上次登录时间"
	Lang_User_ListTableColLine(10) = "备注"
	Lang_User_ListTableColLine(11) = "状态"
	Lang_User_ListTableColLine(12) = "定单"
	Lang_User_ListTableColLine(13) = "商品咨询"
	Lang_User_ListTableColLine(14) = "商品评论"
	Lang_User_ListTableColLine(15) = "小头像"
	Lang_User_ListTableColLine(16) = "登陆次数"
    Lang_User_ListTableColLine(17) = "IP"
	Lang_User_ListTableColLine(18) = "大头像"
	
Dim Lang_User(30)
    Lang_User(0)   = "会员信息"
	Lang_User(1)   = "会员信息"
	Lang_User(2)   = "用户名"
	Lang_User(3)   = "Email"
	Lang_User(4)   = "状态"
	Lang_User(5)   = "注册时间"
	Lang_User(6)   = "上次登录时间"
	Lang_User(7)   = "基本信息"
	Lang_User(8)   = "真实姓名"
	Lang_User(9)   = "性 别"
	Lang_User(10)  = "生 日"
	Lang_User(11)  = "家 乡"
	Lang_User(12)  = "目前所在地"
	Lang_User(13)  = "婚姻状态"
	Lang_User(14)  = "身 高"
	Lang_User(15)  = "体 重"
	Lang_User(16)  = "血 型"
	Lang_User(17)  = "联系方式"
	Lang_User(18)  = "QQ号码"
	Lang_User(19)  = "手机号码"
	Lang_User(20)  = "联系电话"
	Lang_User(21)  = "其他信息"
	Lang_User(22)  = "吸 烟"
	Lang_User(23)  = "喝 酒"
	Lang_User(24)  = "宗教信仰"
	Lang_User(25)  = "梦想的旅游地"
	Lang_User(26)  = "梦想的居住地"
	Lang_User(27)  = "梦想的工作"
	Lang_User(28)  = "梦想的伴侣"
	Lang_User(29)  = "梦想的汽车"
	
	
Dim Lang_User_Cue(17)
    Lang_User_Cue(0)   = "会员不存在."
	Lang_User_Cue(1)   = "成功添加新会员."
	Lang_User_Cue(2)   = "成功编辑会员."
	Lang_User_Cue(3)   = "成功删除会员至回收站."
	Lang_User_Cue(4)   = "成功彻底删除会员."
	Lang_User_Cue(5)   = "成功还原会员."
	Lang_User_Cue(6)   = "成功禁用会员."
	Lang_User_Cue(7)   = "成功启用会员."
	Lang_User_Cue(8)   = "会员信息修改成功!"
	Lang_User_Cue(9)   = "必须填写Email."
	Lang_User_Cue(10)  = "Email格式不正确."
	Lang_User_Cue(11)  = "Email已经被使用了."
	Lang_User_Cue(12)  = "会员信息修改失败!"
	Lang_User_Cue(13)  = "删除会员时会删除所有与该会员有关的资料和信息，请谨慎操作！"
	Lang_User_Cue(14)  = "删除会员<b>{$:username}</b>失败,其为系统用户<b>{$:sysusername}</b>的前台关系会员,若要删除请先解除<b>{$:sysusername}</b>与<b>{$:username}</b>的绑定关系."
	Lang_User_Cue(15)  = ""
	Lang_User_Cue(16)  = ""
	
Dim Lang_Gender(3)
    Lang_Gender(0) = "保密"
	Lang_Gender(1) = "男"
	Lang_Gender(2) = "女"
	
Dim Lang_MaritalStatus(3)
    Lang_MaritalStatus(0) = "保密"
	Lang_MaritalStatus(1) = "未婚"
	Lang_MaritalStatus(2) = "已婚"

Dim Lang_BloodType(4)
    Lang_BloodType(0) = "A型"
	Lang_BloodType(1) = "B型"
	Lang_BloodType(2) = "O型"
	Lang_BloodType(3) = "AB型"
	
Dim Lang_Smoke(4)
    Lang_Smoke(0) = "不吸烟"
	Lang_Smoke(1) = "很少吸烟"
	Lang_Smoke(2) = "偶尔吸烟"
	Lang_Smoke(3) = "经常吸烟"
	
Dim Lang_Drink(4)
    Lang_Drink(0) = "不喝酒"
	Lang_Drink(1) = "很少喝酒"
	Lang_Drink(2) = "偶尔喝酒"
	Lang_Drink(3) = "经常喝酒"

Dim Lang_Religion(6)
    Lang_Religion(0) = "无宗教信仰"
	Lang_Religion(1) = "佛教"
	Lang_Religion(2) = "道教"
	Lang_Religion(3) = "基督教"
	Lang_Religion(4) = "伊斯兰教"
	Lang_Religion(5) = "天主教"

%>
