<%
'=========================================================
' File     : paytradeno.lang.asp
' 功能注释 : 系统后台语言文档(支付交易记录)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
	
Dim Lang_PayTradeLog_ListTableColLine(12)
    Lang_PayTradeLog_ListTableColLine(0)  = "交易类型"
	Lang_PayTradeLog_ListTableColLine(1)  = "交易单号"
	Lang_PayTradeLog_ListTableColLine(2)  = "货币"
	Lang_PayTradeLog_ListTableColLine(3)  = "交易金额"
	Lang_PayTradeLog_ListTableColLine(4)  = "状态"
	Lang_PayTradeLog_ListTableColLine(5)  = "支付方式"
	Lang_PayTradeLog_ListTableColLine(6)  = "订单号"
	Lang_PayTradeLog_ListTableColLine(7)  = "开始时间"
	Lang_PayTradeLog_ListTableColLine(8)  = "完成时间"
	Lang_PayTradeLog_ListTableColLine(9)  = "会员ID"
	Lang_PayTradeLog_ListTableColLine(10) = "银行交易号"
	Lang_PayTradeLog_ListTableColLine(11) = "IP"
	Lang_PayTradeLog_ListTableColLine(12) = "备注"

%>
