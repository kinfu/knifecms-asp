<%
'=========================================================
' File     : content.lang.asp
' 功能注释 : 系统后台语言文档(内容系统)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================

Const Lang_CreateTime          = "创建时间"

Const Lang_CreateNewSubSystem  = "安装新模型"
Const Lang_SubSystemSet        = "设置"
Const Lang_UninstallSubSystem  = "卸载"
Const Lang_UninstallSubSystemConfirm = "卸载将会删除此模型所有的数据！\n\n您确认要卸载吗？"

Const Lang_ListTableColLine_BriefTitle = "简短标题"
Const Lang_ListTableColLine_Author     = "作者"
Const Lang_ListTableColLine_Thumbnail  = "缩略图"
Const Lang_ListTableColLine_CommentSet = "评论设置"
Const Lang_ListTableColLine_TopLine    = "头条"
Const Lang_ListTableColLine_OnTop      = "置顶"
Const Lang_ListTableColLine_Hot        = "热门"
Const Lang_ListTableColLine_Elite      = "精华"

Const Lang_SeoTitleCue             = ""
Const Lang_SeoTitleCue_0           = "不填写的情况下则自动复制系统名称"
Const Lang_SeoUrlCue               = ""
Const Lang_SeoUrlCue_0             = "可不填"
Const Lang_MetaKeywordsCue         = "最多允许100个字符"
Const Lang_MetaKeywordsCue_0       = "一般不超过100个字符"
Const Lang_MetaDescriptionCue      = "最多允许250个字符"
Const Lang_MetaDescriptionCue_0    = "一般不超过200个字符"

Dim Lang_SubSystem(10)
    Lang_SubSystem(0)  = "模型参数设置"
	Lang_SubSystem(1)  = "模型参数"
	Lang_SubSystem(2)  = "模型名称"
	Lang_SubSystem(3)  = "模型排序"
	Lang_SubSystem(4)  = "选择模板文件"
	Lang_SubSystem(5)  = "模型首页模板文件"
	Lang_SubSystem(6)  = "分类页模板文件"
	Lang_SubSystem(7)  = "内容页模板文件"
	Lang_SubSystem(8)  = ""
	Lang_SubSystem(9)  = ""
	
'一些提示信息
Dim Lang_SubSystemCue(17)
    Lang_SubSystemCue(0)  = ""
	Lang_SubSystemCue(1)  = "模型参数必须为小写字母的组合 ( 如: 新闻中心 则写成 news 或 xinwen )"
	Lang_SubSystemCue(2)  = "模型名称 ( 如: 新闻中心 )"
	Lang_SubSystemCue(3)  = "模型参数不能为空."
	Lang_SubSystemCue(4)  = "模型名称不能为空."
	Lang_SubSystemCue(5)  = "成功安装模型."
	Lang_SubSystemCue(6)  = "成功设置模型."
	Lang_SubSystemCue(7)  = "不存在该模型."
	Lang_SubSystemCue(8)  = "数值越小越靠前."
	Lang_SubSystemCue(9)  = "默认为空(即使用默认的模板文件)."
	Lang_SubSystemCue(10)  = "已经存在系统参数值相同的模型."
	Lang_SubSystemCue(11)  = "已经存在系统名称相同的模型."
	Lang_SubSystemCue(12)  = "成功卸载模型."
	Lang_SubSystemCue(13)  = "系统默认的系统不可以卸载."
	Lang_SubSystemCue(14)  = "默认为content.subsys.htm"
	Lang_SubSystemCue(15)  = "默认为content.subsys.category.htm"
	Lang_SubSystemCue(16)  = "默认为content.subsys.detail.htm"

Dim Lang_ContentNav(7)
    Lang_ContentNav(1)  = "基本内容填写"
	Lang_ContentNav(2)  = "高级表单内容"
	Lang_ContentNav(3)  = "SEO优化设置"
	Lang_ContentNav(4)  = "添加关联投票"
	Lang_ContentNav(5)  = "添加相关内容"
	Lang_ContentNav(6)  = "SEO优化设置"
	Lang_ContentNav(7)  = "自定义字段"
	
Dim Lang_ContentBtn(3)    
    Lang_ContentBtn(0)  = "修改完毕保存"
	Lang_ContentBtn(1)  = "保存发布"
	Lang_ContentBtn(2)  = "保存草稿"
		
Dim Lang_Content(35)
    Lang_Content(0)  = ""
	Lang_Content(1)  = "所属分类"
	Lang_Content(2)  = "标题"
	Lang_Content(3)  = "简短标题"
	Lang_Content(4)  = "作者"
	Lang_Content(5)  = "来源"
	Lang_Content(6)  = "发布时间"
	Lang_Content(7)  = "缩略图"
	Lang_Content(8)  = "转向链接"
	Lang_Content(9)  = "关键词标签"
	Lang_Content(10) = "内容摘要"
	Lang_Content(11) = "分页设置"
	Lang_Content(12) = "图片采集"
	Lang_Content(13) = "详细内容"
	Lang_Content(14) = "浏览量"
	Lang_Content(15) = "属性"
	Lang_Content(16) = "评论设置"
	Lang_Content(17) = "保密设置"
	Lang_Content(18) = "头条"
	Lang_Content(19) = "置顶"
	Lang_Content(20) = "热门"
	Lang_Content(21) = "精华"
	Lang_Content(22) = "自由评论"
	Lang_Content(23) = "禁止评论"
	Lang_Content(24) = "只允许会员评论"
	Lang_Content(25) = "完全公开"
	Lang_Content(26) = "完全保密"
	Lang_Content(27) = "查看需要密码"
	Lang_Content(28) = "输入密码"
	Lang_Content(29) = "确认密码"
	Lang_Content(30) = "多图片"
	Lang_Content(31) = "关联投票"
	Lang_Content(32) = "相关内容"
	
	
	
Dim Lang_ContentCue(20)
    Lang_ContentCue(0)  = ""
	Lang_ContentCue(1)  = "成功添加内容."
	Lang_ContentCue(2)  = "成功编辑内容."
	Lang_ContentCue(3)  = "成功删除内容."
	Lang_ContentCue(4)  = "成功彻底删除内容."
	Lang_ContentCue(5)  = "成功发布内容."
	Lang_ContentCue(6)  = "成功还原内容."
	Lang_ContentCue(7)  = "不存在内容."
	Lang_ContentCue(8)  = ""
	Lang_ContentCue(9)  = ""
	Lang_ContentCue(10) = "采用详细内容的第一张图片作为缩略图"
	Lang_ContentCue(11) = "使用转向链接"
	Lang_ContentCue(12) = "中间用“,”隔开(为空时系统将会自动从内容中提取6个关键词)"
	Lang_ContentCue(13) = "采用详细内容的前250个字作为摘要"
	Lang_ContentCue(14) = "分页符标记为“&lt;!--[nextpage]--&gt;”；手动分页请在HTML源代码模式下加入分页符标记，注意大小写。"
	Lang_ContentCue(15) = "采集图片保存到本地"
	Lang_ContentCue(16) = "请选择分类."
	Lang_ContentCue(17) = "标题不能为空."
	Lang_ContentCue(18) = "请填写查看密码."
	Lang_ContentCue(19) = "密码长度不能超过50位."

Dim Lang_CollectCue(3)
	Lang_CollectCue(0)  = ""
	Lang_CollectCue(1)  = "正在采集图片......"
	Lang_CollectCue(2)  = "图片采集完成."

























%>
