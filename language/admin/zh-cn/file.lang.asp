<%
'=========================================================
' File     : file.lang.asp
' 功能注释 : 系统后台语言文档(文件[图片、视频等]管理)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
'Ajax图片上传
Dim Lang_AjaxUp(15)
    Lang_AjaxUp(0)       = ""
	Lang_AjaxUp(1)       = "本地上传"
	Lang_AjaxUp(2)       = "服务器中选择"
	Lang_AjaxUp(3)       = "网络添加"
	Lang_AjaxUp(4)       = "展开"
	Lang_AjaxUp(5)       = "收起"
	Lang_AjaxUp(6)       = "网络路径"
	Lang_AjaxUp(7)       = ""
	Lang_AjaxUp(8)       = "准备插入的图片"
	Lang_AjaxUp(9)       = "准备插入的文件"
	Lang_AjaxUp(10)      = "准备插入的Flash"
	Lang_AjaxUp(11)      = "准备插入的音乐"
	Lang_AjaxUp(12)      = "准备插入的视频"
	
Dim Lang_AjaxUp_Cue(30)
    Lang_AjaxUp_Cue(0)   = ""
	Lang_AjaxUp_Cue(1)   = "你的系统不能本地上传文件<br>原因: 创建文件夹失败<br>路径: "
	Lang_AjaxUp_Cue(2)   = "允许上传的图片格式: jpg、jpeg、jpe、png、gif、bmp"
	Lang_AjaxUp_Cue(3)   = "允许上传的文件格式: doc、txt、pdf、zip、rar"
	Lang_AjaxUp_Cue(4)   = "允许上传的flash格式: swf"
	Lang_AjaxUp_Cue(5)   = "允许上传的音乐格式: mp3、wmv"
	Lang_AjaxUp_Cue(6)   = "允许上传的视频格式: rm、rmvb、avi、wmv、mp4"
	Lang_AjaxUp_Cue(7)   = ""
	Lang_AjaxUp_Cue(8)   = "举例:http://www.knifecms.com.cn/images/logo.gif"
	Lang_AjaxUp_Cue(9)   = "举例:http://www.knifecms.com.cn/images/logo.doc"
	Lang_AjaxUp_Cue(10)  = "举例:http://www.knifecms.com.cn/images/logo.swf"
	Lang_AjaxUp_Cue(11)  = "举例:http://www.knifecms.com.cn/images/logo.mp3"
	Lang_AjaxUp_Cue(12)  = "举例:http://www.knifecms.com.cn/images/logo.rmvb"
	Lang_AjaxUp_Cue(13)  = ""

Dim Lang_FileManage(15)
    Lang_FileManage(0)  = "文件管理"
	Lang_FileManage(1)  = "预览"
	Lang_FileManage(2)  = "上传"
	Lang_FileManage(3)  = "重名名"
	Lang_FileManage(4)  = "冗余文件检测"
	Lang_FileManage(5)  = "删除"
	Lang_FileManage(6)  = "图片管理"
	Lang_FileManage(7)  = "普通文件管理"
	Lang_FileManage(8)  = "Flash文件管理"
	Lang_FileManage(9)  = "音乐文件管理"
	Lang_FileManage(10) = "视频文件管理"
	Lang_FileManage(11) = "正在检测冗余文件，请稍候 ..."
	Lang_FileManage(12) = ""
	Lang_FileManage(13) = ""

%>
