<%
'=========================================================
' File     : shipbill.lang.asp
' 功能注释 : 系统后台语言文档(收货单)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
	
Dim Lang_ShipBill_ListTableColLine(15)
    Lang_ShipBill_ListTableColLine(0)  = ""
	Lang_ShipBill_ListTableColLine(1)  = "发货单号"
	Lang_ShipBill_ListTableColLine(2)  = "订单号"
	Lang_ShipBill_ListTableColLine(3)  = "物流费用"
	Lang_ShipBill_ListTableColLine(4)  = "是否保价"
	Lang_ShipBill_ListTableColLine(5)  = "收货人"
	Lang_ShipBill_ListTableColLine(6)  = "配送方式"
	Lang_ShipBill_ListTableColLine(7)  = "物流公司"
	Lang_ShipBill_ListTableColLine(8)  = "物流单号"
	Lang_ShipBill_ListTableColLine(9)  = "会员用户名"
	Lang_ShipBill_ListTableColLine(10) = "单据创建时间"
	Lang_ShipBill_ListTableColLine(11) = "操作员"
	Lang_ShipBill_ListTableColLine(12) = "商品编号"
	Lang_ShipBill_ListTableColLine(13) = "商品名称"
	Lang_ShipBill_ListTableColLine(14) = "发货数量"
	
Dim Lang_ShipBill(20)
    Lang_ShipBill(0)   = "发货单"
	Lang_ShipBill(1)   = "发货单信息"
	Lang_ShipBill(2)   = "发货单号"
	Lang_ShipBill(3)   = "订单号"
	Lang_ShipBill(4)   = "配送方式"
	Lang_ShipBill(5)   = "物流公司"
	Lang_ShipBill(6)   = "物流费用"
	Lang_ShipBill(7)   = "物流单号"
	Lang_ShipBill(8)   = "实际保价"
	Lang_ShipBill(9)   = "保价费用"
	Lang_ShipBill(10)  = "发货人姓名"
	Lang_ShipBill(11)  = "发货人Email"
	Lang_ShipBill(12)  = "联系手机"
	Lang_ShipBill(13)  = "联系电话"
	Lang_ShipBill(14)  = "发货地区"
	Lang_ShipBill(15)  = "邮政编码"
	Lang_ShipBill(16)  = "详细地址"
	Lang_ShipBill(17)  = "退货原因"
	Lang_ShipBill(18)  = "备注"
	Lang_ShipBill(19)  = ""
	
Dim Lang_ShipBill_Cue(10)
    Lang_ShipBill_Cue(0)   = ""
	Lang_ShipBill_Cue(1)   = "成功添加新发货单."
	Lang_ShipBill_Cue(2)   = "成功编辑发货单."
	Lang_ShipBill_Cue(3)   = "成功删除发货单."
	Lang_ShipBill_Cue(4)   = "成功彻底删除发货单."
	Lang_ShipBill_Cue(5)   = "成功还原发货单."
	Lang_ShipBill_Cue(6)   = ""
	Lang_ShipBill_Cue(7)   = "发货单不存在."
	Lang_ShipBill_Cue(8)   = "名称不能为空."

%>
