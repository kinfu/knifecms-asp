<%
'=========================================================
' File     : common.main.lang.asp
' 功能注释 : 系统前台后台共用语言文档
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
Const Lang_CurrentLocation   = "您当前的位置："
Const Lang_Client_Home       = "首页"
Const Lang_Client_Guestbook  = "在线留言"
Const Lang_Client_ShopCart   = "购物车"
Const Lang_Client_Order      = "订单"
Const Lang_Client_Shop       = "产品商城"
Const Lang_Client_Vote       = "投票系统"
Const Lang_Client_Search     = "搜索"


Dim Lang_Pages(5)
    Lang_Pages(0) = "共{tpl:num}条记录"
	Lang_Pages(1) = "上一页"
	Lang_Pages(2) = "下一页"
	Lang_Pages(3) = "共{tpl:num}页"
	Lang_Pages(4) = "{tpl:num}条/页"
	
Dim Lang_FieldType(11)
	Lang_FieldType(0)  = "字段类型"
	Lang_FieldType(1)  = "单行文本"
	Lang_FieldType(2)  = "多行文本"
	Lang_FieldType(3)  = "编辑器"
	Lang_FieldType(4)  = "单选按钮"
	Lang_FieldType(5)  = "复选框"
	Lang_FieldType(6)  = "下拉选择"
	Lang_FieldType(7)  = "数字"
	Lang_FieldType(8)  = "日期时间"
	Lang_FieldType(9)  = "图片"
	Lang_FieldType(10) = "附件"





%>
