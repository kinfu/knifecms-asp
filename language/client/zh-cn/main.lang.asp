<%
'=========================================================
' File     : main.lang.asp
' 功能注释 : 系统前台语言文档(共用)
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
Const Lang_PaymentNotExist     = "支付方式不存在."
Const Lang_OrderIDNotExist     = "订单不存在或已被删除."
Const Lang_OrderIDIsError      = "订单号不正确."
Const Lang_OrderPayedSuccess   = "订单支付成功."
Const Lang_OrderPayedFail      = "订单支付失败."
Const Lang_PayTradeLogID       = "交易单号"
Const Lang_OrderID             = "订单号"
Const Lang_ViewMyOrder         = "查看我的订单"
Const Lang_NeedPoundage        = "需要支付手续费："
Const Lang_VerifyFail          = "验证失败，请不要提交非法数据！"
Const Lang_PayTradeLogNotExist = "支付交易记录不存在或已被删除！"

Const Lang_Confirm             = "确认"

Dim Lang_Guestbook   : Lang_Guestbook   = "留言"
Dim Lang_Guest       : Lang_Guest       = "游客"
Dim Lang_Reply       : Lang_Reply       = "回复"
Dim Lang_ReplyIn     : Lang_ReplyIn     = "回复于"
Dim Lang_ShrinkReply : Lang_ShrinkReply = "收起回复"
Dim Lang_Repay       : Lang_Repay       = "转播"
Dim Lang_Admin       : Lang_Admin       = "管理员"

Dim Lang_Bank(10)
    Lang_Bank(1)   = "支付宝"
	Lang_Bank(2)   = "中国工商银行"
	Lang_Bank(3)   = "中国交通银行"

Dim Lang_Cart(21)
    Lang_Cart(10)  = "生成错误的订单ID."

Dim Lang_OrderLogBehavior(5)
    Lang_OrderLogBehavior(0) = "添加"
	Lang_OrderLogBehavior(1) = "付款"
	Lang_OrderLogBehavior(2) = "退款"
	Lang_OrderLogBehavior(3) = "发货"
	Lang_OrderLogBehavior(4) = "退货"
	
Dim Lang_OrderLogText(5)
    Lang_OrderLogText(0) = "创建订单"
	Lang_OrderLogText(1) = "订单支付"
	Lang_OrderLogText(2) = "订单退款"
	Lang_OrderLogText(3) = "订单发货"
	Lang_OrderLogText(4) = "订单退货"
	
Dim Lang_Search(10)
    Lang_Search(1)   = "请输入关键词进行搜索"
	Lang_Search(2)   = "在<span class=""subsystem"">{tpl:subsystem}</span>中总共找到<b class=""search_count"">{tpl:search_count}</b>条与<span class=""keywords"">""<font>{tpl:keywords}</font>""</span>相关的搜索结果"
	Lang_Search(3)   = "未搜索到相关内容"
	Lang_Search(4)   = "商品编号："
	Lang_Search(5)   = "{tpl:keywords}的搜索结果"
	
Dim Lang_User(10)
	Lang_User(0)  = "用户"
	Lang_User(1)  = "用户不存在"
	Lang_User(2)  = "用户已被禁用"
	Lang_User(3)  = "用户已被删入回收站"
	Lang_User(4)  = "此找回密码链接已失效或已过期"
	

%>
