<%
'=========================================================
' File     : common.main.lang.asp
' 功能注释 : 系统前台后台共用语言文档
' Version  : 1.x
' Authority: http://www.knifecms.com
' Write By : Smith
'=========================================================
Const Lang_CurrentLocation   = "Your position:"
Const Lang_Client_Home       = "Index"
Const Lang_Client_Guestbook  = "Guestbook"
Const Lang_Client_ShopCart   = "Cart"
Const Lang_Client_Order      = "Order"
Const Lang_Client_Shop       = "Mall"
Const Lang_Client_Vote       = "Vote"
Const Lang_Client_Search     = "Search"


Dim Lang_Pages(5)
    Lang_Pages(0) = "total:{tpl:num}"
	Lang_Pages(1) = "prev page"
	Lang_Pages(2) = "next page"
	Lang_Pages(3) = "total {tpl:num}"
	Lang_Pages(4) = "{tpl:num} /p"
	
Dim Lang_FieldType(11)
	Lang_FieldType(0)  = "字段类型"
	Lang_FieldType(1)  = "单行文本"
	Lang_FieldType(2)  = "多行文本"
	Lang_FieldType(3)  = "编辑器"
	Lang_FieldType(4)  = "单选按钮"
	Lang_FieldType(5)  = "复选框"
	Lang_FieldType(6)  = "下拉选择"
	Lang_FieldType(7)  = "数字"
	Lang_FieldType(8)  = "日期时间"
	Lang_FieldType(9)  = "图片"
	Lang_FieldType(10) = "附件"





%>
