<%
Function ParseRewritePule()
	'http://www.knifecms.com/?content/news/index.htm
	KnifeCMS.RewriteRule "\?content/(\w+)/([\s\S]*?)"& FileSuffix,"ctl=content&subsys=$1"
	
	'http://www.knifecms.com/?content/news/index_page2.htm
	KnifeCMS.RewriteRule "\?content/(\w+)/([\s\S]*?)_page(\d+)"& FileSuffix,"ctl=content&subsys=$1&page=$3"
	
	'http://www.knifecms.com/?content/news/345/seourl.htm
	KnifeCMS.RewriteRule "\?content/(\w+)/(\d+)/([\s\S]*?)"& FileSuffix,"ctl=content&subsys=$1&id=$2"
	
	'http://www.knifecms.com/?content/news/345_2/seourl.htm
	KnifeCMS.RewriteRule "\?content/(\w+)/(\d+)_(\d+)/([\s\S]*?)"& FileSuffix,"ctl=content&subsys=$1&id=$2&page=$3"    
	  
	'http://www.knifecms.com/?content/news/category/10/seourl.htm
	KnifeCMS.RewriteRule "\?content/(\w+)/category/(\d+)/([\s\S]*?)"& FileSuffix,"ctl=content&subsys=$1&category=true&id=$2"   
	
	'http://www.knifecms.com/?content/news/category/10_2/seourl.htm
	KnifeCMS.RewriteRule "\?content/(\w+)/category/(\d+)_(\d+)/([\s\S]*?)"& FileSuffix,"ctl=content&subsys=$1&category=true&id=$2&page=$3"
	
	
	'http://www.knifecms.com/?vote.htm
	KnifeCMS.RewriteRule "\?vote"& FileSuffix,"ctl=vote"  
			 
	'http://www.knifecms.com/?vote_page2.htm            
	KnifeCMS.RewriteRule "\?vote_page(\d+)"& FileSuffix,"ctl=vote&page=$1"
	
	'http://www.knifecms.com/?vote/345/seourl.htm
	KnifeCMS.RewriteRule "\?vote/(\d+)/([\s\S]*?)"& FileSuffix,"ctl=vote&id=$1"
	 
	'http://www.knifecms.com/?vote/category/10/seourl.htm
	KnifeCMS.RewriteRule "\?vote/category/(\d+)/([\s\S]*?)"& FileSuffix,"ctl=vote&category=true&id=$1"  
	
	'http://www.knifecms.com/?vote/category/10_2/seourl.htm
	KnifeCMS.RewriteRule "\?vote/category/(\d+)_(\d+)/([\s\S]*?)"& FileSuffix,"ctl=vote&category=true&id=$1&page=$2"
	
	
	'http://www.knifecms.com/?shop.htm
	KnifeCMS.RewriteRule "\?shop"& FileSuffix,"ctl=shop"
	
	'http://www.knifecms.com/?shop.htm
	KnifeCMS.RewriteRule "\?shop_page(\d+)"& FileSuffix,"ctl=shop&page=$1"
	
	'http://www.knifecms.com/?shop/33/seourl.htm
	KnifeCMS.RewriteRule "\?shop/(\d+)/([\s\S]*?)"& FileSuffix,"ctl=shop&id=$1"
	
	'http://www.knifecms.com/?shop/category/10/seourl.htm
	KnifeCMS.RewriteRule "\?shop/category/(\d+)/([\s\S]*?)"& FileSuffix,"ctl=shop&category=true&id=$1"  
	
	'http://www.knifecms.com/?shop/category/10_2/seourl.htm
	KnifeCMS.RewriteRule "\?shop/category/(\d+)_(\d+)/([\s\S]*?)"& FileSuffix,"ctl=shop&category=true&id=$1&page=$2"
	
	
	'http://www.knifecms.com/?mycart.htm
	KnifeCMS.RewriteRule "\?mycart"& FileSuffix,"ctl=mycart"
	
	'http://www.knifecms.com/?mycart/checkout.htm
	KnifeCMS.RewriteRule "\?mycart/(\w+)"& FileSuffix,"ctl=mycart&action=$1"
	
	'http://www.knifecms.com/?mycart/suc/2011022800535997.htm
	KnifeCMS.RewriteRule "\?mycart/(\w+)/(\d+)"& FileSuffix,"ctl=mycart&action=$1&orderid=$2"
	
	'http://www.knifecms.com/?order/payresult/2011022800535997.htm
	KnifeCMS.RewriteRule "\?order/(\w+)/(\d+)"& FileSuffix,"ctl=order&action=$1&orderid=$2"
	
	'http://www.knifecms.com/?login.htm
	KnifeCMS.RewriteRule "\?login"& FileSuffix,"ctl=login"
	
	'http://www.knifecms.com/?login/out.htm
	KnifeCMS.RewriteRule "\?login/out"& FileSuffix,"ctl=login&action=out"
	
	'http://www.knifecms.com/?login/result.htm
	KnifeCMS.RewriteRule "\?login/result"& FileSuffix,"ctl=login&action=result"
	
	'http://www.knifecms.com/?reg.htm
	KnifeCMS.RewriteRule "\?reg"& FileSuffix,"ctl=reg"
	
	'http://www.knifecms.com/?reg/result.htm
	KnifeCMS.RewriteRule "\?reg/result"& FileSuffix,"ctl=reg&action=result"
	
	'http://www.knifecms.com/?myhome.htm
	KnifeCMS.RewriteRule "\?myhome"& FileSuffix,"ctl=myhome"
	
	'http://www.knifecms.com/?myhome/home.htm
	KnifeCMS.RewriteRule "\?myhome/(\w+)"& FileSuffix,"ctl=myhome&subsys=$1"
	
	'http://www.knifecms.com/?user/2524.htm
	KnifeCMS.RewriteRule "\?user/(\d+)"& FileSuffix,"ctl=userhome&userid=$1"
	
	'http://www.knifecms.com/?user/2524/mood.htm
	KnifeCMS.RewriteRule "\?user/(\d+)/(\w+)"& FileSuffix,"ctl=userhome&userid=$1&subsys=$2"
	
	'http://www.knifecms.com/?guestbook.htm
	KnifeCMS.RewriteRule "\?guestbook"& FileSuffix,"ctl=guestbook"
	
	'http://www.knifecms.com/?error404.htm
	KnifeCMS.RewriteRule "\?error404"& FileSuffix,"ctl=error404"
	
	KnifeCMS.ParseRewrite
End Function
Call ParseRewritePule()
%>