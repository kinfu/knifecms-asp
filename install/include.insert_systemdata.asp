<%
'企业介绍
Print "正在安装内容系统[企业介绍]数据......"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"ContentSubSystem] ([SysID],[SysDefault],[System],[SystemName],[CreateTime],[Orders],[Template],[CategoryTemplate],[ContentTemplate],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription]) VALUES ('110320-000000-00000-00000-000001',0,'about','企业介绍','"& Fn_NowTime &"',0,'content.about.htm','content.about.category.htm','content.about.detail.htm','企业介绍','index','','')"

Pv_TempHTML = "<table cellspacing=""0"" cellpadding=""0"" border=""0""><tbody><tr><td valign=""top""><a href="""& Pv_SystemPath &"?content/about/category/1/index.htm""><img width=""145"" vspace=""0"" hspace=""0"" height=""200"" border=""0"" src="""& Pv_SystemPath &"images/pub/about.jpg"" style=""background:none repeat scroll 0% 0% #f6f6f6;border:1px solid #cccccc;padding:4px;float:none;width:145px;height:200px;""></a></td><td valign=""top"" style=""padding-left:12px;""><p><strong><span style=""font-size:14px;"">KnifeCMS简介 <span style=""font-size:14px;color:#7f7f7f;"">KnifeCMS Introduction</span></span></strong></p><p>KnifeCMS全称开放式商务建站系统，是一种完全开源免费的全新互联网建站系统。</p><p>KnifeCMS功能模块非常丰富，可随意组合出个性化的网站，用户在短时间内即可迅速架设属于自己的企业网站、电子商务网站、外贸网站、资讯门户和博客。</p><p>KnifeCMS内核健壮，程序和数据库采用严谨的程序数据优化算法，保证系统完全稳定运行；同时系统独创全新的SEO深入优化方案，即使用户不懂SEO，系统也会自动进行优化，方便用户快速有效地推广网站。</p><p>KnifeCMS易学易懂，采用应用式模块化管理和专用模板机制，使用简易的模板标签方式，完全实现可视化的编辑，真正做到所见即所得...<br></p></td></tr></tbody></table>"
Pv_TempHTML = KnifeCMS.Data.HTMLEncode(Pv_TempHTML)
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_about_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('公司介绍',1,0,1,0,0,1,'公司介绍','index','','','"& Pv_TempHTML &"',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_about_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('公司文化',1,0,2,0,0,1,'公司文化','index','','','这里填写公司文化内容',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_about_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('管理团队',1,0,3,0,0,1,'管理团队','index','','','这里填写管理团队内容',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_about_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('公司历史',1,0,4,0,0,1,'公司历史','index','','','这里填写公司历史内容',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_about_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('公司资质',1,0,5,0,0,1,'公司资质','index','','','这里填写公司资质内容',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_about_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('合作伙伴',1,0,6,0,0,1,'合作伙伴','index','','','这里填写合作伙伴内容',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_about_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('招聘信息',1,0,7,0,0,1,'招聘信息','index','','','这里填写招聘信息内容',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_about_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('联系我们',1,0,8,0,0,1,'联系我们','index','','','KnifeCMS群:374069166，欢迎加入！',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_about_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('法律公告',1,0,9,0,0,1,'法律公告','index','','','这里填写法律公告内容',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_about_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('隐私保护',1,0,10,0,0,1,'隐私保护','index','','','这里填写隐私保护内容',0)"
Print "完成<br>"

'新闻中心
Print "正在安装内容系统[新闻中心]数据......"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"ContentSubSystem] ([SysID],[SysDefault],[System],[SystemName],[CreateTime],[Orders],[Template],[CategoryTemplate],[ContentTemplate],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription]) VALUES ('110320-000000-00000-00000-000002',0,'news','新闻中心','"& Fn_NowTime &"',2,'content.news.htm','content.news.category.htm','content.news.detail.htm','新闻中心','index','','')"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_news_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('公司新闻',1,0,1,2,0,0,'公司新闻','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_news_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('公司动态',2,1,1,0,0,0,'公司动态','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_news_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('媒体报道',2,1,2,0,0,0,'媒体报道','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_news_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('行业新闻',1,0,2,2,0,0,'行业新闻','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_news_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('行业动态',2,4,1,0,0,0,'行业动态','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_news_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('技术资讯',2,4,2,0,0,0,'技术资讯','index','','',0)"
Print "完成<br>"

'产品中心
Print "正在安装内容系统[产品中心]数据......"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"ContentSubSystem] ([SysID],[SysDefault],[System],[SystemName],[CreateTime],[Orders],[Template],[CategoryTemplate],[ContentTemplate],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription]) VALUES ('110320-000000-00000-00000-000003',0,'product','产品中心','"& Fn_NowTime &"',3,'content.product.htm','content.product.category.htm','content.product.detail.htm','产品中心','index','','')"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_product_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('数码家电系列',1,0,1,0,0,0,'数码家电系列','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_product_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('家居建材系列',1,0,2,0,0,0,'家居建材系列','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_product_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('汽车配件系列',1,0,3,0,0,0,'汽车配件系列','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_product_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('水果食品系列',1,0,4,0,0,0,'水果食品系列','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_product_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('母婴用品系列',1,0,5,0,0,0,'母婴用品系列','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_product_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('其他系列',1,0,6,0,0,0,'其他系列','index','','',0)"
Print "完成<br>"

Call KnifeCMS.DB.AddRecord(Pv_TablePre &"FieldSet",Array("Field:c_price","FieldName:参考价格","FieldType:text","System:product","NotNull:0","FieldOptions:","FieldTips:","OrderNum:1","Recycle:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"FieldSet",Array("Field:c_markettime","FieldName:上市时间","FieldType:text","System:product","NotNull:0","FieldOptions:","FieldTips:","OrderNum:2","Recycle:0"))
'增加产品字段
Select Case Pv_DB_Type
Case 0
	Call KnifeCMS.DB.AddColumn(Pv_TablePre &"Content_product","c_price text (250) NULL")
	Call KnifeCMS.DB.AddColumn(Pv_TablePre &"Content_product","c_markettime text (250) NULL")
Case 1
	Call KnifeCMS.DB.AddColumn(Pv_TablePre &"Content_product","c_price [nvarchar] (250) NULL")
	Call KnifeCMS.DB.AddColumn(Pv_TablePre &"Content_product","c_markettime [nvarchar] (250) NULL")
End Select

Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Content_product",Array("SysID:000000-000000-00000-00000-000001","ClassID:1","Title:体验产品","PublicTime:"& Fn_NowTime,"UseLinkUrl:0","Thumbnail:"& Pv_SystemPath &"images/pic/product_1_s.jpg","SlidesFirstImg:"& Pv_SystemPath &"images/pic/product_1.jpg","SlidesImgs:"& Pv_SystemPath &"images/pic/product_1.jpg,"& Pv_SystemPath &"images/pic/product_1_2.jpg,"& Pv_SystemPath &"images/pic/product_1_3.jpg","Abstract:这里是产品简介","Content:这里是产品详细信息","SeoTitle:体验产品","SeoUrl:test_product","MetaKeywords:体验产品,KnifeCMS,产品","MetaDescription:这是一个体验产品页面","Hits:0","Comments:0","Diggs:0","TopLine:0","OnTop:0","Hot:0","Elite:0","CommentSet:0","Openness:0","Status:1","Recycle:0","AddTime:"& Fn_NowTime,"c_price:29800.00","c_markettime:2012年6月8日"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Content_product",Array("SysID:000000-000000-00000-00000-000002","ClassID:2","Title:体验产品2","PublicTime:"& Fn_NowTime,"UseLinkUrl:0","Thumbnail:"& Pv_SystemPath &"images/pic/product_2_s.jpg","SlidesFirstImg:"& Pv_SystemPath &"images/pic/product_2.jpg","SlidesImgs:"& Pv_SystemPath &"images/pic/product_2.jpg,"& Pv_SystemPath &"images/pic/product_2_2.jpg,"& Pv_SystemPath &"images/pic/product_2_3.jpg","Abstract:这里是产品简介","Content:这里是产品详细信息","SeoTitle:体验产品","SeoUrl:test_product","MetaKeywords:体验产品,KnifeCMS,产品","MetaDescription:这是一个体验产品页面","Hits:0","Comments:0","Diggs:0","TopLine:0","OnTop:0","Hot:0","Elite:0","CommentSet:0","Openness:0","Status:1","Recycle:0","AddTime:"& Fn_NowTime,"c_price:12800.00","c_markettime:2012年2月15日"))


'案例展示
Print "正在安装内容系统[案例展示]数据......"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"ContentSubSystem] ([SysID],[SysDefault],[System],[SystemName],[CreateTime],[Orders],[Template],[CategoryTemplate],[ContentTemplate],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription]) VALUES ('110320-000000-00000-00000-000004',0,'case','案例展示','"& Fn_NowTime &"',4,'content.case.htm','content.case.category.htm','content.case.detail.htm','案例展示','index','','')"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_case_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('集团/上市公司',1,0,1,0,0,0,'集团/上市公司','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_case_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('房地产/商业中心',1,0,2,0,0,0,'房地产/商业中心','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_case_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('电子商务/网店',1,0,3,0,0,0,'电子商务/网店','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_case_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('家电/电子/科技',1,0,4,0,0,0,'家电/电子/科技','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_case_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('医疗/制药',1,0,5,0,0,0,'医疗/制药','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_case_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('酒店/餐饮/食品',1,0,6,0,0,0,'酒店/餐饮/食品','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_case_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('教育培训/服务',1,0,7,0,0,0,'教育培训/服务','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_case_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('其它领域',1,0,8,0,0,0,'其它领域','index','','',0)"
Print "完成<br>"

Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Content_case",Array("SysID:000000-000000-00000-00000-000001","ClassID:1","Title:体验案例展示","PublicTime:"& Fn_NowTime,"UseLinkUrl:0","Thumbnail:"& Pv_SystemPath &"images/pic/case_1_s.jpg","SlidesFirstImg:"& Pv_SystemPath &"images/pic/case_1.jpg","SlidesImgs:"& Pv_SystemPath &"images/pic/case_1.jpg,"& Pv_SystemPath &"images/pic/case_1_2.jpg,"& Pv_SystemPath &"images/pic/case_1_3.jpg","Abstract:这里是案例简介","Content:这里是案例详细信息","SeoTitle:体验案例展示","SeoUrl:test_product","MetaKeywords:体验案例展示,KnifeCMS,案例","MetaDescription:这是一个案例展示页面","Hits:0","Comments:0","Diggs:0","TopLine:0","OnTop:0","Hot:0","Elite:0","CommentSet:0","Openness:0","Status:1","Recycle:0","AddTime:"& Fn_NowTime))

'帮助中心
Print "正在安装内容系统[帮助中心]数据......"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"ContentSubSystem] ([SysID],[SysDefault],[System],[SystemName],[CreateTime],[Orders],[Template],[CategoryTemplate],[ContentTemplate],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription]) VALUES ('110320-000000-00000-00000-000005',0,'help','帮助中心','"& Fn_NowTime &"',5,'content.help.htm','content.help.category.htm','content.help.detail.htm','帮助中心','index','','')"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_help_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('客户服务',1,0,1,2,0,1,'客户服务','index','','','客户服务相关内容',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_help_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('售后服务',2,1,1,0,0,1,'售后服务','index','','','售后服务相关内容',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_help_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('技术支持',2,1,2,0,0,1,'技术支持','index','','','技术支持相关内容',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_help_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('常见问题',1,0,2,2,0,1,'常见问题','index','','','常见问题相关内容',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_help_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('购物指南',2,4,1,0,0,1,'购物指南','index','','','购物指南相关内容',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Content_help_Class] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Remarks],[Recycle]) VALUES ('支付与配送',2,4,2,0,0,1,'支付与配送','index','','','支付与配送相关内容',0)"
Print "完成<br>"

Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Content_help",Array("SysID:000000-000000-00000-00000-000001","ClassID:1","Title:常见问题解答","PublicTime:"& Fn_NowTime,"UseLinkUrl:0","Thumbnail:","SlidesFirstImg:","SlidesImgs:","Abstract:常见问题解答简介","Content:这里是常见问题解答详细信息","SeoTitle:常见问题解答","SeoUrl:test_product","MetaKeywords:常见问题解答,KnifeCMS,常见问题","MetaDescription:常见问题解答简介","Hits:0","Comments:0","Diggs:0","TopLine:0","OnTop:1","Hot:0","Elite:0","CommentSet:0","Openness:0","Status:1","Recycle:0","AddTime:"& Fn_NowTime))

'投票中心
Print "正在安装投票中心数据......"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"VoteClass] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('默认分类',1,0,1,0,0,0,'默认分类','index','','',0)"
Print "完成<br>"

'系统用户/会员
Print "正在安装系统用户数据......"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"SysUserGroup] ([SysID],[Founder],[GroupName],[Degree],[Remarks]) VALUES ('100000-000000-00000-00000-000001',1,'网站站长',99999,'权限最高的系统用户组')"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"SysUserGroup] ([SysID],[Founder],[GroupName],[Degree],[Remarks]) VALUES ('100000-000000-00000-00000-000002',0,'超级管理员',9999,'')"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"SysUserGroup] ([SysID],[Founder],[GroupName],[Degree],[Remarks]) VALUES ('100000-000000-00000-00000-000003',0,'普通管理员',999,'')"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"SysUser] ([SysID],[Username],[Password],[TruePassword],[Email],[GroupID],[MemberID],[UserDisable],[LoginTimes],[RegisterTime],[LastLoginTime],[Remarks],[Recycle]) VALUES ('110320-000000-00000-00000-000001','"& Fn_Sysuser_Username &"','"& Fn_Sysuser_Password &"','','"& Fn_Sysuser_Email &"',1,1,0,0,'"& Fn_NowTime &"','"& Fn_NowTime &"','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Members] ([Username],[Password],[TruePassword],[Email],[BigHead],[HeadImg],[SysUserID],[Grade],[Points],[RegTime],[LastLoginTime],[Remarks],[Disable],[Recycle]) VALUES ('"& Fn_Sysuser_Username &"','"& Fn_Sysuser_Password &"','','"& Fn_Sysuser_Email &"','','',1,0,0,'"& Fn_NowTime &"','"& Fn_NowTime &"','',0,0)"
Print "完成<br>"

'**商品形式**
Print "正在安装网店商品系统数据......"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsType] ([TypeKey],[TypeName],[OrderNum]) VALUES (1,'商品(直接销售型)',1)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsType] ([TypeKey],[TypeName],[OrderNum]) VALUES (2,'配件(非直接销售型)',2)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsType] ([TypeKey],[TypeName],[OrderNum]) VALUES (3,'赠品',3)"

'**商品分类**
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsClass] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('服装/内衣/配件',1,0,1,2,0,0,'服装/内衣/配件','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsClass] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('品牌女装',2,1,1,3,0,0,'品牌女装','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsClass] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('连衣裙',3,2,1,0,0,0,'连衣裙','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsClass] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('针织衫',3,2,2,0,0,0,'针织衫','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsClass] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('婚纱/礼服/旗袍',3,2,3,0,0,0,'婚纱/礼服/旗袍','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsClass] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('精品男装',2,1,2,0,0,0,'精品男装','index','','',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsClass] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('珠宝饰品/手表眼镜',1,0,2,2,0,0,'珠宝饰品/手表眼镜','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsClass] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('时尚饰品',2,7,1,0,0,0,'时尚饰品','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsClass] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('男士饰品',2,7,2,0,0,0,'男士饰品','index','','',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsClass] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('手机数码/电器',1,0,3,0,0,0,'手机数码/电器','index','','',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"GoodsClass] ([ClassName],[ClassDepth],[ParentClassID],[ClassOrder],[ClassChildNum],[ClassHide],[ClassType],[SeoTitle],[SeoUrl],[MetaKeywords],[MetaDescription],[Recycle]) VALUES ('家居/家具建材',1,0,4,0,0,0,'家居/家具建材','index','','',0)"


'**商品类型**
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsModel",Array("SysID:000000-000000-00000-00000-000001","ModelName:服装类","OrderNum:1","AddTime:"& Fn_NowTime))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsModelAttribute",Array("SysID:000000-000000-00000-00000-000001","GoodsModelID:1","AttrName:颜色","AttrOrder:1","IsLinked:1","AttrType:1","AttrInputType:1","AttrValues:白色\n红色\n粉红色\n银白色\n浅黄色\n棕色\n黑色","AddTime:"& Fn_NowTime))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsModelAttribute",Array("SysID:000000-000000-00000-00000-000002","GoodsModelID:1","AttrName:尺码","AttrOrder:1","IsLinked:1","AttrType:1","AttrInputType:1","AttrValues:S\nM\nL\nXL\nXXL\nXXXL","AddTime:"& Fn_NowTime))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsModelAttribute",Array("SysID:000000-000000-00000-00000-000003","GoodsModelID:1","AttrName:附加服务","AttrOrder:1","IsLinked:1","AttrType:2","AttrInputType:1","AttrValues:加花\n加精美包装\n加贺卡","AddTime:"& Fn_NowTime))

'**商品**
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Goods",Array("SysID:000000-000000-00000-00000-000001","GoodsType:1","ClassID:1","GoodsName:体验商品(带属性)","GoodsBN:G86CE8DB3A982B6E1","GoodsSN:S000001","Marketable:1","Price:100","Cost:50","MktPrice:150","Weight:500","Store:1000","Unit:件","BrandID:1","Thumbnail:"& Pv_SystemPath &"images/pic/goods_1_s.jpg","FirstImg:"& Pv_SystemPath &"images/pic/goods_1.jpg","Imgs:"& Pv_SystemPath &"images/pic/goods_1.jpg,"& Pv_SystemPath &"images/pic/goods_1_2.jpg,"& Pv_SystemPath &"images/pic/goods_1_3.jpg","Abstract:这里是商品简介","Intro:这里是商品详细信息","GoodsModel:1","SeoTitle:体验商品","SeoUrl:test_goods","MetaKeywords:体验商品,KnifeCMS,商品","MetaDescription:这是一个体验商品页面","Hits:1","Diggs:0","Recommend:0","Hot:0","New:0","HomepageRecommend:1","Recycle:0","AddTime:"& Fn_NowTime))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Goods",Array("SysID:000000-000000-00000-00000-000002","GoodsType:1","ClassID:10","GoodsName:体验商品2","GoodsBN:G86CE8DB3A982B6E2","GoodsSN:S000001","Marketable:1","Price:1200","Cost:800","MktPrice:1500","Weight:4500","Store:100","Unit:台","BrandID:0","Thumbnail:"& Pv_SystemPath &"images/pic/goods_2_s.jpg","FirstImg:"& Pv_SystemPath &"images/pic/goods_2.jpg","Imgs:"& Pv_SystemPath &"images/pic/goods_2.jpg,"& Pv_SystemPath &"images/pic/goods_2_2.jpg,"& Pv_SystemPath &"images/pic/goods_2_3.jpg","Abstract:这里是商品简介","Intro:这里是商品详细信息","GoodsModel:0","SeoTitle:体验商品2","SeoUrl:test_goods","MetaKeywords:体验商品,KnifeCMS,商品","MetaDescription:这是一个体验商品页面","Hits:1","Diggs:0","Recommend:0","Hot:0","New:0","HomepageRecommend:1","Recycle:0","AddTime:"& Fn_NowTime))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Goods",Array("SysID:000000-000000-00000-00000-000003","GoodsType:1","ClassID:10","GoodsName:体验商品3","GoodsBN:G86CE8DB3A982B6E3","GoodsSN:S000001","Marketable:1","Price:3500","Cost:3000","MktPrice:3880","Weight:3000","Store:200","Unit:台","BrandID:0","Thumbnail:"& Pv_SystemPath &"images/pic/goods_3_s.jpg","FirstImg:"& Pv_SystemPath &"images/pic/goods_3.jpg","Imgs:"& Pv_SystemPath &"images/pic/goods_3.jpg,"& Pv_SystemPath &"images/pic/goods_3_2.jpg,"& Pv_SystemPath &"images/pic/goods_3_3.jpg","Abstract:这里是商品简介","Intro:这里是商品详细信息","GoodsModel:0","SeoTitle:体验商品3","SeoUrl:test_goods","MetaKeywords:体验商品,KnifeCMS,商品","MetaDescription:这是一个体验商品页面","Hits:1","Diggs:0","Recommend:1","Hot:0","New:0","HomepageRecommend:0","Recycle:0","AddTime:"& Fn_NowTime))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Goods",Array("SysID:000000-000000-00000-00000-000004","GoodsType:1","ClassID:10","GoodsName:体验商品4","GoodsBN:G86CE8DB3A982B6E4","GoodsSN:S000001","Marketable:1","Price:4000","Cost:3200","MktPrice:4500","Weight:3000","Store:200","Unit:台","BrandID:0","Thumbnail:"& Pv_SystemPath &"images/pic/goods_4_s.jpg","FirstImg:"& Pv_SystemPath &"images/pic/goods_4.jpg","Imgs:"& Pv_SystemPath &"images/pic/goods_4.jpg,"& Pv_SystemPath &"images/pic/goods_4_2.jpg,"& Pv_SystemPath &"images/pic/goods_4_3.jpg","Abstract:这里是商品简介","Intro:这里是商品详细信息","GoodsModel:0","SeoTitle:体验商品4","SeoUrl:test_goods","MetaKeywords:体验商品,KnifeCMS,商品","MetaDescription:这是一个体验商品页面","Hits:1","Diggs:0","Recommend:0","Hot:0","New:0","HomepageRecommend:0","Recycle:0","AddTime:"& Fn_NowTime))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Goods",Array("SysID:000000-000000-00000-00000-000005","GoodsType:1","ClassID:11","GoodsName:体验商品5","GoodsBN:G86CE8DB3A982B6E5","GoodsSN:S000001","Marketable:1","Price:2000","Cost:1500","MktPrice:2500","Weight:30000","Store:50","Unit:件","BrandID:0","Thumbnail:"& Pv_SystemPath &"images/pic/goods_5_s.jpg","FirstImg:"& Pv_SystemPath &"images/pic/goods_5.jpg","Imgs:"& Pv_SystemPath &"images/pic/goods_5.jpg,"& Pv_SystemPath &"images/pic/goods_5_2.jpg,"& Pv_SystemPath &"images/pic/goods_5_3.jpg","Abstract:这里是商品简介","Intro:这里是商品详细信息","GoodsModel:0","SeoTitle:体验商品5","SeoUrl:test_goods","MetaKeywords:体验商品,KnifeCMS,商品","MetaDescription:这是一个体验商品页面","Hits:1","Diggs:0","Recommend:0","Hot:0","New:1","HomepageRecommend:0","Recycle:0","AddTime:"& Fn_NowTime))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Goods",Array("SysID:000000-000000-00000-00000-000006","GoodsType:1","ClassID:11","GoodsName:体验商品6","GoodsBN:G86CE8DB3A982B6E6","GoodsSN:S000001","Marketable:1","Price:200","Cost:80","MktPrice:300","Weight:4000","Store:500","Unit:张","BrandID:0","Thumbnail:"& Pv_SystemPath &"images/pic/goods_6_s.jpg","FirstImg:"& Pv_SystemPath &"images/pic/goods_6.jpg","Imgs:"& Pv_SystemPath &"images/pic/goods_6.jpg,"& Pv_SystemPath &"images/pic/goods_6_2.jpg,"& Pv_SystemPath &"images/pic/goods_6_3.jpg","Abstract:这里是商品简介","Intro:这里是商品详细信息","GoodsModel:0","SeoTitle:体验商品6","SeoUrl:test_goods","MetaKeywords:体验商品,KnifeCMS,商品","MetaDescription:这是一个体验商品页面","Hits:1","Diggs:0","Recommend:0","Hot:1","New:1","HomepageRecommend:0","Recycle:0","AddTime:"& Fn_NowTime))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:1","AttrValue:白色","AttrPrice:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:1","AttrValue:红色","AttrPrice:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:1","AttrValue:粉红色","AttrPrice:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:1","AttrValue:银白色","AttrPrice:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:1","AttrValue:浅黄色","AttrPrice:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:2","AttrValue:S","AttrPrice:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:2","AttrValue:M","AttrPrice:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:2","AttrValue:L","AttrPrice:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:2","AttrValue:XL","AttrPrice:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:2","AttrValue:XXL","AttrPrice:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:2","AttrValue:XXXL","AttrPrice:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:3","AttrValue:加花","AttrPrice:10"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:3","AttrValue:加精美包装","AttrPrice:10"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"GoodsAttr",Array("GoodsID:1","AttrID:3","AttrValue:加贺卡","AttrPrice:10"))

'**品牌**
Pv_TempHTML = "爱马仕（Hermes）是世界著名的奢侈品品牌，1837年由Thierry Hermes创立于法国巴黎，早年以制造高级马具起家，迄今已有170多年的悠久历史。"
Pv_TempHTML = KnifeCMS.Data.HTMLEncode(Pv_TempHTML)
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Brand",Array("SysID:000000-000000-00000-00000-000001","BrandName:爱马仕","BrandLogo:","BrandUrl:"& KnifeCMS.Data.HTMLEncode("http://www.hermes.com/"),"OrderNum:1","SeoTitle:爱马仕","SeoUrl:hermes","MetaKeywords:爱马仕","MetaDescription:爱马仕品牌详细","Abstract:"& Pv_TempHTML,"Intro:"&Pv_TempHTML,"Recycle:0"))


Print "完成<br>"

'**配送方式/支付方式**
Print "正在添加默认配送方式数据......"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Delivery] ([SysID],[DeliveryName],[Config],[Expressions],[Intro],[Price],[DeliveryType],[Gateway],[Protect],[ProtectRate],[ProtectMinPrice],[HasCod],[CorpID],[OrderNum],[Disabled]) VALUES ('100000-000000-00000-00000-000001','快递送货上门','a:7:{s:10:""FirstPrice"";s:1:""0"";s:9:""FirstUnit"";s:4:""1000"";s:13:""ContinuePrice"";s:1:""0"";s:12:""ContinueUnit"";s:4:""1000"";s:11:""Expressions"";s:19:""0+((w-1000)/1000)*0"";s:10:""AreaFeeSet"";s:1:""1"";s:19:""SetAreaDefaultFee"";s:1:""0"";}','0+((w-1000)/1000)*0','当地配送快递送货上门',0,1,1,0,0,0,1,0,0,0)"
Print "完成<br>"

Print "正在添加默认支付方式数据......"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Payments] ([PaymentName],[PayType],[Config],[PayMethod],[Fee],[PaymentDes],[OrderNum],[Disabled]) VALUES ('货到付款','cod','a:0:{s:9:""Partner"";s:0:"""";s:10:""PrivateKey"";s:0:"""";s:11:""InterFaceType"";s:1:""0"";}',2,0,'快递送货上门收款，请在收货时将款项交给快递员',0,0)"
Print "完成<br>"


'****** 站点导航 ******
Print "正在安装网站配置数据......"
'**顶部导航**
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (1,0,1,'客户服务','?content/help/category/1/index"& FileSuffix &"',0,',1,',1,0,1,'_self',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (2,0,1,'常见问题','?content/help/category/4/index"& FileSuffix &"',0,',2,',1,0,2,'_self',0)"

'**网站主导航**
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (3,0,2,'首页','?index"& FileSuffix &"',0,',3,',1,0,3,'_self',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (4,0,2,'公司介绍','?content/about/category/1/index"& FileSuffix &"',0,',4,',1,5,4,'_self',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (5,0,2,'管理团队','?content/about/category/3/index"& FileSuffix &"',4,',4,5,',2,0,1,'_self',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (6,0,2,'公司资质','?content/about/category/5/index"& FileSuffix &"',4,',4,6,',2,0,2,'_self',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (7,0,2,'合作伙伴','?content/about/category/6/index"& FileSuffix &"',4,',4,6,',2,0,3,'_self',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (8,0,2,'招聘信息','?content/about/category/7/index"& FileSuffix &"',4,',4,7,',2,0,4,'_self',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (9,0,2,'联系我们','?content/about/category/8/index"& FileSuffix &"',4,',4,8,',2,0,5,'_self',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (10,0,2,'新闻中心','?content/news/index"& FileSuffix &"',0,',10,',1,0,5,'_self',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (11,0,2,'产品中心','?content/product/index"& FileSuffix &"',0,',11,',1,0,6,'_self',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (12,0,2,'案例展示','?content/case/index"& FileSuffix &"',0,',12,',1,0,7,'_self',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (13,0,2,'产品商城','?shop"& FileSuffix &"',0,',13,',1,0,8,'_self',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (14,0,2,'投票中心','?vote"& FileSuffix &"',0,',14,',1,0,9,'_self',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (15,0,2,'帮助中心','?content/help/index"& FileSuffix &"',0,',15,',1,0,10,'_self',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (16,0,2,'在线留言','?guestbook"& FileSuffix &"',0,',16,',1,0,11,'_self',0)"

'**底部导航**
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (17,0,4,'关于我们','?content/about/category/1/index"& FileSuffix &"',0,',17,',1,0,12,'_blank',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (18,0,4,'管理团队','?content/about/category/3/index"& FileSuffix &"',0,',18,',1,0,13,'_blank',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (19,0,4,'公司资质','?content/about/category/5/index"& FileSuffix &"',0,',19,',1,0,14,'_blank',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (20,0,4,'合作伙伴','?content/about/category/6/index"& FileSuffix &"',0,',20,',1,0,15,'_blank',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (21,0,4,'招聘信息','?content/about/category/7/index"& FileSuffix &"',0,',21,',1,0,16,'_blank',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (22,0,4,'联系我们','?content/about/category/8/index"& FileSuffix &"',0,',22,',1,0,17,'_blank',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (23,0,4,'隐私保护','?content/about/category/10/index"& FileSuffix &"',0,',23,',1,0,18,'_blank',0)"

'**底部分类导航**
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (24,0,3,'服装/内衣/配件','?shop/category/1/index"& FileSuffix &"',0,',24,',1,2,19,'_blank',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (25,0,3,'珠宝饰品/手表眼镜','?shop/category/7/index"& FileSuffix &"',0,',25,',1,2,20,'_blank',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (26,0,3,'品牌女装','?shop/category/2/index"& FileSuffix &"',24,',24,26,',2,0,1,'_blank',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (27,0,3,'精品男装','?shop/category/6/index"& FileSuffix &"',24,',24,27,',2,0,2,'_blank',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (28,0,3,'时尚饰品','?shop/category/8/index"& FileSuffix &"',25,',25,28,',2,0,1,'_blank',0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Navigator] ([ID],[NavDefault],[NavType],[NavName],[NavUrl],[P_NavID],[NavPath],[NavDepth],[ChildNum],[OrderNum],[Target],[Disabled]) VALUES (29,0,3,'男士饰品','?shop/category/9/index"& FileSuffix &"',25,',25,29,',2,0,2,'_blank',0)"
Print "完成<br>"

'**广告**
Print "正在安装系统广告位数据，请耐心等候......"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Group] ([SysID],[AdsDefault],[AdsGroupName],[Identify],[StartTime],[EndTime],[Width],[Height],[OrderNum],[Disabled],[Remarks],[Recycle]) VALUES ('100001-000001-00000-00000-000000',1,'首页主广告栏','pubindex','"& Fn_NowTime &"','2022-01-01 00:00:01',980,250,1,0,'',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100001-000001-00000-00000-000001',1,'标题01','http://www.knifecms.com/','','"& Pv_SystemPath &"images/pub/index_01.jpg',1,0,0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100001-000001-00000-00000-000002',1,'标题02','http://www.knifecms.com/','','"& Pv_SystemPath &"images/pub/index_02.jpg',2,0,0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100001-000001-00000-00000-000003',1,'标题03','http://www.knifecms.com/','','"& Pv_SystemPath &"images/pub/index_03.jpg',3,0,0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100001-000001-00000-00000-000004',1,'标题04','http://www.knifecms.com/','','"& Pv_SystemPath &"images/pub/index_04.jpg',4,0,0)"


KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Group] ([SysID],[AdsDefault],[AdsGroupName],[Identify],[StartTime],[EndTime],[Width],[Height],[OrderNum],[Disabled],[Remarks],[Recycle]) VALUES ('100002-000000-00000-00000-000000',1,'内容模型共用广告位','pubsubsys','"& Fn_NowTime &"','2022-01-01 00:00:01',980,120,12,0,'',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100002-000000-00000-00000-000001',2,'','#01','','"& Pv_SystemPath &"images/pub/subsys_01.jpg',1,0,0)"


KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Group] ([SysID],[AdsDefault],[AdsGroupName],[Identify],[StartTime],[EndTime],[Width],[Height],[OrderNum],[Disabled],[Remarks],[Recycle]) VALUES ('100002-000001-00000-00000-000000',1,'企业介绍广告位','pubabout','"& Fn_NowTime &"','2022-01-01 00:00:01',980,120,10,0,'',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100002-000001-00000-00000-000001',3,'','#01','','"& Pv_SystemPath &"images/pub/about_01.jpg',1,0,0)"


KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Group] ([SysID],[AdsDefault],[AdsGroupName],[Identify],[StartTime],[EndTime],[Width],[Height],[OrderNum],[Disabled],[Remarks],[Recycle]) VALUES ('100002-000002-00000-00000-000000',1,'新闻中心广告位','pubnews','"& Fn_NowTime &"','2022-01-01 00:00:01',980,120,11,0,'',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100002-000002-00000-00000-000001',4,'','#01','','"& Pv_SystemPath &"images/pub/news_01.jpg',1,0,0)"


KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Group] ([SysID],[AdsDefault],[AdsGroupName],[Identify],[StartTime],[EndTime],[Width],[Height],[OrderNum],[Disabled],[Remarks],[Recycle]) VALUES ('100002-000003-00000-00000-000000',1,'产品中心广告位','pubproduct','"& Fn_NowTime &"','2022-01-01 00:00:01',980,120,11,0,'',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100002-000003-00000-00000-000001',5,'','#01','','"& Pv_SystemPath &"images/pub/product_01.jpg',1,0,0)"


KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Group] ([SysID],[AdsDefault],[AdsGroupName],[Identify],[StartTime],[EndTime],[Width],[Height],[OrderNum],[Disabled],[Remarks],[Recycle]) VALUES ('100002-000004-00000-00000-000000',1,'案例展示广告位','pubcase','"& Fn_NowTime &"','2022-01-01 00:00:01',980,120,11,0,'',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100002-000004-00000-00000-000001',6,'','#01','','"& Pv_SystemPath &"images/pub/case_01.jpg',1,0,0)"


KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Group] ([SysID],[AdsDefault],[AdsGroupName],[Identify],[StartTime],[EndTime],[Width],[Height],[OrderNum],[Disabled],[Remarks],[Recycle]) VALUES ('100002-000005-00000-00000-000000',1,'帮助中心广告位','pubhelp','"& Fn_NowTime &"','2022-01-01 00:00:01',980,120,11,0,'',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100002-000005-00000-00000-000001',7,'','#01','','"& Pv_SystemPath &"images/pub/help_01.jpg',1,0,0)"


KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Group] ([SysID],[AdsDefault],[AdsGroupName],[Identify],[StartTime],[EndTime],[Width],[Height],[OrderNum],[Disabled],[Remarks],[Recycle]) VALUES ('100003-000001-00000-00000-000000',1,'商城首页广告','pubshopindex','"& Fn_NowTime &"','2022-01-01 00:00:01',770,300,20,0,'',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100003-000001-00000-00000-000001',8,'标题01','#01','','"& Pv_SystemPath &"images/pub/shop_01.jpg',1,0,0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100003-000001-00000-00000-000002',8,'标题02','#02','','"& Pv_SystemPath &"images/pub/shop_02.jpg',2,0,0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100003-000001-00000-00000-000003',8,'标题03','#03','','"& Pv_SystemPath &"images/pub/shop_03.jpg',3,0,0)"
KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100003-000001-00000-00000-000004',8,'标题04','#04','','"& Pv_SystemPath &"images/pub/shop_04.jpg',4,0,0)"


KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Group] ([SysID],[AdsDefault],[AdsGroupName],[Identify],[StartTime],[EndTime],[Width],[Height],[OrderNum],[Disabled],[Remarks],[Recycle]) VALUES ('100003-000002-00000-00000-000000',1,'商城左边广告一','pubshopleft_1','"& Fn_NowTime &"','2022-01-01 00:00:01',200,254,21,0,'',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100003-000002-00000-00000-000001',9,'','http://www.knifecms.com/','','"& Pv_SystemPath &"images/pub/shopleft_01.jpg',1,0,0)"


KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Group] ([SysID],[AdsDefault],[AdsGroupName],[Identify],[StartTime],[EndTime],[Width],[Height],[OrderNum],[Disabled],[Remarks],[Recycle]) VALUES ('100003-000003-00000-00000-000000',1,'商城左边广告二','pubshopleft_2','"& Fn_NowTime &"','2022-01-01 00:00:01',200,230,22,0,'',0)"

KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Ads_Detail] ([SysID],[AdsGroupID],[AdsTitle],[AdsUrl],[AdsTxt],[ImageConfig],[OrderNum],[Disabled],[Recycle]) VALUES ('100003-000003-00000-00000-000002',10,'','http://www.knifecms.com/','','"& Pv_SystemPath &"images/pub/shopleft_02.jpg',1,0,0)"
Print "完成<br>"

'**添加邮件模板**
Print "正在安装邮件模板......"
Pv_TempHTML = "取回密码说明<br/><p>{tpl:username}，这封信是由 {tpl:sitename} 发送的。</p><p>您收到这封邮件，是由于这个邮箱地址在 {tpl:sitename} 被登记为用户邮箱，且该用户请求使用 Email 密码重置功能所致。</p><p>----------------------------------------------------------------------<br/><strong>重要！</strong><br/>----------------------------------------------------------------------</p><p>如果您没有提交密码重置的请求或不是 {tpl:sitename} 的注册用户，请立即忽略并删除这封邮件。只有在您确认需要重置密码的情况下，才需要继续阅读下面的内容。</p><p>----------------------------------------------------------------------<br/><strong>密码重置说明</strong><br/>----------------------------------------------------------------------</p><p></p>请通过点击下面的链接重置您的密码：<br/>{tpl:forgetpassword_link}<br/>(如果上面不是链接形式，请将该地址手工粘贴到浏览器地址栏再访问)<p></p><p>在上面的链接所打开的页面中输入新的密码后提交，您即可使用新的密码登录网站了。您可以在用户控制面板中随时修改您的密码。</p><p>本请求提交者的 IP 为 {tpl:ip}</p><p>此致<br/></p><p>{tpl:sitename} 管理团队. {tpl:website}</p>"
Pv_TempHTML = KnifeCMS.Data.HTMLEncode(Pv_TempHTML)
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"MailTemplate",Array("MailType:getpassword","MailTitle:找回密码","MailBody:"& Pv_TempHTML,"Remarks:发送到用户邮箱的找回密码邮件模板","OrderNum:1","Recycle:0"))
Print "完成<br>"

'**添加自定内容模块**
Print "正在安装自定内容模块......"
Pv_TempHTML = "<table cellspacing=""0"" cellpadding=""0"" border=""0""><tbody><tr><td valign=""top""><a href="""& Pv_SystemPath &"?content/about/category/1/index.htm""><img width=""145"" vspace=""0"" hspace=""0"" height=""200"" border=""0"" src="""& Pv_SystemPath &"images/pub/about.jpg"" style=""background:none repeat scroll 0% 0% #f6f6f6;border:1px solid #cccccc;padding:4px;float:none;width:145px;height:200px;""></a></td><td valign=""top"" style=""padding-left:12px;""><p><strong><span style=""font-size:14px;"">KnifeCMS简介 <span style=""font-size:14px;color:#7f7f7f;"">KnifeCMS Introduction</span></span></strong></p><p>KnifeCMS全称开放式商务建站系统，是一种完全开源免费的全新互联网建站系统。</p><p>KnifeCMS功能模块非常丰富，可随意组合出个性化的网站，用户在短时间内即可迅速架设属于自己的企业网站、电子商务网站、外贸网站、资讯门户和博客。</p><p>KnifeCMS内核健壮，程序和数据库采用严谨的程序数据优化算法，保证系统完全稳定运行；同时系统独创全新的SEO深入优化方案，即使用户不懂SEO，系统也会自动进行优化，方便用户快速有效地推广网站。</p><p>KnifeCMS易学易懂，采用应用式模块化管理和专用模板机制，使用简易的模板标签方式...<br>此内容在 后台>自定义标签 里修改</p></td></tr></tbody></table>"
Pv_TempHTML = KnifeCMS.Data.HTMLEncode(Pv_TempHTML)
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"CModule",Array("CModuleName:网站首页公司简介","CModuleLabel:about_us","Content:"& Pv_TempHTML,"Remarks:","OrderNum:1","Recycle:0"))
Pv_TempHTML = "<p>Copyright &copy; 2008-2018 <a href="""& Pv_SiteURL & Pv_SystemPath &""" target=""_blank"">"& Pv_SiteName &"</a>. All rights reserved</p>"
Pv_TempHTML = KnifeCMS.Data.HTMLEncode(Pv_TempHTML)
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"CModule",Array("CModuleName:网站底部版权信息","CModuleLabel:copyright","Content:"& Pv_TempHTML,"Remarks:","OrderNum:2","Recycle:0"))
Print "完成<br>"

'**添加友情链接数据**
Print "正在安装友情链接数据......"
'图片型
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Link",Array("SysID:100000-000000-00000-00001-000001","LinkName:企业建站系统","Category:1","LinkLogo:"& Pv_SystemPath &"images/logo/kfc.gif","LinkURL:http://www.knifecms.com/","Remarks:KnifeCMS企业建站系统官方网站","Recommend:1","Checked:1","Hits:0","OrderNum:1","AddTime:"& Fn_NowTime,"Recycle:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Link",Array("SysID:100000-000000-00000-00001-000002","LinkName:站酷网","Category:1","LinkLogo:"& Pv_SystemPath &"images/logo/zcool.gif","LinkURL:http://www.zcool.com.cn/","Remarks:","Recommend:1","Checked:1","Hits:0","OrderNum:2","AddTime:"& Fn_NowTime,"Recycle:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Link",Array("SysID:100000-000000-00000-00001-000003","LinkName:Iconfans","Category:1","LinkLogo:"& Pv_SystemPath &"images/logo/iconfans.gif","LinkURL:http://www.iconfans.com/","Remarks:","Recommend:1","Checked:1","Hits:0","OrderNum:3","AddTime:"& Fn_NowTime,"Recycle:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Link",Array("SysID:100000-000000-00000-00001-000004","LinkName:蓝色理想","Category:1","LinkLogo:"& Pv_SystemPath &"images/logo/blueidea.gif","LinkURL:http://www.blueidea.com/","Remarks:","Recommend:1","Checked:1","Hits:0","OrderNum:4","AddTime:"& Fn_NowTime,"Recycle:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Link",Array("SysID:100000-000000-00000-00001-000005","LinkName:视觉同盟","Category:1","LinkLogo:"& Pv_SystemPath &"images/logo/visionunion.gif","LinkURL:http://www.visionunion.com/","Remarks:","Recommend:1","Checked:1","Hits:0","OrderNum:5","AddTime:"& Fn_NowTime,"Recycle:0"))
'文字型
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Link",Array("SysID:100000-000000-00000-00002-000001","LinkName:企业建站系统","Category:1","LinkLogo:","LinkURL:http://www.knifecms.com/","Remarks:企业建站系统","Recommend:1","Checked:1","Hits:0","OrderNum:1","AddTime:"& Fn_NowTime,"Recycle:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Link",Array("SysID:100000-000000-00000-00002-000002","LinkName:开源建站系统交流","Category:1","LinkLogo:","LinkURL:http://bbs.knifecms.com/","Remarks:开源建站系统交流","Recommend:1","Checked:1","Hits:0","OrderNum:2","AddTime:"& Fn_NowTime,"Recycle:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Link",Array("SysID:100000-000000-00000-00002-000003","LinkName:Reeoo web design","Category:1","LinkLogo:","LinkURL:http://www.reeoo.com/","Remarks:","Recommend:1","Checked:1","Hits:0","OrderNum:3","AddTime:"& Fn_NowTime,"Recycle:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Link",Array("SysID:100000-000000-00000-00002-000004","LinkName:百度站长平台","Category:1","LinkLogo:","LinkURL:http://zhanzhang.baidu.com/","Remarks:","Recommend:1","Checked:1","Hits:0","OrderNum:4","AddTime:"& Fn_NowTime,"Recycle:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Link",Array("SysID:100000-000000-00000-00002-000005","LinkName:ThemeForest","Category:1","LinkLogo:","LinkURL:http://www.themeforest.net/","Remarks:","Recommend:1","Checked:1","Hits:0","OrderNum:5","AddTime:"& Fn_NowTime,"Recycle:0"))
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Link",Array("SysID:100000-000000-00000-00002-000006","LinkName:deviantART","Category:1","LinkLogo:","LinkURL:http://www.deviantart.com/","Remarks:","Recommend:1","Checked:1","Hits:0","OrderNum:6","AddTime:"& Fn_NowTime,"Recycle:0"))

'**添加新闻中心数据**
Call KnifeCMS.DB.AddRecord(Pv_TablePre &"Content_news",Array("SysID:100000-000000-00000-00000-000001","ClassID:1","Title:"& KnifeCMS.Data.HTMLEncode("世界，你好！"),"PublicTime:"& Fn_NowTime,"Thumbnail:","UseLinkUrl:0","Abstract:"& KnifeCMS.Data.HTMLEncode("欢迎使用 KnifeCMS，这是系统自动生成的演示文章。"),"Content:"& KnifeCMS.Data.HTMLEncode("欢迎使用 KnifeCMS。这是系统自动生成的演示文章。编辑或者删除它，然后开始您的建站之旅！"),"Hits:1","Comments:0","TopLine:0","OnTop:0","Hot:0","Elite:0","CommentSet:0","Openness:0","SlidesFirstImg:","SlidesImgs:","Status:1","AddTime:"& Fn_NowTime,"Diggs:0","SeoTitle:","SeoUrl:","MetaKeywords:","MetaDescription:","Recycle:0"))

Print "完成<br>"
%>