<%
'/****** 系统用户组权限 ******/
Echo "正在安装系统用户组权限数据......" : Response.Flush()
Call InsertSystemCtlAction(1,"systemindex","view","系统主界面","使用",0)
Call InsertSystemCtlAction(2,"systeminfo","view","系统信息","查看系统信息",1)
Call InsertSystemCtlAction(3,"systemset","view","系统设置","查看系统设置",2)
Call InsertSystemCtlAction(4,"systemset","edit","系统设置","编辑系统设置",2)
Call InsertSystemCtlAction(5,"regionset","view","地区管理","查看地区",3)
Call InsertSystemCtlAction(6,"regionset","add","地区管理","添加地区",3)
Call InsertSystemCtlAction(7,"regionset","edit","地区管理","编辑地区",3)
Call InsertSystemCtlAction(8,"regionset","del","地区管理","删除地区",3)
Call InsertSystemCtlAction(10,"navigator","view","站点导航管理","查看导航",4)
Call InsertSystemCtlAction(11,"navigator","add","站点导航管理","添加导航",4)
Call InsertSystemCtlAction(12,"navigator","edit","站点导航管理","编辑导航",4)
Call InsertSystemCtlAction(13,"navigator","batchedit","站点导航管理","批量编辑",4)
Call InsertSystemCtlAction(14,"navigator","del","站点导航管理","删除导航",4)
Call InsertSystemCtlAction(15,"cache","view","缓存管理","查看缓存列表",5)
Call InsertSystemCtlAction(16,"cache","edit","缓存管理","更新缓存",5)
Call InsertSystemCtlAction(17,"cache","del","缓存管理","清除缓存",5)
Call InsertSystemCtlAction(20,"sysusergroup","view","系统用户组","查看用户组列表",6)
Call InsertSystemCtlAction(21,"sysusergroup","add","系统用户组","添加用户组",6)
Call InsertSystemCtlAction(22,"sysusergroup","edit","系统用户组","编辑用户组",6)
Call InsertSystemCtlAction(23,"sysusergroup","del","系统用户组","删除用户组",6)
Call InsertSystemCtlAction(25,"sysuser","view","系统用户","查看用户列表",7)
Call InsertSystemCtlAction(27,"sysuser","add","系统用户","添加用户",7)
Call InsertSystemCtlAction(28,"sysuser","edit","系统用户","编辑用户",7)
Call InsertSystemCtlAction(29,"sysuser","del","系统用户","删除用户",7)
Call InsertSystemCtlAction(36,"sysuser_recycle","view","系统用户回收站","查看用户列表",8)
Call InsertSystemCtlAction(37,"sysuser_recycle","edit","系统用户回收站","还原用户",8)
Call InsertSystemCtlAction(38,"sysuser_recycle","del","系统用户回收站","删除用户",8)
Call InsertSystemCtlAction(40,"syslog","view","系统日志","查看日志列表",9)
Call InsertSystemCtlAction(41,"syslog","del","系统日志","删除日志",9)
Call InsertSystemCtlAction(42,"syslog_recycle","view","系统日志回收站","查看日志列表",10)
Call InsertSystemCtlAction(43,"syslog_recycle","edit","系统日志回收站","还原日志",10)
Call InsertSystemCtlAction(44,"syslog_recycle","del","系统日志回收站","删除日志",10)
Call InsertSystemCtlAction(50,"paytradelog","view","支付交易记录","查看记录",11)

Call InsertSystemCtlAction(54,"template","view","模板管理","查看模板",12)
Call InsertSystemCtlAction(55,"template","add","模板管理","添加模板",12)
Call InsertSystemCtlAction(56,"template","activate","模板管理","启用模板",12)
Call InsertSystemCtlAction(57,"template","edit","模板管理","编辑模板",12)
Call InsertSystemCtlAction(58,"template","del","模板管理","删除模板",12)

Call InsertSystemCtlAction(70,"file_image","view","图片管理","查看图片",15)
Call InsertSystemCtlAction(71,"file_image","add","图片管理","上传图片",15)
Call InsertSystemCtlAction(72,"file_image","del","图片管理","删除图片",15)
Call InsertSystemCtlAction(73,"file_image","check","图片管理","冗余图片检测",15)
Call InsertSystemCtlAction(75,"file_file","view","普通文件管理","查看普通文件",16)
Call InsertSystemCtlAction(76,"file_file","add","普通文件管理","上传普通文件",16)
Call InsertSystemCtlAction(77,"file_file","del","普通文件管理","删除普通文件",16)
Call InsertSystemCtlAction(78,"file_file","check","普通文件管理","冗余普通文件检测",16)
Call InsertSystemCtlAction(80,"file_flash","view","Flash文件管理","查看Flash文件",17)
Call InsertSystemCtlAction(81,"file_flash","add","Flash文件管理","上传Flash文件",17)
Call InsertSystemCtlAction(82,"file_flash","del","Flash文件管理","删除Flash文件",17)
Call InsertSystemCtlAction(83,"file_flash","check","Flash文件管理","冗余Flash文件检测",17)
Call InsertSystemCtlAction(85,"file_music","view","音乐文件管理","查看音乐文件",18)
Call InsertSystemCtlAction(86,"file_music","add","音乐文件管理","上传音乐文件",18)
Call InsertSystemCtlAction(87,"file_music","del","音乐文件管理","删除音乐文件",18)
Call InsertSystemCtlAction(88,"file_music","check","音乐文件管理","冗余音乐文件检测",18)
Call InsertSystemCtlAction(90,"file_media","view","视频文件管理","查看视频文件",19)
Call InsertSystemCtlAction(91,"file_media","add","视频文件管理","上传视频文件",19)
Call InsertSystemCtlAction(92,"file_media","del","视频文件管理","删除视频文件",19)
Call InsertSystemCtlAction(93,"file_media","check","视频文件管理","冗余视频文件检测",19)

Call InsertSystemCtlAction(95,"systembackup","view","系统备份管理","查看备份信息",20)
Call InsertSystemCtlAction(96,"systembackup","backup","系统备份管理","进行备份操作",20)
Call InsertSystemCtlAction(97,"systembackup","restore","系统备份管理","系统还原",20)
Call InsertSystemCtlAction(98,"systembackup","del","系统备份管理","删除备份文件",20)

Call InsertSystemCtlAction(100,"onlineservice","view","在线客服设置","查看客服设置",21)
Call InsertSystemCtlAction(101,"onlineservice","edit","在线客服设置","修改客服设置",21)


Call InsertSystemCtlAction(200,"goods","view","商品系统","查看商品列表",120)
Call InsertSystemCtlAction(201,"goods","add","商品系统","添加商品",120)
Call InsertSystemCtlAction(202,"goods","edit","商品系统","编辑商品",120)
Call InsertSystemCtlAction(203,"goods","del","商品系统","删除商品",120)
Call InsertSystemCtlAction(210,"goods_recycle","view","商品回收站","查看商品列表",121)
Call InsertSystemCtlAction(211,"goods_recycle","edit","商品回收站","还原商品",121)
Call InsertSystemCtlAction(212,"goods_recycle","del","商品回收站","删除商品",121)
Call InsertSystemCtlAction(215,"goodsfieldset","view","自定义商品字段管理","查看自定义字段",119)
Call InsertSystemCtlAction(216,"goodsfieldset","add","自定义商品字段管理","添加自定义字段",119)
Call InsertSystemCtlAction(217,"goodsfieldset","edit","自定义商品字段管理","编辑自定义字段",119)
Call InsertSystemCtlAction(218,"goodsfieldset","del","自定义商品字段管理","删除自定义字段",119)
Call InsertSystemCtlAction(220,"goods_class","view","商品系统分类","查看分类列表",122)
Call InsertSystemCtlAction(221,"goods_class","add","商品系统分类","添加分类",122)
Call InsertSystemCtlAction(222,"goods_class","edit","商品系统分类","编辑分类",122)
Call InsertSystemCtlAction(223,"goods_class","batchedit","商品系统分类","批量编辑",122)
Call InsertSystemCtlAction(224,"goods_class","del","商品系统分类","删除分类",122)
Call InsertSystemCtlAction(225,"goodsvirtual_class","view","商品系统虚拟分类","查看分类列表",123)
Call InsertSystemCtlAction(226,"goodsvirtual_class","add","商品系统虚拟分类","添加分类",123)
Call InsertSystemCtlAction(227,"goodsvirtual_class","edit","商品系统虚拟分类","编辑分类",123)
Call InsertSystemCtlAction(228,"goodsvirtual_class","batchedit","商品系统虚拟分类","批量编辑",123)
Call InsertSystemCtlAction(229,"goodsvirtual_class","del","商品系统虚拟分类","删除分类",123)
Call InsertSystemCtlAction(230,"goodsinvirtualclass","view","虚拟分类商品管理","查看商品列表",124)
Call InsertSystemCtlAction(231,"goodsinvirtualclass","add","虚拟分类商品管理","添加商品",124)
Call InsertSystemCtlAction(232,"goodsinvirtualclass","edit","虚拟分类商品管理","编辑商品",124)
Call InsertSystemCtlAction(233,"goodsinvirtualclass","del","虚拟分类商品管理","删除商品",124)
Call InsertSystemCtlAction(240,"goodsmodel","view","商品类型","查看类型列表",125)
Call InsertSystemCtlAction(241,"goodsmodel","add","商品类型","添加类型",125)
Call InsertSystemCtlAction(242,"goodsmodel","edit","商品类型","编辑类型",125)
Call InsertSystemCtlAction(243,"goodsmodel","del","商品类型","删除类型",125)
Call InsertSystemCtlAction(245,"attribute","view","商品类型属性","查看属性",126)
Call InsertSystemCtlAction(246,"attribute","add","商品类型属性","添加属性",126)
Call InsertSystemCtlAction(247,"attribute","edit","商品类型属性","编辑属性",126)
Call InsertSystemCtlAction(248,"attribute","del","商品类型属性","删除属性",126)
Call InsertSystemCtlAction(250,"brand","view","品牌管理系统","查看品牌",130)
Call InsertSystemCtlAction(251,"brand","add","品牌管理系统","添加品牌",130)
Call InsertSystemCtlAction(252,"brand","edit","品牌管理系统","编辑品牌",130)
Call InsertSystemCtlAction(253,"brand","del","品牌管理系统","删除品牌",130)
Call InsertSystemCtlAction(255,"brand_recycle","view","品牌回收站","查看品牌",131)
Call InsertSystemCtlAction(256,"brand_recycle","edit","品牌回收站","还原品牌",131)
Call InsertSystemCtlAction(257,"brand_recycle","del","品牌回收站","删除品牌",131)
Call InsertSystemCtlAction(260,"order","view","订单系统","查看订单",132)
Call InsertSystemCtlAction(261,"order","add","订单系统","添加订单",132)
Call InsertSystemCtlAction(262,"order","edit","订单系统","编辑订单",132)
Call InsertSystemCtlAction(263,"order","del","订单系统","删除订单",132)
Call InsertSystemCtlAction(264,"order","orderpay","订单系统","订单支付",132)
Call InsertSystemCtlAction(265,"order","orderrefund","订单系统","订单退款",132)
Call InsertSystemCtlAction(266,"order","ordership","订单系统","订单发货",132)
Call InsertSystemCtlAction(267,"order","orderreship","订单系统","订单退货",132)
Call InsertSystemCtlAction(268,"order","orderfinish","订单系统","订单完成",132)
Call InsertSystemCtlAction(269,"order","orderinvalid","订单系统","订单作废",132)
Call InsertSystemCtlAction(270,"order_recycle","view","订单回收站","查看订单",133)
Call InsertSystemCtlAction(271,"order_recycle","edit","订单回收站","还原订单",133)
Call InsertSystemCtlAction(272,"order_recycle","del","订单回收站","删除订单",133)
Call InsertSystemCtlAction(273,"paymentconfig","view","支付方式","查看支付方式",135)
Call InsertSystemCtlAction(274,"paymentconfig","add","支付方式","添加支付方式",135)
Call InsertSystemCtlAction(275,"paymentconfig","edit","支付方式","编辑支付方式",135)
Call InsertSystemCtlAction(276,"paymentconfig","del","支付方式","删除支付方式",135)
Call InsertSystemCtlAction(280,"deliveryconfig","view","配送方式","查看配送方式",136)
Call InsertSystemCtlAction(281,"deliveryconfig","add","配送方式","添加配送方式",136)
Call InsertSystemCtlAction(282,"deliveryconfig","edit","配送方式","编辑配送方式",136)
Call InsertSystemCtlAction(283,"deliveryconfig","del","配送方式","删除配送方式",136)
Call InsertSystemCtlAction(285,"deliverycorp","view","物流公司管理","查看物流公司",138)
Call InsertSystemCtlAction(286,"deliverycorp","add","物流公司管理","添加物流公司",138)
Call InsertSystemCtlAction(287,"deliverycorp","edit","物流公司管理","编辑物流公司",138)
Call InsertSystemCtlAction(288,"deliverycorp","del","物流公司管理","删除物流公司",138)
Call InsertSystemCtlAction(290,"goods_consult","view","商品咨询","查看咨询列表",140)
Call InsertSystemCtlAction(291,"goods_consult","add","商品咨询","回复咨询",140)
Call InsertSystemCtlAction(292,"goods_consult","edit","商品咨询","编辑咨询",140)
Call InsertSystemCtlAction(293,"goods_consult","del","商品咨询","删除咨询",140)
Call InsertSystemCtlAction(294,"goods_comment","view","商品评论","查看评论列表",141)
Call InsertSystemCtlAction(295,"goods_comment","add","商品评论","回复评论",141)
Call InsertSystemCtlAction(296,"goods_comment","edit","商品评论","咨询评论",141)
Call InsertSystemCtlAction(297,"goods_comment","del","商品评论","删除评论",141)
Call InsertSystemCtlAction(300,"paymentbill","view","收款单","查看收款单",145)
Call InsertSystemCtlAction(303,"paymentbill","del","收款单","删除收款单",145)
Call InsertSystemCtlAction(306,"refundbill","view","退款单","查看退款单",146)
Call InsertSystemCtlAction(309,"refundbill","del","退款单","删除退款单",146)
Call InsertSystemCtlAction(310,"shipbill","view","发货单","查看发货单",147)
Call InsertSystemCtlAction(313,"shipbill","del","发货单","删除发货单",147)
Call InsertSystemCtlAction(316,"reshipbill","view","退货单","查看退货单",148)
Call InsertSystemCtlAction(319,"reshipbill","del","退货单","删除退货单",148)


Call InsertSystemCtlAction(400,"adsgroup","view","广告位管理","查看广告位列表",170)
Call InsertSystemCtlAction(401,"adsgroup","add","广告位管理","添加广告位",170)
Call InsertSystemCtlAction(402,"adsgroup","edit","广告位管理","编辑广告位",170)
Call InsertSystemCtlAction(403,"adsgroup","del","广告位管理","删除广告位",170)
Call InsertSystemCtlAction(405,"adsgroup_recycle","view","广告位回收站","查看广告位",171)
Call InsertSystemCtlAction(406,"adsgroup_recycle","edit","广告位回收站","还原广告位",171)
Call InsertSystemCtlAction(407,"adsgroup_recycle","del","广告位回收站","删除广告位",171)
Call InsertSystemCtlAction(410,"adsdetail","view","广告详细","查看广告",172)
Call InsertSystemCtlAction(411,"adsdetail","add","广告详细","添加广告",172)
Call InsertSystemCtlAction(412,"adsdetail","edit","广告详细","编辑广告",172)
Call InsertSystemCtlAction(413,"adsdetail","del","广告详细","删除广告",172)


Call InsertSystemCtlAction(420,"user","view","会员管理","查看会员信息",175)
Call InsertSystemCtlAction(422,"user","edit","会员管理","编辑会员",175)
Call InsertSystemCtlAction(423,"user","del","会员管理","删除会员",175)
Call InsertSystemCtlAction(424,"user_recycle","view","会员回收站","查看会员信息",176)
Call InsertSystemCtlAction(425,"user_recycle","edit","会员回收站","还原会员",176)
Call InsertSystemCtlAction(426,"user_recycle","del","会员回收站","删除会员",176)
Call InsertSystemCtlAction(430,"twriter","view","会员微博","查看微博",177)
Call InsertSystemCtlAction(433,"twriter","del","会员微博","删除微博",177)
Call InsertSystemCtlAction(435,"userguestbook","view","会员留言板","查看留言",178)
Call InsertSystemCtlAction(438,"userguestbook","del","会员留言板","删除留言",178)
Call InsertSystemCtlAction(440,"weixin","view","微信管理","查看微信",180)
Call InsertSystemCtlAction(441,"weixin","del","微信管理","删除微信",180)


Call InsertSystemCtlAction(500,"collection","view","采集系统","查看采集器列表",200)
Call InsertSystemCtlAction(501,"collection","add","采集系统","添加采集器",200)
Call InsertSystemCtlAction(502,"collection","edit","采集系统","编辑采集器",200)
Call InsertSystemCtlAction(503,"collection","del","采集系统","删除采集器",200)
Call InsertSystemCtlAction(504,"collection","copy","采集系统","复制采集器",200)
Call InsertSystemCtlAction(505,"collection","cmd","采集系统","运行采集器(调试/采集)",200)
Call InsertSystemCtlAction(506,"collection_result","view","采集结果","查看采集数据",201)
Call InsertSystemCtlAction(507,"collection_result","edit","采集结果","编辑采集数据",201)
Call InsertSystemCtlAction(508,"collection_result","del","采集结果","删除采集数据",201)
Call InsertSystemCtlAction(509,"collection_result","export","采集结果","导出采集数据",201)

Call InsertSystemCtlAction(510,"vote_class","view","投票系统分类","查看分类列表",202)
Call InsertSystemCtlAction(511,"vote_class","add","投票系统分类","编辑分类",202)
Call InsertSystemCtlAction(512,"vote_class","edit","投票系统分类","添加分类",202)
Call InsertSystemCtlAction(513,"vote_class","del","投票系统分类","删除分类",202)
Call InsertSystemCtlAction(514,"vote_class","batchedit","投票系统分类","批量编辑",202)
Call InsertSystemCtlAction(515,"vote","view","投票系统","查看投票列表",203)
Call InsertSystemCtlAction(516,"vote","add","投票系统","添加投票",203)
Call InsertSystemCtlAction(517,"vote","edit","投票系统","编辑投票",203)
Call InsertSystemCtlAction(518,"vote","del","投票系统","删除投票",203)
Call InsertSystemCtlAction(520,"vote_recycle","view","投票系统回收站","查看投票列表",204)
Call InsertSystemCtlAction(521,"vote_recycle","edit","投票系统回收站","还原投票",204)
Call InsertSystemCtlAction(522,"vote_recycle","del","投票系统回收站","删除投票",204)

Call InsertSystemCtlAction(525,"guestbook","view","留言板","查看留言列表",205)
Call InsertSystemCtlAction(526,"guestbook","add","留言板","回复留言",205)
Call InsertSystemCtlAction(527,"guestbook","edit","留言板","审核留言",205)
Call InsertSystemCtlAction(528,"guestbook","del","留言板","删除留言",205)

Call InsertSystemCtlAction(530,"link","view","友情链接管理","查看友情链接",206)
Call InsertSystemCtlAction(531,"link","add","友情链接管理","添加友情链接",206)
Call InsertSystemCtlAction(532,"link","edit","友情链接管理","编辑友情链接",206)
Call InsertSystemCtlAction(533,"link","del","友情链接管理","删除友情链接",206)

Call InsertSystemCtlAction(542,"cmodule","view","自定义标签","查看标签",208)
Call InsertSystemCtlAction(543,"cmodule","add","自定义标签","添加标签",208)
Call InsertSystemCtlAction(544,"cmodule","edit","自定义标签","编辑标签",208)
Call InsertSystemCtlAction(545,"cmodule","del","自定义标签","删除标签",208)

Call InsertSystemCtlAction(546,"fieldset","view","自定义字段管理","查看自定义字段",209)
Call InsertSystemCtlAction(547,"fieldset","add","自定义字段管理","添加自定义字段",209)
Call InsertSystemCtlAction(548,"fieldset","edit","自定义字段管理","编辑自定义字段",209)
Call InsertSystemCtlAction(549,"fieldset","del","自定义字段管理","删除自定义字段",209)

Call InsertSystemCtlAction(550,"contentsubsystem","view","内容模型管理","查看内容模型",210)
Call InsertSystemCtlAction(551,"contentsubsystem","add","内容模型管理","安装内容模型",210)
Call InsertSystemCtlAction(552,"contentsubsystem","edit","内容模型管理","内容模型设置",210)
Call InsertSystemCtlAction(553,"contentsubsystem","del","内容模型管理","卸载内容模型",210)

Call InsertSystemCtlAction(554,"content_about","view","企业介绍","查看内容列表",211)
Call InsertSystemCtlAction(555,"content_about","add","企业介绍","添加内容",211)
Call InsertSystemCtlAction(556,"content_about","edit","企业介绍","编辑内容",211)
Call InsertSystemCtlAction(557,"content_about","del","企业介绍","删除内容",211)

Call InsertSystemCtlAction(558,"content_about_drafts","view","企业介绍草稿箱","查看内容列表",212)
Call InsertSystemCtlAction(559,"content_about_drafts","edit","企业介绍草稿箱","编辑内容",212)
Call InsertSystemCtlAction(560,"content_about_drafts","del","企业介绍草稿箱","删除内容",212)

Call InsertSystemCtlAction(561,"content_about_recycle","view","企业介绍回收站","查看内容列表",213)
Call InsertSystemCtlAction(562,"content_about_recycle","edit","企业介绍回收站","还原内容",213)
Call InsertSystemCtlAction(563,"content_about_recycle","del","企业介绍回收站","删除内容",213)

Call InsertSystemCtlAction(564,"content_about_class","view","企业介绍分类","查看分类列表",214)
Call InsertSystemCtlAction(565,"content_about_class","add","企业介绍分类","添加分类",214)
Call InsertSystemCtlAction(566,"content_about_class","edit","企业介绍分类","编辑分类",214)
Call InsertSystemCtlAction(567,"content_about_class","batchedit","企业介绍分类","批量编辑",214)
Call InsertSystemCtlAction(568,"content_about_class","del","企业介绍分类","删除分类",214)

Call InsertSystemCtlAction(569,"content_about_comment","view","企业介绍评论","查看评论列表",215)
Call InsertSystemCtlAction(570,"content_about_comment","add","企业介绍评论","回复评论",215)
Call InsertSystemCtlAction(571,"content_about_comment","edit","企业介绍评论","编辑评论",215)
Call InsertSystemCtlAction(572,"content_about_comment","del","企业介绍评论","删除评论",215)

Call InsertSystemCtlAction(573,"content_news","view","新闻中心","查看内容列表",216)
Call InsertSystemCtlAction(574,"content_news","add","新闻中心","添加内容",216)
Call InsertSystemCtlAction(575,"content_news","edit","新闻中心","编辑内容",216)
Call InsertSystemCtlAction(576,"content_news","del","新闻中心","删除内容",216)

Call InsertSystemCtlAction(577,"content_news_drafts","view","新闻中心草稿箱","查看内容列表",217)
Call InsertSystemCtlAction(578,"content_news_drafts","edit","新闻中心草稿箱","编辑内容",217)
Call InsertSystemCtlAction(579,"content_news_drafts","del","新闻中心草稿箱","删除内容",217)

Call InsertSystemCtlAction(580,"content_news_recycle","view","新闻中心回收站","查看内容列表",218)
Call InsertSystemCtlAction(581,"content_news_recycle","edit","新闻中心回收站","还原内容",218)
Call InsertSystemCtlAction(582,"content_news_recycle","del","新闻中心回收站","删除内容",218)

Call InsertSystemCtlAction(583,"content_news_class","view","新闻中心分类","查看分类列表",219)
Call InsertSystemCtlAction(584,"content_news_class","add","新闻中心分类","添加分类",219)
Call InsertSystemCtlAction(585,"content_news_class","edit","新闻中心分类","编辑分类",219)
Call InsertSystemCtlAction(586,"content_news_class","batchedit","新闻中心分类","批量编辑",219)
Call InsertSystemCtlAction(587,"content_news_class","del","新闻中心分类","删除分类",219)

Call InsertSystemCtlAction(588,"content_news_comment","view","新闻中心评论","查看评论列表",220)
Call InsertSystemCtlAction(589,"content_news_comment","add","新闻中心评论","回复评论",220)
Call InsertSystemCtlAction(590,"content_news_comment","edit","新闻中心评论","编辑评论",220)
Call InsertSystemCtlAction(591,"content_news_comment","del","新闻中心评论","删除评论",220)

Call InsertSystemCtlAction(592,"content_help","view","帮助中心","查看内容列表",221)
Call InsertSystemCtlAction(593,"content_help","add","帮助中心","添加内容",221)
Call InsertSystemCtlAction(594,"content_help","edit","帮助中心","编辑内容",221)
Call InsertSystemCtlAction(595,"content_help","del","帮助中心","删除内容",221)

Call InsertSystemCtlAction(596,"content_help_drafts","view","帮助中心草稿箱","查看内容列表",222)
Call InsertSystemCtlAction(597,"content_help_drafts","edit","帮助中心草稿箱","编辑内容",222)
Call InsertSystemCtlAction(598,"content_help_drafts","del","帮助中心草稿箱","删除内容",222)

Call InsertSystemCtlAction(599,"content_help_recycle","view","帮助中心回收站","查看内容列表",223)
Call InsertSystemCtlAction(600,"content_help_recycle","edit","帮助中心回收站","还原内容",223)
Call InsertSystemCtlAction(601,"content_help_recycle","del","帮助中心回收站","删除内容",223)

Call InsertSystemCtlAction(602,"content_help_class","view","帮助中心分类","查看分类列表",224)
Call InsertSystemCtlAction(603,"content_help_class","add","帮助中心分类","添加分类",224)
Call InsertSystemCtlAction(604,"content_help_class","edit","帮助中心分类","编辑分类",224)
Call InsertSystemCtlAction(605,"content_help_class","batchedit","帮助中心分类","批量编辑",224)
Call InsertSystemCtlAction(606,"content_help_class","del","帮助中心分类","删除分类",224)

Call InsertSystemCtlAction(607,"content_help_comment","view","帮助中心评论","查看评论列表",225)
Call InsertSystemCtlAction(608,"content_help_comment","add","帮助中心评论","回复评论",225)
Call InsertSystemCtlAction(609,"content_help_comment","edit","帮助中心评论","编辑评论",225)
Call InsertSystemCtlAction(610,"content_help_comment","del","帮助中心评论","删除评论",225)

Call InsertSystemCtlAction(611,"content_product","view","产品中心","查看内容列表",226)
Call InsertSystemCtlAction(612,"content_product","add","产品中心","添加内容",226)
Call InsertSystemCtlAction(613,"content_product","edit","产品中心","编辑内容",226)
Call InsertSystemCtlAction(614,"content_product","del","产品中心","删除内容",226)

Call InsertSystemCtlAction(615,"content_product_drafts","view","产品中心草稿箱","查看内容列表",227)
Call InsertSystemCtlAction(616,"content_product_drafts","edit","产品中心草稿箱","编辑内容",227)
Call InsertSystemCtlAction(617,"content_product_drafts","del","产品中心草稿箱","删除内容",227)

Call InsertSystemCtlAction(618,"content_product_recycle","view","产品中心回收站","查看内容列表",228)
Call InsertSystemCtlAction(619,"content_product_recycle","edit","产品中心回收站","还原内容",228)
Call InsertSystemCtlAction(620,"content_product_recycle","del","产品中心回收站","删除内容",228)

Call InsertSystemCtlAction(621,"content_product_class","view","产品中心分类","查看分类列表",229)
Call InsertSystemCtlAction(622,"content_product_class","add","产品中心分类","添加分类",229)
Call InsertSystemCtlAction(623,"content_product_class","edit","产品中心分类","编辑分类",229)
Call InsertSystemCtlAction(624,"content_product_class","batchedit","产品中心分类","批量编辑",229)
Call InsertSystemCtlAction(625,"content_product_class","del","产品中心分类","删除分类",229)

Call InsertSystemCtlAction(626,"content_product_comment","view","产品中心评论","查看评论列表",230)
Call InsertSystemCtlAction(627,"content_product_comment","add","产品中心评论","回复评论",230)
Call InsertSystemCtlAction(628,"content_product_comment","edit","产品中心评论","编辑评论",230)
Call InsertSystemCtlAction(629,"content_product_comment","del","产品中心评论","删除评论",230)

Call InsertSystemCtlAction(630,"content_case","view","案例展示","查看内容列表",231)
Call InsertSystemCtlAction(631,"content_case","add","案例展示","添加内容",231)
Call InsertSystemCtlAction(632,"content_case","edit","案例展示","编辑内容",231)
Call InsertSystemCtlAction(633,"content_case","del","案例展示","删除内容",231)

Call InsertSystemCtlAction(634,"content_case_drafts","view","案例展示草稿箱","查看内容列表",232)
Call InsertSystemCtlAction(635,"content_case_drafts","edit","案例展示草稿箱","编辑内容",232)
Call InsertSystemCtlAction(636,"content_case_drafts","del","案例展示草稿箱","删除内容",232)

Call InsertSystemCtlAction(637,"content_case_recycle","view","案例展示回收站","查看内容列表",233)
Call InsertSystemCtlAction(638,"content_case_recycle","edit","案例展示回收站","还原内容",233)
Call InsertSystemCtlAction(639,"content_case_recycle","del","案例展示回收站","删除内容",233)

Call InsertSystemCtlAction(640,"content_case_class","view","案例展示分类","查看分类列表",234)
Call InsertSystemCtlAction(641,"content_case_class","add","案例展示分类","添加分类",234)
Call InsertSystemCtlAction(642,"content_case_class","edit","案例展示分类","编辑分类",234)
Call InsertSystemCtlAction(643,"content_case_class","batchedit","案例展示分类","批量编辑",234)
Call InsertSystemCtlAction(644,"content_case_class","del","案例展示分类","删除分类",234)

Call InsertSystemCtlAction(645,"content_case_comment","view","案例展示评论","查看评论列表",235)
Call InsertSystemCtlAction(646,"content_case_comment","add","案例展示评论","回复评论",235)
Call InsertSystemCtlAction(647,"content_case_comment","edit","案例展示评论","编辑评论",235)
Call InsertSystemCtlAction(648,"content_case_comment","del","案例展示评论","删除评论",235)

'添加创始人管理员组的权限(拥有所有权限)
Set Fn_Rs = KnifeCMS.DB.GetRecord(Pv_TablePre &"SystemCtlAction:ID,SystemCtl,SystemAction","","ORDER BY ID ASC")
Do While Not(Fn_Rs.Eof)
	KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"SysUserPermission] ([GroupID],[SystemCtl],[SystemAction],[IsAllow]) VALUES (1,'"& Fn_Rs(1) &"','"& Fn_Rs(2) &"',1)"
	Fn_Rs.Movenext()
Loop
KnifeCMS.DB.CloseRs Fn_Rs
'添加创始人管理员组的权限:end
Echo "完成<br>" : Response.Flush()
%>