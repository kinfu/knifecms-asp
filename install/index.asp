<!--#include file="../config/config.main.asp"-->
<!--#include file="../include/kfc.main.asp"-->
<%
Dim ClassInstall
Set ClassInstall = New Class_Install
Set ClassInstall = Nothing
Class Class_Install
	Private Pv_CPUs,Pv_CPU,Pv_OS,Pv_ObjectVersion
	Private Pv_SiteName,Pv_SiteURL,Pv_Domain,Pv_SystemPath,Pv_DB_Type,Pv_DB_AccessFilePath,Pv_DB_AccessFileName,Pv_DB_Server,Pv_DB_Port,Pv_DB_Name,Pv_DB_Username,Pv_DB_Password,Pv_TablePre
	Private Pv_Action,Pv_CanDoNext,Pv_IsCanCreateDBTable
	Private Pv_Sysuser_Username,Pv_Sysuser_Password,Pv_Sysuser_Repassword,Pv_Sysuser_Email,Pv_Encryptionkey
	Private Pv_ConnStr,Pv_CreateNewDBConnStr
	Private Pv_TempHTML
	
	Private Sub Class_Initialize()
		Call Header()
		Pv_CanDoNext          = True
		Pv_IsCanCreateDBTable = False
		Call GetSysInfo()
		Call GetDomainAndSystemPath()
		Pv_Action        = KnifeCMS.GetForm("both","action")
		Pv_DB_Type       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","db_type"))
		Pv_DB_AccessFileName= Replace(Replace(KnifeCMS.GetForm("post","db_accessfilename")," ",""),Chr(32),"")
		Pv_DB_Server     = KnifeCMS.GetForm("post","db_server")
		Pv_DB_Port       = KnifeCMS.GetForm("post","db_port")
		Pv_DB_Name       = KnifeCMS.GetForm("post","db_name")
		Pv_DB_Username   = KnifeCMS.GetForm("post","db_username")
		Pv_DB_Password   = KnifeCMS.GetForm("post","db_password")
		Pv_TablePre      = KnifeCMS.GetForm("post","db_tablepre")
		Pv_Sysuser_Username   = KnifeCMS.GetForm("post","sysuser_username")
		Pv_Sysuser_Password   = KnifeCMS.GetForm("post","sysuser_password")
		Pv_Sysuser_Repassword = KnifeCMS.GetForm("post","sysuser_repassword")
		Pv_Sysuser_Email      = KnifeCMS.GetForm("post","sysuser_email")
		Pv_Encryptionkey      = KnifeCMS.GetForm("post","encryptionkey")
		If KnifeCMS.FSO.FileExists("data/install.lock") And Pv_Action<>"step5" Then
			Echo "<div id=""mbody""><div class=""cuebox"">系统已经安装，若要重新安装请先删除data/install.lock文件。</div></div>"
		Else
			Select Case Pv_Action
				Case "step1"
					Call Step1()
				Case "step2"
					Call Step2()
				Case "step3"
					Pv_Encryptionkey = EncryptionKey
					If Pv_Encryptionkey &""="" Then Pv_Encryptionkey = KnifeCMS.MD5.Encrypt(KnifeCMS.Data.FormatDateTime(SysTime,9))
					Pv_DB_AccessFileName = "db"& KnifeCMS.Data.FormatDateTime(SysTime,9) &".asp"
					Call Step3()
				Case "step4"
					If Pv_Encryptionkey &""="" Then Pv_Encryptionkey = KnifeCMS.MD5.Encrypt(KnifeCMS.Data.FormatDateTime(SysTime,9))
					Call CheckDataBaseAndSaveConfig()
					If Pv_CanDoNext Then Call Step4()
				Case "step5"
					Call Step5()
				Case Else
					Call Start()
			End Select
		End If
		Call Footer()
	End Sub
	Private Sub Class_Terminate()
		Set OS = Nothing
	End Sub
	
	
	'获取服务器常用参数
	Sub GetSysInfo()
		On Error Resume Next
		Dim Fn_WshShell,Fn_WshSysEnv
		Set Fn_WshShell  = Server.CreateObject("WScript.Shell")
		Set Fn_WshSysEnv = Fn_WshShell.Environment("SYSTEM")
		Pv_OS   = cstr(Fn_WshSysEnv("OS"))
		Pv_CPUs = cstr(Fn_WshSysEnv("NUMBER_OF_PROCESSORS"))
		Pv_CPU  = cstr(Fn_WshSysEnv("PROCESSOR_IDENTIFIER"))
		if isempty(Pv_CPUs) then
		  Pv_CPUs = Request.ServerVariables("NUMBER_OF_PROCESSORS")
		end if
		if Pv_CPUs & "" = "" then Pv_CPUs = "(未知)"
		if Pv_OS & "" = "" then Pv_OS = "(未知)"
	End Sub
	
	Function GetDomainAndSystemPath()
		Dim Fn_URL,Fn_HttpStr,Fn_Domain,Fn_Port,Fn_Dir,Fn_SystemPath
		If LCase(Request.ServerVariables("HTTPS"))="off" Then Fn_HttpStr="http://" : Else : Fn_HttpStr="https://" : End If
		Fn_Domain=LCase(Request.ServerVariables("SERVER_NAME"))
		If Request.ServerVariables("SERVER_PORT")<>80 Then Fn_Port=Request.ServerVariables("SERVER_PORT")
		Fn_URL=LCase(Request.ServerVariables("URL")) '/knifecms/install/index.asp
		Fn_Dir=Left(Fn_URL,InstrRev(Fn_URL,"/")) '/knifecms/install/
		Fn_SystemPath = Replace(Fn_Dir,"install/","")
		If Fn_Port &""<>"" Then
			Pv_Domain = Fn_Domain &":"& Fn_Port
		Else
			Pv_Domain = Fn_Domain
		End If
		Pv_SiteURL    = Fn_HttpStr & Pv_Domain
		Pv_SystemPath = Fn_SystemPath
	End Function
	
	'插入系统Ctl\Action
	Function InsertSystemCtlAction(ByVal BV_ID,ByVal BV_Ctl,ByVal BV_Action,ByVal BV_CtlName,ByVal BV_ActionName,ByVal BV_Orders)
		KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"SystemCtlAction] (ID,[SystemCtl],[SystemAction],[SystemCtlName],[SystemActionName],[Orders]) VALUES ("& BV_ID &",'"& BV_Ctl &"','"& BV_Action &"','"& BV_CtlName &"','"& BV_ActionName &"',"& BV_Orders &")"
	End Function
	
	'插入地区数据
	Function InsertRegion(ByVal BV_ID,ByVal BV_Package,ByVal BV_P_Region_ID,ByVal BV_Region_Path,ByVal BV_Region_Grade,ByVal BV_Local_Name,ByVal BV_En_Name,ByVal BV_P_1,ByVal BV_P_2,ByVal BV_OrderNum,ByVal BV_Disabled)
		BV_P_Region_ID = KnifeCMS.IIF(KnifeCMS.Data.IsNul(BV_P_Region_ID),"NUll",BV_P_Region_ID)
		BV_En_Name     = KnifeCMS.IIF(KnifeCMS.Data.IsNul(BV_En_Name),"NUll",BV_En_Name)
		BV_P_1         = KnifeCMS.IIF(KnifeCMS.Data.IsNul(BV_P_1),"NUll",BV_P_1)
		BV_P_2         = KnifeCMS.IIF(KnifeCMS.Data.IsNul(BV_P_2),"NUll",BV_P_2)
		BV_OrderNum    = KnifeCMS.IIF(KnifeCMS.Data.IsNul(BV_OrderNum),"NUll",BV_OrderNum)
		KnifeCMS.DB.Execute "INSERT INTO ["& Pv_TablePre &"Regions] (ID, Package, P_Region_ID, Region_Path, Region_Grade, Local_Name, En_Name, P_1, P_2, OrderNum, Disabled) VALUES ("& BV_ID &",'"& BV_Package &"',"& BV_P_Region_ID &",'"& BV_Region_Path &"',"& BV_Region_Grade &",'"& BV_Local_Name &"',"& BV_En_Name &","& BV_P_1 &","& BV_P_2 &","& BV_OrderNum &",'"& BV_Disabled &"')"
	End Function
	
	Sub CheckDataBaseAndSaveConfig()
		'检查信息是否正确
		Pv_SiteName    = KnifeCMS.Data.HTMLEncode(Trim(KnifeCMS.GetForm("post","sitename")))
		Pv_Domain      = Trim(KnifeCMS.GetForm("post","domain"))
		Pv_SystemPath  = Trim(KnifeCMS.GetForm("post","systempath"))
		If KnifeCMS.Data.IsNul(Pv_SiteName) Then Alert "网站名称","goback" : Pv_CanDoNext = False : Exit Sub
		If KnifeCMS.Data.IsNul(Pv_Domain) Then Alert "网站域名不能为空","goback" : Pv_CanDoNext = False : Exit Sub
		If KnifeCMS.Data.IsNul(Pv_SystemPath) Then Alert "系统路径不能为空","goback" : Pv_CanDoNext = False : Exit Sub
		If KnifeCMS.Data.IsNul(Pv_Sysuser_Username) Then Alert "站长用户名不能为空","goback" : Pv_CanDoNext = False : Exit Sub
		If KnifeCMS.Data.IsNul(Pv_Sysuser_Password) Then Alert "站长用户密码不能为空","goback" : Pv_CanDoNext = False : Exit Sub
		If Len(Pv_Sysuser_Password)<6 Then Alert "站长用户密码不能小于6位","goback" : Pv_CanDoNext = False : Exit Sub
		If Pv_Sysuser_Password<>Pv_Sysuser_Repassword Then Alert "两次输入的站长用户密码不一致","goback" : Pv_CanDoNext = False : Exit Sub
		If KnifeCMS.Data.IsNul(Pv_Sysuser_Email) Then Alert "站长Email不能为空","goback" : Pv_CanDoNext = False : Exit Sub
		If Not(KnifeCMS.Data.IsEmail(Pv_Sysuser_Email)) Then Alert "站长Email不正确","goback" : Pv_CanDoNext = False : Exit Sub
		
		'检查数据库配置信息是否正确
		On Error Resume Next
		Dim Fn_ObjConn,Fn_Error,Fn_ObjError,Fn_ErrorString
		Select Case  Pv_DB_Type
			Case 0
				If KnifeCMS.Data.IsNul(Pv_DB_AccessFileName) Then Alert "ACCESS文件名不能为空","goback" : Pv_CanDoNext = False : Exit Sub
				Pv_DB_AccessFilePath = "data/"& Pv_DB_AccessFileName
				Pv_DB_Server   = ""
				Pv_DB_Port     = ""
				Pv_DB_Name     = ""
				Pv_DB_Username = ""
				Pv_DB_Password = ""
				If Not(KnifeCMS.FSO.FileExists(Server.MapPath(Pv_SystemPath & Pv_DB_AccessFilePath))) Then
					If Not(CreateDataBase(Pv_DB_Name)) Then
						If LCase(Right(Pv_DB_AccessFileName,4))=".mdb" Or LCase(Right(Pv_DB_AccessFileName,6))=".accdb" Then
							Echo "<div style='color:#fff; padding:20px;'>无法创建ACCESS数据库文件"& Pv_DB_AccessFilePath &"，请确认您的服务器是否支持Access并且有创建Access数据库的权限！</div>" : Response.Flush()
							Alert "无法创建ACCESS数据库文件"& Pv_DB_AccessFilePath &"，请确认您的服务器是否支持Access并且有创建Access数据库的权限！","goback"
						Else
							Echo "<div style='color:#fff; padding:20px;'>无法创建ACCESS数据库文件"& Pv_DB_AccessFilePath &",请将ACCESS文件名改为.mdb或.accdb后缀(如:"& KnifeCMS.Data.ReplaceString(Pv_DB_AccessFileName,".asp",".mdb")&")再试试!</div>" : Response.Flush()
							Alert "无法创建ACCESS数据库文件"& Pv_DB_AccessFilePath &",请将ACCESS文件名改为.mdb或.accdb后缀(如:"& KnifeCMS.Data.ReplaceString(Pv_DB_AccessFileName,".asp",".mdb")&")再试试!","goback"
						End If
						Pv_CanDoNext = False
						Exit Sub
					End If
				End If
			Case 1
				If KnifeCMS.Data.IsNul(Pv_DB_Server) Then Alert "数据库地址不能为空","goback" : Pv_CanDoNext = False : Exit Sub
				If KnifeCMS.Data.IsNul(Pv_DB_Name) Then Alert "数据库名称不能为空","goback" : Pv_CanDoNext = False : Exit Sub
				
				Dim Fn_DB_Server : Fn_DB_Server = Pv_DB_Server
				If Not(KnifeCMS.Data.IsNul(Pv_DB_Port)) Then Fn_DB_Server = Fn_DB_Server &","& Pv_DB_Port
				If KnifeCMS.Data.IsNul(Pv_DB_Username) Then
					Pv_ConnStr            = "Provider=SQLOLEDB;Data Source="& Fn_DB_Server &";Initial Catalog="& Pv_DB_Name &";Integrated Security=SSPI;"
					Pv_CreateNewDBConnStr = "Provider=SQLOLEDB;Data Source="& Fn_DB_Server &";Initial Catalog=;Integrated Security=SSPI;"
				Else
					Pv_ConnStr            = "Provider=SQLOLEDB;Data Source="& Fn_DB_Server &";Initial Catalog="& Pv_DB_Name &";User Id="& Pv_DB_Username &";Password="& Pv_DB_Password &";"
					Pv_CreateNewDBConnStr = "Provider=SQLOLEDB;Data Source="& Fn_DB_Server &";Initial Catalog=;User Id="& Pv_DB_Username &";Password="& Pv_DB_Password &";"
				End If
				Set Fn_ObjConn=Server.CreateObject("ADODB.Connection")
				Fn_ObjConn.ConnectionTimeout=3
				Fn_ObjConn.Open Pv_ConnStr
				
				If Fn_ObjConn.Errors.count>0 Then
					Set Fn_ObjError = Fn_ObjConn.Errors(0)
					Fn_ErrorString = "\n\n错误信息：\n"
					Fn_ErrorString = Fn_ErrorString &"Error.Description:"& Fn_ObjError.Description &"\n"
					Fn_ErrorString = Fn_ErrorString &"Error.HelpContext:"& Fn_ObjError.HelpContext &"\n"
					Fn_ErrorString = Fn_ErrorString &"Error.HelpFile:"& Fn_ObjError.HelpFile &"\n"
					Fn_ErrorString = Fn_ErrorString &"Error.NativeError:"& Fn_ObjError.NativeError &"\n"
					Fn_ErrorString = Fn_ErrorString &"Error.Number:"& Fn_ObjError.Number &"\n"
					Fn_ErrorString = Fn_ErrorString &"Error.Source:"& Fn_ObjError.Source &"\n"
					Fn_ErrorString = Fn_ErrorString &"Error.SQLState:"& Fn_ObjError.SQLState &"\n"
					Fn_ErrorString = KnifeCMS.Data.ReplaceString(Fn_ErrorString,"""","\""")
					Fn_Error=KnifeCMS.Data.CLng(Fn_ObjConn.Errors(0).NativeError)
					Set Fn_ObjError = Nothing
					Fn_ObjConn.Close
					Set Fn_ObjConn = Nothing
					Select Case Fn_Error
						Case "17"
							Alert "无法连接 SQL Server 数据库服务器“"& Fn_DB_Server &"”，请检查数据库服务器地址是否正确。"& Fn_ErrorString,"goback"
							Pv_CanDoNext = False
							Exit Sub
						Case "4060"
						'数据库名称不存在
							If Not(CreateDataBase(Pv_DB_Name)) Then
								Alert "无法创建数据库["& Pv_DB_Name &"]"& Fn_ErrorString,"goback"
								Pv_CanDoNext = False
								Exit Sub
							End If
						Case "18456"
						'用户无法登陆
							Alert "无法连接 SQL Server 数据库服务器“"& Fn_DB_Server &"”，请检查数据库帐号或密码是否正确。"& Fn_ErrorString,"goback"
							Pv_CanDoNext = False
							Exit Sub
						Case Else
							Alert "无法连接 SQL Server 数据库服务器“"& Fn_DB_Server &"”，请检查 SQL Server 数据库是否已正确安装。"& Fn_ErrorString,"goback"
							Pv_CanDoNext = False
							Exit Sub
					End Select
					
				Else
					
					Fn_ObjConn.Close
					Set Fn_ObjConn = Nothing
				End If
				
		End Select
		If Err.Number <> 0 Then
			Err.Clear
		End If
		'保存配置到config.main.asp
		Dim Fn_Text
		Fn_Text = KnifeCMS.Read(Pv_SystemPath & "config/config.main.asp")
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"Sys_IsInstalled([\s]*?)=([\s]*?)(\d+?)","Sys_IsInstalled = 1")
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteURL([\s]*?)=([\s]*?)(""\S*"")","SiteURL = """& Pv_SiteURL &"""")
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"Domain([\s]*?)=([\s]*?)(""\S*"")","Domain = """& Pv_Domain &"""")
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SystemPath([\s]*?)=([\s]*?)(""\S*"")","SystemPath = """& Pv_SystemPath &"""")
		
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"DB_Type([\s]*?)=([\s]*?)(\d+?)","DB_Type = "& Pv_DB_Type &"")
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"DB_AccessFilePath([\s]*?)=([\s]*?)(""\S*"")","DB_AccessFilePath = """& Pv_DB_AccessFilePath &"""")
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"DB_Server([\s]*?)=([\s]*?)(""\S*"")","DB_Server = """& Pv_DB_Server &"""")
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"DB_Port([\s]*?)=([\s]*?)(""\S*"")","DB_Port = """& Pv_DB_Port &"""")
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"DB_Name([\s]*?)=([\s]*?)(""\S*"")","DB_Name = """& Pv_DB_Name &"""")
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"DB_Username([\s]*?)=([\s]*?)(""\S*"")","DB_Username = """& Pv_DB_Username &"""")
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"DB_Password([\s]*?)=([\s]*?)(""\S*"")","DB_Password = """& Pv_DB_Password &"""")
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"TablePre([\s]*?)=([\s]*?)(""\S*"")","TablePre = """& Pv_TablePre &"""")
		
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"EncryptionKey([\s]*?)=([\s]*?)(""\S*"")","EncryptionKey = """& Pv_Encryptionkey &"""")
		
		'更新logo地址
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteLogo([\s]*?)=([\s]*?)(""\S*"")","SiteLogo = """& Pv_SystemPath & TemplateRoot & TemplateName &"/images/logo.jpg""")
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteMyhomeLogo([\s]*?)=([\s]*?)(""\S*"")","SiteMyhomeLogo = """& Pv_SystemPath & TemplateRoot & TemplateName &"/themes/default/logo.gif""")
		
		'更新网站名称
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteName([\s]*?)=([\s]*?)(""\S*"")","SiteName = """& Pv_SiteName &"""")
		
		KnifeCMS.AES.EncryptKey = Pv_Encryptionkey
		If KnifeCMS.FSO.SaveTextFile(Server.MapPath(Pv_SystemPath &"config/config.main.asp"),Fn_Text)=True Then
			Pv_CanDoNext = True
		Else
			Pv_CanDoNext = False
			Alert "无法保存配置信息,请检查config/config.main.asp是否有写入权限。","goback" : Pv_CanDoNext = False : Exit Sub
		End If
	End Sub
	
	'创建一个数据库
	Private Function CreateDataBase(ByVal BV_DB_Name)
		On Error Resume Next
		Dim Fn_ObjConn
		Select Case  Pv_DB_Type
			Case 0
				SET Fn_ObjConn=Server.CreateObject("ADOX.Catalog")
				Fn_ObjConn.Create "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Server.Mappath( Pv_SystemPath & Pv_DB_AccessFilePath)
				SET Fn_ObjConn=Nothing
			Case 1
				Set Fn_ObjConn=Server.CreateObject("ADODB.Connection")
				Fn_ObjConn.ConnectionTimeout=3
				Fn_ObjConn.Open Pv_CreateNewDBConnStr
				Fn_ObjConn.Execute "IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'"& BV_DB_Name &"') DROP DATABASE ["& BV_DB_Name &"]"
				Fn_ObjConn.Execute "CREATE DATABASE ["& BV_DB_Name &"]"
				Fn_ObjConn.Close
				Set Fn_ObjConn = Nothing
		End Select
		If Err.Number <> 0 Then 
			Err.Clear
			CreateDataBase = False
		Else
			CreateDataBase = True
		End If
	End Function
	
	Function OK()
		Echo "<span class=""cue_ok"">支持</span>"
	End Function
	
	Function Wrong()
		Echo "<span class=""cue_err"">不支持</span>"
	End Function
	
	Function Writeable()
		Echo "<span class=""cue_ok"">可写</span>"
	End Function
	Function UnWriteable()
		Echo "<span class=""cue_err"">不可写</span>"
	End Function
	
	Sub CreateOK(ByVal Bv_Table)
		Echo "创建数据表 "& Bv_Table &" ... 成功<br>" : Response.Flush()
	End Sub
	Sub CreateErr(ByVal Bv_Table)
		Pv_CanDoNext = False
		Echo "<font style='color:#FF0000;'>创建数据表 "& Bv_Table &" ... 失败</font><br>" : Response.Flush()
	End Sub
	
	
	Sub Header()
	%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>KnifeCMS系统安装向导</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link rel="stylesheet" type="text/css" href="css/global.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />
    <script type="text/javascript" src="../include/javascript/kfc.js"></script>
    <script type="text/javascript" src="../plugins/jquery/jquery.js"></script>
	<script type="text/javascript">
	// table 换行变色 选中变色
	function TrBgChange(AreaId,OddClass,EvenClass){var trs=document.getElementById(AreaId).getElementsByTagName("tr");for(var i=1;i<trs.length;i++){trs[i].className=(trs[i].sectionRowIndex%2==0)?OddClass:EvenClass;}}
	</script>
	</head>
	<body>
	<div id="abody">
    <div id="bbody">
        <div id="header"><div><a href="http://www.knifecms.com/" target="_blank"><img src="images/logo.gif" /></a></div></div>
	<%
	End Sub
	
	Sub Footer
	%>
    <div id="footer"><!--<div>Powered By KnifeCMS 开放式互联网商务建站系统</div>--></div>
    </div>
    </div>
    </body>
    </html>
	<%
	End Sub
	
	'安装须知
	Sub Start() %>
		<div id="mbody">
            <div class="leftpanel">
                <ul class="step">
                <li class="installing"><b></b>安装须知</li>
                <li><b></b>阅读安装协议</li>
                <li><b></b>运行环境检测</li>
                <li><b></b>填写数据信息</li>
                <li><b></b>安装详细信息</li>
                <li><b></b>安装完成</li>
                </ul>
            </div>
            <div class="mainpanel">
                <div class="rightpanel">
                    <div class="readbefore">
                        <h5>一. 运行环境需求</h5>
                        <p class="pl20 pt5">
                        1. 可用的 Windows 服务器（如IIS等）<br />
                        2. 开启父路径<br />
                        3. 确认站点默认文档设置中有一个为index.asp<br />
                        3. 确认服务器已安装ACCESS 2003 或 MSSQL 2000 及以上版本<br />
                        
                        </p>
                        <h5 class="mt10">二. 先确认以下目录和文件属性为可写模式。</h5>
                        <p class="pl20 pt5">
                        attachment/<br />
                        config/config.main.asp<br />
                        data/<br />
                        data/backup/<br />
                        data/cache/<br />
                        data/javascript/region.json.js<br />
                        template/<br />
                        </p>
                    </div>
                </div>
                <div class="submitarea">
                <input type="button" class="button" onclick="window.location.href='?action=step1'" value="开始安装" />
                </div>
            </div>
        </div>
	<%
	End Sub
	
	'阅读安装协议
	Sub Step1() %>
		<div id="mbody">
            <div class="leftpanel">
                <ul class="step">
                <li class="installed"><b></b>安装须知</li>
                <li class="installing"><b></b>阅读安装协议</li>
                <li><b></b>运行环境检测</li>
                <li><b></b>填写数据信息</li>
                <li><b></b>安装详细信息</li>
                <li><b></b>安装完成</li>
                </ul>
            </div>
            <div class="mainpanel">
                <div class="rightpanel">
                    <div class="licensebox">
<div class="license">
<h1>KnifeCMS 授权协议</h1>
<p>本授权协议适用且仅适用于 KnifeCMS V2.X.X 版本，KnifeCMS官方拥有对本授权协议的最终解释权。</p>

<h3>I. 协议许可的权利</h3>
<ol>
<li>您可以在完全遵守本最终用户授权协议的基础上使用本软件。</li>
<li>您可以在协议规定的约束和限制范围内修改 KnifeCMS 源代码或界面风格以适应您的网站要求。</li>
<li>您拥有使用本软件构建的网站中全部内容的所有权，并独立承担与之相关的法律义务。</li>
</ol>

<h3>II. 协议规定的约束和限制</h3>
<ol>
<li>若要用于商业用途，请务必保留页脚处的 KnifeCMS 名称，不能隐藏、清除或修改。</li>
<li>若要对源码进行修改，请务必以论坛发帖或电子邮件的形式告知KnifeCMS官方修改目的及改动说明。</li>
<li>如果您未能遵守本协议的条款，您的授权将被终止，所被许可的权利将被收回，并承担相应法律责任。</li>
</ol>

<h3>III. 有限担保和免责声明</h3>
<ol>
<li>本软件及所附带的文件是作为不提供任何明确的或隐含的赔偿或担保的形式提供的。</li>
<li>KnifeCMS官方不对使用本软件构建的网站中的文章、商品和其他信息承担责任。</li>
</ol>
</div>
                    </div>
                </div>
                <div class="submitarea">
                <input type="button" class="button" onclick="window.location.href='?action=start'" value="<< 返回" />
                <input type="button" class="button" onclick="window.location.href='?action=step2'" value="同意协议,下一步 >>" />
                </div>
                
            </div>
        </div>
	<%
	End Sub
	
	'运行环境检测
	Sub Step2() %>
		<div id="mbody">
            <div class="leftpanel">
                <ul class="step">
                <li class="installed"><b></b>安装须知</li>
                <li class="installed"><b></b>阅读安装协议</li>
                <li class="installing"><b></b>运行环境检测</li>
                <li><b></b>填写数据信息</li>
                <li><b></b>安装详细信息</li>
                <li><b></b>安装完成</li>
                </ul>
            </div>
            <div class="mainpanel">
                <div class="rightpanel">
                    <div>
                    <table border="0" cellpadding="0" cellspacing="0" class="listTable">
                    <tr class="top"><th colspan="2">服务器信息</th></tr>
                    <tr class="data"><td class="first"><div style="width:121px;">操作系统</div></td><td><div style="width:480px;"><%=Pv_OS%></div></td></tr>
                    <tr class="data"><td class="first">web 服务器</td><td><%=Request.ServerVariables("SERVER_SOFTWARE")%></td></tr>
                    <tr class="data"><td class="first">服务器地址</td>
                                     <td><%=Request.ServerVariables("SERVER_NAME")%>:<%=Request.ServerVariables("SERVER_PORT")%> (IP:<%=Request.ServerVariables("LOCAL_ADDR")%>)</td>
                    </tr>
                    <tr class="data"><td class="first">服务器时间</td><td><%=now()%></td>
                    </tr>
                    <tr class="data"><td class="first">脚本超时时间</td><td><%=Server.ScriptTimeout%> 秒</td>
                    </tr>
                    <tr class="data"><td class="first">服务器脚本引擎</td>
                                     <td><%=ScriptEngine & "/"& ScriptEngineMajorVersion &"."&ScriptEngineMinorVersion&"."& ScriptEngineBuildVersion %> , <%="JScript/" & getjver()%></td>
                    </tr>
                    <tr class="data"><td class="first">服务器CPU通道数</td><td><%=Pv_CPUs%> 个</td>
                    </tr>
                    </table>
                    </div>
                    <div style="margin-top:5px;">
                    <table border="0" cellpadding="0" cellspacing="0" class="listTable">
                    <tr class="top"><th><div style="width:320px;">系统组件</div></th>
                                    <th><div style="width:80px;">当前状态</div></th>
                                    <th><div style="width:190px;">所需状态</div></th>
                                    </tr>
                    <tr class="data"><td class="first">Adodb.Recordset</td>
                                     <td><% if KnifeCMS.IsObjInstalled("Adodb.Recordset") Then OK : Else Pv_CanDoNext=False : Wrong : End If %></td>
                                     <td>支持</td>
                    </tr>
                    <tr class="data"><td class="first">Adodb.Connection<p class="exinfo">(ADO 数据对象)</p></td>
                                     <td><% if KnifeCMS.IsObjInstalled("Adodb.Connection") Then OK : Else Pv_CanDoNext=False : Wrong : End If %></td>
                                     <td>支持</td>
                    </tr>
                    <tr class="data"><td class="first">Adodb.Stream<p class="exinfo">(ADO 数据流对象,常见被用在无组件上传程序中)</p></td>
                                     <td><% if KnifeCMS.IsObjInstalled("Adodb.Stream") Then OK : Else Pv_CanDoNext=False : Wrong : End If %></td>
                                     <td>支持</td>
                    </tr>
                    <tr class="data"><td class="first">Scripting.Dictionary<p class="exinfo">(保存数据键和项目对的对象)</p></td>
                                     <td><% if KnifeCMS.IsObjInstalled("Scripting.Dictionary") Then OK : Else Pv_CanDoNext=False : Wrong : End If %></td>
                                     <td>支持</td>
                    </tr>
                    <tr class="data"><td class="first">Microsoft.XMLHTTP<p class="exinfo">(Http 组件, 在采集系统中必须用到)</p></td>
                                     <td><% if KnifeCMS.IsObjInstalled("Microsoft.XMLHTTP") Then OK : Else Pv_CanDoNext=False : Wrong : End If %></td>
                                     <td>支持</td>
                    </tr>
                    <tr class="data"><td class="first">Scripting.FileSystemObject<p class="exinfo">(FSO 文件系统管理、文本文件读写)</p></td>
                                     <td><% if KnifeCMS.IsObjInstalled("Scripting.FileSystemObject") Then OK : Else Pv_CanDoNext=False : Wrong : End If %></td>
                                     <td>支持</td>
                    </tr>
                    <tr class="data"><td><div class="p5">JMail.SmtpMail<p class="exinfo">(Dimac JMail 邮件收发)</p></div></td>
                                         <td><% if KnifeCMS.IsObjInstalled("JMail.SmtpMail") Then OK : Else Wrong : End If %></td>
                                         <td>建议支持</td>
                    </tr>
                    </table>
                    </div>
                    <div style="margin-top:5px;">
                    <table border="0" cellpadding="0" cellspacing="0" class="listTable">
                    <tr class="top"><th><div style="width:320px;">目录文件</div></th>
                                    <th><div style="width:80px;">当前状态</div></th>
                                    <th><div style="width:190px;">所需状态</div></th>
                                    </tr>
                    <tr class="data"><td class="first">attachment/</td>
                                     <td><% if KnifeCMS.FSO.FolderWriteable(Server.MapPath("../attachment/")) Then Writeable : Else Pv_CanDoNext=False : UnWriteable : End If %></td>
                                     <td>可写</td>
                    </tr>
                    <tr class="data"><td class="first">config/config.main.asp</td>
                                     <td><% if KnifeCMS.FSO.FileWriteable(Server.MapPath("../config/config.main.asp")) Then Writeable : Else Pv_CanDoNext=False : UnWriteable : End If %></td>
                                     <td>可写</td>
                    </tr>
                    <tr class="data"><td class="first">data/</td>
                                     <td><% if KnifeCMS.FSO.FolderWriteable(Server.MapPath("../data/")) Then Writeable : Else Pv_CanDoNext=False : UnWriteable : End If %></td>
                                     <td>可写</td>
                    </tr>
                    <tr class="data"><td class="first">data/backup/</td>
                                     <td><% if KnifeCMS.FSO.FolderWriteable(Server.MapPath("../data/backup/")) Then Writeable : Else Pv_CanDoNext=False : UnWriteable : End If %></td>
                                     <td>可写</td>
                    </tr>
                    <tr class="data"><td class="first">data/cache/</td>
                                     <td><% if KnifeCMS.FSO.FolderWriteable(Server.MapPath("../data/cache/")) Then Writeable : Else Pv_CanDoNext=False : UnWriteable : End If %></td>
                                     <td>可写</td>
                    </tr>
                    <tr class="data"><td class="first">data/javascript/region.json.js</td>
                                     <td><% if KnifeCMS.FSO.FileWriteable(Server.MapPath("../data/javascript/region.json.js")) Then Writeable : Else Pv_CanDoNext=False : UnWriteable : End If %></td>
                                     <td>可写</td>
                    </tr>
                    <tr class="data"><td class="first">template/</td>
                                     <td><% if KnifeCMS.FSO.FolderWriteable(Server.MapPath("../template/")) Then Writeable : Else Pv_CanDoNext=False : UnWriteable : End If %></td>
                                     <td>可写</td>
                    </tr>
                    </table>
                    </div>
                </div>
                <div class="submitarea">
                <input type="button" class="button" onclick="window.location.href='?action=step1'" value="<< 返回" />
				<% If Pv_CanDoNext Then %>
				<input type="button" class="button" onclick="window.location.href='?action=step3'" value="下一步 >>" />
				<% Else %>
				<input type="button" class="button" onclick="window.location.href='?action=step2'" value="刷新，重新检测" />
				<% End If %>
                </div>
            </div>
        </div>
		<script language="JScript" runat="server">
        //获取JScript 版本
        function getJVer(){return ScriptEngineMajorVersion() +"."+ScriptEngineMinorVersion()+"."+ ScriptEngineBuildVersion();}
        </script>
	<%
	End Sub
	
	'填写数据库信息与站长信息
	Sub Step3() %>
		<div id="mbody">
            <div class="leftpanel">
                <ul class="step">
                <li class="installed"><b></b>安装须知</li>
                <li class="installed"><b></b>阅读安装协议</li>
                <li class="installed"><b></b>运行环境检测</li>
                <li class="installing"><b></b>填写数据信息</li>
                <li><b></b>安装详细信息</li>
                <li><b></b>安装完成</li>
                </ul>
            </div>
            <div class="mainpanel">
                <form name="submitForm" action="index.asp?action=step4" method="post">
                <div class="rightpanel">
                    <div class="step4_box">
                        <div class="cuetitle">填写网站基本信息<span style="font-weight:normal;">(默认系统会自动获取不用修改，如有错请修改.)</span></div>
                        <table border="0" cellpadding="0" cellspacing="0" class="infoTable">
                        <tr><td class="titletd">网站名称:</td><td class="infotd">
                            <input type="text" class="text" name="sitename" value="<%=SiteName%>" />
                            <label class="normal"></label>
                            </td>
                        </tr>
                        <tr><td class="titletd">网站域名:</td><td class="infotd">
                            <input type="text" class="text" name="domain" value="<%=Pv_Domain%>" />
                            <label class="normal">例: www.knifecms.com</label>
                            </td>
                        </tr>
                        <tr><td class="titletd">网站网址:</td><td class="infotd">
                            <input type="text" class="text" name="siteurl" value="<%=Pv_SiteURL%>" />
                            <label class="normal">以http开头，后面不能加"/" 例: http://www.knifecms.com</label>
                            </td>
                        </tr>
                        <tr><td class="titletd">系统路径:</td><td class="infotd">
                            <input type="text" class="text" name="systempath" value="<%=Pv_SystemPath%>" />
                            <label class="normal">"/"开头并以"/"结束 例: /systempath/ 或 /</label>
                            </td>
                        </tr>
                        </table>
                    </div>
                    <div class="step4_box">
                        <div class="cuetitle">填写数据库信息</div>
                        <div>
                        <table border="0" cellpadding="0" cellspacing="0" class="infoTable">
                        <tr><td class="titletd">选择数据库类型:</td><td class="infotd">
                            <span class="db_type_1"><input type="radio" name="db_type" id="db_type_1" checked="checked" value="0" /><span>Microsoft Access</span></span>
                            <span class="db_type_2"><input type="radio" name="db_type" id="db_type_2" value="1" /><span>Microsoft SQL Server</span></span>
                            </td>
                        </tr>
                        </table>
                        </div>
                        <div id="db_1">
                        <table border="0" cellpadding="0" cellspacing="0" class="infoTable">
                        <tr><td class="titletd">ACCESS文件名:</td><td class="infotd">
                            <input type="text" class="text" name="db_accessfilename" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9a-zA-Z-_.#$]*/g','maxLength:50'))" value="<%=Pv_DB_AccessFileName%>" />
                            <label class="normal">ACCESS数据库文件名，默认在data文件夹下</label>
                            </td>
                        </tr>
                        </table>
                        </div>
                        <div id="db_2" style="display:none;">
                        <table border="0" cellpadding="0" cellspacing="0" class="infoTable">
                        <tr><td class="titletd">数据库服务器:</td><td class="infotd">
                            <input type="text" class="text" name="db_server" value="<%=DB_Server%>" />
                            <label class="normal"></label>
                            </td>
                        </tr>
                        <tr><td class="titletd">数据库端口号:</td><td class="infotd">
                            <input type="text" class="text" name="db_port" value="" style="width:80px;" />
                            <label class="normal">针对特殊端口号则须填写，一般不用填写</label>
                            </td>
                        </tr>
                        <tr><td class="titletd">数据库名称:</td><td class="infotd">
                            <input type="text" class="text" name="db_name" value="<%=DB_Name%>" />
                            <label class="normal"></label>
                            </td>
                        </tr>
                        <tr><td class="titletd">数据库帐号:</td><td class="infotd">
                            <input type="text" class="text" name="db_username" value="<%=DB_Username%>" />
                            <label class="normal"></label>
                            </td>
                        </tr>
                        <tr><td class="titletd">数据库密码:</td><td class="infotd">
                            <input type="text" class="text" name="db_password" value="<%=DB_Password%>" />
                            <label class="normal"></label>
                            </td>
                        </tr>
                        </table>
                        </div>
                        <table border="0" cellpadding="0" cellspacing="0" class="infoTable">
                        <tr><td class="titletd">数据表前缀:</td><td class="infotd">
                            <input type="text" class="text" name="db_tablepre" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9a-zA-Z_]*/g','maxLength:20'))" value="<%=TablePre%>" />
                            <label class="normal">必须以字母开头，非必要请保持默认kfc_</label>
                            </td>
                        </tr>
                        </table>
                    </div>
                    <div class="step4_box">
                        <div class="cuetitle">填写站长信息</div>
                        <table border="0" cellpadding="0" cellspacing="0" class="infoTable">
                        <tr><td class="titletd">站长用户名:</td><td class="infotd">
                            <input type="text" class="text" name="sysuser_username" maxlength="20" value="admin" />
                            <label class="normal">0到20个字符，不含非法字符！</label>
                            </td>
                        </tr>
                        <tr><td class="titletd">用户密码:</td><td class="infotd">
                            <input type="password" class="text" name="sysuser_password" maxlength="20" value="" />
                            <label class="normal">6到20个字符</label>
                            </td>
                        </tr>
                        <tr><td class="titletd">确认密码:</td><td class="infotd">
                            <input type="password" class="text" name="sysuser_repassword" maxlength="20" value="" />
                            <label class="normal"></label>
                            </td>
                        </tr>
                        <tr><td class="titletd">Email:</td><td class="infotd">
                            <input type="text" class="text" name="sysuser_email" maxlength="50" value="" />
                            <label class="normal"></label>
                            </td>
                        </tr>
                        <tr><td class="titletd">加解密密钥:</td><td class="infotd">
                            <input type="text" class="text" name="encryptionkey" maxlength="32" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9a-zA-Z]*/g','maxLength:20'))" value="<%=Pv_Encryptionkey%>" />
                            <label class="normal">MD5/AES加解密的密钥，每个网站都应唯一设置，以防被人破解</label>
                            </td>
                        </tr>
                        </table>
                    </div>
                </div>
                <div class="submitarea" style="padding: 30px 0px 20px 110px;">
                <input type="button" class="button" onclick="window.location.href='?action=step2'" value="<< 返回" />
				<input type="button" class="button" id="submitinput" onclick="formSubmit();" value="下一步 >>" />
                </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
		var db_type_1 = document.getElementById("db_type_1");
		var db_type_2 = document.getElementById("db_type_2");
		db_type_1.onclick = function(){
			if(this.checked==true){
				document.getElementById("db_1").style.display = "";
				document.getElementById("db_2").style.display = "none";
			}
		}
		db_type_2.onclick = function(){
			if(this.checked==true){
				document.getElementById("db_1").style.display = "none";
				document.getElementById("db_2").style.display = "";
			}
		}
		function formSubmit(){
			var sitename = $("input[name='sitename']");
			var db_tablepre = $("input[name='db_tablepre']");
			var v_db_tablepre = $.trim(db_tablepre.val());
			var temp ="";
			var check = true;
			if($.trim(sitename.val())==""){
				check = false;
				alert("请填写网站名称!");
				sitename.focus();
			}
			if(check){
				if(v_db_tablepre==""){
					check = false;
					alert("数据表前缀不能为空!");
					db_tablepre.focus();
				}else{
					temp = v_db_tablepre.substr(0,1);
					temp = temp.replace(eval('/[^a-zA-Z]*/g'),"");
					if(temp==""){
						check = false;
						alert("数据表前缀必须以字母开头!");
						db_tablepre.focus();
					}
				}
			}
			if(check){
				$("form[name='submitForm']").submit();
			}
		}
        </script>
	<%
	End Sub
	Sub Step4()
	%>
		<script type="text/javascript">
		KnifeCMS.BodyScroll();
        </script>
		<div id="mbody">
            <div class="leftpanel">
                <ul class="step">
                <li class="installed"><b></b>安装须知</li>
                <li class="installed"><b></b>阅读安装协议</li>
                <li class="installed"><b></b>运行环境检测</li>
                <li class="installed"><b></b>填写数据信息</li>
                <li class="installing"><b></b>安装详细信息</li>
                <li><b></b>安装完成</li>
                </ul>
            </div>
            <div class="mainpanel">
                <div class="rightpanel">
                   <% Call CreateDBTable() %>
                </div>
                <div class="submitarea">
                <% If Pv_CanDoNext Then %>
                <input type="button" class="button" disabled="disabled" value="<< 返回" />
                <input type="button" class="button" onclick="window.location.href='?action=step5'" value="下一步 >>" />
                <% Else %>
				<input type="button" class="button" onclick="window.location.href='?action=step3'" value="重新安装" />
                <input type="button" class="button" disabled="disabled" value="下一步 >>" />
				<% End If %>
                </div>
            </div>
        </div>
        <script type="text/javascript">
		KnifeCMS.StopScroll=true;
        </script>
	<%
	End Sub
	Sub Step5()
	%>
		<div id="mbody">
            <div class="leftpanel">
                <ul class="step">
                <li class="installed"><b></b>安装须知</li>
                <li class="installed"><b></b>阅读安装协议</li>
                <li class="installed"><b></b>运行环境检测</li>
                <li class="installed"><b></b>填写数据信息</li>
                <li class="installed"><b></b>安装详细信息</li>
                <li class="installing"><b></b>安装完成</li>
                </ul>
            </div>
            <div class="mainpanel">
                <div class="rightpanel">
                   <p>您已成功完成了KnifeCMS的安装<br />点击"完成"按钮退出安装向导<br /><br /></p>
                   <p style="padding-bottom:10px;">进入<a href="../" target="_self">网站首页</a><br /></p>
                   <p>进入<a href="../<%=ManagerPath%>login.asp" target="_self">网站管理后台</a></p>
                </div>
                <div class="submitarea">
                <input type="button" class="button" onclick="window.location.href='../'" value="完成" />
                </div>
            </div>
        </div>
	<%
	End Sub
	
	
	'创建数据表
	Sub CreateDBTable()
		Dim Fn_ResultCT : Fn_ResultCT = 0
		Dim Fn_ResultAT : Fn_ResultAT = 0
		Dim Fn_SQL,Fn_i,Fn_Array,Fn_SubSystem
		Fn_Array = Array("about","news","product","case","help")
		Server.ScriptTimeOut = 1200
		
		KnifeCMS.DB.SystemPath  = Pv_SystemPath
		KnifeCMS.DB.DB_Type     = Pv_DB_Type
		Select Case Pv_DB_Type
		Case 0
		KnifeCMS.DB.DB_Server   = Pv_DB_AccessFilePath
		Case 1
		KnifeCMS.DB.DB_Server   = Pv_DB_Server
		End Select
		KnifeCMS.DB.DB_Port     = Pv_DB_Port
		KnifeCMS.DB.DB_Name     = Pv_DB_Name
		KnifeCMS.DB.DB_Username = Pv_DB_Username
		KnifeCMS.DB.DB_Password = Pv_DB_Password

		%><!--#include file="include.create_dbtable.asp"--><%
		
		Echo "正在安装系统数据，安装时间根据你的服务器由几秒到几分钟不等，请耐心等候......<br>" : Response.Flush()
		If Pv_CanDoNext Then Call InsertDBData()
		If Pv_CanDoNext Then Call KnifeCMS.FSO.SaveTextFile(Server.MapPath(Pv_SystemPath &"data/install.lock"),"install_lock")
		Echo "<br>系统数据安装结束.<br>" : Response.Flush()
	End Sub
	
	'插入数据
	Sub InsertDBData()
	    Dim Fn_Rs
		Dim Fn_Sysuser_Username : Fn_Sysuser_Username = Pv_Sysuser_Username
		Dim Fn_Sysuser_Password : Fn_Sysuser_Password = KnifeCMS.ParsePassWord(Pv_Sysuser_Password)
		Dim Fn_Sysuser_Email    : Fn_Sysuser_Email    = Pv_Sysuser_Email
		Dim Fn_NowTime          : Fn_NowTime          = SysTime
		
		%>
		<!--#include file="include.insert_systemdata.asp"-->
		<!--#include file="include.insert_systemctlaction.asp"-->
		<!--#include file="include.insert_region.asp"-->
		<%
	End Sub
End Class
%>
