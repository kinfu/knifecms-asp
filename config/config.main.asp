﻿<%@ LANGUAGE = VBScript CodePage = 65001%>
<%
Option Explicit
Response.Buffer = True
Sub Init()
	On Error Resume Next
	Response.Charset="UTF-8"
	If Err.Number <> 0 Then Err.Clear() : Response.Write("<!--error:Response.Charset=""UTF-8""-->")
End Sub
Call Init()

'**
'配置主文件
'filename: config.main.asp
'copyright: KFC
'**
%>
<!--#include file="config.system.asp"-->
<%
Const Sys_IsInstalled = 1 '是否已安装：0未安装,1已安装
Const SiteURL = "http://localhost:81" '网站网址,后面不用加"/"
Const Domain = "localhost81" '网站域名
Const SystemPath = "/" '系统路径,默认为"/"; 如:"/mycompany/" 或 "/"
Const ManagerPath = "admin/" '后台路径,前面不用加"/",默认为"administrator/"
Const UploadFile_Pre = "" '上传文件名/图片名前缀[默认为空];
Const IsDeBug = 1 '是否开启调试模式, 默认为不开启为0 , 当调式系统的时候开启为1
Const ShowSQL = 1 '是否显示SQL查询语句，默认为不显示0; 只有开启调试模式时这项设置才有效

Const MoneySb = "￥"

'系统采用XML版本设置
Const MsxmlVersion=".3.0"

'**数据库配置**
'数据库类型:0为ACCESS,1为MSSQL,2为MYSQL,3为ORACLE
Const DB_Type = 0
'ACCESS数据库文件路径
Const DB_AccessFilePath = "data/db20140126114543.asp"
'数据库服务器地址,默认为(local)
Const DB_Server = ""
'数据库端口号,默认为空
Const DB_Port = ""
'数据库名称
Const DB_Name = ""
'数据库用户名
Const DB_Username = ""
'数据库用户密码
Const DB_Password = ""
'数据表前缀(默认为"kfc_")
Const TablePre = "kfc_" 

Const EncryptionKey = "knifecms" '加解密密钥(MD5 AES 加解密的密钥,每个网站都应唯一设置，以防被人破解)

Dim FileSuffix,IsCache,CacheFlag,CacheTime,RunMode,TemplateRoot,TemplateName,TemplatePath,TemplateHTMLFolder
FileSuffix = ".htm"
IsCache = 0 '是否开启缓存1:开启,0:不开启
CacheTime = 86400 '秒
CacheFlag = "knifecms_" '缓存前缀
RunMode = "dynamic" '运行模式{static:静态,dynamic:动态}
TemplateRoot = "template/" '模板文件夹主目录
TemplateName = "default" '模板文件夹
TemplatePath = SystemPath & TemplateRoot & TemplateName &"/" '[不可修改]模板路径,最后一定以"/"结尾
TemplateHTMLFolder = "html" '模板html文件夹


'**以下配置变量由系统自动生成,不熟悉系统者请不要随便修改和删除**
Dim SeoTitle,SeoUrl,MetaKeywords,MetaDescription
Dim SiteTitle,SiteMetaKeywords,SiteMetaDescription
Dim ShopTitle,ShopMetaKeywords,ShopMetaDescription
Dim SiteFilePathType
Dim SiteLanguage,SiteAdminLanguage,SystemConfig,CloseModuleVote,CloseModuleShop,CloseTWriter,CloseUFriends,CloseUGuestbook,SiteFilterWords,SiteFilterIP,SiteNotice,SiteName,SiteLogo,SiteMyhomeLogo,SiteBriefIntroduction,SiteIcp,SiteCopyright,SiteStatisticsCode,SiteCompanyName,SiteAddress,SitePostcode,SiteTelephone,SiteFax,SiteMobilePhone,SiteEmail,SiteStatus,SiteClosedReasons,SiteIsTax,SiteTaxRate,GoodsConsultSet,GoodsCommentSet,IsGoodsConsultCheck,IsGoodsCommentCheck,ContentCommentSet,IsContentCommentCheck
Dim MailConfig

SiteLanguage = "zh-cn"
SiteAdminLanguage = "zh-cn"
SystemConfig = "UserCookieType=0;AdminCookieType=0;OnlineService={""onlineservice_show"":""1"",""onlineservice"":[{""name"":""%u5728%u7EBF%u54A8%u8BE2%u70ED%u7EBF"",""tel"":""400-0000-0000"",""qq"":"""",""wangwang"":"""",""skype"":"""",""text"":""""},{""name"":""KnifeCMS%u5EFA%u7AD9%u54A8%u8BE2"",""tel"":"""",""qq"":""710633139"",""wangwang"":"""",""skype"":"""",""text"":""""},{""name"":""KnifeCMS%u5EFA%u7AD9%u8BA8%u8BBA"",""tel"":"""",""qq"":"""",""wangwang"":"""",""skype"":"""",""text"":""QQ%u7FA4%3A%20374069166""}],""kf_show"":""1"",""kf"":""""}"
CloseModuleVote = 0
CloseModuleShop = 0
CloseTWriter = 0
CloseUFriends = 0
CloseUGuestbook = 0
SiteFilePathType = "relative"'图片/文件路径方式 relative:采用相对路径,absolute:采用绝对路径
SiteFilterWords = ""
SiteFilterIP = ""
SiteNotice = "祝贺本网站安装成功！"
SiteName = "智富者科技"
SiteLogo = "/template/default/images/logo.jpg"
SiteMyhomeLogo = "/template/default/themes/default/logo.gif"
SiteBriefIntroduction = "" '网站简介
SiteIcp = "京ICP备13009267号" 'ICP证书号
SiteCopyright = "" '底部版权信息
SiteStatisticsCode = "" '网站统计代码
SiteCompanyName = "" '企业名称
SiteAddress = "" '企业详细地址
SitePostcode = "" '邮编
SiteTelephone = "" '固定电话
SiteFax = "" '传真
SiteMobilePhone = "" '移动电话
SiteEmail = "" 'Email
SiteStatus = 1 '网站状态{0:关闭 1:开启}
SiteClosedReasons = "网站暂时关闭!" '网站关闭原因
SiteTitle = "网站首页"
SiteMetaKeywords = "KnifeCMS,企业建站系统,网店系统,CMS,国内CMS,开源CMS系统,内容管理系统,CMS系统"
SiteMetaDescription = "KnifeCMS全称开放式商务建站系统，是一种完全开源免费的全新互联网建站系统。"
ShopTitle = "网店系统主页"
ShopMetaKeywords = "网店系统,shop系统,网上商城,网上购物系统"
ShopMetaDescription = "产品在线销售，网上购物系统"
GoodsConsultSet = 0
GoodsCommentSet = 0
IsGoodsConsultCheck = 1
IsGoodsCommentCheck = 1
ContentCommentSet = 0
IsContentCommentCheck = 1
SiteIsTax = 1
SiteTaxRate = "5" '即5%
MailConfig = "{""smtp"":""smtp.exmail.qq.com"",""mailserver_username"":""test@test.com"",""mailserver_password"":""123456"",""from"":""test@test.com"",""fromname"":""系统管理员""}"
'**
SiteTaxRate = CDbl(SiteTaxRate)
%>
<!--#include file="config.cookiesession.asp"-->