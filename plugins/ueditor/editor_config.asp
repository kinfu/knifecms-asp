<!--#include file="../../config/config.main.asp"-->
<%
Dim Ueditor
Set Ueditor = New Class_Ueditor
Call Ueditor.Init()
Set Ueditor = Nothing
Class Class_Ueditor
	Private Pv_Config
	Private Sub Class_Initialize()
	End Sub
	Private Sub Class_Terminate()
	End Sub
	
	Public Function Init()
		Call GetEditorConfig()
		Response.Write(Pv_Config)
	End Function
	
	Public Function GetEditorConfig()
		Pv_Config = "(function(){"
		Pv_Config = Pv_Config &"var URL;"
		Pv_Config = Pv_Config &"var tmp = location.protocol.indexOf('file')==-1 ? location.pathname : location.href;"
		Pv_Config = Pv_Config &"URL = window.UEDITOR_HOME_URL||tmp.substr(0,tmp.lastIndexOf('\/')+1).replace('_examples/','').replace('website/','');"
		Pv_Config = Pv_Config &"window.UEDITOR_CONFIG = {"
        Pv_Config = Pv_Config &"UEDITOR_HOME_URL:URL"
		Pv_Config = Pv_Config &",toolbars:[['fullscreen','source','|','undo','redo','|','bold','italic','underline','strikethrough','superscript','subscript','forecolor','backcolor','|','insertorderedlist','insertunorderedlist','selectall','|','rowspacingtop','rowspacingbottom','lineheight','|','justifyleft','justifycenter','justifyright','justifyjustify','|','touppercase','tolowercase','|','paragraph','fontfamily','fontsize','|','directionalityltr','directionalityrtl','indent','|','link','unlink','anchor','|','inserttable','deletetable','insertparagraphbeforetable','insertrow','deleterow','insertcol','deletecol','mergecells','mergeright','mergedown','|','emotion','map','gmap','insertframe','highlightcode','template','spechars','|','insertimage','insertvideo','|','insert_file','insert_music','insert_flash','|','imagenone','imageleft','imageright','imagecenter','|','removeformat','formatmatch','autotypeset','pasteplain','searchreplace','preview','help']]"
        Pv_Config = Pv_Config &",labelMap:{'anchor':'','undo':'','insert_file':'插入文件','insert_music':'插入音乐','insert_flash':'插入flash'}"
        Pv_Config = Pv_Config &",webAppKey:''"
        Pv_Config = Pv_Config &",charset:'UTF-8'"
        Pv_Config = Pv_Config &",initialContent:''"
        Pv_Config = Pv_Config &",initialFrameWidth:760"
        Pv_Config = Pv_Config &",initialFrameHeight:400"
        Pv_Config = Pv_Config &",iframeCssUrl:'"& TemplatePath &"css/global.css'"
        Pv_Config = Pv_Config &",textarea:'TextContent'"
        Pv_Config = Pv_Config &",imagePopup:true"
		Pv_Config = Pv_Config &",initialStyle:'body{font-size:12px}'"
        Pv_Config = Pv_Config &",'fontsize':[6,8,9,10,11,12,14,16,18,20,24,36,48,60]"
        Pv_Config = Pv_Config &",wordCount:false"
        Pv_Config = Pv_Config &",maximumWords:200000"
		Pv_Config = Pv_Config &",scaleEnabled:false"
        Pv_Config = Pv_Config &",autoFloatEnabled:true"
        Pv_Config = Pv_Config &",topOffset:21"
        Pv_Config = Pv_Config &",pageBreakTag:'<!--[nextpage]-->'"
		Pv_Config = Pv_Config &"};"
		Pv_Config = Pv_Config &"})();"
	End Function
End Class

%>


