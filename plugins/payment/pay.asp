<!--#include file="../../config/config.main.asp"-->
<!--#include file="../../include/kfc.main.asp"-->
<!--#include file="../../include/kfc.initialization.asp"-->
<!--#include file="pay.alipay.asp"-->
<%
Dim PaymentPlugin
Set PaymentPlugin = New Class_PaymentPlugin
Set PaymentPlugin = Nothing
Set OS            = Nothing
Class Class_PaymentPlugin
	Private Pv_PaymentID,Pv_PayType,Pv_OrderID,Pv_IsOrderExist,Pv_IsPaymentExist
	Private PayObject
	
	Private Sub Class_Initialize()
		Pv_IsOrderExist   = False
		Pv_IsPaymentExist = False
		Pv_OrderID        = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","orderid"),"[^0-9]",""),16)
		Set Rs = KnifeCMS.DB.GetRecord(DBTable_Order &":PaymentID",Array("OrderID:"& Pv_OrderID,"Recycle:0"),"")
		If Not(Rs.Eof) Then
			Pv_IsOrderExist = True
			Pv_PaymentID    = KnifeCMS.Data.CLng(Rs(0))
		End If
		KnifeCMS.DB.CloseRs Rs
		'如果订单合法存在...
		If Pv_IsOrderExist Then
			Set Rs = KnifeCMS.DB.GetRecord(DBTable_Payments &":PayType",Array("ID:"& Pv_PaymentID),"")
			If Not(Rs.Eof) Then
				Pv_PayType        = Rs(0)
				Pv_IsPaymentExist = True
			End If
			KnifeCMS.DB.CloseRs Rs
			If Pv_IsPaymentExist Then
				Select Case Pv_PayType
					'转向支付宝支付页面
					Case "alipay"
						Dim AlipayObject
						Set AlipayObject = New LibPayment_Alipay
						AlipayObject.Action    = "alipayto"
						AlipayObject.PaymentID = Pv_PaymentID
						AlipayObject.OrderID   = Pv_OrderID
						AlipayObject.Init()
						Set AlipayObject = Nothing
					'输出“错误的支付方式ID”
					Case Else
						Echo "ILLEGAL_PAYMENTID"
				End Select
			'支付方式不存在
			Else
				Echo Lang_PaymentNotExist
			
			End If
		Else
			Echo Lang_OrderIDNotExist
		End If
	End Sub
	Private Sub Class_Terminate()
	End Sub
End Class
%>