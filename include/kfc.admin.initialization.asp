<%
'**
'系统后台初始化加载程序
'filename: kfc.initialization.asp
'copyright: KFC
'**
Dim SubCtl,ActionType,Method,Url,DBTable,DBTable_Class,tempCondition
Dim Admin,ClassID,Result,i,Msg,ErrMsg,Operation,PageUrlPara,C_TempID,C_TempStr,C_TempArr,C_TempHtml
Dim NavigationLine,AdvancedSearchBtnline,GetSelectItemsBtnLine
Ctl       = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","ctl"),"[^0-9a-zA-Z,-_]",""),50)
C_TempArr = Split(Ctl,"_") :  IF Ubound(C_TempArr)>0 Then SubCtl = C_TempArr(1)
Action    = KnifeCMS.Data.Left(KnifeCMS.GetForm("both","act"),50)
If Left(Ctl,5)="file_" Then Method = "get" Else Method = "both"
SubAction = KnifeCMS.Data.Left(KnifeCMS.GetForm(Method,"subact"),50)
ActionType= KnifeCMS.Data.Left(KnifeCMS.GetForm(Method,"acttype"),50)
Url       = KnifeCMS.Data.Left(KnifeCMS.GetForm(Method,"url"),250)
ID        = KnifeCMS.Data.CLng(KnifeCMS.GetForm(Method,"id"))
SysID     = KnifeCMS.Data.Left(KnifeCMS.GetForm(Method,"sysid"),32)
ClassID   = KnifeCMS.Data.CLng(KnifeCMS.GetForm(Method,"ClassID"))
If KnifeCMS.Data.IsNul(SysID) Then SysID = KnifeCMS.CreateSysID

FileSuffix = KnifeCMS.IIF(Trim(FileSuffix)="",".htm",FileSuffix)
%>
<!--#include file="kfc.dbtable.define.asp"-->
<%
Set Admin = New Class_KnifeCMS_Admin
Admin.Language = SiteAdminLanguage         '设置语言包
Admin.Init
If Not(Ctl="sysuserlogin" And (Action="login" Or Action="loginout")) Then Admin.SysUserCheck  '用户是否登陆及是否有权限检测
If ((Action="add" Or Action="edit") AND SubAction="save") Then Admin.CheckPostShow()                                   '防止外部提交
If SubAction = "search" Then
	Admin.Search.ExplicitSearch = KnifeCMS.IIF(KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","explicitsearch"))=1,true,false)
	Admin.Search.ClassID        = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","classid"))
	Admin.Search.CollectionID   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","collectionid"))
	Admin.Search.GoodsType      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","goodstype"))
	Admin.Search.BrandID        = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","brandid"))
	Admin.Search.KeyWords       = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("both","keywords")),50)
	Admin.Search.StartDate      = KnifeCMS.Data.FormatDateTime(KnifeCMS.GetForm("both","startdate"),0)
	Admin.Search.EndDate        = KnifeCMS.Data.FormatDateTime(KnifeCMS.GetForm("both","enddate"),0)
	If Admin.Search.EndDate = "" Then Admin.Search.EndDate = KnifeCMS.Data.FormatDateTime(SysTime,0)
	If KnifeCMS.Data.DateDiff("d",Admin.Search.StartDate,Admin.Search.EndDate) < 0 Then Admin.ShowMessage Lang_Cue_SearchDateErr,"goback",0 : Die ""
End If

'一些常用的Html内容
NavigationLine = Lang_YourPosition &": "& ManageSystem &" <span>&raquo;</span> "& Admin.SystemCtlName &" <span>&raquo;</span> "& Admin.SystemActionName
AdvancedSearchBtnline = "<div class=""actline mt5""><button type=""submit"" class=""sysSaveBtn""><b class=""icon icon_search""></b>"& Lang_Btn_Search &"</button> <button type=""reset"" class=""sysSaveBtn""><b class=""icon icon_reset""></b>"& Lang_Btn_Reset &"</button></div>"
GetSelectItemsBtnLine = "<div class=""bottomSaveline"" id=""SubmitButtoms""><button type=""button"" class=""sysSaveBtn"" name=""SubmitBtn"" id=""SubmitBtn"" onclick=""Admin.RebackSelectItems('InputName')""><b class=""icon icon_save""></b>"& Lang_Btn_Confirm &"</button><button type=""button"" class=""sysSaveBtn"" name=""CloseBtn"" id=""CloseBtn"" onclick=""WinClose()""><b class=""icon icon_reset""></b>"& Lang_Btn_Close &"</button></div>"
%>