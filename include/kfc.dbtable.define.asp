<%
Dim DBTable_SysUser,DBTable_SysUserGroup,DBTable_SysUserPermission,DBTable_SystemCtlAction,DBTable_SystemLog

Dim DBTable_SystemSet,DBTable_Navigator,DBTable_Regions,DBTable_MailTemplate,DBTable_Delivery,DBTable_Delivery_Area,DBTable_DeliveryCorp

Dim DBTable_BankAccount,DBTable_Payments,DBTable_PayTradeLog,DBTable_PaymentBill,DBTable_RefundBill,DBTable_ShipBill,DBTable_ShipItem,DBTable_ReShipBill
Dim DBTable_Goods,DBTable_GoodsFieldSet,DBTable_GoodsAdjunct,DBTable_GoodsAdjunctGoods,DBTable_GoodsAttr,DBTable_GoodsClass,DBTable_GoodsInVirtualClass,DBTable_GoodsLink,DBTable_GoodsModel,DBTable_GoodsModelAttribute,DBTable_GoodsType,DBTable_GoodsVirtualClass,DBTable_GoodsComment,DBTable_GoodsConsult,DBTable_Brand
Dim DBTable_Order,DBTable_OrderGoods,DBTable_OrderLog

Dim DBTable_Ads_Group,DBTable_Ads_Detail
Dim DBTable_ContentSubSystem,DBTable_FieldSet,DBTable_CModule
Dim DBTable_Guestbook,DBTable_Link
Dim DBTable_Vote,DBTable_VoteClass,DBTable_VoteOption

Dim DBTable_Members,DBTable_MemberGrade,DBTable_Members_Info,DBTable_Members_Addrs,DBTable_Members_Feeds,DBTable_Members_GuestBook,DBTable_Friend,DBTable_FriendGroup

Dim DBTable_Visitor,DBTable_Notify

'***********定义系统表***********
DBTable_SysUser             = TablePre & "SysUser"
DBTable_SysUserGroup        = TablePre & "SysUserGroup"
DBTable_SysUserPermission   = TablePre & "SysUserPermission"
DBTable_SystemCtlAction     = TablePre & "SystemCtlAction"


DBTable_SystemSet           = TablePre & "SystemSet"
DBTable_SystemLog           = TablePre & "SystemLog"
DBTable_Navigator           = TablePre & "Navigator"
DBTable_Regions             = TablePre & "Regions"

DBTable_MailTemplate        = TablePre & "MailTemplate"

DBTable_Delivery            = TablePre & "Delivery"
DBTable_Delivery_Area       = TablePre & "Delivery_Area"
DBTable_DeliveryCorp        = TablePre & "DeliveryCorp"

DBTable_BankAccount         = TablePre & "BankAccount"
DBTable_Payments            = TablePre & "Payments"
DBTable_PayTradeLog         = TablePre & "PayTradeLog"
DBTable_PaymentBill         = TablePre & "PaymentBill"
DBTable_RefundBill          = TablePre & "RefundBill"
DBTable_ShipBill            = TablePre & "ShipBill"
DBTable_ShipItem            = TablePre & "ShipItem"
DBTable_ReShipBill          = TablePre & "ReShipBill"

DBTable_Goods               = TablePre & "Goods"
DBTable_GoodsFieldSet       = TablePre & "GoodsFieldSet"
DBTable_GoodsAdjunct        = TablePre & "GoodsAdjunct"
DBTable_GoodsAdjunctGoods   = TablePre & "GoodsAdjunctGoods" 
DBTable_GoodsAttr           = TablePre & "GoodsAttr"
DBTable_GoodsClass          = TablePre & "GoodsClass"
DBTable_GoodsInVirtualClass = TablePre & "GoodsInVirtualClass"
DBTable_GoodsLink           = TablePre & "GoodsLink"
DBTable_GoodsModel          = TablePre & "GoodsModel"
DBTable_GoodsModelAttribute = TablePre & "GoodsModelAttribute"
DBTable_GoodsType           = TablePre & "GoodsType"
DBTable_GoodsVirtualClass   = TablePre & "GoodsVirtualClass"
DBTable_GoodsComment        = TablePre & "GoodsComment"
DBTable_GoodsConsult        = TablePre & "GoodsConsult"
DBTable_Order               = TablePre & "Order"
DBTable_OrderGoods          = TablePre & "OrderGoods"
DBTable_OrderLog            = TablePre & "OrderLog"
DBTable_Brand               = TablePre & "Brand"

DBTable_ContentSubSystem    = TablePre & "ContentSubSystem"
DBTable_FieldSet            = TablePre & "FieldSet"
DBTable_CModule             = TablePre & "CModule"

DBTable_Guestbook           = TablePre & "Guestbook"
DBTable_Link                = TablePre & "Link"

DBTable_Ads_Group           = TablePre & "Ads_Group"
DBTable_Ads_Detail          = TablePre & "Ads_Detail"

DBTable_Vote                = TablePre & "Vote"
DBTable_VoteClass           = TablePre & "VoteClass"
DBTable_VoteOption          = TablePre & "VoteOption"

DBTable_Members             = TablePre & "Members"
DBTable_MemberGrade         = TablePre & "MemberGrade"
DBTable_Members_Info        = TablePre & "Members_Info"
DBTable_Members_Addrs       = TablePre & "Members_Addrs"
DBTable_Members_Feeds       = TablePre & "Members_Feeds"
DBTable_Members_GuestBook   = TablePre & "Members_GuestBook"
DBTable_Friend              = TablePre & "Friend"
DBTable_FriendGroup         = TablePre & "FriendGroup"

DBTable_Visitor             = TablePre & "Visitor"

DBTable_Notify              = TablePre & "Notify"

%>