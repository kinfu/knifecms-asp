<!--#include file="kfc.asp"-->
<%
'当出现系统错误时调用此方法
'关闭程序进程（注销所有类、注销所有对象）
Sub ProcessClose()
	Set OS = Nothing
	Err.Clear()
	Response.End()
End Sub

Sub PageRedirect(BV_Page)
	Set OS = Nothing
	Err.Clear()
	Response.Redirect(BV_Page)
End Sub

Dim Rs,Rs2,Rs3,Rs4,Sql,ID,SysID,Ctl,TempCtl,SubSystem,Action,SubAction,Category
Dim Logined
Dim UserID,Username,UserPassword,TruePassword,UserEmail,UserBigHead,UserSmallHead,UserDisable
Dim CurrentURL,TagLine,SubSystemName,RegStatusMsg
Dim Content_ID,Content_CategoryID,Content_ClassName,Content_Title,Content_BriefTitle,Content_TitleFontColor,Content_TitleFontType,Content_BriefTitleFontColor,Content_BriefTitleFontType,Content_From,Content_Author,Content_PublicTime,Content_Thumbnail,Content_UseLinkUrl,Content_LinkUrl,Content_Tags,Content_HaveAbstract,Content_Abstract,Content_Content,Content_ContentPages,Content_HaveImgs,Content_FirstImg,Content_Imgs,Content_Hits,Content_Comments,Content_TopLine,Content_OnTop,Content_Hot,Content_Elite,Content_CommentSet,Content_Openness,Content_AddTime,Content_EditTime,Content_Diggs,Content_LinkVotes,Content_LinkContents
Dim Vote_ID,Vote_ClassID,Vote_ClassName,Vote_Title,Vote_VoteMode,Vote_VoteModeMore,Vote_Description,Vote_ResultDescribe,Vote_Hits,Vote_Joins,Vote_Addtime,Vote_Edittime
Dim Goods_ID,Goods_ClassID,Goods_ClassName,Goods_Name,Goods_BN,Goods_SN,Goods_Price,Goods_MktPrice,Goods_Weight,Goods_Store,Goods_Unit,Goods_BrandID,Goods_BrandName,Goods_BrandLogo,Goods_Thumbnail,Goods_FirstImg,Goods_Imgs,Goods_Abstract,Goods_Intro,Goods_GoodsModel,Goods_AddTime,Goods_Attribute,Goods_LinkGoods
Dim Cart_OrderID,Cart_OrderTime,Cart_PaymentID,Cart_PaymentName,Cart_OrderAmount,Cart_Payed,Cart_IsHavePayed,Cart_IsCOD,Cart_PaymentsList
Dim MyAddrs_Region,MyAddrs_Address,MyAddrs_Zip,MyAddrs_Name,MyAddrs_Mobile,MyAddrs_Telephone
Dim SearchResult

'检查前后两次提交数据的时间是否在指定的禁止提交时间内(防止恶意提交)
Public Function CheckIsInForbidPostTime(ByVal BV_ForbidPostTime,ByVal BV_SqlString,ByVAl BV_NowTime)
	Dim Fn_Rs,Fn_LastPostTime
	Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(BV_SqlString)
	If Not(Fn_Rs.Eof) Then Fn_LastPostTime = Fn_Rs(0)
	KnifeCMS.DB.CloseRs Fn_Rs
	If KnifeCMS.Data.IsNul(Fn_LastPostTime) Then CheckIsInForbidPostTime = False : Exit Function
	If DateDiff("s",Fn_LastPostTime,BV_NowTime) < 15 Then CheckIsInForbidPostTime = True : Exit Function
	CheckIsInForbidPostTime = False
End Function

'$:检查内容模型是否存在
Function IsContentSubSystemExist(ByVal BV_ContentSubSystem)
	Dim Fn_Rs,Fn_String : Fn_String = False
	Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_ContentSubSystem &"] WHERE System='"& BV_ContentSubSystem &"'")
	If Not(Fn_Rs.Eof) Then Fn_String = True
	KnifeCMS.DB.CloseRs Fn_Rs
	IsContentSubSystemExist = Fn_String
End Function

'获取内容系统信息
Function GetContentSubSystemInfo(ByVal BV_ContentSubSystem)
	Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT SystemName,Template,CategoryTemplate,ContentTemplate,SeoTitle,SeoUrl,MetaKeywords,MetaDescription FROM ["& DBTable_ContentSubSystem &"] WHERE System='"& BV_ContentSubSystem &"'")
	If Not(Rs.Eof) Then
		IsContentExist  = True
		SubSystemName   = KnifeCMS.Data.ReplaceHtml(KnifeCMS.Data.HTMLDecode(Rs("SystemName")))
		IndexTemplate    = Rs("Template")
		CategoryTemplate = Rs("CategoryTemplate")
		ContentTemplate  = Rs("ContentTemplate")
		SeoTitle        = Rs("SeoTitle")
		SeoUrl          = Rs("SeoUrl")
		MetaKeywords    = Rs("MetaKeywords")
		MetaDescription = Rs("MetaDescription")
	Else
		IsContentExist  = False
	End If
	KnifeCMS.DB.CloseRs Rs
End Function

'获取分类信息
Function GetCategoryInfo(ByVal BV_DBTable,ByVal BV_ID)
	Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT SeoTitle,SeoUrl,MetaKeywords,MetaDescription FROM ["& BV_DBTable &"] WHERE ID="& BV_ID &"")
	If Not(Rs.Eof) Then
		SeoTitle        = Rs(0)
		SeoUrl          = Rs(1)
		MetaKeywords    = Rs(2)
		MetaDescription = Rs(3)
	End If
	KnifeCMS.DB.CloseRs Rs
End Function

'获取内容详细
Sub GetArticleDetail(ByVal BV_ContentSubSystem,ByVal BV_ID)
	KnifeCMS.Tpl.CSubSystem = BV_ContentSubSystem
	Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM "& TablePre &"Content_"& BV_ContentSubSystem &" WHERE [ID]="& BV_ID &" AND [Status]=1 AND [Recycle]=0")
	If Not(Rs.Eof) Then
		IsContentExist         = True
		Content_CategoryID     = Rs("ClassID")
		Content_Title          = KnifeCMS.Data.ReplaceString(Rs("Title"),"&nbsp;",Chr(32))  : SiteTitle = Content_Title
		Content_BriefTitle     = Rs("BriefTitle")
		Content_Author         = Rs("Author")
		Content_From           = KnifeCMS.Data.HTMLDecode(Rs("From"))
		Content_PublicTime     = Rs("PublicTime")
		Content_Thumbnail      = KnifeCMS.Data.URLDecode(Rs("Thumbnail"))
		Content_FirstImg       = Rs("SlidesFirstImg")
		Content_Imgs           = Rs("SlidesImgs")
		Content_UseLinkUrl     = Rs("UseLinkUrl")
		Content_LinkUrl        = Rs("LinkUrl")
		Content_Tags           = Rs("Tags")  : 
		Content_Abstract       = KnifeCMS.Data.AbstractDecode(Rs("Abstract"))
		Content_Content        = KnifeCMS.Data.EditorContentDecode(Rs("Content"))
		Content_Hits           = Rs("Hits")
		Content_Comments       = Rs("Comments")
		Content_TopLine        = Rs("TopLine")
		Content_OnTop          = Rs("OnTop")
		Content_Hot            = Rs("Hot")
		Content_Elite          = Rs("Elite")
		Content_CommentSet     = Rs("CommentSet")
		Content_Openness       = Rs("Openness")
		Content_AddTime        = Rs("AddTime")
		Content_EditTime       = Rs("EditTime")
		Content_Diggs          = Rs("Diggs")
		Content_LinkVotes      = KnifeCMS.Data.ParseID(Rs("LinkVotes"))
		Content_LinkContents   = KnifeCMS.Data.ParseID(Rs("LinkContents"))
		SeoTitle           = Rs("SeoTitle")
		SeoUrl             = Rs("SeoUrl")
		MetaKeywords       = Rs("MetaKeywords")
		MetaDescription    = Rs("MetaDescription")
	Else
		IsContentExist = False
	End If
	KnifeCMS.DB.CloseRs Rs
	If IsContentExist Then
		If Not(KnifeCMS.Data.IsNul(Content_Content)) Then
			Dim Fn_Array   : Fn_Array   = Split(Content_Content,"<!--[nextpage]-->")
			Dim Fn_PageMax : Fn_PageMax = Ubound(Fn_Array)
			If Not(Page>0) Then Page = 1
			If Page>Fn_PageMax+1 Then Page=Fn_PageMax+1
			Content_Content = Fn_Array(Page-1)
			
			'生成分页标签
			Dim Fn_Page,Fn_a,Fn_Temp_i,Fn_PageCount : Fn_Page = Page
			Dim Fn_PageString
			If Fn_PageMax>0 Then
				Fn_PageCount  = Fn_PageMax + 1
				Fn_PageString = "<div class=pagestyle>"
				If Fn_Page>1 Then Fn_PageString = Fn_PageString &"<a href="""& SiteURL&SystemPath &"?"& Ctl &"/"& SubSystem &"/"& BV_ID &"_"& Fn_Page-1 &"/"&  SeoUrl & FileSuffix &""">"& Lang_Pages(1) &"</a>"
				if Fn_Page>1 then Fn_a=1 : if Fn_Page>2 then Fn_a=2 : if Fn_Page>3 then Fn_a=3 : if Fn_Page>4 then Fn_a=4 : if Fn_Page>5 then Fn_a=5 : if Fn_Page>6 then Fn_a=6
				for Fn_Temp_i=Fn_Page-Fn_a to Fn_Page-1
					Fn_PageString = Fn_PageString & "<a href="""& SiteURL&SystemPath &"?"& Ctl &"/"& SubSystem &"/"& BV_ID &"_"& Fn_Temp_i &"/"&  SeoUrl & FileSuffix &""">"& Fn_Temp_i &"</a>"
				next
				Fn_PageString = Fn_PageString &"<span class=current>"& Fn_Page &"</span>"
				
				for Fn_Temp_i=Fn_Page+1 to Fn_Page+6
					if Fn_Temp_i > Fn_PageCount then Exit for
					Fn_PageString = Fn_PageString & "<a href="""& SiteURL&SystemPath &"?"& Ctl &"/"& SubSystem &"/"& BV_ID &"_"& Fn_Temp_i &"/"&  SeoUrl & FileSuffix &""">"& Fn_Temp_i &"</a>"
				next
				
				if Not(Fn_Page>=Fn_PageCount) then Fn_PageString = Fn_PageString & "<a href="""& SiteURL&SystemPath &"?"& Ctl &"/"& SubSystem &"/"& BV_ID &"_"& Fn_Page+1 &"/"&  SeoUrl & FileSuffix &""">"& Lang_Pages(2) &"</a>"
				Fn_PageString = Fn_PageString & "<span class=pages>"&  Replace(Lang_Pages(3),"{tpl:num}",Fn_PageCount) &"</span></div>"
			End If
			Content_ContentPages = Fn_PageString
		End If
	End If
End Sub

'获取投票详细
Sub GetVoteDetail()
	Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT a.ClassID,b.ClassName,a.VoteTitle,a.VoteMode,a.VoteMode_More,a.Description,a.ResultDescribe,a.Hits,a.Joins,a.AddTime,a.EditTime FROM "& DBTable_Vote &" a LEFT JOIN "& DBTable_VoteClass &" b ON (a.ClassID=b.ID) WHERE a.ID="& Vote_ID &" AND a.Recycle=0")
	If Not(Rs.Eof) Then
		IsContentExist = True
		Vote_ClassID   = Rs("ClassID")
		Vote_ClassName = Rs("ClassName")
		Vote_Title     = Rs("VoteTitle")
		Vote_VoteMode  = Rs("VoteMode")
		Vote_VoteModeMore = Rs("VoteMode_More")
		Vote_Description  = Rs("Description")
		Vote_ResultDescribe  = Rs("ResultDescribe")
		Vote_Hits         = Rs("Hits")
		Vote_Joins        = Rs("Joins")
		Vote_Addtime      = Rs("AddTime")
		Vote_Edittime     = Rs("EditTime")
	Else
		IsContentExist = False
	End If
	KnifeCMS.DB.CloseRs Rs
End Sub

'获取商品详细
Sub GetGoodsDetail(ByVal BV_GoodsID)
	Dim Fn_Rs,Fn_Rs2,Fn_ID,Fn_AttrID,Fn_AttrName,Fn_AttrValue,Fn_AttrPrice,Fn_TempAttrPrice,Fn_AttrType
	Dim Fn_MoneySign '价格正负号
	Goods_Attribute = ""
	Goods_LinkGoods = ""
	Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM "& DBTable_Goods &" WHERE [ID]="& BV_GoodsID &" AND [Marketable]=1 AND [Recycle]=0")
	If Not(Fn_Rs.Eof) Then
		IsContentExist      = True
		Goods_ClassID       = Fn_Rs("ClassID")
		Goods_Name          = KnifeCMS.Data.ReplaceString(Fn_Rs("GoodsName"),"&nbsp;",Chr(32))
		Goods_BN            = Fn_Rs("GoodsBN")
		Goods_SN            = Fn_Rs("GoodsSN")
		Goods_Price         = Fn_Rs("Price")
		Goods_MktPrice      = Fn_Rs("MktPrice")
		Goods_Weight        = Fn_Rs("Weight")
		Goods_Store         = Fn_Rs("Store")
		Goods_Unit          = Fn_Rs("Unit")
		Goods_BrandID       = Fn_Rs("BrandID")
		Goods_Thumbnail     = KnifeCMS.IIF(Fn_Rs("Thumbnail")<>"",Fn_Rs("Thumbnail"),Fn_Rs("FirstImg"))
		Goods_FirstImg      = Fn_Rs("FirstImg")
		Goods_Imgs          = Fn_Rs("Imgs")
		Goods_Abstract      = KnifeCMS.Data.HTMLDecode(Fn_Rs("Abstract"))
		Goods_Intro         = KnifeCMS.Data.EditorContentDecode(Fn_Rs("Intro"))
		Goods_GoodsModel    = Fn_Rs("GoodsModel")
		Goods_AddTime       = Fn_Rs("AddTime")
		SeoTitle            = Fn_Rs("SeoTitle")
		SeoUrl              = Fn_Rs("SeoUrl")
		MetaKeywords        = Fn_Rs("MetaKeywords")
		MetaDescription     = Fn_Rs("MetaDescription")
	Else
		IsContentExist = False
	End If
	KnifeCMS.DB.CloseRs Fn_Rs
	Goods_GoodsModel = KnifeCMS.Data.CLng(Goods_GoodsModel)
	Goods_BrandID    = KnifeCMS.Data.CLng(Goods_BrandID)
	If IsContentExist Then
		'获取商品品牌名称及名牌logo
		If Goods_BrandID>0 Then
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT BrandName,BrandLogo FROM "& DBTable_Brand &" WHERE [ID]="& Goods_BrandID &" AND Recycle=0")
			If Not(Fn_Rs.Eof) Then
				Goods_BrandName = Fn_Rs("BrandName")
				Goods_BrandLogo = Fn_Rs("BrandLogo")
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
		End If
		'获取商品属性
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT a.AttrID,b.AttrName,b.AttrType FROM ["&DBTable_GoodsAttr&"] a LEFT JOIN ["&DBTable_GoodsModelAttribute&"] b ON (a.AttrID=b.ID) WHERE a.GoodsID="&BV_GoodsID&" GROUP BY b.AttrOrder,a.AttrID,b.AttrName,b.AttrType")
		Do While Not(Fn_Rs.Eof)
			Fn_ID       = Fn_Rs(0)
			Fn_AttrName = Fn_Rs(1)
			Fn_AttrType = KnifeCMS.Data.CLng(Fn_Rs(2))
			If Fn_AttrType>0 Then
				Goods_Attribute = Goods_Attribute & "<dl name=""attr"" class=""stock"" attrid="""& Fn_ID &""" attrname="""& Fn_AttrName &""" attrtype="""& Fn_AttrType &"""><dt>"&Fn_AttrName&":</dt><dd><ul id=""attr_"& Fn_ID &""" attr_id="""& Fn_ID &""" attrtype="""& Fn_AttrType &""">"
			Else
				Goods_Attribute = Goods_Attribute & "<dl name=""attr_single"" class=""stock"" attrid="""& Fn_ID &""" attrname="""& Fn_AttrName &""" attrtype="""& Fn_AttrType &"""><dt>"&Fn_AttrName&":</dt><dd>"
			End If
			Set Fn_Rs2 = KnifeCMS.DB.GetRecordBySQL("SELECT ID,AttrValue,AttrPrice FROM ["&DBTable_GoodsAttr&"] WHERE GoodsID="& BV_GoodsID &" AND AttrID="& Fn_ID &" ")
			Do While Not(Fn_Rs2.Eof)
				Fn_AttrID        = Fn_Rs2(0)
				Fn_AttrValue     = KnifeCMS.Data.UnEscape(KnifeCMS.Data.HTMLDecode(Fn_Rs2(1)))
				Fn_AttrPrice     = Fn_Rs2(2)
				Fn_TempAttrPrice = Fn_AttrPrice
				If Fn_AttrType>0 Then
					If KnifeCMS.Data.Left(Fn_TempAttrPrice,1)="-" Then
						Fn_TempAttrPrice = Mid(Fn_TempAttrPrice,2,Len(Fn_TempAttrPrice)-1)
						Fn_MoneySign = "-"
					ElseIf KnifeCMS.Data.Left(Fn_TempAttrPrice,1)="+" Then
						Fn_TempAttrPrice = Mid(Fn_TempAttrPrice,2,Len(Fn_TempAttrPrice)-1)
						Fn_MoneySign = "+"
					Else
						Fn_MoneySign = "+"
					End If
					Goods_Attribute = Goods_Attribute & "<li id=""attr-"& Fn_AttrName &"-"& Fn_AttrValue &""" goodsattrid="""& Fn_AttrID &""" attrvalue="""& Fn_AttrValue &""" attrprice="""& Fn_AttrPrice &""" tag=""attrs""><a href=""javascript:void(0);"" title="""& Fn_MoneySign &" "& MoneySb & Fn_TempAttrPrice &""" hidefocus=""true""><span>"& Fn_AttrValue &"</span></a></li>"
				Else
					Goods_Attribute = Goods_Attribute & Fn_AttrValue
				End If
				Fn_Rs2.Movenext()
			Loop
			KnifeCMS.DB.CloseRs Fn_Rs2
			If Fn_AttrType>0 Then
				Goods_Attribute = Goods_Attribute & "</ul></dd></dl>"
			Else
				Goods_Attribute = Goods_Attribute & "</dd></dl>"
			End If
			Fn_Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT LinkGoodsID FROM ["& DBTable_GoodsLink &"] WHERE GoodsID="& BV_GoodsID &" ")
		Do While Not(Fn_Rs.Eof)
			Goods_LinkGoods = Goods_LinkGoods &","& Fn_Rs(0)
			Fn_Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
	End If
End Sub

'获取支付方式列表
Function GetCartPaymentsList(BV_OrderID)
	Dim Fn_Rs,Fn_ReturnString,Fn_ShippingID,Fn_Final_Amount,Fn_PaymentID,Fn_IsHasCod,Fn_Array,Fn_TempOrderID
	Dim Fn_PayMethod,Fn_Fee,Fn_Poundage,Fn_PoundageHTML
	Fn_TempOrderID = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(BV_OrderID,"[^0-9]",""),16)
	If Fn_TempOrderID<>BV_OrderID Or Len(BV_OrderID)<>16 Then GetCartPaymentsList="ILLEGAL_ORDERID" : Exit Function
	Fn_ReturnString = "<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""infoTable""><tbody>"

	Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Order &":ShippingID,Final_Amount,PaymentID",Array("OrderID:"& BV_OrderID),"")
	If Not(Fn_Rs.Eof) Then
		Fn_ShippingID   = Fn_Rs("ShippingID")
		Fn_Final_Amount = Fn_Rs("Final_Amount")
		Fn_PaymentID    = Fn_Rs("PaymentID")
	End If
	KnifeCMS.DB.CloseRs Fn_Rs
	
	Fn_IsHasCod   = KnifeCMS.Data.CLng(KnifeCMS.DB.GetFieldByArrayField(DBTable_Delivery,"HasCod",Array("ID:"& Fn_ShippingID)))
	If Fn_IsHasCod=1 Then
		Fn_Array = Array("Disabled:0")
	Else
		Fn_Array = Array("Disabled:0","PayType<>'cod'")
	End If
	
	Set Fn_Rs=KnifeCMS.DB.GetRecord(DBTable_Payments,Fn_Array,"ORDER BY OrderNum ASC")
	Do While Not(Fn_Rs.Eof)
		'**计算手续费**
		Fn_PayMethod    = Fn_Rs("PayMethod")
		Fn_Fee          = Fn_Rs("Fee")
		Fn_Poundage     = KnifeCMS.Data.FormatCurrency(KnifeCMS.IIF(Fn_PayMethod=1,Fn_Final_Amount * Fn_Fee,Fn_Fee))
		If Fn_Poundage>0 Then
			Fn_PoundageHTML = "<div class=""poundage"">"& Lang_NeedPoundage &"<strong class=""redh""><em class=""moneySb"">"& MoneySb &"</em>"& Fn_Poundage &"</strong></div>"
		Else
			Fn_PoundageHTML = ""
		End If
		'****
		Fn_ReturnString = Fn_ReturnString & "<tr><td class=""titletd"" style=""text-align:left;""><input type=""radio"" name=""payment"" onclick="""" value="""& Fn_Rs("ID") &""" "& KnifeCMS.IIF(Fn_PaymentID=Fn_Rs("ID"),"checked=""checked""","")&" />"& Fn_Rs("PaymentName") &"</td><td class=""infotd"">"& Fn_PoundageHTML &"<div>"& KnifeCMS.DAta.HTMLDecode(Fn_Rs("PaymentDes")) &"</div></td></tr>"
		Fn_Rs.Movenext()
	Loop
	KnifeCMS.DB.CloseRs Fn_Rs
	
	Fn_ReturnString     = Fn_ReturnString & "</tbody></table>"
	GetCartPaymentsList = Fn_ReturnString
End Function

'获取分类及子分类，DB_Table:要查询的数据表
Function GetChildClass(ByVal BV_DBTable, ByVal ParID)
	Dim Fn_Rs,Fn_ID,Fn_ChildClass
	ParID=KnifeCMS.Data.CLng(ParID)
	If ParID < 0 Then Exit Function
	Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,ClassChildNum FROM ["& BV_DBTable &"] WHERE ParentClassID="& ParID &" ORDER BY ClassOrder ASC")
	Do While Not Fn_Rs.Eof
		Fn_ID      = Fn_Rs(0)
		Fn_ChildClass = Fn_ChildClass&","&Fn_ID
		If Fn_Rs(1) > 0 Then Fn_ChildClass = Fn_ChildClass &","& GetChildClass(BV_DBTable,Fn_ID)
		Fn_Rs.Movenext
	Loop
	KnifeCMS.DB.CloseRs Fn_Rs
	GetChildClass = Fn_ChildClass
End Function

'$获取导航栏中的分类导航
Function GetClassTagLine(ByVal BV_System, ByVal BV_ContentSubSystem, ByVal BV_DBTable, ByVal BV_ID)
	Dim Fn_Rs,Fn_ID,Fn_SeoUrl,Fn_TagLine
	BV_ID = KnifeCMS.Data.CLng(BV_ID)
	If BV_ID <= 0 Then Exit Function
	Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ClassName,ParentClassID,SeoUrl FROM ["& BV_DBTable &"] WHERE ID="& BV_ID &"")
	If Not(Fn_Rs.Eof) Then
		SiteTitle  = KnifeCMS.IIF(KnifeCMS.Data.IsNul(SiteTitle),KnifeCMS.Data.ReplaceHtml(KnifeCMS.Data.HTMLDecode(Fn_Rs(0))),SiteTitle)
		Fn_ID      = Fn_Rs(1)
		Fn_SeoUrl  = KnifeCMS.Data.OKData(Fn_Rs(2))
		Fn_TagLine = "<span class=""raquo"">&raquo;</span><a href="""& SiteURL & SystemPath &"?"& BV_System &"/"& KnifeCMS.IIF(KnifeCMS.Data.IsNul(BV_ContentSubSystem),"",BV_ContentSubSystem &"/") &"category/"& BV_ID &"/"& Fn_SeoUrl & FileSuffix &""">"& KnifeCMS.Data.HTMLDecode(Fn_Rs(0)) &"</a>"
		If Fn_ID > 0 Then Fn_TagLine = GetClassTagLine(BV_System,BV_ContentSubSystem,BV_DBTable,Fn_ID) & Fn_TagLine
	End If
	KnifeCMS.DB.CloseRs Fn_Rs
	GetClassTagLine = Fn_TagLine
End Function

'$获取父级路径
Function Get_P_PathAndDepth(ByVal BV_DBTable, ByVal BV_ID)
	Dim Fn_Rs,Fn_Array(2)
	Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT Path,Depth FROM ["& BV_DBTable &"] WHERE ID="& BV_ID &"")
	If Not(Fn_Rs.Eof) Then
		Fn_Array(0)=Fn_Rs(0)
		Fn_Array(1)=Fn_Rs(1)
	End If
	KnifeCMS.DB.CloseRs Fn_Rs
	Get_P_PathAndDepth = Fn_Array
End Function

Function GetUserLatestFeeds(ByVal BV_MemberID)
	Dim Fn_Rs,Fn_ReturnString
	    Fn_ReturnString = ""
	Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT TOP 1 Content FROM ["& DBTable_Members_Feeds &"] WHERE MemberID="& BV_MemberID &" ORDER BY ID DESC")
	If Not(Fn_Rs.Eof) Then Fn_ReturnString=Fn_Rs(0)
	KnifeCMS.DB.CloseRs Fn_Rs
	GetUserLatestFeeds = Fn_ReturnString
End Function

Public Function GetUserInfo()
	Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_Members &"] WHERE ID="& User_ID &"")
	If Not(Rs.Eof) Then
		IsUserExist    = True
		User_Name      = Rs("Username")
		User_BigHead   = KnifeCMS.Data.URLDecode(Rs("BigHead"))
		User_SmallHead = KnifeCMS.Data.URLDecode(Rs("HeadImg"))
	Else
	    IsUserExist    = False
	End If
	KnifeCMS.DB.CloseRs Rs
End Function

Function GetUserInfoDetail(ByVal BV_UserID)
	If Not(Logined=1) Then Exit Function
	UserInfo_Email    = UserEmail
	Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_Members_Info &"] WHERE UserID="& BV_UserID &"")
	If Not(Rs.Eof) Then
		UserInfo_RealName = Rs("RealName")
		UserInfo_Gender   = Rs("Gender")
		UserInfo_HomeTown(0) = Rs("HomeTown")
		UserInfo_HomeTown(1) = GetRegionsByPath(UserInfo_HomeTown(0))
		UserInfo_MySite(0)   = Rs("MySite")
		UserInfo_MySite(1)   = GetRegionsByPath(UserInfo_MySite(0))
		UserInfo_Birthday = Rs("Birthday")
		UserInfo_Mobile   = Rs("Mobile")
		UserInfo_Telephone= Rs("Telephone")
		UserInfo_QQ       = Rs("QQ")
		UserInfo_Height   = Rs("Height")
		UserInfo_Weight   = Rs("Weight")
		UserInfo_MaritalStatus = Rs("MaritalStatus")
		UserInfo_BloodType= Rs("BloodType")
		UserInfo_Religion = Rs("Religion")
		UserInfo_Smoke    = Rs("Smoke")
		UserInfo_Drink    = Rs("Drink")
		UserInfo_FavTravel= Rs("FavTravel")
		UserInfo_FavLiving= Rs("FavLiving")
		UserInfo_FavWork  = Rs("FavWork")
		UserInfo_FavPartner = Rs("FavPartner")
		UserInfo_FavCar = Rs("FavCar")
	End If
	KnifeCMS.DB.CloseRs Rs
End Function

Public Function GetRegionsByPath(ByVal BV_Path)
	If KnifeCMS.Data.IsNul(BV_Path) Then Exit Function
	Dim Fn_Array : Fn_Array=Split(BV_Path,",")
	Dim Fn_i,Fn_ID,Fn_ReturnString,Fn_TempName
	For Fn_i=0 To Ubound(Fn_Array)
		Fn_ID = KnifeCMS.Data.CLng(Fn_Array(Fn_i))
		If(Fn_ID>0) Then
			Fn_TempName = KnifeCMS.DB.GetFieldByID(DBTable_Regions,"Local_Name",Fn_ID)
			If Fn_TempName<>"" Then Fn_ReturnString = Fn_ReturnString &" "& Fn_TempName
		End If
	Next
	GetRegionsByPath = Fn_ReturnString
End Function

Function GetMyAddrs()
	If Not(Logined=1) Then Exit Function
	Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_Members_Addrs &"] WHERE UserID="& UserID &"")
	If Not(Rs.Eof) Then
		MyAddrs_Region    = Rs("Region")
		MyAddrs_Address   = Rs("Address")
		MyAddrs_Zip       = Rs("Zip")
		MyAddrs_Name      = Rs("Name")
		MyAddrs_Mobile    = Rs("Mobile")
		MyAddrs_Telephone = Rs("Telephone")
	End If
	KnifeCMS.DB.CloseRs Rs
End Function

Public Function GetContentCommentSet()
	Dim Fn_ResultString,Fn_DBTableContent
	If SubSystem="goodscomment" Then
		Fn_ResultString = GoodsCommentSet
	ElseIf SubSystem="goodsconsult" Then
		Fn_ResultString = GoodsConsultSet
	Else
		Fn_DBTableContent = TablePre &"Content_"& SubSystem
		If Not(IsContentSubSystemExist(SubSystem)) Then CommentSet=-1 : Exit Function
		Fn_ResultString = KnifeCMS.DB.GetFieldByID(Fn_DBTableContent,"CommentSet",ID)
		If KnifeCMS.Data.IsNul(Fn_ResultString) Then Fn_ResultString=-1
	End If
	GetContentCommentSet = Fn_ResultString
End Function

'提取地区路径如",1,2,6,"中的最后有效地区ID"6"
Public Function PickUpLastRegionID(ByVal BV_String)
	If KnifeCMS.Data.IsNul(BV_String) Then PickUpLastRegionID="" : Exit Function
	Dim Fn_Array : Fn_Array = Split(BV_String,",")
	If Ubound(Fn_Array)>0 Then
		PickUpLastRegionID=Fn_Array(Ubound(Fn_Array)-1)
	Else
		PickUpLastRegionID=Fn_Array(0)
	End If
End Function

'记录来访者
Public Function RecordVisitor
	If UserID = User_ID Then Exit Function
	Dim Fn_Check
	Dim Fn_Sql,Fn_Rs,Fn_CTime
	Fn_Check  = true
	Fn_CTime  = KnifeCMS.Data.FormatDateTime(SysTime,8)
	Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Visitor &":ID",Array("UserID:"& User_ID,"VisitorID:"& UserID,"CTime:"& Fn_CTime),"")
    If Not(Fn_Rs.Eof) Then Fn_Check = false
    KnifeCMS.DB.CloseRs Fn_Rs
	If Fn_Check Then Call KnifeCMS.DB.AddRecord(DBTable_Visitor,Array("UserID:"& User_ID,"VisitorID:"& UserID,"VTime:"& SysTime,"CTime:"& Fn_CTime))
End Function

Public Function GetMyNotifyCount(ByVal BV_NotifyType)
	Dim Fn_Rs,Fn_Count : Fn_Count=0
	If Not(1=Logined) Then GetMyNotifyCount=Fn_Count : Exit Function
	Set Fn_Rs=KnifeCMS.DB.GetRecordBySQL("SELECT COUNT(*) FROM ["& DBTable_Notify &"] WHERE [SNUserID]="& UserID &" AND [NotifyType]='"& BV_NotifyType &"' AND [IsTreated]=0 ")
	If Not(Fn_Rs.Eof) Then Fn_Count=Fn_Rs(0)
	KnifeCMS.DB.CloseRs Fn_Rs
	GetMyNotifyCount = Fn_Count
End Function

'获取购物车商品数量
Public Function GetMyCartShopNum()
	Dim Fn_CartJSONString,Fn_TheSkus,Fn_Skus,Fn_ThePacks,Fn_Packs,Fn_Num,Fn_TotalNum,Fn_ii
	Fn_CartJSONString = KnifeCMS.Cookie.GetCookie("mycart")
	If KnifeCMS.Data.IsNul(Fn_CartJSONString) Then GetMyCartShopNum=0 : Exit Function
	Fn_TheSkus  = KnifeCMS.Data.RegSubMatches(Fn_CartJSONString,"""theskus"":\[([\s\S]*?)\]")(0,0)
	Fn_ThePacks = KnifeCMS.Data.RegSubMatches(Fn_CartJSONString,"""thepacks"":\[([\s\S]*?)\]")(0,0)
	Fn_Skus  = KnifeCMS.Data.RegSubMatches(Fn_TheSkus,"\{([\s\S]*?)\}")
	Fn_Packs = KnifeCMS.Data.RegSubMatches(Fn_ThePacks,"\{([\s\S]*?)\}")
	For Fn_ii=0 To Ubound(Fn_Skus,2)
		Fn_Num = KnifeCMS.Data.CLng(KnifeCMS.Data.RegSubMatches(Fn_Skus(0,Fn_ii),"""num"":""([\s\S]*?)""")(0,0))
		If Fn_Num>0 Then Fn_TotalNum=Fn_TotalNum+1
	Next
	For Fn_ii=0 To Ubound(Fn_Packs,2)
		Fn_Num = KnifeCMS.Data.CLng(KnifeCMS.Data.RegSubMatches(Fn_Packs(0,Fn_ii),"""num"":""([\s\S]*?)""")(0,0))
		If Fn_Num>0 Then Fn_TotalNum=Fn_TotalNum+1
	Next
	GetMyCartShopNum = Fn_TotalNum
End Function

'获取通知信息
Public Function GetUserNotifyNum()
	If Logined=1 And UserCenter Then
		MySystemNotifyNum  = GetMyNotifyCount("acceptfriend")+GetMyNotifyCount("rejectfriend")
		MyFriendRequestNum = GetMyNotifyCount("addfriend")
	End If
End Function

Function GetHttpPage(url, charset) 
    Dim http 
    Set http = Server.createobject("Msxml2.ServerXMLHTTP")
    http.Open "GET", url, false
    http.Send() 
    If http.readystate<>4 Then
        Exit Function 
    End If 
    GetHttpPage = BytesToStr(http.ResponseBody, charset)
    Set http = Nothing
End function

Function BytesToStr(body, charset)
    Dim objStream
    Set objStream = Server.CreateObject("Adodb.Stream")
    objStream.Type = 1
    objStream.Mode = 3
    objStream.Open
    objStream.Write body
    objStream.Position = 0
    objStream.Type = 2
    objStream.Charset = charset
    BytesToStr = objStream.ReadText 
    objStream.Close
    Set objStream = Nothing
End Function

Const SystemName  = "knifecms"
Dim PoweredBy: PoweredBy = "Powered by <a href=""http://www.knifecms.com/"" target=""_blank""><font style=""color:#CC3202; font-weight:bold;"">Kni"&"feCMS</font> "& Sys_Version &"</a>"
Dim SitePoweredBy:SitePoweredBy=PoweredBy

Dim CertStat:CertStat=0
Dim CertContent:CertContent=GetHttpPage("http://www.knifecms.com/cer/?a=search&h="& Domain,"UTF-8")
if instr(CertContent,"已授权")>0 then
	PoweredBy="KnifeCMS正版授权"
	SitePoweredBy=""
end if

Dim CertificateLink : CertificateLink = "<a href=""http://www.knifecms.com/cer/?a=search&h="& Domain &""" target=""_blank"">授权验证</a>"
Dim OS
Set OS = New Class_KnifeCMS_Main
Class Class_KnifeCMS_Main

	Public UserSession
	Public TempRegion
	Private Pv_IsInRegion,Pv_SystemName
	Private Pv_Rs
	
	Private Sub Class_Initialize()
		IsInRegion    = false
	End Sub
	
	Private Sub Class_Terminate()
		
		Set UserSession = Nothing
		Set KnifeCMS = Nothing
		On Error Resume Next
		Set Feeds   = Nothing
		Set Friend  = Nothing
		Set Visitor = Nothing
		Set Notify  = Nothing
		Set Cart    = Nothing
		If Err.Number <> 0 Then Err.Clear
		if IsObject(TempRegion) Then Set TempRegion = Nothing
	End Sub
	
	Public Property Let IsInRegion(ByVal BV_String)
		Pv_IsInRegion = BV_String
	End Property
	Public Property Get IsInRegion()
		IsInRegion = Pv_IsInRegion
	End Property
	
	'检查BV_Region_Path是否在BV_Region(json对象)中
	Public Function CheckIsInRegion(ByVal BV_Region_Path,ByVal BV_Region)
		if BV_Region_Path="," Or KnifeCMS.Data.IsNul(BV_Region_Path) Then
			Pv_IsInRegion=false
		else
			if BV_Region.length>0 Then
				Dim Fn_kk
				for Fn_kk=0 to BV_Region.length-1
					if trim(BV_Region.get(Fn_kk).p)=trim(BV_Region_Path) then
						Pv_IsInRegion=true
						Exit For
					else
						if Instr(BV_Region_Path,BV_Region.get(Fn_kk).p)>0 Then
							Call CheckIsInRegion(BV_Region_Path,BV_Region.get(Fn_kk).c)
						end if
					end if
				next
			else
				Pv_IsInRegion=false
			end if
		end if
	End Function
	
	'根据路径BV_Region_Path获取地区BV_Region(json对象)中其对应的子地区
	Public Function GetRegion(ByVal BV_Region_Path,ByVal BV_Region)
		TempRegion = ""
		if BV_Region_Path="," Or KnifeCMS.Data.IsNul(BV_Region_Path) Then
			Set TempRegion=BV_Region '一级地区
		else
			if BV_Region.length>0 then
				Dim fn_Array : fn_Array = split(BV_Region_Path,",")
				Dim fn_Grade : fn_Grade = Ubound(fn_Array) - 1
				Dim Fn_kk
				for Fn_kk=0 to BV_Region.length-1
					if BV_Region.get(Fn_kk).p=BV_Region_Path then
						Set TempRegion=BV_Region.get(Fn_kk).c
						exit for
					else
						if Instr(BV_Region_Path,BV_Region.get(Fn_kk).p)>0 then
							Call GetRegion(BV_Region_Path,BV_Region.get(Fn_kk).c)
						end if
					end if
				next
			else
				TempRegion = ""
			end if
		end if
	End Function
	
	Public Sub CheckUserLogin()
		Dim Fn_Rs,Fn_Username,Fn_UserPassword,Fn_TruePassword,Fn_DataArray
		Logined      = 0
		UserID       = KnifeCMS.Data.CLng(KnifeCMS.Cookie.GetCookie("my:id"))
		Username     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.Data.RegReplace(KnifeCMS.Cookie.GetCookie("my:name"),"[\(\)]",""),"[\s]",""),50)
		TruePassword = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.Cookie.GetCookie("my:pw"),"[^0-9a-zA-Z]",""),32)
		'判断Cookie记录是否有效
		If Not(UserID>0) Or KnifeCMS.Data.IsNul(Username) Or KnifeCMS.Data.IsNul(TruePassword) Then Exit Sub
		
		Select Case KnifeCMS.Data.CLng(KnifeCMS.SysConfig("UserCookieType"))
		Case 0
		'Cookie+Session双保险模式
			Set UserSession=KnifeCMS.CreateXmlDoc("msxml2.FreeThreadedDOMDocument"& MsxmlVersion)
			'判断是否有该用户的session记录，没有session记录或session记录与cookie记录不一致则为非登陆状态{
			If Not UserSession.loadxml(KnifeCMS.Session.GetSession("UserID") & "") Then Exit Sub
			If KnifeCMS.Data.CLng(UserSession.documentElement.selectSingleNode("userinfo/@userid").text) <> UserID Then Exit Sub
			If UserSession.documentElement.selectSingleNode("userinfo/@truepassword").text <> TruePassword Then Exit Sub
			'}判断完毕
			UserPassword = UserSession.documentElement.selectSingleNode("userinfo/@userpassword").text
			Set Fn_Rs=KnifeCMS.DB.Execute("SELECT Username,Password,TruePassword,Email,BigHead,HeadImg,SysUserID,Disable FROM ["& DBTable_Members &"] WHERE ID="& UserID &"")
			If Not(Fn_Rs.Eof) then Fn_DataArray = Fn_Rs.GetRows()
			KnifeCMS.DB.CloseRs Fn_Rs
			If IsArray(Fn_DataArray) Then
				Fn_Username     = Fn_DataArray(0,0)
				Fn_UserPassword = Fn_DataArray(1,0)
				Fn_TruePassword = Fn_DataArray(2,0)
				If Fn_Username=Username And Fn_UserPassword=UserPassword And Fn_TruePassword=TruePassword Then
					Logined         = 1
					UserEmail       = Fn_DataArray(3,0)
					UserBigHead     = KnifeCMS.Data.URLDecode(Fn_DataArray(4,0))
					UserSmallHead   = KnifeCMS.Data.URLDecode(Fn_DataArray(5,0))
					UserDisable     = KnifeCMS.Data.CLng(Fn_DataArray(7,0))
				End If
			End If
		Case 1
		'Cookie保险模式
			Set Fn_Rs=KnifeCMS.DB.Execute("SELECT Username,Password,TruePassword,Email,BigHead,HeadImg,SysUserID,Disable FROM ["& DBTable_Members &"] WHERE ID="& UserID &"")
			If Not(Fn_Rs.Eof) then Fn_DataArray = Fn_Rs.GetRows()
			KnifeCMS.DB.CloseRs Fn_Rs
			If IsArray(Fn_DataArray) Then
				Fn_Username     = Fn_DataArray(0,0)
				Fn_TruePassword = Fn_DataArray(2,0)
				If Fn_Username=Username And Fn_TruePassword=TruePassword Then
					Logined         = 1
					UserEmail       = Fn_DataArray(3,0)
					UserBigHead     = KnifeCMS.Data.URLDecode(Fn_DataArray(4,0))
					UserSmallHead   = KnifeCMS.Data.URLDecode(Fn_DataArray(5,0))
					UserDisable     = KnifeCMS.Data.CLng(Fn_DataArray(7,0))
				End If
			End If
		End Select
	End Sub
	
	'会员用户登陆
	'用户不存在:返回"null"  会员已经被冻结:返回"disable"  其他错误:返回"error_描述"
	Public Function UserLogining()
	    '验证码
		If Not(OS.ValidCheckCode) Then UserLogining="error_checkcode" : Exit Function
		
		Call SetUserSession(array(UserID,Fn_Username,UserPassword,TruePassword))
		
		Dim Fn_Rs,Fn_ReturnString
		Dim Fn_IP       : Fn_IP       = KnifeCMS.GetClientIP()
		Dim Fn_Username : Fn_Username = KnifeCMS.ParseUserName(KnifeCMS.GetForm("post","username"))
		Dim Fn_Password : Fn_Password = KnifeCMS.ParsePassWord(KnifeCMS.GetForm("post","password"))
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,Username,Password,TruePassword,Disable FROM ["& DBTable_Members &"] WHERE Username='"& Fn_Username &"'")
		If Fn_Rs.Eof Then
			UserLogining="null"
		Else
			If KnifeCMS.Data.CLng(Fn_Rs("Disable"))<>0 Then
				UserLogining="disable"
			Else
				If Fn_Password = Fn_Rs("Password") Then
					UserID       = Fn_Rs("ID")
					Username     = Fn_Rs("Username")
					UserPassword = Fn_Rs("Password")
					TruePassword = UpdateTruePassword(DBTable_Members,UserID)
					If TruePassword <> False Then
						Fn_ReturnString = "ok"
						'写入用户cookies
						Call SetUserCookies(UserID,Username,TruePassword)
						'写入用户session
						Call SetUserSession(Array(UserID,Username,UserPassword,TruePassword))
						'更新登陆次数\登陆时间\IP
						Call KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("LoginTimes=LoginTimes+1","LastLoginTime:"& SysTime,"IP:"& Fn_IP),Array("ID:"& UserID))
					End If
				End If
			End If
		End If
		
		KnifeCMS.DB.CloseRs Fn_Rs
		If Fn_ReturnString = "ok" Then
			If Api_Enable Then
				'------------------
				'这里写系统整合代码
				'------------------
			End If
		End If
		UserLogining = Fn_ReturnString
	End Function
	
	'会员注册
	Public Function UserRegistering()
		'验证码
		If Not(OS.ValidCheckCode) Then UserRegistering="error_checkcode" : Exit Function
	
		Dim Fn_Rs,Fn_Result,Fn_ReturnString,Fn_ID
		Dim Fn_IP       : Fn_IP       = KnifeCMS.GetClientIP()
		Dim Fn_Username : Fn_Username = KnifeCMS.ParseUserName(KnifeCMS.GetForm("post","username"))
		Dim Fn_Password : Fn_Password = KnifeCMS.ParsePassWord(KnifeCMS.GetForm("post","password"))
		Dim Fn_Email    : Fn_Email    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","email")),50)
		
		If "ok"<>CheckEmail() Then UserRegistering="error_email_used" : Exit Function
		If "ok"<>CheckUsername() Then UserRegistering="error_username_used" : Exit Function

		Fn_Result = KnifeCMS.DB.AddRecord(DBTable_Members,Array("Username:"& Fn_Username,"Password:"& Fn_Password,"Email:"& Fn_Email,"SysUserID:0","Disable:0","Recycle:0"))
		If Fn_Result Then
			Logined  = 1
			Set Fn_Rs=KnifeCMS.DB.GetRecord(DBTable_Members &":ID,Username,Password",Array("Username:"& Fn_Username,"Password:"& Fn_Password,"Email:"& Fn_Email),"")
			If Not(Fn_Rs.Eof) Then
				UserID       = Fn_Rs("ID")
				Username     = Fn_Rs("Username")
				UserPassword = Fn_Rs("Password")
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
			TruePassword = UpdateTruePassword(DBTable_Members,UserID)
			If TruePassword <> False Then
				Fn_ReturnString = "ok"
				'写入用户cookies
				Call SetUserCookies(UserID,Username,TruePassword)
				'写入用户session
				Call SetUserSession(array(UserID,Fn_Username,UserPassword,TruePassword))
				'更新登陆次数\登陆时间\IP
				Call KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("LoginTimes=0","RegTime:"& SysTime,"IP:"& Fn_IP),Array("ID:"& UserID))
			End If
		End If

		If Fn_ReturnString = "ok" Then
			If Api_Enable Then
				'------------------
				'这里写系统整合代码
				'------------------
			End If
		End If
		UserRegistering = Fn_ReturnString
	End Function
	
	'更新会员用户动态密码 失败:返回false 成功:返回动态密码
	Public Function UpdateTruePassword(ByVal BV_DBTable,ByVal BV_UserID)
		If Not KnifeCMS.Data.CLng(BV_UserID)>0 Then UpdateTruePassword=False : Exit Function
		Dim Fn_Rs,Fn_Result,Fn_UserID,Fn_Password,Fn_TruePassword
		Fn_Password = KnifeCMS.DB.GetFieldByField(BV_DBTable,"Password","ID",BV_UserID)
		If Not KnifeCMS.Data.IsNul(Fn_Password) Then
			Fn_TruePassword = KnifeCMS.MD5.Encrypt(Fn_Password & KnifeCMS.CreateSysID)
			Fn_Result = KnifeCMS.DB.UpdateRecord(BV_DBTable,Array("TruePassword:"& Fn_TruePassword),Array("ID:"& BV_UserID))
			If Fn_Result Then UpdateTruePassword=Fn_TruePassword
		End If
	End Function
	
	Public Function SetUserCookies(ByVal BV_UserID, ByVal BV_Username, ByVal BV_Password)
		Call KnifeCMS.Cookie.SetCookie("my:id",BV_UserID,KnifeCMS.Cookie.LoginCookieTime)
		Call KnifeCMS.Cookie.SetCookie("my:name",BV_Username,KnifeCMS.Cookie.LoginCookieTime)
		Call KnifeCMS.Cookie.SetCookie("my:pw",BV_Password,KnifeCMS.Cookie.LoginCookieTime)
	End Function
	
	'用户注册/登陆后保存session
	Public Function SetUserSession(ByVal BV_UserInfo)
		Set UserSession = KnifeCMS.CreateXmlDoc("msxml2.FreeThreadedDOMDocument"& MsxmlVersion)	
		UserSession.Loadxml "<?xml version=""1.0"" encoding=""gb2312""?><xml><userinfo userid="""& BV_UserInfo(0) &""" username="""& BV_UserInfo(1) &""" userpassword="""& BV_UserInfo(2) &""" truepassword="""& BV_UserInfo(3) &""" usergroupid="""" cometime="""" activetime="""" statuserid=""0"" /></xml>"
		UserSession.documentElement.selectSingleNode("userinfo/@cometime").text=Now()
		UserSession.documentElement.selectSingleNode("userinfo/@activetime").text=DateAdd("s",-3600,Now())
		Call KnifeCMS.Session.SetSession("UserID",OS.UserSession.xml)
	End Function
	
	Public Function LoginOut()
		Logined = 0 : UserID = "" : Username = "" : UserPassword = ""
		KnifeCMS.Cookie.RemoveCookie("my:pw")
		KnifeCMS.Cookie.RemoveCookie("my:id")
		If 1<>KnifeCMS.Data.CLng(KnifeCMS.Cookie.GetCookie("savemyname")) Then KnifeCMS.Cookie.RemoveCookie("my:name")
		KnifeCMS.Session.RemoveSession("UserID")
	End Function
	
	'检查Email是否已经被某会员注册过了
	Public Function EmailHaveUsed(ByVal BV_Email)
		Dim Fn_Check : Fn_Check = True
		Set Pv_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID FROM ["& DBTable_Members &"] WHERE Email='"& BV_Email &"'")
		If Pv_Rs.eof Then Fn_Check = False
		KnifeCMS.DB.CloseRs Pv_Rs
		EmailHaveUsed = Fn_Check
	End Function
	
	Public Function CheckEmail()
		Dim Fn_Result
		Dim Fn_Email : Fn_Email = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","email")),50)
		If KnifeCMS.Data.IsEmail(Fn_Email) Then
			If EmailHaveUsed(Fn_Email) Then
				Fn_Result = "used"
			Else
				Fn_Result = "ok"
			End If
		Else
		    Fn_Result = "error"
		End If
		CheckEmail = Fn_Result
	End Function
	
	'检查用户名是否已经被某会员注册过了
	Public Function UsernameHaveUsed(ByVal BV_Username)
		Dim Fn_Check : Fn_Check = True
		Set Pv_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID FROM ["& DBTable_Members &"] WHERE Username='"& BV_Username &"'")
		If Pv_Rs.eof Then Fn_Check = False
		KnifeCMS.DB.CloseRs Pv_Rs
		UsernameHaveUsed = Fn_Check
	End Function
	
	Public Function CheckUsername()
		Dim Fn_Result
		Dim Fn_Username : Fn_Username = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","username")),50)
		Dim Fn_Len : Fn_Len = KnifeCMS.Data.StringLength(Fn_Username)
		'Echo Fn_Username
		If InStr(Fn_Username,"&nbsp;")>0 Then CheckUsername = "error" : Exit Function
		If Fn_Len<3 Or Fn_Len>15 Then
			Fn_Result = "error"
		Else
		    If UsernameHaveUsed(Fn_Username) Then
				Fn_Result = "used"
			Else
				Fn_Result = "ok"
			End If
		End If
		CheckUsername = Fn_Result
	End Function
	
	'找回密码
	Public Function ForgetPassword()
		'验证码
		If Not(OS.ValidCheckCode) Then ForgetPassword="error_checkcode" : Exit Function
		Dim Fn_Account,Fn_UserExist,Fn_Result,Fn_ReturnString
		Dim Fn_SQL,Fn_UserID,Fn_Username,Fn_Email,Fn_Password,Fn_TruePassword,Fn_RegTime,Fn_LastLoginTime,Fn_Disable,Fn_Recycle
		Dim Fn_MailTitle,Fn_MailBody,Fn_URL,Fn_Hash,Fn_GetPassword_Link,Fn_WebSite,Fn_IP
		Fn_Account = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","useraccount")),50)
		If KnifeCMS.Data.IsNul(Fn_Account) Then ForgetPassword="error_user_not_exist" : Exit Function
		'获得用户信息
		If KnifeCMS.Data.IsEmail(Fn_Account) Then
			Fn_SQL = "SELECT ID,Username,Email,Password,TruePassword,RegTime,LastLoginTime,Disable,Recycle FROM ["& DBTable_Members &"] WHERE Email='"& Fn_Account &"'"
		Else
			Fn_SQL = "SELECT ID,Username,Email,Password,TruePassword,RegTime,LastLoginTime,Disable,Recycle FROM ["& DBTable_Members &"] WHERE Username='"& Fn_Account &"'"
		End If
		Set Pv_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_SQL)
		If Pv_Rs.Eof Then
			Fn_UserExist = False
		Else
			Fn_UserExist = True
			Fn_UserID        = Pv_Rs("ID")
			Fn_Username      = Pv_Rs("Username")
			Fn_Email         = Pv_Rs("Email")
			Fn_Password      = Pv_Rs("Password")
			Fn_TruePassword  = Pv_Rs("TruePassword")
			Fn_RegTime       = Pv_Rs("RegTime")
			Fn_LastLoginTime = Pv_Rs("LastLoginTime")
			Fn_Disable       = KnifeCMS.Data.CLng(Pv_Rs("Disable"))
			Fn_Recycle       = KnifeCMS.Data.CLng(Pv_Rs("Recycle"))
		End If
		KnifeCMS.DB.CloseRs Pv_Rs
		If Fn_UserExist Then
			If Fn_Disable=1 Then ForgetPassword="error_user_is_disable" : Exit Function
			If Fn_Recycle=1 Then ForgetPassword="error_user_is_recycle" : Exit Function
			'获取并处理邮件内容
			Set Pv_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT TOP 1 MailTitle,MailBody FROM ["& DBTable_MailTemplate &"] WHERE MailType='getpassword'")
			If Not(Pv_Rs.Eof) Then
				Fn_MailTitle = KnifeCMS.Data.HTMLDecode(Pv_Rs("MailTitle"))
				Fn_MailBody  = KnifeCMS.Data.HTMLDecode(Pv_Rs("MailBody"))
			End If
			KnifeCMS.DB.CloseRs Pv_Rs
			Fn_Hash = KnifeCMS.MD5.Encrypt(Fn_UserID &":"& Fn_Password &":"& Fn_TruePassword &":"& Fn_LastLoginTime)
			Fn_URL  = SiteURL & SystemPath &"index.asp?ctl=getpassword&hash="& Fn_Hash &"&uid="& Fn_UserID
			Fn_GetPassword_Link = "<a href="""& Fn_URL &""" target=_blank>"& Fn_URL &"</a>"
			Fn_WebSite          = "<a href="""& SiteURL & SystemPath &""" target=_blank>"& SiteURL & SystemPath &"</a>"
			Fn_IP               = KnifeCMS.GetClientIP()
			Fn_MailBody = Replace(Fn_MailBody,"{tpl:username}",Fn_Username)
			Fn_MailBody = Replace(Fn_MailBody,"{tpl:sitename}",SiteName)
			Fn_MailBody = Replace(Fn_MailBody,"{tpl:forgetpassword_link}",Fn_GetPassword_Link)
			Fn_MailBody = Replace(Fn_MailBody,"{tpl:website}",Fn_WebSite)
			Fn_MailBody = Replace(Fn_MailBody,"{tpl:ip}",Fn_IP)
			
			'发送邮件
			Fn_Result = KnifeCMS.SendEmail("",Fn_Email,Fn_MailTitle,Fn_MailBody)
			If Fn_Result=True Then
				Fn_ReturnString = "ok"
			Else
				Fn_ReturnString = Fn_Result
			End If
		Else
			Fn_ReturnString = "error_user_not_exist"
		End If
		'返回结果信息
		ForgetPassword  = Fn_ReturnString
	End Function
	
	'重设密码
	Public Function ResetPassword()
		'验证码
		If Not(OS.ValidCheckCode) Then ResetPassword="error_checkcode" : Exit Function
		
		Dim Fn_Rs,Fn_UserExist,Fn_Check,Fn_Hash,Fn_GetHash,Fn_Result,Fn_ReturnString
		Dim Fn_SQL,Fn_UserID,Fn_Username,Fn_Email,Fn_Password,Fn_TruePassword,Fn_RegTime,Fn_LastLoginTime,Fn_Disable,Fn_Recycle
		Fn_Check = False
		Fn_UserExist = False
		'用户信息
		Fn_UserID =  KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","uid"))
		If Fn_UserID>0 Then
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,Username,Email,Password,TruePassword,RegTime,LastLoginTime,Disable,Recycle FROM ["& DBTable_Members &"] WHERE ID="& Fn_UserID &"")
			If Not(Fn_Rs.Eof) Then
				Fn_UserExist     = True
				Fn_Username      = Fn_Rs("Username")
				Fn_Email         = Fn_Rs("Email")
				Fn_Password      = Fn_Rs("Password")
				Fn_TruePassword  = Fn_Rs("TruePassword")
				Fn_RegTime       = Fn_Rs("RegTime")
				Fn_LastLoginTime = Fn_Rs("LastLoginTime")
				Fn_Disable       = KnifeCMS.Data.CLng(Fn_Rs("Disable"))
				Fn_Recycle       = KnifeCMS.Data.CLng(Fn_Rs("Recycle"))
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
		End If
		If Fn_UserExist=True Then
		    If Fn_Disable=1 Then ResetPassword="error_user_is_disable" : Exit Function
			If Fn_Recycle=1 Then ResetPassword="error_user_is_recycle" : Exit Function
		    '分析是否是有效的地址
			Fn_Hash    = KnifeCMS.MD5.Encrypt(Fn_UserID &":"& Fn_Password &":"& Fn_TruePassword &":"& Fn_LastLoginTime)
			Fn_GetHash = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","hash"),"[^0-9a-zA-Z]",""),250)
			If Fn_Hash=Fn_GetHash Then
				Fn_Check = True
			Else
				Fn_Check = False
				ResetPassword = "error_getpassword_link_disable" : Exit Function
			End If
		Else
			ResetPassword = "error_user_not_exist" : Exit Function
		End If
		'重新设置密码
		If Fn_Check Then
			Fn_Password = KnifeCMS.ParsePassWord(KnifeCMS.GetForm("both","password"))
			If KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("Password:"& Fn_Password),Array("ID:"& Fn_UserID)) Then
				Fn_ReturnString = "ok"
			Else
				Fn_ReturnString = "error_update_record"
			End If
		End If
		ResetPassword = Fn_ReturnString
	End Function
	
	'生成唯一的单据ID(18位)
	Public Function CreateBillID(ByVal BV_Type)
		Dim Fn_ii,Fn_BillID,Fn_Table
		Select Case BV_Type
			Case "paymentbill"
				Fn_Table = DBTable_PaymentBill
			Case "refundbill"
			    Fn_Table = DBTable_RefundBill
			Case "shipbill"
			    Fn_Table = DBTable_ShipBill
			Case "reshipbill"
			    Fn_Table = DBTable_ReShipBill
			Case Else
			    KnifeCMS.Error.Msg = "OS.CreateBillID(BV_Type) Error"
			    KnifeCMS.Error.Raise 10
		End Select
		Fn_BillID = KnifeCMS.Data.FormatDateTime(SysTime,9) & KnifeCMS.MakeRandom(4)
		If Len(Fn_BillID) <> 18 Then
			KnifeCMS.Error.Msg = "OS.CreateBillID(BV_Type) Error"
			KnifeCMS.Error.Raise 10
		End If
		For Fn_ii=0 To 10000
			If Not(KnifeCMS.DB.CheckIsRecordExistByField(Fn_Table,Array("BillID:"& Fn_BillID))) Then Exit For
			Fn_BillID = KnifeCMS.Data.FormatDateTime(SysTime,9) & KnifeCMS.MakeRandom(4)
		Next
		CreateBillID = Fn_BillID
	End Function
	
	'生成唯一的支付交易ID(20位)
	Public Function CreatePayTradeNo()
		Dim Fn_ii,Fn_TradeNo
		Fn_TradeNo = KnifeCMS.Data.FormatDateTime(SysTime,9) & KnifeCMS.MakeRandom(6)
		If Len(Fn_TradeNo) <> 20 Then
			KnifeCMS.Error.Msg = "OS.CreatePayTradeNo() Error"
			KnifeCMS.Error.Raise 10
		End If
		For Fn_ii=0 To 100000
			If Not(KnifeCMS.DB.CheckIsRecordExistByField(DBTable_PayTradeLog,Array("TradeNo:"& Fn_TradeNo))) Then Exit For
			Fn_TradeNo = KnifeCMS.Data.FormatDateTime(SysTime,9) & KnifeCMS.MakeRandom(6)
		Next
		CreatePayTradeNo = Fn_TradeNo
	End Function
	
	Public Function GetAdjunctGoods(ByVal Fn_AdjunctID)
		Fn_AdjunctID = KnifeCMS.Data.CLng(Fn_AdjunctID)
		If Not(Fn_AdjunctID>0) Then GetAdjunctGoods="" : Exit Function
		Dim Fn_Rs,Fn_ReturnString
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT AdjunctGoodsID FROM ["& DBTable_GoodsAdjunctGoods &"] WHERE AdjunctID="& Fn_AdjunctID &"")
		Do While Not(Fn_Rs.Eof)
			If Fn_ReturnString<>"" Then 
				Fn_ReturnString = Fn_ReturnString &","& Fn_Rs(0)
			Else
				Fn_ReturnString = Fn_Rs(0)
			End If
			Fn_Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		GetAdjunctGoods = Fn_ReturnString
	End Function
	
	Public Function SaveAddress(ByVal BV_UserID,ByVal BV_Region,ByVal BV_Address,ByVal BV_Zip,ByVal BV_Name,ByVal BV_Telephone,ByVal BV_Mobile)
		Dim Fn_Check,Fn_SysID,Fn_Result,Fn_ReturnString
		Dim Fn_UserID,Fn_Region,Fn_Address,Fn_Zip,Fn_Name,Fn_Mobile,Fn_Telephone
		'判断是否是当前登陆的用户
		Fn_UserID     = KnifeCMS.Data.CLng(BV_UserID)
		If Not(1=Logined) Or Fn_UserID<>UserID Then SaveAddress="unlogined" : Exit Function
		Fn_Region     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(BV_Region,"[^0-9,]",""),50)
		Fn_Address    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(BV_Address),250)
		Fn_Zip        = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(BV_Zip,"[^0-9\-]",""),20)
		Fn_Name       = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(BV_Name),50)
		Fn_Telephone  = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(BV_Telephone,"[^0-9\-,]",""),50)
		Fn_Mobile     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(BV_Mobile,"[^0-9,]",""),50)
		'没有当前用户的数据记录则添加，有则修改
		If KnifeCMS.Data.IsNul(KnifeCMS.DB.GetFieldByField(DBTable_Members_Addrs,"ID","UserID",Fn_UserID)) Then
			Fn_SysID  = KnifeCMS.CreateSysID()
			Fn_Result = KnifeCMS.DB.AddRecord(DBTable_Members_Addrs,Array("SysID:"& Fn_SysID,"UserID:"& Fn_UserID,"Name:"& Fn_Name,"Area:","Region:"& Fn_Region,"Country:","Province:","City:","Address:"& Fn_Address,"Zip:"& Fn_Zip,"Telephone:"& Fn_Telephone,"Mobile:"& Fn_Mobile,"IsDefault:0"))
		Else
			Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_Members_Addrs,Array("Name:"& Fn_Name,"Region:"& Fn_Region,"Address:"& Fn_Address,"Zip:"& Fn_Zip,"Telephone:"& Fn_Telephone,"Mobile:"& Fn_Mobile),Array("UserID:"&Fn_UserID))
		End If
		If Fn_Result Then Fn_ReturnString = "ok"
		SaveAddress = Fn_ReturnString
	End Function
	
	'获取配送方式数据，以JSON格式数据返回
	Public Function GetDeliveryData()
		Dim Fn_Result,Fn_ReturnString,Fn_TempString
		Dim Fn_Rs,Fn_Rs2
		Dim Fn_ID,Fn_DeliveryName,Fn_Config,Fn_Expressions,Fn_Intro,Fn_Protect,Fn_ProtectRate,Fn_ProtectMinPrice,Fn_HasCod,Fn_FirstPrice,Fn_FirstUnit,Fn_ContinuePrice,Fn_ContinueUnit,Fn_AreaFeeSet,Fn_SetAreaDefaultFee
		Dim Fn_AreaConfig,Fn_AreaName_Group,Fn_AreaID_Group,Fn_AreaFirstPrice,Fn_AreaContinuePrice,Fn_AreaExpressions,Fn_AreaTempString,Fn_AreaString
		Dim Fn_i : Fn_i = 0
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_Delivery &"] WHERE Disabled=0 ORDER BY OrderNum ASC,ID ASC")
		Do While Not(Fn_Rs.Eof)
			Fn_i              = Fn_i + 1
			Fn_ID             = Fn_Rs("ID")
			Fn_DeliveryName   = KnifeCMS.Data.Escape(Fn_Rs("DeliveryName"))
			Fn_Config         = Fn_Rs("Config")
			Fn_Expressions    = Fn_Rs("Expressions")
			Fn_Intro          = KnifeCMS.Data.Escape(KnifeCMS.DAta.HTMLDecode(Fn_Rs("Intro")))
			Fn_Protect        = Fn_Rs("Protect")
			Fn_ProtectRate    = Fn_Rs("ProtectRate")
			Fn_ProtectMinPrice= Fn_Rs("ProtectMinPrice")
			Fn_HasCod         = Fn_Rs("HasCod")
			Fn_FirstPrice     = KnifeCMS.Data.FormatDouble(KnifeCMS.GetConfigParameter(Fn_Config,"FirstPrice"))
			Fn_FirstUnit      = KnifeCMS.Data.CLng(KnifeCMS.GetConfigParameter(Fn_Config,"FirstUnit"))
			Fn_ContinuePrice  = KnifeCMS.Data.FormatDouble(KnifeCMS.GetConfigParameter(Fn_Config,"ContinuePrice"))
			Fn_ContinueUnit   = KnifeCMS.Data.CLng(KnifeCMS.GetConfigParameter(Fn_Config,"ContinueUnit"))
			Fn_AreaFeeSet     = KnifeCMS.Data.CLng(KnifeCMS.GetConfigParameter(Fn_Config,"AreaFeeSet"))
			Fn_SetAreaDefaultFee = KnifeCMS.Data.CLng(KnifeCMS.GetConfigParameter(Fn_Config,"SetAreaDefaultFee"))
			Fn_AreaString     = ""
			Set Fn_Rs2 = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_Delivery_Area &"] WHERE DeliveryID="& Fn_ID &" ORDER BY OrderNum ASC")
			Do While Not(Fn_Rs2.Eof)
				Fn_AreaConfig        = Fn_Rs2("Config")
                Fn_AreaName_Group    = KnifeCMS.Data.Escape(Fn_Rs2("AreaName_Group"))
                Fn_AreaID_Group      = Fn_Rs2("AreaID_Group")
				Fn_AreaExpressions   = Fn_Rs2("Expressions")
                Fn_AreaFirstPrice    = KnifeCMS.Data.FormatDouble(KnifeCMS.GetConfigParameter(Fn_AreaConfig,"FirstPrice"))
                Fn_AreaContinuePrice = KnifeCMS.Data.FormatDouble(KnifeCMS.GetConfigParameter(Fn_AreaConfig,"ContinuePrice"))
				Fn_AreaTempString = "{""areaname"":"""& Fn_AreaName_Group &""",""areaid"":"& Fn_AreaID_Group &",""areafirstprice"":"""& Fn_AreaFirstPrice &""",""areacontinueprice"":"""& Fn_AreaContinuePrice &""",""expressions"":"""& Fn_AreaExpressions &"""}"
				If Fn_AreaString="" Then Fn_AreaString=Fn_AreaTempString : Else Fn_AreaString=Fn_AreaString &","& Fn_AreaTempString
				Fn_Rs2.Movenext()
			Loop
			KnifeCMS.DB.CloseRs Fn_Rs2
			
			Fn_TempString   = "{""id"":"""& Fn_ID &""",""name"":"""& Fn_DeliveryName &""",""expressions"":"""& Fn_Expressions &""",""intro"":"""& Fn_Intro &""",""protect"":"""& Fn_Protect &""",""protectrate"":"""& Fn_ProtectRate &""",""protectminprice"":"""& Fn_ProtectMinPrice &""",""hascod"":"""& Fn_HasCod &""",""firstprice"":"""& Fn_FirstPrice &""",""firstunit"":"""& Fn_FirstUnit &""",""continueprice"":"""& Fn_ContinuePrice &""",""continueunit"":"""& Fn_ContinueUnit &""",""areafeeset"":"""& Fn_AreaFeeSet &""",""setareadefaultfee"":"""& Fn_SetAreaDefaultFee &""",""area"":["& Fn_AreaString &"]}"
			If Fn_ReturnString="" Then Fn_ReturnString = Fn_TempString : Else Fn_ReturnString = Fn_ReturnString &","& Fn_TempString
			Fn_Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		Fn_ReturnString = "{""delivery"":["& Fn_ReturnString &"]}"
		GetDeliveryData = Fn_ReturnString
	End Function
	
	'根据公式计算配送费用
	Public Function ShippingFee(ByVal BV_E, ByVal BV_W)
		Dim Fn_E,Fn_RE,Fn_Ms,Fn_M,Fn_Int,Fn_Result
		Fn_E = BV_E
		Set Fn_RE = New RegExp : Fn_RE.Pattern = "{([\s\S]+?)}"
		Set Fn_Ms = Fn_RE.Execute(Fn_E)
		For Each Fn_M in Fn_Ms
			Fn_Int = Eval(Replace(Fn_M.SubMatches(0),"w",BV_W))
			If Fn_Int<0 Then Fn_Int = 0
			Fn_E = Replace(Fn_E,Fn_M.Value,Fn_Int)
		Next
		Set Fn_Ms = Nothing
		Set Fn_RE = Nothing
		Fn_Result   = Eval(Replace(Fn_E,"w",BV_W))
		ShippingFee = Fn_Result
	End Function
	
	'根据配送方式（送货方式）计算配送费用
	'返回 array("配送方式是否合法存在","配送费用","是否保价","物流保价费")
	Public Function GetShippingFee(ByVal BV_ShippingID,ByVal Fn_Cost_Item,ByVal BV_CartWeight,ByVal Fn_Ship_AreaArray,ByVal Fn_Ship_AreaGrade)
		Dim Fn_ii,Fn_jj,Fn_kk,Fn_ReturnArray
		Dim Fn_AreaFeeSet,Fn_SetAreaDefaultFee,Fn_Area,Fn_Protect,Fn_ProtectRate,Fn_ProtectMinPrice,Fn_IsInDeliveryRegion
		Dim Fn_DeliveryData,Fn_DeliveryDataJSON
		Dim Fn_ShippingID : Fn_ShippingID = BV_ShippingID
		Dim Fn_CartWeight : Fn_CartWeight = BV_CartWeight
		Dim Fn_DeliveryExist : Fn_DeliveryExist = false '所选择的配送方式是否合法存在
		Dim Fn_Cost_Freight  : Fn_Cost_Freight = 0
		Dim Fn_Cost_Protect  : Fn_Cost_Protect = 0
		ReDim Fn_ReturnArray(4)
		'配送方式ID有效并在数据库中存在则继续
		Fn_DeliveryData = GetDeliveryData()
		If Len(Fn_DeliveryData)>10 Then
			Set Fn_DeliveryDataJSON = KnifeCMS.JSON.Parse(Fn_DeliveryData)
			For Fn_ii=0 To Fn_DeliveryDataJSON.delivery.length-1
				'Echo KnifeCMS.JSON.Stringify(Fn_Temp)&"<br><br><br>"
				If Fn_ShippingID=KnifeCMS.Data.CLng(Fn_DeliveryDataJSON.delivery.get(Fn_ii).id) Then
					
					Fn_AreaFeeSet         = KnifeCMS.Data.CLng(Fn_DeliveryDataJSON.delivery.get(Fn_ii).areafeeset)         '配送地区费用设置 1:统一设置 2:指定配送地区和费用
					Fn_SetAreaDefaultFee  = KnifeCMS.Data.CLng(Fn_DeliveryDataJSON.delivery.get(Fn_ii).setareadefaultfee)  '当指定配送地区和费用时 是否启用默认费用 0:不启用 1:启用
					Set Fn_Area           = Fn_DeliveryDataJSON.delivery.get(Fn_ii).area                                  '支持的配送地区
				    Fn_Protect            = KnifeCMS.Data.CLng(Fn_DeliveryDataJSON.delivery.get(Fn_ii).protect)            '支持支持物流保价  0:不启用 1:启用
				    Fn_ProtectRate        = KnifeCMS.Data.FormatDouble(Fn_DeliveryDataJSON.delivery.get(Fn_ii).protectrate)       '支持物流保价 费率
				    Fn_ProtectMinPrice    = KnifeCMS.Data.FormatDouble(Fn_DeliveryDataJSON.delivery.get(Fn_ii).protectminprice)   '支持物流保价 最低保价费
				    Fn_IsInDeliveryRegion = false
					If Fn_AreaFeeSet<>1 And Fn_AreaFeeSet<>2 Then Fn_AreaFeeSet=2
					
					If Fn_AreaFeeSet=1 Then
						'统一设置
						Fn_DeliveryExist = True
						Fn_Cost_Freight  = OS.ShippingFee(Fn_DeliveryDataJSON.delivery.get(Fn_ii).expressions,Fn_CartWeight)
					Else
						'指定配送地区和费用
						'检查选择的地区是否在此配送方式指定的配送地区内，若在指定的配送地区则 按 该地区指定的费用运算公式;
						Fn_kk=Fn_Ship_AreaGrade-1
						Do While Fn_kk>=0
							If Fn_IsInDeliveryRegion=true Then
								Exit Do
							Else
								'配送地区
								For Fn_jj=0 To Fn_Area.length-1
									OS.IsInRegion = false
									Call OS.CheckIsInRegion(Fn_Ship_AreaArray(Fn_kk),Fn_Area.get(Fn_jj).areaid)
									if OS.IsInRegion=true Then
										'明确存在
										if Fn_kk=Fn_Ship_AreaGrade Then
											Fn_Cost_Freight       = OS.ShippingFee(Fn_Area.get(Fn_jj).expressions,Fn_CartWeight)
											Fn_IsInDeliveryRegion = true
											Fn_DeliveryExist      = True
											Exit For
										else
										'其上级存在
											'获取当前地区A的子地区，如果子地区为空则表示A下的所有地区都有效
											Call OS.GetRegion(Fn_Ship_AreaArray(Fn_kk),Fn_Area.get(Fn_jj).areaid)
											if KnifeCMS.Data.IsNul(OS.TempRegion) then
												Fn_Cost_Freight       = OS.ShippingFee(Fn_Area.get(Fn_jj).expressions,Fn_CartWeight)
												Fn_IsInDeliveryRegion = true
												Fn_DeliveryExist      = True
												Exit For
											end if
										end if
									end if
								Next
							End If
							Fn_kk = Fn_kk - 1
						Loop
						'启用默认费用 若选择的地区不在指定的配送地区则 按 默认费用运算公式;
						If Not(Fn_IsInDeliveryRegion=true) And Fn_SetAreaDefaultFee=1 Then
							Fn_DeliveryExist = True
							Fn_Cost_Freight  = OS.ShippingFee(Fn_DeliveryDataJSON.delivery.get(Fn_ii).expressions,Fn_CartWeight)
						End If
					End If
					
					'==计算配送保价费
					If Fn_DeliveryExist Then
						If Fn_Protect=1 Then
							Fn_Cost_Protect = KnifeCMS.Data.FormatDouble(Fn_Cost_Item *  Fn_ProtectRate)
							If Fn_Cost_Protect < Fn_ProtectMinPrice Then Fn_Cost_Protect=Fn_ProtectMinPrice
						End If
					End If
					
				End If
			Next
		Else
			Fn_DeliveryExist=false
		End If
		Set Fn_DeliveryDataJSON = nothing
		Fn_ReturnArray(0) = Fn_DeliveryExist
		Fn_ReturnArray(1) = Fn_Cost_Freight
		Fn_ReturnArray(2) = Fn_Protect
		Fn_ReturnArray(3) = Fn_Cost_Protect
		GetShippingFee = Fn_ReturnArray
	End Function
	
	'OS.CreateOption(Array("any:任意状态","1:活动订单","2:已作废","3:已完成"),Split(Pv_Status,","))
	Public Function CreateOption(ByVal BV_Options,ByVal BV_SelectOptions)
		Dim Fn_ii,Fn_jj,Fn_Select,Fn_Value,Fn_Text,Fn_Temp,Fn_ReturnString
		If IsArray(BV_Options) Then
			For Fn_ii=0 To Ubound(BV_Options)
				If Instr(BV_Options(Fn_ii),":")>0 Then
					Fn_Value  = KnifeCMS.Data.CLeft(BV_Options(Fn_ii),":")
					Fn_Text   = KnifeCMS.Data.CRight(BV_Options(Fn_ii),":")
				Else
					Fn_Value  = BV_Options(Fn_ii)
					Fn_Text   = BV_Options(Fn_ii)
				End If
				Fn_Select = ""
				If IsArray(BV_SelectOptions) Then
					For Fn_jj=0 To Ubound(BV_SelectOptions)
						If Trim(BV_SelectOptions(Fn_jj))=Trim(Fn_Value) Then
							Fn_Select = "selected=""selected"""
						End If
					Next
				Else
					If Trim(BV_SelectOptions)=Trim(Fn_Value) Then
						Fn_Select = "selected=""selected"""
					End If
				End If
				Fn_Temp = "<option "& Fn_Select &" value="""& Fn_Value &""">"& Fn_Text &"</option>"
				Fn_ReturnString = Fn_ReturnString & Fn_Temp
			Next
		End If
		CreateOption  = Fn_ReturnString
	End Function
	
	'根据地区路径获取地区 广东-广州市-天河区
	Public Function GetRegionByPath(ByVal BV_Path)
		Dim Fn_Rs,Fn_ii,Fn_Array,Fn_Temp,Fn_ReturnString
		Fn_ReturnString = ""
		Fn_Array = Split(BV_Path,",")
		For Fn_ii=0 To Ubound(Fn_Array)
			Fn_Temp = KnifeCMS.Data.CLng(Fn_Array(Fn_ii))
			If Fn_Temp>0 Then
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT Local_Name FROM ["& DBTable_Regions &"] WHERE ID="& Fn_Temp &"")
				If Not(Fn_Rs.Eof) Then
					If Fn_ReturnString="" Then Fn_ReturnString = Fn_Rs(0) : Else Fn_ReturnString = Fn_ReturnString &"-"& Fn_Rs(0)
				End If
				KnifeCMS.DB.CloseRs Fn_Rs
			End If
		Next
		GetRegionByPath = Fn_ReturnString
	End Function
	
	'获取内容模型参数
	Public Function GetSubSystem(ByVal BV_String)
		Dim Fn_TempArray,Fn_Temp
		Fn_TempArray=Split(BV_String,"_")
		If Ubound(Fn_TempArray)<1 Then GetSubSystem="" : Exit Function
		Fn_Temp = Fn_TempArray(1)
		If Fn_Temp="" Then
			GetSubSystem = ""
		Else
			GetSubSystem = LCase(KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Fn_TempArray(1),"[^a-zA-Z]",""),50))
		End If
	End Function
	
	'有效的验证码
	Public Function ValidCheckCode()
		If ValidateCheckCode="ok" Then
			ValidCheckCode = True
		Else
			ValidCheckCode = False
		End If
	End Function
	
	'证实验证码:(出于安全考虑,不管有没有验证成功)验证一次则清空验证码Session
	Public Function ValidateCheckCode()
		Dim Fn_Result,Fn_CheckcodeName,Fn_CheckcodeValue
		Fn_CheckcodeName  = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","checkcode_name"),"[^0-9]",""),5)
		Fn_CheckcodeValue = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","checkcode_value"),"[^0-9]",""),5)
		
		If KnifeCMS.Data.IsNul(Fn_CheckcodeName) Or KnifeCMS.Data.IsNul(Fn_CheckcodeValue) Then ValidateCheckCode = "error_checkcode_is_null" : Exit Function
		If KnifeCMS.Data.IsNul(KnifeCMS.Session.GetSession("getcode"& Fn_CheckcodeName)) Then ValidateCheckCode = "error_checkcode_is_null" : Exit Function

		If CStr(KnifeCMS.Session.GetSession("getcode"& Fn_CheckcodeName)) = CStr(Fn_CheckcodeValue) Then
			Fn_Result = "ok"
		Else
			Fn_Result = "error"
		End If
		Call KnifeCMS.Session.RemoveSession("getcode"& Fn_CheckcodeName)
		ValidateCheckCode = Fn_Result
	End Function
End Class
%>