function AjaxGetMyPayTradeRecord(page){
	var MyTradeLogList = KnifeCMS.$id("traderecord");KnifeCMS.DataLoading(MyTradeLogList);
	var fn_TempHTML = "<div class=\"pb10\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"listTable\"><tr class=\"top\"><th><div style=\"width:60px;\">"+ Lang_Js_PayTradeRecord[1] +"</div></th><th><div style=\"width:120px;\">"+ Lang_Js_PayTradeRecord[2] +"</div></th><th><div style=\"width:88px;\">"+ Lang_Js_PayTradeRecord[3] +"</div></th><th><div style=\"width:60px;\">"+ Lang_Js_PayTradeRecord[4] +"</div></th><th><div style=\"width:125px;\">"+ Lang_Js_PayTradeRecord[5] +"</div></th><th><div style=\"width:100px;\">"+ Lang_Js_PayTradeRecord[6] +"</div></th></tr>"
	var ajaxString  = "page="+page;
	var ajaxUrl     = "include/module/ajax/getmypaytraderecord.asp?r="+parseInt(Math.random()*1000);
	var ajaxSta     = posthttp(ajaxString,ajaxUrl)//;window.open(ajaxUrl);     
	//alert(ajaxSta);
	if(ajaxSta=="ERROR" || ajaxSta=="false" || ajaxSta==""){
		fn_TempHTML="<div>"+ Lang_Js_Cue_LoadingFail +"</div>";
	}else if(ajaxSta=="NOT_LOGIN"){
		KnifeCMS.LoginDiaWindow();
	}else{
		var trade,JSON = KnifeCMS.JSON.parseJSON(ajaxSta);
		var pagestring = unescape(JSON.pagestring);
		for(var i=0;i<JSON.tradedata.length;i++){
			trade = JSON.tradedata[i];
			if(trade!=null){
				fn_TempHTML = fn_TempHTML +"<tr class=\"data\"><td>"+ KnifeCMS.PayTradeType(trade.tradetype) +"</td><td>"+ trade.trade_no +"</td><td>"+ trade.tradefee +"</td><td>"+ KnifeCMS.PayTradeStatus(trade.status) +"</td><td>"+ trade.starttime +"</td><td>"+ trade.remarks +"</td></tr>"
			}
		}
	}
	fn_TempHTML = fn_TempHTML + "</table></div>";
	KnifeCMS.$id("eofPage").innerHTML = pagestring;
	MyTradeLogList.innerHTML = fn_TempHTML
}