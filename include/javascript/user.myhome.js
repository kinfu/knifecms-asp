//页面加载初始化
function ClientPageInit(){
}
function class_Myhome(){
	this.Friend  = new class_MyhomeFriend();
	this.Feeds   = new class_MyhomeFeeds();
	this.Visitor = new class_MyhomeVisitor();
	this.Notify  = new class_MyhomeNotify();
	this.RelatedUser = function(BV_Area){
		var Area = KnifeCMS.$id(BV_Area);KnifeCMS.DataLoading(Area);
		var url,fn_TempHTML = "";
		var ajaxString  = "";
		var ajaxUrl     = "include/module/ajax/getuserlist.asp?ctl=relateduser&r="+parseInt(Math.random()*1000);
		var ajaxSta     = posthttp(ajaxString,ajaxUrl)//;window.open(ajaxUrl);//alert(ajaxSta);
		if(ajaxSta=="ERROR" || ajaxSta=="false" || ajaxSta==""){
			fn_TempHTML="<div>"+ Lang_Js_Cue_LoadingFail +"</div>";
		}else if(ajaxSta=="NOT_LOGIN"){
			KnifeCMS.LoginDiaWindow();
		}else{
			var user,headimg,JSON = KnifeCMS.JSON.parseJSON(ajaxSta);
			for(var i=0;i<JSON.user.length;i++){
				user = JSON.user[i];
				if(user!=null){
					url = "?user/"+ user.id + filesuffix
					username = unescape(user.username);
					if(user.headimg==""){headimg="images/userhead_s.jpg"}else{headimg=unescape(user.headimg)};
					fn_TempHTML = fn_TempHTML +"<li><span class=\"userpic\"><a href=\""+url+"\" target=\"_blank\"><img src=\""+headimg+"\" /></a></span><span class=\"name\"><a href=\""+url+"\"  target=\"_blank\">"+username+"</a></span><span class=\"do\"><a href=\"javascript:;\" onclick=\"Myhome.Friend.AddForFriend("+user.id+",'"+username+"','"+headimg+"')\">加好友</a></span></li>";
				}
			}
		}
		Area.innerHTML = fn_TempHTML
	}
}
function class_MyhomeFeeds(){
	var _Feeds = this;
	this.UserMoodShowAreaID="";
	this.MyFeedsShowAreaID="";
	this.FriendFeedsShowAreaID="";
	this.AllFeedsShowAreaID="";
	this.GuestBookShowAreaID="";
	this.GetUserMood = function(){
		Myhome.Feeds.GetFeeds("um",userid,"0");
	}
	//获取自己的最新动态
	this.GetMyFeeds = function(){
		Myhome.Feeds.GetFeeds("mf",'',"0");
	}
	//获取好友的最新动态(包括自己的)
	this.GetFriendFeeds = function(){
		Myhome.Feeds.GetFeeds("ff",'',"0");
	}
	//获取好友的最新动态(包括自己的)
	this.GetAllFeeds = function(){
		Myhome.Feeds.GetFeeds("af",'',"0");
	}
	//获取留言
	this.GetGuestBook = function(){
		Myhome.Feeds.GetFeeds("gb",userid,"0");
	}
	//展开或收起回复
	this.GetReplyOfTheDoing = function(_this,SubSystem,FeedsID){
		var div = KnifeCMS.$id(SubSystem+"_feedsreply_"+FeedsID);
		if(objExist(div)){
			if(div.style.display=="none"){
				div.style.display="";
				_this.innerHTML=Lang_Js_ShrinkReply; //"收起回复";
			}else{
				div.style.display="none";
				_this.innerHTML=Lang_Js_Reply;//"回复";
			}
		}
	}
	//发布说说 SuccessDoFunction:发布成功后要执行的操作
	this.MoodPost = function(SuccessDoFunction){
		 var Input  = KnifeCMS.$id("publisher_statusInput");
		 var result = _Feeds.Sending('mood','',Input.value);
		 if(result){
			 Input.value="";
			 Input.className="moodposter";
			 eval(SuccessDoFunction)
		 }
	}
	//发布留言 memberid:谁的留言板
	this.GuestBookPost = function(memberid){
		 var Input  = KnifeCMS.$id("publisher_guestbookInput");
		 var result = _Feeds.Sending('guestbook',memberid,Input.value);
		 if(result){
			 Input.value="";
			 Input.className="moodposter";
			 _Feeds.GetGuestBook();
		  }
	}
	//发布说说/留言 提交数据到服务器
	this.Sending = function(SubSystem,MemberID,Content){
		var SendingSuccessMsg,SendingErrorMsg;
		var Ctl,listarea;
		if(SubSystem=="mood"){
			SendingSuccessMsg = Lang_Js_User[40];
			SendingErrorMsg   = Lang_Js_User[41];
			Ctl      = "postmood";
		}else if(SubSystem=="guestbook"){
			SendingSuccessMsg = Lang_Js_User[42];
			SendingErrorMsg   = Lang_Js_User[43];
			Ctl      = "postguestbook";
		}
		var fn_DialogID = KnifeCMS.CreateID();
		var fn_Title    = Lang_Js_DiaTitle[0];
		var fn_HTML     = "<div id=\"fixbox\" style=\"padding:10px 4px 5px 12px;\"></div>";
		var fn_Btn      = "";
		KnifeCMS.CreateDiaWindow(320,100,fn_DialogID,fn_Title,fn_HTML,true,false);
		var CueMsgBox=KnifeCMS.$id("fixbox");
		KnifeCMS.DataSending(CueMsgBox);
		var ajaxString = "memberid="+parseInt(MemberID)+"&content="+escape(Content);
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl="+Ctl+"&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		KnifeCMS.DiaWindowCloseByID(fn_DialogID);
		if(ajaxSta=="ok"){
			fn_Btn = "";
			KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,false);
			KnifeCMS.$id("fixbox").innerHTML = SendingSuccessMsg;
			window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID);},1500);
			return true;
		}else if(ajaxSta=="unlogined"){
			fn_Btn = "<input type=\"button\" class=\"sysBtn\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');KnifeCMS.LoginDiaWindow();\" value="+Lang_Js_Btn_Login+"><input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Close+">";
			KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,true);
			KnifeCMS.$id("fixbox").innerHTML = Lang_Js_Unlogined;
			KnifeCMS.LoginDiaWindow();
			return false;
		}else if(ajaxSta=="null"){
			fn_Btn = "<input type=\"button\" class=\"sysBtn\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Confirm+">";
			KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,false);
			KnifeCMS.$id("fixbox").innerHTML = Lang_Js_User[44];
			return false;
		}else{
			if(ajaxSta.indexOf("inforbidtime")>=0){
				fn_Btn      = "";
			    KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,false);
				var forbidtime = parseInt(ajaxSta.split("_")[1]);
				KnifeCMS.$id("fixbox").innerHTML = Lang_Js_InForbidTimeConnotPost.replace("{ptl:time}",forbidtime);
				window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID);},4000);
			}else{
				fn_Btn      = "<input type=\"button\" class=\"sysBtn\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Close+">";
			    KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,true);
				KnifeCMS.$id("fixbox").innerHTML = SendingErrorMsg;
				window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID);},1500);
			}
			return false;
		}
	}
	//提交回复
	this.PostReply = function(_this,SubSystem,P_ID,MemberID){
		//alert(SubSystem+"_textarea_"+P_ID);
		var textarea=KnifeCMS.$id(SubSystem+"_textarea_"+P_ID);
		var Content=textarea.value;
		_Feeds.ReplySending(_this,SubSystem,P_ID,Content,MemberID);
	}
	this.ReplySending = function(_this,SubSystem,P_ID,Content,MemberID){
		//alert(SubSystem);
		var SuccessFunction;
		if(SubSystem=="myfeeds"){
			SuccessFunction="Myhome.Feeds.GetMyFeeds()";
		}else if(SubSystem=="friendfeeds"){
			SuccessFunction="Myhome.Feeds.GetFriendFeeds()";
		}else if(SubSystem=="allfeeds"){
			SuccessFunction="Myhome.Feeds.GetAllFeeds()";
		}else if(SubSystem=="guestbook"){
			SuccessFunction="Myhome.Feeds.GetGuestBook()";
		}else if(SubSystem=="usermood"){
			SuccessFunction="Myhome.Feeds.GetUserMood()";
			//listarea = 'guestbook';
		}
		var fn_DialogID = KnifeCMS.CreateID();
		var fn_Title    = Lang_Js_DiaTitle[0];
		var fn_HTML     = "<div id=\"fixbox\" style=\"padding:10px 4px 5px 12px;\"></div>";
		var fn_Btn      = "";
		KnifeCMS.CreateDiaWindow(320,100,fn_DialogID,fn_Title,fn_HTML,true,false);
		var CueMsgBox=KnifeCMS.$id("fixbox");
		KnifeCMS.DataSending(CueMsgBox);
		//lert(Content);
		var ajaxString = "p_id="+parseInt(P_ID)+"&content="+escape(Content);
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=postreply&subsys="+escape(SubSystem)+"&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		KnifeCMS.DiaWindowCloseByID(fn_DialogID);
		if(ajaxSta=="ok"){
			fn_Btn = "";
			KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,false);
			KnifeCMS.$id("fixbox").innerHTML = Lang_Js_User[40];
			window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID);eval(SuccessFunction)},1200);
		}else if(ajaxSta=="unlogined"){
			fn_Btn = "<input type=\"button\" class=\"sysBtn\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');KnifeCMS.LoginDiaWindow();\" value="+Lang_Js_Btn_Login+"><input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Close+">";
			KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,true);
			KnifeCMS.$id("fixbox").innerHTML = Lang_Js_Unlogined;
			KnifeCMS.LoginDiaWindow();
		}else if(ajaxSta=="null"){
			fn_Btn = "<input type=\"button\" class=\"sysBtn\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Confirm+">";
			KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,false);
			KnifeCMS.$id("fixbox").innerHTML = Lang_Js_User[44];
		}else{
			if(ajaxSta.indexOf("inforbidtime")>=0){
				fn_Btn      = "";
				KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,false);
				var forbidtime = parseInt(ajaxSta.split("_")[1]);
				KnifeCMS.$id("fixbox").innerHTML = Lang_Js_InForbidTimeConnotPost.replace("{ptl:time}",forbidtime);
				window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID);},4000);
			}else{
				fn_Btn      = "<input type=\"button\" class=\"sysBtn\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Close+">";
				KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,true);
				KnifeCMS.$id("fixbox").innerHTML = Lang_Js_User[41];
				window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID);},1500);
			}
		}
	}
	this.ToReply = function(_this,SubSystem,ID){
		Myhome.Feeds.CreateReplyPanel(SubSystem+"_feedsreply_"+ID)
	}
	this.CreateReplyPanel = function(ReplyPanel_ParentNode_id){
		var AllPanel = false;
		if(ReplyPanel_ParentNode_id=="all"){ var re_divs=getElementsByTagNameAndName("div","feedsreply");AllPanel=true;}
		else{ var re_divs=[];re_divs[0]=KnifeCMS.$id(ReplyPanel_ParentNode_id);AllPanel = false;}
		var SubSystem,FeedsID,MemberID;
		var def_text=Lang_Js_FeedReplyDefaultValue;
		for(var i=0;i<re_divs.length;i++){
			FeedsID   = re_divs[i].getAttribute("feedsid");
			SubSystem = re_divs[i].getAttribute("subsys");
			//MemberID  = re_divs[i].getAttribute("memberid");
			//re_divs[i].getElementsByTagName("div");
			if(!objExist(KnifeCMS.$id(SubSystem+"_replypanel_"+FeedsID))){
				var div=document.createElement("div");
				    div.className="replypanel";
				    div.id       =SubSystem+"_replypanel_"+FeedsID;
				var div_author=document.createElement("div");
				    div_author.className="author";
				var div_ctbody=document.createElement("div");
				    div_ctbody.className="ctbody";
				var a=document.createElement("a");
				    a.href="javascript:;";
				var img=document.createElement("img");
				    img.src=mysmallhead;
				var t_div=document.createElement("div");
				    t_div.style.width="100%";
				var textarea=document.createElement("textarea");
				    textarea.className="mv";
				    textarea.id       =SubSystem+"_textarea_"+FeedsID;
				    textarea.value    =def_text;
				var divBtn = document.createElement("div");
					divBtn.className = "divBtn";
				var submitBtn = document.createElement("input");
					submitBtn.type      = "button";
					submitBtn.className = "sysBtn_s";
					submitBtn.setAttribute("feedsid",FeedsID);
					submitBtn.setAttribute("subsys",SubSystem);
					submitBtn.value     = Lang_Js_Btn_Reply;
					submitBtn.onclick   = function(){
						var id=KnifeCMS.DataSending(this.parentNode);
						this.disabled=true;
						_Feeds.PostReply(this,this.getAttribute("subsys"),this.getAttribute("feedsid"),userid);
						this.disabled=false;
						KnifeCMS.RemoveElementByID(id);
					}
				div.appendChild(div_author);
					div_author.appendChild(a);
							 a.appendChild(img);
				div.appendChild(div_ctbody);
					div_ctbody.appendChild(t_div);
						 t_div.appendChild(textarea);
					div_ctbody.appendChild(divBtn);
						divBtn.appendChild(submitBtn);
				textarea.onblur = function(){
					if(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.className=="ifeeds_list2"){
						if(KnifeCMS.IsNull(this.value) || this.value==def_text){
							var TempParentNode=this.parentNode.parentNode.parentNode.parentNode;
							TempParentNode.removeChild(this.parentNode.parentNode.parentNode);
						}else{
							this.parentNode.parentNode.parentNode.className="replypanel_ing";
						}
					}else{
						if(KnifeCMS.IsNull(this.value) || this.value==def_text){
							this.className="mv";
							this.value=def_text;
							this.parentNode.parentNode.parentNode.className="replypanel";
						}else{
							this.parentNode.parentNode.parentNode.className="replypanel_ing";
						}
					}
				}
				textarea.onfocus = function(){
					if(this.className!="on"){
						this.parentNode.parentNode.parentNode.className="replypanel_ing";
						this.className="on";
						if(this.value==def_text){this.value="";}
					}
				}
				re_divs[i].appendChild(div);
				if(!AllPanel){textarea.focus();}
			}
		}
	}
	this.Page = function(SubSystem,MemberID,Page){
		_Feeds.GetFeeds(SubSystem,MemberID,Page);
	}
	this.GetFeeds = function(SubSystem,MemberID,Page){
		var Check=true;
		var Area,ajaxString,ajaxUrl,ajaxSta;
		switch(SubSystem)
		{
			case "um":
			Area=KnifeCMS.$id(_Feeds.UserMoodShowAreaID);
			var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=getfeeds&subsys=usermood&r="+parseInt(Math.random()*1000);
			break;
			case "mf":
			Area=KnifeCMS.$id(_Feeds.MyFeedsShowAreaID);
			var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=getfeeds&subsys=myfeeds&r="+parseInt(Math.random()*1000);
			break;
			case "ff":
			Area=KnifeCMS.$id(_Feeds.FriendFeedsShowAreaID);
			var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=getfeeds&subsys=friendfeeds&r="+parseInt(Math.random()*1000);
			break;
			case "af":
			Area=KnifeCMS.$id(_Feeds.AllFeedsShowAreaID);
			var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=getfeeds&subsys=allfeeds&r="+parseInt(Math.random()*1000);
			break;
			case "gb":
			Area=KnifeCMS.$id(_Feeds.GuestBookShowAreaID);
			var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=getfeeds&subsys=guestbook&r="+parseInt(Math.random()*1000);
			break;
			default:
			Check=false;
		}
		if(objExist(Area) && Check){
			Area.innerHTML = ""; KnifeCMS.DataLoading(Area);
			ajaxString = "memberid="+MemberID+"&page="+Page;
			ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
			//alert(ajaxSta);
			if(ajaxSta==false || ajaxSta=="error"){
				Area.innerHTML = Lang_Js_Cue_LoadingFail;
			}else{
				if(ajaxSta.toLowerCase()=="null"){ Area.innerHTML = Lang_Js_Tie[5];}else{Area.innerHTML = ajaxSta;}
			}
		}
		//创建回复框
		_Feeds.CreateReplyPanel("all");
		//创建删除按钮
		_Feeds.CreateFeedsDelButton("ifeeds_list");
		_Feeds.CreateFeedsDelButton("ifeeds_list2");
	}
	this.CreateFeedsDelButton = function(ClassName){
		var ifeeds_list =document.getElementsByClassName("div",ClassName);
		for(var i=0; i<ifeeds_list.length; i++){
			ifeeds_list[i].onmouseover=function(){
				var span=getElementsByClassNameAndAttribute(this,"span","del","feedsid",this.getAttribute("feedsid"));
				if(!objExist(span[0])){
					var del=document.createElement("span");
					del.className="del";
					del.setAttribute("feedsid",this.getAttribute("feedsid"));
					del.setAttribute("subsys",this.getAttribute("subsys"));
					del.setAttribute("path",this.getAttribute("path"));
					var a=document.createElement("a");a.className="icon icon_close";a.title=Lang_Js_DoDel +"/"+ Lang_Js_DoHidden;
					a.onclick     = function(){
						_a  =this;
						_div=_a.parentNode.parentNode;
						var p_ifeeds_list=getElementsByClassNameAndAttribute(document,"div","ifeeds_list","feedsid",_div.getAttribute("path").split(",")[1]);
						var deletedo=false;
						var fn_ctl="";
						if((_div.getAttribute("subsys")!="guestbook")&&(myid==_div.getAttribute("author") || myid==p_ifeeds_list[0].getAttribute("author"))){
							deletedo = true;
							fn_ctl   = "feedsdelete";
						}else if((_div.getAttribute("subsys")=="guestbook")&&(myid==_div.getAttribute("author") || myid==p_ifeeds_list[0].getAttribute("author"))){
							deletedo = true;
							fn_ctl   = "guestbookdelete";
						}else if(subsystem=="home"){
							deletedo = true;
							fn_ctl   = "guestbookdelete";
						}
						if(deletedo){
							//确定要删除吗?
							if(confirm(Lang_Js_User[48])){
								var Success=false;
								var fn_DialogID=KnifeCMS.CreateID();
								var fn_HTML    ="<div class=\"p10\" id=\"dialogtext_new\"></div>";
								var fn_Btn     ="<input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Close+">";
								KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,Lang_Js_DiaTitle[0],Array(fn_HTML,fn_Btn),true,true);
								KnifeCMS.Processing(KnifeCMS.$id("dialogtext_new"));
								var ajaxString = "feedsid="+_a.parentNode.getAttribute("feedsid");
								var ajaxUrl    = "include/module/ajax/ajax.asp?ctl="+fn_ctl+"&subsys="+_a.parentNode.getAttribute("subsys")+"&r="+parseInt(Math.random()*1000);
								var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
								if(ajaxSta=="ok"){
									KnifeCMS.$id("dialogtext_new").innerHTML=Lang_Js_User[46];
									Success=true;
								}else if(ajaxSta=="notexist"){
									Success=true;
								}else{
									KnifeCMS.$id("dialogtext_new").innerHTML=Lang_Js_User[47];
								}
								window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID)},800);
								if(Success){window.setTimeout(function(){_a.parentNode.parentNode.parentNode.removeChild(_div)},1300);}
							}
						}else{
							_a.parentNode.parentNode.parentNode.removeChild(_div);
						}
					}
					a.onmouseover = function(){this.className="icon icon_close_hover";}
					a.onmouseout  = function(){this.className="icon icon_close";}
					del.appendChild(a);
					this.appendChild(del);
				}else{
					span[0].style.display="";
				}
			}
			ifeeds_list[i].onmouseout=function(){
				var span=getElementsByClassNameAndAttribute(this,"span","del","feedsid",this.getAttribute("feedsid"));
				if(objExist(span[0])){
					span[0].style.display="none";
				}
			}
			
		}
	}
	//转发
	this.RelayFeeds = function(_this,FeedsID){
		var DialogID = KnifeCMS.DiaDoing("",true,true);
		var ajaxString = "feedsid="+ FeedsID;
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=repayfeeds&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta=="ok"){
			KnifeCMS.DiaWindowCueMessage(Lang_Js_User[54],4);
			KnifeCMS.DiaWindowCloseByID(DialogID);
			Myhome.Feeds.GetFriendFeeds();
		}else if(ajaxSta=="unlogined"){
			KnifeCMS.LoginDiaWindow();
			KnifeCMS.DiaWindowCloseByID(DialogID);
		}else if(ajaxSta=="" || ajaxSta==false){
			KnifeCMS.DiaWindowCueMessage(Lang_Js_User[55],4);
			KnifeCMS.DiaWindowCloseByID(DialogID);
		}else{
			if(ajaxSta.indexOf("inforbidtime")>=0){
				var forbidtime = parseInt(ajaxSta.split("_")[1]);
				KnifeCMS.DiaWindowCueMessage(Lang_Js_InForbidTimeConnotPost.replace("{ptl:time}",forbidtime),4);
				KnifeCMS.DiaWindowCloseByID(DialogID);
			}else{
				KnifeCMS.DiaWindowCueMessage(Lang_Js_User[55],4);
				KnifeCMS.DiaWindowCloseByID(DialogID);
			}
		}
	}
}
function class_MyhomeFriend(){
	var _Friend = this;
	this.FriendListPageSize      = 20;
	this.FriendListAreaID        = "";
	this.FriendListAreaPageingID = [];
	this.MembersFriendsAreaID     = "";
	this.MembersFriendsListAreaID = "";
	this.AddForFriendDefaultText  = Lang_Js_Friend[22];//附加信息(选填，45字内)
	this.RejectFriendDefaultText  = Lang_Js_Friend[34];//拒绝理由(选填,45字内)
	//加为好友弹出框
	this.Adding = function(_this,BV_UserID,BV_UserName){
		var fn_DialogID = _this.parentNode.parentNode.parentNode.id;
		_this.parentNode.innerHTML="";
		var fn_msg  = KnifeCMS.$id("addFriendMessage").value;
		if(fn_msg == _Friend.AddForFriendDefaultText){fn_msg="";}
		var text_id = "dialogtext_new";
		KnifeCMS.$id(text_id).innerHTML="";
		KnifeCMS.DataSending(KnifeCMS.$id(text_id));
		var ajaxString = "userid="+ escape(BV_UserID) + "&msg="+ escape(fn_msg);
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=addforfriend&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta=="ok"){
			KnifeCMS.$id(text_id).innerHTML=Lang_Js_Friend[23].replace(/{tpl:name}/g,BV_UserName);
			window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID)},2500);
		}else if(ajaxSta=="isfriend"){
			KnifeCMS.$id(text_id).innerHTML=Lang_Js_Friend[24].replace(/{tpl:name}/g,BV_UserName);
			window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID)},2500);
		}else{
			if(ajaxSta.indexOf("inforbidtime")>=0){
				var fn_DialogID_2=KnifeCMS.CreateID();
			    KnifeCMS.CreateDiaWindow(400,180,fn_DialogID_2,Lang_Js_DiaTitle[0],Array("<div class=\"p10\">"+Lang_Js_Friend[32]+"</div>",""),true,false);
				window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID_2);KnifeCMS.DiaWindowCloseByID(fn_DialogID)},4000);
			}else{
				KnifeCMS.$id(text_id).innerHTML=Lang_Js_Friend[25].replace(/{tpl:name}/g,BV_UserName);
				window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID)},2500);
			}
		}
	}
	this.AddForFriend = function(BV_UserID,BV_UserName,BV_HeadImg){
		if(BV_UserID==myid){alert(Lang_Js_Friend[37]);return false;}
		var fn_DialogID = KnifeCMS.CreateID();
		var fn_HTML     = "<p class=\"pl5 pt3 pb3\" style=\"background:#FEFBC9;\">"+Lang_Js_Friend[21].replace(/{tpl:name}/g,BV_UserName)+"</p><div id=\"dialogtext_new\" class=\"diainbox mb5\"><table border=\"0\" cellpadding=\"5\" cellspacing=\"0\"><tr><td valign=\"top\"><span class=\"userhead\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td><a href=\"?user/"+BV_UserID+filesuffix+"\"><img src=\""+BV_HeadImg+"\" /></a></td></tr></table></span></td><td valign=\"top\"><textarea class=\"text color888\" id=\"addFriendMessage\" onblur=\"if(this.value=='')this.value=this.title;this.style.color='#888';\" onfocus=\"if(this.value==this.title)this.value='';this.style.color='#000';\" title=\""+_Friend.AddForFriendDefaultText+"\" style=\"height:50px; width:290px;\">"+_Friend.AddForFriendDefaultText+"</textarea></td></tr></table></div>";
		var fn_Btn      = "<input type=\"button\" class=\"sysBtn ml5\" onclick=\"Myhome.Friend.Adding(this,'"+BV_UserID+"','"+BV_UserName+"')\" value="+Lang_Js_Btn_Confirm+"><input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Cancel+">";
		KnifeCMS.CreateDiaWindow(400,178,fn_DialogID,Lang_Js_Friend[20].replace(/{tpl:name}/g,BV_UserName),Array(fn_HTML,fn_Btn),true,true);
	}
	//我的好友申请
	this.FriendRequest = function(){
		var fn_DialogID=KnifeCMS.CreateID();
		var fn_textid  ="myfriendrequest";
		var fn_HTML    ="<div id=\""+fn_textid+"\" class=\"ml10 mr10 mb5 mt5\"></div>";
		var fn_Btn     ="<input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"Myhome.Friend.FriendRequestBatchDo('ignore','"+fn_DialogID+"');\" value="+Lang_Js_Btn_IgnoreAll+"><input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"Myhome.Friend.FriendRequestCount();KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Close+">";
		KnifeCMS.CreateDiaWindow(680,440,fn_DialogID,Lang_Js_Friend[26],Array(fn_HTML,fn_Btn),true,false);
		KnifeCMS.DataLoading(KnifeCMS.$id(fn_textid));
		var ajaxString = "";
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=getfriendrequest&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta===false){
			KnifeCMS.$id(fn_textid).innerHTML=Lang_Js_Cue_LoadingFail;
		}else if(ajaxSta==""){
			KnifeCMS.$id(fn_textid).innerHTML=Lang_Js_Friend[38];//"无好友申请"
		}else{
			var HaveFriendRequest=false;
			var DataString = ajaxSta;
			if(!KnifeCMS.IsNull(DataString)){
				var array = DataString.split(";");
				var fn_id,fn_userid,fn_username,headimg,message,notifytype,time,ul,li;
				ul=document.createElement("ul");
				ul.className="itemImglist1";
				for(var i=0; i<array.length; i++){
					fn_id       = unescape(KnifeCMS.RegMatch(/"id":"([\s\S]*?)"/,array[i]));
					fn_userid   = unescape(KnifeCMS.RegMatch(/"userid":"([\s\S]*?)"/,array[i]));
					fn_username = unescape(KnifeCMS.RegMatch(/"username":"([\s\S]*?)"/,array[i]));
					headimg  = unescape(KnifeCMS.RegMatch(/"headimg":"([\s\S]*?)"/,array[i]));
					message  = unescape(KnifeCMS.RegMatch(/"message":"([\s\S]*?)"/,array[i]));
					time     = unescape(KnifeCMS.RegMatch(/"time":"([\s\S]*?)"/,array[i]));
                    var month   = KnifeCMS.GetDateTime("month",time);
					var day     = KnifeCMS.GetDateTime("day",time);
					var hour    = KnifeCMS.GetDateTime("hour",time);
					var minutes = KnifeCMS.GetDateTime("minutes",time);
					time    = month+Lang_Js_Month+day+""+Lang_Js_Day+" "+hour+":"+minutes;
					if(fn_username.length>0){
						HaveFriendRequest=true;
						li=document.createElement("li");
						li.setAttribute("name","frienrequest_"+fn_userid);
						li.setAttribute("userid",fn_userid);
						li.innerHTML="<div class=\"boxl\"><span class=\"simg\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td><a href=\"?user/"+fn_userid+filesuffix+"\" target=\"_blank\"><img src=\""+headimg+"\" /></a></td></tr></table></span></div><div class=\"boxr\"><p class=\"name\"><a href=\"?user/"+fn_userid+filesuffix+"\" target=\"_blank\">"+fn_username+"</a><span class=\"ml5\">"+time+"</span><span class=\"ml5 typetext\">"+Lang_Js_Friend[39]+"</span></p><div class=\"dd\"><p class=\"q_msg\"><q>\"</q>"+message+"<q>\"</q></p><p class=\"pb5\"><input type=\"button\" class=\"sysBtn_s\" onclick=\"Myhome.Friend.AcceptFriend(event,'"+fn_userid+"','"+fn_username+"','"+headimg+"');\" value="+Lang_Js_Btn_Accept+"><input type=\"button\" class=\"sysBtn_s ml5\" onclick=\"Myhome.Friend.RejectFriend(event,'"+fn_userid+"','"+fn_username+"','"+headimg+"');\" value="+Lang_Js_Btn_Reject+"><input type=\"button\" class=\"sysBtn_grey_s ml5\" onclick=\"Myhome.Friend.IgnoreFriend(event,'"+fn_userid+"','"+fn_username+"','"+headimg+"');\" value="+Lang_Js_Btn_Ignore+"></p></div></div>";
						ul.appendChild(li);
					}
				}
			}
			KnifeCMS.$id(fn_textid).innerHTML="";
			if(HaveFriendRequest){
				KnifeCMS.$id(fn_textid).appendChild(ul);
			}else{
				KnifeCMS.$id(fn_textid).innerHTML="<p style=\"padding-top:10px;\">"+Lang_Js_Friend[38]+"</p>";
			}
		}
	}
	this.FriendRequestCount = function(){
		if(objExist(KnifeCMS.$id("myfriendrequestcount"))){
			var lis=KnifeCMS.$id("myfriendrequest").getElementsByTagName("li");
			KnifeCMS.$id("myfriendrequestcount").innerHTML=lis.length;
		}
	}
	this.AcceptFriend = function(_event,BV_UserID,BV_UserName,BV_HeadImg){
		var fn_DialogID=KnifeCMS.CreateID();
		var fn_textid  ="acceptfriendrequest";
		var fn_spanid  =KnifeCMS.CreateID();
		var fn_HTML    ="<div id=\"text_"+fn_DialogID+"\" class=\"mt5 mr10 mb5 ml10\"><ul class=\"itemImglist1\"><li><div class=\"boxl\"><span class=\"simg\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td><a href=\"?user/"+BV_UserID+filesuffix+"\" target=\"_blank\"><img src=\""+BV_HeadImg+"\" /></a></td></tr></table></span></div><div class=\"boxr\"><p class=\"name\"><a href=\"?user/"+BV_UserID+filesuffix+"\" target=\"_blank\">"+BV_UserName+"</a></p><div class=\"dd\"><label id=\"d_selectmyfriendgroup\">选择分组</label><div id=\""+fn_textid+"\"><span id=\""+fn_spanid+"\"></span></div></div></div></li></ul><div>";
		var fn_Btn     ="<input type=\"button\" class=\"sysBtn\" onclick=\"Myhome.Friend.AcceptFriendSave('"+BV_UserID+"','"+fn_DialogID+"');\" value="+Lang_Js_Btn_Confirm+"><input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Cancel+">";
		KnifeCMS.CreateDiaWindow(360,180,fn_DialogID,Lang_Js_Friend[27].replace(/{tpl:name}/g,BV_UserName),Array(fn_HTML,fn_Btn),true,false);
		_Friend.CreateSelectMyFriendGroup(fn_spanid,"selectmyfriendgroup");
		var fn_CreateGroup=false;
		var a=document.createElement("a");
		a.innerHTML=Lang_Js_Friend[31];
		a.href="javascript:;";
		a.style.marginLeft="5px";
		a.onclick=function(){
			_Friend.CreateAddMyGroupDiaWin("Myhome.Friend.CreateSelectMyFriendGroup('"+fn_spanid+"','selectmyfriendgroup')");
		}
		KnifeCMS.$id(fn_textid).appendChild(a);
	}
	this.AcceptFriendSave = function(BV_UserID,BV_DialogID){
		var fn_GroupID=parseInt(KnifeCMS.$id("selectmyfriendgroup").value)
		if(!(fn_GroupID>0)){
			KnifeCMS.$id("d_selectmyfriendgroup").className="d_err";
			return false;
		}else{
			KnifeCMS.$id("d_selectmyfriendgroup").className="";
		}
		var fn_DiaTextID = "text_"+BV_DialogID;
		KnifeCMS.SetSubmitButtomsDisabled(true,"DiaBottom_"+BV_DialogID);
		KnifeCMS.Processing(KnifeCMS.$id(fn_DiaTextID));
		var Ctl        = "friendrequestdo";
		var Action     = "accept";
		var ajaxString = "cnuserid="+BV_UserID+"&groupid="+fn_GroupID;
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl="+Ctl+"&action="+Action+"&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta=="ok" || ajaxSta=="null" || ajaxSta=="isfriend"){
			if(ajaxSta=="ok"){
				KnifeCMS.$id(fn_DiaTextID).innerHTML=Lang_Js_Friend[43];//"成功接受对方的好友请求并加对方为好友.";
			}else if(ajaxSta=="isfriend"){
				KnifeCMS.$id(fn_DiaTextID).innerHTML=Lang_Js_Friend[44];//"对方已经是你的好友.";
			}else{
				KnifeCMS.$id(fn_DiaTextID).innerHTML=Lang_Js_Cue_IllegalData;//"请不要拟造非法数据.";
			}
			window.setTimeout(function(){
									   KnifeCMS.DiaWindowCloseByID(BV_DialogID);
									   var lis=getElementsByTagNameAndName("li","frienrequest_"+BV_UserID);
									   for(var i=0;i<lis.length;i++){lis[i].parentNode.removeChild(lis[i]);}
									   },1000);
		}else{
			KnifeCMS.SetSubmitButtomsDisabled(false,"DiaBottom_"+BV_DialogID);
		}
	}
	this.RejectFriend=function(_event,BV_UserID,BV_UserName,BV_HeadImg){
		var fn_DialogID=KnifeCMS.CreateID();
		var fn_textid  ="rejectfriendrequest";
		var fn_spanid  =KnifeCMS.CreateID();
		var fn_HTML    ="<div id=\"text_"+fn_DialogID+"\" class=\"mt5 mr10 mb5 ml10\"><ul class=\"itemImglist1\"><li><div class=\"boxl\"><span class=\"simg\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td><a href=\"?user/"+BV_UserID+filesuffix+"\" target=\"_blank\"><img src=\""+BV_HeadImg+"\" /></a></td></tr></table></span></div><div class=\"boxr\"><p class=\"name\"><a href=\"?user/"+BV_UserID+filesuffix+"\" target=\"_blank\">"+BV_UserName+"</a></p><div class=\"dd mt10\"><div id=\""+fn_textid+"\"><span id=\""+fn_spanid+"\"><input type=\"text\" class=\"text\" id=\"rejectmessage\" style=\"width:260px;color:#888;\" onblur=\"if(this.value=='')this.value=this.title;this.style.color='#888';\" onfocus=\"if(this.value==this.title)this.value='';this.style.color='#000';\" title=\""+_Friend.RejectFriendDefaultText+"\" value=\""+_Friend.RejectFriendDefaultText+"\" /></span></div></div></div></li></ul><div>";
		var fn_Btn     ="<input type=\"button\" class=\"sysBtn\" onclick=\"Myhome.Friend.RejectFriendSave('"+BV_UserID+"','"+fn_DialogID+"');\" value="+Lang_Js_Btn_Confirm+"><input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Cancel+">";
		KnifeCMS.CreateDiaWindow(360,180,fn_DialogID,Lang_Js_Friend[33].replace(/{tpl:name}/g,BV_UserName),Array(fn_HTML,fn_Btn),true,false);
	}
	this.IgnoreFriend = function(_event,BV_UserID,BV_UserName,BV_HeadImg){
		var fn_DialogID= KnifeCMS.CreateID();
		var fn_HTML    = "<div id=\"text_"+fn_DialogID+"\" class=\"mt5 mr10 mb5 ml10\"></div>";
		var fn_Btn     = "";
		KnifeCMS.CreateDiaWindow(360,180,fn_DialogID,Lang_Js_Friend[36].replace(/{tpl:name}/g,BV_UserName),Array(fn_HTML,fn_Btn),true,false);
		_Friend.IgnoreFriendSave(BV_UserID,fn_DialogID);
	}
	this.RejectFriendSave = function(BV_UserID,BV_DialogID){_Friend.RejectOrIgnoreFriendSave("reject",BV_UserID,BV_DialogID)}
	this.IgnoreFriendSave = function(BV_UserID,BV_DialogID){_Friend.RejectOrIgnoreFriendSave("ignore",BV_UserID,BV_DialogID)}
	this.RejectOrIgnoreFriendSave = function(BV_Action,BV_UserID,BV_DialogID){
		if(BV_Action=="reject"){
			var fn_RejectMessage=KnifeCMS.$id("rejectmessage").value;
			if(fn_RejectMessage == _Friend.RejectFriendDefaultText){fn_RejectMessage="";}
			if(fn_RejectMessage!=""){fn_RejectMessage=escape(fn_RejectMessage)}else{fn_RejectMessage="";}
			var Action     = "reject";
		}else if(BV_Action=="ignore"){
			var Action     = "ignore";
		}else{return false;}
		var fn_DiaTextID = "text_"+BV_DialogID;
		KnifeCMS.SetSubmitButtomsDisabled(true,"DiaBottom_"+BV_DialogID);
		KnifeCMS.Processing(KnifeCMS.$id(fn_DiaTextID));
		var Ctl        = "friendrequestdo";
		var ajaxString = "cnuserid="+BV_UserID+"&rejectmsg="+fn_RejectMessage;
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl="+Ctl+"&action="+Action+"&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta=="ok" || ajaxSta=="null" || ajaxSta=="isfriend"){
			if(ajaxSta=="ok"){
				KnifeCMS.$id(fn_DiaTextID).innerHTML=Lang_Js_User[51];//"操作完成.";
			}else if(ajaxSta=="isfriend"){
				KnifeCMS.$id(fn_DiaTextID).innerHTML=Lang_Js_Friend[42];//"对方已经是你的好友，请在好友页面删除.";
			}else{
				KnifeCMS.$id(fn_DiaTextID).innerHTML=Lang_Js_Cue_IllegalData;//"请不要拟造非法数据.";
			}
			window.setTimeout(function(){
									   KnifeCMS.DiaWindowCloseByID(BV_DialogID);
									   var lis=getElementsByTagNameAndName("li","frienrequest_"+BV_UserID);
									   for(var i=0;i<lis.length;i++){lis[i].parentNode.removeChild(lis[i]);}
									   },1000);
		}else{
			KnifeCMS.$id(fn_DiaTextID).innerHTML=Lang_Js_Friend[35];
			window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(BV_DialogID);},1000)
		}
	}
	this.FriendRequestBatchDo = function(BV_Action,BV_DialogID){
		var lis=KnifeCMS.$id("myfriendrequest").getElementsByTagName("li");
		if(!(lis.length>0)){ KnifeCMS.DiaWindowCloseByID(BV_DialogID); return false; }
		var fn_DialogID=KnifeCMS.CreateID();
		var fn_textid  = "dialognew_text";
		var fn_HTML    = "<div id=\""+fn_textid+"\" class=\"ml10 mr10 mb5 mt5\"></div>";
		KnifeCMS.CreateDiaWindow(360,140,fn_DialogID,Lang_Js_DiaTitle[0],fn_HTML,true,false);
		KnifeCMS.Processing(KnifeCMS.$id(fn_textid));
		var Ctl        = "friendrequestdo";
		var ajaxString = "";
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl="+Ctl+"&action="+BV_Action+"&subaction=all&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta=="ok"){
			KnifeCMS.$id("myfriendrequest").innerHTML="";
			KnifeCMS.$id(fn_textid).innerHTML=Lang_Js_Friend[47];
		}else{
			KnifeCMS.$id(fn_textid).innerHTML=Lang_Js_Friend[48];
		}
		_Friend.FriendRequestCount();
		window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID);KnifeCMS.DiaWindowCloseByID(BV_DialogID);},2000);
	}
	//创建我的好友分组下拉选择框
	this.CreateSelectMyFriendGroup = function(BV_AreaID,BV_SelectID){
		KnifeCMS.DataLoading(KnifeCMS.$id(BV_AreaID));
		var GroupDataString = Myhome.Friend.GetMyGroupData();//alert(GroupDataString);
		var array = GroupDataString.split(";");
		var fn_GroupID,fn_GroupName,fn_FriendNum;
		var Select,_Option;
			Select=document.createElement("select");
			Select.id=BV_SelectID;//"selectmyfriendgroup";
			Select.options.add(new Option(Lang_Js_Friend[30],""));
		for(var i=0;i<array.length;i++){
			fn_GroupID   = unescape(KnifeCMS.RegMatch(/"groupid":"([\s\S]*?)"/i,array[i]));
			fn_GroupName = unescape(KnifeCMS.RegMatch(/"groupname":"([\s\S]*?)"/i,array[i]));if(fn_GroupName==""){fn_GroupName=Lang_Js_Friend[29]}
			if(parseInt(fn_GroupID)>=0){
				_Option=document.createElement("option");
				_Option.value = fn_GroupID;
				_Option.text  = fn_GroupName;
				Select.options.add(_Option);
			}
		}
		KnifeCMS.$id(BV_AreaID).innerHTML="";
		KnifeCMS.$id(BV_AreaID).appendChild(Select);
	}
	//ajax获取我的分组数据
	this.GetMyGroupData = function(){
		var ajaxString = "";
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=getmygroup&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		return ajaxSta;
	}
	//ajax获取我的好友数据，PageSize='unpage'时表示不分页
	this.GetMyFriendsData = function(GroupID,PageSize,Page){
		var ajaxString = "groupid="+GroupID+"&page="+Page+"&pagesize="+PageSize;
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=getmyfriends&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta==false || ajaxSta==""){ return ""; }else{ return ajaxSta;}
	}
	//ajax获取某成员的好友数据，PageSize='unpage'时表示不分页，PageSize=Array('unpage','20')表示获取前20个好友的数据
	this.GetMembersFriendsData = function(MemberID,PageSize,Page){
		var ajaxString = "memberid="+MemberID+"&page="+Page+"&pagesize="+PageSize;
		if(PageSize.constructor == window.Array ){
			if(PageSize[0]=='unpage'){var ajaxString = "memberid="+MemberID+"&pagesize=unpage&topnum="+PageSize[1];}
		}
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=getmembersfriends&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta==false || ajaxSta==""){ return ""; }else{ return ajaxSta;}
	}
	this.Del = function(FriendID){
		FriendID=parseInt(FriendID);
		if(FriendID>0){
			var Name=KnifeCMS.$id("member_name_"+FriendID).innerHTML;
			var fn_DialogID=KnifeCMS.CreateID();
			var fn_HTML    ="<div class=\"p10\" id=\"dialogtext_new\">"+Lang_Js_Friend[16].replace(/{tpl:name}/g,Name)+"</div>";
		    var fn_Btn     ="<input type=\"button\" class=\"sysBtn ml5\" onclick=\"Myhome.Friend.Deleteing(this,'"+FriendID+"')\" value="+Lang_Js_Btn_Confirm+"><input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Cancel+">";
			KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,Lang_Js_DiaTitle[0],Array(fn_HTML,fn_Btn),true,true);
		}
	}
	this.Deleteing = function(_this,FriendID){
		KnifeCMS.DiaWindowCloseByID(_this.parentNode.parentNode.parentNode.id);
		var Name=KnifeCMS.$id("member_name_"+FriendID).innerHTML;
		var fn_DialogID=KnifeCMS.CreateID();
		var fn_HTML    ="<div class=\"p10\" id=\"dialogtext_new\"></div>";
		var fn_Btn     ="<input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Close+">";
		KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,Lang_Js_DiaTitle[0],Array(fn_HTML,fn_Btn),true,true);
		var text_id="dialogtext_new";
		KnifeCMS.Processing(KnifeCMS.$id(text_id));
		var ajaxString = "friendid="+FriendID;
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=frienddelete&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta=="ok"){
			KnifeCMS.$id(text_id).innerHTML=Lang_Js_Friend[17].replace(/{tpl:name}/g,Name);
			if(objExist(KnifeCMS.$id("myfriend_"+FriendID))){KnifeCMS.$id("myfriend_"+FriendID).parentNode.removeChild(KnifeCMS.$id("myfriend_"+FriendID));}
			//if(KnifeCMS.$id("friendgroup_"+GroupID).getElementsByTagName("a")[0].className=="current"){KnifeCMS.$id(Myhome.Friend.FriendListAreaID).innerHTML="";}
			//KnifeCMS.$id("friendgroup_"+GroupID).parentNode.removeChild(KnifeCMS.$id("friendgroup_"+GroupID));
		}else{
			KnifeCMS.$id(text_id).innerHTML=Lang_Js_Friend[18].replace(/{tpl:name}/g,Name);
		}
		window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID)},1500);
	}
	this.FriendGroupListAction = function(objID){
		if(objExist(document.getElementById(objID))){
			var ul=document.getElementById(objID);
			var lis=ul.getElementsByTagName("li");
			for(var i=0; i<lis.length; i++){
				lis[i].onmouseover = function(){
					this.getElementsByTagName("span")[0].style.display="block";
				}
				lis[i].onmouseout = function(){
					this.getElementsByTagName("span")[0].style.display="none";
				}
			}
			var a=ul.getElementsByTagName("a");
			for(var i=0; i<a.length; i++){
				a[i].onclick = function(){
					var menu_list=ul.getElementsByTagName("a");
					for(var j=0; j<menu_list.length; j++){
						menu_list[j].className="normal";
					}
					this.className="current";
					Myhome.Friend.FriendList(this.parentNode.getAttribute("groupid"),Myhome.Friend.FriendListPageSize,0);
				}
			}
		}
	}
	//好友分组
	this.CreateMyGroupPanel = function(area){
		var panelarea=KnifeCMS.$id(area);
		if(objExist(panelarea)){
			panelarea.innerHTML="";
			var GroupDataString = Myhome.Friend.GetMyGroupData();
			var array=GroupDataString.split(";");
			var fn_GroupID,fn_GroupName,fn_FriendNum,DoString;
			var ul,li;
			var ul_ID="friendgroup";
			ul=document.createElement("ul");
			ul.id=ul_ID;
			ul.className="friendgroup";
			li=document.createElement("li");
			li.setAttribute("groupid","");
			li.id=ul_ID+"_0";
			li.onclick = function(){Myhome.Friend.FriendList('',Myhome.Friend.FriendListPageSize,0,'friendlisting')}
			li.innerHTML="<a href=\"javascript:;\" class=\"current\"><em class=\"icon icon_friend\"></em><b>"+Lang_Js_Friend[0]+"</b></a><span class=\"do\"></span>";
			ul.appendChild(li);
			for(var i=0; i<array.length; i++){
				fn_GroupID   = unescape(KnifeCMS.RegMatch(/"groupid":"([\s\S]*?)"/i,array[i]));
				fn_GroupName = unescape(KnifeCMS.RegMatch(/"groupname":"([\s\S]*?)"/i,array[i]));//if(fn_GroupName==""){fn_GroupName="未分组好友";}
				fn_FriendNum = unescape(KnifeCMS.RegMatch(/"friendnum":"([\s\S]*?)"/i,array[i]));
				if(parseInt(fn_GroupID)>=0){
					if(fn_GroupID=='0' || fn_GroupID==0){ DoString = "<span class=\"do\"></span>"}else{DoString = "<span class=\"do\"><em class=\"icon icon_friend_groupedit\" onclick=\"Myhome.Friend.EditGroup('"+fn_GroupID+"')\" title=\""+Lang_Js_Btn_Edit+"\"></em><em class=\"icon icon_friend_groupdel\" onclick=\"Myhome.Friend.DeleteGroup('"+fn_GroupID+"')\" title=\""+Lang_Js_Btn_Delete+"\"></em></span>"}
					li=document.createElement("li");
					li.setAttribute("groupid",fn_GroupID);
					li.id=ul_ID+"_"+fn_GroupID;
					li.innerHTML="<a href=\"javascript:;\"><em class=\"icon icon_friend_group\"></em><b>"+fn_GroupName+"</b> ("+fn_FriendNum+")</a>"+DoString;
					//li.onclick = function(){Myhome.Friend.FriendList(this.getAttribute("groupid"),Myhome.Friend.FriendListPageSize,0)}
					ul.appendChild(li);
				}
			}
			panelarea.appendChild(ul);
			Myhome.Friend.FriendGroupListAction(ul_ID);
		}else{
			alert(Lang_Js[2]);
		}
	}
	//分组成员列表
	this.GroupMyFriendPage = function(GroupID,PageSize,Page){
		var area    = "groupfriendlistarea";
		var ul_id   = "myfsbox2_list";
		var pagefunction = "javascript:Myhome.Friend.GroupMyFriendPage";
		Myhome.Friend.MyFriendsPageing(GroupID,PageSize,Page,area,ul_id,pagefunction);
	}
	//选择成员分页列表
	this.SelectMyFriendsPage = function(GroupID,PageSize,Page){
		var area    = "selectfriendlistarea";
		var ul_id   = "myfsbox3_list";
		var pagefunction = "javascript:Myhome.Friend.SelectMyFriendsPage";
		Myhome.Friend.MyFriendsPageing(GroupID,PageSize,Page,area,ul_id,pagefunction);
	}
	//通过下拉框按组选择成员
	this.SelectMyFriendsByGroup = function(_this,PageSize,Page){
		var GroupID=_this.value;
		Myhome.Friend.SelectMyFriendsPage(GroupID,PageSize,Page);
	}
	//好友列表
	this.FriendListPage = function(GroupID,PageSize,Page){Myhome.Friend.FriendList(GroupID,PageSize,Page);}
	this.FriendList = function(GroupID,PageSize,Page){
		var HaveFriend=false;
		var Area_ID=Myhome.Friend.FriendListAreaID;
		KnifeCMS.DataLoading(KnifeCMS.$id(Area_ID));
		var FriendDataString = Myhome.Friend.GetMyFriendsData(GroupID,PageSize,Page);
		var PageString = unescape(KnifeCMS.RegMatch(/"pagestring":"([\s\S]*?)"/,FriendDataString));
		    PageString = PageString.replace(/javascript:Page/g,"javascript:Myhome.Friend.FriendListPage");
		var FriendString = KnifeCMS.RegMatch(/{([\s\S]*?)}/,FriendDataString);
		     //alert(FriendString);
		if(!KnifeCMS.IsNull(FriendString)){
			var array = FriendString.split(";");
			var id,name,headimg,gourpid,gourpname,latestfeeds,ul,li;
			ul=document.createElement("ul");
			ul.className="itemImglist1 itemfriend";
			for(var i=0; i<array.length; i++){
				id          = unescape(KnifeCMS.RegMatch(/"id":"([\s\S]*?)"/,array[i]));
				name        = unescape(KnifeCMS.RegMatch(/"name":"([\s\S]*?)"/,array[i]));
				headimg     = unescape(KnifeCMS.RegMatch(/"headimg":"([\s\S]*?)"/,array[i]));
				gourpid     = unescape(KnifeCMS.RegMatch(/"gourpid":"([\s\S]*?)"/,array[i]));
				gourpname   = unescape(KnifeCMS.RegMatch(/"groupname":"([\s\S]*?)"/,array[i]));
				latestfeeds = KnifeCMS.Substr(unescape(KnifeCMS.RegMatch(/"latestfeeds":"([\s\S]*?)"/,array[i])),0,70);
				//if(gourpname==""){gourpname="未分组好友";}
				if(name.length>0){
					HaveFriend=true;
					li=document.createElement("li");
					li.setAttribute("userid",id);
					li.id="myfriend_"+id;
					li.innerHTML="<div class=\"boxl\"><span class=\"simg\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td><a href=\"?user/"+id+filesuffix+"\" target=\"_blank\"><img src=\""+headimg+"\" width=\"50\" height=\"50\" /></a></td></tr></table></span></div><div class=\"boxr\"><div class=\"rightdo\"><a href=\"javascript:;\" class=\"lli\" onclick=\"Myhome.Friend.Del('"+id+"')\">"+Lang_Js_Btn_DelForUnfriend+"<a></div><div class=\"textinfo\"><p class=\"name\"><a href=\"?user/"+id+filesuffix+"\" id=\"member_name_"+id+"\" target=\"_blank\">"+name+"</a><span class=\"font12 ml10\">"+gourpname+"</span></p><div class=\"dd\"><p>"+latestfeeds+"</p></div></div></div>";
					ul.appendChild(li);
				}
			}
		}
		KnifeCMS.$id(Area_ID).innerHTML="";
		if(HaveFriend){
			KnifeCMS.$id(Area_ID).appendChild(ul);
			var divpage=document.createElement("div");
			divpage.innerHTML=PageString;
			KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[0]).innerHTML="";
			KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[0]).appendChild(divpage);
			KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[1]).innerHTML=KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[0]).innerHTML;
		}else{
			KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[0]).innerHTML="";
			KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[1]).innerHTML="";
			KnifeCMS.$id(Area_ID).innerHTML="<div style=\"border:1px solid #EEE; padding:10px;\">"+Lang_Js_Friend[15]+"</div>";
		}
	}
	
	//成员分页列表
	this.MyFriendsPageing = function(GroupID,PageSize,Page,area,ul_id,pagefunction){
		KnifeCMS.DataLoading(KnifeCMS.$id(area));
		var FriendDataString = Myhome.Friend.GetMyFriendsData(GroupID,PageSize,Page);
		    //alert(FriendDataString);
		var PageString = unescape(KnifeCMS.RegMatch(/"pagestring":"([\s\S]*?)"/i,FriendDataString));
		     PageString = PageString.replace(/javascript:Page/g,pagefunction);
		var array = KnifeCMS.RegMatch(/{([\s\S]*?)}/i,FriendDataString).split(";");
		var id,name,headimg,ul,li;
		ul=document.createElement("ul")
		ul.id       =ul_id;
		ul.className="myfsbox2_list";
		for(var i=0; i<array.length; i++){
			id      = unescape(KnifeCMS.RegMatch(/"id":"([\s\S]*?)"/i,array[i]));
			name    = unescape(KnifeCMS.RegMatch(/"name":"([\s\S]*?)"/i,array[i]));
			headimg = unescape(KnifeCMS.RegMatch(/"headimg":"([\s\S]*?)"/i,array[i]));
			if(name.length>0){
				li=document.createElement("li");
				li.setAttribute("userid",id);
				li.id=ul.id+"_"+id;
				li.innerHTML="<span class=\"picbox\"><img src=\""+headimg+"\" /></span><h4>"+name+"</h4>"
				ul.appendChild(li);
				li.onmouseover = function(){if(this.className==""){this.className="hover"}}
				li.onmouseout  = function(){if(this.className=="hover"){this.className=""}}
				li.onclick     = function(){if(area!="groupfriendlistarea"){if(this.className=="current" || this.className=="selected"){this.className="";}else{this.className="current";}}}
			}			
		}
		KnifeCMS.$id(area).innerHTML="";
		KnifeCMS.$id(area).appendChild(ul);
		var divpage=KnifeCMS.$id(ul_id+"_page");
		if(objExist(divpage)){ var _thispnode=divpage.parentNode;_thispnode.removeChild(divpage);}
		divpage=document.createElement("div");
		divpage.id=ul_id+"_page";
		divpage.className="mt5 ml3";
		divpage.innerHTML=PageString;
		KnifeCMS.$id(area).parentNode.appendChild(divpage);
		if(area=="groupfriendlistarea"){Myhome.Friend.ReOrderFriendsForDelete(area);}
	}
	this.ReOrderFriendsForDelete = function(area_id){
		var lis=KnifeCMS.$id(area_id).getElementsByTagName("li");
		for(var i=0; i<lis.length; i++){
			lis[i].onmouseover = function(){
				if(this.className==""){this.className="hover"}
				var _li=this;
				var span=_li.getElementsByTagName("span");
				if(!objExist(span[1])){
					var del=document.createElement("span");del.className="del";
					var a=document.createElement("a");a.className="icon icon_close";a.title=Lang_Js_Friend[3];
					a.onclick     = function(){_li.parentNode.removeChild(_li);}
					a.onmouseover = function(){this.className="icon icon_close_hover";}
					a.onmouseout  = function(){this.className="icon icon_close";}
					del.appendChild(a);
					_li.appendChild(del);
				}
			}
		}
	}
	//添加分组
	this.AddNewFriendGroup = function(){
		_Friend.CreateAddMyGroupDiaWin("Myhome.Friend.CreateMyGroupPanel('myfriendgroup_panel')");
	}
	//创建添加分组的窗口
	this.CreateAddMyGroupDiaWin = function(Suc_function){
		var fn_DialogID=KnifeCMS.CreateID();
		var fn_Title   =Lang_Js_Friend[1];
		var fn_HTML    ="<div id=\"fixbox\" style=\"padding:0px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:710px;\" class=\"infoTable\"><tbody><tr><td class=\"titletd\">"+Lang_Js_Friend[45]+":</td><td class=\"infotd\"><input type=\"text\" class=\"text\" name=\"groupname\" id=\"groupname_input\" groupid=\"\" value=\"\" /></td></tr></tbody></table><div class=\"mt2 pt5 pl5\">"+Lang_Js_Friend[46]+"<span class=\"ml20 blue_a\"><a href=\"javascript:;\" onclick=\"Myhome.Friend.FriendListInAddGroup()\">+"+Lang_Js_Btn_Add+"</a></span></div><div id=\"groupfriendlistarea\"><ul class=\"myfsbox2_list\" id=\"myfsbox2_list\"></ul></div></div>";
		var fn_Btn     ="<input type=\"button\" class=\"sysBtn ml5\" onclick=Myhome.Friend.GroupSave(this,'add',\""+ Suc_function +"\") value="+Lang_Js_Btn_ConfirmSave+"><input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Close+">";
		KnifeCMS.CreateDiaWindow(716,528,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,false);
		//Myhome.Friend.GroupMyFriendPage('','unpage','0');
	}
	//编辑分组
	this.EditGroup = function(GroupID){
		GroupID=parseInt(GroupID);
		if(GroupID>0){
			var Suc_function = "Myhome.Friend.CreateMyGroupPanel('myfriendgroup_panel')";
			var fn_DialogID = KnifeCMS.CreateID();
			var fn_Title    = Lang_Js_Friend[7];
			var fn_HTML     = "<div id=\"fixbox\" style=\"padding:0px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:710px;\" class=\"infoTable\"><tbody><tr><td class=\"titletd\">"+Lang_Js_Friend[45]+":</td><td class=\"infotd\"><input type=\"text\" class=\"text\" name=\"groupname\" id=\"groupname_input\" groupid=\""+GroupID+"\" value=\"\" /></td></tr></tbody></table><div class=\"mt2 pt5 pl5\">"+Lang_Js_Friend[46]+"<span class=\"ml20 blue_a\"><a href=\"javascript:;\" onclick=\"Myhome.Friend.FriendListInAddGroup()\">+"+Lang_Js_Btn_Add+"</a></span></div><div id=\"groupfriendlistarea\"></div></div>";
			var fn_Btn     ="<input type=\"button\" class=\"sysBtn ml5\" onclick=Myhome.Friend.GroupSave(this,'edit',\""+ Suc_function +"\") value="+Lang_Js_Btn_ConfirmSave+"><input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Close+">";
			KnifeCMS.CreateDiaWindow(716,528,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,false);
			var GroupName=KnifeCMS.$id("friendgroup_"+GroupID).getElementsByTagName("b")[0].innerHTML;
			//KnifeCMS.$id("groupname_input").setAttribute("groupid",GroupID);
			KnifeCMS.$id("groupname_input").value=GroupName;
			Myhome.Friend.GroupMyFriendPage(GroupID,'unpage','0');
		}
	}
	this.GroupSave = function(_this,Action,Suc_function){
		var Ctl="";
		var Msg=[];
		    Msg[1]=Lang_Js_Friend[12];
			Msg[2]=Lang_Js_Friend[13];
			Msg[3]=Lang_Js_Friend[14];
		if(Action=="add"){
			Ctl="groupadd";
		}else if(Action=="edit"){
			Ctl="groupedit";
		}else{
			return false;
		}
		var GroupID  =KnifeCMS.$id("groupname_input").getAttribute("groupid");
		var GroupName=KnifeCMS.$id("groupname_input").value;
		var FriendID="";
		if(KnifeCMS.IsNull(GroupName)){KnifeCMS.$id("groupname_input").focus();return false;}
		var DialogNew_ID=KnifeCMS.CreateID();
		KnifeCMS.CreateDiaWindow(400,160,DialogNew_ID,Lang_Js_DiaTitle[0],"<div class=\"p10\" id=\"dialogtext_new\"></div>",true,false);
		KnifeCMS.DataSending(KnifeCMS.$id("dialogtext_new"));
		var ul=KnifeCMS.$id("myfsbox2_list");
		var lis=ul.getElementsByTagName("li");
		for(var i=0; i<lis.length; i++){
			if(i==0){FriendID=lis[i].getAttribute("userid")}else{FriendID=FriendID+","+lis[i].getAttribute("userid");}
		}
		var ajaxString = "groupid="+escape(GroupID)+"&groupname="+escape(GroupName)+"&friendid="+escape(FriendID);
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl="+Ctl+"&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta=="ok"){
			KnifeCMS.$id("dialogtext_new").innerHTML=Msg[1];
			eval(Suc_function);
		}else{
			if(ajaxSta=="exist"){
				KnifeCMS.$id("dialogtext_new").innerHTML="["+GroupName+"]"+Msg[2];
			}else{
				KnifeCMS.$id("dialogtext_new").innerHTML=Msg[3];
			}
		}
		window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(_this.parentNode.parentNode.parentNode.id);KnifeCMS.DiaWindowCloseByID(DialogNew_ID)},2000);
	}
	//删除分组
	this.DeleteGroup = function(GroupID){
		GroupID=parseInt(GroupID);
		if(GroupID>0){
			var GroupName=KnifeCMS.$id("friendgroup_"+GroupID).getElementsByTagName("b")[0].innerHTML;
			var fn_DialogID=KnifeCMS.CreateID();
			var fn_HTML    ="<div class=\"p10\" id=\"dialogtext_new\">"+Lang_Js_Friend[9].replace(/{tpl:groupname}/g,GroupName)+"</div>";
		    var fn_Btn     ="<input type=\"button\" class=\"sysBtn ml5\" onclick=\"Myhome.Friend.DeleteGroupDoing(this,'"+GroupID+"')\" value="+Lang_Js_Btn_Confirm+"><input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Cancel+">";
			KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,Lang_Js_DiaTitle[0],Array(fn_HTML,fn_Btn),false,true);
		}
	}
	this.DeleteGroupDoing = function(_this,GroupID){
		KnifeCMS.DiaWindowCloseByID(_this.parentNode.parentNode.parentNode.id);
		var GroupName=KnifeCMS.$id("friendgroup_"+GroupID).getElementsByTagName("b")[0].innerHTML;
		var fn_DialogID=KnifeCMS.CreateID();
		var fn_HTML    ="<div class=\"p10\" id=\"dialogtext_new\"></div>";
		var fn_Btn     ="<input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Close+">";
		KnifeCMS.CreateDiaWindow(400,140,fn_DialogID,Lang_Js_DiaTitle[0],Array(fn_HTML,fn_Btn),true,true);
		var text_id="dialogtext_new";
		KnifeCMS.Processing(KnifeCMS.$id(text_id));
		var ajaxString = "groupid="+GroupID;
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=groupdelete&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta=="ok"){
			KnifeCMS.$id(text_id).innerHTML=Lang_Js_Friend[10].replace(/{tpl:groupname}/g,GroupName);
			if(KnifeCMS.$id("friendgroup_"+GroupID).getElementsByTagName("a")[0].className=="current"){
				KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[0]).innerHTML="";
				KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[1]).innerHTML="";
				KnifeCMS.$id(Myhome.Friend.FriendListAreaID).innerHTML="";
			}
			KnifeCMS.$id("friendgroup_"+GroupID).parentNode.removeChild(KnifeCMS.$id("friendgroup_"+GroupID));
		}else{
			KnifeCMS.$id(text_id).innerHTML=Lang_Js_Friend[11].replace(/{tpl:groupname}/g,GroupName);
		}
		window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID)},1500);
	}
	//选择成员
	this.FriendListInAddGroup = function(){
		var GroupID  = 0;
		var PageSize = Myhome.Friend.FriendListPageSize;
		var Page     = 0;
		var fn_DialogID=KnifeCMS.CreateID();
		var fn_Title   =Lang_Js_Friend[2];
		var fn_HTML    ="<div class=\"pt5 pl5 pb3 pr5\" id=\"selectfriend_bygroup\"></div><div id=\"selectfriendlistarea\"></div>";
		var fn_Btn     ="<input type=\"button\" class=\"sysBtn ml5\" onclick=\"Myhome.Friend.AddFriendToGroup()\" value="+Lang_Js_Btn_AddToGroup+"><input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Close+">";
		KnifeCMS.CreateDiaWindow(580,486,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,true);
		KnifeCMS.DataLoading(KnifeCMS.$id("selectfriend_bygroup"))
		var GroupDataString = Myhome.Friend.GetMyGroupData();
		//alert(GroupDataString);
		var array = GroupDataString.split(";");
		var fn_GroupID,fn_GroupName,fn_FriendNum;
		var Select,_Option;
			Select=document.createElement("select");
			Select.options.add(new Option(Lang_Js_Friend[0],""));
			Select.onchange = function(){Myhome.Friend.SelectMyFriendsByGroup(this,PageSize,this.value);}
		for(var i=0;i<array.length;i++){
			fn_GroupID   = unescape(KnifeCMS.RegMatch(/"groupid":"([\s\S]*?)"/i,array[i]));
			fn_GroupName = unescape(KnifeCMS.RegMatch(/"groupname":"([\s\S]*?)"/i,array[i]));if(fn_GroupName==""){fn_GroupName="未分组好友";}
			fn_FriendNum = unescape(KnifeCMS.RegMatch(/"friendnum":"([\s\S]*?)"/i,array[i]));
			if(parseInt(fn_GroupID)>=0){
				_Option=document.createElement("option");
				_Option.value = fn_GroupID;
				_Option.text  = fn_GroupName+"("+fn_FriendNum+")";
				Select.options.add(_Option);
			}
			
		}
		KnifeCMS.$id("selectfriend_bygroup").innerHTML="";
		KnifeCMS.$id("selectfriend_bygroup").appendChild(Select);
		Myhome.Friend.SelectMyFriendsPage("",PageSize,Page);
	}
	this.AddFriendToGroup = function(){
		var GroupMyFriendPage_ul   = document.getElementById("myfsbox2_list");
		var SelectMyFriendsPage_ul = document.getElementById("myfsbox3_list");
		if(objExist(GroupMyFriendPage_ul) && objExist(SelectMyFriendsPage_ul)){
			var Success=false;
			var HaveExist=false;
			var DialogNew_ID=KnifeCMS.CreateID();
			KnifeCMS.CreateDiaWindow(400,160,DialogNew_ID,Lang_Js_DiaTitle[0],"<div class=\"p10\" id=\"dialogtext_new\"></div>",true,true);
			KnifeCMS.Processing(KnifeCMS.$id("dialogtext_new"));
			var lis=SelectMyFriendsPage_ul.getElementsByTagName("li");
			var li,li_id;
			for(var i=0;i<lis.length;i++){
				if(lis[i].className=="current"){
					li_id=GroupMyFriendPage_ul.id+"_"+lis[i].id.split("_")[2];
					//如果所选的好友不在目标分组中则加入
					if(!objExist(KnifeCMS.$id(li_id))){
						li=document.createElement("li");
						li.id        = li_id;
						li.className = "selected";
						li.setAttribute("userid",lis[i].getAttribute("userid"));
						li.innerHTML = lis[i].innerHTML;
						GroupMyFriendPage_ul.appendChild(li);
						lis[i].className="selected";
						Success=true;
					}else{
						lis[i].className="";
						HaveExist=true;
					}
				}
			}
			if(Success){KnifeCMS.$id("dialogtext_new").innerHTML=Lang_Js_Friend[4];Myhome.Friend.ReOrderFriendsForDelete("myfsbox2_list");}else{if(HaveExist){KnifeCMS.$id("dialogtext_new").innerHTML=Lang_Js_Friend[5];}else{KnifeCMS.$id("dialogtext_new").innerHTML=Lang_Js_Friend[6];}}
			window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(DialogNew_ID)},1500);
		}else{
			alert(Lang_Js[2]);
		}
	}
	//某成员的好友
	this.MembersFriends = function(MemberID,PageSize,Page){
		var HaveFriend=false;
		var Area_ID=Myhome.Friend.MembersFriendsAreaID;
		KnifeCMS.DataLoading(KnifeCMS.$id(Area_ID));
		var FriendDataString = Myhome.Friend.GetMembersFriendsData(MemberID,PageSize,Page);
		var FriendString = KnifeCMS.RegMatch(/{([\s\S]*?)}/,FriendDataString);
		if(!KnifeCMS.IsNull(FriendString)){
			var array = FriendString.split(";");
			var id,name,headimg,gourpid,gourpname,latestfeeds,ul,li;
			ul=document.createElement("ul");
			ul.className="people-list";
			for(var i=0; i<array.length; i++){
				id          = unescape(KnifeCMS.RegMatch(/"id":"([\s\S]*?)"/,array[i]));
				name        = unescape(KnifeCMS.RegMatch(/"name":"([\s\S]*?)"/,array[i]));
				headimg     = unescape(KnifeCMS.RegMatch(/"headimg":"([\s\S]*?)"/,array[i]));
				gourpid     = unescape(KnifeCMS.RegMatch(/"gourpid":"([\s\S]*?)"/,array[i]));
				gourpname   = unescape(KnifeCMS.RegMatch(/"groupname":"([\s\S]*?)"/,array[i]));
				latestfeeds = KnifeCMS.Substr(unescape(KnifeCMS.RegMatch(/"latestfeeds":"([\s\S]*?)"/,array[i])),0,70);
				if(name.length>0){
					HaveFriend=true;
					li=document.createElement("li");
					li.setAttribute("userid",id);
					li.innerHTML="<a href=\"?user/"+id+filesuffix+"\" title=\""+name+"\" class=\"avatar\"><img width=\"50\" height=\"50\" src=\""+headimg+"\"></a><div class=\"people-detail\"><a href=\"?user/"+id+filesuffix+"\" title=\""+name+"\" class=\"lively-user\">"+name+"</a></div>";
					ul.appendChild(li);
				}
			}
		}
		KnifeCMS.$id(Area_ID).innerHTML="";
		if(HaveFriend){
			KnifeCMS.$id(Area_ID).appendChild(ul);
		}else{
			KnifeCMS.$id(Area_ID).innerHTML="<p style=\"padding-top:10px;\">"+Lang_Js_Friend[19]+"</p>";
		}
	}
	//好友列表
	this.MembersFriendListPage = function(MemberID,PageSize,Page){Myhome.Friend.FriendList(MemberID,PageSize,Page);}
	this.MembersFriendList = function(MemberID,PageSize,Page){
		var HaveFriend=false;
		var Area_ID=Myhome.Friend.FriendListAreaID;
		KnifeCMS.DataLoading(KnifeCMS.$id(Area_ID));
		var FriendDataString = Myhome.Friend.GetMembersFriendsData(MemberID,PageSize,Page);
		var PageString = unescape(KnifeCMS.RegMatch(/"pagestring":"([\s\S]*?)"/,FriendDataString));
		    PageString = PageString.replace(/javascript:Page/g,"javascript:Myhome.Friend.MembersFriendListPage");
		var FriendString = KnifeCMS.RegMatch(/{([\s\S]*?)}/,FriendDataString);
		     //alert(FriendString);
		if(!KnifeCMS.IsNull(FriendString)){
			var array = FriendString.split(";");
			var id,name,headimg,latestfeeds,ul,li;
			ul=document.createElement("ul");
			ul.className="itemImglist1 itemfriend";
			for(var i=0; i<array.length; i++){
				id          = unescape(KnifeCMS.RegMatch(/"id":"([\s\S]*?)"/,array[i]));
				name        = unescape(KnifeCMS.RegMatch(/"name":"([\s\S]*?)"/,array[i]));
				headimg     = unescape(KnifeCMS.RegMatch(/"headimg":"([\s\S]*?)"/,array[i]));
				latestfeeds = KnifeCMS.Substr(unescape(KnifeCMS.RegMatch(/"latestfeeds":"([\s\S]*?)"/,array[i])),0,70);
				//if(gourpname==""){gourpname="未分组好友";}
				if(name.length>0){
					HaveFriend=true;
					li=document.createElement("li");
					li.setAttribute("userid",id);
					li.id="myfriend_"+id;
					li.innerHTML="<div class=\"boxl\"><span class=\"simg\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td><a href=\"?user/"+id+filesuffix+"\" target=\"_blank\"><img src=\""+headimg+"\" width=\"50\" height=\"50\" /></a></td></tr></table></span></div><div class=\"boxr\"><div class=\"rightdo\"><a href=\"javascript:;\" class=\"lli\" onclick=\"Myhome.Friend.AddForFriend("+id+",'"+name+"','"+headimg+"')\">"+Lang_Js_Btn_AddForFriend+"<a></div><div class=\"textinfo\"><p class=\"name\"><a href=\"?user/"+id+filesuffix+"\" id=\"member_name_"+id+"\" target=\"_blank\">"+name+"</a></p><div class=\"dd\"><p>"+latestfeeds+"</p></div></div></div>";
					ul.appendChild(li);
				}
			}
		}
		KnifeCMS.$id(Area_ID).innerHTML="";
		if(HaveFriend){
			KnifeCMS.$id(Area_ID).appendChild(ul);
			var divpage=document.createElement("div");
			divpage.innerHTML=PageString;
			KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[0]).innerHTML="";
			KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[0]).appendChild(divpage);
			KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[1]).innerHTML=KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[0]).innerHTML;
		}else{
			KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[0]).innerHTML="";
			KnifeCMS.$id(Myhome.Friend.FriendListAreaPageingID[1]).innerHTML="";
			KnifeCMS.$id(Area_ID).innerHTML="<div style=\"border:1px solid #EEE; padding:10px;\">"+Lang_Js_Friend[15]+"</div>";
		}
	}
}
function class_MyhomeVisitor(){
	var _Visitor=this;
	this.VisitorAreaID   = "";
	this.BeVisitedAreaID = "";
	//ajax获取 来访人员 或 被访问过的成员 数据 
	this.GetVisitorData = function(MemberID,Ctl){
		var ajaxString = "memberid="+MemberID;
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl="+Ctl+"&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta==false || ajaxSta==""){ return ""; }else{ return ajaxSta;}
	}
	this.CreateVisitorPanel = function(MemberID,Ctl,Area_ID){
		var HaveVisitor=false;
		var DataString = _Visitor.GetVisitorData(MemberID,Ctl);
		var VisitorString = KnifeCMS.RegMatch(/{([\s\S]*?)}/,DataString);
		if(!KnifeCMS.IsNull(VisitorString)){
			var array = VisitorString.split(";");
			var id,name,headimg,visittime,ul,li;
			ul=document.createElement("ul");
			ul.className="itemImglist1";
			for(var i=0; i<array.length; i++){
				id          = unescape(KnifeCMS.RegMatch(/"id":"([\s\S]*?)"/,array[i]));
				name        = unescape(KnifeCMS.RegMatch(/"name":"([\s\S]*?)"/,array[i]));
				headimg     = unescape(KnifeCMS.RegMatch(/"headimg":"([\s\S]*?)"/,array[i]));
				visittime   = unescape(KnifeCMS.RegMatch(/"visittime":"([\s\S]*?)"/,array[i]));
				var month   = KnifeCMS.GetDateTime("month",visittime);
				var day     = KnifeCMS.GetDateTime("day",visittime);
				var hour    = KnifeCMS.GetDateTime("hour",visittime);
				var minutes = KnifeCMS.GetDateTime("minutes",visittime);
				visittime    = month+Lang_Js_Month+day+""+Lang_Js_Day+" "+hour+":"+minutes;
				if(name.length>0){
					HaveVisitor=true;
					li=document.createElement("li");
					li.setAttribute("userid",id);
					li.innerHTML="<div class=\"boxl\"><span class=\"simg\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td><a href=\"?user/"+id+filesuffix+"\" target=\"_blank\"><img src=\""+headimg+"\" /></a></td></tr></table></span></div><div class=\"boxr\"><p class=\"name\"><a href=\"?user/"+id+filesuffix+"\" target=\"_blank\">"+name+"</a></p><div class=\"dd\"><p class=\"time\">"+visittime+"</p><p><em class=\"icon icon_friend_groupadd\" onclick=\"Myhome.Friend.AddForFriend("+id+",'"+name+"','"+headimg+"')\" title=\"加为好友\"></em></p></div></div>";
					ul.appendChild(li);
				}
			}
		}
		KnifeCMS.$id(Area_ID).innerHTML="";
		if(HaveVisitor){
			KnifeCMS.$id(Area_ID).appendChild(ul);
		}else{
			KnifeCMS.$id(Area_ID).innerHTML="<p style=\"padding-top:10px;\">"+Lang_Js_User[45]+"</p>";
		}
		//KnifeCMS.$id(Area_ID).innerHTML=VisitorString;
	}
	this.MyVisitor = function(MemberID){
		_Visitor.CreateVisitorPanel(MemberID,"getvisitor",_Visitor.VisitorAreaID);
	}
	this.BeVisited = function(MemberID){
		_Visitor.CreateVisitorPanel(MemberID,"getbevisited",_Visitor.BeVisitedAreaID);
	}
}

function class_MyhomeNotify(){
	var _Notify=this;
	this.NotifyCount = function(){
		if(objExist(KnifeCMS.$id("mynotifylist")) && objExist(KnifeCMS.$id("mynotifycount"))){
			var lis=KnifeCMS.$id("mynotifylist").getElementsByTagName("li");
			KnifeCMS.$id("mynotifycount").innerHTML=lis.length;
		}
	}
	this.GetMyNotify=function(){
		var fn_DialogID=KnifeCMS.CreateID();
		var fn_textid  ="mynotifylist";
		var fn_HTML    ="<div id=\""+fn_textid+"\" class=\"ml10 mr10 mb5 mt5\"></div>";
		var fn_Btn     ="<input type=\"button\" class=\"sysBtn\" onclick=\"Myhome.Notify.DeleteMyAllNotify('"+fn_DialogID+"');\" value="+Lang_Js_Btn_DeleteAll+"><input type=\"button\" class=\"sysBtn_grey ml5\" onclick=\"Myhome.Notify.NotifyCount();KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Close+">";
		KnifeCMS.CreateDiaWindow(680,440,fn_DialogID,Lang_Js_User[50],Array(fn_HTML,fn_Btn),true,false);
		KnifeCMS.DataLoading(KnifeCMS.$id(fn_textid));
		var ajaxString = "";
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=getnotify&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//KnifeCMS.$id(fn_textid).innerHTML=ajaxSta;
		//alert(ajaxSta);
		if(ajaxSta===false){
			KnifeCMS.$id(fn_textid).innerHTML=Lang_Js_Cue_LoadingFail;
		}else if(ajaxSta==""){
			KnifeCMS.$id(fn_textid).innerHTML=Lang_Js_User[53];//"暂无消息";
		}else{
			var HaveNotify=false;
			var DataString = ajaxSta;
			if(!KnifeCMS.IsNull(DataString)){
				var array = DataString.split(";");
				var fn_id,fn_userid,fn_username,headimg,message,notifytype,time,ul,li;
				var fn_TypeText="";
				var fn_MsgText="";
				ul=document.createElement("ul");
				ul.className="itemImglist1";
				for(var i=0; i<array.length; i++){
					fn_id       = unescape(KnifeCMS.RegMatch(/"id":"([\s\S]*?)"/,array[i]));
					fn_userid   = unescape(KnifeCMS.RegMatch(/"userid":"([\s\S]*?)"/,array[i]));
					fn_username = unescape(KnifeCMS.RegMatch(/"username":"([\s\S]*?)"/,array[i]));
					headimg  = unescape(KnifeCMS.RegMatch(/"headimg":"([\s\S]*?)"/,array[i]));
					message  = unescape(KnifeCMS.RegMatch(/"message":"([\s\S]*?)"/,array[i]));
					notifytype = unescape(KnifeCMS.RegMatch(/"notifytype":"([\s\S]*?)"/,array[i]));
					time       = unescape(KnifeCMS.RegMatch(/"time":"([\s\S]*?)"/,array[i]));
						if(time!=""){
						var month   = KnifeCMS.GetDateTime("month",time);
						var day     = KnifeCMS.GetDateTime("day",time);
						var hour    = KnifeCMS.GetDateTime("hour",time);
						var minutes = KnifeCMS.GetDateTime("minutes",time);
						time    = month+Lang_Js_Month+day+""+Lang_Js_Day+" "+hour+":"+minutes;
					}
					if(notifytype=="acceptfriend"){
						fn_TypeText=Lang_Js_Friend[40];//"接受了你的好友请求并加你为好友";
						fn_MsgText="<p class=\"mt3 mb3 typetext\">"+fn_TypeText+"</p>";
					}else if(notifytype=="rejectfriend"){
						fn_TypeText=Lang_Js_Friend[41]//"拒绝了你的好友请求";
						fn_MsgText="<p class=\"mt3 mb3 typetext\">"+fn_TypeText+"</p><p class=\"q_msg\"><q>\"</q>"+message+"<q>\"</q></p>";
					}
					//if(name.length>0){
						HaveNotify=true;
						li=document.createElement("li");
						li.setAttribute("name","notify_"+fn_id);
						li.setAttribute("userid",fn_userid);
						li.innerHTML="<div class=\"boxl\"><span class=\"simg\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td><a href=\"?user/"+fn_userid+filesuffix+"\" target=\"_blank\"><img src=\""+headimg+"\" /></a></td></tr></table></span></div><div class=\"boxr\"><p class=\"name\"><a href=\"?user/"+fn_userid+filesuffix+"\" target=\"_blank\">"+fn_username+"</a><span class=\"ml5\">"+time+"</span></p><div class=\"dd\">"+fn_MsgText+"<p class=\"pb5 alignright\"><input type=\"button\" class=\"sysBtn_grey_s ml5\" onclick=\"Myhome.Notify.DeleteNotify(event,'"+fn_id+"');\" value="+Lang_Js_Btn_ISee+"></p></div></div>";
						ul.appendChild(li);
					//}
				}
			}
			KnifeCMS.$id(fn_textid).innerHTML="";
			if(HaveNotify){
				KnifeCMS.$id(fn_textid).appendChild(ul);
			}else{
				KnifeCMS.$id(fn_textid).innerHTML="<p style=\"padding-top:10px;\">"+Lang_Js_User[53]+"</p>";
			}
		}
	}
	this.DeleteNotify = function(_event,BV_ID){
		window.setTimeout(function(){
								   var lis=getElementsByTagNameAndName("li","notify_"+BV_ID);
								   for(var i=0;i<lis.length;i++){lis[i].parentNode.removeChild(lis[i]);}
								   },500);
		var Ctl        = "notifydelete";
		var ajaxString = "id="+BV_ID;
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl="+Ctl+"&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
	}
	this.DeleteMyAllNotify = function(BV_DialogID){
		var lis=KnifeCMS.$id("mynotifylist").getElementsByTagName("li");
		if(!(lis.length>0)){ KnifeCMS.DiaWindowCloseByID(BV_DialogID); return false; }
		var fn_DialogID=KnifeCMS.CreateID();
		var fn_textid  = "dialognew_text";
		var fn_HTML    = "<div id=\""+fn_textid+"\" class=\"ml10 mr10 mb5 mt5\"></div>";
		KnifeCMS.CreateDiaWindow(360,140,fn_DialogID,Lang_Js_DiaTitle[0],fn_HTML,true,false);
		KnifeCMS.Processing(KnifeCMS.$id(fn_textid));
		var Ctl        = "myallnotifydelete";
		var ajaxString = "";
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl="+Ctl+"&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta=="ok"){
			KnifeCMS.$id("mynotifylist").innerHTML="";
			KnifeCMS.$id(fn_textid).innerHTML=Lang_Js_User[51];
		}else{
			KnifeCMS.$id(fn_textid).innerHTML=Lang_Js_User[52];
		}
		_Notify.NotifyCount();
		window.setTimeout(function(){KnifeCMS.DiaWindowCloseByID(fn_DialogID);KnifeCMS.DiaWindowCloseByID(BV_DialogID);},2000);
	}
}
var Myhome = new class_Myhome();
























