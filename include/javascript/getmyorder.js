function AjaxGetMyOrder(page){
	var MyOrderList = KnifeCMS.$id("myorder");KnifeCMS.DataLoading(MyOrderList);
	var fn_TempHTML = '<div class="pb10"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable"><tr class="top"><th><div>'+ Lang_Js_Order[1] +'</div></th><th><div>'+ Lang_Js_Order[2] +'</div></th><th><div>'+ Lang_Js_Order[3] +'</div></th><th><div>'+ Lang_Js_Order[4] +'</div></th><th><div>'+ Lang_Js_Order[7] +'</div></th><th><div>'+ Lang_Js_Order[5] +'</div></th><th><div>'+ Lang_Js_Order[8] +'</div></th></tr>';
	var needPay     = false;
	var payString   = "";
	var ajaxString  = "page="+page;
	var ajaxUrl     = "include/module/ajax/getmyorder.asp?r="+parseInt(Math.random()*1000);
	var ajaxSta     = posthttp(ajaxString,ajaxUrl)//;window.open(ajaxUrl);     
	//alert(ajaxSta);
	if(ajaxSta=="ERROR" || ajaxSta=="false" || ajaxSta==""){
		fn_TempHTML="<div>"+ Lang_Js_Cue_LoadingFail +"</div>";
	}else if(ajaxSta=="NOT_LOGIN"){
		KnifeCMS.LoginDiaWindow();
	}else{
		var order,JSON = KnifeCMS.JSON.parseJSON(ajaxSta);
		var pagestring = unescape(JSON.pagestring);
		for(var i=0;i<JSON.orderdata.length;i++){
			order = JSON.orderdata[i];
			if(order!=null){
				if((parseFloat(order.finalamount)-parseFloat(order.payed))>0 && order.status=="1"){
					needPay = true;
				}else{
					needPay = false;
				}
				if(needPay){payString=' | <a href="javascript:KnifeCMS.C.Order.Pay(\''+ order.orderid +'\')">'+ Lang_Js_Order[10] +'</a>'}else{payString='';}
				fn_TempHTML = fn_TempHTML +'<tr class="data"><td><a href="javascript:GetOrderDetail(\''+ order.orderid +'\')">'+ order.orderid +'</a></td><td>'+ order.createtime +'</td><td>'+ order.finalamount +'</td><td>'+ order.payed +'</td><td>'+ BackShipStatus(order.shipstatus) +'</td><td>'+ BackStatus(order.status) +'</td><td><a href="javascript:GetOrderDetail(\''+ order.orderid +'\')">'+ Lang_Js_Order[9] +'</a>'+ payString +'</td></tr>'
			}
		}
	}
	fn_TempHTML = fn_TempHTML + "</table></div>";
	KnifeCMS.$id("eofPage").innerHTML = pagestring;
	MyOrderList.innerHTML = fn_TempHTML
}
function BackStatus(status){
	status =parseInt(status);
	return Lang_Js_OrderStatus[status];
}
function BackPayStatus(status){
	status =parseInt(status);
	return Lang_Js_PayStatus[status];
}
function BackShipStatus(status){
	status =parseInt(status);
	return Lang_Js_ShipStatus[status];
}