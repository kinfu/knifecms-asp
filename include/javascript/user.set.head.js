var UploadingDialogID;
function uploadbighead(){
	KnifeCMS.$id("uploadbigheadform").submit();
	var fn_DialogID = KnifeCMS.CreateID();
	var fn_Title    = Lang_Js_DiaTitle[0];
	var fn_HTML     = "<div style=\"padding:10px 4px 5px 12px;\" id=\"uploadresultcue\">"+Lang_Js_Cue_Uploading+"<br>"+Lang_Js[30]+"</div>";
	KnifeCMS.CreateDiaWindow(320,100,fn_DialogID,fn_Title,fn_HTML,true,false);
	UploadingDialogID = fn_DialogID;
}
function uploadsmallhead(){
	KnifeCMS.$id("uploadsmallheadform").submit();
	var fn_DialogID = KnifeCMS.CreateID();
	var fn_Title    = Lang_Js_DiaTitle[0];
	var fn_HTML     = "<div style=\"padding:10px 4px 5px 12px;\" id=\"uploadresultcue\">"+Lang_Js_Cue_Uploading+"<br>"+Lang_Js[30]+"</div>";
	KnifeCMS.CreateDiaWindow(320,100,fn_DialogID,fn_Title,fn_HTML,true,false);
	UploadingDialogID = fn_DialogID;
}
function uploadingfinish(){
	var args = uploadingfinish.arguments;
	var argc = uploadingfinish.arguments.length;
	var ResultMsg = "";
	if(args[1]=="FINISH"){
		var url = args[2];
		if(!KnifeCMS.IsNull(url)){
			if(args[0]=="bighead" || args[0]=="smallhead"){AjaxUploadHead(args[0],url);}
		}
	}else{
		var size,msg = args[1];
		if(args[0]=="bighead"){size="200K"}else{size="50K"}
		KnifeCMS.DiaWindowCloseByID(UploadingDialogID);
		if(msg=="NOT_LOGIN"){
			ResultMsg = Lang_Js[6];
			KnifeCMS.LoginDiaWindow();
		}else if(msg=="HAVE_NOT_BYTES"){
			ResultMsg = Lang_Js[31]
		}else if(msg=="UPLOAD_DATA_OVER_MAXTOTALSIZE "){
			ResultMsg = Lang_Js[32]
		}else if(msg=="SYSTEM_NONSUPPORT_ADODB_STREAM"){
			ResultMsg = Lang_Js[33];
		}else if(msg=="FILE_OVER_MAXSIZE"){
			ResultMsg = Lang_Js[34].replace("{$:size}",size);
		}else if(msg=="ILLEGAL_TYPE_OF_FILE"){
			ResultMsg = Lang_Js[35];
		}else if(msg=="GET_UPLOAD_DATA_ERROR"){
			ResultMsg = Lang_Js[36];
		}else if(msg=="HAVE_NOT_UPLOAD_ANY_FILE"){
			ResultMsg = Lang_Js[37];
		}else if(msg=="UPLOAD_FILES_OVER_THE_INCEPTMAXFILE"){
			ResultMsg = Lang_Js[38];
		}else if(msg=="SAVETOFILE_ERROR"){
			ResultMsg = Lang_Js[39];
		}else if(msg=="THIS_FILE_IS_NULL"){
			ResultMsg = Lang_Js[40];
		}
		KnifeCMS.DiaWindowCueMessage(ResultMsg);
	}
}
function AjaxUploadHead(type,url){
	var ResultMsg = "";
	if(type=="bighead" || type=="smallhead"){
		var headurl,SuccessMsg;
		if(type=="bighead"){
			CurrentHead = KnifeCMS.$id("currentBigHead");
			SuccessMsg = Lang_Js[4];
		}else{
			CurrentHead = KnifeCMS.$id("currentSmallHead");
			SuccessMsg = Lang_Js[8];
		}
		var ajaxString = "headtype="+type+"&headurl="+escape(url);
		var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=uploadhead&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		//alert(ajaxSta);
		if(ajaxSta=="ok"){
			ResultMsg=SuccessMsg;
			CurrentHead.src=url;
		}else if(ajaxSta=="unlogined"){
			ResultMsg=Lang_Js[6];
		}else if(ajaxSta=="para_error"){
			ResultMsg=Lang_Js[7];
		}else{
			ResultMsg=Lang_Js[5];
		}
		KnifeCMS.DiaWindowCloseByID(UploadingDialogID);
		KnifeCMS.DiaWindowCueMessage(ResultMsg);
	}
}