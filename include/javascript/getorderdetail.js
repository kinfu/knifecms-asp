function GetOrderDetail(orderid){
	//创建弹出窗
	var fn_DialogID = KnifeCMS.CreateID();
	var fn_Title    = Lang_Js_OrderDetail +"["+orderid+"]";
	var fn_HTML     = '<div class="cart"><div id="diatext_orderdetail" class="cart_goods" style="padding:10px;"></div></div>';
	KnifeCMS.CreateDiaWindow(900,540,fn_DialogID,fn_Title,fn_HTML,true,true);
	//创建弹出窗:END
	var ObjectDiv  = KnifeCMS.$id("diatext_orderdetail");KnifeCMS.DataLoading(ObjectDiv);
	var ajaxString = "orderid="+orderid+"";
	var ajaxUrl    = "include/module/ajax/getorderdata.asp?ctl=getorderdata&orderid="+orderid+"&r="+parseInt(Math.random()*1000);
	var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
	//alert(ajaxSta);
	if(ajaxSta=="ORDER_IS_NOT_EXIST"){
		ObjectDiv.innerHTML = Lang_Js_Order[11];
	}else if(ajaxSta=="ORDER_IS_ERROR"){
		ObjectDiv.innerHTML = Lang_Js_Order[12];
	}else if(ajaxSta=="THIS_ORDER_IS_LIMIT_TO_YOU"){
		ObjectDiv.innerHTML = Lang_Js_Order[13];
	}else{
		if(ajaxSta=="error" || ajaxSta=="false" || ajaxSta==""){
			ObjectDiv.innerHTML = Lang_Js_Cue_LoadingFail;
		}else{
			CreateOrderDeatailHTML(ajaxSta);
		}
	}
}
function CreateOrderDeatailHTML(BV_DataJson){
	var ObjectDiv = KnifeCMS.$id("diatext_orderdetail");
	var Order     = KnifeCMS.JSON.parseJSON(BV_DataJson);
	var IsTax;if(parseInt(Order.istax)==1){IsTax=Lang_Js_Yes}else{IsTax=Lang_Js_No}	
	var innerHTML='<div class="font12 bold mb5 color000">'+ Lang_Js_Order[16] +'</div><div class="inbox"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable"><tr><td class="titletd">'+ Lang_Js_Order[17] +':</td><td class="infotd">'+ unescape(Order.ship_name) +'</td><td class="titletd">'+ Lang_Js_Order[18] +':</td><td class="infotd">'+ unescape(Order.ship_email) +'</td></tr><tr><td class="titletd">'+ Lang_Js_Order[19] +':</td><td class="infotd">'+ unescape(Order.shippingarea) +'</td><td class="titletd">'+ Lang_Js_Order[22] +':</td><td class="infotd">'+ unescape(Order.ship_tel) +'</td></tr><tr><td class="titletd">'+ Lang_Js_Order[21] +':</td><td class="infotd">'+ unescape(Order.ship_zip) +'</td><td class="titletd">'+ Lang_Js_Order[23] +':</td><td class="infotd">'+ unescape(Order.ship_mobile) +'</td></tr><tr><td class="titletd">'+ Lang_Js_Order[24] +':</td><td class="infotd">'+ unescape(Order.shipping) +'</td><td class="titletd">'+ Lang_Js_Order[26] +':</td><td class="infotd">'+ unescape(Order.ship_time) +'</td></tr><tr><td class="titletd">'+ Lang_Js_Order[27] +':</td><td class="infotd">'+ unescape(Order.weight) +'g</td><td class="titletd">'+ Lang_Js_Order[25] +':</td><td class="infotd">'+ unescape(Order.payment) +'</td></tr><tr><td class="titletd">'+ Lang_Js_Order[20] +':</td><td class="infotd" colspan="3">'+ unescape(Order.ship_address) +'</td></tr><tr><td class="titletd">'+ Lang_Js_Order[28] +':</td><td class="infotd" colspan="3">'+ unescape(Order.mark_text) +'</td></tr></table></div><div class="inbox"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable"><tr><td class="titletd">'+ Lang_Js_Order[29] +':</td><td class="infotd" colspan="3">'+ IsTax +'</td></tr><tr><td class="titletd">'+ Lang_Js_Order[20] +':</td><td class="infotd" colspan="3">'+ unescape(Order.tax_company) +'</td></tr></table></div><div class="font12 bold mt5 mb5 color000">'+ Lang_Js_Order[31] +'</div><div class="inbox"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable"><tr class="top"><th><div style="width:30px;">'+ Lang_Js_Order[32] +'</div></th><th><div style="width:60px;">'+ Lang_Js_Order[33] +'</div></th><th><div style="width:440px;">'+ Lang_Js_Order[34] +'</div></th><th><div style="width:88px;">'+ Lang_Js_Order[35] +'</div></th><th><div style="width:40px;">'+ Lang_Js_Order[36] +'</div></th><th><div style="width:88px;">'+ Lang_Js_Order[37] +'</div></th></tr><tbody id="cartgoods"><!--显示商品区--></tbody></table></div><div class="inbox mt5"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable"><tbody><tr><td class="titletd"><div style="width:700px;">'+ Lang_Js_Order[38] +':</div></td><td class="infotd2"><span class="font14 redh"><em class="moneySb">￥</em>'+ Order.cost_item +'</span></td></tr><tr><td class="titletd"><div style="width:700px;">'+ Lang_Js_Order[39] +':</div></td><td class="infotd2"><span class="font12 redh"><em class="moneySb">￥</em>'+ Order.cost_freight +'</span></td></tr><tr><td class="titletd"><div style="width:700px;">'+ Lang_Js_Order[40] +':</div></td><td class="infotd2"><span class="font12 redh"><em class="moneySb">￥</em>'+ Order.cost_protect +'</span></td></tr><tr><td class="titletd"><div style="width:700px;">'+ Lang_Js_Order[41] +':</div></td><td class="infotd2"><span class="font12 redh"><em class="moneySb">￥</em>'+ Order.cost_tax +'</span></td></tr><tr><td class="titletd"><div style="width:700px;">'+ Lang_Js_Order[42] +':</div></td><td class="infotd2"><strong class="font16 redh"><em class="moneySb">￥</em>'+ Order.total_amount +'</strong></td></tr></tbody></table></div>';
	ObjectDiv.innerHTML = innerHTML;
	KnifeCMS.C.Cart.GetCart(2,KnifeCMS.JSON.toJSONString(Order.cartgoods[0]));
}
function ShowCartGoods(){
	
}