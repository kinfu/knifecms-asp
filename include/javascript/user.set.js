function Class_MySet(){
	this.SaveInfo = function(id,userid){
		var form=KnifeCMS.$id(id);
		if(objExist(form)){
			var fn_DialogID=KnifeCMS.CreateID();
			var fn_Title=Lang_Js[9];
			var fn_HTML="<div id=\"fixbox\" style=\"padding:5px 4px 5px 10px;\"></div>";
			var fn_Btn ="<input type=\"button\" class=\"sysBtn ml5\" onclick=\"href('?myhome/info"+filesuffix+"')\" value="+Lang_Js_Btn_Confirm+">";
			var gender     = "";
			if(form.gender1.checked==true){
				gender     = form.gender1.value;
			}else{
				gender     = form.gender2.value;
			}
			var myhometown = "";
			var mysite     = "";
			var selects=getElementsByTagNameAndName("select","hometown");
			for(var i=0;i<selects.length;i++){
				if(selects[i].options[selects[i].selectedIndex].value!=""){
					myhometown=selects[i].options[selects[i].selectedIndex].value;
				}
			}
			selects=getElementsByTagNameAndName("select","mysite");
			for(var i=0;i<selects.length;i++){
				if(selects[i].options[selects[i].selectedIndex].value!=""){
					mysite=selects[i].options[selects[i].selectedIndex].value;
				}
			}
			var birthday   = form.birthday_year.value +"-"+ form.birthday_month.value +"-"+ form.birthday_day.value;
			//alert(myhometown);
			var ajaxString = "userid="+parseInt(userid)+"&realname="+escape(form.realname.value)+"&gender="+escape(gender)+"&myhometown="+escape(myhometown)+"&mysite="+escape(mysite)+"&birthday="+escape(birthday)+"&email="+escape(form.email.value)+"&mobile="+escape(form.mobile.value)+"&telephone="+escape(form.telephone.value)+"&qq="+escape(form.qq.value)+"&height="+escape(form.height.value)+"&weight="+escape(form.weight.value)+"&maritalstatus="+escape(form.maritalstatus.value)+"&bloodtype="+escape(form.bloodtype.value)+"&religion="+escape(form.religion.value)+"&smoke="+escape(form.smoke.value)+"&drink="+escape(form.drink.value)+"&favtravel="+escape(form.favtravel.value)+"&favliving="+escape(form.favliving.value)+"&favwork="+escape(form.favwork.value)+"&favpartner="+escape(form.favpartner.value)+"&favcar="+escape(form.favcar.value);
			
			KnifeCMS.CreateDiaWindow(320,100,fn_DialogID,fn_Title,fn_HTML,true,false);
			var CueMsgBox=KnifeCMS.$id("fixbox");
			KnifeCMS.DataSending(CueMsgBox);
			var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=saveinfo&r="+parseInt(Math.random()*1000);
			var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
			CueMsgBox=KnifeCMS.$id("fixbox");
			if(ajaxSta=="ok"){
				CueMsgBox.innerHTML=Lang_Js[10];
				KnifeCMS.$id("d_email").className="Normal";KnifeCMS.$id("d_email").innerHTML="";
				window.setTimeout(function(){href('?myhome/infoset'+filesuffix+'');},1500);
			}else if(ajaxSta=="unlogined"){
				CueMsgBox.innerHTML=Lang_Js[11];
				KnifeCMS.LoginDiaWindow();
			}else if(ajaxSta=="email_error"){
				CueMsgBox.innerHTML=Lang_Js[12];
				KnifeCMS.$id("d_email").className="d_err";
				KnifeCMS.$id("d_email").innerHTML=Lang_Js[12];
			}else if(ajaxSta=="email_used"){
				CueMsgBox.innerHTML=Lang_Js[13];
				KnifeCMS.$id("d_email").className="d_err";
				KnifeCMS.$id("d_email").innerHTML=Lang_Js[13];
			}else{
				CueMsgBox.innerHTML=Lang_Js[14];
				KnifeCMS.$id("d_email").className="Normal";KnifeCMS.$id("d_email").innerHTML="";
			}
		}else{
			alert(Lang_Js[2]);
		}
	}
	this.SaveAddress = function(id,userid){
		var form=KnifeCMS.$id(id);
		var Check=true;
		if(objExist(form)){
			var region = "";
			var selects=getElementsByTagNameAndName("select","region");
			for(var i=0;i<selects.length;i++){
				if(selects[i].options[selects[i].selectedIndex].value!=""){
					region=selects[i].options[selects[i].selectedIndex].value;
				}
			}
			if(KnifeCMS.IsNull(region)){
				Check=false;
				KnifeCMS.$id("d_region").className="d_err";
				KnifeCMS.$id("d_region").innerHTML=Lang_Js[17];
			}else{
				KnifeCMS.$id("d_region").className="Normal";
				KnifeCMS.$id("d_region").innerHTML="";
			}
			if(!KnifeCMS.CheckItem("address","d_address",Array("Normal",""),Array("d_err",Lang_Js[18]))){Check=false;}
			if(!KnifeCMS.CheckItem("zip","d_zip",Array("Normal",""),Array("d_err",Lang_Js[19]))){Check=false;}
			if(!KnifeCMS.CheckItem("name","d_name",Array("Normal",""),Array("d_err",Lang_Js[20]))){Check=false;}
			if(!KnifeCMS.CheckItem("mobile|telephone","d_telephone",Array("Normal",""),Array("d_err",Lang_Js[21]))){Check=false;}
			if(Check){
				var fn_DialogID = KnifeCMS.CreateID();
				var fn_Title    = Lang_Js[15];
				var fn_HTML     = "<div id=\"fixbox\" style=\"padding:10px 4px 5px 12px;\"></div>";
				var ajaxString = "userid="+parseInt(userid)+"&region="+escape(region)+"&address="+escape(form.address.value)+"&zip="+escape(form.zip.value)+"&name="+escape(form.name.value)+"&telephone="+escape(form.telephone.value)+"&mobile="+escape(form.mobile.value);
				KnifeCMS.CreateDiaWindow(320,100,fn_DialogID,fn_Title,fn_HTML,true,false);
				var CueMsgBox=KnifeCMS.$id("fixbox");
				KnifeCMS.DataSending(CueMsgBox);
				var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=saveaddress&r="+parseInt(Math.random()*1000);
				var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
				if(ajaxSta=="ok"){
					CueMsgBox.innerHTML=Lang_Js[16];
				}else if(ajaxSta=="unlogined"){
					CueMsgBox.innerHTML=Lang_Js[11];
					KnifeCMS.LoginDiaWindow();
				}else{
					CueMsgBox.innerHTML=Lang_Js[14];
				}
				window.setTimeout(function(){href('?myhome/deliveryset'+filesuffix+'');},1500);
			}
		}else{
			alert(Lang_Js[2]);
		}
	}
	this.ChangePassword = function(id,userid){
		var form=KnifeCMS.$id(id);
		var Check=true;
		if(objExist(form)){
			if(!KnifeCMS.CheckItem("passwordold","d_passwordold",Array("Normal",""),Array("d_err",Lang_Js[22]))){Check=false;}
			if(!KnifeCMS.CheckItem("passwordnew","d_passwordnew",Array("Normal",""),Array("d_err",Lang_Js[23]))){Check=false;}
			if(form.passwordnew.value.length<6){
				Check=false;
				KnifeCMS.$id("d_passwordnew").className="d_err";
				KnifeCMS.$id("d_passwordnew").innerHTML=Lang_Js[25];
			}
			if(form.passwordnew.value!=form.repasswordnew.value){
				Check=false;
				KnifeCMS.$id("d_repasswordnew").className="d_err";
				KnifeCMS.$id("d_repasswordnew").innerHTML=Lang_Js[24];
			}else{
				KnifeCMS.$id("d_repasswordnew").className="Normal";
				KnifeCMS.$id("d_repasswordnew").innerHTML="";
			}
			if(Check){
				var fn_DialogID = KnifeCMS.CreateID();
				var fn_Title    = Lang_Js[29];
				var fn_HTML     = "<div id=\"fixbox\" style=\"padding:10px 4px 5px 12px;\"></div>";
				var fn_Btn      = "<input type=\"button\" class=\"sysBtn ml5\" onclick=\"href('?myhome/password"+filesuffix+"')\" value="+Lang_Js_Btn_Confirm+">";
				KnifeCMS.CreateDiaWindow(320,100,fn_DialogID,fn_Title,fn_HTML,true,false);
				var CueMsgBox=KnifeCMS.$id("fixbox");
				KnifeCMS.DataSending(CueMsgBox);
				var ajaxString = "userid="+parseInt(userid)+"&passwordold="+escape(form.passwordold.value)+"&passwordnew="+escape(form.passwordnew.value)+"&repasswordnew="+escape(form.repasswordnew.value)
				var ajaxUrl    = "include/module/ajax/ajax.asp?ctl=changepassword&r="+parseInt(Math.random()*1000);
				var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
				//alert(ajaxSta);
				KnifeCMS.DiaWindowCloseByID(fn_DialogID);
				KnifeCMS.CreateDiaWindow(400,180,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,true);
				CueMsgBox=KnifeCMS.$id("fixbox");
				if(ajaxSta=="ok"){
					CueMsgBox.innerHTML=Lang_Js[26];
				}else if(ajaxSta=="unlogined"){
					CueMsgBox.innerHTML=Lang_Js[11];
					KnifeCMS.LoginDiaWindow();
				}else if(ajaxSta=="error_password"){
					CueMsgBox.innerHTML=Lang_Js[27];
				}else if(ajaxSta=="error_pwdift"){
					CueMsgBox.innerHTML=Lang_Js[24];
				}else{
					CueMsgBox.innerHTML=Lang_Js[28];
				}
			}
		}else{
			alert(Lang_Js[2]);
		}
	}
}
var MySet = new Class_MySet()