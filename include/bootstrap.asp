<!--#include file="../config/config.main.asp"-->
<!--#include file="kfc.main.asp"-->
<!--#include file="kfc.common.asp"-->
<!--#include file="../root.htaccess.asp"-->
<!--#include file="kfc.initialization.asp"-->
<%
If Sys_IsInstalled=0 Then
	Response.Write("网站还未安装，点击<a href='install'>开始安装</a>")
Else
	If SiteStatus=0 Then
		Response.Write(SiteClosedReasons)
	Else
		KnifeCMS.Tpl.ShowRunInfo = True
		Select Case Ctl
			Case "content"
				Call CreateHTML_ContentSystem(SubSystem,Category,ID,Page)
			Case "vote"
				Call CreateHTML_Vote(Category,ID,Page)
			Case "shop"
				Call CreateHTML_Shop(Category,ID,Page)
			Case "login"
				Call CreateHTML_Login(Action)
			Case "reg"
				Call CreateHTML_Reg(Action)
			Case "getpassword"
				Call CreateHTML_GetPassword()
			Case "myhome"
				Call CreateHTML_MyHome()
			Case "userhome"
				Call CreateHTML_UserHome()
			Case "mycart"
				Call CreateHTML_MyCart(Action)
			Case "myorder"
				Call CreateHTML_MyOrder()
			Case "order"
				Call CreateHTML_Order(Action)
			Case "search"
				Call CreateHTML_Search()
			Case "guestbook"
				Call CreateHTML_Guestbook()
			Case "error404"
				Call CreateHTML_Error404()
			Case Else
				Call CreateHTML_Index()
		End Select
		Echo HTMLString
	End If
End If
Set OS = Nothing
%>
