<%
'错误信息类
Class Class_KnifeCMS_Error
	Private objError
	Private StrMsg
	
	Private Sub Class_Initialize
		Set objError = Server.CreateObject("Scripting.Dictionary")
	End Sub
	
	Public Default Property Get Err_(ByVal Num)
		If objError.Exists(Num) Then
			Err_ = objError(Num)
		Else
			Err_ = "Unknown Error"
		End If
	End Property
	
	Public Property Let Err_(ByVal Num, ByVal Str)
		Num=Int(Num)
		If Num > 0 Then objError(Num) = Str
	End Property
	
	Public Property Get Msg
		Msg = StrMsg
	End Property
	Public Property Let Msg(ByVal Str)
		StrMsg = Str
	End Property
	
	Public Sub Raise(ByVal Num)
		If Int(Num)=0 Then Exit Sub
		Echo ShowError(Num,objError(Num))
		StrMsg = ""
	End Sub
	
	Private Function ShowError(ByVal Err_Num, ByVal Err_Msg)
		Dim ErrorHtml
		ErrorHtml="<table cellpadding=0 cellspacing=0><tbody><tr><td>"& vbCrLf
		ErrorHtml=ErrorHtml & "<div style=""font-size:12px;font-family:Verdana, Geneva, sans-serif;background:#E0E0E0;padding:8px 8px 8px 10px;margin:10px"">"& vbCrLf
		ErrorHtml=ErrorHtml & "  <p style=""padding:3px 0px 5px 0px; margin:0px;""><b>K"&"ni"&"f"&"e"&"C"&"M"&"S E"&"r"&"r"&"o"&"r</b></p>"& vbCrLf
		ErrorHtml=ErrorHtml & "  <p style=""padding:3px 0px; margin:0px;"">["&Err_Num&"]:"&Err_Msg&"</p>"& vbCrLf
		If CBool(IsDeBug) Then
			ErrorHtml = ErrorHtml & "<ul style=""margin:0px 0px 0px 20px;"">" & vbCrLf
			ErrorHtml = ErrorHtml & "<li>Error.Message: " & Msg & "</li>" & vbCrLf
			ErrorHtml = ErrorHtml & "<li>Error.Page: " & KnifeCMS.Data.URLDecode(KnifeCMS.GetUrl("")) & "</li>" & vbCrLf
			If Err.Number<>0 Then
			ErrorHtml = ErrorHtml & "<li>Error.Number: " & Err.Number & "</li>" & vbCrLf
			ErrorHtml = ErrorHtml & "<li>Error.Description: " & Err.Description & "</li>" & vbCrLf
			ErrorHtml = ErrorHtml & "<li>Error.Source: " & Err.Source & "</li>" & vbCrLf
			End If
			ErrorHtml = ErrorHtml & "</ul>" & vbCrLf
		End If
		ErrorHtml=ErrorHtml & "</div>"& vbCrLf
		ErrorHtml=ErrorHtml & "</td></tr></tbody></table>"& vbCrLf
		Echo ErrorHtml
		Call ProcessClose()'关闭程序进程
	End Function
	
	Private Sub Class_Terminate
		Set objError = Nothing
	End Sub
End Class
%>