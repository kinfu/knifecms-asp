<%
'Session处理类

Class Class_KnifeCMS_Session

	Private Pv_Encrypt     '是否对Session加密
	Private Pv_SessionPre  'Session 前缀
	Private Pv_Timeout     '超时时间,以分钟为单位
	
	Private Sub Class_Initialize()
		Pv_Encrypt = False 'Session默认不加密
	End Sub
	Private Sub Class_Terminate()
	End Sub

	Public Property Let Encrypt(ByVal BV_Bool)
		Pv_Encrypt = BV_Bool
	End Property
	Public Property Get Encrypt()
		Encrypt = Pv_Encrypt
	End Property
	
	Public Property Let SessionPre(ByVal BV_SessionPre)
		Pv_SessionPre = BV_SessionPre
	End Property
	Public Property Get SessionPre()
		SessionPre = Pv_SessionPre
	End Property
	
	Public Property Let [Timeout](ByVal BV_Timeout)
		Pv_Timeout = BV_Timeout
	End Property
	Public Property Get [Timeout]()
		[Timeout] = Pv_Timeout
	End Property
	
	Private Function SessionEncrypt(ByVal BV_String)
		SessionEncrypt = KnifeCMS.Base64.Encode(BV_String)
	End Function
	Private Function SessionDecrypt(ByVal BV_String)
		SessionDecrypt = KnifeCMS.Base64.Decode(BV_String)
	End Function

	Public Function GetSession(ByVal BV_Key)
		If Not(KnifeCMS.Data.IsNul(BV_Key)) Then
			Dim Fn_Key : Fn_Key = Pv_SessionPre & BV_Key
			If IsObject(Session(Fn_Key)) Then
				Set GetSession = Session(Fn_Key)
			Else
				If Pv_Encrypt Then BV_Value = SessionDecrypt(BV_Value)
				GetSession = Session(Fn_Key)
			End If
		End If
	End Function
	
	Public Function SetSession(ByVal BV_Key, ByVal BV_Value)
		Dim Fn_Key : Fn_Key = Pv_SessionPre & BV_Key
		If IsObject(BV_Value) Then
			Set Session(Fn_Key) = BV_Value
		Else
			If Pv_Encrypt Then BV_Value = SessionEncrypt(BV_Value)
			Session(Fn_Key) = BV_Value
		End If
		Session.Timeout = Pv_Timeout
	End Function
	
	Public Function RemoveSession(ByVal BV_Key)
		Dim Fn_Key : Fn_Key = Pv_SessionPre & BV_Key
		If IsObject(Session(Fn_Key)) Then
			Set Session(Fn_Key) = Nothing
		End If
        Session.Contents.Remove(Fn_Key)
	End Function
	
	Public Function RemoveAll()
        Session.Contents.RemoveAll()
	End Function
	
	Private Sub Clear()
		Session.Abandon()
	End Sub
	
	
End Class
%>