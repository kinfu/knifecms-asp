<%
'Feature     : 32 LM MD5 HASH
'Update Date : 2010-11-03 14:46:12
'Description : Encrypt strings with MD5 in KnifeCMS

Class Class_MD5
	Private m_lOnBits(30)
	Private m_l2Power(30)
	'------------------------------------
	Private BITS_TO_A_BYTE,BYTES_TO_A_WORD,BITS_TO_A_WORD
	Private Pv_Md5Bits
	
	Private Sub Class_Initialize()
		BITS_TO_A_BYTE  = 8
		BYTES_TO_A_WORD = 4
		BITS_TO_A_WORD  = 32
		Pv_Md5Bits      = 32
	End Sub
	
	'设置生成的MD5的位数
	Public Property Let MD5Bits(ByVal BV_Bits)
		Pv_Md5Bits = BV_Bits
	End Property
	Public Property Get MD5Bits()
		MD5Bits = Pv_Md5Bits
	End Property
	
	Private Function LShift(lValue, iShiftBits)
		If iShiftBits = 0 Then
			LShift = lValue
			Exit Function
		ElseIf iShiftBits = 31 Then
			If lValue And 1 Then
				LShift = &H80000000
			Else
				LShift = 0
			End If
			Exit Function
		ElseIf iShiftBits < 0 Or iShiftBits > 31 Then
			Err.Raise 6
		End If
	
		If (lValue And m_l2Power(31 - iShiftBits)) Then
			LShift = ((lValue And m_lOnBits(31 - (iShiftBits + 1))) * m_l2Power(iShiftBits)) Or &H80000000
		Else
			LShift = ((lValue And m_lOnBits(31 - iShiftBits)) * m_l2Power(iShiftBits))
		End If
	End Function
	
	Private Function RShift(lValue, iShiftBits)
		If iShiftBits = 0 Then
			RShift = lValue
			Exit Function
		ElseIf iShiftBits = 31 Then
			If lValue And &H80000000 Then
				RShift = 1
			Else
				RShift = 0
			End If
			Exit Function
		ElseIf iShiftBits < 0 Or iShiftBits > 31 Then
			Err.Raise 6
		End If
	
		RShift = (lValue And &H7FFFFFFE) \ m_l2Power(iShiftBits)
	
		If (lValue And &H80000000) Then
			RShift = (RShift Or (&H40000000 \ m_l2Power(iShiftBits - 1)))
		End If
	End Function
	
	Private Function RotateLeft(lValue, iShiftBits)
		RotateLeft = LShift(lValue, iShiftBits) Or RShift(lValue, (32 - iShiftBits))
	End Function
	
	Private Function AddUnsigned(lX, lY)
		Dim lX4
		Dim lY4
		Dim lX8
		Dim lY8
		Dim lResult
	
		lX8 = lX And &H80000000
		lY8 = lY And &H80000000
		lX4 = lX And &H40000000
		lY4 = lY And &H40000000
	
		lResult = (lX And &H3FFFFFFF) + (lY And &H3FFFFFFF)
	
		If lX4 And lY4 Then
			lResult = lResult Xor &H80000000 Xor lX8 Xor lY8
		ElseIf lX4 Or lY4 Then
			If lResult And &H40000000 Then
				lResult = lResult Xor &HC0000000 Xor lX8 Xor lY8
			Else
				lResult = lResult Xor &H40000000 Xor lX8 Xor lY8
			End If
		Else
			lResult = lResult Xor lX8 Xor lY8
		End If
	
		AddUnsigned = lResult
	End Function
	
	Private Function md5_F(x, y, z)
		md5_F = (x And y) Or ((Not x) And z)
	End Function
	
	Private Function md5_G(x, y, z)
		md5_G = (x And z) Or (y And (Not z))
	End Function
	
	Private Function md5_H(x, y, z)
		md5_H = (x Xor y Xor z)
	End Function
	
	Private Function md5_I(x, y, z)
		md5_I = (y Xor (x Or (Not z)))
	End Function
	
	Private Sub md5_FF(a, b, c, d, x, s, ac)
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(md5_F(b, c, d), x), ac))
		a = RotateLeft(a, s)
		a = AddUnsigned(a, b)
	End Sub
	
	Private Sub md5_GG(a, b, c, d, x, s, ac)
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(md5_G(b, c, d), x), ac))
		a = RotateLeft(a, s)
		a = AddUnsigned(a, b)
	End Sub
	
	Private Sub md5_HH(a, b, c, d, x, s, ac)
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(md5_H(b, c, d), x), ac))
		a = RotateLeft(a, s)
		a = AddUnsigned(a, b)
	End Sub
	
	Private Sub md5_II(a, b, c, d, x, s, ac)
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(md5_I(b, c, d), x), ac))
		a = RotateLeft(a, s)
		a = AddUnsigned(a, b)
	End Sub
	
	Private Function ConvertToWordArray(sMessage)
		Dim lMessageLength
		Dim lNumberOfWords
		Dim lWordArray()
		Dim lBytePosition
		Dim lByteCount
		Dim lWordCount
	
		Const MODULUS_BITS = 512
		Const CONGRUENT_BITS = 448
	
		lMessageLength = Len(sMessage)
	
		lNumberOfWords = (((lMessageLength + ((MODULUS_BITS - CONGRUENT_BITS) \ BITS_TO_A_BYTE)) \ (MODULUS_BITS \ BITS_TO_A_BYTE)) + 1) * (MODULUS_BITS \ BITS_TO_A_WORD)
		ReDim lWordArray(lNumberOfWords - 1)
	
		lBytePosition = 0
		lByteCount = 0
		Do Until lByteCount >= lMessageLength
			lWordCount = lByteCount \ BYTES_TO_A_WORD
			lBytePosition = (lByteCount Mod BYTES_TO_A_WORD) * BITS_TO_A_BYTE
			lWordArray(lWordCount) = lWordArray(lWordCount) Or LShift(Asc(Mid(sMessage, lByteCount + 1, 1)), lBytePosition)
			lByteCount = lByteCount + 1
		Loop
	
		lWordCount = lByteCount \ BYTES_TO_A_WORD
		lBytePosition = (lByteCount Mod BYTES_TO_A_WORD) * BITS_TO_A_BYTE
	
		lWordArray(lWordCount) = lWordArray(lWordCount) Or LShift(&H80, lBytePosition)
	
		lWordArray(lNumberOfWords - 2) = LShift(lMessageLength, 3)
		lWordArray(lNumberOfWords - 1) = RShift(lMessageLength, 29)
	
		ConvertToWordArray = lWordArray
	End Function
	
	Private Function WordToHex(lValue)
		Dim lByte
		Dim lCount
	
		For lCount = 0 To 3
			lByte = RShift(lValue, lCount * BITS_TO_A_BYTE) And m_lOnBits(BITS_TO_A_BYTE - 1)
			WordToHex = WordToHex & Right("0" & Hex(lByte), 2)
		Next
	End Function
	
	Public Function MD5(sMessage)
		m_lOnBits(0) = CLng(1)
		m_lOnBits(1) = CLng(3)
		m_lOnBits(2) = CLng(7)
		m_lOnBits(3) = CLng(15)
		m_lOnBits(4) = CLng(31)
		m_lOnBits(5) = CLng(63)
		m_lOnBits(6) = CLng(127)
		m_lOnBits(7) = CLng(255)
		m_lOnBits(8) = CLng(511)
		m_lOnBits(9) = CLng(1023)
		m_lOnBits(10) = CLng(2047)
		m_lOnBits(11) = CLng(4095)
		m_lOnBits(12) = CLng(8191)
		m_lOnBits(13) = CLng(16383)
		m_lOnBits(14) = CLng(32767)
		m_lOnBits(15) = CLng(65535)
		m_lOnBits(16) = CLng(131071)
		m_lOnBits(17) = CLng(262143)
		m_lOnBits(18) = CLng(524287)
		m_lOnBits(19) = CLng(1048575)
		m_lOnBits(20) = CLng(2097151)
		m_lOnBits(21) = CLng(4194303)
		m_lOnBits(22) = CLng(8388607)
		m_lOnBits(23) = CLng(16777215)
		m_lOnBits(24) = CLng(33554431)
		m_lOnBits(25) = CLng(67108863)
		m_lOnBits(26) = CLng(134217727)
		m_lOnBits(27) = CLng(268435455)
		m_lOnBits(28) = CLng(536870911)
		m_lOnBits(29) = CLng(1073741823)
		m_lOnBits(30) = CLng(2147483647)
	
		m_l2Power(0) = CLng(1)
		m_l2Power(1) = CLng(2)
		m_l2Power(2) = CLng(4)
		m_l2Power(3) = CLng(8)
		m_l2Power(4) = CLng(16)
		m_l2Power(5) = CLng(32)
		m_l2Power(6) = CLng(64)
		m_l2Power(7) = CLng(128)
		m_l2Power(8) = CLng(256)
		m_l2Power(9) = CLng(512)
		m_l2Power(10) = CLng(1024)
		m_l2Power(11) = CLng(2048)
		m_l2Power(12) = CLng(4096)
		m_l2Power(13) = CLng(8192)
		m_l2Power(14) = CLng(16384)
		m_l2Power(15) = CLng(32768)
		m_l2Power(16) = CLng(65536)
		m_l2Power(17) = CLng(131072)
		m_l2Power(18) = CLng(262144)
		m_l2Power(19) = CLng(524288)
		m_l2Power(20) = CLng(1048576)
		m_l2Power(21) = CLng(2097152)
		m_l2Power(22) = CLng(4194304)
		m_l2Power(23) = CLng(8388608)
		m_l2Power(24) = CLng(16777216)
		m_l2Power(25) = CLng(33554432)
		m_l2Power(26) = CLng(67108864)
		m_l2Power(27) = CLng(134217728)
		m_l2Power(28) = CLng(268435456)
		m_l2Power(29) = CLng(536870912)
		m_l2Power(30) = CLng(1073741824)
	
	
		Dim x
		Dim k
		Dim AA
		Dim BB
		Dim CC
		Dim DD
		Dim a
		Dim b
		Dim c
		Dim d
	
		Const S11 = 7
		Const S12 = 12
		Const S13 = 17
		Const S14 = 22
		Const S21 = 5
		Const S22 = 9
		Const S23 = 14
		Const S24 = 20
		Const S31 = 4
		Const S32 = 11
		Const S33 = 16
		Const S34 = 23
		Const S41 = 6
		Const S42 = 10
		Const S43 = 15
		Const S44 = 21
	
		x = ConvertToWordArray(sMessage)
	
		a = &H67452301
		b = &HEFCDAB89
		c = &H98BADCFE
		d = &H10325476
	
		For k = 0 To UBound(x) Step 16
			AA = a
			BB = b
			CC = c
			DD = d
	
			md5_FF a, b, c, d, x(k + 0), S11, &HD76AA478
			md5_FF d, a, b, c, x(k + 1), S12, &HE8C7B756
			md5_FF c, d, a, b, x(k + 2), S13, &H242070DB
			md5_FF b, c, d, a, x(k + 3), S14, &HC1BDCEEE
			md5_FF a, b, c, d, x(k + 4), S11, &HF57C0FAF
			md5_FF d, a, b, c, x(k + 5), S12, &H4787C62A
			md5_FF c, d, a, b, x(k + 6), S13, &HA830461A
			md5_FF b, c, d, a, x(k + 7), S14, &HFD469501
			md5_FF a, b, c, d, x(k + 8), S11, &H698098DB
			md5_FF d, a, b, c, x(k + 9), S12, &H8B44F7AF
			md5_FF c, d, a, b, x(k + 10), S13, &HFFFF5BB1
			md5_FF b, c, d, a, x(k + 11), S14, &H895CD7BE
			md5_FF a, b, c, d, x(k + 12), S11, &H6B90112E
			md5_FF d, a, b, c, x(k + 13), S12, &HFD987193
			md5_FF c, d, a, b, x(k + 14), S13, &HA6794383
			md5_FF b, c, d, a, x(k + 15), S14, &H49B40821
	
			md5_GG a, b, c, d, x(k + 1), S21, &HF61E2562
			md5_GG d, a, b, c, x(k + 6), S22, &HC040B340
			md5_GG c, d, a, b, x(k + 11), S23, &H265E5A51
			md5_GG b, c, d, a, x(k + 0), S24, &HE9B6C7AA
			md5_GG a, b, c, d, x(k + 5), S21, &HD62F105D
			md5_GG d, a, b, c, x(k + 10), S22, &H2441453
			md5_GG c, d, a, b, x(k + 15), S23, &HD8A1E681
			md5_GG b, c, d, a, x(k + 4), S24, &HE7D3FBC8
			md5_GG a, b, c, d, x(k + 9), S21, &H21E1CDE6
			md5_GG d, a, b, c, x(k + 14), S22, &HC33707D6
			md5_GG c, d, a, b, x(k + 3), S23, &HF4D50D87
			md5_GG b, c, d, a, x(k + 8), S24, &H455A14ED
			md5_GG a, b, c, d, x(k + 13), S21, &HA9E3E905
			md5_GG d, a, b, c, x(k + 2), S22, &HFCEFA3F8
			md5_GG c, d, a, b, x(k + 7), S23, &H676F02D9
			md5_GG b, c, d, a, x(k + 12), S24, &H8D2A4C8A
	
			md5_HH a, b, c, d, x(k + 5), S31, &HFFFA3942
			md5_HH d, a, b, c, x(k + 8), S32, &H8771F681
			md5_HH c, d, a, b, x(k + 11), S33, &H6D9D6122
			md5_HH b, c, d, a, x(k + 14), S34, &HFDE5380C
			md5_HH a, b, c, d, x(k + 1), S31, &HA4BEEA44
			md5_HH d, a, b, c, x(k + 4), S32, &H4BDECFA9
			md5_HH c, d, a, b, x(k + 7), S33, &HF6BB4B60
			md5_HH b, c, d, a, x(k + 10), S34, &HBEBFBC70
			md5_HH a, b, c, d, x(k + 13), S31, &H289B7EC6
			md5_HH d, a, b, c, x(k + 0), S32, &HEAA127FA
			md5_HH c, d, a, b, x(k + 3), S33, &HD4EF3085
			md5_HH b, c, d, a, x(k + 6), S34, &H4881D05
			md5_HH a, b, c, d, x(k + 9), S31, &HD9D4D039
			md5_HH d, a, b, c, x(k + 12), S32, &HE6DB99E5
			md5_HH c, d, a, b, x(k + 15), S33, &H1FA27CF8
			md5_HH b, c, d, a, x(k + 2), S34, &HC4AC5665
	
			md5_II a, b, c, d, x(k + 0), S41, &HF4292244
			md5_II d, a, b, c, x(k + 7), S42, &H432AFF97
			md5_II c, d, a, b, x(k + 14), S43, &HAB9423A7
			md5_II b, c, d, a, x(k + 5), S44, &HFC93A039
			md5_II a, b, c, d, x(k + 12), S41, &H655B59C3
			md5_II d, a, b, c, x(k + 3), S42, &H8F0CCC92
			md5_II c, d, a, b, x(k + 10), S43, &HFFEFF47D
			md5_II b, c, d, a, x(k + 1), S44, &H85845DD1
			md5_II a, b, c, d, x(k + 8), S41, &H6FA87E4F
			md5_II d, a, b, c, x(k + 15), S42, &HFE2CE6E0
			md5_II c, d, a, b, x(k + 6), S43, &HA3014314
			md5_II b, c, d, a, x(k + 13), S44, &H4E0811A1
			md5_II a, b, c, d, x(k + 4), S41, &HF7537E82
			md5_II d, a, b, c, x(k + 11), S42, &HBD3AF235
			md5_II c, d, a, b, x(k + 2), S43, &H2AD7D2BB
			md5_II b, c, d, a, x(k + 9), S44, &HEB86D391
	
			a = AddUnsigned(a, AA)
			b = AddUnsigned(b, BB)
			c = AddUnsigned(c, CC)
			d = AddUnsigned(d, DD)
		Next
	
		If Pv_Md5Bits=32 then
			MD5 = LCase(WordToHex(a) & WordToHex(b) & WordToHex(c) & WordToHex(d))
		Else
		    MD5 = LCase(WordToHex(b) & WordToHex(c))  'I crop this to fit 16byte database password :D
		End If
	End Function
	
	Public Function Encrypt(ByVal BV_String)
		BV_String = Trim(BV_String)
		If BV_String = "" Then Encrypt = "" : Exit Function
		Encrypt = MD5(BV_String)
	End Function
	
End Class


'=========================================================
'Feature     : AES Encryption
'Author      : Roderick Divilbiss
'Update      : linx(knifecms@qq.com)
'Update Date : 2010-11-03 12:13:26
'Description : Encrypt strings with AES in KnifeCMS
'=========================================================
Class Class_AES
	Private m_lOnBits(30)
	Private m_l2Power(30)
	Private m_bytOnBits(7)
	Private m_byt2Power(7)

	Private m_InCo(3)

	Private m_fbsub(255)
	Private m_rbsub(255)
	Private m_ptab(255)
	Private m_ltab(255)
	Private m_ftable(255)
	Private m_rtable(255)
	Private m_rco(29)

	Private m_Nk
	Private m_Nb
	Private m_Nr
	Private m_fi(23)
	Private m_ri(23)
	Private m_fkey(119)
	Private m_rkey(119)

	Private Pv_EncryptKey

	Private Sub Class_Initialize()
		m_InCo(0) = &HB
		m_InCo(1) = &HD
		m_InCo(2) = &H9
		m_InCo(3) = &HE
			
		m_bytOnBits(0) = 1
		m_bytOnBits(1) = 3
		m_bytOnBits(2) = 7
		m_bytOnBits(3) = 15
		m_bytOnBits(4) = 31
		m_bytOnBits(5) = 63
		m_bytOnBits(6) = 127
		m_bytOnBits(7) = 255
			
		m_byt2Power(0) = 1
		m_byt2Power(1) = 2
		m_byt2Power(2) = 4
		m_byt2Power(3) = 8
		m_byt2Power(4) = 16
		m_byt2Power(5) = 32
		m_byt2Power(6) = 64
		m_byt2Power(7) = 128
			
		m_lOnBits(0) = 1
		m_lOnBits(1) = 3
		m_lOnBits(2) = 7
		m_lOnBits(3) = 15
		m_lOnBits(4) = 31
		m_lOnBits(5) = 63
		m_lOnBits(6) = 127
		m_lOnBits(7) = 255
		m_lOnBits(8) = 511
		m_lOnBits(9) = 1023
		m_lOnBits(10) = 2047
		m_lOnBits(11) = 4095
		m_lOnBits(12) = 8191
		m_lOnBits(13) = 16383
		m_lOnBits(14) = 32767
		m_lOnBits(15) = 65535
		m_lOnBits(16) = 131071
		m_lOnBits(17) = 262143
		m_lOnBits(18) = 524287
		m_lOnBits(19) = 1048575
		m_lOnBits(20) = 2097151
		m_lOnBits(21) = 4194303
		m_lOnBits(22) = 8388607
		m_lOnBits(23) = 16777215
		m_lOnBits(24) = 33554431
		m_lOnBits(25) = 67108863
		m_lOnBits(26) = 134217727
		m_lOnBits(27) = 268435455
		m_lOnBits(28) = 536870911
		m_lOnBits(29) = 1073741823
		m_lOnBits(30) = 2147483647
			
		m_l2Power(0) = 1
		m_l2Power(1) = 2
		m_l2Power(2) = 4
		m_l2Power(3) = 8
		m_l2Power(4) = 16
		m_l2Power(5) = 32
		m_l2Power(6) = 64
		m_l2Power(7) = 128
		m_l2Power(8) = 256
		m_l2Power(9) = 512
		m_l2Power(10) = 1024
		m_l2Power(11) = 2048
		m_l2Power(12) = 4096
		m_l2Power(13) = 8192
		m_l2Power(14) = 16384
		m_l2Power(15) = 32768
		m_l2Power(16) = 65536
		m_l2Power(17) = 131072
		m_l2Power(18) = 262144
		m_l2Power(19) = 524288
		m_l2Power(20) = 1048576
		m_l2Power(21) = 2097152
		m_l2Power(22) = 4194304
		m_l2Power(23) = 8388608
		m_l2Power(24) = 16777216
		m_l2Power(25) = 33554432
		m_l2Power(26) = 67108864
		m_l2Power(27) = 134217728
		m_l2Power(28) = 268435456
		m_l2Power(29) = 536870912
		m_l2Power(30) = 1073741824

		Pv_EncryptKey = "OPENWBS_AES"
	End Sub
	Private Sub Class_Terminate()
	End Sub

	Public Property Let EncryptKey(ByVal BV_EK)
		Pv_EncryptKey = BV_EK
	End Property
	Public Property Get EncryptKey()
		EncryptKey = Pv_EncryptKey
	End Property

	Private Function LShift(lValue, iShiftBits)
		If iShiftBits = 0 Then
			LShift = lValue
			Exit Function
		ElseIf iShiftBits = 31 Then
			If lValue And 1 Then
				LShift = &H80000000
			Else
				LShift = 0
			End If
			Exit Function
		ElseIf iShiftBits < 0 Or iShiftBits > 31 Then
			Err.Raise 6
		End If
		
		If (lValue And m_l2Power(31 - iShiftBits)) Then
			LShift = ((lValue And m_lOnBits(31 - (iShiftBits + 1))) * m_l2Power(iShiftBits)) Or &H80000000
		Else
			LShift = ((lValue And m_lOnBits(31 - iShiftBits)) * m_l2Power(iShiftBits))
		End If
	End Function

	Private Function RShift(lValue, iShiftBits)
		If iShiftBits = 0 Then
			RShift = lValue
			Exit Function
		ElseIf iShiftBits = 31 Then
			If lValue And &H80000000 Then
				RShift = 1
			Else
				RShift = 0
			End If
			Exit Function
		ElseIf iShiftBits < 0 Or iShiftBits > 31 Then
			Err.Raise 6
		End If
		
		RShift = (lValue And &H7FFFFFFE) \ m_l2Power(iShiftBits)
		
		If (lValue And &H80000000) Then
			RShift = (RShift Or (&H40000000 \ m_l2Power(iShiftBits - 1)))
		End If
	End Function

	Private Function LShiftByte(bytValue, bytShiftBits)
		If bytShiftBits = 0 Then
			LShiftByte = bytValue
			Exit Function
		ElseIf bytShiftBits = 7 Then
			If bytValue And 1 Then
				LShiftByte = &H80
			Else
				LShiftByte = 0
			End If
			Exit Function
		ElseIf bytShiftBits < 0 Or bytShiftBits > 7 Then
			Err.Raise 6
		End If
		
		LShiftByte = ((bytValue And m_bytOnBits(7 - bytShiftBits)) * m_byt2Power(bytShiftBits))
	End Function

	Private Function RShiftByte(bytValue, bytShiftBits)
		If bytShiftBits = 0 Then
			RShiftByte = bytValue
			Exit Function
		ElseIf bytShiftBits = 7 Then
			If bytValue And &H80 Then
				RShiftByte = 1
			Else
				RShiftByte = 0
			End If
			Exit Function
		ElseIf bytShiftBits < 0 Or bytShiftBits > 7 Then
			Err.Raise 6
		End If
		
		RShiftByte = bytValue \ m_byt2Power(bytShiftBits)
	End Function

	Private Function RotateLeft(lValue, iShiftBits)
		RotateLeft = LShift(lValue, iShiftBits) Or RShift(lValue, (32 - iShiftBits))
	End Function

	Private Function RotateLeftByte(bytValue, bytShiftBits)
		RotateLeftByte = LShiftByte(bytValue, bytShiftBits) Or RShiftByte(bytValue, (8 - bytShiftBits))
	End Function

	Private Function Pack(b())
		Dim lCount
		Dim lTemp
		
		For lCount = 0 To 3
			lTemp = b(lCount)
			Pack = Pack Or LShift(lTemp, (lCount * 8))
		Next
	End Function

	Private Function PackFrom(b(), k)
		Dim lCount
		Dim lTemp
		
		For lCount = 0 To 3
			lTemp = b(lCount + k)
			PackFrom = PackFrom Or LShift(lTemp, (lCount * 8))
		Next
	End Function

	Private Sub Unpack(a, b())
		b(0) = a And m_lOnBits(7)
		b(1) = RShift(a, 8) And m_lOnBits(7)
		b(2) = RShift(a, 16) And m_lOnBits(7)
		b(3) = RShift(a, 24) And m_lOnBits(7)
	End Sub

	Private Sub UnpackFrom(a, b(), k)
		b(0 + k) = a And m_lOnBits(7)
		b(1 + k) = RShift(a, 8) And m_lOnBits(7)
		b(2 + k) = RShift(a, 16) And m_lOnBits(7)
		b(3 + k) = RShift(a, 24) And m_lOnBits(7)
	End Sub

	Private Function xtime(a)
		Dim b
		
		If (a And &H80) Then
			b = &H1B
		Else
			b = 0
		End If
		
		xtime = LShiftByte(a, 1)
		xtime = xtime Xor b
	End Function

	Private Function bmul(x, y)
		If x <> 0 And y <> 0 Then
			bmul = m_ptab((CLng(m_ltab(x)) + CLng(m_ltab(y))) Mod 255)
		Else
			bmul = 0
		End If
	End Function

	Private Function SubByte(a)
		Dim b(3)
		
		Unpack a, b
		b(0) = m_fbsub(b(0))
		b(1) = m_fbsub(b(1))
		b(2) = m_fbsub(b(2))
		b(3) = m_fbsub(b(3))
		
		SubByte = Pack(b)
	End Function

	Private Function product(x, y)
		Dim xb(3)
		Dim yb(3)
		
		Unpack x, xb
		Unpack y, yb
		product = bmul(xb(0), yb(0)) Xor bmul(xb(1), yb(1)) Xor bmul(xb(2), yb(2)) Xor bmul(xb(3), yb(3))
	End Function

	Private Function InvMixCol(x)
		Dim y
		Dim m
		Dim b(3)
		
		m = Pack(m_InCo)
		b(3) = product(m, x)
		m = RotateLeft(m, 24)
		b(2) = product(m, x)
		m = RotateLeft(m, 24)
		b(1) = product(m, x)
		m = RotateLeft(m, 24)
		b(0) = product(m, x)
		y = Pack(b)
		
		InvMixCol = y
	End Function

	Private Function ByteSub(x)
		Dim y
		Dim z
		
		z = x
		y = m_ptab(255 - m_ltab(z))
		z = y
		z = RotateLeftByte(z, 1)
		y = y Xor z
		z = RotateLeftByte(z, 1)
		y = y Xor z
		z = RotateLeftByte(z, 1)
		y = y Xor z
		z = RotateLeftByte(z, 1)
		y = y Xor z
		y = y Xor &H63
		
		ByteSub = y
	End Function

	Public Sub gentables()
		Dim i
		Dim y
		Dim b(3)
		Dim ib
		
		m_ltab(0) = 0
		m_ptab(0) = 1
		m_ltab(1) = 0
		m_ptab(1) = 3
		m_ltab(3) = 1
		
		For i = 2 To 255
			m_ptab(i) = m_ptab(i - 1) Xor xtime(m_ptab(i - 1))
			m_ltab(m_ptab(i)) = i
		Next
		
		m_fbsub(0) = &H63
		m_rbsub(&H63) = 0
		
		For i = 1 To 255
			ib = i
			y = ByteSub(ib)
			m_fbsub(i) = y
			m_rbsub(y) = i
		Next
		
		y = 1
		For i = 0 To 29
			m_rco(i) = y
			y = xtime(y)
		Next
		
		For i = 0 To 255
			y = m_fbsub(i)
			b(3) = y Xor xtime(y)
			b(2) = y
			b(1) = y
			b(0) = xtime(y)
			m_ftable(i) = Pack(b)
			
			y = m_rbsub(i)
			b(3) = bmul(m_InCo(0), y)
			b(2) = bmul(m_InCo(1), y)
			b(1) = bmul(m_InCo(2), y)
			b(0) = bmul(m_InCo(3), y)
			m_rtable(i) = Pack(b)
		Next
	End Sub

	Public Sub gkey(nb, nk, key())                
		Dim i
		Dim j
		Dim k
		Dim m
		Dim N
		Dim C1
		Dim C2
		Dim C3
		Dim CipherKey(7)
		
		m_Nb = nb
		m_Nk = nk
		
		If m_Nb >= m_Nk Then
			m_Nr = 6 + m_Nb
		Else
			m_Nr = 6 + m_Nk
		End If
		
		C1 = 1
		If m_Nb < 8 Then
			C2 = 2
			C3 = 3
		Else
			C2 = 3
			C3 = 4
		End If
		
		For j = 0 To nb - 1
			m = j * 3
			
			m_fi(m) = (j + C1) Mod nb
			m_fi(m + 1) = (j + C2) Mod nb
			m_fi(m + 2) = (j + C3) Mod nb
			m_ri(m) = (nb + j - C1) Mod nb
			m_ri(m + 1) = (nb + j - C2) Mod nb
			m_ri(m + 2) = (nb + j - C3) Mod nb
		Next
		
		N = m_Nb * (m_Nr + 1)
		
		For i = 0 To m_Nk - 1
			j = i * 4
			CipherKey(i) = PackFrom(key, j)
		Next
		
		For i = 0 To m_Nk - 1
			m_fkey(i) = CipherKey(i)
		Next
		
		j = m_Nk
		k = 0
		Do While j < N
			m_fkey(j) = m_fkey(j - m_Nk) Xor _
				SubByte(RotateLeft(m_fkey(j - 1), 24)) Xor m_rco(k)
			If m_Nk <= 6 Then
				i = 1
				Do While i < m_Nk And (i + j) < N
					m_fkey(i + j) = m_fkey(i + j - m_Nk) Xor _
						m_fkey(i + j - 1)
					i = i + 1
				Loop
			Else
				i = 1
				Do While i < 4 And (i + j) < N
					m_fkey(i + j) = m_fkey(i + j - m_Nk) Xor _
						m_fkey(i + j - 1)
					i = i + 1
				Loop
				If j + 4 < N Then
					m_fkey(j + 4) = m_fkey(j + 4 - m_Nk) Xor _
						SubByte(m_fkey(j + 3))
				End If
				i = 5
				Do While i < m_Nk And (i + j) < N
					m_fkey(i + j) = m_fkey(i + j - m_Nk) Xor _
						m_fkey(i + j - 1)
					i = i + 1
				Loop
			End If
			
			j = j + m_Nk
			k = k + 1
		Loop
		
		For j = 0 To m_Nb - 1
			m_rkey(j + N - nb) = m_fkey(j)
		Next
		
		i = m_Nb
		Do While i < N - m_Nb
			k = N - m_Nb - i
			For j = 0 To m_Nb - 1
				m_rkey(k + j) = InvMixCol(m_fkey(i + j))
			Next
			i = i + m_Nb
		Loop
		
		j = N - m_Nb
		Do While j < N
			m_rkey(j - N + m_Nb) = m_fkey(j)
			j = j + 1
		Loop
	End Sub

	Public Sub Encrypting(buff())
		Dim i
		Dim j
		Dim k
		Dim m
		Dim a(7)
		Dim b(7)
		Dim x
		Dim y
		Dim t
		
		For i = 0 To m_Nb - 1
			j = i * 4
			
			a(i) = PackFrom(buff, j)
			a(i) = a(i) Xor m_fkey(i)
		Next
		
		k = m_Nb
		x = a
		y = b
		
		For i = 1 To m_Nr - 1
			For j = 0 To m_Nb - 1
				m = j * 3
				y(j) = m_fkey(k) Xor m_ftable(x(j) And m_lOnBits(7)) Xor _
					RotateLeft(m_ftable(RShift(x(m_fi(m)), 8) And m_lOnBits(7)), 8) Xor _
					RotateLeft(m_ftable(RShift(x(m_fi(m + 1)), 16) And m_lOnBits(7)), 16) Xor _
					RotateLeft(m_ftable(RShift(x(m_fi(m + 2)), 24) And m_lOnBits(7)), 24)
				k = k + 1
			Next
			t = x
			x = y
			y = t
		Next
		
		For j = 0 To m_Nb - 1
			m = j * 3
			y(j) = m_fkey(k) Xor m_fbsub(x(j) And m_lOnBits(7)) Xor _
				RotateLeft(m_fbsub(RShift(x(m_fi(m)), 8) And m_lOnBits(7)), 8) Xor _
				RotateLeft(m_fbsub(RShift(x(m_fi(m + 1)), 16) And m_lOnBits(7)), 16) Xor _
				RotateLeft(m_fbsub(RShift(x(m_fi(m + 2)), 24) And m_lOnBits(7)), 24)
			k = k + 1
		Next
		
		For i = 0 To m_Nb - 1
			j = i * 4
			UnpackFrom y(i), buff, j
			x(i) = 0
			y(i) = 0
		Next
	End Sub

	Public Sub Decrypting(buff())
		Dim i
		Dim j
		Dim k
		Dim m
		Dim a(7)
		Dim b(7)
		Dim x
		Dim y
		Dim t
		
		For i = 0 To m_Nb - 1
			j = i * 4
			a(i) = PackFrom(buff, j)
			a(i) = a(i) Xor m_rkey(i)
		Next
		
		k = m_Nb
		x = a
		y = b
		
		For i = 1 To m_Nr - 1
			For j = 0 To m_Nb - 1
				m = j * 3
				y(j) = m_rkey(k) Xor m_rtable(x(j) And m_lOnBits(7)) Xor _
					RotateLeft(m_rtable(RShift(x(m_ri(m)), 8) And m_lOnBits(7)), 8) Xor _
					RotateLeft(m_rtable(RShift(x(m_ri(m + 1)), 16) And m_lOnBits(7)), 16) Xor _
					RotateLeft(m_rtable(RShift(x(m_ri(m + 2)), 24) And m_lOnBits(7)), 24)
				k = k + 1
			Next
			t = x
			x = y
			y = t
		Next
		
		For j = 0 To m_Nb - 1
			m = j * 3
			
			y(j) = m_rkey(k) Xor m_rbsub(x(j) And m_lOnBits(7)) Xor _
				RotateLeft(m_rbsub(RShift(x(m_ri(m)), 8) And m_lOnBits(7)), 8) Xor _
				RotateLeft(m_rbsub(RShift(x(m_ri(m + 1)), 16) And m_lOnBits(7)), 16) Xor _
				RotateLeft(m_rbsub(RShift(x(m_ri(m + 2)), 24) And m_lOnBits(7)), 24)
			k = k + 1
		Next
		
		For i = 0 To m_Nb - 1
			j = i * 4
			
			UnpackFrom y(i), buff, j
			x(i) = 0
			y(i) = 0
		Next
	End Sub

	Private Function IsInitialized(vArray)
		On Error Resume Next
		IsInitialized = IsNumeric(UBound(vArray))
	End Function

	Private Sub CopyBytesASP(bytDest, lDestStart, bytSource(), lSourceStart, lLength)
		Dim lCount
		lCount = 0
		Do
			bytDest(lDestStart + lCount) = bytSource(lSourceStart + lCount)
			lCount = lCount + 1
		Loop Until lCount = lLength
	End Sub

	Public Function EncryptData(bytMessage, bytPassword)
		Dim bytKey(31)
		Dim bytIn()
		Dim bytOut()
		Dim bytTemp(31)
		Dim lCount
		Dim lLength
		Dim lEncodedLength
		Dim bytLen(3)
		Dim lPosition
		
		If Not IsInitialized(bytMessage) Then
			Exit Function
		End If
		If Not IsInitialized(bytPassword) Then
			Exit Function
		End If
		
		For lCount = 0 To UBound(bytPassword)
			bytKey(lCount) = bytPassword(lCount)
			If lCount = 31 Then
				Exit For
			End If
		Next
		
		gentables
		gkey 8, 8, bytKey
		
		lLength = UBound(bytMessage) + 1
		lEncodedLength = lLength + 4
		
		If lEncodedLength Mod 32 <> 0 Then
			lEncodedLength = lEncodedLength + 32 - (lEncodedLength Mod 32)
		End If
		ReDim bytIn(lEncodedLength - 1)
		ReDim bytOut(lEncodedLength - 1)
		
		Unpack lLength, bytIn
		CopyBytesASP bytIn, 4, bytMessage, 0, lLength

		For lCount = 0 To lEncodedLength - 1 Step 32
			CopyBytesASP bytTemp, 0, bytIn, lCount, 32
			Encrypting bytTemp
			CopyBytesASP bytOut, lCount, bytTemp, 0, 32
		Next
		
		EncryptData = bytOut
	End Function

	Public Function DecryptData(bytIn, bytPassword)
		Dim bytMessage()
		Dim bytKey(31)
		Dim bytOut()
		Dim bytTemp(31)
		Dim lCount
		Dim lLength
		Dim lEncodedLength
		Dim bytLen(3)
		Dim lPosition
		
		If Not IsInitialized(bytIn) Then
			Exit Function
		End If
		If Not IsInitialized(bytPassword) Then
			Exit Function
		End If
		
		lEncodedLength = UBound(bytIn) + 1
		
		If lEncodedLength Mod 32 <> 0 Then
			Exit Function
		End If
		
		For lCount = 0 To UBound(bytPassword)
			bytKey(lCount) = bytPassword(lCount)
			If lCount = 31 Then
				Exit For
			End If
		Next
		
		gentables
		gkey 8, 8, bytKey

		ReDim bytOut(lEncodedLength - 1)
		
		For lCount = 0 To lEncodedLength - 1 Step 32
			CopyBytesASP bytTemp, 0, bytIn, lCount, 32
			Decrypting bytTemp
			CopyBytesASP bytOut, lCount, bytTemp, 0, 32
		Next

		lLength = Pack(bytOut)
		
		If lLength > lEncodedLength - 4 Then
			Exit Function
		End If
		
		ReDim bytMessage(lLength - 1)
		CopyBytesASP bytMessage, 0, bytOut, 4, lLength
		
		DecryptData = bytMessage
	End Function

	Function AESEncrypt(sPlain,sPassword)
		Dim bytIn()
		Dim bytOut
		Dim bytPassword()
		Dim lCount
		Dim lLength
		Dim sTemp
		
		lLength = Len(sPlain)
		ReDim bytIn(lLength-1)
		For lCount = 1 To lLength
			bytIn(lCount-1) = CByte(AscB(Mid(sPlain,lCount,1)))
		Next
		lLength = Len(sPassword)
		ReDim bytPassword(lLength-1)
		For lCount = 1 To lLength
			bytPassword(lCount-1) = CByte(AscB(Mid(sPassword,lCount,1)))
		Next

		bytOut = EncryptData(bytIn, bytPassword)

		sTemp = ""
		For lCount = 0 To UBound(bytOut)
			sTemp = sTemp & Right("0" & Hex(bytOut(lCount)), 2)
		Next

		AESEncrypt = sTemp
	End Function

	Function AESDecrypt(sCypher, sPassword)
		Dim bytIn()
		Dim bytOut
		Dim bytPassword()
		Dim lCount
		Dim lLength
		Dim sTemp
		On Error Resume Next
		lLength = Len(sCypher)
		ReDim bytIn(lLength/2-1)
		For lCount = 0 To lLength/2-1
			bytIn(lCount) = CByte("&H" & Mid(sCypher,lCount*2+1,2))
		Next
		lLength = Len(sPassword)
		ReDim bytPassword(lLength-1)
		For lCount = 1 To lLength
			bytPassword(lCount-1) = CByte(AscB(Mid(sPassword,lCount,1)))
		Next
		bytOut = DecryptData(bytIn, bytPassword)
		If IsArray(bytOut) Then
			lLength = UBound(bytOut) + 1
			sTemp = ""
			For lCount = 0 To lLength - 1
				sTemp = sTemp & Chr(bytOut(lCount))
			Next
		End If
		If Err.number <> 0 Then Err.Clear
		AESDecrypt = sTemp
	End Function
	
	Public Function Encrypt(ByVal BV_String)
		BV_String = Trim(BV_String)
		If BV_String = "" Then Encrypt = "" : Exit Function
		Encrypt = AESEncrypt(KnifeCMS.Data.Escape(BV_String),KnifeCMS.Data.Escape(Pv_EncryptKey))
	End Function
	
	Public Function Decrypt(ByVal BV_String)
		BV_String = Trim(BV_String)
		If BV_String = "" Then Decrypt = "" : Exit Function
		Decrypt = KnifeCMS.Data.UnEscape(AESDecrypt(BV_String,KnifeCMS.Data.Escape(Pv_EncryptKey)))
	End Function
	
End Class

' Functions to provide encoding/decoding of strings with Base64.
' 
' Encoding: myEncodedString = base64_encode( inputString )
' Decoding: myDecodedString = base64_decode( encodedInputString )
'
' Programmed by Markus Hartsmar for ShameDesigns in 2002. 
' Email me at: mark@shamedesigns.com
' Visit our website at: http://www.shamedesigns.com/
'
'=========================================================
'Feature     : Encoding/Decoding of strings with Base64
'Author      : Markus Hartsmar(mark@shamedesigns.com)
'Update      : linx(knifecms@qq.com)
'Update Date : 2010-11-04 10:42:27
'Description : Encoding strings with Base64 in KnifeCMS
'=========================================================
Class Class_Base64

	Private Base64Chars

	Public Sub Class_Initialize		
		'Base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
		Base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	End Sub

	' Functions for encoding string to Base64
	Public Function Encode( byVal strIn )
		Dim c1, c2, c3, w1, w2, w3, w4, n, strOut
		For n = 1 To Len( strIn ) Step 3
			c1 = Asc( Mid( strIn, n, 1 ) )
			c2 = Asc( Mid( strIn, n + 1, 1 ) + Chr(0) )
			c3 = Asc( Mid( strIn, n + 2, 1 ) + Chr(0) )
			w1 = Int( c1 / 4 ) : w2 = ( c1 And 3 ) * 16 + Int( c2 / 16 )
			If Len( strIn ) >= n + 1 Then 
				w3 = ( c2 And 15 ) * 4 + Int( c3 / 64 ) 
			Else 
				w3 = -1
			End If
			If Len( strIn ) >= n + 2 Then 
				w4 = c3 And 63 
			Else 
				w4 = -1
			End If
			strOut = strOut + mimeencode( w1 ) + mimeencode( w2 ) + _
					  mimeencode( w3 ) + mimeencode( w4 )
		Next
		Encode = strOut
	End Function

	Private Function mimeencode( byVal intIn )
		If intIn >= 0 Then 
			mimeencode = Mid( Base64Chars, intIn + 1, 1 ) 
		Else 
			mimeencode = ""
		End If
	End Function

	' Function to decode string from Base64
	Public Function Decode( byVal strIn )
		Dim w1, w2, w3, w4, n, strOut
		For n = 1 To Len( strIn ) Step 4
			w1 = mimedecode( Mid( strIn, n, 1 ) )
			w2 = mimedecode( Mid( strIn, n + 1, 1 ) )
			w3 = mimedecode( Mid( strIn, n + 2, 1 ) )
			w4 = mimedecode( Mid( strIn, n + 3, 1 ) )
			If w2 >= 0 Then _
				strOut = strOut + _
					Chr( ( ( w1 * 4 + Int( w2 / 16 ) ) And 255 ) )
			If w3 >= 0 Then _
				strOut = strOut + _
					Chr( ( ( w2 * 16 + Int( w3 / 4 ) ) And 255 ) )
			If w4 >= 0 Then _
				strOut = strOut + _
					Chr( ( ( w3 * 64 + w4 ) And 255 ) )
		Next
		Decode = strOut
	End Function

	Private Function mimedecode( byVal strIn )
		If Len( strIn ) = 0 Then 
			mimedecode = -1 : Exit Function
		Else
			mimedecode = InStr( Base64Chars, strIn ) - 1
		End If
	End Function
End Class
%>