<%
'模板处理类

Dim HTMLString
Dim TemplateFilePath,RedirectURL,IsStaticRunMode
Dim Page,IndexTemplate,CategoryTemplate,ContentTemplate,CacheName,StaticFileName
Dim IsContentExist,IsArticleDetail,IsVoteDetail,IsGoodsDetail,UserCenter,IsUserInfoDetail,IsMyAddrs,IsUserHome,IsUserExist,IsShopCart
Dim MySystemNotifyNum,MyFriendRequestNum,MyCartShopNum
Dim PlaceOrderResult

Function ParseHTML_Init()
	TempCtl          = Ctl
	TemplateFilePath = TemplatePath & TemplateHTMLFolder &"/"
	TagLine          = Lang_CurrentLocation &"<a href="""& SiteURL & SystemPath &""">"& Lang_Client_Home &"</a>"
	RedirectURL      = ""
	If RunMode="static" Then IsStaticRunMode=True Else IsStaticRunMode=False
End Function

Function ParseHTML_Terminate()
	Ctl = TempCtl
End Function

Function ParseHTML(ByVal BV_TemplateFilePath)
	If RedirectURL<>"" Then Call PageRedirect(RedirectURL) : Exit Function
	'静态模式
	If RunMode="static" And StaticFileName<>"" Then Call PageRedirect(StaticFileName) : Exit Function
	
	'获取用户动态通知信息
	Call GetUserNotifyNum()
	
	'动态模式
	If IsCache=1 And CacheName<>"" then
	'开启缓存的情况下
		If KnifeCMS.Cache.CheckCache(CacheName) then 
			HTMLString = KnifeCMS.Cache.GetCache(CacheName) 
			HTMLString = KnifeCMS.Data.RegReplace(HTMLString,"{kfc:sqlquerynum}",KnifeCMS.DB.SqlQueryNum)
			HTMLString = KnifeCMS.Data.RegReplace(HTMLString,"{kfc:runtime}",KnifeCMS.RunTime)
		Else
			Call ParseTplHTML(BV_TemplateFilePath)
			Call KnifeCMS.Cache.SetCache(CacheName,KnifeCMS.Tpl.Content)
			If KnifeCMS.Tpl.ShowRunInfo=True Then Call KnifeCMS.Tpl.ParseRunInfo()
			HTMLString = KnifeCMS.Tpl.Content
		End if
	Else
	'不开启缓存的情况下
		Call ParseTplHTML(BV_TemplateFilePath)
		If KnifeCMS.Tpl.ShowRunInfo=True Then Call KnifeCMS.Tpl.ParseRunInfo()
		HTMLString = KnifeCMS.Tpl.Content
	End if
	Call ParseHTML_Terminate()
End Function

'解析模版标签
Function ParseTplHTML(ByVal BV_TemplateFilePath)
	KnifeCMS.Tpl.Content = KnifeCMS.Read(BV_TemplateFilePath)
	KnifeCMS.Tpl.ParseTopAndFoot()
	KnifeCMS.Tpl.ParseGlobal()
	If UserCenter Then
		KnifeCMS.Tpl.ParseUserCenter()
		KnifeCMS.Tpl.ParseUserHome()
	Else
		KnifeCMS.Tpl.ParseContentAndShop()
	End If
	If IsShopCart Then KnifeCMS.Tpl.ParseShopCart()
	KnifeCMS.Tpl.Content = KnifeCMS.Tpl.ParseIf(KnifeCMS.Tpl.Content)
	KnifeCMS.Tpl.Content = KnifeCMS.Tpl.ParseSubIf(KnifeCMS.Tpl.Content)
	KnifeCMS.Tpl.Content = KnifeCMS.Tpl.ParseComIf(KnifeCMS.Tpl.Content)
	Call KnifeCMS.Tpl.ParseSEO()
End Function

'生成[网站首页]静态页面
Function CreateHTML_Index()
	Call ParseHTML_Init()
	CacheName        = "index"& FileSuffix
	SeoTitle         = SiteTitle
	MetaKeywords     = SiteMetaKeywords
	MetaDescription  = SiteMetaDescription
	TemplateFilePath = TemplateFilePath &"index.htm"
	Call ParseHTML(TemplateFilePath)
End Function


'生成[网站首页]静态页面
Function CreateHTML_ContentSystem(ByVal BV_ContentSubSystem,ByVal BV_IsCategory,ByVal BV_ID,ByVal BV_Page)
	Call ParseHTML_Init()
	Ctl       = "content"
	SubSystem = BV_ContentSubSystem
	ID        = BV_ID
	Page      = BV_Page
	If Not(BV_IsCategory) And BV_ID>0 Then
	'内容详细页
		If BV_Page>0 Then
			CacheName = Lcase("content/"& BV_ContentSubSystem &"/"& BV_ID &"_"& BV_Page &"/index"& FileSuffix)
		Else
			CacheName = Lcase("content/"& BV_ContentSubSystem &"/"& BV_ID &"/index"& FileSuffix)
		End If
		If Not((IsCache=1 And KnifeCMS.Cache.CheckCache(CacheName)) Or IsStaticRunMode) Then
			Call GetContentSubSystemInfo(BV_ContentSubSystem)
			If IsContentExist Then
				TemplateFilePath = TemplateFilePath & KnifeCMS.Tpl.GetContentTemplate(TablePre &"Content_"& BV_ContentSubSystem &"_Class",BV_ID,ContentTemplate)
				If Not(KnifeCMS.FSO.FileExists(Server.MapPath(TemplateFilePath))) then
					KnifeCMS.Error.Msg = TemplateFilePath
					KnifeCMS.Error.Raise 8
					Err.Clear
				Else
					Content_ID      = BV_ID
					IsArticleDetail = True
					Call GetArticleDetail(BV_ContentSubSystem,Content_ID)
					If IsContentExist Then
						If KnifeCMS.Data.IsNul(Content_LinkContents) Then Content_LinkContents = KnifeCMS.GetLikeContents(BV_ContentSubSystem,Content_ID,0,10)
						TagLine   = TagLine &"<span class=""raquo"">&raquo;</span><a href="""& SiteURL & SystemPath &"?content/"& BV_ContentSubSystem &"/"& SeoUrl & FileSuffix &""" target=""_blank"">"& SubSystemName &"</a>"& GetClassTagLine("content",BV_ContentSubSystem,TablePre &"Content_"& BV_ContentSubSystem &"_Class",Content_CategoryID) &"<span class=""raquo"">&raquo;</span>"& Content_Title
					Else
						CacheName = Lcase("error404"& FileSuffix) : TemplateFilePath = TemplateFilePath &"error404.htm"
					End If
				End If
			Else
				CacheName = Lcase("error404"& FileSuffix) : TemplateFilePath = TemplateFilePath &"error404.htm"
			End If
		End If
	ElseIf BV_IsCategory And BV_ID>0 Then
	'分类页
		If BV_Page>0 Then
			CacheName = Lcase("content/"& BV_ContentSubSystem &"/category/" & BV_ID &"_"& BV_Page &"/index"& FileSuffix)
		Else
			CacheName = Lcase("content/"& BV_ContentSubSystem &"/category/" & BV_ID &"/index"& FileSuffix)
		End If
		If Not((IsCache=1 And KnifeCMS.Cache.CheckCache(CacheName)) Or IsStaticRunMode) Then
			Call GetContentSubSystemInfo(BV_ContentSubSystem)
			If IsContentExist Then
				TemplateFilePath = TemplateFilePath & KnifeCMS.Tpl.GetCategoryTemplate(TablePre &"Content_"& BV_ContentSubSystem &"_Class",BV_ID,CategoryTemplate)
				If Not(KnifeCMS.FSO.FileExists(Server.MapPath(TemplateFilePath))) then
					KnifeCMS.Error.Msg = TemplateFilePath
					KnifeCMS.Error.Raise 8
					Err.Clear
				Else
					TagLine = TagLine &"<span class=""raquo"">&raquo;</span><a href="""& SiteURL & SystemPath &"?content/"& BV_ContentSubSystem &"/"& SeoUrl & FileSuffix &""">"& SubSystemName &"</a>"& GetClassTagLine("content",BV_ContentSubSystem,TablePre&"Content_"& BV_ContentSubSystem &"_Class",BV_ID)
					Call GetCategoryInfo(TablePre &"Content_"& BV_ContentSubSystem &"_Class",BV_ID)
				End If
			Else
				CacheName = Lcase("error404"& FileSuffix) : TemplateFilePath = TemplateFilePath &"error404.htm"
			End If
		End If
	Else
	'模型首页
		If BV_Page>0 Then
			CacheName = Lcase("content/"& BV_ContentSubSystem &"/index_page"& BV_Page & FileSuffix)
		Else
			CacheName = Lcase("content/"& BV_ContentSubSystem &"/index"& FileSuffix)
		End If
		If Not((IsCache=1 And KnifeCMS.Cache.CheckCache(CacheName)) Or IsStaticRunMode) Then
			Call GetContentSubSystemInfo(BV_ContentSubSystem)
			If IsContentExist Then
				TemplateFilePath = TemplateFilePath & IndexTemplate
				If Not(KnifeCMS.FSO.FileExists(Server.MapPath(TemplateFilePath))) then
					KnifeCMS.Error.Msg = TemplateFilePath
					KnifeCMS.Error.Raise 8
					Err.Clear
				Else
					TagLine   = TagLine &"<span class=""raquo"">&raquo;</span><a href="""& SiteURL & SystemPath &"?content/"& BV_ContentSubSystem &"/"& SeoUrl & FileSuffix &""" target=""_blank"">"& SubSystemName &"</a>"
				End If
			Else
				CacheName = Lcase("error404"& FileSuffix) : TemplateFilePath = TemplateFilePath &"error404.htm"
			End If
		End If
	End If
	Call ParseHTML(TemplateFilePath)
End Function

'生成[投票系统]静态页面
Function CreateHTML_Vote(ByVal BV_IsCategory,ByVal BV_ID,ByVal BV_Page)
	Call ParseHTML_Init()
	Ctl       = "vote"
	ID        = BV_ID
	Page      = BV_Page
	If Not(BV_IsCategory) And BV_ID>0 Then
	'详细页
		CacheName = Lcase("vote/"& BV_ID &"/index"& FileSuffix)
		If Not((IsCache=1 And KnifeCMS.Cache.CheckCache(CacheName)) Or IsStaticRunMode) Then
			TemplateFilePath = TemplateFilePath & KnifeCMS.Tpl.GetContentTemplate(TablePre &"VoteClass",BV_ID,"vote.detail.htm")
			Vote_ID      = BV_ID
			IsVoteDetail = True
			Call GetVoteDetail()
			If IsContentExist Then
				TagLine   = TagLine &"<span class=""raquo"">&raquo;</span><a href="""& SiteURL & SystemPath &"?vote"& FileSuffix &""" target=""_blank"">"& Lang_Client_Vote &"</a>"& GetClassTagLine("vote","",DBTable_VoteClass,Vote_ClassID) &"<span class=""raquo"">&raquo;</span>"& Vote_Title
			Else
				CacheName = Lcase("error404"& FileSuffix) : TemplateFilePath = TemplateFilePath &"error404.htm"
			End If
		End If
	ElseIf BV_IsCategory And BV_ID>0 Then
	'分类页
		If BV_Page>0 Then
			CacheName = Lcase("vote/category/"& BV_ID &"_"& BV_Page &"/index"& FileSuffix )
		Else
			CacheName = Lcase("vote/category/"& BV_ID &"/index"& FileSuffix )
		End If
		If Not((IsCache=1 And KnifeCMS.Cache.CheckCache(CacheName)) Or IsStaticRunMode) Then
			Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM "& DBTable_VoteClass &" WHERE ID="& BV_ID &"")
			If Not(Rs.Eof) Then IsContentExist  = True Else IsContentExist = False
			KnifeCMS.DB.CloseRs Rs
			If IsContentExist Then
				TemplateFilePath = TemplateFilePath & KnifeCMS.Tpl.GetCategoryTemplate(TablePre &"VoteClass",BV_ID,"vote.category.htm")
				TagLine   = TagLine &"<span class=""raquo"">&raquo;</span><a href="""& SiteURL & SystemPath &"?vote"& FileSuffix &"""target=""_blank"">"& Lang_Client_Vote &"</a>"& GetClassTagLine("vote","",DBTable_VoteClass,BV_ID)
				Call GetCategoryInfo(TablePre &"VoteClass",BV_ID)
			Else
				CacheName = Lcase("error404"& FileSuffix) : TemplateFilePath = TemplateFilePath &"error404.htm"
			End If
		End If
	Else
	'主页
		If BV_Page>0 Then
			CacheName = Lcase(Ctl &"_page"& BV_Page & FileSuffix)
		Else
			CacheName = Lcase(Ctl & FileSuffix)
		End If
		SeoTitle  = Lang_Client_Vote
		TagLine   = TagLine &"<span class=""raquo"">&raquo;</span><a href="""& SiteURL & SystemPath &"?vote"& FileSuffix &""" target=""_blank"">"& Lang_Client_Vote &"</a>"
		TemplateFilePath = TemplateFilePath &"vote.htm"
	End If
	Call ParseHTML(TemplateFilePath)
End Function

'生成[]静态页面
Function CreateHTML_Shop(ByVal BV_IsCategory,ByVal BV_ID,ByVal BV_Page)
	Call ParseHTML_Init()
	Ctl       = "shop"
	ID        = BV_ID
	Page      = BV_Page
	If Not(BV_IsCategory) And BV_ID>0 Then
		CacheName = Lcase("shop/"& BV_ID & FileSuffix)
		If Not((IsCache=1 And KnifeCMS.Cache.CheckCache(CacheName)) Or IsStaticRunMode) Then
			TemplateFilePath = TemplateFilePath & KnifeCMS.Tpl.GetContentTemplate(TablePre &"GoodsClass",BV_ID,"shop.goods.detail.htm")
			Goods_ID      = BV_ID
			IsGoodsDetail = True
			Call GetGoodsDetail(Goods_ID)
			If IsContentExist Then
				if SeoTitle="" then SeoTitle = Goods_Name
				TagLine   = TagLine &"<span class=""raquo"">&raquo;</span><a href="""& SiteURL & SystemPath &"?shop"& FileSuffix &""">"& Lang_Client_Shop &"</a>"& GetClassTagLine("shop","",DBTable_GoodsClass,Goods_ClassID) &"<span class=""raquo"">&raquo;</span>"& Goods_Name
			Else
				CacheName = Lcase("error404"& FileSuffix) : TemplateFilePath = TemplateFilePath &"error404.htm"
			End If
		End If
	ElseIf BV_IsCategory And BV_ID>0 Then
		If BV_Page>0 Then
			CacheName = Lcase("shop/category/"& BV_ID &"_"& BV_Page &"/index"& FileSuffix )
		Else
			CacheName = Lcase("shop/category/"& BV_ID &"/index"& FileSuffix )
		End If
		If Not((IsCache=1 And KnifeCMS.Cache.CheckCache(CacheName)) Or IsStaticRunMode) Then
			Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM "& DBTable_GoodsClass &" WHERE ID="& BV_ID &"")
			If Not(Rs.Eof) Then IsContentExist  = True Else IsContentExist = False
			KnifeCMS.DB.CloseRs Rs
			If IsContentExist Then
				TemplateFilePath = TemplateFilePath & KnifeCMS.Tpl.GetCategoryTemplate(TablePre &"GoodsClass",BV_ID,"shop.goods.category.htm")
				TagLine   = TagLine &"<span class=""raquo"">&raquo;</span><a href="""& SiteURL & SystemPath &"?shop"& FileSuffix &""">"& Lang_Client_Shop &"</a>"& GetClassTagLine("shop","",DBTable_GoodsClass,BV_ID)
				Call GetCategoryInfo(TablePre &"GoodsClass",BV_ID)
			Else
				CacheName = Lcase("error404" & FileSuffix) : TemplateFilePath = TemplateFilePath &"error404.htm"
			End If
		End If
	Else
		If BV_Page>0 Then
			CacheName = Lcase(Ctl &"_page"& BV_Page & FileSuffix)
		Else
			CacheName = Lcase(Ctl & FileSuffix)
		End If
		SeoTitle         = ShopTitle
		MetaKeywords     = ShopMetaKeywords
		MetaDescription  = ShopMetaDescription
		TemplateFilePath = TemplateFilePath &"shop.htm"
	End If
	Call ParseHTML(TemplateFilePath)
End Function

'生成[会员登陆]静态页面
Function CreateHTML_Login(ByVal BV_Action)
	Call ParseHTML_Init()
	If BV_Action = "out" Then
		Call OS.LoginOut()
		TemplateFilePath = TemplateFilePath &"login.out.htm"
	ElseIf BV_Action = "result" Then
		TemplateFilePath = TemplateFilePath &"login.result.htm"
	Else
		If Logined=1 Then 
			UserCenter = true : TemplateFilePath = TemplateFilePath &"my.index.htm"
		Else
			Call KnifeCMS.Cookie.SetCookie("lasturl",Cstr(Request.ServerVariables("HTTP_REFERER")),"")
			CacheName = Lcase("login"& FileSuffix)
			TemplateFilePath = TemplateFilePath &"login.htm"
		End If
	End If
	Call ParseHTML(TemplateFilePath)
End Function

'生成[会员注册]静态页面
Function CreateHTML_Reg(ByVal BV_Action)
	Call ParseHTML_Init()
	If BV_Action = "result" Then
		TemplateFilePath = TemplateFilePath &"reg.result.htm"
	Else
		If Logined=1 Then
			UserCenter = true : TemplateFilePath = TemplateFilePath &"my.index.htm"
		Else
			Call KnifeCMS.Cookie.SetCookie("lasturl",Cstr(Request.ServerVariables("HTTP_REFERER")),"")
			CacheName = Lcase("reg"& FileSuffix)
			TemplateFilePath = TemplateFilePath &"reg.htm"
		End If
	End If
	Call ParseHTML(TemplateFilePath)
End Function

'生成[找回密码-重设密码]静态页面
Function CreateHTML_GetPassword()
	Call ParseHTML_Init()
	Dim Fn_Rs,Fn_UserExist,Fn_Check
	Dim Fn_SQL,Fn_UserID,Fn_Username,Fn_Email,Fn_Password,Fn_TruePassword,Fn_RegTime,Fn_LastLoginTime,Fn_Disable,Fn_Recycle
	Dim Fn_Hash,Fn_GetHash,Fn_HTML
	Fn_Check     = False
	Fn_UserExist = False
	'用户信息
	Fn_UserID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("get","uid"))
	If Fn_UserID>0 Then
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,Username,Email,Password,TruePassword,RegTime,LastLoginTime,Disable,Recycle FROM ["& DBTable_Members &"] WHERE ID="& Fn_UserID &"")
		If Not(Fn_Rs.Eof) Then
			Fn_UserExist     = True
			Fn_Username      = Fn_Rs("Username")
			Fn_Email         = Fn_Rs("Email")
			Fn_Password      = Fn_Rs("Password")
			Fn_TruePassword  = Fn_Rs("TruePassword")
			Fn_RegTime       = Fn_Rs("RegTime")
			Fn_LastLoginTime = Fn_Rs("LastLoginTime")
			Fn_Disable       = KnifeCMS.Data.CLng(Fn_Rs("Disable"))
			Fn_Recycle       = KnifeCMS.Data.CLng(Fn_Rs("Recycle"))
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
	End If
	If Fn_UserExist=True Then
		'分析是否是有效的地址
		Fn_Hash    = KnifeCMS.MD5.Encrypt(Fn_UserID &":"& Fn_Password &":"& Fn_TruePassword &":"& Fn_LastLoginTime)
		Fn_GetHash = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("get","hash"),"[^0-9a-zA-Z]",""),250)
		If Fn_Hash=Fn_GetHash Then
			Fn_Check = True
		Else
			Fn_Check = False
			Fn_HTML = Fn_HTML &"<div class=""eline2"">"& Lang_User(4) &"</div>"
		End If
	Else
		Fn_HTML  = Fn_HTML &"<div class=""eline2"">"& Lang_User(1) &"</div>"
	End If
	If Fn_Check=False Then
		Echo PrintHTMLHeader()
		Echo Fn_HTML
		Echo PrintHTMLFooter()
	Else
		TemplateFilePath = TemplateFilePath &"getpassword.htm"
		Call ParseHTML(TemplateFilePath)
		HTMLString = KnifeCMS.Data.ReplaceString(HTMLString,"{getpassword:userid}",Fn_UserID)
		HTMLString = KnifeCMS.Data.ReplaceString(HTMLString,"{getpassword:username}",Fn_Username)
		HTMLString = KnifeCMS.Data.ReplaceString(HTMLString,"{getpassword:hash}",Fn_GetHash)
	End If
End Function

'生成[会员个人中心]页面
Function CreateHTML_MyHome()
	Call ParseHTML_Init()
	If Not(Logined=1) Then 
		RedirectURL = "?login"& FileSuffix
	Else
		UserCenter = true
		If SubSystem="avatarset" Then
			TemplateFilePath = TemplateFilePath &"my.set.avatar.htm"
		ElseIf SubSystem="infoset" Then
			IsUserInfoDetail = True : Call GetUserInfoDetail(UserID)
			TemplateFilePath = TemplateFilePath &"my.set.info.htm"
		ElseIf SubSystem="deliveryset" Then
			IsMyAddrs = True : Call GetMyAddrs()
			TemplateFilePath = TemplateFilePath &"my.set.delivery.htm"
		ElseIf SubSystem="passwordset" Then
			TemplateFilePath = TemplateFilePath &"my.set.password.htm"
		ElseIf SubSystem="browse" Then
			TemplateFilePath = TemplateFilePath &"my.browse.htm"
		ElseIf SubSystem="friend" Then
			TemplateFilePath = TemplateFilePath &"my.friend.htm"
		ElseIf  SubSystem="order" Then
			TemplateFilePath = TemplateFilePath &"my.order.htm"
		ElseIf SubSystem="finance" Then
			TemplateFilePath = TemplateFilePath &"my.finance.htm"
		Else
			TemplateFilePath = TemplateFilePath &"my.index.htm"
		End If
	End If
	Call ParseHTML(TemplateFilePath)
End Function

'生成[会员个人主页]页面
Function CreateHTML_UserHome()
	Call ParseHTML_Init()
	If Not(Logined=1) Then 
		RedirectURL = "?login"& FileSuffix
	Else
		UserCenter = true
		IsUserHome = true
		Call GetUserInfo()
		If IsUserExist Then
			Call RecordVisitor()
			If SubSystem="info" Then
				IsUserInfoDetail = True : Call GetUserInfoDetail(User_ID)
				TemplateFilePath = TemplateFilePath &"user.info.htm"
			ElseIf SubSystem="twriter" Then
				TemplateFilePath = TemplateFilePath &"user.twriter.htm"
			ElseIf SubSystem="friend" Then
				TemplateFilePath = TemplateFilePath &"user.friend.htm"
			Else
				TemplateFilePath = TemplateFilePath &"user.index.htm"
			End If
		Else
			TemplateFilePath = TemplateFilePath &"error404.htm"
		End If
	End If
	Call ParseHTML(TemplateFilePath)
End Function

'生成[我的购物车]静态页面
Function CreateHTML_MyCart(ByVal BV_Action)
	Call ParseHTML_Init()
	Cart_IsCOD       = 0
	Cart_IsHavePayed = 0
	If BV_Action = "checkout" Then
	'第2步:填写订单信息
		If Not(Logined=1) Then 
			RedirectURL = "?login"& FileSuffix
		Else
			IsShopCart = True
			IsMyAddrs  = True
			Call GetMyAddrs()
			TemplateFilePath = TemplateFilePath &"shop.cart.step2.htm"
		End If
	ElseIf BV_Action = "ordercreate" Then
	'第3步:创建订单
		If Not(Logined=1) Then
			RedirectURL = "?login"& FileSuffix
		Else
			PlaceOrderResult = Cart.PlaceOrder
			If Not(KnifeCMS.Data.IsNul(PlaceOrderResult)) Then
				Set PlaceOrderResult = KnifeCMS.JSON.Parse(PlaceOrderResult) 'Echo "==="& KnifeCMS.JSON.Stringify(PlaceOrderResult.errorinfo)
				If KnifeCMS.Data.IsNul(PlaceOrderResult.errorinfo) Then
					IsShopCart = True
					Cart_OrderID      = PlaceOrderResult.orderid
					Cart_OrderTime    = KnifeCMS.Data.FormatDateTime(PlaceOrderResult.ordertime,0)
					Cart_OrderAmount  = KnifeCMS.Data.FormatCurrency(PlaceOrderResult.orderamount)
					Cart_IsCOD        = KnifeCMS.Data.CLng(PlaceOrderResult.iscod)
					Cart_PaymentsList = GetCartPaymentsList(Cart_OrderID)
					TemplateFilePath  = TemplateFilePath &"shop.cart.step3.htm"
				Else
					TemplateFilePath  = TemplateFilePath &"error.htm"
				End If
				Set PlaceOrderResult  = nothing
			Else
				TemplateFilePath = TemplateFilePath &"error.htm"
			End If
		End If
	ElseIf BV_Action = "suc" Then
	'第4步:成功提交订单
		If Not(Logined=1) Then 
			RedirectURL = "?login"& FileSuffix
		Else
			IsShopCart       = True
			Cart_OrderID     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","orderid"),"[^0-9]",""),16)
			Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT Final_Amount,PaymentID,Payment,Payed FROM ["& DBTable_Order &"] WHERE OrderID='"& Cart_OrderID &"'")
			If Not(Rs.Eof) Then
				Cart_OrderAmount = KnifeCMS.Data.FormatDouble(Rs(0))
				Cart_PaymentID   = Rs(1)
				Cart_PaymentName = Rs(2)
				Cart_Payed       = KnifeCMS.Data.FormatDouble(Rs(3))
				If Cart_Payed > 0 Then Cart_IsHavePayed=3
				If Cart_OrderAmount = Cart_Payed Then Cart_IsHavePayed=1
			End If
			KnifeCMS.DB.CloseRs Rs
			If KnifeCMS.DB.GetFieldByArrayField(DBTable_Payments,"PayType",Array("ID:"& Cart_PaymentID))="cod" Then Cart_IsCOD=1
			Cart_OrderAmount = KnifeCMS.Data.FormatCurrency(Cart_OrderAmount)
			TemplateFilePath = TemplateFilePath &"order.pay.result.htm"
		End If
	Else
	'第1步:查看我的购物车
		CacheName = Lcase("mycart"& FileSuffix)
		TemplateFilePath = TemplateFilePath &"shop.cart.htm"
	End If
	SeoTitle  = Lang_Client_ShopCart
	TagLine   = TagLine &"<span class=""raquo"">&raquo;</span>"& Lang_Client_ShopCart
	Call ParseHTML(TemplateFilePath)
End Function

'生成[我的订单]静态页面
Function CreateHTML_MyOrder()
	Call ParseHTML_Init()
	TemplateFilePath = TemplateFilePath &"myorder.htm"
	Call ParseHTML(TemplateFilePath)
End Function

'订单支付
Function CreateHTML_Order(ByVal BV_Action)
	Call ParseHTML_Init()
	Cart_IsCOD       = 0
	Cart_IsHavePayed = 0
	If BV_Action = "payresult" Then
	'订单支付结果
		If Not(Logined=1) Then 
			RedirectURL = "?login"& FileSuffix
		Else
			IsShopCart       = True
			Cart_OrderID     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","orderid"),"[^0-9]",""),16)
			Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT Final_Amount,PaymentID,Payment,Payed FROM ["& DBTable_Order &"] WHERE OrderID='"& Cart_OrderID &"'")
			If Not(Rs.Eof) Then
				Cart_OrderAmount = KnifeCMS.Data.FormatDouble(Rs(0))
				Cart_PaymentID   = Rs(1)
				Cart_PaymentName = Rs(2)
				Cart_Payed       = KnifeCMS.Data.FormatDouble(Rs(3))
				If Cart_Payed > 0 Then Cart_IsHavePayed=3
				If Cart_OrderAmount = Cart_Payed Then Cart_IsHavePayed=1
			End If
			KnifeCMS.DB.CloseRs Rs
			If KnifeCMS.DB.GetFieldByArrayField(DBTable_Payments,"PayType",Array("ID:"& Cart_PaymentID))="cod" Then Cart_IsCOD=1
			Cart_OrderAmount = KnifeCMS.Data.FormatCurrency(Cart_OrderAmount)
			TemplateFilePath = TemplateFilePath &"order.pay.result.htm"
		End If
	End If
	SeoTitle  = Lang_Client_Order
	TagLine   = TagLine &"<span class=""raquo"">&raquo;</span>"& Lang_Client_Order
	Call ParseHTML(TemplateFilePath)
End Function

'生成[搜索]页面
Function CreateHTML_Search()
	Call ParseHTML_Init()
	TemplateFilePath = TemplateFilePath &"search.htm"
	Call KnifeCMS.Include("include/module/search.class.asp")
	Call Search.Init()
	Call ParseHTML(TemplateFilePath)
	HTMLString = KnifeCMS.Data.ReplaceString(HTMLString,"{kfc:searchresult}",SearchResult)
End Function

'生成[在线留言]静态页面
Function CreateHTML_Guestbook()
	Call ParseHTML_Init()
	CacheName = Lcase("guestbook"& FileSuffix)
	SeoTitle  = Lang_Client_Guestbook
	TagLine   = TagLine &"<span class=""raquo"">&raquo;</span>"& Lang_Client_Guestbook
	TemplateFilePath = TemplateFilePath &"guestbook.htm"
	Call ParseHTML(TemplateFilePath)
End Function

'生成[error404]静态页面
Function CreateHTML_Error404()
	Call ParseHTML_Init()
	CacheName        = Lcase("error404"& FileSuffix)
	TemplateFilePath = TemplateFilePath &"error404.htm"
	Call ParseHTML(TemplateFilePath)
End Function

Class Class_KnifeCMS_Template
	Private Pv_ShowRunInfo,Pv_HeaderHTML,Pv_FooterHTML
	Private Pv_Content,Pv_SubSystem,Pv_MenuArray,Pv_ContentCateArray,Pv_VoteCateArray,Pv_GoodsCateArray
	Private Pv_Rs,Pv_Sql,Pv_Field,Pv_Len,Pv_TempString,Pv_TempArray
	Private Pv_Year,Pv_Month,Pv_Day,Pv_Hour,Pv_Minute,Pv_Second
	Private LabelRule,LabelRuleField,RegExpObj,StrDictionary,AttrDictionary,DataArrayDictionary
	Private LabelAttrList,LoopStringList,LoopStringListNew,LoopStringTotal,GlobalPreString,GlobalSufString
	Private Matches,Match,MatchesList,Matchlist,MatchField,MatchesField,FieldNameAndAttr,FieldName,FieldAttr
	Private DataArray,DataNum
	
	Private Sub Class_Initialize()
		Pv_ShowRunInfo       = False
		Set RegExpObj        = new RegExp
		RegExpObj.ignoreCase = True
		RegExpObj.Global     = True
		Set StrDictionary       = Server.CreateObject("Scripting.Dictionary")
		Set DataArrayDictionary = Server.CreateObject("Scripting.Dictionary")
	End Sub
	
	Private Sub Class_Terminate()
		Set RegExpObj     = Nothing
		Set StrDictionary = Nothing
		Set DataArrayDictionary = Nothing
	End Sub
	
	Public Property Let CSubSystem(ByVal BV_String)
		Pv_SubSystem = BV_String
	End Property
	Public Property Get CSubSystem()
		CSubSystem = Pv_SubSystem
	End Property
	
	Public Property Let Content(ByVal BV_String)
		Pv_Content = BV_String
	End Property
	Public Property Get Content()
		Content = Pv_Content
	End Property
	
	Public Property Let ShowRunInfo(ByVal BV_String)
		Pv_ShowRunInfo = BV_String
	End Property
	Public Property Get ShowRunInfo()
		ShowRunInfo = Pv_ShowRunInfo
	End Property
	
	Public Function ParseAttr(ByVal BV_Attr)
		Dim Fn_A,Fn_B
		Dim Fn_i,Fn_C,Fn_D,Fn_E
		Fn_A = KnifeCMS.Data.TrimOuter(KnifeCMS.Data.RegReplaceM(BV_Attr,"[\s]+",chr(32)))
		Fn_B = Split(Fn_A,chr(32))
		for Fn_i=0 to ubound(Fn_B)
			Fn_C = Split(Fn_B(Fn_i),chr(61))
			If ubound(Fn_C) >= 1 then
				Fn_D = Fn_C(0)
				Fn_E = Fn_C(1)
				If Not StrDictionary.Exists(Fn_D) Then 
					StrDictionary.Add Fn_D,Fn_E
				Else
					StrDictionary(Fn_D)=Fn_E
				End If
			End If
		next
		Set ParseAttr=StrDictionary
	End Function
	
	Public Function GetContentTemplate(ByVal BV_DBTable, ByVal BV_ID, ByVal BV_DefaultTpl)
		GetContentTemplate = GetCTemplate(BV_DBTable,"ContentTemplate",BV_ID,BV_DefaultTpl)
	End Function
	
	Public Function GetCategoryTemplate(ByVal BV_DBTable, ByVal BV_ID, ByVal BV_DefaultTpl)
		GetCategoryTemplate = GetCTemplate(BV_DBTable,"CategoryTemplate",BV_ID,BV_DefaultTpl)
	End Function
	
	'获取分类设置的分类/内容的模版文件
	Public Function GetCTemplate(ByVal BV_DBTable, ByVal BV_Field, ByVal BV_ID, ByVal BV_DefaultTpl)
		Dim Fn_Rs,Fn_PID,Fn_TSet,Fn_Tpl,Fn_Temp
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ParentClassID,TemplateSet,"& BV_Field &" FROM "& BV_DBTable &" WHERE ID="& BV_ID &"")
		If Not(Fn_Rs.Eof) Then
			Fn_PID  = KnifeCMS.Data.CLng(Fn_Rs("ParentClassID"))
			Fn_TSet = KnifeCMS.Data.CLng(Fn_Rs("TemplateSet"))
			If Fn_TSet=1 Then
			'已设置模版
				Fn_Temp = KnifeCMS.Data.ParseRs(Fn_Rs(BV_Field))
			Else
			'继承父类
				If Fn_PID > 0 Then
					Fn_Temp = GetCTemplate(BV_DBTable,BV_Field,Fn_PID,BV_DefaultTpl)
				End If
			End If
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		Fn_Tpl = Fn_Temp
		If Fn_Tpl="" Then Fn_Tpl = BV_DefaultTpl
		GetCTemplate = Fn_Tpl
	End Function
	
	Public Function GetMenuArray()
		Dim Fn_Rs
        Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("Select ID,NavName,NavUrl,Target,P_NavID,NavType FROM "&DBTable_Navigator&" WHERE Disabled=0 ORDER BY OrderNum ASC,ID ASC")
        If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then Pv_MenuArray = Fn_Rs.GetRows()
		KnifeCMS.DB.CloseRs Fn_Rs
	End Function
	
	Public Function GetGoodsCateArray()
		Dim Fn_Rs
        Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("Select ID,ParentClassID,ClassName,SeoUrl,ClassType,Remarks,Icon,Pic FROM ["&DBTable_GoodsClass&"] WHERE ClassHide=0 ORDER BY ClassOrder ASC,ID ASC")
        If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then Pv_GoodsCateArray = Fn_Rs.GetRows()
		KnifeCMS.DB.CloseRs Fn_Rs
	End Function
	
	Public Function GetVoteCateArray()
		Dim Fn_Rs
        Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("Select ID,ParentClassID,ClassName,SeoUrl,ClassType,Remarks,Icon,Pic FROM ["&DBTable_VoteClass&"] WHERE ClassHide=0 ORDER BY ClassOrder ASC,ID ASC")
        If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then Pv_VoteCateArray = Fn_Rs.GetRows()
		KnifeCMS.DB.CloseRs Fn_Rs
	End Function
	
	Public Function GetContentCateArray(ByVal BV_ContentSubSystem)
		If KnifeCMS.Data.IsNul(BV_ContentSubSystem) Then Exit Function
		Dim Fn_Rs,Fn_String
		If DataArrayDictionary.Exists(BV_ContentSubSystem) Then 
			GetContentCateArray = DataArrayDictionary(BV_ContentSubSystem)
		Else
			If Not(IsContentSubSystemExist(BV_ContentSubSystem)) Then Exit Function
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("Select ID,ParentClassID,ClassName,SeoUrl,ClassType,Remarks,Icon,Pic FROM ["& TablePre &"Content_"& BV_ContentSubSystem &"_Class] WHERE ClassHide=0 ORDER BY ClassOrder ASC,ID ASC")
			If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then
				Fn_String = Fn_Rs.GetRows()
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
			DataArrayDictionary.Add BV_ContentSubSystem,Fn_String
			GetContentCateArray = Fn_String
			Set Fn_String = Nothing
		End If
	End Function
	
	'从缓存中获取
	Function LoadFileOnCache(Byval BV_FileFlag,Byval BV_FilePath)
		Dim Fn_CacheName,Fn_String
		Fn_CacheName = BV_FileFlag & BV_FilePath
		If IsCache=1 Then
			If KnifeCMS.Cache.CheckCache(Fn_CacheName) Then
				Fn_String = KnifeCMS.Cache.GetCache(Fn_CacheName)
			Else
				Fn_String = KnifeCMS.Read(SystemPath & BV_FilePath)
				Call KnifeCMS.Cache.SetCache(Fn_CacheName,Fn_String)
			End If
		Else
			Fn_String = KnifeCMS.Read(SystemPath & BV_FilePath)
		End if
		LoadFileOnCache = Fn_String
	End Function
	
	'解析头和尾
	Public Function ParseTopAndFoot()
		Dim Fn_FileName
		RegExpObj.Pattern = "<!-- *?#include +?(file|virtual) *?= *?""??([^"":?*\f\n\r\t\v]+?)""?? *?-->"
		Set Matches = RegExpObj.Execute(Pv_Content)
		For Each Match in Matches
			Fn_FileName = Match.SubMatches(1)
			Pv_Content  = KnifeCMS.Data.RegReplace(Pv_Content,Match.Value,KnifeCMS.Read(TemplatePath & TemplateHTMLFolder &"/"& Match.SubMatches(1)))
		Next
		If Not(IsArray(Pv_MenuArray)) Then Call GetMenuArray()
		Pv_Content = Replace(Pv_Content,"../../../",SystemPath)
		Pv_Content = Replace(Pv_Content,"../../",SystemPath & TemplateRoot)
		Pv_Content = Replace(Pv_Content,"../",TemplatePath)
	End Function
	
	'整站全局解析
	Public Function ParseGlobal()
		'SitePoweredby="Powered by <a href=""http://www.knifecms.com/"" title=""KnifeCMS 建站系统"" target=""_blank"">KnifeCMS 建站系统</a>"
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:version}",Sys_Version)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:domain}",Domain)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:siteurl}",SiteURL)
		
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:moneysb}",MoneySb)
		
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:sitenotice}",KnifeCMS.Data.HTMLDecode(SiteNotice))
		
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:sitename}",KnifeCMS.Data.ReplaceString(SiteName,"&nbsp;",Chr(32)))
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:sitetitle}",KnifeCMS.Data.ReplaceString(SiteTitle,"&nbsp;",Chr(32)))

		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:sitelogo}",SiteLogo)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:sitemyhomelogo}",SiteMyhomeLogo)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:sitebriefintroduction}",KnifeCMS.Data.HTMLDecode(SiteBriefIntroduction))
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:siteicp}",SiteIcp)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:powerdby}",SitePoweredBy)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:sitecopyright}",KnifeCMS.Data.HTMLDecode(SiteCopyright))
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:sitestatisticscode}",KnifeCMS.Data.HTMLDecode(SiteStatisticsCode))
		
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:systempath}",SystemPath)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:templatepath}",TemplatePath)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:filesuffix}",FileSuffix)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:ctl}",Ctl)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:id}",ID)

		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:subsystem}",SubSystem)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:subsystemname}",SubSystemName)
		
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:cookiepre}",CookiePre)
		
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:language}",KnifeCMS.Language)
		
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:regstatusmsg}",RegStatusMsg)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:logined}",Logined)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:userid}",UserID)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:username}",Username)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:usersmallhead}",UserSmallHead)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:userbighead}",UserBigHead)
		
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:contentcommentset}",ContentCommentSet)
		
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:closemodulevote}",CloseModuleVote)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:closemoduleshop}",CloseModuleShop)
	End Function
	
	Public Sub ParseRunInfo()
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:sqlquerynum}",KnifeCMS.DB.SqlQueryNum)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:runtime}",KnifeCMS.RunTime)
	End Sub
	
	'整站全局解析
	Public Function ParseSEO()
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:seotitle}",KnifeCMS.Data.ReplaceString(SeoTitle,"&nbsp;",Chr(32)))
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:metakeywords}",KnifeCMS.Data.ReplaceString(MetaKeywords,"&nbsp;",Chr(32)))
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:metadescription}",KnifeCMS.Data.ReplaceString(MetaDescription,"&nbsp;",Chr(32)))
	End Function
	
	'解析内容和商城全局
	Public Function ParseContentAndShopGlobal()
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:tagline}",TagLine)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:goodsconsultset}",GoodsConsultSet)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:goodscommentset}",GoodsCommentSet)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:isgoodsconsultcheck}",IsGoodsConsultCheck)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:isgoodscommentcheck}",IsGoodsCommentCheck)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:iscontentcommentcheck}",IsContentCommentCheck)
	End Function
	
	'解析在线客服
	Public Function ParseOnlineService()
		Dim Fn_JSON,Fn_OnlineService,Fn_OS_Show,Fn_KF,Fn_KF_Show
		Fn_JSON = KnifeCMS.SysConfig("OnlineService")
		If Fn_JSON="" Then
			Fn_OnlineService = ""
			Fn_KF = ""
		Else
			Set Fn_JSON = KnifeCMS.JSON.Parse(Fn_JSON)
			Fn_OS_Show = KnifeCMS.Data.CLng(Fn_JSON.onlineservice_show)
			Fn_KF_Show = KnifeCMS.Data.CLng(Fn_JSON.kf_show)
			If Fn_OS_Show=2 Then
				Fn_OnlineService = ""
			Else
				Fn_OnlineService = KnifeCMS.JSON.Stringify(Fn_JSON.onlineservice)
			End If
			If Fn_KF_Show=2 Then
				Fn_KF = ""
			Else
				Fn_KF = KnifeCMS.Data.UnEscape(Fn_JSON.kf)
			End If
		End If
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:onlineservice}",Fn_OnlineService)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:kf}",Fn_KF)
	End Function
	
	'解析内容系统、投票系统、网店系统
	Public Function ParseContentAndShop()
		
		Call ParseContentAndShopGlobal()
		
		Call ParseAd()
		Call ParseLinkList()
		
		If KnifeCMS.Data.RegTest(Pv_Content,"{kfc:([\s\S]*?)category([\s\S]*?)type=goods([\s\S]*?)}") Then GetGoodsCateArray
		If KnifeCMS.Data.RegTest(Pv_Content,"{kfc:([\s\S]*?)category([\s\S]*?)type=vote([\s\S]*?)}") Then GetVoteCateArray
		Call ParseCategory("")
		
		If IsArticleDetail Then Call ParseContentDetail()
		Call ParseContentList()
		
		If IsVoteDetail Then Call ParseVoteDetail()
		Call ParseVoteList()
		Call ParseVoteOptionList()

		If IsGoodsDetail Then
			Call ParseGoodsDetail()
			Call ParseAdjunctList()
		End If
		Call ParseGoodsList()
		
		Call ParseNavlist("")
		Call ParseCModule()

		Call ParseOnlineService()
	End Function
	
	'解析会员系统
	Public Function ParseUserCenter()
		Call ParseUserGlobal()
		If IsUserInfoDetail Then Call ParseUserInfoDetail()
		If IsMyAddrs Then Call ParseMyAddrs()
		Call ParseNavlist("")
		Call ParseCModule()
	End Function
	
	'解析会员全局
	Public Function ParseUserGlobal()
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:mysystemnotifynum}",MySystemNotifyNum)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:myfriendrequestnum}",MyFriendRequestNum)
	End Function
	
	'解析购物车定单系统
	Public Function ParseShopCart()
		If IsMyAddrs  Then Call ParseMyAddrs()
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:istax}",SiteIsTax)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{kfc:taxrate}",SiteTaxRate)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{cart:ordersysid}",KnifeCMS.CreateSysID)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{cart:orderid}",Cart_OrderID)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{cart:ordertime}",Cart_OrderTime)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{cart:orderamount}",Cart_OrderAmount)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{cart:iscod}",Cart_IsCOD)'是否是货到付款(0:不是，1：是)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{cart:paymentslist}",Cart_PaymentsList)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{cart:paymentname}",Cart_PaymentName)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{cart:ishavepayed}",Cart_IsHavePayed)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{cart:payedfee}",Cart_Payed)
	End Function
	
	'解析会员详细页面
	Public Function  ParseUserInfoDetail()
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:email}",UserInfo_Email)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:realname}",UserInfo_RealName)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:gender}",UserInfo_Gender)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:hometown style=path}",UserInfo_HomeTown(0))
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:hometown style=name}",UserInfo_HomeTown(1))
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:mysite style=path}",UserInfo_MySite(0))
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:mysite style=name}",UserInfo_MySite(1))
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:birthday}",UserInfo_Birthday)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:mobile}",UserInfo_Mobile)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:telephone}",UserInfo_Telephone)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:qq}",UserInfo_QQ)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:height}",UserInfo_Height)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:weight}",UserInfo_Weight)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:maritalstatus}",UserInfo_MaritalStatus)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:bloodtype}",UserInfo_BloodType)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:religion}",UserInfo_Religion)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:smoke}",UserInfo_Smoke)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:drink}",UserInfo_Drink)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:favtravel}",UserInfo_FavTravel)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:favliving}",UserInfo_FavLiving)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:favWork}",UserInfo_FavWork)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:favPartner}",UserInfo_FavPartner)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{userinfo:favCar}",UserInfo_FavCar)
	End Function
	
	'解析会员收货地址
	Public Function  ParseMyAddrs()
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{myaddrs:region}",MyAddrs_Region)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{myaddrs:address}",MyAddrs_Address)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{myaddrs:zip}",MyAddrs_Zip)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{myaddrs:name}",MyAddrs_Name)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{myaddrs:mobile}",MyAddrs_Mobile)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{myaddrs:telephone}",MyAddrs_Telephone)
	End Function
	
	'会员主页
	Public Function  ParseUserHome()
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{user:id}",User_ID)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{user:name}",User_Name)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{user:smallhead}",User_SmallHead)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{user:bighead}",User_BigHead)	
	End Function
	
	'解析内容详细页
	Public Function ParseContentDetail()
		Dim Fn_i,Fn_TempArray,Fn_FieldType
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:categoryid}",Content_CategoryID)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:title}",Content_Title)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:brieftitle}",Content_BriefTitle)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:author}",Content_Author)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:from}",Content_From)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:publictime}",Content_PublicTime)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:thumbnail}",Content_Thumbnail)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:uselinkurl}",Content_UseLinkUrl)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:linkurl}",Content_LinkUrl)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:tags}",Content_Tags)
		
		if KnifeCMS.Data.IsNul(Content_Abstract) Then Content_HaveAbstract=false else Content_HaveAbstract=True
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:haveabstract}",Content_HaveAbstract)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:abstract}",Content_Abstract)
		
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:content}",Content_Content)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:pages}",Content_ContentPages)
		
		If KnifeCMS.Data.IsNul(Content_FirstImg) Then Content_FirstImg = Content_Thumbnail
		Pv_Content   = KnifeCMS.Data.RegReplace(Pv_Content,"{content:firstimg}",Content_FirstImg)
		
		On Error Resume Next
		Content_HaveImgs = False
		If Content_Imgs<>"" Then
			Fn_TempArray = Split(Replace(Content_Imgs,",,",","),",")
			Content_Imgs = ""
			For Fn_i=0 To Ubound(Fn_TempArray)
				If Fn_TempArray(Fn_i)<>"" Then
				Content_Imgs     = Content_Imgs & "<li><a rel=""useZoom:'bigimgview',smallImage:'"&Fn_TempArray(Fn_i)&"'"" class=""cloud-zoom-gallery"" href="""&Fn_TempArray(Fn_i)&"""><img src="""& Fn_TempArray(Fn_i) &""" alt="""& Content_Title &""" /></a></li>"
				End If
			Next
			If Content_Imgs<>"" Then Content_HaveImgs = True
		End If
		If Err.Number<>0 Then Err.Clear()
		
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:haveimgs}",Content_HaveImgs)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:imgs}",Content_Imgs)
		
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:hits}",Content_Hits)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:comments}",Content_Comments)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:commentset}",Content_CommentSet)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:openness}",Content_Openness)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:topline}",Content_TopLine)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:ontop}",Content_OnTop)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:hot}",Content_Hot)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:elite}",Content_Elite)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:diggs}",Content_Diggs)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:edittime}",Content_EditTime)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:linkcontents}",Content_LinkContents)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{content:linkvotes}",Content_LinkVotes)
		
		'解析自定义字段
		If KnifeCMS.IsStringExist(Pv_Content,"{content:"& KnifeCMS.DB.FieldPre) Then
			RegExpObj.Pattern = "{content:"& KnifeCMS.DB.FieldPre &"([\s\S]+?)}"
			Set Matches = RegExpObj.Execute(Pv_Content)
			For Each Match in Matches
				FieldName     = KnifeCMS.Data.HTMLEncode(Match.SubMatches(0))
				Pv_TempString = ""
				If FieldName<>"" Then
					FieldName = KnifeCMS.DB.FieldPre & FieldName
					If KnifeCMS.DB.IsColumnExist(TablePre &"Content_"& Pv_SubSystem,FieldName) Then
						Set Pv_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT FieldType FROM ["& DBTable_FieldSet &"] WHERE Field='"& FieldName &"'")
						If Not(Pv_Rs.Eof) Then
							Fn_FieldType = Pv_Rs("FieldType")
						End If
						KnifeCMS.DB.CloseRs Pv_Rs
						Set Pv_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT "& FieldName &" FROM ["& TablePre &"Content_"& Pv_SubSystem &"] WHERE [ID]="& Content_ID &"")
						If Not(Pv_Rs.Eof) Then
							If Fn_FieldType="editor" Then
								Pv_TempString = KnifeCMS.Data.EditorContentDecode(Pv_Rs(0))
							Else
								Pv_TempString = KnifeCMS.Data.HTMLDecode(Pv_Rs(0))
							End If
						End If
						KnifeCMS.DB.CloseRs Pv_Rs
					End If
				End If
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,Pv_TempString)
			Next
			Set Matches = Nothing
		End If
		Pv_Content = ParseSubIf(Pv_Content)
	End Function
	
	'解析自定义标签内容模块
	Public Function ParseCModule()
		If Not(KnifeCMS.IsStringExist(Pv_Content,"{label:")) Then Exit Function
		RegExpObj.Pattern = "{label:([\s\S]+?)}"
		Set Matches = RegExpObj.Execute(Pv_Content)
		For Each Match in Matches
			FieldName     = KnifeCMS.Data.HTMLEncode(Match.SubMatches(0))
			Pv_TempString = ""
			If FieldName<>"" Then
				Set Pv_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT Content FROM ["& DBTable_CModule &"] WHERE CModuleLabel='"& FieldName &"'AND Recycle=0")
				If Not(Pv_Rs.Eof) Then
					Pv_TempString = KnifeCMS.Data.HTMLDecode(Pv_Rs(0))
				End If
				KnifeCMS.DB.CloseRs Pv_Rs
			End If
			Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,Pv_TempString)
		Next
		Set Matches = Nothing
	End Function
	
	'解析投票详细页
	Public Function ParseVoteDetail()
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{vote:id}",Vote_ID)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{vote:categoryid}",Vote_ClassID)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{vote:categoryname}",Vote_ClassName)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{vote:title}",Vote_Title)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{vote:votemode}",Vote_VoteMode)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{vote:votemodemore}",Vote_VoteModeMore)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{vote:description}",Vote_Description)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{vote:resultdescribe}",Vote_ResultDescribe)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{vote:hits}",Vote_Hits)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{vote:joins}",Vote_Joins)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{vote:addtime}",Vote_Addtime)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{vote:edittime}",Vote_Edittime)
	End Function
	
	'$:广告模板
	Public Function ParseAd()
		If Not(KnifeCMS.IsStringExist(Pv_Content,"{kfc:ad")) Then Exit Function
		Dim Fn_Rs,Fn_i,Fn_Len,Fn_Identify,Fn_AdsGroupID,Fn_Width,Fn_Height,Fn_StartTime,Fn_EndTime
		Dim Fn_TempString,Fn_AbsLen
		Dim Fn_Check : Fn_Check = False
		LabelRule      = "{kfc:ad([\s\S]*?)}([\s\S]*?){ad:loop}([\s\S]*?){/ad:loop}([\s\S]*?){/kfc:ad}"
		LabelRuleField = "\[ad:([\s\S]+?)\]"
		RegExpObj.Pattern = LabelRule
		Set Matches = RegExpObj.Execute(Pv_Content)
		For Each Match in Matches
			Fn_Check        = True
			LabelAttrList   = Match.SubMatches(0)
			GlobalPreString = Match.SubMatches(1)
			LoopStringList  = Match.SubMatches(2)
			GlobalSufString = Match.SubMatches(3)
			Set AttrDictionary=ParseAttr(LabelAttrList)
			Fn_Identify=AttrDictionary("identify")
			Set AttrDictionary=Nothing
			Fn_AdsGroupID=0
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,Width,Height,StartTime,EndTime FROM ["& DBTable_Ads_Group &"] WHERE Identify='"& Fn_Identify &"' AND Disabled=0 AND Recycle=0")
			If Not(Fn_Rs.Eof) Then
				Fn_Check      = True
				Fn_AdsGroupID = KnifeCMS.Data.CLng(Fn_Rs(0))
				Fn_Width      = Fn_Rs(1)
				Fn_Height     = Fn_Rs(2)
				Fn_StartTime  = Fn_Rs(3)
				Fn_EndTime    = Fn_Rs(4)
			Else
				Fn_Check      = False
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
			'判断广告位存在并有效
			If Not(Fn_AdsGroupID>0) Then Fn_Check = False
			If Not(KnifeCMS.Data.IsNul(Fn_StartTime)) Then : If DateDiff("d",Fn_StartTime,SysTime) < 0 Then Fn_Check = False
			If Not(KnifeCMS.Data.IsNul(Fn_EndTime))   Then : If DateDiff("d",SysTime,Fn_EndTime)   < 0 Then Fn_Check = False
			'广告位有效则进行下面的操作
            If Fn_Check Then
				
				RegExpObj.Pattern = LabelRuleField
				Set MatchesField  = RegExpObj.Execute(LoopStringList)
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT AdsTitle,AdsUrl,AdsTxt,ImageConfig FROM ["&DBTable_Ads_Detail&"] WHERE AdsGroupID="&Fn_AdsGroupID&" ORDER BY OrderNum ASC")
				If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then DataArray = Fn_Rs.GetRows() Else DataArray = ""
				KnifeCMS.DB.CloseRs Fn_Rs
				If IsArray(DataArray) Then DataNum=Ubound(DataArray,2) Else DataNum=-1
				GlobalPreString = KnifeCMS.Data.ReplaceString(GlobalPreString,"[ad:height]",Fn_Height)
				GlobalPreString = KnifeCMS.Data.ReplaceString(GlobalPreString,"[ad:width]",Fn_Width)
				GlobalSufString = KnifeCMS.Data.ReplaceString(GlobalSufString,"[ad:height]",Fn_Height)
				GlobalSufString = KnifeCMS.Data.ReplaceString(GlobalSufString,"[ad:width]",Fn_Width)
				LoopStringList  = KnifeCMS.Data.ReplaceString(LoopStringList,"[ad:height]",Fn_Height)
				LoopStringList  = KnifeCMS.Data.ReplaceString(LoopStringList,"[ad:width]",Fn_Width)
				LoopStringTotal = GlobalPreString
				'{ad:loop}
				For Fn_i=0 To DataNum
					LoopStringListNew = LoopStringList
					For Each MatchField in MatchesField
						FieldNameAndAttr=KnifeCMS.Data.RegReplace(MatchField.SubMatches(0),"[\s]+",chr(32))
						FieldNameAndAttr=KnifeCMS.Data.TrimOuter(FieldNameAndAttr)
						Fn_Len=Instr(FieldNameAndAttr,chr(32))
						If Fn_Len > 0 Then 
							FieldName = Left(FieldNameAndAttr,Fn_Len-1)
							FieldAttr =	Right(FieldNameAndAttr,Len(FieldNameAndAttr) - Fn_Len)
						Else
							FieldName = FieldNameAndAttr
							FieldAttr =	""
						End If
						Select Case FieldName
							Case "i"        : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_i+1)
							Case "title"    : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(0,Fn_i))
							Case "url","link" : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(1,Fn_i))
							Case "abstract"
									Fn_TempString = KnifeCMS.Data.HTMLDecode(DataArray(2,Fn_i))
									Fn_AbsLen = KnifeCMS.Data.CLng(ParseAttr(FieldAttr)("len"))
									If Fn_AbsLen>0 Then
										Fn_TempString=Left(Fn_TempString,Fn_AbsLen)&"..."
									End If
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
							Case "imageurl" : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(3,Fn_i))
						End Select
						StrDictionary.removeAll
					Next
					LoopStringTotal = LoopStringTotal & LoopStringListNew
				Next
				'{/ad:loop}
				Set MatchesField = Nothing
				LoopStringTotal = LoopStringTotal & GlobalSufString
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,LoopStringTotal)
				StrDictionary.removeAll
			Else
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,"")
				StrDictionary.removeAll
            End If
		Next
		Set Matches = Nothing
	End Function
	
	'$:导航模板
	Public Function ParseNavlist(ByVal BV_String)
		If Not(KnifeCMS.IsStringExist(Pv_Content,"{kfc:"&BV_String&"navlist")) Then Exit Function
		Dim Fn_i,Fn_CNum,Fn_Len,Fn_MenuType,Fn_ParentID
		Dim Fn_Check : Fn_Check = True
		LabelRule      = "{kfc:"&BV_String&"navlist([\s\S]*?)}([\s\S]*?){"&BV_String&"navlist:loop}([\s\S]*?){/"&BV_String&"navlist:loop}([\s\S]*?){/kfc:"&BV_String&"navlist}"
		LabelRuleField = "\["&BV_String&"navlist:([\s\S]+?)\]"
		RegExpObj.Pattern = LabelRule
		Set Matches = RegExpObj.Execute(Pv_Content)
		For Each Match in Matches
		    LabelAttrList   = Match.SubMatches(0)
			GlobalPreString = Match.SubMatches(1)
			LoopStringList  = Match.SubMatches(2)
			GlobalSufString = Match.SubMatches(3)
			Set AttrDictionary=ParseAttr(LabelAttrList)
			Fn_MenuType=AttrDictionary("type")
			Select Case Fn_MenuType
				Case "top"    : Fn_MenuType=1
				Case "main"   : Fn_MenuType=2
				Case "bottomcate" : Fn_MenuType=3
				Case "bottom" : Fn_MenuType=4
				Case Else     : Fn_MenuType=0
			End Select
			Fn_ParentID=KnifeCMS.Data.CLng(AttrDictionary("parentid"))
			Set AttrDictionary=Nothing
			If Not(IsArray(Pv_MenuArray)) Then Fn_Check = False
            If Fn_Check Then
				RegExpObj.Pattern = LabelRuleField
				Set MatchesField  = RegExpObj.Execute(LoopStringList)
				DataArray = Pv_MenuArray
				If IsArray(DataArray) Then DataNum=Ubound(DataArray,2) Else DataNum=-1
				LoopStringTotal = ""
				Fn_CNum  = 0
				'{ad:loop}
				For Fn_i=0 To DataNum
					If Fn_ParentID=DataArray(4,Fn_i) And Fn_MenuType=DataArray(5,Fn_i) Then
					    Fn_CNum  = Fn_CNum + 1
						LoopStringListNew = LoopStringList
						For Each MatchField in MatchesField
							FieldNameAndAttr=KnifeCMS.Data.RegReplace(MatchField.SubMatches(0),"[\s]+",chr(32))
							FieldNameAndAttr=KnifeCMS.Data.TrimOuter(FieldNameAndAttr)
							Fn_Len=Instr(FieldNameAndAttr,chr(32))
							If Fn_Len > 0 Then 
								FieldName = Left(FieldNameAndAttr,Fn_Len-1)
								FieldAttr =	Right(FieldNameAndAttr,Len(FieldNameAndAttr) - Fn_Len)
							Else
								FieldName = FieldNameAndAttr
								FieldAttr =	""
							End If ''ID,NavName,NavUrl,Target,P_NavID,NavType
							Select Case FieldName
								Case "i"        : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_CNum)
								Case "id"       : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(0,Fn_i))
								Case "name"     : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,KnifeCMS.Data.HTMLDecode(DataArray(1,Fn_i)))
								Case "url"      : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(2,Fn_i))
								Case "target"   : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(3,Fn_i))
							End Select
							StrDictionary.removeAll
						Next
						LoopStringTotal = LoopStringTotal & LoopStringListNew
					End If
					
				Next
				'{/ad:loop}
				Set MatchesField = Nothing
				If Fn_CNum > 0 Then LoopStringTotal = GlobalPreString & LoopStringTotal & GlobalSufString
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,LoopStringTotal)
				StrDictionary.removeAll
			Else
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,"")
				StrDictionary.removeAll
            End If
		Next
		Set Matches = Nothing
		If KnifeCMS.IsStringExist(Pv_Content,"{kfc:sub1navlist") then ParseNavlist("sub1") Else Exit Function
		If KnifeCMS.IsStringExist(Pv_Content,"{kfc:sub2navlist") then ParseNavlist("sub2") Else Exit Function
	End Function
	
	'$:分类模板
	Public Function ParseCategory(ByVal BV_String)
		If Not(KnifeCMS.IsStringExist(Pv_Content,"{kfc:"& BV_String &"category")) Then Exit Function
		Dim Fn_i,Fn_j,Fn_CNum,Fn_Len,Fn_CateType,Fn_ParentID,Fn_ID,Fn_IDArray,Fn_URL,Fn_ShowNum,Fn_TempString
		Dim Fn_TempIsOK,Fn_IsOK
		Dim Fn_Check : Fn_Check = True
		LabelRule      = "{kfc:"&BV_String&"category([\s\S]*?)}([\s\S]*?){"&BV_String&"category:loop}([\s\S]*?){/"&BV_String&"category:loop}([\s\S]*?){/kfc:"&BV_String&"category}"
		LabelRuleField = "\["&BV_String&"category:([\s\S]+?)\]"
		RegExpObj.Pattern = LabelRule
		Set Matches = RegExpObj.Execute(Pv_Content)
		For Each Match in Matches
		    LabelAttrList   = Match.SubMatches(0)
			GlobalPreString = Match.SubMatches(1)
			LoopStringList  = Match.SubMatches(2)
			GlobalSufString = Match.SubMatches(3)
			Pv_ContentCateArray= ""
			Set AttrDictionary = ParseAttr(LabelAttrList)
			Fn_CateType = AttrDictionary("type")
			Fn_CateType = KnifeCMS.IIF(Fn_CateType="get[]","content_"& SubSystem,Fn_CateType) : Fn_CateType = KnifeCMS.Data.RegReplace(Fn_CateType,"[^a-zA-Z_]","")
			Fn_ParentID = AttrDictionary("parentid") : Fn_ParentID = KnifeCMS.IIF(Fn_ParentID="get[id]",ID,Fn_ParentID)
			Fn_ParentID = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_ParentID),0,KnifeCMS.Data.CLng(Fn_ParentID))
			Fn_ID       = AttrDictionary("id") : Fn_ID = KnifeCMS.IIF(Fn_ID="get[id]",ID,Fn_ID)
			Fn_ID       = KnifeCMS.Data.ParseID(Fn_ID) : Fn_ID = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_ID),-1,Fn_ID)
			Fn_ShowNum  = AttrDictionary("shownum") : Fn_ShowNum = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_ShowNum),2000,KnifeCMS.Data.CLng(Fn_ShowNum))
			Set AttrDictionary=Nothing
			Select Case Fn_CateType
				Case "goods"
					DataArray = Pv_GoodsCateArray
					Fn_URL    = SiteURL & SystemPath &"?shop/category/"
				Case "vote"
					DataArray = Pv_VoteCateArray
					Fn_URL    = SiteURL & SystemPath &"?vote/category/"
				Case Else
					If Left(Fn_CateType,7)="content" Then
						If Not(IsArray(Pv_ContentCateArray)) Then Pv_ContentCateArray = GetContentCateArray(Mid(Fn_CateType,9))
					End If
					DataArray = Pv_ContentCateArray
					Fn_URL    = SiteURL & SystemPath &"?"& Replace(Fn_CateType,"_","/") &"/category/"
			End Select
			If Not(IsArray(DataArray)) Then Fn_Check = False
            If Fn_Check Then
				RegExpObj.Pattern = LabelRuleField
				Set MatchesField  = RegExpObj.Execute(LoopStringList)
				If IsArray(DataArray) Then DataNum=Ubound(DataArray,2) Else DataNum=-1
				LoopStringTotal = ""
				Fn_CNum         = 0
				'*计算条件
				If Fn_ID<>-1 Then
					Fn_IDArray = Split(Fn_ID,",")
				End If
				'*计算条件:end
				
				For Fn_i=0 To DataNum
					'*计算条件
					Fn_IsOK=False
					If Fn_ParentID>=0 And Fn_ID<>-1 Then
						Fn_TempIsOK=False
						For Fn_j=0 To Ubound(Fn_IDArray)
							If Trim(Fn_IDArray(Fn_j))=Trim(DataArray(0,Fn_i)) Then Fn_TempIsOK=True : Exit For
						Next
						'If Fn_TempIsOK=True And Trim(Fn_ParentID)=Trim(DataArray(1,Fn_i)) Then Fn_IsOK=True
						If Fn_TempIsOK=True Then
							If Fn_ParentID>0 Then
								If Fn_ParentID=Trim(DataArray(1,Fn_i)) Then Fn_IsOK=True
							Else
								Fn_IsOK=True
							End If
						End If
					ElseIf Fn_ParentID>=0 And Fn_ID=-1 Then
						If Fn_ParentID=DataArray(1,Fn_i) Then Fn_IsOK=True
					Else
						Fn_IsOK=True
					End If
					'*计算条件:end
					
					If Fn_IsOK Then
					    Fn_CNum = Fn_CNum + 1
						LoopStringListNew = LoopStringList
						For Each MatchField in MatchesField
							FieldNameAndAttr=KnifeCMS.Data.RegReplace(MatchField.SubMatches(0),"[\s]+",chr(32))
							FieldNameAndAttr=KnifeCMS.Data.TrimOuter(FieldNameAndAttr)
							Fn_Len=Instr(FieldNameAndAttr,chr(32))
							If Fn_Len > 0 Then 
								FieldName = Left(FieldNameAndAttr,Fn_Len-1)
								FieldAttr =	Right(FieldNameAndAttr,Len(FieldNameAndAttr) - Fn_Len)
							Else
								FieldName = FieldNameAndAttr
								FieldAttr =	""
							End If
							
							Select Case FieldName
								Case "i"        : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_CNum)
								Case "id"       : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(0,Fn_i))
								Case "name"     : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,KnifeCMS.Data.HTMLDecode(DataArray(2,Fn_i)))
								Case "url"
									Pv_TempString = Fn_URL & DataArray(0,Fn_i) &"/"& DataArray(3,Fn_i) & FileSuffix
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Pv_TempString)
								Case "ispagecontent"
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,KnifeCMS.Data.CLng(DataArray(4,Fn_i)))
								Case "pagecontent"
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,KnifeCMS.Data.HTMLDecode(KnifeCMS.Data.ReplaceString(DataArray(5,Fn_i)," ","")))
								Case "icon"
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,KnifeCMS.Data.URLDecode(DataArray(6,Fn_i)))
								Case "pic"
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,KnifeCMS.Data.URLDecode(DataArray(7,Fn_i)))
							End Select
							StrDictionary.removeAll
						Next
						LoopStringTotal = LoopStringTotal & LoopStringListNew
						If Fn_CNum >= Fn_ShowNum Then Exit For
					End If
				Next
				Set MatchesField = Nothing
				If Fn_CNum > 0 Then LoopStringTotal = GlobalPreString & LoopStringTotal & GlobalSufString
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,LoopStringTotal)
				StrDictionary.removeAll
			Else
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,"")
				StrDictionary.removeAll
            End If
		Next
		Set Matches = Nothing
		If KnifeCMS.IsStringExist(Pv_Content,"{kfc:sub1category") then ParseCategory("sub1") Else Exit Function
		If KnifeCMS.IsStringExist(Pv_Content,"{kfc:sub2category") then ParseCategory("sub2") Else Exit Function
	End Function
	
	'$:文章列表
	Public Function ParseContentList()
		If Not(KnifeCMS.IsStringExist(Pv_Content,"{kfc:contentlist")) Then Exit Function
		Dim Fn_Rs,Fn_i,Fn_j,Fn_CNum,Fn_Len,Fn_CateType,Fn_GetMateInfo,Fn_ShowNum,Fn_CategoryID,Fn_Condition,Fn_Orderby
		Dim Fn_ID,Fn_TempID,Fn_TempCateID,Fn_KeyWords,Fn_TempString,Fn_IsTopLine,Fn_IsOntop,Fn_IsHot,Fn_IsElite,Fn_IsThumbnail,Fn_IsSlidesimgs,Fn_Color
		Dim Fn_AbsLen,Fn_TimeStyle,Fn_Month_Day
		Dim Fn_Sql,Fn_SqlTop,Fn_SqlC,Fn_SqlOrderBy
		Dim Fn_Match,Fn_Matches
		Dim Fn_IsPage,Fn_PageSize,Fn_Page,Fn_PageMatche,Fn_PageMatches,Fn_PageString,Fn_PageUrl,Fn_PageCategoryID,Fn_PageCategoryString,Fn_SeoUrl
		Dim Fn_GetMatche,Fn_GetMatches
		Dim Fn_Check
		LabelRule      = "{kfc:contentlist([\s\S]*?)}([^\""][\s\S]*?){contentlist:loop}([\s\S]*?){/contentlist:loop}([\s\S]*?){/kfc:contentlist}"
		LabelRuleField = "\[contentlist:([\s\S]+?)\]"
		RegExpObj.Pattern = LabelRule
		Set Matches = RegExpObj.Execute(Pv_Content)
		
		For Each Match In Matches
			Fn_Check = True
			Fn_PageCategoryID = 0
			Fn_SeoUrl       = ""
			LabelAttrList   = Match.SubMatches(0)
			GlobalPreString = Match.SubMatches(1)
			LoopStringList  = Match.SubMatches(2)
			GlobalSufString = Match.SubMatches(3)
			Set AttrDictionary=ParseAttr(LabelAttrList)
			Fn_CateType   = AttrDictionary("type") : Fn_CateType   = KnifeCMS.IIF(Fn_CateType="get[]",SubSystem,Fn_CateType)
			Fn_CateType   = KnifeCMS.Data.RegReplace(Fn_CateType,"[^a-zA-Z]","")
			Fn_Condition  = AttrDictionary("condition")
			Fn_Orderby    = AttrDictionary("orderby")
			Fn_GetMateInfo= KnifeCMS.Data.Cbool(AttrDictionary("getmetainfo"))
			Fn_ShowNum    = AttrDictionary("shownum") : Fn_ShowNum = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_ShowNum),-1,KnifeCMS.Data.CLng(Fn_ShowNum))
			Fn_PageSize   = KnifeCMS.Data.CLng(AttrDictionary("pagesize")) : Fn_PageSize = KnifeCMS.IIF(Fn_PageSize>0 And Fn_PageSize<200,Fn_PageSize,100)
			Set AttrDictionary=Nothing
			If KnifeCMS.Data.RegTest(Fn_Condition,"get\[([\s\S]+?)\]") Then
				Set Fn_GetMatches = KnifeCMS.Data.RegMatch(Fn_Condition,"get\[([\s\S]+?)\]")
				If Fn_GetMatches.Count > 0 Then
					For Each Fn_GetMatche In Fn_GetMatches
						Fn_Condition = KnifeCMS.Data.ReplaceString(Fn_Condition,Fn_GetMatche.value,KnifeCMS.GetForm("both",Fn_GetMatche.SubMatches(0)))'"get\[(\s\S)\]")
					Next
				End If
			End If
			Fn_Condition = KnifeCMS.Data.RegReplace(Fn_Condition,"get\[(\s\S)\]",KnifeCMS.GetForm("both",""))
			If KnifeCMS.Data.RegTest(Match.Value,"\[contentlist:pages\]") Then Fn_IsPage = True
			If IsContentSubSystemExist(Fn_CateType) Then
				If Fn_ShowNum >=0 Then Fn_SqlTop = " TOP "& Fn_ShowNum &" "
				Set Fn_Matches = KnifeCMS.Data.RegMatch(Fn_Condition,"""([\s\S]+?)"":""([\s\S]*?)""")
				Fn_SqlC = ""
				For Each Fn_Match In Fn_Matches
					Select Case Fn_Match.SubMatches(0)
						Case "id"
							Fn_ID = KnifeCMS.Data.ParseID(Fn_Match.SubMatches(1))
							If KnifeCMS.Data.IsNul(Fn_ID) Then
								Fn_SqlC = Fn_SqlC &" AND a.ID IN (0) "
							Else
								Fn_SqlC = Fn_SqlC &" AND a.ID IN ("& Fn_ID &") "
							End If
						Case "categoryid"
							Fn_CategoryID = ""
							Fn_TempID = Split(Fn_Match.SubMatches(1),",")
							For Fn_j=0 To Ubound(Fn_TempID)
								Fn_TempCateID = Fn_TempID(Fn_j)
								If InStr(Fn_TempCateID,"[getsub]")>0 Then
									Fn_TempCateID = KnifeCMS.Data.ReplaceString(Fn_TempCateID,"[getsub]","")
									Fn_TempCateID = KnifeCMS.Data.RegReplace(Fn_TempCateID,"[^0-9]","")
									If Not(KnifeCMS.Data.IsNul(Fn_TempCateID)) Then
										Fn_TempCateID = Fn_TempCateID &","& GetChildClass(TablePre&"Content_"& Fn_CateType &"_Class",Fn_TempCateID)
										Fn_TempCateID = Join(KnifeCMS.Data.ClearSameDatas(Split(Fn_TempCateID,","),false),",")
									End If
								Else
									Fn_TempCateID = KnifeCMS.Data.RegReplace(Fn_TempCateID,"[^0-9]","")
								End If
								Fn_CategoryID = Fn_CategoryID &","& Fn_TempCateID
							Next
							If Not(KnifeCMS.Data.IsNul(Fn_CategoryID)) Then Fn_CategoryID = Join(KnifeCMS.Data.ClearSameDatas(Split(Fn_CategoryID,","),false),",")
							Fn_CategoryID = KnifeCMS.Data.ReplaceString(Fn_CategoryID,"$err$","")
							Fn_CategoryID = KnifeCMS.Data.ParseID(Fn_CategoryID)
							If Not(KnifeCMS.Data.IsNul(Fn_CategoryID)) Then
								Fn_SqlC = Fn_SqlC &" AND a.ClassID IN ("& Fn_CategoryID &") "
							End If
						Case "keywords"
							Fn_KeyWords = KnifeCMS.Data.HtmlEncode(Fn_Match.SubMatches(1))
							If Not(KnifeCMS.Data.IsNul(Fn_KeyWords)) Then Fn_SqlC = Fn_SqlC &" AND a.Title LIKE '%"& Fn_KeyWords &"%' "
						Case "topline"
							Fn_IsTopLine = Fn_Match.SubMatches(1)
							If Fn_IsTopLine = "yes" Then Fn_SqlC = Fn_SqlC &" AND a.TopLine=1 "
							If Fn_IsTopLine = "no"  Then Fn_SqlC = Fn_SqlC &" AND a.TopLine=0 "
						Case "ontop"
							Fn_IsOntop = Fn_Match.SubMatches(1)
							If Fn_IsOntop = "yes" Then Fn_SqlC = Fn_SqlC &" AND a.OnTop=1 "
							If Fn_IsOntop = "no"  Then Fn_SqlC = Fn_SqlC &" AND a.OnTop=0 "
						Case "hot"
							Fn_IsHot = Fn_Match.SubMatches(1)
							If Fn_IsHot = "yes" Then Fn_SqlC = Fn_SqlC &" AND a.Hot=1 "
							If Fn_IsHot = "no"  Then Fn_SqlC = Fn_SqlC &" AND a.Hot=0 "
						Case "elite"
							Fn_IsElite = Fn_Match.SubMatches(1)
							If Fn_IsElite = "yes" Then Fn_SqlC = Fn_SqlC &" AND a.Elite=1 "
							If Fn_IsElite = "no"  Then Fn_SqlC = Fn_SqlC &" AND a.Elite=0 "
						Case "thumbnail"
							Fn_IsThumbnail = Fn_Match.SubMatches(1)
							If Fn_IsThumbnail = "yes" Then Fn_SqlC = Fn_SqlC &" AND ( Len(a.Thumbnail)>5 Or  Len(a.SlidesFirstImg)>5 ) "
							If Fn_IsThumbnail = "no"  Then Fn_SqlC = Fn_SqlC &" AND ( Len(a.Thumbnail)<5 And Len(a.SlidesFirstImg)<5 ) "
						Case "slidesimgs"
							Fn_IsSlidesimgs = Fn_Match.SubMatches(1)
							If Fn_IsSlidesimgs = "yes" Then Fn_SqlC = Fn_SqlC &" AND Len(a.SlidesImgs)>5 "
							If Fn_IsSlidesimgs = "no"  Then Fn_SqlC = Fn_SqlC &" AND Len(a.SlidesImgs)<5 "
					End Select
				Next
				Set Fn_Matches = KnifeCMS.Data.RegMatch(Fn_Orderby,"""([\s\S]+?)"":""([\s\S]+?)""")
				If Fn_Matches.Count>0 Then
					Fn_SqlOrderBy = " ORDER BY "
					For Each Fn_Match In Fn_Matches
						Select Case Lcase(Fn_Match.SubMatches(0))
							Case "id"
								Fn_TempString = " a.ID "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
							Case "publictime"
								Fn_TempString = " a.PublicTime "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
							Case "hits"
								Fn_TempString = " a.Hits "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
							Case "diggs"
								Fn_TempString = " a.Diggs "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
							Case "topline"'头条
								Fn_TempString = " a.TopLine "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
							Case "ontop"'固顶
								Fn_TempString = " a.OnTop "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
							Case "hot"'热门
								Fn_TempString = " a.Hot "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
							Case "elite"'精华
								Fn_TempString = " a.Elite "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
						End Select
					Next
				End if
				'取得自定义字段
				Pv_Field = ""
				Pv_Field = KnifeCMS.DB.GetField(TablePre &"Content_"& Fn_CateType,"a")
				If Pv_Field<>"" Then
					Pv_TempString = Replace(Pv_Field,"a.","")
					Pv_TempArray  = Split(Pv_TempString,",")
					Pv_Field      = ","& Pv_Field
				Else
					Pv_TempString = ""
					Pv_TempArray  = ""
				End If
				Select Case DB_Type
				Case 0
				Fn_Sql="SELECT "& Fn_SqlTop &" a.ID,a.Title,a.TitleFontColor,a.TitleFontType,a.[From],a.Author,a.PublicTime,IIF( LEN(a.Thumbnail)=0,a.SlidesFirstImg,a.Thumbnail),a.UseLinkUrl,a.LinkUrl,a.Abstract,a.Hits,a.Comments,a.TopLine,a.OnTop,a.Hot,a.Elite,a.CommentSet,a.Openness,a.AddTime,a.EditTime,a.Diggs,a.SeoUrl,a.ClassID,b.ClassName"& Pv_Field &" FROM ["& TablePre &"Content_"& Fn_CateType &"] a LEFT JOIN ["& TablePre &"Content_"& Fn_CateType &"_Class] b ON (b.ID=a.ClassID) WHERE a.Recycle=0 AND a.Status=1 "& Fn_SqlC &" "& Fn_SqlOrderBy &""
				Case 1
				Fn_Sql="SELECT "& Fn_SqlTop &" a.ID,a.Title,a.TitleFontColor,a.TitleFontType,a.[From],a.Author,a.PublicTime,CASE WHEN LEN(a.Thumbnail)=0 THEN a.SlidesFirstImg ELSE a.Thumbnail END,a.UseLinkUrl,a.LinkUrl,a.Abstract,a.Hits,a.Comments,a.TopLine,a.OnTop,a.Hot,a.Elite,a.CommentSet,a.Openness,a.AddTime,a.EditTime,a.Diggs,a.SeoUrl,a.ClassID,b.ClassName"& Pv_Field &" FROM ["& TablePre &"Content_"& Fn_CateType &"] a LEFT JOIN ["& TablePre &"Content_"& Fn_CateType &"_Class] b ON (b.ID=a.ClassID) WHERE a.Recycle=0 AND a.Status=1 "& Fn_SqlC &" "& Fn_SqlOrderBy &""
				End Select
				'echo Fn_Sql
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
				If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then
					Dim Fn_Temp_i,Fn_Temp_j,Fn_Temp_k,Fn_a,Fn_RsCount,Fn_PageCount,Fn_DataArray
					Fn_RsCount     = Fn_Rs.RecordCount : If Fn_Rs.RecordCount<1 then Fn_RsCount=0
					Fn_Rs.PageSize = Fn_PageSize
					Fn_PageCount   = Fn_Rs.PageCount
					Fn_Page        = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","page")) : Fn_Page = KnifeCMS.IIF(Fn_Page>0,Fn_Page,1)
					'Die KnifeCMS.URLQueryString
					if Fn_Page > Fn_PageCount then
						Fn_Rs.AbsolutePage = Fn_PageCount
					else
						Fn_Rs.AbsolutePage = Fn_Page
					end if
					Fn_Page = Fn_Rs.AbsolutePage
					
					ReDim DataArray(Fn_Rs.Fields.Count,Fn_PageSize)
					For Fn_Temp_i=0 to Fn_PageSize-1
						If Fn_Rs.EOF Then Exit For
						for Fn_Temp_j=0 to Fn_Rs.Fields.Count-1
							DataArray(Fn_Temp_j,Fn_Temp_i)=Fn_Rs(Fn_Temp_j)
						next
						Fn_Rs.movenext
					Next
					
					'**生成分页的地址**
					Fn_PageCategoryID = ID
					If KnifeCMS.Data.CLng(Fn_PageCategoryID)>0 Then
						Fn_PageCategoryString = "content/"& Fn_CateType &"/category/"& Fn_PageCategoryID &"_{tpl:page}/index"& FileSuffix
						'content/news/category/10_2/seourl.htm
					Else
						Fn_PageCategoryString = "content/"& Fn_CateType &"/index_page{tpl:page}"& FileSuffix
						'content/news/index_page2.htm
					End If
					
					If RunMode = "dynamic" Then
						Fn_PageUrl = SiteURL & SystemPath &"?"& Fn_PageCategoryString
					Else
						Fn_PageUrl = SiteURL & SystemPath & Fn_PageCategoryString
					End If
					'****
					
					Fn_PageString = "<div class=pagestyle><span class=pages>"& Replace(Lang_Pages(0),"{tpl:num}",Fn_RsCount) &"</span>"
					If Fn_Page>1 Then Fn_PageString = Fn_PageString &"<a href="""& Replace(Fn_PageUrl,"{tpl:page}",Fn_Page-1) &""">"& Lang_Pages(1) &"</a>"
					if Fn_Page>1 then Fn_a=1 : if Fn_Page>2 then Fn_a=2 : if Fn_Page>3 then Fn_a=3 : if Fn_Page>4 then Fn_a=4 : if Fn_Page>5 then Fn_a=5 : if Fn_Page>6 then Fn_a=6 : if Fn_Page>7 then Fn_a=7 : if Fn_Page>8 then Fn_a=8 : if Fn_Page>9 then Fn_a=9
					for Fn_Temp_i=Fn_Page-Fn_a to Fn_Page-1
						Fn_PageString = Fn_PageString & "<a href="""& Replace(Fn_PageUrl,"{tpl:page}",Fn_Temp_i)&""">"&Fn_Temp_i&"</a>"
					next
					Fn_PageString = Fn_PageString &"<span class=current>"&Fn_Page&"</span>"
					
					for Fn_Temp_i=Fn_Page+1 to Fn_Page+9
						if Fn_Temp_i > Fn_PageCount then Exit for
						Fn_PageString = Fn_PageString & "<a href="""& Replace(Fn_PageUrl,"{tpl:page}",Fn_Temp_i) &""">"&Fn_Temp_i&"</a>"
					next
					
					if Not(Fn_Page>=Fn_PageCount) then Fn_PageString = Fn_PageString & "<a href="""& Replace(Fn_PageUrl,"{tpl:page}",Fn_Page+1) &""">"& Lang_Pages(2) &"</a>"
					Fn_PageString = Fn_PageString & "<span class=pages>"& Replace(Lang_Pages(3),"{tpl:num}",Fn_PageCount) &"</span><span class=pages>"& Replace(Lang_Pages(4),"{tpl:num}",Fn_PageSize) &"</span></div>"
				Else
					DataArray=""
				End If
				KnifeCMS.DB.CloseRs Fn_Rs
			Else
				DataArray=""
			End If
			If Not(IsArray(DataArray)) Then Fn_Check = False
            If Fn_Check Then
				GlobalPreString = KnifeCMS.Data.ReplaceString(GlobalPreString,"[contentlist:pages]",Fn_PageString)
				GlobalSufString = KnifeCMS.Data.ReplaceString(GlobalSufString,"[contentlist:pages]",Fn_PageString)
				RegExpObj.Pattern = LabelRuleField
				Set MatchesField  = RegExpObj.Execute(LoopStringList)
				If IsArray(DataArray) Then DataNum=Ubound(DataArray,2) Else DataNum=-1
				LoopStringTotal = ""
				Fn_CNum  = 0
				'{ad:loop}
				For Fn_i=0 To DataNum
					    If KnifeCMS.Data.IsNul(DataArray(0,Fn_i)) Then Exit For
						Fn_CNum  = Fn_CNum + 1
						LoopStringListNew = LoopStringList
						For Each MatchField in MatchesField
							
							FieldNameAndAttr=KnifeCMS.Data.RegReplace(MatchField.SubMatches(0),"[\s]+",chr(32))
							FieldNameAndAttr=KnifeCMS.Data.TrimOuter(FieldNameAndAttr)
							Fn_Len=Instr(FieldNameAndAttr,chr(32))
							If Fn_Len > 0 Then 
								FieldName = Left(FieldNameAndAttr,Fn_Len-1)
								FieldAttr =	Right(FieldNameAndAttr,Len(FieldNameAndAttr) - Fn_Len)
							Else
								FieldName = FieldNameAndAttr
								FieldAttr =	""
							End If
							Select Case FieldName
								Case "i"             : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_CNum)
								Case "id"            : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(0,Fn_i))
								Case "url"
									If KnifeCMS.Data.CLng(DataArray(8,Fn_i))>0 Then
										Fn_TempString = KnifeCMS.Data.URLDecode(DataArray(9,Fn_i))
									Else
										Fn_TempString = SiteURL & SystemPath &"?content/"& Fn_CateType &"/"& DataArray(0,Fn_i) &"/"& KnifeCMS.Data.URLDecode(DataArray(22,Fn_i)) & FileSuffix
									End If
									LoopStringListNew = KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "title"
									Fn_TempString    = KnifeCMS.Data.HTMLDecode(DataArray(1,Fn_i))
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "titlecolor"
									Fn_TempString    = KnifeCMS.Data.ParseRs(DataArray(2,Fn_i))
									If Fn_TempString<>"" Then Fn_TempString = "color:"& Fn_TempString &";"
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "from"
									Fn_TempString    = KnifeCMS.Data.HTMLDecode(DataArray(4,Fn_i))
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "author"
									Fn_TempString    = KnifeCMS.Data.HTMLDecode(DataArray(5,Fn_i))
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "publictime"
									Fn_TimeStyle  = ParseAttr(FieldAttr)("style")
									Fn_TempString = DataArray(6,Fn_i)
									If Fn_TimeStyle<>"" Then Fn_TempString = KnifeCMS.Data.FormatDateTime(Fn_TempString,Fn_TimeStyle)
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "thumbnail"
									Fn_TempString    = KnifeCMS.Data.URLDecode(DataArray(7,Fn_i))
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "abstract"
									Fn_TempString = KnifeCMS.Data.HTMLDecode(DataArray(10,Fn_i))
									Fn_AbsLen = KnifeCMS.Data.CLng(ParseAttr(FieldAttr)("len")) : If Fn_AbsLen=0 Then Fn_AbsLen = 250
									If Len(Fn_TempString) > Fn_AbsLen Then Fn_TempString=KnifeCMS.Data.Left(Fn_TempString,Fn_AbsLen)&"..."
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "hits"           : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(11,Fn_i))
								Case "comments"       : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(12,Fn_i))
								Case "categoryid"     : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(23,Fn_i))
								Case "categoryname"
									Fn_TempString    = KnifeCMS.Data.HTMLDecode(DataArray(24,Fn_i))
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
							End Select
							'自定义字段
							If Pv_Field<>"" Then
								For Fn_j=0 To Ubound(Pv_TempArray)
									If LCase(FieldName)=LCase(Pv_TempArray(Fn_j)) Then
										Fn_TempString    = KnifeCMS.Data.HTMLDecode(DataArray(24+Fn_j+1,Fn_i))
										LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
									End If
								Next
							End If
							StrDictionary.removeAll
						Next
						LoopStringTotal = LoopStringTotal & LoopStringListNew
				Next
				'{/ad:loop}
				Set MatchesField = Nothing
				If Fn_CNum > 0 Then LoopStringTotal = GlobalPreString & LoopStringTotal & GlobalSufString
				LoopStringTotal = ParseSubIf(LoopStringTotal)
				LoopStringTotal = ParseComIf(LoopStringTotal)
				LoopStringTotal = ParseIf(LoopStringTotal)
				If LoopStringTotal="" Then LoopStringTotal = "没有数据..."
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,LoopStringTotal)
				StrDictionary.removeAll
			Else
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,"")
				StrDictionary.removeAll
            End If
		Next
		Set Matches = Nothing
	End Function
	
	'$:投票列表
	Public Function ParseVoteList()
		If Not(KnifeCMS.IsStringExist(Pv_Content,"{kfc:votelist")) Then Exit Function
		Dim Fn_Rs,Fn_i,Fn_j,Fn_CNum,Fn_Len,Fn_CateType,Fn_ShowNum,Fn_CategoryID,Fn_Condition,Fn_Orderby
		Dim Fn_ID,Fn_TempID,Fn_TempCateID,Fn_KeyWords,Fn_TempString,Fn_IsTopLine,Fn_IsOntop,Fn_IsHot,Fn_IsElite,Fn_IsThumbnail,Fn_IsSlidesimgs
		Dim Fn_AbsLen,Fn_TimeStyle,Fn_Month_Day
		Dim Fn_Sql,Fn_SqlTop,Fn_SqlC,Fn_SqlOrderBy
		Dim Fn_Match,Fn_Matches
		Dim Fn_IsPage,Fn_PageSize,Fn_Page,Fn_PageMatche,Fn_PageMatches,Fn_PageString,Fn_PageUrl,Fn_PageCategoryID,Fn_PageCategoryString,Fn_SeoUrl
		Dim Fn_GetMatche,Fn_GetMatches
		Dim Fn_Check
		LabelRule      = "{kfc:votelist([\s\S]*?)}([^\""][\s\S]*?){votelist:loop}([\s\S]*?){/votelist:loop}([\s\S]*?){/kfc:votelist}"
		LabelRuleField = "\[votelist:([\s\S]+?)\]"
		RegExpObj.Pattern = LabelRule
		Set Matches = RegExpObj.Execute(Pv_Content)
		For Each Match In Matches
		If CloseModuleVote<>1 Then
			Fn_Check = True
			LabelAttrList   = Match.SubMatches(0)
			GlobalPreString = Match.SubMatches(1)
			LoopStringList  = Match.SubMatches(2)
			GlobalSufString = Match.SubMatches(3)
			Set AttrDictionary=ParseAttr(LabelAttrList)
			Fn_Condition  = AttrDictionary("condition")
			Fn_Orderby    = AttrDictionary("orderby")
			Fn_ShowNum    = AttrDictionary("shownum") : Fn_ShowNum = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_ShowNum),-1,KnifeCMS.Data.CLng(Fn_ShowNum))
			Fn_PageSize   = KnifeCMS.Data.CLng(AttrDictionary("pagesize")) : Fn_PageSize = KnifeCMS.IIF(Fn_PageSize>0 And Fn_PageSize<200,Fn_PageSize,20)
			Set AttrDictionary=Nothing
			If KnifeCMS.Data.RegTest(Fn_Condition,"get\[([\s\S]+?)\]") Then
				Set Fn_GetMatches = KnifeCMS.Data.RegMatch(Fn_Condition,"get\[([\s\S]+?)\]")
				If Fn_GetMatches.Count > 0 Then
					For Each Fn_GetMatche In Fn_GetMatches
						Fn_Condition = KnifeCMS.Data.ReplaceString(Fn_Condition,Fn_GetMatche.value,KnifeCMS.GetForm("both",Fn_GetMatche.SubMatches(0)))'"get\[(\s\S)\]")
					Next
				End If
			End If
			Fn_Condition = KnifeCMS.Data.RegReplace(Fn_Condition,"get\[(\s\S)\]",KnifeCMS.GetForm("both",""))
			If KnifeCMS.Data.RegTest(Match.Value,"\[votelist:pages\]") Then Fn_IsPage = True
			If Fn_ShowNum >=0 Then Fn_SqlTop = " TOP "& Fn_ShowNum &" "
			'生成查询条件
			Set Fn_Matches = KnifeCMS.Data.RegMatch(Fn_Condition,"""([\s\S]+?)"":""([\s\S]*?)""")
			Fn_SqlC = ""
			For Each Fn_Match In Fn_Matches
				Select Case Fn_Match.SubMatches(0)
					Case "id"
						Fn_ID = KnifeCMS.Data.ParseID(Fn_Match.SubMatches(1))
						If Not(KnifeCMS.Data.IsNul(Fn_ID)) Then
							Fn_SqlC  = Fn_SqlC &" AND a.ID IN ("& Fn_ID &") "
						Else
							Fn_SqlC  = Fn_SqlC &" AND a.ID IN (0) "
							Fn_Check = False
						End If
					Case "categoryid"
						Fn_CategoryID = ""
						If Fn_Match.SubMatches(1)="0" Then
							Fn_SqlC = Fn_SqlC &" "
						Else
							Fn_TempID = Split(Fn_Match.SubMatches(1),",")
							For Fn_j=0 To Ubound(Fn_TempID)
								Fn_TempCateID = Fn_TempID(Fn_j)
								If InStr(Fn_TempCateID,"[getsub]")>0 Then
									Fn_TempCateID = KnifeCMS.Data.ReplaceString(Fn_TempCateID,"[getsub]","")
									Fn_TempCateID = KnifeCMS.Data.RegReplace(Fn_TempCateID,"[^0-9]","")
									If Not(KnifeCMS.Data.IsNul(Fn_TempCateID)) Then
										Fn_TempCateID = Fn_TempCateID &","& GetChildClass(DBTable_VoteClass,Fn_TempCateID)
										Fn_TempCateID = Join(KnifeCMS.Data.ClearSameDatas(Split(Fn_TempCateID,","),false),",")
									End If
								Else
									Fn_TempCateID = KnifeCMS.Data.RegReplace(Fn_TempCateID,"[^0-9]","")
								End If
								Fn_CategoryID = Fn_CategoryID &","& Fn_TempCateID
							Next
							If Not(KnifeCMS.Data.IsNul(Fn_CategoryID)) Then Fn_CategoryID = Join(KnifeCMS.Data.ClearSameDatas(Split(Fn_CategoryID,","),false),",")
							Fn_CategoryID = KnifeCMS.Data.ReplaceString(Fn_CategoryID,"$err$","")
							Fn_CategoryID = KnifeCMS.Data.ParseID(Fn_CategoryID)
							'If Not(KnifeCMS.Data.IsNul(Fn_CategoryID)) Then Fn_SqlC = Fn_SqlC &" AND a.ClassID IN ("& Fn_CategoryID &") "
							If Not(KnifeCMS.Data.IsNul(Fn_CategoryID)) Then
								Fn_SqlC   = Fn_SqlC &" AND a.ClassID IN ("& Fn_CategoryID &") "
							End If
						End If
					Case "keywords"
						Fn_KeyWords = KnifeCMS.Data.HtmlEncode(Fn_Match.SubMatches(1))
						If Not(KnifeCMS.Data.IsNul(Fn_KeyWords)) Then Fn_SqlC = Fn_SqlC &" AND a.VoteTitle LIKE '%"& Fn_KeyWords &"%' "
				End Select
			Next
			'生成查询条件结束
			'生成排序条件
			If Fn_Check = True Then
				Set Fn_Matches = KnifeCMS.Data.RegMatch(Fn_Orderby,"""([\s\S]+?)"":""([\s\S]+?)""")
				If Fn_Matches.Count>0 Then
					Fn_SqlOrderBy = " ORDER BY "
					For Each Fn_Match In Fn_Matches
						Select Case Lcase(Fn_Match.SubMatches(0))
							Case "id"
								Fn_TempString = " a.ID "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
							Case "addtime"
								Fn_TempString = " a.AddTime "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
							Case "hits"
								Fn_TempString = " a.Hits "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
							Case "joins"
								Fn_TempString = " a.Joins "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
						End Select
					Next
				End if
			End if
			'生成排序条件结束
			If Fn_Check = True Then
				Fn_Sql="SELECT "& Fn_SqlTop &" a.ID,a.ClassID,b.ClassName,a.VoteTitle,a.VoteMode,a.VoteMode_More,a.Description,a.ResultDescribe,a.Hits,a.Joins,a.AddTime,a.EditTime,a.SeoUrl FROM ["& DBTable_Vote &"] a LEFT JOIN ["& DBTable_VoteClass &"] b ON (b.ID=a.ClassID) WHERE a.Recycle=0 "& Fn_SqlC &" "& Fn_SqlOrderBy &""
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
				If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then
					Dim Fn_Temp_i,Fn_Temp_j,Fn_Temp_k,Fn_a,Fn_RsCount,Fn_PageCount,Fn_DataArray
					Fn_RsCount=Fn_Rs.RecordCount : If Fn_Rs.RecordCount<1 then Fn_RsCount=0
					Fn_Rs.PageSize = Fn_PageSize
					Fn_PageCount = Fn_Rs.PageCount
					Fn_Page = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","page")) : Fn_Page = KnifeCMS.IIF(Not(Fn_Page>0),1,Fn_Page)
					'Die KnifeCMS.URLQueryString
					if Fn_Page > Fn_PageCount then
						Fn_Rs.AbsolutePage = Fn_PageCount
					else
						Fn_Rs.AbsolutePage = Fn_Page
					end if
					Fn_Page = Fn_Rs.AbsolutePage
					ReDim DataArray(Fn_Rs.Fields.Count,Fn_PageSize)
					For Fn_Temp_i=0 to Fn_PageSize-1
						If Fn_Rs.EOF Then Exit For
						for Fn_Temp_j=0 to Fn_Rs.Fields.Count-1
							DataArray(Fn_Temp_j,Fn_Temp_i)=Fn_Rs(Fn_Temp_j)
						next
						Fn_Rs.movenext
					Next
					
					'**生成分页的地址**
					Fn_PageCategoryID = ID
					If KnifeCMS.Data.CLng(Fn_PageCategoryID)>0 Then
						Fn_PageCategoryString = "vote/category/"& Fn_PageCategoryID &"_{tpl:page}/index"& FileSuffix
						'vote/category/10_2/seourl.htm
					Else
						Fn_PageCategoryString = "vote_page{tpl:page}"& FileSuffix
						'vote_page2.htm   
					End If
					
					If RunMode = "dynamic" Then
						Fn_PageUrl = SiteURL & SystemPath &"?"& Fn_PageCategoryString
					Else
						Fn_PageUrl = SiteURL & SystemPath & Fn_PageCategoryString
					End If
					'****
					
					Fn_PageString = "<div class=pagestyle><span class=pages>"& Replace(Lang_Pages(0),"{tpl:num}",Fn_RsCount) &"</span>"
					If Fn_Page>1 Then Fn_PageString = Fn_PageString &"<a href="""& Replace(Fn_PageUrl,"{tpl:page}",Fn_Page-1) &""">"& Lang_Pages(1) &"</a>"
					if Fn_Page>1 then Fn_a=1 : if Fn_Page>2 then Fn_a=2 : if Fn_Page>3 then Fn_a=3 : if Fn_Page>4 then Fn_a=4 : if Fn_Page>5 then Fn_a=5 : if Fn_Page>6 then Fn_a=6 : if Fn_Page>7 then Fn_a=7 : if Fn_Page>8 then Fn_a=8 : if Fn_Page>9 then Fn_a=9
					for Fn_Temp_i=Fn_Page-Fn_a to Fn_Page-1
						Fn_PageString = Fn_PageString & "<a href="""& Replace(Fn_PageUrl,"{tpl:page}",Fn_Temp_i)&""">"&Fn_Temp_i&"</a>"
					next
					Fn_PageString = Fn_PageString &"<span class=current>"&Fn_Page&"</span>"
					
					for Fn_Temp_i=Fn_Page+1 to Fn_Page+9
						if Fn_Temp_i > Fn_PageCount then Exit for
						Fn_PageString = Fn_PageString & "<a href="""& Replace(Fn_PageUrl,"{tpl:page}",Fn_Temp_i) &""">"&Fn_Temp_i&"</a>"
					next
					
					if Not(Fn_Page>=Fn_PageCount) then Fn_PageString = Fn_PageString & "<a href="""& Replace(Fn_PageUrl,"{tpl:page}",Fn_Page+1) &""">"& Lang_Pages(2) &"</a>"
					Fn_PageString = Fn_PageString & "<span class=pages>"& Replace(Lang_Pages(3),"{tpl:num}",Fn_PageCount) &"</span><span class=pages>"& Replace(Lang_Pages(4),"{tpl:num}",Fn_PageSize) &"</span></div>"
				Else
					DataArray=""
				End If
			Else
				DataArray=""
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
			If Not(IsArray(DataArray)) Then Fn_Check = False
            If Fn_Check Then
				GlobalPreString = KnifeCMS.Data.ReplaceString(GlobalPreString,"[votelist:pages]",Fn_PageString)
				GlobalSufString = KnifeCMS.Data.ReplaceString(GlobalSufString,"[votelist:pages]",Fn_PageString)
				RegExpObj.Pattern = LabelRuleField
				Set MatchesField  = RegExpObj.Execute(LoopStringList)
				If IsArray(DataArray) Then DataNum=Ubound(DataArray,2) Else DataNum=-1
				LoopStringTotal = ""
				Fn_CNum  = 0
				'{ad:loop}
				For Fn_i=0 To DataNum
					    If KnifeCMS.Data.IsNul(DataArray(0,Fn_i)) Then Exit For
						Fn_CNum  = Fn_CNum + 1
						LoopStringListNew = LoopStringList
						For Each MatchField in MatchesField
							
							FieldNameAndAttr=KnifeCMS.Data.RegReplace(MatchField.SubMatches(0),"[\s]+",chr(32))
							FieldNameAndAttr=KnifeCMS.Data.TrimOuter(FieldNameAndAttr)
							Fn_Len=Instr(FieldNameAndAttr,chr(32))
							If Fn_Len > 0 Then 
								FieldName = Left(FieldNameAndAttr,Fn_Len-1)
								FieldAttr =	Right(FieldNameAndAttr,Len(FieldNameAndAttr) - Fn_Len)
							Else
								FieldName = FieldNameAndAttr
								FieldAttr =	""
							End If
							Select Case FieldName
								Case "i"             : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_CNum)
								Case "id"            : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(0,Fn_i))
								Case "categoryid"    : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(1,Fn_i))
								Case "categoryname"  : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(2,Fn_i))
								Case "url"
									Fn_TempString     = SiteURL & SystemPath &"?vote/"& DataArray(0,Fn_i) &"/"& KnifeCMS.Data.URLDecode(DataArray(12,Fn_i)) & FileSuffix
									LoopStringListNew = KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "title"         : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(3,Fn_i))
								Case "votemode"      : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(4,Fn_i))
								Case "votemodemore"  : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(5,Fn_i))
								Case "description"
									Fn_TempString = KnifeCMS.Data.HTMLDecode(DataArray(6,Fn_i))
									Fn_AbsLen = KnifeCMS.Data.CLng(ParseAttr(FieldAttr)("len"))
									If Fn_AbsLen>0 Then
										If Len(Fn_TempString) > Fn_AbsLen Then Fn_TempString=Left(Fn_TempString,Fn_AbsLen)&"..."
									Else
										Fn_TempString=Fn_TempString
									End If
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "resultdescribe"
									Fn_TempString = KnifeCMS.Data.HTMLDecode(DataArray(7,Fn_i))
									Fn_AbsLen = KnifeCMS.Data.CLng(ParseAttr(FieldAttr)("len"))
									If Fn_AbsLen>0 Then
										If Len(Fn_TempString) > Fn_AbsLen Then Fn_TempString=Left(Fn_TempString,Fn_AbsLen)&"..."
									Else
										Fn_TempString=Fn_TempString
									End If
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "hits"  : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(8,Fn_i))
								Case "joins" : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(9,Fn_i))
								Case "addtime"
									Fn_TimeStyle  = ParseAttr(FieldAttr)("style")
									Fn_TempString = DataArray(10,Fn_i)
									If Fn_TimeStyle<>"" Then Fn_TempString = KnifeCMS.Data.FormatDateTime(Fn_TempString,Fn_TimeStyle)
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "edittime"
									Fn_TimeStyle  = ParseAttr(FieldAttr)("style")
									Fn_TempString = DataArray(11,Fn_i)
									If Fn_TimeStyle<>"" Then Fn_TempString = KnifeCMS.Data.FormatDateTime(Fn_TempString,Fn_TimeStyle)
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
							End Select
							StrDictionary.removeAll
						Next
						LoopStringTotal = LoopStringTotal & LoopStringListNew
				Next
				'{/ad:loop}
				Set MatchesField = Nothing
				If Fn_CNum > 0 Then LoopStringTotal = GlobalPreString & LoopStringTotal & GlobalSufString
				LoopStringTotal = ParseIf(LoopStringTotal)
				If LoopStringTotal="" Then LoopStringTotal = "没有数据..."
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,LoopStringTotal)
				StrDictionary.removeAll
			Else
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,"")
				StrDictionary.removeAll
            End If
		Else
			Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,"")
		End If
		Next
		Set Matches = Nothing
	End Function
	
	'$:投票选项列表
	Public Function ParseVoteOptionList()
		If Not(KnifeCMS.IsStringExist(Pv_Content,"{kfc:voteoptionlist")) Then Exit Function
		Dim Fn_Rs,Fn_i,Fn_j,Fn_CNum,Fn_Len,Fn_CateType,Fn_ShowNum,Fn_CategoryID,Fn_Condition,Fn_Orderby
		Dim Fn_ID,Fn_TempID,Fn_TempCateID,Fn_KeyWords,Fn_TempString,Fn_IsTopLine,Fn_IsOntop,Fn_IsHot,Fn_IsElite,Fn_IsThumbnail,Fn_IsSlidesimgs
		Dim Fn_AbsLen,Fn_TimeStyle,Fn_Month_Day
		Dim Fn_Sql,Fn_SqlTop,Fn_SqlC,Fn_SqlOrderBy
		Dim Fn_Match,Fn_Matches
		Dim Fn_IsPage,Fn_PageSize,Fn_Page,Fn_PageMatche,Fn_PageMatches,Fn_PageString,Fn_PageUrl
		Dim Fn_GetMatche,Fn_GetMatches
		Dim Fn_Check
		LabelRule      = "{kfc:voteoptionlist([\s\S]*?)}([^\""][\s\S]*?){voteoptionlist:loop}([\s\S]*?){/voteoptionlist:loop}([\s\S]*?){/kfc:voteoptionlist}"
		LabelRuleField = "\[voteoptionlist:([\s\S]+?)\]"
		RegExpObj.Pattern = LabelRule
		Set Matches = RegExpObj.Execute(Pv_Content)
		For Each Match In Matches
			Fn_Check = True
			LabelAttrList   = Match.SubMatches(0)
			GlobalPreString = Match.SubMatches(1)
			LoopStringList  = Match.SubMatches(2)
			GlobalSufString = Match.SubMatches(3)
			Set AttrDictionary=ParseAttr(LabelAttrList)
			Fn_Condition  = AttrDictionary("condition")
			Fn_Orderby    = AttrDictionary("orderby")
			Fn_ShowNum    = AttrDictionary("shownum") : Fn_ShowNum = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_ShowNum),-1,KnifeCMS.Data.CLng(Fn_ShowNum))
			Set AttrDictionary=Nothing
			Fn_IsPage = False
			'If Fn_ShowNum >=0 Then Fn_SqlTop = " TOP "& Fn_ShowNum &" "
			'生成查询条件
			Set Fn_Matches = KnifeCMS.Data.RegMatch(Fn_Condition,"""([\s\S]+?)"":""([\s\S]*?)""")
			Fn_SqlC = ""
			For Each Fn_Match In Fn_Matches
				Select Case Fn_Match.SubMatches(0)
					Case "id"
						Fn_ID = KnifeCMS.Data.CLng(Fn_Match.SubMatches(1))
						If Fn_ID>0 Then
							Fn_SqlC = Fn_SqlC &" a.VoteID="& Fn_ID &" "
						Else
							Fn_SqlC = Fn_SqlC &" a.VoteID=0 "
							Fn_Check = False
						End If
				End Select
			Next
			'生成查询条件结束
			If Fn_Check Then
				'生成排序条件
				Set Fn_Matches = KnifeCMS.Data.RegMatch(Fn_Orderby,"""([\s\S]+?)"":""([\s\S]+?)""")
				If Fn_Matches.Count>0 Then
					Fn_SqlOrderBy = " ORDER BY "
					For Each Fn_Match In Fn_Matches
						Select Case Lcase(Fn_Match.SubMatches(0))
							Case "id"
								Fn_TempString = " a.ID "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
							Case "votenum"
								Fn_TempString = " a.VoteNum "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
								Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
						End Select
					Next
				End if
				'生成排序条件结束
				
			   '查询数据库获取数据
				Fn_Sql="SELECT a.ID,a.Content,a.VoteNum FROM ["& DBTable_VoteOption &"] a WHERE "& Fn_SqlC &" "& Fn_SqlOrderBy &""
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
				If Not(Fn_Rs.Eof) Then DataArray = Fn_Rs.GetRows() : Else Set DataArray = nothing
				KnifeCMS.DB.CloseRs Fn_Rs
				If Not(IsArray(DataArray)) Then
					Fn_Check = False : DataNum=-1
				Else
					DataNum=Ubound(DataArray,2)
				End If
			End If
            If Fn_Check Then
				GlobalPreString = KnifeCMS.Data.ReplaceString(GlobalPreString,"[votelist:pages]",Fn_PageString)
				GlobalSufString = KnifeCMS.Data.ReplaceString(GlobalSufString,"[votelist:pages]",Fn_PageString)
				RegExpObj.Pattern = LabelRuleField
				Set MatchesField  = RegExpObj.Execute(LoopStringList)
				If IsArray(DataArray) Then DataNum=Ubound(DataArray,2) Else DataNum=-1
				LoopStringTotal = ""
				Fn_CNum  = 0
				'计算所有投票数
				Dim Fn_VoteAll : Fn_VoteAll=0
				For Fn_i=0 To DataNum
					Fn_VoteAll = Fn_VoteAll + KnifeCMS.Data.CLng(DataArray(2,Fn_i))
				Next
				'{ad:loop}
				'判断循环显示几个投票选项
				Dim Fn_LoopNum : Fn_LoopNum=-1
				If Fn_ShowNum>0 Then
					If DataNum>=0 then
						If Fn_ShowNum > DataNum Then
							Fn_LoopNum = DataNum
						Else
							Fn_LoopNum = Fn_ShowNum-1
						End If
					End If
				Else
					Fn_LoopNum = DataNum
				End If
				For Fn_i=0 To Fn_LoopNum
					    If KnifeCMS.Data.IsNul(DataArray(0,Fn_i)) Then Exit For
						Fn_CNum  = Fn_CNum + 1
						LoopStringListNew = LoopStringList
						For Each MatchField in MatchesField
							
							FieldNameAndAttr=KnifeCMS.Data.RegReplace(MatchField.SubMatches(0),"[\s]+",chr(32))
							FieldNameAndAttr=KnifeCMS.Data.TrimOuter(FieldNameAndAttr)
							Fn_Len=Instr(FieldNameAndAttr,chr(32))
							If Fn_Len > 0 Then 
								FieldName = Left(FieldNameAndAttr,Fn_Len-1)
								FieldAttr =	Right(FieldNameAndAttr,Len(FieldNameAndAttr) - Fn_Len)
							Else
								FieldName = FieldNameAndAttr
								FieldAttr =	""
							End If
							Select Case FieldName
								Case "i"             : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_CNum)
								Case "id"            : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(0,Fn_i))
								Case "content"       : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(1,Fn_i))
								Case "votenum"       : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(2,Fn_i))
								Case "percent"       : 
									If Fn_VoteAll>0 And KnifeCMS.Data.CLng(DataArray(2,Fn_i))>0 Then
										LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,KnifeCMS.Data.CLng(DataArray(2,Fn_i)*100/Fn_VoteAll) &"%")
									Else
										LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,"0%")
									End If
									
							End Select
							StrDictionary.removeAll
						Next
						LoopStringTotal = LoopStringTotal & LoopStringListNew
				Next
				'{/ad:loop}
				Set MatchesField = Nothing
				If Fn_CNum > 0 Then LoopStringTotal = GlobalPreString & LoopStringTotal & GlobalSufString
				LoopStringTotal = ParseIf(LoopStringTotal)
				If LoopStringTotal="" Then LoopStringTotal = "没有数据..."
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,LoopStringTotal)
				StrDictionary.removeAll
			Else
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,"")
				StrDictionary.removeAll
            End If
		Next
		Set Matches = Nothing
	End Function
	
	'$:友情链接列表
	Public Function ParseLinkList()
		If Not(KnifeCMS.IsStringExist(Pv_Content,"{kfc:linklist")) Then Exit Function
		Dim Fn_Rs,Fn_i,Fn_Len,Fn_Type,Fn_Sql
		Dim Fn_TempString,Fn_AbsLen
		Dim Fn_Check : Fn_Check = False
		LabelRule      = "{kfc:linklist([\s\S]*?)}([\s\S]*?){linklist:loop}([\s\S]*?){/linklist:loop}([\s\S]*?){/kfc:linklist}"
		LabelRuleField = "\[linklist:([\s\S]+?)\]"
		RegExpObj.Pattern = LabelRule
		Set Matches = RegExpObj.Execute(Pv_Content)
		For Each Match in Matches
			Fn_Check        = True
			LabelAttrList   = Match.SubMatches(0)
			GlobalPreString = Match.SubMatches(1)
			LoopStringList  = Match.SubMatches(2)
			GlobalSufString = Match.SubMatches(3)
			Set AttrDictionary=ParseAttr(LabelAttrList)
			Fn_Type=AttrDictionary("type")
			Set AttrDictionary=Nothing
			Select Case Fn_Type
			Case "pic"
				Fn_Sql = "SELECT ID,LinkName,LinkLogo,LinkURL,Remarks FROM ["& DBTable_Link &"] WHERE LEN(LinkLogo)>4 AND Checked=1 AND Recycle=0 ORDER BY Recommend DESC,OrderNum ASC,ID ASC"
			Case "font"
				Fn_Sql = "SELECT ID,LinkName,LinkLogo,LinkURL,Remarks FROM ["& DBTable_Link &"] WHERE LEN(LinkLogo)<4 AND Checked=1 AND Recycle=0 ORDER BY Recommend DESC,OrderNum ASC,ID ASC"
			Case Else
				Fn_Sql = "SELECT ID,LinkName,LinkLogo,LinkURL,Remarks FROM ["& DBTable_Link &"] WHERE Checked=1 AND Recycle=0 ORDER BY Recommend DESC,OrderNum ASC,ID ASC"
			End Select
            If Fn_Check Then
				RegExpObj.Pattern = LabelRuleField
				Set MatchesField  = RegExpObj.Execute(LoopStringList)
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
				If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then DataArray = Fn_Rs.GetRows() Else DataArray = ""
				KnifeCMS.DB.CloseRs Fn_Rs
				If IsArray(DataArray) Then DataNum=Ubound(DataArray,2) Else DataNum=-1
				LoopStringTotal = GlobalPreString
				'{loop}
				For Fn_i=0 To DataNum
					LoopStringListNew = LoopStringList
					For Each MatchField in MatchesField
						FieldNameAndAttr=KnifeCMS.Data.RegReplace(MatchField.SubMatches(0),"[\s]+",chr(32))
						FieldNameAndAttr=KnifeCMS.Data.TrimOuter(FieldNameAndAttr)
						Fn_Len=Instr(FieldNameAndAttr,chr(32))
						If Fn_Len > 0 Then 
							FieldName = Left(FieldNameAndAttr,Fn_Len-1)
							FieldAttr =	Right(FieldNameAndAttr,Len(FieldNameAndAttr) - Fn_Len)
						Else
							FieldName = FieldNameAndAttr
							FieldAttr =	""
						End If
						Select Case FieldName
							Case "i"        : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_i+1)
							Case "id"       : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(0,Fn_i))
							Case "name"     : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(1,Fn_i))
							Case "pic"      : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(2,Fn_i))
							Case "url"      : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(3,Fn_i))
							Case "abstract"
								Fn_TempString = KnifeCMS.Data.HTMLDecode(DataArray(4,Fn_i))
								Fn_AbsLen = KnifeCMS.Data.CLng(ParseAttr(FieldAttr)("len"))
								If Fn_AbsLen>0 Then
									Fn_TempString=Left(Fn_TempString,Fn_AbsLen)&"..."
								End If
								LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
						End Select
						StrDictionary.removeAll
					Next
					LoopStringTotal = LoopStringTotal & LoopStringListNew
				Next
				'{/loop}
				Set MatchesField = Nothing
				LoopStringTotal = LoopStringTotal & GlobalSufString
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,LoopStringTotal)
				StrDictionary.removeAll
			Else
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,"")
				StrDictionary.removeAll
            End If
		Next
		Set Matches = Nothing
	End Function
	
	'$:商品列表
	Public Function ParseGoodsList()
		If Not(KnifeCMS.IsStringExist(Pv_Content,"{kfc:goodslist")) Then Exit Function
		Dim Fn_Rs,Fn_i,Fn_j,Fn_CNum,Fn_Len,Fn_CateType,Fn_ShowNum,Fn_CategoryID,Fn_Condition,Fn_Orderby
		Dim Fn_ID,Fn_TempID,Fn_TempCateID,Fn_KeyWords,Fn_TempString,Fn_IsTopLine,Fn_IsOntop,Fn_IsHot,Fn_IsElite,Fn_IsThumbnail,Fn_IsSlidesimgs
		Dim Fn_AbsLen,Fn_TimeStyle,Fn_Month_Day
		Dim Fn_Sql,Fn_SqlTop,Fn_SqlC,Fn_SqlOrderBy
		Dim Fn_Match,Fn_Matches
		Dim Fn_IsPage,Fn_PageSize,Fn_Page,Fn_PageMatche,Fn_PageMatches,Fn_PageString,Fn_PageUrl,Fn_PageCategoryID,Fn_PageCategoryString,Fn_SeoUrl
		Dim Fn_GetMatche,Fn_GetMatches
		Dim Fn_Check
		LabelRule      = "{kfc:goodslist([\s\S]*?)}([^\""][\s\S]*?){goodslist:loop}([\s\S]*?){/goodslist:loop}([\s\S]*?){/kfc:goodslist}"
		LabelRuleField = "\[goodslist:([\s\S]+?)\]"
		RegExpObj.Pattern = LabelRule
		Set Matches = RegExpObj.Execute(Pv_Content)
		For Each Match In Matches
		If CloseModuleShop<>1 Then
			Fn_Check = True
			LabelAttrList   = Match.SubMatches(0)
			GlobalPreString = Match.SubMatches(1)
			LoopStringList  = Match.SubMatches(2)
			GlobalSufString = Match.SubMatches(3)
			Set AttrDictionary=ParseAttr(LabelAttrList)
			Fn_CateType   = AttrDictionary("type")
			Select Case Fn_CateType
				Case "goods" : Fn_CateType=1
				Case "adjunct" : Fn_CateType=2
				Case "present" : Fn_CateType=3
				Case Else : Fn_CateType=1
			End Select
			'Fn_CateType   = KnifeCMS.Data.RegReplace(Fn_CateType,"[^a-zA-Z]","")
			Fn_Condition  = AttrDictionary("condition")
			Fn_Orderby    = AttrDictionary("orderby")
			Fn_ShowNum    = AttrDictionary("shownum") : Fn_ShowNum = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_ShowNum),-1,KnifeCMS.Data.CLng(Fn_ShowNum))
			Fn_PageSize   = KnifeCMS.Data.CLng(AttrDictionary("pagesize")) : Fn_PageSize = KnifeCMS.IIF(Fn_PageSize>0 And Fn_PageSize<200,Fn_PageSize,20)
			Set AttrDictionary=Nothing
			If KnifeCMS.Data.RegTest(Fn_Condition,"get\[([\s\S]+?)\]") Then
				Set Fn_GetMatches = KnifeCMS.Data.RegMatch(Fn_Condition,"get\[([\s\S]+?)\]")
				If Fn_GetMatches.Count > 0 Then
					For Each Fn_GetMatche In Fn_GetMatches
						Fn_Condition = KnifeCMS.Data.ReplaceString(Fn_Condition,Fn_GetMatche.value,KnifeCMS.GetForm("both",Fn_GetMatche.SubMatches(0)))'"get\[(\s\S)\]")
					Next
				End If
			End If
			Fn_Condition = KnifeCMS.Data.RegReplace(Fn_Condition,"get\[(\s\S)\]",KnifeCMS.GetForm("both",""))
			If KnifeCMS.Data.RegTest(Match.Value,"\[goodslist:pages\]") Then Fn_IsPage = True
			If Fn_ShowNum >=0 Then Fn_SqlTop = " TOP "& Fn_ShowNum &" "
			Set Fn_Matches = KnifeCMS.Data.RegMatch(Fn_Condition,"""([\s\S]+?)"":""([\s\S]*?)""")
			Fn_SqlC = ""
			For Each Fn_Match In Fn_Matches
				Select Case Fn_Match.SubMatches(0)
					Case "id"
						Fn_ID = KnifeCMS.Data.ParseID(Fn_Match.SubMatches(1))
						If Not(KnifeCMS.Data.IsNul(Fn_ID)) Then
							Fn_SqlC = Fn_SqlC &" AND a.ID IN ("& Fn_ID &") "
						Else
							Fn_SqlC = Fn_SqlC &" AND a.ID IN (0) "
						End If
					Case "categoryid"
						Fn_CategoryID = ""
						Fn_TempID = Split(Fn_Match.SubMatches(1),",")
						For Fn_j=0 To Ubound(Fn_TempID)
							Fn_TempCateID = Fn_TempID(Fn_j)
							If InStr(Fn_TempCateID,"[getsub]")>0 Then
								Fn_TempCateID = KnifeCMS.Data.ReplaceString(Fn_TempCateID,"[getsub]","")
								Fn_TempCateID = KnifeCMS.Data.RegReplace(Fn_TempCateID,"[^0-9]","")
								If Not(KnifeCMS.Data.IsNul(Fn_TempCateID)) Then
									Fn_TempCateID = Fn_TempCateID &","& GetChildClass(DBTable_GoodsClass,Fn_TempCateID)
									Fn_TempCateID = Join(KnifeCMS.Data.ClearSameDatas(Split(Fn_TempCateID,","),false),",")
								End If
							Else
								Fn_TempCateID = KnifeCMS.Data.RegReplace(Fn_TempCateID,"[^0-9]","")
							End If
							Fn_CategoryID = Fn_CategoryID &","& Fn_TempCateID
						Next
						If Not(KnifeCMS.Data.IsNul(Fn_CategoryID)) Then Fn_CategoryID = Join(KnifeCMS.Data.ClearSameDatas(Split(Fn_CategoryID,","),false),",")
						Fn_CategoryID = KnifeCMS.Data.ReplaceString(Fn_CategoryID,"$err$","")
						Fn_CategoryID = KnifeCMS.Data.ParseID(Fn_CategoryID)
						If Not(KnifeCMS.Data.IsNul(Fn_CategoryID)) Then Fn_SqlC = Fn_SqlC &" AND a.ClassID IN ("& Fn_CategoryID &") "
					Case "keywords"
						Fn_KeyWords = KnifeCMS.Data.HtmlEncode(Fn_Match.SubMatches(1))
						If Not(KnifeCMS.Data.IsNul(Fn_KeyWords)) Then Fn_SqlC = Fn_SqlC &" AND a.GoodsName LIKE '%"& Fn_KeyWords &"%' "
					Case "thumbnail"
						Fn_IsThumbnail = Fn_Match.SubMatches(1)
						If Fn_IsThumbnail = "yes" Then Fn_SqlC = Fn_SqlC &" AND ( Len(a.Thumbnail)>5 Or  Len(a.SlidesFirstImg)>5 ) "
						If Fn_IsThumbnail = "no"  Then Fn_SqlC = Fn_SqlC &" AND ( Len(a.Thumbnail)<5 And Len(a.SlidesFirstImg)<5 ) "
					Case "slidesimgs"
						Fn_IsSlidesimgs = Fn_Match.SubMatches(1)
						If Fn_IsSlidesimgs = "yes" Then Fn_SqlC = Fn_SqlC &" AND Len(a.SlidesImgs)>5 "
						If Fn_IsSlidesimgs = "no"  Then Fn_SqlC = Fn_SqlC &" AND Len(a.SlidesImgs)<5 "
				End Select
			Next
			Set Fn_Matches = KnifeCMS.Data.RegMatch(Fn_Orderby,"""([\s\S]+?)"":""([\s\S]+?)""")
			If Fn_Matches.Count>0 Then
				Fn_SqlOrderBy = " ORDER BY "
				For Each Fn_Match In Fn_Matches
					Select Case Lcase(Fn_Match.SubMatches(0))
						Case "id"
							Fn_TempString = " a.ID "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
							Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
						Case "addtime"
							Fn_TempString = " a.AddTime "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
							Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
						Case "hits"
							Fn_TempString = " a.Hits "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
							Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
						Case "diggs"
							Fn_TempString = " a.Diggs "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
							Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
						Case "price"
							Fn_TempString = " a.Price "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
							Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
						Case "recommend"
							Fn_TempString = " a.Recommend "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
							Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
						Case "hot"
							Fn_TempString = " a.Hot "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
							Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
						Case "new"
							Fn_TempString = " a.New "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
							Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
						Case "homepagerecommend"
							Fn_TempString = " a.Homepagerecommend "& KnifeCMS.IIF(Fn_Match.SubMatches(1)="asc","ASC","DESC")
							Fn_SqlOrderBy = Fn_SqlOrderBy & KnifeCMS.IIF(Fn_SqlOrderBy<>" ORDER BY ",","& Fn_TempString,Fn_TempString)
					End Select
				Next
			End if
			'取得自定义字段
			Pv_Field = ""
			Pv_Field = KnifeCMS.DB.GetField(DBTable_Goods,"a")
			If Pv_Field<>"" Then
				Pv_TempString = Replace(Pv_Field,"a.","")
				Pv_TempArray  = Split(Pv_TempString,",")
				Pv_Field      = ","& Pv_Field
			Else
				Pv_TempString = ""
				Pv_TempArray  = ""
			End If
			Select Case DB_Type
			Case 0
			Fn_Sql="SELECT "& Fn_SqlTop &" a.ID,a.ClassID,a.GoodsName,a.GoodsBN,a.GoodsSN,a.Price,a.MktPrice,a.Weight,a.Store,a.BrandID,a.AddTime,IIF(LEN(a.Thumbnail)=0,a.FirstImg,a.Thumbnail),a.Abstract,a.SeoUrl,b.ClassName"& Pv_Field &" FROM ["& DBTable_Goods &"] a LEFT JOIN ["& DBTable_GoodsClass &"] b ON (b.ID=a.ClassID) WHERE a.Recycle=0 AND a.Marketable=1 AND a.GoodsType = "& Fn_CateType &" "& Fn_SqlC &" "& Fn_SqlOrderBy &""
			Case 1
			Fn_Sql="SELECT "& Fn_SqlTop &" a.ID,a.ClassID,a.GoodsName,a.GoodsBN,a.GoodsSN,a.Price,a.MktPrice,a.Weight,a.Store,a.BrandID,a.AddTime,CASE WHEN LEN(a.Thumbnail)=0 THEN a.FirstImg ELSE a.Thumbnail END,a.Abstract,a.SeoUrl,b.ClassName"& Pv_Field &" FROM ["& DBTable_Goods &"] a LEFT JOIN ["& DBTable_GoodsClass &"] b ON (b.ID=a.ClassID) WHERE a.Recycle=0 AND a.Marketable=1 AND a.GoodsType = "& Fn_CateType &" "& Fn_SqlC &" "& Fn_SqlOrderBy &""
			End Select
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
			'Echo Fn_Sql &"--<br>"
			If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then
				Dim Fn_Temp_i,Fn_Temp_j,Fn_Temp_k,Fn_a,Fn_RsCount,Fn_PageCount,Fn_DataArray
				Fn_RsCount=Fn_Rs.RecordCount : If Fn_Rs.RecordCount<1 then Fn_RsCount=0
				Fn_Rs.PageSize = Fn_PageSize
				Fn_PageCount = Fn_Rs.PageCount
				Fn_Page = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","page")) : Fn_Page = KnifeCMS.IIF(Not(Fn_Page>0),1,Fn_Page)
				if Fn_Page > Fn_PageCount then
					Fn_Rs.AbsolutePage = Fn_PageCount
				else
					Fn_Rs.AbsolutePage = Fn_Page
				end if
				Fn_Page = Fn_Rs.AbsolutePage
				ReDim DataArray(Fn_Rs.Fields.Count,Fn_PageSize)
				For Fn_Temp_i=0 to KnifeCMS.IIF(Fn_ShowNum<=Fn_PageSize,Fn_ShowNum-1,Fn_PageSize-1)
					If Fn_Rs.Eof Then Exit For
					for Fn_Temp_j=0 to Fn_Rs.Fields.Count-1
						DataArray(Fn_Temp_j,Fn_Temp_i)=Fn_Rs(Fn_Temp_j)
					next
					Fn_Rs.movenext
				Next
				'**生成分页的地址**
				Fn_PageCategoryID = ID
				If KnifeCMS.Data.CLng(Fn_PageCategoryID)>0 Then
					Fn_PageCategoryString = "shop/category/"& Fn_PageCategoryID &"_{tpl:page}/index"& FileSuffix
					'shop/category/10_2/seourl.htm
				Else
					Fn_PageCategoryString = "shop_page{tpl:page}"& FileSuffix
					'shop_page2.htm
				End If
				
				If RunMode = "dynamic" Then
					Fn_PageUrl = SiteURL & SystemPath &"?"& Fn_PageCategoryString
				Else
					Fn_PageUrl = SiteURL & SystemPath & Fn_PageCategoryString
				End If
				'****
				
				Fn_PageString = "<div class=pagestyle><span class=pages>"& Replace(Lang_Pages(0),"{tpl:num}",Fn_RsCount) &"</span>"
				If Fn_Page>1 Then Fn_PageString = Fn_PageString &"<a href="""& Replace(Fn_PageUrl,"{tpl:page}",Fn_Page-1) &""">"& Lang_Pages(1) &"</a>"
				if Fn_Page>1 then Fn_a=1 : if Fn_Page>2 then Fn_a=2 : if Fn_Page>3 then Fn_a=3 : if Fn_Page>4 then Fn_a=4 : if Fn_Page>5 then Fn_a=5 : if Fn_Page>6 then Fn_a=6 : if Fn_Page>7 then Fn_a=7 : if Fn_Page>8 then Fn_a=8 : if Fn_Page>9 then Fn_a=9
				for Fn_Temp_i=Fn_Page-Fn_a to Fn_Page-1
					Fn_PageString = Fn_PageString &"<a href="""& Replace(Fn_PageUrl,"{tpl:page}",Fn_Temp_i)&""">"&Fn_Temp_i&"</a>"
				next
				Fn_PageString = Fn_PageString &"<span class=current>"&Fn_Page&"</span>"
				
				for Fn_Temp_i=Fn_Page+1 to Fn_Page+9
					if Fn_Temp_i > Fn_PageCount then Exit for
					Fn_PageString = Fn_PageString &"<a href="""& Replace(Fn_PageUrl,"{tpl:page}",Fn_Temp_i) &""">"&Fn_Temp_i&"</a>"
				next
				
				if Not(Fn_Page>=Fn_PageCount) then Fn_PageString = Fn_PageString &"<a href="""& Replace(Fn_PageUrl,"{tpl:page}",Fn_Page+1) &""">"& Lang_Pages(2) &"</a>"
				Fn_PageString = Fn_PageString &"<span class=pages>"& Replace(Lang_Pages(3),"{tpl:num}",Fn_PageCount) &"</span><span class=pages>"& Replace(Lang_Pages(4),"{tpl:num}",Fn_PageSize) &"</span></div>"
			Else
				DataArray=""
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
			If Not(IsArray(DataArray)) Then Fn_Check = False
            If Fn_Check Then
				GlobalPreString = KnifeCMS.Data.ReplaceString(GlobalPreString,"[goodslist:pages]",Fn_PageString)
				GlobalSufString = KnifeCMS.Data.ReplaceString(GlobalSufString,"[goodslist:pages]",Fn_PageString)
				RegExpObj.Pattern = LabelRuleField
				Set MatchesField  = RegExpObj.Execute(LoopStringList)
				If IsArray(DataArray) Then DataNum=Ubound(DataArray,2) Else DataNum=-1
				LoopStringTotal = ""
				Fn_CNum  = 0
				'{ad:loop}
				'Echo DataNum &"<br>"
				For Fn_i=0 To DataNum
					    'Echo "DataArray(0,"& Fn_i&"):"& DataArray(0,Fn_i) &"<br>"
						If KnifeCMS.Data.IsNul(DataArray(0,Fn_i)) Then Exit For
						Fn_CNum  = Fn_CNum + 1
						LoopStringListNew = LoopStringList
						For Each MatchField in MatchesField
							
							FieldNameAndAttr=KnifeCMS.Data.RegReplace(MatchField.SubMatches(0),"[\s]+",chr(32))
							FieldNameAndAttr=KnifeCMS.Data.TrimOuter(FieldNameAndAttr)
							Fn_Len=Instr(FieldNameAndAttr,chr(32))
							If Fn_Len > 0 Then 
								FieldName = Left(FieldNameAndAttr,Fn_Len-1)
								FieldAttr =	Right(FieldNameAndAttr,Len(FieldNameAndAttr) - Fn_Len)
							Else
								FieldName = FieldNameAndAttr
								FieldAttr =	""
							End If
							Select Case FieldName
								Case "i"             : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_CNum)
								Case "id"            : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(0,Fn_i))
								Case "categoryid"    : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(1,Fn_i))
								Case "categoryname"  : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,KnifeCMS.Data.URLDecode(DataArray(14,Fn_i)))
								Case "url"
									Fn_TempString     = SiteURL & SystemPath &"?shop/"& DataArray(0,Fn_i) &"/"& KnifeCMS.Data.URLDecode(DataArray(13,Fn_i)) & FileSuffix
									LoopStringListNew = KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "name"          : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(2,Fn_i))
								Case "goodsbn"       : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(3,Fn_i))
								Case "goodssn"       : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(4,Fn_i))
								Case "price"         : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,KnifeCMS.Data.FormatCurrency(DataArray(5,Fn_i)))
								Case "marketprice"   : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,KnifeCMS.Data.FormatCurrency(DataArray(6,Fn_i)))
								Case "weight"        : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(7,Fn_i))
								Case "store"         : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(8,Fn_i))
								Case "brandid"       : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(9,Fn_i))
								Case "addtime"
									Fn_TimeStyle  = ParseAttr(FieldAttr)("style")
									Fn_TempString = DataArray(10,Fn_i)
									If Fn_TimeStyle<>"" Then Fn_TempString = KnifeCMS.Data.FormatDateTime(Fn_TempString,Fn_TimeStyle)
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
								Case "thumbnail"     : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,KnifeCMS.Data.URLDecode(DataArray(11,Fn_i)))
								Case "abstract"
									Fn_TempString = KnifeCMS.Data.HTMLDecode(DataArray(12,Fn_i))
									Fn_AbsLen = KnifeCMS.Data.CLng(ParseAttr(FieldAttr)("len")) : If Fn_AbsLen=0 Then Fn_AbsLen = 250
									If Len(Fn_TempString) > Fn_AbsLen Then Fn_TempString=Left(Fn_TempString,Fn_AbsLen)&"..."
									LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
							End Select
							'自定义字段
							If Pv_Field<>"" Then
								For Fn_j=0 To Ubound(Pv_TempArray)
									If LCase(FieldName)=LCase(Pv_TempArray(Fn_j)) Then
										Fn_TempString    = KnifeCMS.Data.HTMLDecode(DataArray(14+Fn_j+1,Fn_i))
										LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_TempString)
									End If
								Next
							End If
							StrDictionary.removeAll
						Next
						LoopStringTotal = LoopStringTotal & LoopStringListNew
				Next
				'{/ad:loop}
				Set MatchesField = Nothing
				If Fn_CNum > 0 Then LoopStringTotal = GlobalPreString & LoopStringTotal & GlobalSufString
				LoopStringTotal = ParseIf(LoopStringTotal)
				If LoopStringTotal="" Then LoopStringTotal = "没有数据..."
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,LoopStringTotal)
				StrDictionary.removeAll
			Else
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,"")
				StrDictionary.removeAll
            End If
		Else
		Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,"")
		End If
		Next
		Set Matches = Nothing
	End Function
	
	'$:商品配件组列表
	Public Function ParseAdjunctList()
		If Not(KnifeCMS.IsStringExist(Pv_Content,"{kfc:adjunctlist")) Then Exit Function
		Dim Fn_Rs,Fn_Sql,Fn_SqlC,Fn_SqlOrderBy,Fn_Condition,Fn_Orderby
		Dim Fn_Match,Fn_Matches
		Dim Fn_i,Fn_j,Fn_CNum,Fn_Len,Fn_ShowNum,Fn_CategoryID
		Dim Fn_ID,Fn_TempID,Fn_AdjunctType,Fn_TempString
		Dim Fn_Check
		Dim Fn_AdjunctGoodsID
		LabelRule      = "{kfc:adjunctlist([\s\S]*?)}([\s\S]*?){adjunctlist:loop}([\s\S]*?){/adjunctlist:loop}([\s\S]*?){/kfc:adjunctlist}"
		LabelRuleField = "\[adjunctlist:([\s\S]+?)\]"
		RegExpObj.Pattern = LabelRule
		Set Matches = RegExpObj.Execute(Pv_Content)
		For Each Match In Matches
			Fn_Check = True
			LabelAttrList   = Match.SubMatches(0)
			GlobalPreString = Match.SubMatches(1)
			LoopStringList  = Match.SubMatches(2)
			GlobalSufString = Match.SubMatches(3)
			Set AttrDictionary=ParseAttr(LabelAttrList)
			Fn_Condition  = AttrDictionary("condition")
			Set AttrDictionary=Nothing
			StrDictionary.removeAll
			Set Fn_Matches = KnifeCMS.Data.RegMatch(Fn_Condition,"""([\s\S]+?)"":""([\s\S]*?)""")
			Fn_SqlC = ""
			For Each Fn_Match In Fn_Matches
				Select Case Fn_Match.SubMatches(0)
					Case "id"
						Fn_ID = KnifeCMS.Data.CLng(Fn_Match.SubMatches(1))
						If Fn_ID>0 Then Fn_SqlC = Fn_SqlC &" AND a.GoodsID="& Fn_ID &" " : Else Fn_Check = False
					Case "type"
						Fn_AdjunctType = KnifeCMS.Data.HtmlEncode(Fn_Match.SubMatches(1))
						If Not(KnifeCMS.Data.IsNul(Fn_AdjunctType)) Then Fn_SqlC = Fn_SqlC &" AND a.AdjunctType='"& Fn_AdjunctType &"' "
				End Select
			Next
			'查询数据库获取数据
			If Fn_Check Then
				Fn_Sql="SELECT a.ID,a.AdjunctName,a.AdjunctType,a.MinNum,a.MaxNum,a.PriceType,a.Price FROM ["& DBTable_GoodsAdjunct &"] a WHERE a.Disable=0 "& Fn_SqlC &" "& Fn_SqlOrderBy &""
				'Echo Fn_Sql &"--<br>"
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
				If Not(Fn_Rs.Eof) Then DataArray = Fn_Rs.GetRows() : Else Set DataArray = nothing
				KnifeCMS.DB.CloseRs Fn_Rs
				If Not(IsArray(DataArray)) Then
					Fn_Check = False : DataNum=-1
				Else
					DataNum=Ubound(DataArray,2)
				End If
			End If
			
            If Fn_Check Then
				RegExpObj.Pattern = LabelRuleField
				Set MatchesField  = RegExpObj.Execute(LoopStringList)
				LoopStringTotal = ""
				Fn_CNum         = 0
				'{loop}
				For Fn_i=0 To DataNum
					If KnifeCMS.Data.IsNul(DataArray(0,Fn_i)) Then Exit For
					Fn_CNum  = Fn_CNum + 1
					LoopStringListNew = LoopStringList
					For Each MatchField in MatchesField
						FieldNameAndAttr=KnifeCMS.Data.RegReplace(MatchField.SubMatches(0),"[\s]+",chr(32))
						FieldNameAndAttr=KnifeCMS.Data.TrimOuter(FieldNameAndAttr)
						Fn_Len=Instr(FieldNameAndAttr,chr(32))
						If Fn_Len > 0 Then
							FieldName = Left(FieldNameAndAttr,Fn_Len-1)
							FieldAttr =	Right(FieldNameAndAttr,Len(FieldNameAndAttr) - Fn_Len)
						Else
							FieldName = FieldNameAndAttr
							FieldAttr =	""
						End If
						Select Case FieldName
							Case "i"             : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,Fn_CNum)
							Case "id"            : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(0,Fn_i))
							Case "name"          : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(1,Fn_i))
							Case "type"          : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(2,Fn_i))
							Case "minnum"        : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(3,Fn_i))
							Case "maxnum"        : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(4,Fn_i))
							Case "pricetype"     : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(5,Fn_i))
							Case "price"         : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,DataArray(6,Fn_i))
							Case "goodsid"       : LoopStringListNew=KnifeCMS.Data.ReplaceString(LoopStringListNew,MatchField.value,OS.GetAdjunctGoods(DataArray(0,Fn_i)))
						End Select
					Next
					LoopStringTotal = LoopStringTotal & LoopStringListNew
				Next
				'{/loop}
				Set MatchesField = Nothing
				If Fn_CNum > 0 Then LoopStringTotal = GlobalPreString & LoopStringTotal & GlobalSufString
				LoopStringTotal = ParseIf(LoopStringTotal)
				If LoopStringTotal="" Then LoopStringTotal = "没有数据..."
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,LoopStringTotal)
			Else
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,"")
            End If
			StrDictionary.removeAll
		Next
		Set Matches = Nothing
	End Function
	
	Public Function ParseGoodsDetail()
		Dim Fn_i,Fn_TempArray,Fn_FieldType
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:categoryid}",Goods_ClassID)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:categoryname}",Goods_ClassName)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:id}",Goods_ID)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:name}",Goods_Name)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:bn}",Goods_BN)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:sn}",Goods_SN)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:price}",KnifeCMS.Data.FormatCurrency(Goods_Price))
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:marketprice}",KnifeCMS.Data.FormatCurrency(Goods_MktPrice))
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:weight}",Goods_Weight)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:store}",Goods_Store)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:unit}",Goods_Unit)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:brandid}",Goods_BrandID)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:brandlogo}",Goods_BrandLogo)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:brandname}",Goods_BrandName)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:thumbnail}",Goods_Thumbnail)
		If KnifeCMS.Data.IsNul(Goods_FirstImg) Then Goods_FirstImg = Goods_Thumbnail
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:firstimg}",Goods_FirstImg)
		Fn_TempArray = Split(Goods_Imgs,",")
		Goods_Imgs = ""
		For Fn_i=0 To Ubound(Fn_TempArray)
			Goods_Imgs = Goods_Imgs & "<li><a rel=""useZoom:'bigimgview',smallImage:'"&Fn_TempArray(Fn_i)&"'"" class=""cloud-zoom-gallery"" href="""&Fn_TempArray(Fn_i)&"""><img src="""&Fn_TempArray(Fn_i)&""" alt="""& Goods_Name &"""></a></li>"
		Next
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:imgs}",Goods_Imgs)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:abstract}",Goods_Abstract)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:intro}",Goods_Intro)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:goodsmodel}",Goods_GoodsModel)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:addtime}",Goods_AddTime)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:attribute}",Goods_Attribute)
		Pv_Content = KnifeCMS.Data.RegReplace(Pv_Content,"{goods:linkgoods}",Goods_LinkGoods)
		
		'解析自定义商品字段
		If KnifeCMS.IsStringExist(Pv_Content,"{goods:"& KnifeCMS.DB.FieldPre) Then
			RegExpObj.Pattern = "{goods:"& KnifeCMS.DB.FieldPre &"([\s\S]+?)}"
			Set Matches = RegExpObj.Execute(Pv_Content)
			For Each Match in Matches
				FieldName     = KnifeCMS.Data.HTMLEncode(Match.SubMatches(0))
				Pv_TempString = ""
				Fn_FieldType  = ""
				If FieldName<>"" Then
					FieldName = KnifeCMS.DB.FieldPre & FieldName
					If KnifeCMS.DB.IsColumnExist(DBTable_Goods,FieldName) Then
						'获取自定义字段类型
						Set Pv_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT FieldType FROM ["& DBTable_GoodsFieldSet &"] WHERE Field='"& FieldName &"'")
						If Not(Pv_Rs.Eof) Then
							Fn_FieldType = Pv_Rs("FieldType")
						End If
						KnifeCMS.DB.CloseRs Pv_Rs
						'获取自定义字段数据
						Set Pv_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT "& FieldName &" FROM ["& DBTable_Goods &"] WHERE [ID]="& Goods_ID &"")
						If Not(Pv_Rs.Eof) Then
							If Fn_FieldType="editor" Then
								Pv_TempString = KnifeCMS.Data.EditorContentDecode(Pv_Rs(0))
							Else
								Pv_TempString = KnifeCMS.Data.HTMLDecode(Pv_Rs(0))
							End If
						End If
						KnifeCMS.DB.CloseRs Pv_Rs
					End If
				End If
				Pv_Content = KnifeCMS.Data.ReplaceString(Pv_Content,Match.Value,Pv_TempString)
			Next
			Set Matches = Nothing
		End If
	End Function

	Public Function ParseIf(ByVal BV_Content)
		On Error Resume Next
		LabelRule="{if:([\s\S]+?)}([\s\S]*?){end\s+if}"
		If Not(KnifeCMS.Data.RegTest(BV_Content,LabelRule)) Then ParseIf=BV_Content : Exit Function
		Dim LabelRule2,LabelRule3
		Dim MatchIf,MatchesIf,StringIf,StringThen,StringThen1,StringElse1
		Dim IfFlag,ElseIfFlag,ResultString,StringElseIf,StringElseIfThen
		Dim ElseIf_Array,ElseIf_SubArray,ElseIf_ArrayLen,ElseIf_Len
		LabelRule2="{elseif"
		LabelRule3="{else}"
		ElseIfFlag=false
		regExpObj.Pattern=LabelRule
		set MatchesIf=regExpObj.Execute(BV_Content)
		for each MatchIf in MatchesIf 
			StringIf=MatchIf.SubMatches(0)
			StringThen=MatchIf.SubMatches(1)
			if Instr(StringThen,LabelRule2)>0 then
				ElseIf_Array=split(StringThen,LabelRule2)
				ElseIf_ArrayLen=ubound(ElseIf_Array)
				ElseIf_SubArray=split(ElseIf_Array(ElseIf_ArrayLen),LabelRule3)
				ResultString=ElseIf_SubArray(1)
				Execute("if "&StringIf&" then ResultString=ElseIf_Array(0)")
				for ElseIf_Len=1 to ElseIf_ArrayLen-1
					StringElseIf=GetSubStringByFromAndEnd(ElseIf_Array(ElseIf_Len),":","}","")
					StringElseIfThen=GetSubStringByFromAndEnd(ElseIf_Array(ElseIf_Len),"}","","start")
					Execute("if "&StringElseIf&" then ResultString=StringElseIfThen")
					Execute("if "&StringElseIf&" then ElseIfFlag=true else  ElseIfFlag=false")
					if ElseIfFlag then exit for
				next
				Execute("if "&GetSubStringByFromAndEnd(ElseIf_SubArray(0),":","}","")&" then ResultString=GetSubStringByFromAndEnd(ElseIf_SubArray(0),""}"","""",""start""):ElseIfFlag=true")
				BV_Content=Replace(BV_Content,MatchIf.value,ResultString)
			else 
				if Instr(StringThen,"{else}")>0 then 
					StringThen1=split(StringThen,LabelRule3)(0)
					StringElse1=split(StringThen,LabelRule3)(1)
					Execute("if "&StringIf&" then IfFlag=true else IfFlag=false")
					if IfFlag then BV_Content=Replace(BV_Content,MatchIf.value,StringThen1) else BV_Content=Replace(BV_Content,MatchIf.value,StringElse1)
				else
					Execute("if "&StringIf&" then IfFlag=true else IfFlag=false")
					if IfFlag then BV_Content=Replace(BV_Content,MatchIf.value,StringThen) else BV_Content=Replace(BV_Content,MatchIf.value,"")
				end if
			end if
			ElseIfFlag=false
		next
		If Err.number <> 0 Then Err.Clear
		ParseIf = BV_Content
		set MatchesIf=nothing
	End Function
	
	Public Function ParseSubIf(ByVal BV_Content)
		On Error Resume Next
		LabelRule="{subif:([\s\S]+?)}([\s\S]*?){end\s+subif}"
		If Not(KnifeCMS.Data.RegTest(BV_Content,LabelRule)) Then ParseSubIf=BV_Content : Exit Function
		Dim LabelRule2,LabelRule3
		Dim MatchIf,MatchesIf,StringIf,StringThen,StringThen1,StringElse1
		Dim IfFlag,ElseIfFlag,ResultString,StringElseIf,StringElseIfThen
		Dim ElseIf_Array,ElseIf_SubArray,ElseIf_ArrayLen,ElseIf_Len
		LabelRule2="{elsesubif"
		LabelRule3="{elsesub}"
		ElseIfFlag=false
		regExpObj.Pattern=LabelRule
		set MatchesIf=regExpObj.Execute(BV_Content)
		for each MatchIf in MatchesIf 
			StringIf=MatchIf.SubMatches(0)
			StringThen=MatchIf.SubMatches(1)
			if Instr(StringThen,LabelRule2)>0 then
				ElseIf_Array=split(StringThen,LabelRule2)
				ElseIf_ArrayLen=ubound(ElseIf_Array)
				ElseIf_SubArray=split(ElseIf_Array(ElseIf_ArrayLen),LabelRule3)
				ResultString=ElseIf_SubArray(1)
				Execute("if "&StringIf&" then ResultString=ElseIf_Array(0)")
				for ElseIf_Len=1 to ElseIf_ArrayLen-1
					StringElseIf=GetSubStringByFromAndEnd(ElseIf_Array(ElseIf_Len),":","}","")
					StringElseIfThen=GetSubStringByFromAndEnd(ElseIf_Array(ElseIf_Len),"}","","start")
					Execute("if "&StringElseIf&" then ResultString=StringElseIfThen")
					Execute("if "&StringElseIf&" then ElseIfFlag=true else  ElseIfFlag=false")
					if ElseIfFlag then exit for
				next
				Execute("if "&GetSubStringByFromAndEnd(ElseIf_SubArray(0),":","}","")&" then ResultString=GetSubStringByFromAndEnd(ElseIf_SubArray(0),""}"","""",""start""):ElseIfFlag=true")
				BV_Content=Replace(BV_Content,MatchIf.value,ResultString)
			else 
				if Instr(StringThen,"{elsesub}")>0 then 
					StringThen1=split(StringThen,LabelRule3)(0)
					StringElse1=split(StringThen,LabelRule3)(1)
					Execute("if "&StringIf&" then IfFlag=true else IfFlag=false")
					if IfFlag then BV_Content=Replace(BV_Content,MatchIf.value,StringThen1) else BV_Content=Replace(BV_Content,MatchIf.value,StringElse1)
				else
					Execute("if "&StringIf&" then IfFlag=true else IfFlag=false")
					if IfFlag then BV_Content=Replace(BV_Content,MatchIf.value,StringThen) else BV_Content=Replace(BV_Content,MatchIf.value,"")
				end if
			end if
			ElseIfFlag=false
		next
		If Err.number <> 0 Then Err.Clear
		ParseSubIf = BV_Content
		set MatchesIf=nothing
	End Function
	
	Public Function ParseComIf(ByVal BV_Content)
		On Error Resume Next
		LabelRule="{comif:([\s\S]+?)}([\s\S]*?){end\s+comif}"
		If Not(KnifeCMS.Data.RegTest(BV_Content,LabelRule)) Then ParseComIf=BV_Content : Exit Function
		Dim LabelRule2,LabelRule3
		Dim MatchIf,MatchesIf,StringIf,StringThen,StringThen1,StringElse1
		Dim IfFlag,ElseIfFlag,ResultString,StringElseIf,StringElseIfThen
		Dim ElseIf_Array,ElseIf_SubArray,ElseIf_ArrayLen,ElseIf_Len
		LabelRule2="{elsecomif"
		LabelRule3="{elsecom}"
		ElseIfFlag=false
		regExpObj.Pattern=LabelRule
		set MatchesIf=regExpObj.Execute(BV_Content)
		for each MatchIf in MatchesIf 
			StringIf=MatchIf.SubMatches(0)
			StringThen=MatchIf.SubMatches(1)
			if Instr(StringThen,LabelRule2)>0 then
				ElseIf_Array=split(StringThen,LabelRule2)
				ElseIf_ArrayLen=ubound(ElseIf_Array)
				ElseIf_SubArray=split(ElseIf_Array(ElseIf_ArrayLen),LabelRule3)
				ResultString=ElseIf_SubArray(1)
				Execute("if "&StringIf&" then ResultString=ElseIf_Array(0)")
				for ElseIf_Len=1 to ElseIf_ArrayLen-1
					StringElseIf=GetSubStringByFromAndEnd(ElseIf_Array(ElseIf_Len),":","}","")
					StringElseIfThen=GetSubStringByFromAndEnd(ElseIf_Array(ElseIf_Len),"}","","start")
					Execute("if "&StringElseIf&" then ResultString=StringElseIfThen")
					Execute("if "&StringElseIf&" then ElseIfFlag=true else  ElseIfFlag=false")
					if ElseIfFlag then exit for
				next
				Execute("if "&GetSubStringByFromAndEnd(ElseIf_SubArray(0),":","}","")&" then ResultString=GetSubStringByFromAndEnd(ElseIf_SubArray(0),""}"","""",""start""):ElseIfFlag=true")
				BV_Content=Replace(BV_Content,MatchIf.value,ResultString)
			else 
				if Instr(StringThen,"{elsecom}")>0 then 
					StringThen1=split(StringThen,LabelRule3)(0)
					StringElse1=split(StringThen,LabelRule3)(1)
					Execute("if "&StringIf&" then IfFlag=true else IfFlag=false")
					if IfFlag then BV_Content=Replace(BV_Content,MatchIf.value,StringThen1) else BV_Content=Replace(BV_Content,MatchIf.value,StringElse1)
				else
					Execute("if "&StringIf&" then IfFlag=true else IfFlag=false")
					if IfFlag then BV_Content=Replace(BV_Content,MatchIf.value,StringThen) else BV_Content=Replace(BV_Content,MatchIf.value,"")
				end if
			end if
			ElseIfFlag=false
		next
		If Err.number <> 0 Then Err.Clear
		ParseComIf = BV_Content
		set MatchesIf=nothing
	End Function
	
	Function GetSubStringByFromAndEnd(ByVal BV_String,ByVal BV_Start,ByVal BV_End,ByVal BV_OperType)
		Dim Fn_Loc1,Fn_Loc2
		Select Case BV_OperType
			Case "start" : Fn_Loc1=Instr(BV_String,BV_Start)+Len(BV_Start) : Fn_Loc2=Len(BV_String)+1
			Case "end"   : Fn_Loc1=1 : Fn_Loc2=Instr(Fn_Loc1,BV_String,BV_End)
			Case else    : Fn_Loc1=Instr(BV_String,BV_Start)+Len(BV_Start) : Fn_Loc2=Instr(Fn_Loc1,BV_String,BV_End)
		End Select
		GetSubStringByFromAndEnd=Mid(BV_String,Fn_Loc1,Fn_Loc2-Fn_Loc1) 
	End Function
	
End Class
%>