﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Response.Charset="UTF-8"
Response.ContentType="text/xml"


'###注意事项：　如果没有微信无法正常回复信息，请在手机上取消公众用户的关注，然后再加上试试。我就因为没有重新加，搞了好久也没找到问题出在哪。


'#接口申请时请去掉下面二行注释符
'response.write request("echostr")
'response.end 


'#转为UNIX时间格式
Function ToUnixTime(strTime, intTimeZone)
If IsEmpty(strTime) or Not IsDate(strTime) Then strTime = Now
If IsEmpty(intTimeZone) or Not isNumeric(intTimeZone) Then intTimeZone = 0
ToUnixTime = DateAdd("h",-intTimeZone,strTime)
ToUnixTime = DateDiff("s","1970-1-1 0:0:0", ToUnixTime)
End Function


			set xml_dom = Server.CreateObject("MSXML2.DOMDocument")
			
			'接收微信用户发送的信息
			xml_dom.load request
			
			'模拟微信用户发送信息，可用在本地网页调试。请去掉注释，用你接收的XML替换掉相应部分
			'xml_dom.loadxml "<xml><ToUserName><![CDATA[gh_33bb5907f91e]]></ToUserName><FromUserName><![CDATA[ojLh3jkYtiszyEY-_sM_8yrNxSc4]]></FromUserName><CreateTime>1363228874</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA[花园酒店]]></Content><MsgId>1234567890123</MsgId></xml>"
			

			ToUserName=xml_dom.getelementsbytagname("ToUserName").item(0).text	'微信公众号
			FromUserName=xml_dom.getelementsbytagname("FromUserName").item(0).text	'用户微信
			CreateTime=xml_dom.getelementsbytagname("CreateTime").item(0).text
			MsgType=xml_dom.getelementsbytagname("MsgType").item(0).text
			content=xml_dom.getelementsbytagname("Content").item(0).text
			MsgId=xml_dom.getelementsbytagname("MsgId").item(0).text			
			
			
			'#微信返回回复信息,自定义时，可直接判断content的内容来返回不同的回复
			strsend="您好！"&content
			
			
			'#以下为调试结果
			'#调试接收到的微信信处
			temp=vbcrlf	&vbcrlf	&vbcrlf	&"####以下内容为接收#####"&vbcrlf			
			temp=temp&"<xml>"&vbcrlf
			temp=temp&"<ToUserName><![CDATA["&ToUserName&"]]></ToUserName>"&vbcrlf   
			temp=temp&"<FromUserName><![CDATA["&FromUserName&"]]></FromUserName>"&vbcrlf 
			temp=temp&"<CreateTime>"&CreateTime&"</CreateTime>"&vbcrlf 
			temp=temp&"<MsgType><![CDATA[text]]></MsgType>"&vbcrlf
			temp=temp&"<Content><![CDATA["&content&"]]></Content>"&vbcrlf 
			temp=temp&"<MsgId>"&MsgId&"</MsgId>"&vbcrlf
			temp=temp&"</xml>"&vbcrlf&vbcrlf
			temp=temp&"####以下内容为配置接口返回内容#####"&vbcrlf
			
			
			'#调试返回的回复信息
			strresponse="<xml>"&vbcrlf
			strresponse=strresponse&"<ToUserName><![CDATA["&FromUserName&"]]></ToUserName>"&vbcrlf 
			strresponse=strresponse&"<FromUserName><![CDATA["&ToUserName&"]]></FromUserName>"&vbcrlf
			strresponse=strresponse&"<CreateTime>"&ToUnixTime(now,+8)&"</CreateTime>"&vbcrlf
			strresponse=strresponse&"<MsgType><![CDATA[text]]></MsgType>"&vbcrlf
			strresponse=strresponse&"<Content><![CDATA["&strsend&"]]></Content>"&vbcrlf
			strresponse=strresponse&"<FuncFlag>0</FuncFlag>"&vbcrlf
			strresponse=strresponse&"</xml>"
			response.write strresponse


			'#把调试信息写入到log.txt文件
			filepath=server.mappath(".")&"\log.txt"
			Set fso = Server.CreateObject("scripting.FileSystemObject")
			set fopen=fso.OpenTextFile(filepath, 8 ,true)
			fopen.writeline(temp&strresponse)
			set fso=nothing
			set fopen=Nothing






%>
