<%
'文件操作类

Class Class_KnifeCMS_FSO

	Private Pv_FolderOrder
	Private Pv_CharSet,Pv_FSOName,Pv_FSO,Pv_OverWrite
	
	Private Sub Class_Initialize()
		Pv_CharSet    = KnifeCMS.CharSet
		Pv_FSOName    = KnifeCMS.FSOName
		Pv_OverWrite  = True
		Set Pv_FSO    = Server.CreateObject(Pv_FSOName)
		KnifeCMS.Error(50) = "Folder path error."'文件夹路径错误.
		KnifeCMS.Error(51) = "File path error."'文件路径错误.
		KnifeCMS.Error(52) = "Create folder error."'创建文件夹错误.
		KnifeCMS.Error(53) = "Create file error."'创建文件错误.
		KnifeCMS.Error(54) = "File does not Exist."'文件不存在.
		KnifeCMS.Error(55) = "Failed to save file."'保存文件失败.
	End Sub
	
	Private Sub Class_Terminate()
		Set Pv_FSO    = nothing
	End Sub
	
	Public Property Let [CharSet](ByVal BV_String) Pv_CharSet = Ucase(BV_String) : End Property
	Public Property Get [CharSet]() [CharSet] = Pv_CharSet : End Property
	Public Property Let OverWrite(ByVal BV_Bool) Pv_OverWrite = BV_Bool : End Property
	Public Property Get OverWrite() OverWrite = Pv_OverWrite : End Property
	Public Property Get ObjectFSO() Set ObjectFSO = Pv_FSO : End Property
	
	Public Function CheckFSOInstalled()
		If KnifeCMS.IsFSOInstalled = False Then
			KnifeCMS.Error.Msg = "KnifeCMS.FSO.CheckFSOInstalled()"
			KnifeCMS.Error.Raise 1 '系统服务器不支持Scripting.FileSystemObject.
			Err.Clear()
		End If
	End Function
	
	'文件是否存在
	Public Function FileExists(ByVal BV_FilePath)
		BV_FilePath = ABSPath(BV_FilePath)
		If KnifeCMS.IsFSOInstalled Then
			If Pv_FSO.FileExists(BV_FilePath) Then FileExists = True
		Else
			FileExists = False
			KnifeCMS.Error.Msg = "KnifeCMS.FSO.FileExists("""& BV_FilePath &""")"
			KnifeCMS.Error.Raise 1
			Err.Clear()
		End If
	End Function
	
	'文件夹是否存在
	Public Function FolderExists(ByVal BV_FolderPath)
		BV_FolderPath = ABSPath(BV_FolderPath)
		If KnifeCMS.IsFSOInstalled Then
			If Pv_FSO.FolderExists(BV_FolderPath) Then FolderExists = True
		Else
			FolderExists = False
			KnifeCMS.Error.Msg = "KnifeCMS.FSO.FolderExists("""& BV_FolderPath &""")"
			KnifeCMS.Error.Raise 1
			Err.Clear()
		End If
	End Function
	
	'绝对路径: E:\web\KnifeCMS\wwwroot\data\javascript\region.json.js
	'相对路径: data/javascript/region.json.js
	'*******************************************
	'返回绝对路径(absolute path) 无论BV_Path是相对路径还是绝对路径都转化为绝对路径,如果文件夹路径为空而且是"/////"或"\\\\\"形式的错误路径则返回空字符串
	'*******************************************
	Public Function ABSPath(ByVal BV_Path)
		Dim Fn_Path
		Fn_Path = BV_Path
		Fn_Path = Replace(Fn_Path,"*","")
		Fn_Path = Replace(Fn_Path,"?","")
		Fn_Path = Replace(Fn_Path,"""","")
		Fn_Path = Replace(Fn_Path,"|","")
		Fn_Path = Replace(Fn_Path,"<","")
		Fn_Path = Replace(Fn_Path,">","")
		
		If KnifeCMS.Data.IsNul(Fn_Path) Or KnifeCMS.Data.IsNul(Replace(Fn_Path,"/","")) Or KnifeCMS.Data.IsNul(Replace(Fn_Path,"\","")) Then ABSPath = "" : Exit Function
		If Mid(Fn_Path,2,1)<>":" Then
			Fn_Path = Replace(Fn_Path,"\","/")
			Fn_Path = KnifeCMS.Data.RegReplace(Fn_Path,"[/]+","/")
			If Left(Fn_Path,1)="/" Then Fn_Path = Mid(Fn_Path,2)
			Fn_Path = Server.MapPath(SystemPath & Fn_Path)
		Else
			Fn_Path = KnifeCMS.Data.RegReplace(Fn_Path,"[/]+","/")
		End If
		If Right(Fn_Path,1)="\" Then Fn_Path=Left(Fn_Path,Len(Fn_Path)-1)
		ABSPath = Fn_Path
	End Function
	
	'保存文件
	Public Function SaveTextFile(ByVal BV_FilePath, ByVal BV_Text)
		SaveTextFile = SaveTextFileByADO(BV_FilePath,BV_Text)
	End Function
	
	'保存文本文件(直接覆盖保存)[会改变文件的读写权限]
	'如果文件不存在则自动创建
	Public Function SaveTextFileByADO(ByVal BV_FilePath, ByVal BV_FileContent)
		On Error Resume Next
		If KnifeCMS.Data.IsNul(BV_FilePath) Then SaveTextFileByADO=False : Exit Function
		Dim Fn_ADO,Fn_FilePath,Fn_FolderPath,Fn_Result
		Fn_FilePath = ABSPath(BV_FilePath)
		'检查文件夹是否存在(不存在则创建)
		Fn_FolderPath = Left(Fn_FilePath,InstrRev(Fn_FilePath,"\")-1)
		If FolderExists(Fn_FolderPath) = False Then
			Fn_FolderPath = CreateFolder(Fn_FolderPath)
		End If
		If Fn_FolderPath = False Then SaveTextFileByADO = False : Exit Function
		
		'开始写入文件
		Set Fn_ADO = Server.CreateObject("ADODB.Stream")
		With Fn_ADO
			.Mode = 3
			.Type = 2
			.Open
			.CharSet   = Pv_CharSet
			.Position  = 0
			.WriteText = BV_FileContent
			.SaveToFile Fn_FilePath,2
			.Flush
			.Close
		End With
		Set Fn_ADO = Nothing
		Fn_Result = True
		If Err.Number<>0 Then
			Fn_Result = False
			KnifeCMS.Error.Msg = Fn_FilePath
			KnifeCMS.Error.Raise 55
		End If
		Err.Clear()
		SaveTextFileByADO = Fn_Result
	End Function
	
	'保存文本文件(先打开再保存,修改config.main.asp等文件时采用此方式)[不会改变文件的读写权限]
	Public Function SaveTextFileByFSO(ByVal BV_FileABSPath,ByVal BV_Text)
		On Error Resume Next
		Dim Fn_FSO,Fn_File
		Set Fn_FSO  = Server.CreateObject("Scripting.FileSystemObject")     
		Set Fn_File = Fn_FSO.OpenTextFile(BV_FileABSPath,2,True)'文件不存在时则创建
		Fn_File.Write BV_Text
		Set Fn_File = Nothing
		Set Fn_FSO  = Nothing
		If Err.Number<>0 Then
			SaveTextFileByFSO = False
		Else
			SaveTextFileByFSO = True
		End If
		Err.Clear()
	End Function
	
	'创建文件夹
	'{BV_FolderPath}指定路径
	'系统不支持FSO   则程序终止,提示系统不支持FSO(Lang_ObjFSONotInstalled)
	'创建失败则返回False   创建成功则返回正确的绝对路径Fn_OkPath(E:\wwwoot\CMS\ASP\KnifeCMS\wwwroot\attachment\image\201012\27)
	Function CreateFolder(ByVal BV_FolderPath)
		Call CheckFSOInstalled()
		Dim Fn_OkPath,Fn_LocalPath_ii,Fn_FolderPath
		CreateFolder  = False
		Fn_FolderPath = ABSPath(BV_FolderPath)
		
		Dim Fn_ParentFolderExist : Fn_ParentFolderExist = False
		Dim Fn_FolderExist       : Fn_FolderExist       = False
		
		If Not(KnifeCMS.Data.IsNul(Fn_FolderPath)) Then'如果路径有效
			Fn_FolderPath=Split(Fn_FolderPath,"\")
			For Fn_LocalPath_ii=0 to Ubound(Fn_FolderPath)

				If Fn_LocalPath_ii=0 Then
					Fn_OkPath = Trim(Fn_FolderPath(Fn_LocalPath_ii))
				Else
					If Trim(Fn_FolderPath(Fn_LocalPath_ii))<>"" Then
						Fn_OkPath = Fn_OkPath &"\"& Trim(Fn_FolderPath(Fn_LocalPath_ii))
				    End If
				End If

				If Fn_LocalPath_ii>0 Then
				
					'判断是否对系统文件有读取权限
					If Fn_ParentFolderExist=False Then
						If Not Pv_FSO.FolderExists(Fn_OkPath)=False Then
							Fn_ParentFolderExist = True
						End If
					End If

					'对系统文件有读取权限则继续判断子文件夹是否存在(不存在则创建)
					If Fn_ParentFolderExist=True Then
						On Error Resume Next
						'如果文件不存在则创建文件夹
						If Pv_FSO.FolderExists(Fn_OkPath)=False Then
							Call CreateFolderByFSO(Fn_OkPath)
						End If
						If Err.Number<>0 Then
							KnifeCMS.Error.Msg = Fn_OkPath
							KnifeCMS.Error.Raise 50
							Err.Clear()
						End If
					End If
				End If
			Next
			CreateFolder = Fn_OkPath
		End If
	End Function
	
	'通FSO创建文件夹,创建成功返回True,创建失败返回False
	Function CreateFolderByFSO(ByVal BV_FolderPath)
		Err.Clear()
		On Error Resume Next
		Dim Fn_Result : Fn_Result = True
		Pv_FSO.CreateFolder(BV_FolderPath)
		If Err.Number<>0 Then
			Fn_Result = False
		End If
		Err.Clear()
		CreateFolderByFSO = Fn_Result
	End Function
	
	'[BV_FileABSPath:要删除的文件的绝对路径]删除成功返回true,文件不存在或删除失败返回false
	Public Function DeleteFile(ByVal BV_FileABSPath)
		Dim Fn_Result : Fn_Result = True
		On Error Resume Next
		Pv_FSO.DeleteFile(BV_FileABSPath)
		If Err.Number<>0 Then
			Fn_Result = False
		Else
			Fn_Result = True
		End If
		Err.Clear()
		DeleteFile = Fn_Result
	End Function
	
	'删除文件夹
	Public Function DeleteFolder(ByVal BV_FileABSPath)
		Dim Fn_Result : Fn_Result = True
		On Error Resume Next
		Pv_FSO.DeleteFolder(BV_FileABSPath)
		If Err.Number<>0 Then
			Fn_Result = False
		Else
			Fn_Result = True
		End If
		Err.Clear()
		DeleteFolder = Fn_Result
	End Function
	
	'判断文件是否可写入(BV_Path为文件的绝对路径)
	Public Function FileWriteable(ByVal BV_Path)
		On Error Resume Next
		Dim BV_FileABSPath : BV_FileABSPath = BV_Path
		Dim Fn_ADO
		Set Fn_ADO = Server.CreateObject("ADODB.Stream")
		With Fn_ADO
			.Mode = 3
			.Type = 2 '以本模式读取
			.Open
			.CharSet   = Pv_CharSet
			.LoadFromFile BV_FileABSPath
			.SaveToFile BV_FileABSPath,2
			.Close
		End With
		Set Fn_ADO = Nothing
		FileWriteable = True
		If Err.Number<>0 Then FileWriteable = False
		Err.Clear()
	End Function
	
	'判断文件夹是否可写入(BV_ABSPath为文件夹的绝对路径)
	Public Function FolderWriteable(ByVal BV_ABSPath)
		On Error Resume Next
		Dim Fn_FileABSPath,Fn_Content : Fn_Content = "writeable "& SysTime
		If Right(BV_ABSPath,1)="\" Then
			Fn_FileABSPath = BV_ABSPath & "testwriteable.txt"
		Else
			Fn_FileABSPath = BV_ABSPath &"\"& "testwriteable.txt"
		End If
		Dim Fn_ADO
		Set Fn_ADO = Server.CreateObject("ADODB.Stream")
		With Fn_ADO
			.Mode = 3
			.Type = 2
			.Open
			.CharSet   = Pv_CharSet
			.Position  = 0
			.WriteText = Fn_Content
			.SaveToFile Fn_FileABSPath,2
			.Close
		End With
		Set Fn_ADO = Nothing
		If Err.Number<>0 Then
			FolderWriteable = False
		Else
			If FileExists(Fn_FileABSPath) Then
				FolderWriteable = True
				Call DeleteFile(Fn_FileABSPath)
			Else
				FolderWriteable = False
			End If
		End If
		Err.Clear()
	End Function
	
	'获取文件夹列表数据
	Public Function FolderList(ByVal BV_ReturnType,ByVal BV_FolderPath)
		Dim Fn_i,Fn_FSO,Fn_Folder,Fn_SubFolder,Fn_FolderPath,Fn_Json,Fn_html,Fn_Temp

		Fn_FolderPath = KnifeCMS.Data.FolderPath(BV_FolderPath)

		Set Fn_FSO = KnifeCMS.FSO.ObjectFSO
		If Fn_FSO.FolderExists(Server.MapPath(SystemPath & Fn_FolderPath))=False Then
			FolderList = "{""foldername"":"""& KnifeCMS.Data.FolderName(Fn_FolderPath) &""",""size"":""0"",""datetime"":"""",""folderpath"":"""& Fn_FolderPath &""",""subfolderscount"":""0"",""subfolders"":[],""folderexists"":""false""}"
			Exit Function
		End If
		
		Fn_i = 0
		Set Fn_Folder=Fn_FSO.GetFolder(Server.MapPath(SystemPath & Fn_FolderPath))
		For Each Fn_SubFolder in Fn_Folder.SubFolders
			Fn_i    = Fn_i + 1
			Fn_Temp = "{""foldername"":"""& Fn_SubFolder.name &""",""size"":"""& KnifeCMS.Data.FormatSize(Fn_SubFolder.Size) &""",""datetime"":"""& Fn_SubFolder.datelastmodified &""",""folderpath"":"""& Fn_FolderPath & Fn_SubFolder.name &""",""subfolderscount"":"""& Fn_SubFolder.SubFolders.Count &""",""subfolders"":[],""folderexists"":""true""}"
			If Fn_Json = "" Then
				Fn_Json = Fn_Temp
			Else
				Fn_Json = Fn_Json &","& Fn_Temp
			End If
		Next
		Fn_Json = "{""foldername"":"""& KnifeCMS.Data.FolderName(Fn_FolderPath) &""",""size"":"""& KnifeCMS.Data.FormatSize(Fn_Folder.Size) &""",""datetime"":"""& Fn_Folder.datelastmodified &""",""folderpath"":"""& Fn_FolderPath &""",""subfolderscount"":"""& Fn_Folder.SubFolders.Count &""",""subfolders"":["& Fn_Json &"],""folderexists"":""true""}"
		
		Set Fn_Folder = Nothing
		Set Fn_FSO    = Nothing
		
		Select Case LCase(BV_ReturnType)
		Case "json" : FolderList = Fn_Json
		Case "html" : FolderList = Fn_html
		End Select
	End Function
	
	'获取文件列表数据
	'FileList("json","attachment/image/201302/27/")
	Public Function FileList(ByVal BV_ReturnType,ByVal BV_FolderPath)
		Dim Fn_i,Fn_FSO,Fn_File,Fn_Folder,Fn_FolderPath,Fn_Json,Fn_Array,Fn_html,Fn_Temp

		Fn_FolderPath = KnifeCMS.Data.FolderPath(BV_FolderPath)

		Set Fn_FSO = KnifeCMS.FSO.ObjectFSO
		If Fn_FSO.FolderExists(Server.MapPath(SystemPath & Fn_FolderPath))=False Then
			FileList = "{""foldername"":"""& KnifeCMS.Data.FolderName(Fn_FolderPath) &""",""size"":""0"",""datetime"":"""",""folderpath"":"""& Fn_FolderPath &""",""filecount"":""0"",""files"":[],""folderexists"":""false""}"
			Exit Function
		End If
		
		Set Fn_Folder=Fn_FSO.GetFolder(Server.MapPath(SystemPath & Fn_FolderPath))
		Fn_i = 0
		ReDim Fn_Array(Fn_Folder.Files.Count)
		For Each Fn_File in Fn_Folder.Files
			Fn_Array(Fn_i) = Fn_File.name
			Fn_i    = Fn_i + 1
			Fn_Temp = "{""filename"":"""& Fn_File.name &""",""size"":"""& KnifeCMS.Data.FormatSize(Fn_File.Size) &""",""datetime"":"""& Fn_File.datelastmodified &""",""filepath"":"""& Fn_FolderPath & Fn_File.name &"""}"
			If Fn_Json = "" Then
				Fn_Json = Fn_Temp
			Else
				Fn_Json = Fn_Json &","& Fn_Temp
			End If
		Next
		Fn_Json = "{""foldername"":"""& KnifeCMS.Data.FolderName(Fn_FolderPath) &""",""size"":"""& KnifeCMS.Data.FormatSize(Fn_Folder.Size) &""",""datetime"":"""& Fn_Folder.datelastmodified &""",""folderpath"":"""& Fn_FolderPath &""",""filecount"":"""& Fn_Folder.Files.Count &""",""files"":["& Fn_Json &"]}"
		
		Set Fn_Folder = Nothing
		Set Fn_FSO    = Nothing
		
		Select Case LCase(BV_ReturnType)
		Case "json" : FileList = Fn_Json
		Case "array" : FileList = Fn_Array
		Case "html" : FileList = Fn_html
		End Select
	End Function
	
	'拷贝文件
	Public Function CopyFile(ByVal BV_FromFilePath,ByVal BV_ToFilePath)
		Err.Clear()
		On Error Resume Next
		Dim Fn_Result : Fn_Result = True
		If BV_FromFilePath="" Or BV_ToFilePath="" Then CopyFile=False : Exit Function
		Pv_FSO.CopyFile ABSPath(BV_FromFilePath),ABSPath(BV_ToFilePath)
		If Err.Number<>0 Then
			Fn_Result = False
			Err.Clear()
		End If
		CopyFile = Fn_Result
	End Function
	
	'下一级文件夹列表
	Public Function SecFolderList(ParID,FolderPath,FileType)
		Dim Fn_FolderPath,Fn_FSO,Fn_Fold,Fn_FileItem,Fn_i
		    Fn_i=0
		Fn_FolderPath=KnifeCMS.Data.URLDecode(FolderPath)
		Fn_FolderPath=FolderPath
		
		'**把文件夹路径处理成 attachment/image/ 形式
		Fn_FolderPath = KnifeCMS.Data.RegReplace(Fn_FolderPath,"[/]+","/")
		If Left(Fn_FolderPath,1) = "/" Then Fn_FolderPath = Mid(Fn_FolderPath,2)
		If Right(Fn_FolderPath,1) <> "/" Then Fn_FolderPath = Fn_FolderPath &"/"
		
		'**过滤禁止的文件夹字符
		Fn_FolderPath=Replace(Fn_FolderPath,"\","")
		Fn_FolderPath=Replace(Fn_FolderPath,":","")
		Fn_FolderPath=Replace(Fn_FolderPath,"*","")
		Fn_FolderPath=Replace(Fn_FolderPath,"?","")
		Fn_FolderPath=Replace(Fn_FolderPath,"""","")
		Fn_FolderPath=Replace(Fn_FolderPath,"|","")
		Fn_FolderPath=Replace(Fn_FolderPath,"<","")
		Fn_FolderPath=Replace(Fn_FolderPath,">","")
		'**
		
		Set Fn_FSO = KnifeCMS.FSO.ObjectFSO
		If Fn_FSO.FolderExists(Server.MapPath(SystemPath&Fn_FolderPath))=False Then
			Exit Function
		End If
		
		set Fn_Fold=Fn_FSO.getfolder(Server.MapPath(SystemPath&Fn_FolderPath))
		for each Fn_FileItem in Fn_Fold.subfolders
			Pv_FolderOrder=Pv_FolderOrder+1
			Echo "d.add("& Pv_FolderOrder &","&ParID&","""& Fn_FileItem.name &""",""javascript:GetFiles('"& FileType &"','"& Fn_FolderPath & Fn_FileItem.name &"')"");"&vbCrLf
		next
		set Fn_Fold=nothing
		set Fn_FSO=Nothing
	End Function

	' $生成树型文件夹目录
	' {FolderPath} 文件夹路径
	' {RootName} 根目录名称
	' {FileType} 文件类型
	Public Function TreeFolderList(FolderPath,RootName,FileType)
		Dim Fn_FolderPath,Fn_FSO,Fn_Fold,Fn_FileItem,Fn_i
		    Fn_i=0

		Fn_FolderPath = KnifeCMS.Data.FolderPath(FolderPath)
		
		Set Fn_FSO = KnifeCMS.FSO.ObjectFSO
		If Fn_FSO.FolderExists(Server.MapPath(SystemPath&Fn_FolderPath))=False Then
			Echo "FolderPath is Wrong ："&Fn_FolderPath
			Exit Function
		End If
		Set Fn_Fold=Fn_FSO.getfolder(Server.MapPath(SystemPath&Fn_FolderPath))
		Echo "<script type=""text/javascript"">"&vbCrLf
		Echo "d = new dTree('d');"&vbCrLf
		Echo "d.add(0,-1,"""&RootName&""",""javascript:GetFiles('"& FileType &"','"& Fn_FolderPath &"')"");"&vbCrLf
		for each Fn_FileItem in Fn_Fold.subfolders
			Fn_i=Fn_i+1
			Echo "d.add("&Fn_i&",0,"""& Fn_FileItem.name &""",""javascript:GetFiles('"& FileType &"','"& Fn_FolderPath & Fn_FileItem.name &"')"");"&vbCrLf
		next
		Pv_FolderOrder=Fn_i
		Fn_i=0
		'**获取下一级文件夹
		for each Fn_FileItem in Fn_Fold.subfolders
			Fn_i=Fn_i+1
			Pv_FolderOrder=Pv_FolderOrder+1
			Call SecFolderList(Fn_i,Fn_FolderPath & Fn_FileItem.name,FileType)
		next
		Echo "document.write(d);"&vbCrLf
		Echo "</script>"&vbCrLf
		set Fn_Fold=nothing
		set Fn_FSO=Nothing
	End Function
	
	
End Class
%>