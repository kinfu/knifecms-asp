<%
'缓存类

Class Class_KnifeCMS_Cache

	Public Pv_CacheTime,Pu_VCacheFlag,Pv_CreateCacheByAdmin
	Private Pv_CacheFilePath
	Private Pv_CacheName,Pv_CacheData,Pv_CacheNum,Pv_CreateCacheNum

	Private Sub Class_Initialize()
		Pv_CacheTime      = CacheTime
		Pu_VCacheFlag     = CacheFlag
		Pv_CacheNum       = 200
		Pv_CreateCacheNum = 0
		Pv_CreateCacheByAdmin = False
		KnifeCMS.Error(60) = "Please set cache name."'请设置缓存名称.
		KnifeCMS.Error(61) = "Cache value not exist."'缓存值不存在.
		KnifeCMS.Error(62) = "Save cache file error, please check cache folder is writable and can delete."'保存缓存文件失败，请检查缓存目录是否有写入和删除权限.
	End Sub
	Private Sub Class_Terminate()
	End Sub
	Public Property Let CacheTime(ByVal BV_CacheTime)
		Pv_CacheTime = BV_CacheTime
	End Property
	Public Property Get CacheTime
		CacheTime = Pv_CacheTime
	End Property
	
	Public Property Let CacheFilePath(ByVal BV_Path)
		Pv_CacheFilePath = BV_Path
	End Property
	Public Property Get CacheFilePath
		CacheFilePath = Pv_CacheFilePath
	End Property
	
	Public Property Let CreateCacheByAdmin(ByVal BV_CBool)
		Pv_CreateCacheByAdmin = BV_CBool
	End Property
	Public Property Get CreateCacheByAdmin
		CreateCacheByAdmin = Pv_CreateCacheByAdmin
	End Property
	
	'判断静态缓存页面是否已过期
	Public Function CheckCache(ByVal BV_CacheName)
		CheckCache = CheckCacheFile(BV_CacheName)
	End Function
	
	'判断静态缓存页面是否已过期
	Public Function CheckCacheFile(ByVal BV_CacheName)
		On Error Resume Next
		Pv_CacheName=LCase(BV_CacheName)
		Dim Fn_CachePath,Fn_File
		Dim Fn_Result : Fn_Result = False
		If Pv_CacheName<>"" Then
			Pv_CacheName = Pv_CacheFilePath & Pv_CacheName
			Fn_CachePath = Server.MapPath(Pv_CacheName)
			If KnifeCMS.FSO.FileExists(Fn_CachePath) Then
				If Pv_CreateCacheByAdmin = False Then 
					Set Fn_File=KnifeCMS.FSO.ObjectFSO.GetFile(Fn_CachePath)
					If DateDiff("s",CDate(Fn_File.DateLastModified),Now()) < Pv_CacheTime Then
						Fn_Result = True
					End If
					Set Fn_File=Nothing
				Else
					Fn_Result = False
				End If
			End If
		End If
		CheckCacheFile = Fn_Result
	End Function
	
	'获取静态缓存页面
	Public Function GetCache(ByVal BV_CacheName)
		GetCache = GetCacheFile(BV_CacheName)
	End Function

	'保存静态缓存页面
	Public Function SetCache(ByVal BV_CacheName,ByVal BV_CacheValue)
		Call SetCacheFile(BV_CacheName,BV_CacheValue,true)
	End Function
	
	'保存静态缓存页面
	Public Function SetCacheFile(ByVal BV_CacheName,ByVal BV_CacheValue,ByVal BV_OverWrite)
		'On Error Resume Next
		Dim Fn_Temp
		Pv_CacheName=LCase(BV_CacheName)
		If Pv_CacheName<>"" Then
			Pv_CacheName = Pv_CacheFilePath & Pv_CacheName
			Fn_Temp      = KnifeCMS.FSO.OverWrite
			KnifeCMS.FSO.OverWrite = BV_OverWrite
			If KnifeCMS.FSO.SaveTextFile(Pv_CacheName,BV_CacheValue) Then
				Pv_CreateCacheNum = Pv_CreateCacheNum + 1
			Else
				KnifeCMS.Error.Msg = "KnifeCMS.Cache.SetCacheFile: "& BV_CacheName
				KnifeCMS.Error.Raise 61
				Err.Clear
			End If
			KnifeCMS.FSO.OverWrite = Fn_Temp
		Else
			KnifeCMS.Error.Msg = "KnifeCMS.Cache.SetCacheFile: "& BV_CacheName
			KnifeCMS.Error.Raise 60
			Err.Clear
		End If
	End Function
	
	'获取静态缓存页面
    Public Function GetCacheFile(ByVal BV_CacheName)
		On Error Resume Next
		Dim Fn_Text
		Pv_CacheName=LCase(BV_CacheName)
		If Pv_CacheName<>"" Then
			Pv_CacheName = Pv_CacheFilePath & Pv_CacheName
			Fn_Text = KnifeCMS.Read(SystemPath & Pv_CacheName)
		End If
		GetCacheFile = Fn_Text
	End Function

	Public Sub ClearCache(ByVal BV_CacheName)
		Pv_CacheName=LCase(BV_CacheName)
		Application.Lock
		Application(Pu_VCacheFlag & Pv_CacheName) = Empty
		Application.unLock
	End Sub
	
	Public Function ClearAll()
		Application.Lock()
		Application.Contents.RemoveAll()
		Application.UnLock()
	End Function

	Public Function HavedTime(Byval BV_CacheName)
		Pv_CacheName=LCase(BV_CacheName)
		If Pv_CacheName<>"" Then
			Pv_CacheData=Application(Pu_VCacheFlag & Pv_CacheName)
			If IsArray(Pv_CacheData) Then
				HavedTime=Pv_CacheData(1)
			Else
				Die "Error_CacheValue"
			End If
		Else
			Die "Error_CacheName"
		End If
	End Function

	Public Function ShowAllCache()
		Dim Fn_ACon
		Echo "CACHE_OBJECT_NUM: "& GetCacheNum() &"<br><br>"
		for each Fn_ACon in Application.Contents
			Echo Fn_ACon &"<br>"
		next
	End Function

	Public Function GetCacheNum()
		GetCacheNum = application.Contents.Count
	End Function
	
	Public Function CreateCacheNum()
		CreateCacheNum = Pv_CreateCacheNum
	End Function

	Public Function ClearOtherCache()
		Dim Fn_i,Fn_ACon,Fn_OtherNum
		Fn_OtherNum = GetCacheNum - Pv_CacheNum
		Fn_i = 0
		For Each Fn_ACon In Application.Contents
			Application.Lock()
			application.contents.remove(Fn_ACon)
			Application.UnLock()
			Fn_i = Fn_i + 1
			If Fn_i=Fn_OtherNum Then Exit For
		Next
	End Function
End Class
%>