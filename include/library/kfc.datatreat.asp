<%
'数据处理类

Class Class_VBS_Function
	Public Function CBool_(ByVal BV_Str)
		CBool_ = CBool(BV_Str)
	End Function
	Public Function Left_(ByVal BV_Str, ByVal BV_Length)
		Left_ = Left(BV_Str,BV_Length)
	End Function
	Public Function Int_(ByVal BV_Str)
		Int_ = Int(BV_Str)
	End Function
	Public Function CLng_(ByVal BV_Str)
		CLng_ = CLng(BV_Str)
	End Function
	Public Function FormatCurrency_(ByVal BV_Str)
		FormatCurrency_ = FormatCurrency(BV_Str,2,-1)
	End Function
	Public Function DateDiff_(ByVal BV_Type, ByVal BV_STime, ByVal BV_ETime)
		Dim Fn_Result
		Fn_Result = DateDiff(BV_Type,BV_STime,BV_ETime)
		DateDiff_ = Fn_Result
	End Function
End Class

Class Class_KnifeCMS_Data

	private TmpStr
	Private Pv_Arr,Pv_ArrLen
	Private Pv_RegExp,VBS
	
	Private Sub Class_Initialize()
		Set Pv_RegExp = New RegExp
		Set VBS = New Class_VBS_Function
	End Sub
	
	Private Sub Class_Terminate()
		Set Pv_RegExp = Nothing
		Set VBS = Nothing
	End Sub
	
	'作用: 基本过滤
	Public Function CheckStr(BV_Str)
		If Isnull(BV_Str) or BV_Str="" Then
			CheckStr = ""
			Exit Function 
		End If
		BV_Str = Replace(BV_Str,Chr(0),"")'Chr(0)是结束符
		CheckStr = Replace(BV_Str,"'","''") '// 数据库库操作时会自动将英文''处理为'
	End Function
	
	'$ 函数: CheckRuntime(TimeStr)
	'  作用: 只用做处理系统运行时间的函数
	' sTime: 系统运行时间 单位(s)
	Public Function CheckSystemRuntime(TimeStr)
		TimeStr=FormatNumber(TimeStr,4)
		If IsNull(TimeStr) Or TimeStr="" Then
			CheckSystemRuntime="0.00"
		Else
			If Instr(1,TimeStr,".") <= 1 then TimeStr= "0" & TimeStr
			CheckSystemRuntime=TimeStr
		End If
	End Function
	
	Public Function Cbool(BV_Str)
		On Error Resume Next
		BV_Str=VBS.CBool_(BV_Str)
		If Err.number <> 0 Then
			BV_Str=False
			Err.Clear
		End If
		[Cbool]=BV_Str
	End Function
	
	'**************************************************
	' $函数：ClearSameDatas(ArrDatas,ShowResultArrDatas)
	'  作用：去除数组同相同的值
	'        [方法]把ArrDatas中的各个值存进数组tmpArr并去除相同的值,然后把数组tmpArr中有效的值存入最后的数组ArrResult
	'  参数：{ArrDatas} 数组或对象集合
	'        {ShowResultArrDatas}是否显示处理后数组的值 True / False
	'  结果: 不出错则 返回处理后的数组
	'        出错或ArrDatas为空     返回的数组只有一个值，且ClearSameDatas(0)="$err$"
	'**************************************************
	Public Function ClearSameDatas(ByVal ArrDatas,ByVal ShowResultArrDatas)
		ShowResultArrDatas=Cbool(ShowResultArrDatas)
		Dim tmpStr,Num,Num_i,Datai,ArrResult,tmpArr
			Num=0
			Num_i=0
		If IsArray(ArrDatas) Then'如果ArrDatas是数组
			Redim tmpArr(Ubound(ArrDatas)+1)
			For Datai=0 to Ubound(ArrDatas)
				tmpStr=Replace(ArrDatas(Datai),CHR(13),"")'去除回车符
				tmpStr=Replace(tmpStr,CHR(10),"")'去除换行符
				tmpStr=Trim(tmpStr)
				For Num_i=0 To Num
					If tmpArr(Num_i)=tmpStr Then'如果在tmpArr存在tmpStr则退出For循环
						Exit For
					Else
						If Num = Num_i And tmpStr<>"" Then'如果在tmpArr中不存在tmpStr并且已经到了最大有效下标Num 则把tmpStr存入tmpArr(Num)
							tmpArr(Num)=tmpStr
							Num=Num+1
						End If
					End If
				Next
			Next
			
		Else'如果ArrDatas不是数组，则作为数据集合来处理
			On Error Resume Next
			Redim tmpArr(ArrDatas.Count)
			If Err Then'如果ArrDatas既不是数组也不是数据集合,而是一个字符串,则返回ArrDatas值并退出Function.
				err.Clear
				Redim ArrResult(1)
				ArrResult(0)=ArrDatas
				ClearSameDatas=ArrResult
				Exit Function
			End If
			For Each Datai In ArrDatas
				tmpStr=Replace(Datai.value,CHR(13),"")'去除回车符
				tmpStr=Replace(tmpStr,CHR(10),"")'去除换行符
				tmpStr=Trim(tmpStr)
				For Num_i=0 To Num
					If tmpArr(Num_i)=tmpStr Then'如果在tmpArr存在tmpStr则退出For循环
						Exit For
					Else
						If Num = Num_i And tmpStr<>"" Then'如果在tmpArr中不存在tmpStr并且已经到了最大有效下标Num 则把tmpStr存入tmpArr(Num)
							tmpArr(Num)=tmpStr
							Num=Num+1
						End If
					End If
				Next
			Next
		End If
		If Num=0 Then 
			Redim ArrResult(1)
			ArrResult(0)="$err$"
			ClearSameDatas=ArrResult
			Exit Function
		End If
		Redim ArrResult(Num-1)
		If ShowResultArrDatas Then Response.Write "|------------------------------<br>" : response.Flush()
		For Num_i=0 To Num-1
			ArrResult(Num_i)=tmpArr(Num_i)
			If ShowResultArrDatas Then Response.Write "|"&trim(ArrResult(Num_i))&"<br>" : response.Flush()
		Next
		If ShowResultArrDatas Then Response.Write "|------------------------------<br>" : response.Flush()
		ClearSameDatas=ArrResult
	End Function
	
	'返回整数(正整数和负整数),BV_Str为空时返回0
	Public Function [Int](ByVal BV_Str)
		[Int]=0
		If IsNul(BV_Str) Then Exit Function
		On Error Resume Next
		BV_Str=VBS.Int_(BV_Str)
		If Err.number <> 0 Then
			BV_Str=RegReplace(BV_Str,"[^0-9]","")
			Err.Clear
		End If
		[Int]=VBS.Int_(BV_Str)
	End Function
	
	'$ 返回已被转换为 Long 子类型的 Variant (即 -2,147,483,648 到 2,147,483,647 之间的整数), BV_Str为空时返回0
	Public Function [CLng](ByVal BV_Str)
		BV_Str = [Int](BV_Str)
		If BV_Str > 2147483647  Then BV_Str = 2147483647
		If BV_Str < -2147483647 Then BV_Str = -2147483647
		[CLng] = VBS.CLng_(BV_Str)
	End Function
	
	' $函数: Color(Color)
	'  作用: 返回16进制颜色(如:#02DF4C)
	'  参数: {Color}须处理的字符串
	'  结果: Color为空时返回空
	Public Function Color(ByVal BV_Str)
	    Color=""
		BV_Str=Trim(BV_Str)
		If [Left](BV_Str,1)="#" Then BV_Str=Mid(BV_Str,2,Len(BV_Str)-1)
		BV_Str=KnifeCMS.Data.RegReplace(BV_Str,"[^0-9a-zA-Z]","")
		If IsNul(BV_Str) or Len(BV_Str)<6 Then Exit function
		Color="#" & [Left](BV_Str,6)
	End Function
	
	' $函数: CutStr(StrText,cutStart,Strlen)
	'  作用: 截取指定长度字符
	'   StrText: 原字符串
	'cutStart:开始截取的位置,从1开始
	'Strlen: 截取长度
	Function CutStr(ByVal StrText,ByVal cutStart,ByVal Strlen)
		Dim Str,l,t,c,i
		Str=StrText
		cutStart=int(RegReplace(cutStart,"[^0-9,]",""))
		If cutStart<1 Then cutStart=1
		l=Len(Str)
		t=0
		For i=1 to l
			c=Abs(Asc(Mid(Str,i,1)))
			If c>255 Then '一个汉字占两个字节
				t=t+2
			Else
				t=t+1
			End If
			If t>=Strlen Then
				CutStr = Mid(Str, cutStart, i) 
				Exit For
			Else
				CutStr=Str
			End If
		Next
		CutStr=Replace(CutStr,chr(10),"")'替换换行符
		CutStr=Replace(CutStr,chr(13),"")'替换回车符
	End Function
	
	Public Function CLeft(ByVal BV_Str,ByVal BV_SType)
		Dim Fn_ReturnString,Fn_L
		Fn_L = InStr(BV_Str,BV_SType)
		If Fn_L > 1 Then
			Fn_ReturnString = KnifeCMS.Data.Left(BV_Str,InStr(BV_Str,BV_SType)-1)
		Else
			Fn_ReturnString = ""
		End If
		CLeft = Fn_ReturnString
	End Function
	Public Function CRight(ByVal BV_Str,ByVal BV_SType)
		Dim Fn_ReturnString
		Fn_ReturnString = Mid(BV_Str,InStr(BV_Str,BV_SType)+1)
		CRight = Fn_ReturnString
	End Function
	
	
	
	'$过滤非“字母”字符(只留下字母),若 BV_Str 为空则返回空
	Public Function FiltNotLetter(ByVal BV_Str)
		If IsNul(BV_Str) Then Exit Function
		Dim Fn_Rep
		Set Fn_Rep=new RegExp
		Fn_Rep.IgnoreCase =True
		Fn_Rep.Global=True
		Fn_Rep.Pattern="[^a-zA-Z]"
		BV_Str=Fn_Rep.Replace(BV_Str,"")
		Set Fn_Rep=Nothing
		FiltNotLetter=BV_Str
	End Function
	
	'$:返回双精度类型
	' Str为空或处理后为空时返回0，否则返回双精度类型
	' KnifeCMS.Data.FormatDouble("1234567891234.12345678") = 1234567891234.12
	' KnifeCMS.Data.FormatDouble("+17.26548524") = 17.26548524
	' KnifeCMS.Data.FormatDouble("17.26548524")  = 17.26548524
	' KnifeCMS.Data.FormatDouble("17.2654")      = 17.2654
	' KnifeCMS.Data.FormatDouble("17.2657")      = 17.2657
	' KnifeCMS.Data.FormatDouble("17.26")        = 17.26
	' KnifeCMS.Data.FormatDouble("-17.26548524") = -17.26548524
	' KnifeCMS.Data.FormatDouble("-17.2654")     = -17.2654
	' KnifeCMS.Data.FormatDouble("-17.2657")     = -17.2657
	' KnifeCMS.Data.FormatDouble("-17.26")       = -17.26
	Public Function FormatDouble(ByVal BV_Str)
		If IsNul(BV_Str) Then FormatDouble=0 : Exit Function
		Dim Fn_TempString : Fn_TempString = Cstr(Trim(BV_Str))
		Dim Fn_Symbol
		If Left(Fn_TempString,1) = "-" Then Fn_Symbol="-"
		Fn_TempString = RegReplace(Fn_TempString,"[^0-9.]","")
		If IsNul(Fn_TempString) Then FormatDouble=0 : Exit Function
		Pv_Arr        = Split(Fn_TempString,".")
		Pv_ArrLen     = Ubound(Pv_Arr)
		If Pv_ArrLen>=2 Then
			If Not(IsNul(Pv_Arr(0))) Then Fn_TempString=Pv_Arr(0)&"."&Pv_Arr(1)
		End If
		Fn_TempString=CDbl(Fn_TempString)
		If IsNul(Fn_TempString) Then Fn_TempString=0
		If Fn_TempString<>0 Then Fn_TempString = Fn_Symbol & Fn_TempString
		FormatDouble= CDbl(Fn_TempString)
	End Function
	
	'精确到小数点后6位，最低小数点后2位
	Public Function [FormatCurrency](ByVal BV_Str)
		If IsNul(BV_Str) Or BV_Str=0 Then [FormatCurrency]="0.00" : Exit Function
		Dim Fn_ReturnString
		Fn_ReturnString = Mid(Replace(VBS.FormatCurrency_(BV_Str),",",""),2)
		[FormatCurrency] = Fn_ReturnString
	End Function
	
	'$ 函数: FormatDateTime(BV_D,BV_T)
	'  作用: 时间格式化
	'  参数: {BV_D}须处理的时间
	'        {BV_T}返回的时间格式的数值
    '              0:返回日期和时间 (2009-08-10 22:05:06)
	'              1:返回长日期格式日期 (2009年08月10日)
	'              2:返回短日期格式日期 (2009-08-10)
	'              3:返回时间格式显示时间 (下午 00:00:00)
	'              4:返回24小时格式 (00:00:00)
	'              5:返回年 (2010)
	'              6:返回月 (08)
	'              7:返回日 (10)
	'              8:返回年月日 (20090810)
	'              9:返回年月日时分秒 (20090810220506)
	'              10:时 (22)
	'              11:分 (05)
	'              12:秒 (06)
	'  结果: 当BV_D为空 返回 空
	'        按BV_T的值 返回日期/时间
	Public Function [FormatDateTime](ByVal BV_D,ByVal BV_T)
		If BV_D="" Or isNull(BV_D) Then Exit Function
		Dim Fn_Date,YYYY,MM,DD,HH,MN,SS
		If IsDate(BV_D) Then
		   BV_D=CDate(BV_D)
		   YYYY=Year(BV_D)
		   MM=Month(BV_D)
		   if MM<10 then MM=0&MM
		   DD=Day(BV_D)
		   if DD<10 then DD=0&DD
		   HH=Hour(BV_D)
		   if HH<10 then HH=0&HH
		   MN=Minute(BV_D)
		   if MN<10 then MN=0&MN
		   SS=Second(BV_D)
		   if SS<10 then SS=0&SS
		   If Not(KnifeCMS.Data.IsNum(BV_T)) Then BV_T=LCase(BV_T)
		   Select Case BV_T
			   Case "0","yyyy-mm-dd hh:mm:ss","yyyy-mm-dd hh:mn:ss"
				   Fn_Date=YYYY&"-"&MM&"-"&DD&" "&HH&":"&MN&":"&SS
			   Case "1","yyyy年mm月dd日"
				   Fn_Date=YYYY&"年"&MM&"月"&DD&"日"
			   Case "2","yyyy-mm-dd"
				   Fn_Date = YYYY &"-"& MM &"-"& DD
			   Case "3","上午/下午 hh:mm:ss","上午/下午 hh:mn:ss"
				   if Cint(HH)<12 then
					   Fn_Date="上午 "&HH&":"&MN&":"&SS
				   elseif Cint(HH)=12 then
					   Fn_Date="下午 "&HH&":"&MN&":"&SS
				   else
					   HH=Cint(HH)-12
					   if len(HH)=1 then HH=0&HH
					   Fn_Date="下午 "&HH&":"&MN&":"&SS
				   end if
			   Case "4","hh:mm:ss","hh:mn:ss"
				   Fn_Date=HH&":"&MN&":"&SS
			   Case "5","yyyy"
				   Fn_Date=YYYY
			   Case "6","mm"
				   Fn_Date=MM
			   Case "7","dd"
				   Fn_Date=DD
			   Case "8","yyyymmdd"
				   Fn_Date=YYYY&MM&DD
			   Case "9","yyyymmddhhmnss"
				   Fn_Date=YYYY&MM&DD&HH&MN&SS
			   Case "10","hh"
				   Fn_Date=HH
			   Case "11","mn"
				   Fn_Date=MN
			   Case "12","ss"
				   Fn_Date=SS
		   End Select
		End If
		[FormatDateTime] = Fn_Date
    End Function
	
	Function [DateDiff](ByVal BV_Type, ByVal BV_STime, ByVal BV_ETime)
		[DateDiff] = VBS.DateDiff_(BV_Type,BV_STime,BV_ETime)
	End Function
	
	'$转换字节数为简写形式
	Function FormatSize(tSize)
	  If tSize>=1073741824 Then
		FormatSize=int((tSize/1073741824)*1000)/1000 & " GB"
	  ElseIf tSize>=1048576 Then
		FormatSize=int((tSize/1048576)*1000)/1000 & " MB"
	  ElseIf tSize>=1024 Then
		FormatSize=int((tSize/1024)*1000)/1000 & " KB"
	  Else
		FormatSize=tSize & "B"
	  End If
	End Function
	
	'$ 函数: FormatToFixNumber(Length,intStr)
	'  作用: 把数字格式化固定长度的数字，如把01该成0001 注意不能把0004改成04(否则返回4)
	'  参数: {Length}固定长度
	'        {intStr}须处理的数字
	'  结果: 返格式化后长度为Length的数字
	'        其他情况 返回处理前的intStr
	Public Function FormatToFixNumber(Length,intStr)
		Length=Cint(Length)
		intStr=Clng(intStr)
		FormatToFixNumber=0
		If Length < 1  or Length <= Len(intStr) Then
			FormatToFixNumber=intStr
			Exit Function
		End If
		Dim Len_i
		For Len_i=1 To (Length-Len(intStr))
			intStr = 0 & intStr
		Next
		FormatToFixNumber=intStr
	End Function
	
	'获取文件后缀名(不带.的后缀名)
	Public Function FileExName(ByVal BV_FilePath)
		Dim Fn_Array,Fn_FileExName
		If Instr(BV_FilePath,".")>0 Then
			Fn_Array      = Split(BV_FilePath,".")
			Fn_FileExName = Fn_Array(UBound(Fn_Array))
		Else
			Fn_FileExName = ""
		End If
		FileExName = Fn_FileExName
	End Function
	
	'获取文件名(文件路径,是否带后缀名)
	Public Function FileName(ByVal BV_FilePath, ByVal BV_HaveExName)
		Dim Fn_Array,Fn_FileName
		If BV_FilePath="" Then FileName="" : Exit Function
		If Instr(BV_FilePath,"/")>0 Then
			Fn_Array    = Split(BV_FilePath,"/")
			Fn_FileName = Fn_Array(UBound(Fn_Array))
		ElseIf Instr(BV_FilePath,"\")>0 Then
			Fn_Array    = Split(BV_FilePath,"\")
			Fn_FileName = Fn_Array(UBound(Fn_Array))
		Else
			Fn_FileName = BV_FilePath
		End If
		If Not(BV_HaveExName) Then
			Fn_FileName = Left(Fn_FileName,InstrRev(Fn_FileName,".")-1)
		End If
		FileName = Fn_FileName
	End Function
	
	'获取文件夹名称
	Public Function FolderName(ByVal BV_FolderPath)
		Dim Fn_i,Fn_Array,Fn_FolderName
		If Instr(BV_FolderPath,"/")>0 Then
			Fn_Array = Split(BV_FolderPath,"/")
			For Fn_i=UBound(Fn_Array) To 0 Step -1
				If Trim(Fn_Array(Fn_i))<>"" Then
					Fn_FolderName = Trim(Fn_Array(Fn_i))
					Exit For
				End If
			Next
		End If
		FolderName = Fn_FolderName
	End Function
	
	'返回正确的文件夹路径(attachment/image/)
	Public Function FolderPath(ByVal BV_FolderPath)
		Dim Fn_FP
		Fn_FP = Replace(BV_FolderPath,"\","")
		Fn_FP = Replace(Fn_FP,":","")
		Fn_FP = Replace(Fn_FP,"*","")
		Fn_FP = Replace(Fn_FP,"?","")
		Fn_FP = Replace(Fn_FP,"""","")
		Fn_FP = Replace(Fn_FP,"|","")
		Fn_FP = Replace(Fn_FP,"<","")
		Fn_FP = Replace(Fn_FP,">","")
		Fn_FP = KnifeCMS.Data.RegReplace(Fn_FP,"[/]+","/")
		If Left(Fn_FP,1)   = "/" Then Fn_FP = Mid(Fn_FP,2)
		If Right(Fn_FP,1) <> "/" Then Fn_FP = Fn_FP &"/"
		FolderPath = Fn_FP
	End Function
	
	Public Function [HTMLEncode](ByVal BV_Str)
		If Trim(BV_Str)="" Or IsNull(BV_Str) then Exit Function
		'BV_Str = Replace(BV_Str,Chr(13)&Chr(10),"<br/>")'Chr(13)是回车符
		'BV_Str = Replace(BV_Str, Chr(10),"<br/>")'Chr(10)是换行符
		BV_Str = Replace(BV_Str,"%","&#037;")
		BV_Str = Replace(BV_Str,"<","&lt;")
		BV_Str = Replace(BV_Str,">","&gt;")
		BV_Str = Replace(BV_Str,Chr(32), "&nbsp;")'Chr(32)是一个空格符
		BV_Str = Replace(BV_Str,Chr(34), "&quot;")'Chr(34)是英文双引号
		BV_Str = Replace(BV_Str,Chr(39), "&#39;")'Chr(39)是英文单引号
		'BV_Str = Replace(BV_Str, Chr(9), "&nbsp;&nbsp;&nbsp;")'Chr(9)是制表符
		[HTMLEncode]=BV_Str
	End Function
	
	Function HTMLDecode(ByVal BV_Str)
		If Trim(BV_Str)="" Or IsNull(BV_Str) then Exit Function
		BV_Str = Replace(BV_Str,"&#037;","%")
		BV_Str = Replace(BV_Str,"&quot;",Chr(34))
		BV_Str = Replace(BV_Str,"&nbsp;",Chr(32))
		BV_Str = Replace(BV_Str,"&#39;",Chr(39))
		BV_Str = Replace(BV_Str,"&apos;",Chr(39))
		BV_Str = Replace(BV_Str,"&gt;",">")
		BV_Str = Replace(BV_Str,"&lt;","<")
		BV_Str = Replace(BV_Str,"&amp;",Chr(38))
		BV_Str = Replace(BV_Str,"&#38;",Chr(38))
		HTMLDecode = BV_Str
	End Function
	
	Function AbstractDecode(ByVal BV_Str)
		If Trim(BV_Str)="" Or IsNull(BV_Str) then Exit Function
		BV_Str = Replace(BV_Str,"&#037;","%")
		BV_Str = Replace(BV_Str,"&quot;",Chr(34))
		BV_Str = Replace(BV_Str,"&nbsp;",Chr(32))
		BV_Str = Replace(BV_Str,"&#39;",Chr(39))
		BV_Str = Replace(BV_Str,"&apos;",Chr(39))
		BV_Str = Replace(BV_Str,"&gt;",">")
		BV_Str = Replace(BV_Str,"&lt;","<")
		BV_Str = Replace(BV_Str,"&amp;",Chr(38))
		BV_Str = Replace(BV_Str,"&#38;",Chr(38))
		BV_Str = Replace(BV_Str," "," ")
		AbstractDecode = BV_Str
	End Function
	
	Public Function TextareaEncode(ByVal BV_Text)
	    If KnifeCMS.Data.IsNul(BV_Text) Then TextareaEncode="" : Exit Function
		BV_Text = Replace(BV_Text,Chr(13)&Chr(10),"")
		'BV_Text = KnifeCMS.Data.HtmlTextTreat(BV_Text,True,False,False)
		BV_Text = KnifeCMS.Data.HTMLEncode(Replace(BV_Text,"	",""))
		BV_Text = Replace(BV_Text,"&lt;br/&gt;","<br/>")
		TextareaEncode = BV_Text
	End Function
	
	Public Function TextareaDecode(ByVal BV_Text)
		If KnifeCMS.Data.IsNul(BV_Text) Then TextareaDecode="" : Exit Function
		BV_Text = Replace(BV_Text,"<br/>",Chr(10))
		TextareaDecode = BV_Text
	End Function
	
	'编辑器文本编码
	Public Function EditorContentEncode(ByVal BV_Text)
		EditorContentEncode = KnifeCMS.Data.HTMLEncode(BV_Text)
	End Function
	
	'编辑器文本解码
	Public Function EditorContentDecode(ByVal BV_Text)
		If KnifeCMS.Data.IsNul(BV_Text) Then EditorContentDecode="" : Exit Function
		Dim Fn_V,Fv_Code,Match,Matches
		BV_Text = KnifeCMS.Data.HTMLDecode(BV_Text)
		Pv_RegExp.Pattern = "<pre([\s\S]*?)>([\s\S]*?)</pre>"
		Set Matches = Pv_RegExp.Execute(BV_Text)
		For Each Match in Matches
			Fn_V    = Match.SubMatches(1)
			Fv_Code = Fn_V
			Fv_Code = Replace(Fv_Code,"<","&lt;")
			Fv_Code = Replace(Fv_Code,">","&gt;")
			BV_Text = KnifeCMS.Data.ReplaceString(BV_Text,Fn_V,Fv_Code)
		Next
		EditorContentDecode = BV_Text
	End Function
	
	'编辑器文本解码[用于输出到后台textarea编辑器里]
	Public Function TextareaEditorContentDecode(ByVal BV_Text)
		If KnifeCMS.Data.IsNul(BV_Text) Then TextareaEditorContentDecode="" : Exit Function
		Dim Fn_V,Fv_Code,Match,Matches
		BV_Text = KnifeCMS.Data.HTMLDecode(BV_Text)
		Pv_RegExp.Pattern = "<pre([\s\S]*?)>([\s\S]*?)</pre>"
		Set Matches = Pv_RegExp.Execute(BV_Text)
		For Each Match in Matches
			Fn_V    = Match.SubMatches(1)
			Fv_Code = Fn_V
			Fv_Code = Replace(Fv_Code,"<","&amp;lt;")
			Fv_Code = Replace(Fv_Code,">","&amp;gt;")
			BV_Text = KnifeCMS.Data.ReplaceString(BV_Text,Fn_V,Fv_Code)
		Next
		TextareaEditorContentDecode = BV_Text
	End Function
	
	Public Function ParseMetaDescription(ByVal BV_Text)
		If KnifeCMS.Data.IsNul(BV_Text) Then ParseMetaDescription="" : Exit Function
		BV_Text = KnifeCMS.Data.HtmlTextTreat(BV_Text,True,False,False)
		BV_Text = KnifeCMS.Data.HTMLEncode(Replace(BV_Text,"	",""))
		BV_Text = Replace(BV_Text,"&lt;br/&gt;","")
		ParseMetaDescription = KnifeCMS.Data.Left(BV_Text,250)
	End Function
	
	'$ 函数: HtmlTextTreat(strHtml,brExit,pExit,imgExit)
	'  作用: 过滤html标签，只保留<p><br>标签
	'  参数: {strHtml}须处理的字符串
	'        {brExit}保留换行  true / false
	'        {pExit]保留段落   true / false
	'        {imgExit}保留图片 true / false
	'  结果: 返格式化后长度为Length的数字
	'        其他情况 返回处理前的intStr
	Public Function HtmlTextTreat(strHtmlText,brExit,pExit,imgExit)
		brExit  = Cbool(brExit)
		pExit   = Cbool(pExit)
		imgExit = Cbool(imgExit)
		Dim strHtml,Fn_Rep
		
		Set Fn_Rep=new RegExp
		Fn_Rep.IgnoreCase=True
		Fn_Rep.Global=True
		strHtml=strHtmlText
		
		If brExit Then
			Fn_Rep.Pattern="<br([^>]*)>"'br标签
			strHtml=Fn_Rep.Replace(strHtml,"&lt;br&gt;")
			Fn_Rep.Pattern="</br([^>]*)>"'br标签
			strHtml=Fn_Rep.Replace(strHtml,"&lt;/br&gt;")
		End If
		If pExit Then
			Fn_Rep.Pattern="<p([^>]*)>"'P标签
			strHtml=Fn_Rep.Replace(strHtml,"&lt;p&gt;")
			Fn_Rep.Pattern="</p([^>]*)>"'P标签
			strHtml=Fn_Rep.Replace(strHtml,"&lt;/p&gt;")
		End If
		If imgExit Then
			Fn_Rep.Pattern="<img"'img标签
			strHtml=Fn_Rep.Replace(strHtml,"&lt;img")
		End If
		
		Fn_Rep.Pattern="<style[^>]*>[\s\S]*?</style>"'过滤样式
		strHtml=Fn_Rep.Replace(strHtml,"")
		Fn_Rep.Pattern="<script[^>]*>[\s\S]*?</script>"'过滤js脚本
		strHtml=Fn_Rep.Replace(strHtml,"")
		Fn_Rep.Pattern="<(.[^>]*)>"'过滤html
		strHtml=Fn_Rep.Replace(strHtml,"")
		
		If brExit Then
			Fn_Rep.Pattern="&lt;br&gt;"
			strHtml=Fn_Rep.Replace(strHtml,"<br>")
			Fn_Rep.Pattern="&lt;/br&gt;"
			strHtml=Fn_Rep.Replace(strHtml,"</br>")
		End If
		If pExit Then
			Fn_Rep.Pattern="&lt;p&gt;"
			strHtml=Fn_Rep.Replace(strHtml,"<p>")
			Fn_Rep.Pattern="&lt;/p&gt;"
			strHtml=Fn_Rep.Replace(strHtml,"</p>")
		End If
		If imgExit Then
			Fn_Rep.Pattern="&lt;img"
			strHtml=Fn_Rep.Replace(strHtml,"<img")
		End If
		
		Set Fn_Rep=Nothing
		HtmlTextTreat=strHtml
	End Function
	
	'检查是否为16进制的颜色
	Public Function IsColor(ByVal Color)
		IsColor=False
		If IsNul(Color) Then Exit Function
		Dim Fn_Rep
		Set Fn_Rep=new RegExp
		Fn_Rep.IgnoreCase=True
		Fn_Rep.Global=True
		Fn_Rep.Pattern="^#[0-9a-zA-Z]{6}$"
		If Fn_Rep.test(Color) then IsColor=True
		Set Fn_Rep=Nothing
	End Function
	
	'$ 检查Email是否合法  合法:返回True   不合法:返回False
	Public Function IsEmail(ByVal BV_Email)
	    If IsNul(BV_Email) Then IsEmail=false : Exit Function
		IsEmail = RegTest(BV_Email,"^\w+([-+\.]\w+)*@(([\da-zA-Z][\da-zA-Z-]{0,61})?[\da-zA-Z]\.)+([a-zA-Z]{2,4}(?:\.[a-zA-Z]{2})?)$")
	End Function
	
	'$ IsNul(Str) 返回 Boolean 值，判断 Str 是否不包含任何有效数据
	Public Function IsNul(ByVal BV_Str)
		IsNul=False
		Select Case VarType(BV_Str)
			Case vbEmpty, vbNull
				IsNul=True : Exit Function
			Case vbObject
				Select Case TypeName(BV_Str)
					Case "Nothing","Empty"
						IsNul = True : Exit Function
					Case "Recordset"
						If BV_Str.State = 0 Then IsNul=True : Exit Function
						If BV_Str.Bof And BV_Str.Eof Then IsNul = True : Exit Function
					Case "Dictionary"
						If BV_Str.Count = 0 Then IsNul=True : Exit Function
				End Select
			Case vbArray,8194,8204,8209
				If Ubound(BV_Str)=-1 Then IsNul=True : Exit Function
			Case Else
				If IsNull(BV_Str) Then IsNul=True : Exit Function
				BV_Str=Replace(BV_Str,Chr(9),"")'Chr(9)是制表符
				BV_Str=Replace(BV_Str,Chr(10),"")'Chr(10)是换行符
				BV_Str=Replace(BV_Str,Chr(32),"")'Chr(32)是一个空格符
				BV_Str=Replace(BV_Str,Chr(13),"")'Chr(13)是回车符
				'BV_Str=Replace(BV_Str,Chr(255),"")
				'Chr(255)是一个特殊空格符，此处不做处理，防止误删"/"
				If Trim(BV_Str)="" Then IsNul=True
		End Select
	End Function
	
	'$ IsNum(Str) 返回 Boolean 值，判断 Str 的值是否为数字
	Function IsNum(ByVal Str)
		If IsNul(Str) Then
			IsNum=False
		Else
			IsNum=Isnumeric(Str)
		End If
	End Function
	
	'$ IsUrl(Str) 返回 Boolean 值，判断 Str 的是否为合法网址
	Public Function IsUrl(ByVal Str)
	  If not IsNul(Str) Then
		  If LCase(left(Str,7))="http://" Then IsUrl=True Else IsUrl=False
	  Else
		  IsUrl=False
	  End if
	End Function
	
	'返回指定数目的从字符串的左边算起的字符
	Public Function [Left](ByVal BV_Str, ByVal BV_Length)
		Err.Clear : On Error Resume Next
		If IsNul(BV_Str) Then [Left]="" : Exit Function
		BV_Str = VBS.Left_(BV_Str,BV_Length)
		If Err.Number <> 0 Then BV_Str="" : Err.Clear
		[Left] = BV_Str
	End Function

    '$: 替换字符串  示例: KnifeCMS.Data.ReplaceString(Fn_Region_Path,"string","")
	Function ReplaceString(ByVal BV_Content, Byval BV_Str, Byval BV_Result)
		On Error Resume Next
		If BV_Content="" Then ReplaceString="" : Exit Function
        ReplaceString=Replace(BV_Content,BV_Str,BV_Result)
	    If Err Then ReplaceString="" : Err.Clear
	End Function
	
	'$: 正则替换(单行文本)   示例: KnifeCMS.Data.RegReplace(Fn_Region_Path,"[^0-9,]","")
	Function RegReplace(ByVal Str, ByVal Rule, Byval BV_Result)
		RegReplace = RegExpReplace(Str,Rule,BV_Result,0)
	End Function
	'$: 正则替换(多行文本) 
	Function RegReplaceM(ByVal Str, ByVal Rule, Byval BV_Result)
		RegReplaceM = RegExpReplace(Str,Rule,BV_Result,1)
	End Function
	
	Public Function RegExpReplace(ByVal BV_Str, ByVal BV_Rule, ByVal BV_Result, ByVal BV_IsMult)
		On Error Resume Next
		If IsNull(BV_Result) Then BV_Result=""
		If KnifeCMS.Data.IsNul(BV_Str) Then RegExpReplace="" : Exit Function
		If BV_Rule="" Then RegExpReplace=BV_Str : Exit Function
		Pv_RegExp.IgnoreCase = True
		Pv_RegExp.Global     = True
		Pv_RegExp.Pattern    = BV_Rule
		If BV_IsMult = 1 Then Pv_RegExp.Multiline = True '多行文本
		BV_Str = Pv_RegExp.Replace(BV_Str,BV_Result)
		If BV_IsMult = 1 Then Pv_RegExp.Multiline = False
		Pv_RegExp.Pattern = ""
		RegExpReplace = BV_Str
		If Err.number <> 0 Then
			Err.Clear
		End If
	End Function
	
	Public Function RegMatch(ByVal BV_Str, ByVal BV_Rule)
		On Error Resume Next
		Pv_RegExp.IgnoreCase = True
		Pv_RegExp.Global     = True
		Pv_RegExp.Pattern    = BV_Rule
		Set RegMatch  = Pv_RegExp.Execute(BV_Str)
		Pv_RegExp.Pattern    = ""
		If Err.number <> 0 Then
			Err.Clear
			Set RegMatch = Nothing
		End If
	End Function
	
	Public Function RegTest(ByVal BV_Str, ByVal Rule)
		If IsNul(BV_Str) Then RegTest = False : Exit Function
		Pv_RegExp.IgnoreCase = True
		Pv_RegExp.Global     = True
		Pv_RegExp.Pattern    = Rule
		RegTest = Pv_RegExp.Test(CStr(BV_Str))
		Pv_RegExp.Pattern    = ""
	End Function
	
	'String = KnifeCMS.Data.GetStringByMatch(String,"<div id=Article>[list]</div>","[list]")
	Public Function GetStringByMatch(ByVal BV_Str,ByVal BV_Rule,ByVal BV_Area)
		If BV_Str="" Or BV_Rule="" Or BV_Area="" Then GetStringByMatch = "" : Exit Function
		Dim Fn_Rule : Fn_Rule = Replace(BV_Rule,BV_Area,"([\s\S]*?)")
		GetStringByMatch = KnifeCMS.Data.RegSubMatches(BV_Str,Fn_Rule)(0,0)
	End Function
	
	'Fn_Message = KnifeCMS.Data.RegSubMatches(Fn_NotifyConfig,"""msg"":""([\s\S]*?)""")(0,0)
	Public Function RegSubMatches(ByVal BV_Str, ByVal BV_Rule)
		On Error Resume Next
		Dim Fn_Matche,Fn_Matches,Fn_SubMatche
		Dim Fn_Array
		Dim Fn_i : Fn_i=0
		Dim Fn_j : Fn_j=0
		Set Fn_Matches = RegMatch(BV_Str,BV_Rule)
		If Fn_Matches.Count > 0 Then 
			ReDim Fn_Array(Fn_Matches(0).SubMatches.Count,Fn_Matches.Count)
			For Each Fn_Matche In Fn_Matches
				Fn_j=0
				For Each Fn_SubMatche In Fn_Matche.SubMatches
					Fn_Array(Fn_j,Fn_i) = Fn_SubMatche
					Fn_j = Fn_j + 1
				Next
				Fn_i = Fn_i+1
			Next
		Else
			ReDim Fn_Array(1,1)
		End If
		If Err.number <> 0 Then
			Err.Clear
			RegSubMatches = Nothing
		Else
			RegSubMatches = Fn_Array
		End If
	End Function
	
	'返回字符串长度,一个汉字算两字符
	Public Function StringLength(ByVal BV_Str)
		On Error Resume Next
		Dim Fn_Len : Fn_Len = 0
		Dim Fn_i,Fn_Len1,Fn_C
		BV_Str = Trim(BV_Str)
		If Not(IsNul(BV_Str)) Then
			Fn_Len1=Len(BV_Str)
			'Echo Fn_Len1&"-"
			For Fn_i=1 To Fn_Len1
				Fn_C = Asc(Mid(BV_Str,Fn_i,1))
				If Fn_C < 0 Then Fn_C=Fn_C+65536
				if Fn_C > 255 Then 
					Fn_Len = Fn_Len + 2
				Else
					Fn_Len = Fn_Len + 1
				End If
			Next
		End If
		If Err.number <> 0 Then Err.Clear
	    StringLength=Fn_Len
	End Function
	
	Public Function OKData(ByVal BV_Data)
		On Error Resume Next
		Dim Fn_Data
		If KnifeCMS.Data.IsNul(BV_Data) Then
			Fn_Data = ""
		Else
			Fn_Data = BV_Data
		End If
		If Err.number <> 0 Then Err.Clear
		OKData = Fn_Data
	End Function
	
	Public Function ParseID(ByVal BV_ID)
		BV_ID = KnifeCMS.Data.RegReplace(BV_ID,"[^0-9,]","")
		BV_ID = KnifeCMS.Data.RegReplace(BV_ID,"(,{2,})",",")
		If Left(BV_ID,1)="," Then BV_ID = Mid(BV_ID,2)
		If Right(BV_ID,1)="," Then BV_ID = Left(BV_ID,Len(BV_ID)-1)
		ParseID = BV_ID
	End Function
	
	'检查IP是否合法,不合法的IP返回0.0.0.0
	Public Function ParseIP(ByVal Str)
		Pv_RegExp.IgnoreCase = True
		Pv_RegExp.Global     = True
		Pv_RegExp.Pattern    = "^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$"
		If Pv_RegExp.test(Str) then
			ParseIP=Str
		Else
			ParseIP="0.0.0.0"
		End if
	End Function
	
	Public Function ParseRs(ByVal BV_Str)
		If KnifeCMS.Data.IsNul(BV_Str) Then
			ParseRs = ""
		Else
			ParseRs = BV_Str
		End If
	End Function
	
	Public Function TemplateCodeEncode(ByVal BV_Str)
		BV_Str  = Replace(BV_Str,">","&gt;")
		BV_Str  = Replace(BV_Str,"<","&lt;")
		TemplateCodeEncode = BV_Str
	End Function
	
	Public Function TemplateCodeDecode(ByVal BV_Str)
		BV_Str  = Replace(BV_Str,"&gt;",">")
		BV_Str  = Replace(BV_Str,"&lt;","<")
		TemplateCodeDecode = BV_Str
	End Function
	
	'过滤HTML标记
	Public Function ReplaceHtml(ByVal BV_Str)
	    If BV_Str="" or isnull(BV_Str) then
			ReplaceHtml=""
			Exit function
		End if
		Dim Fn_Rep : Set Fn_Rep=new RegExp
		Fn_Rep.IgnoreCase =True
		Fn_Rep.Global=True
		Fn_Rep.Pattern="<(.[^>]*)>"
		BV_Str=Fn_Rep.Replace(BV_Str,"")
		Set Fn_Rep=Nothing
		ReplaceHtml=BV_Str
	End Function
	
	'替换双引号(")、单引号(')为(&quot;)、(&#39;)
	Public Function ReplaceQuot(ByVal BV_Str)
		If BV_Str="" Then Exit Function
		BV_Str=Replace(BV_Str,Chr(34),"&quot;")
		BV_Str=Replace(BV_Str,Chr(39),"&#39;")
		ReplaceQuot=BV_Str
	End Function
	
	'还原双引号、单引号
	Public Function ReturnQuot(ByVal BV_Str)
		If BV_Str="" Then Exit Function
		BV_Str=Replace(BV_Str,"&quot;",Chr(34))
		BV_Str=Replace(BV_Str,"&#39;",Chr(39))
		ReturnQuot=BV_Str
	End Function
	
	Function TrimOuter(Byval BV_Str)
		dim Fn_String : Fn_String=BV_Str
		If Left(Fn_String,1)=chr(32)  Then Fn_String=Right(Fn_String,Len(Fn_String)-1) 
		If Right(Fn_String,1)=chr(32) Then Fn_String=Left(Fn_String,Len(Fn_String)-1)
		TrimOuter=Fn_String
	End Function

	Function TrimOuterString(Byval BV_Str,Byval BV_Flag)
		Dim Fn_String,Fn_Len
		Fn_String = BV_Str
		Fn_Len    = Len(BV_Flag)
		If Left(Fn_String,Fn_Len) =BV_Flag Then Fn_String=Right(Fn_String,Len(Fn_String)-Fn_Len) 
		If Right(Fn_String,Fn_Len)=BV_Flag Then Fn_String=Left(Fn_String,Len(Fn_String)-Fn_Len)
		TrimOuterString=Fn_String
	End Function
	
	Function IsValidHex(ByVal BV_Str)
		If KnifeCMS.Data.IsNul(BV_Str) Then IsValidHex=False : Exit Function
		Dim c
		IsValidHex=true
		BV_Str=ucase(BV_Str)
		if len(BV_Str)<>3 then IsValidHex=false:exit function
		if left(BV_Str,1)<>"%" then IsValidHex=false:exit function
		c=mid(BV_Str,2,1)
		if not (((c>="0") and (c<="9")) or ((c>="A") and (c<="Z"))) then IsValidHex=false:exit function
		c=mid(BV_Str,3,1)
		if not (((c>="0") and (c<="9")) or ((c>="A") and (c<="Z"))) then IsValidHex=false:exit function
	End Function
	
	Public Function URLDecode(ByVal BV_Url)
		Dim deStr,c,i,v
		deStr=""
		If IsNul(BV_Url) Then URLDecode="" : Exit Function
		for i=1 to len(BV_Url)
			c=Mid(BV_Url,i,1)
			if c="%" then
				v=eval("&h"+Mid(BV_Url,i+1,2))
				if v<128 then
					deStr=deStr&chr(v)
					i=i+2
				else
					if IsValidHex(mid(BV_Url,i,3)) then
						if IsValidHex(mid(BV_Url,i+3,3)) then
							v=eval("&h"+Mid(BV_Url,i+1,2)+Mid(BV_Url,i+4,2))
							deStr=deStr&chr(v)
							i=i+5
						else
							v=eval("&h"+Mid(BV_Url,i+1,2)+cstr(hex(asc(Mid(BV_Url,i+3,1)))))
							deStr=deStr&chr(v)
							i=i+3
						end if
					else 
						destr=destr&c
					end if
				end if
			else
				if c="+" then
					deStr=deStr&" "
				else
					deStr=deStr&c
				end if
			end if
		next
		URLDecode=deStr
	End function
	
	Public Function [URLEncode](ByVal BV_Url)
		On Error Resume Next
		Dim Temp_Url : Temp_Url = Server.URLEncode(Trim(BV_Url))
		If Err.Number <> 0 Then Err.Clear
		[URLEncode] = Temp_Url
	End Function
	
	'对应 javascript 中的 escape 方法,对字符串进行编码以便它们能在所有计算机上可读.
	Function Escape(ByVal BV_Str)
		If IsNul(BV_Str) Then Escape="" : Exit Function
		Dim Fn_NTemp,Fn_STemp,Fn_STempChar,Fn_NTempAsc
		For Fn_NTemp =1 To Len(BV_Str)
			Fn_STempChar =Mid(BV_Str, Fn_NTemp, 1)
			Fn_NTempAsc =Ascw(Fn_STempChar)
			If (Fn_NTempAsc >=48 and Fn_NTempAsc <=57) or (Fn_NTempAsc >=65 and Fn_NTempAsc <=90) or (Fn_NTempAsc >=97 and Fn_NTempAsc <=122) or Instr("@*_+-./", Fn_STempChar) >0 Then
				Fn_STemp =Fn_STemp & Fn_STempChar
			ElseIf Fn_NTempAsc >0 and Fn_NTempAsc <16 Then
				Fn_STemp =Fn_STemp & "%0" & Hex(Fn_NTempAsc)
			ElseIf Fn_NTempAsc >=16 and Fn_NTempAsc <256 Then
				Fn_STemp =Fn_STemp & "%" & Hex(Fn_NTempAsc)
			Else
				Fn_STemp =Fn_STemp & "%u" & Hex(Fn_NTempAsc)
			End If
		Next
		Escape =Fn_STemp
	End Function
	
	Public Function UnEscape(ByVal BV_Str)
		If IsNul(BV_Str) Then UnEscape = "" : Exit Function
		Dim Fn_CodeLocal : Fn_CodeLocal = InStr(BV_Str,"%")
		Dim Fn_Temp : Fn_Temp = ""
		Do While Fn_CodeLocal>0
			Fn_Temp = Fn_Temp & Mid(BV_Str,1,Fn_CodeLocal-1)
			If LCase(Mid(BV_Str,Fn_CodeLocal+1,1))="u" Then
				Fn_Temp = Fn_Temp & ChrW(CLng("&H"&Mid(BV_Str,Fn_CodeLocal+2,4)))
				BV_Str = Mid(BV_Str,Fn_CodeLocal+6)
			Else
				Fn_Temp = Fn_Temp & Chr(CLng("&H"&Mid(BV_Str,Fn_CodeLocal+1,2)))
				BV_Str = Mid(BV_Str,Fn_CodeLocal+3)
			End If
			Fn_CodeLocal=InStr(BV_Str,"%")
		Loop
		UnEscape = Fn_Temp & BV_Str
	End Function
	
End Class
%>