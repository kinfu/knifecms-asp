<%
'Cookie处理类

Class Class_KnifeCMS_Cookie

	Private Pv_Encrypt    '是否对Cookie加密
	Private Pv_Secure     'cookie 发送条件 true:仅限加密连接 false:任意类型的连接
	Private Pv_Domain     'cookie 作用域
	Private Pv_Path       'cookie 作用路径
	Private Pv_CookiePre  'cookie 前缀
	Public LoginCookieTime'有效登陆时间,以分钟为单位
	
	Private Sub Class_Initialize()
		Pv_Encrypt = False
		Pv_Secure  = False
	End Sub
	Private Sub Class_Terminate()
	End Sub

	Public Property Let Encrypt(ByVal BV_Bool)
		Pv_Encrypt = BV_Bool
	End Property
	Public Property Get Encrypt()
		Encrypt = Pv_Encrypt
	End Property
	
	Public Property Let Secure(ByVal BV_Bool)
		Pv_Secure = BV_Bool
	End Property
	Public Property Get Secure()
		Secure = Pv_Secure
	End Property
	
	Public Property Let Domain(ByVal BV_Domain)
		Pv_Domain = BV_Domain
	End Property
	Public Property Get Domain()
		Domain = Pv_Domain
	End Property
	
	Public Property Let Path(ByVal BV_Path)
		Pv_Path = BV_Path
	End Property
	Public Property Get Path()
		Path = Pv_Path
	End Property
	
	Public Property Let CookiePre(ByVal BV_CookiePre)
		Pv_CookiePre = BV_CookiePre
	End Property
	Public Property Get CookiePre()
		CookiePre = Pv_CookiePre
	End Property
	
	Private Function CookieEncrypt(ByVal BV_String)
		CookieEncrypt = KnifeCMS.Base64.Encode(BV_String)
	End Function
	Private Function CookieDecrypt(ByVal BV_String)
		CookieDecrypt = KnifeCMS.Base64.Decode(BV_String)
	End Function
	
	Function AttributeTest(ByVal BV_String, ByVal BV_Type)
		Dim Fn_Rule
		Select Case Lcase(BV_Type)
			Case "int"		Fn_Rule = "^[-\+]?\d+$"
			Case "domain"	Fn_Rule = "^(([\da-zA-Z][\da-zA-Z-]{0,61})?[\da-zA-Z]\.)+([a-zA-Z]{2,4}(?:\.[a-zA-Z]{2})?)$"
			Case "ip"		Fn_Rule = "^((25[0-5]|2[0-4]\d|(1\d|[1-9])?\d)\.){3}(25[0-5]|2[0-4]\d|(1\d|[1-9])?\d)$"
			Case Else Fn_Rule = BV_Type
		End Select
		AttributeTest = KnifeCMS.Data.RegTest(CStr(BV_String),Fn_Rule)
	End Function

	'KnifeCMS.Cookie.GetCookie("ip")
	'KnifeCMS.Cookie.GetCookie("my:id")
	'KnifeCMS.Cookie.GetCookie("goods:id")
	'KnifeCMS.Cookie.GetCookie("goods:price")
	Public Function GetCookie(ByVal BV_Key)
		On Error Resume Next
		If BV_Key="" Then GetCookie="" : Exit Function
		Dim Fn_Array,Fn_Key,Fn_SubKey,Fn_Cookie
		If Instr(BV_Key,":") > 0 Then
			Fn_Array  = Split(BV_Key,":")
			Fn_Key    = Fn_Array(0) : Fn_SubKey = Fn_Array(1)
		Else
			Fn_Key = BV_Key
		End If
		If Not(KnifeCMS.Data.IsNul(Fn_Key)) Then
			Fn_Key = Pv_CookiePre & Fn_Key
			If KnifeCMS.Data.IsNul(Fn_SubKey) Then
				Fn_Cookie = Request.Cookies(Fn_Key)
			Else
				If Request.Cookies(Fn_Key).HasKeys Then Fn_Cookie = Request.Cookies(Fn_Key)(Fn_SubKey)
			End If
		Else
			Fn_Cookie = ""
		End If
		If Pv_Encrypt Then
			Fn_Cookie = CookieDecrypt(Fn_Cookie)
		End If
		If Err.number <> 0 Then Err.Clear
		GetCookie = KnifeCMS.Data.UnEscape(Fn_Cookie)
	End Function
	
	Public Function SetCookie(ByVal BV_Key, ByVal BV_Value, ByVal BV_Options)
		Dim Fn_i,Fn_Expires,Fn_Domain,Fn_Path,Fn_Secure,Fn_Array,Fn_Key,Fn_SubKey
		On Error Resume Next
		If Instr(BV_Key,":") > 0 Then
			Fn_Array  = Split(BV_Key,":")
			Fn_Key = Fn_Array(0) : Fn_SubKey = Fn_Array(1)
		Else
			Fn_Key = BV_Key
		End If
		If IsArray(BV_Options) Then
			For Fn_i = 0 To Ubound(BV_Options)
				If IsDate(BV_Options(Fn_i)) Then
					Fn_Expires = CDate(BV_Options(Fn_i))
				ElseIf AttributeTest(BV_Options(Fn_i),"int") Then
					If BV_Options(Fn_i)<>0 Then Fn_Expires = Now()+Int(BV_Options(Fn_i))/60/24
				ElseIf AttributeTest(BV_Options(Fn_i),"domain") or AttributeTest(BV_Options(Fn_i),"ip") Then
					Fn_Domain = BV_Options(Fn_i)
				ElseIf Instr(BV_Options(Fn_i),"/")>0 Then
					Fn_Path = BV_Options(Fn_i)
				ElseIf LCase(BV_Options(Fn_i))="true" or LCase(BV_Options(Fn_i))="false" Then
					Fn_Secure = BV_Options(Fn_i)
				End If
			Next
		Else
			If IsDate(BV_Options) Then
				Fn_Expires = CDate(BV_Options)
			ElseIf AttributeTest(BV_Options,"int") Then
				If BV_Options<>0 Then Fn_Expires = Now()+Int(BV_Options)/60/24
			ElseIf AttributeTest(BV_Options,"domain") or AttributeTest(BV_Options,"ip") Then
				Fn_Domain = BV_Options
			ElseIf Instr(BV_Options,"/")>0 Then
				Fn_Path = BV_Options
			ElseIf LCase(BV_Options) = "true" or LCase(BV_Options) = "false" Then
				Fn_Secure = BV_Options
			End If
		End If
		If Not(KnifeCMS.Data.IsNul(BV_Value)) Then
			BV_Value = KnifeCMS.Data.Escape(BV_Value)
			If Pv_Encrypt Then
				BV_Value = CookieEncrypt(BV_Value)
			End If
		End If
		If Not(KnifeCMS.Data.IsNul(Fn_Key)) Then
			Fn_Key = Pv_CookiePre & Fn_Key
			If KnifeCMS.Data.IsNul(Fn_SubKey) Then
				Response.Cookies(Fn_Key) = BV_Value
			Else
				Response.Cookies(Fn_Key)(Fn_SubKey) = BV_Value
			End If
			If KnifeCMS.Data.IsNul(Fn_Domain) Then Fn_Domain = Pv_Domain
			If KnifeCMS.Data.IsNul(Fn_Path)   Then Fn_Path   = Pv_Path
			If KnifeCMS.Data.IsNul(Fn_Secure) Then Fn_Secure = Pv_Secure
			If Not(KnifeCMS.Data.IsNul(Fn_Expires)) Then Response.Cookies(Fn_Key).Expires = Fn_Expires
			If Not(KnifeCMS.Data.IsNul(Fn_Domain))  Then Response.Cookies(Fn_Key).Domain  = Fn_Domain
			If Not(KnifeCMS.Data.IsNul(Fn_Path))    Then Response.Cookies(Fn_Key).Path    = Fn_Path
			If Not(KnifeCMS.Data.IsNul(Fn_Secure))  Then Response.Cookies(Fn_Key).Secure  = Fn_Secure
		End If
		If Err.number <> 0 Then Err.Clear
	End Function
	
	Public Function RemoveCookie(ByVal BV_Key)
		Dim Fn_Array,Fn_Key,Fn_SubKey
		If Instr(BV_Key,":") > 0 Then
			Fn_Array  = Split(BV_Key,":")
			Fn_Key = Fn_Array(0) : Fn_SubKey = Fn_Array(1)
		Else
			Fn_Key = BV_Key
		End If
		If Not(KnifeCMS.Data.IsNul(Fn_Key)) Then
			Fn_Key = Pv_CookiePre & Fn_Key
			If KnifeCMS.Data.IsNul(Fn_SubKey) Then
				Response.Cookies(Fn_Key) = Empty
				'Response.Cookies(Fn_Key).Expires = Now()'formatDateTime(Now)'SysTime'Now()
			Else
				If Request.Cookies(Fn_Key).HasKeys Then Response.Cookies(Fn_Key)(Fn_SubKey) = Empty
			End If
		End If
	End Function
	
	Private Function Clear()
		Dim Fn_Cookie
		For Each Fn_Cookie In Request.Cookies
			Response.Cookies(Fn_Cookie).Expires = formatDateTime(Now)
		Next
	End Function
	
	
End Class
%>