<%
'**
'系统前台初始化加载程序
'filename: kfc.initialization.asp
'copyright: KFC
'**
Dim User_ID,User_Name,User_BigHead,User_SmallHead
Dim UserInfo_RealName,UserInfo_Gender,UserInfo_HomeTown(2),UserInfo_MySite(2),UserInfo_Email,UserInfo_Birthday,UserInfo_Mobile,UserInfo_Telephone,UserInfo_QQ,UserInfo_Height,UserInfo_Weight,UserInfo_MaritalStatus,UserInfo_BloodType,UserInfo_Religion,UserInfo_Smoke,UserInfo_Drink,UserInfo_FavTravel,UserInfo_FavLiving,UserInfo_FavWork,UserInfo_FavPartner,UserInfo_FavCar

'不能用Request.Form方式获取数据,否则上传文件类会出错(因为使用Request.Form集合之后，不能调用BinaryRead)
FileSuffix = KnifeCMS.IIF(Trim(FileSuffix)="",".htm",FileSuffix)
Ctl       = LCase(KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("get","ctl"),"[^0-9a-zA-Z,]",""),50))
SubSystem = LCase(KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("get","subsys"),"[^0-9a-zA-Z,]",""),20))
Action    = LCase(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("get","action"),"[^0-9a-zA-Z,-_]",""))
SubAction = LCase(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("get","subaction"),"[^0-9a-zA-Z,-_]",""))
ID        = KnifeCMS.Data.CLng(KnifeCMS.GetForm("get","id"))
User_ID   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("get","userid"))
Category  = False : If Not(KnifeCMS.Data.IsNul(KnifeCMS.GetForm("get","category"))) Then Category = true
Page      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("get","page"))
IsContentExist  = True
IsArticleDetail = False
IsVoteDetail    = False
IsGoodsDetail   = False
UserCenter      = False
IsUserInfoDetail= False
IsMyAddrs       = False
IsUserHome      = False
IsUserExist     = False
Logined         = 0
MySystemNotifyNum  = 0
MyFriendRequestNum = 0
MyCartShopNum      = 0
%>
<!--#include file="kfc.dbtable.define.asp"-->
<%

KnifeCMS.Language = SiteLanguage
KnifeCMS.Cache.CacheTime = CacheTime

If Sys_IsInstalled=0 Then
	KnifeCMS.SystemPath = KnifeCMS.GetSystemPath()
End If
KnifeCMS.IncludeLanguageFile "common.main.lang.asp"
KnifeCMS.IncludeLanguageFile "main.lang.asp"


SiteLogo     = KnifeCMS.IIF(KnifeCMS.Data.IsNul(SiteLogo),TemplatePath &"images/logo.png",SiteLogo)
CurrentURL   = KnifeCMS.CurrentURL
Call OS.CheckUserLogin()
If Not(Logined=1) Then
	KnifeCMS.Cookie.RemoveCookie("my:pw")
	KnifeCMS.Cookie.RemoveCookie("my:id")
End If
%>