<!--#include file="module/cart/cart.class.asp"-->
<%
'**
'filename: kfc.common.asp
'copyright: KFC
'**
Public Function PrintHTMLHeader()
	Dim Fn_HTML
	Fn_HTML = Fn_HTML & "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"&vbCrLf
	Fn_HTML = Fn_HTML & "<html xmlns=""http://www.w3.org/1999/xhtml"">"&vbCrLf
	Fn_HTML = Fn_HTML & "<head>"&vbCrLf
	Fn_HTML = Fn_HTML & "<meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" />"&vbCrLf
	Fn_HTML = Fn_HTML & "<title>首页 - Powered by KnifeCMS</title>"&vbCrLf
	Fn_HTML = Fn_HTML & "<meta name=""keywords"" content="""" />"&vbCrLf
	Fn_HTML = Fn_HTML & "<meta name=""description"" content="""" />"&vbCrLf
	Fn_HTML = Fn_HTML & "<meta name=""generator"" content=""KFC "& Sys_Version &""" />"&vbCrLf
	Fn_HTML = Fn_HTML & "<meta name=""author"" content=""KFC Team""  />"&vbCrLf
	Fn_HTML = Fn_HTML & "<style type=""text/css"">"&vbCrLf
	Fn_HTML = Fn_HTML & "body{ color:#444; font-size:12px; font:12px/1.5 ""Tahoma"",""Verdana"",""Arial"",""sans-serif"",""Microsoft yahei"";}"&vbCrLf
	Fn_HTML = Fn_HTML & "a img{ border:0px;}a,a:visited{ color:#333; text-decoration:none;}a:hover{ color:#B80202; text-decoration:underline;}"
	Fn_HTML = Fn_HTML & ".mbody{ margin:10px auto; width:680px;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".mboxer1{border:1px solid #fefefe;}.mboxer2{border:1px solid #fafafa;}.mboxer3{border:1px solid #f6f6f6;}.mboxer4{border:2px solid #f0f0f0;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".mainboxer{ background:#fdfdfd; border:1px solid #d6d6d6; padding:18px 60px 80px 60px;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".toplink{ text-align:center;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".toplink a,.toplink a:visited{ color:#888; text-decoration:none;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".toplink a:hover{ text-decoration:underline;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".eline1{ color:#666; border-bottom:1px dotted #eee; line-height:56px; font-size:32px; padding-top:30px;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".eline1 .success{ color:#4b74b6; }"&vbCrLf
	Fn_HTML = Fn_HTML & ".eline1 .fail{ color:#666;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".eline2{ font-size:18px; padding:8px 0px 0px 0px;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".eline3{ font-size:14px; padding:5px 0px 0px 0px;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".eline_en{ font-size:12px; font-weight:bold; margin:10px 0px 10px 0px;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".btnline{ padding:22px 0px 0px 0px;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".btnline a{ background:#315CA1; display:inline-block; height:20px; line-height:20px; font-size:14px; margin:0px 2px; padding:4px 20px;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".btnline a,.btnline a:visited{ color:#fff; text-decoration:none;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".btnline a:hover{ color:#fff; background:#1e488a; text-decoration:none;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".btnline a span{ font-size:10px; font-weight:bold; margin-left:3px;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".poweredby{ color:#666; font-size:10px; text-align:center; padding:10px 0px 0px 0px;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".poweredby a,.poweredby a:visited{ color:#666; text-decoration:none;}"&vbCrLf
	Fn_HTML = Fn_HTML & ".poweredby a:hover{ text-decoration:underline;}"&vbCrLf
	Fn_HTML = Fn_HTML & "</style>"&vbCrLf
	Fn_HTML = Fn_HTML & "</head>"&vbCrLf
	Fn_HTML = Fn_HTML & "<body>"&vbCrLf
	Fn_HTML = Fn_HTML & "<div class=""mbody"" id=""mbody"">"&vbCrLf
    Fn_HTML = Fn_HTML & "<div class=""mboxer1""><div class=""mboxer2""><div class=""mboxer3""><div class=""mboxer4"">"&vbCrLf
    Fn_HTML = Fn_HTML & "<div class=""mainboxer"">"&vbCrLf
    Fn_HTML = Fn_HTML & "<div class=""toplink""><a href="""& SiteURL & SystemPath &""" title=""Home"" target=""_self"">"& SiteName &"</a></div>"&vbCrLf
	PrintHTMLHeader = Fn_HTML
End Function

Public Function PrintHTMLFooter()
	Dim Fn_HTML
	Fn_HTML = "</div>"&vbCrLf
    Fn_HTML = Fn_HTML & "</div></div></div></div>"&vbCrLf
	Fn_HTML = Fn_HTML & "<div class=""p"&"o"&"w"&"e"&"r"&"e"&"d"&"b"&"y"">P"&"o"&"w"&"e"&"r"&"e"&"d b"&"y <a target=""_blank"" href=""h"&"t"&"t"&"p"&":"&"//w"&"w"&"w.k"&"n"&"i"&"f"&"e"&"cms"&"."&"c"&"o"&"m/""><font style=""color:#CC3202; font-weight:bold;"">K"&"F"&"C"&"</font> <span>"& Sys_Version &"</span></a> P"&"r"&"o"&"c"&"e"&"s"&"s"&"e"&"d i"&"n "& KnifeCMS.RunTime &" s"&"e"&"c"&"o"&"n"&"d"&"("&"s"&"), "& KnifeCMS.DB.SqlQueryNum &" q"&"u"&"e"&"r"&"i"&"e"&"s</div>"&vbCrLf
	Fn_HTML = Fn_HTML & "</div>"&vbCrLf
	Fn_HTML = Fn_HTML & "<script type=""text/javascript"">"&vbCrLf
	Fn_HTML = Fn_HTML & "var webScreenHeight=function(){return document.documentElement.clientHeight;}"&vbCrLf
	Fn_HTML = Fn_HTML & "var bodyheight  = 0;"&vbCrLf
	Fn_HTML = Fn_HTML & "var mbodyhegith = 460;"&vbCrLf
	Fn_HTML = Fn_HTML & "resetting();"&vbCrLf
	Fn_HTML = Fn_HTML & "window.onresize= function(){resetting()};"&vbCrLf
	Fn_HTML = Fn_HTML & "function resetting(){"&vbCrLf
	Fn_HTML = Fn_HTML & "bodyheight = webScreenHeight();"&vbCrLf
	Fn_HTML = Fn_HTML & "if(bodyheight<mbodyhegith){"&vbCrLf
	Fn_HTML = Fn_HTML & "document.getElementById(""mbody"").style.marginTop = ""10px"";"&vbCrLf
	Fn_HTML = Fn_HTML & "}else{"&vbCrLf
	Fn_HTML = Fn_HTML & "document.getElementById(""mbody"").style.marginTop = (bodyheight-mbodyhegith)/2+""px"";"&vbCrLf
	Fn_HTML = Fn_HTML & "}"&vbCrLf
	Fn_HTML = Fn_HTML & "}"&vbCrLf
	Fn_HTML = Fn_HTML & "</script>"&vbCrLf
	Fn_HTML = Fn_HTML & "</body>"&vbCrLf
	Fn_HTML = Fn_HTML & "</html>"&vbCrLf
	PrintHTMLFooter = Fn_HTML 
End Function
%>