<%
Dim Visitor
Set Visitor = New Class_Visitor
Class Class_Visitor
	
	Private Pv_Rs,Pv_Array
	Private S_TempHTML,S_AllowedDepth,S_VisitorTpl
	
	Private Sub Class_Initialize()
		S_VisitorTpl=""
	End Sub
	Private Sub Class_Terminate()
	End Sub
	
	Public Function GetVisitor()
		If Not(1=Logined) Then GetVisitor="unlogined" : Exit Function
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_Sql,Fn_Rs,Fn_SqlTop,Fn_SqlCondition,Fn_SqlOrderBy,Fn_TempGroupID,Fn_MemberID,Fn_TopNum
		Dim Fn_ID,Fn_Name,Fn_HeadImg,Fn_VisitTime
		Dim Fn_Template,Fn_TempString
		
			Fn_MemberID    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","memberid"))
			If Not(Fn_MemberID>0) Then GetVisitor="error" : Exit Function
			Fn_TopNum      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","topnum")) : Fn_TopNum = KnifeCMS.IIF(Fn_TopNum>50,50,Fn_TopNum)
			Fn_SqlTop      = KnifeCMS.IIF(Fn_TopNum>0,"TOP "& Fn_TopNum,"")
			Fn_SqlOrderBy  = " ORDER BY a.ID DESC "
		    Fn_Sql = "SELECT "& Fn_SqlTop &" a.VisitorID,a.VTime,b.Username,b.HeadImg FROM ["& DBTable_Visitor &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.VisitorID) WHERE a.UserID="& Fn_MemberID &" "& Fn_SqlCondition & Fn_SqlOrderBy &""
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
		Do While Not(Fn_Rs.Eof)
			Fn_ID        = KnifeCMS.Data.Escape(Fn_Rs("VisitorID"))
			Fn_Name      = KnifeCMS.Data.Escape(Fn_Rs("Username"))
			Fn_HeadImg   = KnifeCMS.Data.Escape(KnifeCMS.Data.URLDecode(Fn_Rs("HeadImg")))
			Fn_VisitTime = KnifeCMS.Data.Escape(Fn_Rs("VTime"))
			Fn_TempString = """id"":"""& Fn_ID &""",""name"":"""& Fn_Name &""",""headimg"":"""& Fn_HeadImg &""",""visittime"":"""& Fn_VisitTime &""""
			If Fn_ReturnString = "" Then
				Fn_ReturnString = Fn_TempString
			Else
				Fn_ReturnString = Fn_ReturnString &";"& Fn_TempString
			End IF
			Fn_Rs.movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		If Not(Fn_ReturnString="") Then Fn_ReturnString ="{"& Fn_ReturnString &"}"
		GetVisitor = Fn_ReturnString
	End Function
	
	Public Function BeVisited()
		If Not(1=Logined) Then BeVisited="unlogined" : Exit Function
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_Sql,Fn_Rs,Fn_SqlTop,Fn_SqlCondition,Fn_SqlOrderBy,Fn_TempGroupID,Fn_MemberID,Fn_TopNum
		Dim Fn_ID,Fn_Name,Fn_HeadImg,Fn_VisitTime
		Dim Fn_Template,Fn_TempString
		
			Fn_MemberID    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","memberid"))
			If Not(Fn_MemberID>0) Then BeVisited="error" : Exit Function
			Fn_TopNum      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","topnum")) : Fn_TopNum = KnifeCMS.IIF(Fn_TopNum>50,50,Fn_TopNum)
			Fn_SqlTop      = KnifeCMS.IIF(Fn_TopNum>0,"TOP "& Fn_TopNum,"")
			Fn_SqlOrderBy  = " ORDER BY a.ID DESC "
		    Fn_Sql = "SELECT "& Fn_SqlTop &" a.UserID,a.VTime,b.Username,b.HeadImg FROM ["& DBTable_Visitor &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.UserID) WHERE a.VisitorID="& Fn_MemberID &" "& Fn_SqlCondition & Fn_SqlOrderBy &""
			'Die Fn_Sql
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
		Do While Not(Fn_Rs.Eof)
			Fn_ID        = KnifeCMS.Data.Escape(Fn_Rs("UserID"))
			Fn_Name      = KnifeCMS.Data.Escape(Fn_Rs("Username"))
			Fn_HeadImg   = KnifeCMS.Data.Escape(KnifeCMS.Data.URLDecode(Fn_Rs("HeadImg")))
			Fn_VisitTime = KnifeCMS.Data.Escape(Fn_Rs("VTime"))
			Fn_TempString = """id"":"""& Fn_ID &""",""name"":"""& Fn_Name &""",""headimg"":"""& Fn_HeadImg &""",""visittime"":"""& Fn_VisitTime &""""
			If Fn_ReturnString = "" Then
				Fn_ReturnString = Fn_TempString
			Else
				Fn_ReturnString = Fn_ReturnString &";"& Fn_TempString
			End IF
			Fn_Rs.movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		If Not(Fn_ReturnString="") Then Fn_ReturnString ="{"& Fn_ReturnString &"}"
		BeVisited = Fn_ReturnString
	End Function
	
	
End Class
%>