<%
Dim Search
Set Search = New Class_KnifeCMS_Search
Class Class_KnifeCMS_Search
	Private Pv_Rs,Pv_Sql,Pv_TempField,Pv_DataArray,Pv_ShopDataArray,Pv_ContentDataArray,Pv_DataNum,Pv_SubSystem,Pv_Keywords,Pv_WordsArray,Pv_Result,Pv_Temp,Pv_HTML,Pv_TempHTML,Pv_SearchResultHTML
	Private Pv_IsSubSystemExist,Pv_SystemName
	Private Pv_Title,Pv_Thumbnail,Pv_ThumbnailHTML,Pv_Abstract,Pv_URL,Fn_GoodsBN,Fn_GoodsBNHTML
	Private Pv_i
	Private Pv_Count,Pv_PageSize,Pv_Page,Pv_PageUrl,Pv_PageString
	
	Private Sub Class_Initialize()
		Pv_PageSize  = 20
	End Sub
	
	Private Sub Class_Terminate()
	End Sub
	
	Public Sub Init()
		Pv_Keywords   = Trim(KnifeCMS.Data.Left(KnifeCMS.GetForm("both","keywords"),100))
		If Replace(Pv_Keywords,"+","")="" Then
			Pv_WordsArray = Array("+")
		Else
			Pv_Keywords   = KnifeCMS.Data.ReplaceString(Pv_Keywords,Chr(32),"+")
			Pv_WordsArray = Split(Pv_Keywords,"+")
		End If
		'**
		Dim Fn_TempKeywords
		Fn_TempKeywords = Replace(Pv_Keywords,"+"," ")
		SeoTitle         = Replace(Lang_Search(5),"{tpl:keywords}",Fn_TempKeywords)
		MetaKeywords     = Fn_TempKeywords
		MetaDescription  = Replace(Lang_Search(5),"{tpl:keywords}",Fn_TempKeywords)
		'**
		If Pv_Keywords="" Then
			Pv_HTML = "<div class=""result_none"">"& Lang_Search(1) &"</div>"
		Else
			Pv_SubSystem = KnifeCMS.Data.HTMLEnCode(KnifeCMS.GetForm("both","subsys"))
			Select Case Pv_SubSystem
			Case "shop"
				Pv_SearchResultHTML = SearchGoods()
				If Pv_SearchResultHTML="" Then Pv_SearchResultHTML = "<div class=""result_none"">"& Lang_Search(3) &"</div>"
			Case Else
				Pv_SearchResultHTML = SearchContent()
				If Pv_SearchResultHTML="" Then Pv_SearchResultHTML = "<div class=""result_none"">"& Lang_Search(3) &"</div>"
			End Select
			Pv_HTML = "<div class=""search_result_tip"">"& Replace(Replace(Replace(Lang_Search(2),"{tpl:search_count}",KnifeCMS.Data.CLng(Pv_Count)),"{tpl:keywords}",Fn_TempKeywords),"{tpl:subsystem}",Pv_SystemName) &"</div><div class=""search_result"">"& Pv_SearchResultHTML &"</div>"
		End If
		SearchResult = Pv_HTML
	End Sub
	
	Private Function CreateHTML()
		
	End Function
	
	'搜索商品
	Private Function SearchGoods()
		Dim Fn_Rs,Fn_SQLCondition,Fn_Keywords
		Pv_SystemName = Lang_Client_Shop
		TagLine       = TagLine &"<span class=""raquo"">&raquo;</span><a href="""& SiteURL & SystemPath &"?shop"& FileSuffix &""" target=""_blank"">"& Pv_SystemName &"</a><span class=""raquo"">&raquo;</span>"& Lang_Client_Search
		Select Case DB_Type
		Case 0
			Pv_TempField = "IIF(LEN(Thumbnail)=0,FirstImg,Thumbnail)"
		Case 1
			Pv_TempField = "CASE WHEN LEN(Thumbnail)=0 THEN FirstImg ELSE Thumbnail END"
		End Select
		For Pv_i=0 To Ubound(Pv_WordsArray)
			If Pv_WordsArray(Pv_i)<>"" Then
				Fn_Keywords=KnifeCMS.Data.HTMLEncode(Pv_WordsArray(Pv_i))
				Fn_SQLCondition = Fn_SQLCondition &" AND (GoodsBN LIKE '%"& Fn_Keywords &"%' OR GoodsName LIKE '%"& Fn_Keywords &"%' OR Abstract LIKE '%"& Fn_Keywords &"%') "
			End If
		Next
		Pv_Sql = "SELECT TOP 2000 ID,GoodsBN,GoodsName,"& Pv_TempField &",Abstract,SeoUrl FROM ["&DBTable_Goods&"] WHERE Recycle=0 AND Marketable=1 AND GoodsType=1 "& Fn_SQLCondition
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Pv_Sql)
		'Print Pv_Sql
		If Not(Fn_Rs.Eof) Then
			Dim Fn_Temp_i,Fn_Temp_j,Fn_a,Fn_PageCount
			Pv_Count=Fn_Rs.RecordCount : If Fn_Rs.RecordCount<1 then Pv_Count=0
			Fn_Rs.PageSize = Pv_PageSize
			Fn_PageCount   = Fn_Rs.PageCount
			Pv_Page        = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","page")) : Pv_Page = KnifeCMS.IIF(Pv_Page>0,Pv_Page,1)
			If Pv_Page > Fn_PageCount Then
				Fn_Rs.AbsolutePage = Fn_PageCount
			Else
				Fn_Rs.AbsolutePage = Pv_Page
			End If
			Pv_Page = Fn_Rs.AbsolutePage
			ReDim Pv_DataArray(Fn_Rs.Fields.Count,Pv_PageSize)
			For Fn_Temp_i=0 to Pv_PageSize-1
				If Fn_Rs.EOF Then Exit For
				for Fn_Temp_j=0 to Fn_Rs.Fields.Count-1
					Pv_DataArray(Fn_Temp_j,Fn_Temp_i)=Fn_Rs(Fn_Temp_j)
				next
				Fn_Rs.movenext
			Next
			
			'**生成分页地址**
			Pv_PageUrl = SiteURL & SystemPath &"?ctl=search&subsys=shop&page={tpl:page}&keywords="& Pv_Keywords
			'****
			
			Pv_PageString = "<div class=pagestyle><span class=pages>"& Replace(Lang_Pages(0),"{tpl:num}",Pv_Count) &"</span>"
			If Pv_Page>1 Then Pv_PageString = Pv_PageString &"<a href="""& Replace(Pv_PageUrl,"{tpl:page}",Pv_Page-1) &""">"& Lang_Pages(1) &"</a>"
			if Pv_Page>1 then Fn_a=1 : if Pv_Page>2 then Fn_a=2 : if Pv_Page>3 then Fn_a=3 : if Pv_Page>4 then Fn_a=4 : if Pv_Page>5 then Fn_a=5 : if Pv_Page>6 then Fn_a=6 : if Pv_Page>7 then Fn_a=7 : if Pv_Page>8 then Fn_a=8 : if Pv_Page>9 then Fn_a=9
			for Fn_Temp_i=Pv_Page-Fn_a to Pv_Page-1
				Pv_PageString = Pv_PageString & "<a href="""& Replace(Pv_PageUrl,"{tpl:page}",Fn_Temp_i)&""">"&Fn_Temp_i&"</a>"
			next
			Pv_PageString = Pv_PageString &"<span class=current>"&Pv_Page&"</span>"
			
			for Fn_Temp_i=Pv_Page+1 to Pv_Page+9
				if Fn_Temp_i > Fn_PageCount then Exit for
				Pv_PageString = Pv_PageString & "<a href="""& Replace(Pv_PageUrl,"{tpl:page}",Fn_Temp_i) &""">"&Fn_Temp_i&"</a>"
			next
			
			if Not(Pv_Page>=Fn_PageCount) then Pv_PageString = Pv_PageString & "<a href="""& Replace(Pv_PageUrl,"{tpl:page}",Pv_Page+1) &""">"& Lang_Pages(2) &"</a>"
			Pv_PageString = Pv_PageString & "<span class=pages>"& Replace(Lang_Pages(3),"{tpl:num}",Fn_PageCount) &"</span><span class=pages>"& Replace(Lang_Pages(4),"{tpl:num}",Pv_PageSize) &"</span></div>"
		Else
			Pv_DataArray=""
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		
		If IsArray(Pv_DataArray) Then Pv_DataNum=Ubound(Pv_DataArray,2) Else Pv_DataNum=-1
		For Pv_i=0 To Pv_DataNum
			If KnifeCMS.Data.IsNul(Pv_DataArray(0,Pv_i)) Then Exit For
			Fn_GoodsBN  = Pv_DataArray(1,Pv_i)
			Pv_Title    = ReplaceKeywords(Pv_DataArray(2,Pv_i))
			Pv_Thumbnail= Pv_DataArray(3,Pv_i)
			Pv_Abstract = ReplaceKeywords(Pv_DataArray(4,Pv_i))
			Pv_URL      = SystemPath &"?shop/"& Pv_DataArray(0,Pv_i) &"/"& Pv_DataArray(5,Pv_i) & FileSuffix
			Fn_GoodsBNHTML = "<p class=""goodsbn"">"& Lang_Search(4) & ReplaceKeywords(Fn_GoodsBN) &"</p>"
			If Pv_Thumbnail<>"" Then
				Pv_ThumbnailHTML = "<a href="""& Pv_URL &""" target=""_blank""><img src="""& KnifeCMS.Data.URLDecode(Pv_Thumbnail) &""" class=""thumbnail"" /></a>"
			Else
				Pv_ThumbnailHTML = ""
			End If
			Pv_TempHTML = Pv_TempHTML &"<dl><dt><a href="""& Pv_URL &""" target=""_blank"">"& Pv_Title &"</a></dt><dd>"& Pv_ThumbnailHTML & Fn_GoodsBNHTML & Pv_Abstract &"</dd></dl>"
		Next
		SearchGoods = Pv_TempHTML & Pv_PageString
	End Function
	
	'搜索内容
	Private Function SearchContent()
		Dim Fn_Rs,Fn_SQLCondition,Fn_SubSystem,Fn_SeoUrl,Fn_DBTable,Fn_Keywords
		Fn_SubSystem = OS.GetSubSystem(Pv_SubSystem)
		'检查内容模型是否存在
		Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT SystemName,SeoUrl FROM ["& DBTable_ContentSubSystem &"] WHERE System='"& Fn_SubSystem &"'")
		If Not(Rs.Eof) Then
			Pv_IsSubSystemExist = True
			Pv_SystemName       = Rs(0)
			Fn_SeoUrl           = Rs(1)
		Else
			Pv_IsSubSystemExist = False
		End If
		KnifeCMS.DB.CloseRs Rs
		If Pv_IsSubSystemExist=False Then Exit Function
		TagLine   = TagLine &"<span class=""raquo"">&raquo;</span><a href="""& SiteURL & SystemPath &"?content/"& Fn_SubSystem &"/"& Fn_SeoUrl & FileSuffix &""" target=""_blank"">"& Pv_SystemName &"</a><span class=""raquo"">&raquo;</span>"& Lang_Client_Search
		Fn_DBTable = TablePre &"Content_"& Fn_SubSystem
		For Pv_i=0 To Ubound(Pv_WordsArray)
			If Pv_WordsArray(Pv_i)<>"" Then
				Fn_Keywords=KnifeCMS.Data.HTMLEncode(Pv_WordsArray(Pv_i))
				Fn_SQLCondition = Fn_SQLCondition &" AND (Title LIKE '%"& Fn_Keywords &"%' OR Abstract LIKE '%"& Fn_Keywords &"%') "
			End If
		Next
		Pv_Sql = "SELECT TOP 2000 ID,Title,Thumbnail,Abstract,SeoUrl FROM ["& Fn_DBTable &"] WHERE Status=1 AND Recycle=0 "& Fn_SQLCondition
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Pv_Sql)
		If Not(Fn_Rs.Eof) Then
			Dim Fn_Temp_i,Fn_Temp_j,Fn_a,Fn_PageCount
			Pv_Count=Fn_Rs.RecordCount : If Fn_Rs.RecordCount<1 then Pv_Count=0
			Fn_Rs.PageSize = Pv_PageSize
			Fn_PageCount   = Fn_Rs.PageCount
			Pv_Page        = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","page")) : Pv_Page = KnifeCMS.IIF(Pv_Page>0,Pv_Page,1)
			If Pv_Page > Fn_PageCount Then
				Fn_Rs.AbsolutePage = Fn_PageCount
			Else
				Fn_Rs.AbsolutePage = Pv_Page
			End If
			Pv_Page = Fn_Rs.AbsolutePage
			ReDim Pv_DataArray(Fn_Rs.Fields.Count,Pv_PageSize)
			For Fn_Temp_i=0 to Pv_PageSize-1
				If Fn_Rs.EOF Then Exit For
				for Fn_Temp_j=0 to Fn_Rs.Fields.Count-1
					Pv_DataArray(Fn_Temp_j,Fn_Temp_i)=Fn_Rs(Fn_Temp_j)
				next
				Fn_Rs.movenext
			Next
			
			'**生成分页地址**
			Pv_PageUrl = SiteURL & SystemPath &"?ctl=search&subsys="& Pv_SubSystem &"&page={tpl:page}&keywords="& Pv_Keywords
			'****
			
			Pv_PageString = "<div class=pagestyle><span class=pages>"& Replace(Lang_Pages(0),"{tpl:num}",Pv_Count) &"</span>"
			If Pv_Page>1 Then Pv_PageString = Pv_PageString &"<a href="""& Replace(Pv_PageUrl,"{tpl:page}",Pv_Page-1) &""">"& Lang_Pages(1) &"</a>"
			if Pv_Page>1 then Fn_a=1 : if Pv_Page>2 then Fn_a=2 : if Pv_Page>3 then Fn_a=3 : if Pv_Page>4 then Fn_a=4 : if Pv_Page>5 then Fn_a=5 : if Pv_Page>6 then Fn_a=6 : if Pv_Page>7 then Fn_a=7 : if Pv_Page>8 then Fn_a=8 : if Pv_Page>9 then Fn_a=9
			for Fn_Temp_i=Pv_Page-Fn_a to Pv_Page-1
				Pv_PageString = Pv_PageString & "<a href="""& Replace(Pv_PageUrl,"{tpl:page}",Fn_Temp_i)&""">"&Fn_Temp_i&"</a>"
			next
			Pv_PageString = Pv_PageString &"<span class=current>"&Pv_Page&"</span>"
			
			for Fn_Temp_i=Pv_Page+1 to Pv_Page+9
				if Fn_Temp_i > Fn_PageCount then Exit for
				Pv_PageString = Pv_PageString & "<a href="""& Replace(Pv_PageUrl,"{tpl:page}",Fn_Temp_i) &""">"&Fn_Temp_i&"</a>"
			next
			
			if Not(Pv_Page>=Fn_PageCount) then Pv_PageString = Pv_PageString & "<a href="""& Replace(Pv_PageUrl,"{tpl:page}",Pv_Page+1) &""">"& Lang_Pages(2) &"</a>"
			Pv_PageString = Pv_PageString & "<span class=pages>"& Replace(Lang_Pages(3),"{tpl:num}",Fn_PageCount) &"</span><span class=pages>"& Replace(Lang_Pages(4),"{tpl:num}",Pv_PageSize) &"</span></div>"
		Else
			Pv_DataArray=""
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		
		If IsArray(Pv_DataArray) Then Pv_DataNum=Ubound(Pv_DataArray,2) Else Pv_DataNum=-1
		For Pv_i=0 To Pv_DataNum
		
			If KnifeCMS.Data.IsNul(Pv_DataArray(0,Pv_i)) Then Exit For
			Pv_Title    = ReplaceKeywords(Pv_DataArray(1,Pv_i))
			Pv_Thumbnail= Pv_DataArray(2,Pv_i)
			Pv_Abstract = ReplaceKeywords(Pv_DataArray(3,Pv_i))
			Pv_URL      = SystemPath &"?content/"& Fn_SubSystem &"/"& Pv_DataArray(0,Pv_i) &"/"& Pv_DataArray(4,Pv_i) & FileSuffix
			If Pv_Thumbnail<>"" Then
				Pv_ThumbnailHTML = "<a href="""& Pv_URL &""" target=""_blank""><img src="""& KnifeCMS.Data.URLDecode(Pv_Thumbnail) &""" class=""thumbnail"" /></a>"
			Else
				Pv_ThumbnailHTML = ""
			End If
			Pv_TempHTML = Pv_TempHTML &"<dl><dt><a href="""& Pv_URL &""" target=""_blank"">"& Pv_Title &"</a></dt><dd>"& Pv_ThumbnailHTML & Pv_Abstract &"</dd></dl>"
		Next
		SearchContent = Pv_TempHTML & Pv_PageString
	End Function
	
	'搜索关键词标红
	Private Function ReplaceKeywords(ByVal BV_String)
		Dim Fn_i,Fn_Temp
		Fn_Temp = BV_String
		For Fn_i=0 To Ubound(Pv_WordsArray)
			Fn_Temp = Replace(Fn_Temp,Pv_WordsArray(Fn_i),"<font class=""keywords"">"& Pv_WordsArray(Fn_i) &"</font>")
		Next
		ReplaceKeywords = Fn_Temp
	End Function
End Class
%>