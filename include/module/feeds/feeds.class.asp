<%
Dim Feeds
Set Feeds = New Class_Feeds
Class Class_Feeds
	
	Private Pv_Rs,Pv_Array
	Private S_TempHTML,S_AllowedDepth,S_ReplyTpl
	
	Private Sub Class_Initialize()
		S_AllowedDepth = 3 '评论允许最大级数
		S_ReplyTpl     = "<div class=""ifeeds_list2"" subsys=""{tpl:subsys}"" feedsid=""{tpl:id}"" author=""{tpl:userid}"" path=""{tpl:path}""><div class=""author""><a href=""{tpl:userhomeurl}"" ip=""{tpl:ip}"" target=""_blank""><img src=""{tpl:headimg}"" /></a></div><div class=""ctbody""><div class=""uinfo""><a href=""{tpl:userhomeurl}"" class=""name"" target=""_blank"">{tpl:username}</a><span class=""postTime"">{tpl:posttime}</span></div><div class=""cttext"">{tpl:content}<span class=""do"">{tpl:doreply}</span></div><div id=""{tpl:subsys}_feedsreply_{tpl:id}"" subsys=""{tpl:subsys}"" feedsid=""{tpl:id}"">{tpl:replys}</div></div></div>"
	End Sub
	Private Sub Class_Terminate()
	End Sub
	
	Public Property Let Depth(ByVal BV_Depth)
		S_AllowedDepth = LCase(BV_Depth)
	End Property
	Public Property Get Depth()
		Depth = S_AllowedDepth
	End Property
	
	Public Function PostMood()
		PostMood = Posting("mood")
	End Function
	
	Public Function PostGuestBook()
		PostGuestBook = Posting("guestbook")
	End Function
	
	Public Function Posting(ByVal BV_Type)
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_DBTable,Fn_Content,Fn_ID,Fn_IP,Fn_Path
		Dim Fn_MemberID
		If Not(1=Logined) Then Posting="unlogined" : Exit Function
		Select Case BV_Type
			Case "mood"
				Fn_DBTable   = DBTable_Members_Feeds
				Fn_MemberID  = UserID
			Case "guestbook"
				Fn_DBTable   = DBTable_Members_GuestBook
				Fn_MemberID  = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","memberid"))
				'检查用户是否存在
				If KnifeCMS.Data.IsNul(KnifeCMS.DB.GetFieldByID(DBTable_Members,"ID",Fn_MemberID)) Then Posting="error" : Exit Function
			Case Else
				Posting="error" : Exit Function
		End Select
		Fn_Content = KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(KnifeCMS.GetForm("post","content")),250)
		'die Fn_Content
		If KnifeCMS.Data.IsNul(Fn_Content) Then Posting="null" : Exit Function
		'检查是否恶意发布(15秒钟内不许再发布)
		Dim ForbidPostTime : ForbidPostTime = 15 '秒
		If CheckIsInForbidPostTime(ForbidPostTime,"SELECT TOP 1 AddTime FROM ["& Fn_DBTable &"] WHERE UserID="& UserID &" ORDER BY ID DESC ",SysTime) Then Posting="inforbidtime_"&ForbidPostTime : Exit Function
		'Fn_DBTable = DBTable_Members_Feeds
		Fn_IP      = KnifeCMS.GetClientIP()
		SysID      = KnifeCMS.CreateSysID
		Fn_Result  = false
		Fn_Result  = KnifeCMS.DB.AddRecord(Fn_DBTable,Array("SysID:"& SysID,"MemberID:"& Fn_MemberID,"P_ID:0","Path:0" ,"Depth:1" ,"ChildNum:0","Content:"& Fn_Content,"UserID:"& UserID,"Username:"& Username,"HeadImg:"& UserSmallHead,"AddTime:"& SysTime,"UpdateTime:"& SysTime,"IP:"& Fn_IP,"Disabled:0"))
		'更新此记录的路径path
		Fn_ID = KnifeCMS.DB.GetFieldBySysID(Fn_DBTable,"ID",SysID)
		Fn_Path = ","& Fn_ID &","
		Call KnifeCMS.DB.UpdateRecord(Fn_DBTable,Array("Path:"&Fn_Path),Array("ID:"&Fn_ID))
		If Fn_Result Then Posting="ok"
	End Function
	
	Public Function PostReply()
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_DBTable,Fn_ID,Fn_Content,Fn_Path,Fn_P_ID,Fn_P_Path,Fn_P_Depth,Fn_IP,Fn_Disabled
		Dim Fn_MemberID,Fn_Array
		If Not(1=Logined) Then PostReply="unlogined" : Exit Function
		Select Case SubSystem
			Case "myfeeds","friendfeeds","allfeeds","usermood"
				Fn_DBTable   = DBTable_Members_Feeds
			Case "guestbook"
				Fn_DBTable   = DBTable_Members_GuestBook
				S_AllowedDepth = 2
			Case Else
				PostReply="error" : Exit Function
		End Select
		
		Fn_Content = KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(KnifeCMS.GetForm("post","content")),250)
		'If KnifeCMS.Data.IsNul(Fn_Content) Then PostReply="null" : Exit Function
		'检查是否恶意发布(15秒钟内不许再发布)
		Dim ForbidPostTime : ForbidPostTime = 15 '秒
		If CheckIsInForbidPostTime(ForbidPostTime,"SELECT TOP 1 AddTime FROM ["& DBTable_Members_Feeds &"] WHERE UserID="& UserID &" ORDER BY ID DESC ",SysTime) Then PostReply="inforbidtime_"&ForbidPostTime : Exit Function
		
		'Fn_DBTable = DBTable_Members_Feeds
		Fn_P_ID    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","p_id"))
		
		'检查Fn_P_ID对应的内容是否存在
		Fn_MemberID = KnifeCMS.DB.GetFieldByID(Fn_DBTable,"MemberID",Fn_P_ID)
		If KnifeCMS.Data.IsNul(Fn_MemberID) Then PostReply="error" : Exit Function
		'获取Fn_P_ID对应Path,Depth
		If Fn_P_ID > 0 Then
			Fn_Array = Get_P_PathAndDepth(Fn_DBTable,Fn_P_ID)
			Fn_P_Path = Fn_Array(0) : Fn_P_Depth = Fn_Array(1)
		End If
		
		'检查是否超出允许的深度Depth
		If Fn_P_Depth+1 > S_AllowedDepth Then PostReply="error" : Exit Function
		
		Fn_Disabled = 0
		Fn_IP       = KnifeCMS.GetClientIP()
		SysID       = KnifeCMS.CreateSysID

		Fn_Result  = KnifeCMS.DB.AddRecord(Fn_DBTable,Array("SysID:"& SysID,"MemberID:"& Fn_MemberID,"P_ID:"& Fn_P_ID,"Path:0" ,"Depth:"& Fn_P_Depth+1 ,"ChildNum:0","Content:"& Fn_Content,"UserID:"& UserID,"Username:"& Username,"HeadImg:"& UserSmallHead,"AddTime:"& SysTime,"UpdateTime:"& SysTime,"IP:"& Fn_IP,"Disabled:"&Fn_Disabled))

		Fn_ID   = KnifeCMS.DB.GetFieldBySysID(Fn_DBTable,"ID",SysID)
		Fn_Path = Fn_P_Path & Fn_ID &","
		Call KnifeCMS.DB.UpdateRecord(Fn_DBTable,Array("Path:"&Fn_Path),Array("ID:"&Fn_ID))
		Call KnifeCMS.DB.UpdateRecord(Fn_DBTable,Array("ChildNum:ChildNum+1","UpdateTime:"& SysTime),Array("ID:"&Fn_P_ID))
		If Fn_Result Then PostReply="ok"
	End Function
	
	'转发
	Public Function RepayFeeds()
		If Not(1=Logined) Then RepayFeeds="unlogined" : Exit Function
		Dim ForbidPostTime : ForbidPostTime = 15 '秒
		If CheckIsInForbidPostTime(ForbidPostTime,"SELECT TOP 1 AddTime FROM ["& DBTable_Members_Feeds &"] WHERE UserID="& UserID &" ORDER BY ID DESC ",SysTime) Then RepayFeeds="inforbidtime_"&ForbidPostTime : Exit Function
		
		Dim Fn_Rs,Fn_Result,Fn_ReturnString
		Dim Fn_ID,Fn_FeedsID,Fn_Content,Fn_MemberID,Fn_Username,Fn_IP,Fn_Path
		Dim Fn_FeedsExist : Fn_FeedsExist = False
		
		Fn_FeedsID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","feedsid"))
		If Not(Fn_FeedsID>0) Then RepayFeeds="error" : Exit Function
		
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Members_Feeds &":MemberID,Content,Username",Array("ID:"& Fn_FeedsID),"")
		If Not(Fn_Rs.Eof) Then
			Fn_FeedsExist = True
			Fn_MemberID   = Fn_Rs(0)
			Fn_Content    = Fn_Rs(1)
			Fn_Username   = Fn_Rs(2)
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		
		If Fn_FeedsExist = False Then RepayFeeds="error" : Exit Function
		Fn_Content = KnifeCMS.Data.HTMLEncode(Lang_Repay &"<a class=""name"" href=""?user/"& Fn_MemberID & FileSuffix &""" target=""_blank"">#"& Fn_Username &"#</a> ") & Fn_Content
		
		Fn_MemberID= UserID
		Fn_IP      = KnifeCMS.GetClientIP()
		SysID      = KnifeCMS.CreateSysID
		Fn_Result  = False
		Fn_Result  = KnifeCMS.DB.AddRecord(DBTable_Members_Feeds,Array("SysID:"& SysID,"MemberID:"& Fn_MemberID,"P_ID:0","Path:0" ,"Depth:1" ,"ChildNum:0","Content:"& Fn_Content,"UserID:"& UserID,"Username:"& Username,"HeadImg:"& UserSmallHead,"AddTime:"& SysTime,"UpdateTime:"& SysTime,"IP:"& Fn_IP,"Disabled:0"))
		'更新此记录的路径path
		Fn_ID = KnifeCMS.DB.GetFieldBySysID(DBTable_Members_Feeds,"ID",SysID)
		Fn_Path = ","& Fn_ID &","
		Call KnifeCMS.DB.UpdateRecord(DBTable_Members_Feeds,Array("Path:"&Fn_Path),Array("ID:"&Fn_ID))
		If Fn_Result Then RepayFeeds="ok"
	End Function
	
	
	Public Function GetFeeds()
		If Not(1=Logined) Then GetFeeds="unlogined" : Exit Function
		Dim Fn_Result,Fn_ReturnString,Fn_JS_SubSystem
		Dim Fn_DBTable,Fn_Sql,Fn_Rs,Fn_ID,Fn_MemberID,Fn_SqlCondition,Fn_SqlOrderBy,Fn_SubSysText
		Dim Fn_IsPage,Fn_PageSize,Fn_Page,Fn_PageString
		    Fn_IsPage     = True
			Fn_SubSysText = ""
		Select Case SubSystem
			Case "usermood"
				Fn_JS_SubSystem = "um"
				Fn_DBTable      = DBTable_Members_Feeds
				Fn_MemberID     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","memberid"))
				Fn_SqlCondition = " a.MemberID="& Fn_MemberID &" AND "
				Fn_SqlOrderBy   = " ORDER BY a.UpdateTime DESC "
				Fn_PageSize     = 20
				Fn_ID           = "myfeeds_ctbody_{tpl:id}"
			Case "myfeeds"
				Fn_JS_SubSystem = "mf"
				Fn_DBTable       = DBTable_Members_Feeds
				Fn_MemberID     = UserID
				Fn_SqlCondition = " a.MemberID="& Fn_MemberID &" AND a.ChildNum > 0 AND "
				Fn_SqlOrderBy   = " ORDER BY a.UpdateTime DESC "
				Fn_PageSize     = 10
				Fn_ID           = "myfeeds_ctbody_{tpl:id}"
			Case "friendfeeds"
				Fn_JS_SubSystem = "ff"
				Fn_DBTable      = DBTable_Members_Feeds
				Fn_MemberID     = UserID
				Fn_SqlCondition = " (a.MemberID = "& Fn_MemberID &" Or a.MemberID IN ( SELECT FriendID FROM ["& DBTable_Friend &"] WHERE UserID="& Fn_MemberID &")) AND "
				Fn_SqlOrderBy   = " ORDER BY a.UpdateTime DESC "
				Fn_PageSize     = 20
				Fn_ID           = "friendfeeds_ctbody_{tpl:id}"
			Case "allfeeds"
				Fn_JS_SubSystem = "af"
				Fn_DBTable      = DBTable_Members_Feeds
				Fn_MemberID     = UserID
				Fn_SqlCondition = ""
				Fn_SqlOrderBy   = " ORDER BY a.UpdateTime DESC "
				Fn_PageSize     = 20
				Fn_ID           = "allfeeds_ctbody_{tpl:id}"
			Case "guestbook"
				Fn_JS_SubSystem = "gb"
				Fn_DBTable      = DBTable_Members_GuestBook
				Fn_MemberID     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","memberid"))
				Fn_SqlCondition = " a.MemberID="& Fn_MemberID &" AND "
				Fn_SqlOrderBy   = " ORDER BY a.AddTime DESC "
				Fn_PageSize     = 20
				Fn_ID           = "guestbook_ctbody_{tpl:id}"
				S_AllowedDepth  = 2
				Fn_SubSysText   = " "&Lang_Guestbook
			Case Else
				GetFeeds="error" : Exit Function
		End Select
		
		Dim Fn_TieTemplate,Fn_TempString,Fn_TieString,Fn_TieReplyString,Fn_UserID,Fn_Username,Fn_HeadImg,Fn_IP
		Fn_TieTemplate = "<div class=""ifeeds_list"" subsys="""& SubSystem &""" feedsid=""{tpl:id}"" author=""{tpl:userid}"" path=""{tpl:path}"">"
        Fn_TieTemplate = Fn_TieTemplate &"<div class=""author""><a href=""{tpl:userhomeurl}"" ip=""{tpl:ip}"" target=""_blank""><img src=""{tpl:headimg}"" /></a></div>"
        Fn_TieTemplate = Fn_TieTemplate &"<div class=""ctbody"" id="& Fn_ID &">"
        Fn_TieTemplate = Fn_TieTemplate &"<div class=""cttext""><a  href=""{tpl:userhomeurl}"" class=""name"" target=""_blank"" >{tpl:username}</a>"& Fn_SubSysText &" : {tpl:content}</div>"
		Fn_TieTemplate = Fn_TieTemplate &"<div class=""uinfo""><span class=""postTime"">{tpl:posttime}</span><span class=""do""><a href=""javascript:;"" onclick=""Myhome.Feeds.GetReplyOfTheDoing(this,'"& SubSystem &"','{tpl:id}')"">"& Lang_ShrinkReply &"</a>"& KnifeCMS.IIF(Fn_JS_SubSystem<>"gb"," | <a href=""javascript:;"" onclick=""Myhome.Feeds.RelayFeeds(this,'{tpl:id}')"">"& Lang_Repay &"</a>","") &"</span></div>"
		Fn_TieTemplate = Fn_TieTemplate &"<div name=""feedsreply"" subsys="""& SubSystem &""" feedsid=""{tpl:id}"" memberid="""& Fn_MemberID &""" id="""& SubSystem &"_feedsreply_{tpl:id}"">{tpl:replys}</div></div></div>"
		
		Fn_Sql="SELECT TOP 1000 a.ID,a.P_ID,a.Path,a.ChildNum,a.Content,a.UserID,b.Username,b.HeadImg,a.AddTime,a.IP,a.Depth FROM ["& Fn_DBTable &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.UserID) WHERE "& Fn_SqlCondition &" a.Depth=1 AND a.Disabled=0 "& Fn_SqlOrderBy &""
		'Die Fn_Sql
		
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
		If Fn_IsPage Then
			If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then
				Dim Fn_Temp_i,Fn_Temp_j,Fn_Temp_k,Fn_a,Fn_RsCount,Fn_PageCount,Fn_DataArray
				Fn_RsCount=Fn_Rs.RecordCount : If Fn_Rs.RecordCount<1 then Fn_RsCount=0
				Fn_Rs.PageSize = Fn_PageSize
				Fn_PageCount = Fn_Rs.PageCount
				Fn_Page = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","page")) : Fn_Page = KnifeCMS.IIF(Not(Fn_Page>0),1,Fn_Page)
				'Die KnifeCMS.URLQueryString
				if Fn_Page > Fn_PageCount then
					Fn_Rs.AbsolutePage = Fn_PageCount
				else
					Fn_Rs.AbsolutePage = Fn_Page
				end if
				Fn_Page = Fn_Rs.AbsolutePage
				ReDim DataArray(Fn_Rs.Fields.Count,Fn_PageSize)
				
				For Fn_Temp_i=0 to Fn_PageSize-1
					If Fn_Rs.EOF Then Exit For
					Fn_UserID    = Fn_Rs("UserID")
					Fn_Username  = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_Rs("Username")),Lang_Guest,Fn_Rs("Username"))
					Fn_HeadImg   = KnifeCMS.Data.URLDecode(Fn_Rs("HeadImg"))
					Fn_HeadImg   = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_HeadImg),TemplatePath &"images/userhead_s.jpg",Fn_HeadImg)
					Fn_IP        = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_Rs("IP")),"",Fn_Rs("IP"))
					Fn_TempString     = Fn_TieTemplate
					Fn_TieReplyString = ""
					Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:id}",Fn_Rs("ID"))
					Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:path}",Fn_Rs("Path"))
					Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:userid}",Fn_UserID)
					Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:headimg}",Fn_HeadImg)
					Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:username}",Fn_Username)
					Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:userhomeurl}","?user/"& Fn_UserID & FileSuffix)
					Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:content}",KnifeCMS.Data.HTMLDecode(Fn_Rs("Content")))
					Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:posttime}",Fn_Rs("AddTime"))
					Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:ip}",Fn_IP)
					If Fn_Rs("ChildNum") > 0 Then Fn_TieReplyString = GetFeedsReply(Fn_DBTable,Fn_Rs("ID"))
					Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:replys}",Fn_TieReplyString)
					Fn_TieString  = Fn_TieString & Fn_TempString
					Fn_Rs.movenext()
				Next
				Fn_PageString = "<div class=""mt5 mb5""><div class=pagestyle><span class=pages>"& Replace(Lang_Pages(0),"{tpl:num}",Fn_RsCount) &"</span>"
				If Fn_Page>1 Then Fn_PageString = Fn_PageString &"<a href=""javascript:Myhome.Feeds.Page('"& Fn_JS_SubSystem &"','"& Fn_MemberID &"',"&Fn_Page-1&")"">"& Lang_Pages(1) &"</a>"
				if Fn_Page>1 then Fn_a=1 : if Fn_Page>2 then Fn_a=2 : if Fn_Page>3 then Fn_a=3 : if Fn_Page>4 then Fn_a=4 : if Fn_Page>5 then Fn_a=5 : if Fn_Page>6 then Fn_a=6 : if Fn_Page>7 then Fn_a=7 : if Fn_Page>8 then Fn_a=8 : if Fn_Page>9 then Fn_a=9
				for Fn_Temp_i=Fn_Page-Fn_a to Fn_Page-1
					Fn_PageString = Fn_PageString & "<a href=""javascript:Myhome.Feeds.Page('"& Fn_JS_SubSystem &"','"& Fn_MemberID &"',"&Fn_Temp_i&")"">"&Fn_Temp_i&"</a>"
				next
				Fn_PageString = Fn_PageString &"<span class=current>"&Fn_Page&"</span>"
				
				for Fn_Temp_i=Fn_Page+1 to Fn_Page+9
					if Fn_Temp_i > Fn_PageCount then Exit for
					Fn_PageString = Fn_PageString & "<a href=""javascript:Myhome.Feeds.Page('"& Fn_JS_SubSystem &"','"& Fn_MemberID &"',"&Fn_Temp_i&")"">"&Fn_Temp_i&"</a>"
				next
				
				if Not(Fn_Page>=Fn_PageCount) then Fn_PageString = Fn_PageString & "<a href=""javascript:Myhome.Feeds.Page('"& Fn_JS_SubSystem &"','"& Fn_MemberID &"',"&Fn_Page+1&")"">"& Lang_Pages(2) &"</a>"
				Fn_PageString = Fn_PageString & "<span class=pages>"& Replace(Lang_Pages(3),"{tpl:num}",Fn_PageCount) &"</span><span class=pages>"& Replace(Lang_Pages(4),"{tpl:num}",Fn_PageSize) &"</span></div></div>"
			End If
		Else
			Do While Not(Fn_Rs.Eof)
				Fn_UserID    = Fn_Rs(5)
				Fn_Username  = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_Rs(6)),Lang_Guest,Fn_Rs(6))
				Fn_HeadImg   = KnifeCMS.Data.URLDecode(Fn_Rs(7))
				Fn_HeadImg   = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_HeadImg),TemplatePath &"images/userhead_s.jpg",Fn_HeadImg)
				Fn_IP        = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_Rs(10)),"",Fn_Rs(10))
				Fn_TempString     = Fn_TieTemplate
				Fn_TieReplyString = ""
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:id}",Fn_Rs(0))
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:path}",Fn_Rs("Path"))
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:userid}",Fn_UserID)
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:headimg}",Fn_HeadImg)
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:username}",Fn_Username)
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:userhomeurl}","?user/"& Fn_UserID & FileSuffix)
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:content}",Fn_Rs(4))
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:posttime}",Fn_Rs(9))
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:ip}",Fn_IP)
				If Fn_Rs("ChildNum") > 0 Then Fn_TieReplyString = GetFeedsReply(Fn_DBTable,Fn_Rs(0))
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:replys}",Fn_TieReplyString)
				Fn_TieString = Fn_TieString & Fn_TempString
				Fn_Rs.movenext()
			Loop
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		Fn_ReturnString = Fn_TieString & Fn_PageString
		If Fn_ReturnString = "" Then
			GetFeeds = "NULL"
		Else
			GetFeeds = Fn_ReturnString
		End If
		
	End Function
	
	Public Function GetFeedsReply(ByVal BV_DBTable, ByVal BV_FeedsID)
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_Sql,Fn_Rs
		Dim Fn_ID,Fn_HeadImg,Fn_IP,Fn_TempTpl,Fn_ReplyString
		Fn_Sql="SELECT TOP 100 a.ID,a.P_ID,a.Path,a.ChildNum,a.Content,a.UserID,b.Username,b.HeadImg,a.AddTime,a.UpdateTime,a.IP,a.Depth FROM ["& BV_DBTable &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.UserID) WHERE a.P_ID="& BV_FeedsID &" AND a.Disabled=0 ORDER BY a.ID ASC"
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
		Do While Not(Fn_Rs.Eof)
			    Fn_TempTpl     = S_ReplyTpl
				Fn_ReplyString = ""
				Fn_HeadImg     = KnifeCMS.Data.URLDecode(Fn_Rs(7))
				Fn_HeadImg     = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_HeadImg),TemplatePath &"images/userhead_s.jpg",Fn_HeadImg)
				Fn_IP          = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_Rs(10)),"",Fn_Rs(10))
				
				If Fn_Rs("Depth")+1 > S_AllowedDepth Then
					Fn_TempTpl = KnifeCMS.Data.ReplaceString(Fn_TempTpl,"{tpl:doreply}","")
				Else
					Fn_TempTpl = KnifeCMS.Data.ReplaceString(Fn_TempTpl,"{tpl:doreply}","<a href=""javascript:Myhome.Feeds.ToReply(this,'{tpl:subsys}','{tpl:id}');"">"& Lang_Reply &"</a>")
				End If
				Fn_TempTpl = KnifeCMS.Data.ReplaceString(Fn_TempTpl,"{tpl:id}",Fn_Rs("ID"))
				Fn_TempTpl = KnifeCMS.Data.ReplaceString(Fn_TempTpl,"{tpl:subsys}",SubSystem)
				Fn_TempTpl = KnifeCMS.Data.ReplaceString(Fn_TempTpl,"{tpl:path}",Fn_Rs("Path"))
				Fn_TempTpl = KnifeCMS.Data.ReplaceString(Fn_TempTpl,"{tpl:userid}",Fn_Rs("UserID"))
				Fn_TempTpl = KnifeCMS.Data.ReplaceString(Fn_TempTpl,"{tpl:headimg}",Fn_HeadImg)
				Fn_TempTpl = KnifeCMS.Data.ReplaceString(Fn_TempTpl,"{tpl:username}",Fn_Rs("Username"))
				Fn_TempTpl = KnifeCMS.Data.ReplaceString(Fn_TempTpl,"{tpl:userhomeurl}","?user/"& Fn_Rs("UserID") & FileSuffix)
				Fn_TempTpl = KnifeCMS.Data.ReplaceString(Fn_TempTpl,"{tpl:content}",Fn_Rs("Content"))
				Fn_TempTpl = KnifeCMS.Data.ReplaceString(Fn_TempTpl,"{tpl:posttime}",Fn_Rs("AddTime"))
				Fn_TempTpl = KnifeCMS.Data.ReplaceString(Fn_TempTpl,"{tpl:ip}",Fn_IP)
				'获取回复
				If Fn_Rs("ChildNum") > 0 Then Fn_ReplyString = GetFeedsReply(BV_DBTable,Fn_Rs("ID"))
				Fn_TempTpl = KnifeCMS.Data.ReplaceString(Fn_TempTpl,"{tpl:replys}",Fn_ReplyString)
				Fn_ReturnString = Fn_ReturnString & Fn_TempTpl
				Fn_Rs.movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		GetFeedsReply = Fn_ReturnString
	End Function
	
	Public Function FeedsDelete()
	    If Not(1=Logined) Then FeedsDelete="unlogined" : Exit Function
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_DBTable,Fn_Rs,Fn_FeedsExist
		Dim Fn_FeedsID,Fn_Path,Fn_Depth,Fn_UserID,Fn_AddTime,Fn_P_ID,Fn_Top_P_ID
		    Fn_ReturnString = "error"
			Fn_FeedsID      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","feedsid"))
			Fn_Depth        = 0
			Fn_FeedsExist   = false
		If Not(Fn_FeedsID>0) Then FeedsDelete="error" : Exit Function
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Members_Feeds&":Path,Depth,UserID,AddTime,P_ID",Array("ID:"& Fn_FeedsID),"")
		If Not(Fn_Rs.Eof) Then
			Fn_FeedsExist = true
			Fn_Path       = Fn_Rs(0)
			Fn_Depth      = KnifeCMS.Data.CLng(Fn_Rs(1))
			Fn_UserID     = Fn_Rs(2)
			Fn_AddTime    = Fn_Rs(3)
			Fn_P_ID       = Fn_Rs(4)
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		If Not(Fn_FeedsExist) Then FeedsDelete="notexist" : Exit Function
		'如果顶级Feeds的作者是自己可自由删除
		If Fn_Depth>1 And Fn_UserID<>UserID Then 
		    Fn_UserID = ""
			If Not(KnifeCMS.Data.IsNul(Fn_Path)) Then
				Fn_Top_P_ID=Split(Fn_Path,",")(1)
				If Fn_Top_P_ID>0 Then
					Fn_UserID = KnifeCMS.DB.GetFieldByID(DBTable_Members_Feeds,"UserID",Fn_Top_P_ID)
				End If
			End If
		End If
		If Fn_UserID<>UserID Or KnifeCMS.Data.IsNul(Fn_Path) Then FeedsDelete="error" : Exit Function
		'如过是Feeds的作者是自己则可自由删除
		Fn_Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_Members_Feeds &"] WHERE Path LIKE '"& Fn_Path &"%'")
		If Fn_Result Then
			'父级子回复数减1
			If Fn_P_ID>0 Then Call KnifeCMS.DB.UpdateRecord(DBTable_Members_Feeds,"ChildNum=ChildNum-1",Array("ID:"& Fn_P_ID))
			Fn_ReturnString="ok"
		End If
		FeedsDelete=Fn_ReturnString
	End Function
	
	Public Function GuestBookDelete()
		If Not(1=Logined) Then GuestBookDelete="unlogined" : Exit Function
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_DBTable,Fn_Rs,Fn_FeedsExist
		Dim Fn_FeedsID,Fn_Path,Fn_Depth,Fn_UserID,Fn_AddTime,Fn_P_ID,Fn_Top_P_ID,Fn_MemberID
		    Fn_ReturnString = "error"
			Fn_FeedsID      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","feedsid"))
			Fn_Depth        = 0
			Fn_FeedsExist   = false
		If Not(Fn_FeedsID>0) Then GuestBookDelete="error" : Exit Function
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Members_GuestBook&":MemberID,Path,Depth,UserID,AddTime,P_ID",Array("ID:"& Fn_FeedsID),"")
		If Not(Fn_Rs.Eof) Then
			Fn_FeedsExist = true
			Fn_MemberID   = KnifeCMS.Data.CLng(Fn_Rs(0))
			Fn_Path       = Fn_Rs(1)
			Fn_Depth      = KnifeCMS.Data.CLng(Fn_Rs(2))
			Fn_UserID     = KnifeCMS.Data.CLng(Fn_Rs(3))
			Fn_AddTime    = Fn_Rs(4)
			Fn_P_ID       = Fn_Rs(5)
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		If Not(Fn_FeedsExist) Then GuestBookDelete="notexist" : Exit Function
		If Fn_MemberID>0 AND Fn_MemberID=UserID Then
			'如果是自己的留言板则可自由删除
			Fn_Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_Members_GuestBook &"] WHERE Path LIKE '"& Fn_Path &"%'")
			'父级子回复数减1
			If Fn_Result AND Fn_P_ID>0 Then Call KnifeCMS.DB.UpdateRecord(DBTable_Members_GuestBook,"ChildNum=ChildNum-1",Array("ID:"& Fn_P_ID))
		Else
			'如果不是自己的留言板则只可删除自己留下的言
			If Fn_UserID=UserID AND Not(KnifeCMS.Data.IsNul(Fn_Path)) Then
				Fn_Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_Members_GuestBook &"] WHERE Path LIKE '"& Fn_Path &"%'")
				If Fn_Result AND Fn_P_ID>0 Then Call KnifeCMS.DB.UpdateRecord(DBTable_Members_GuestBook,"ChildNum=ChildNum-1",Array("ID:"& Fn_P_ID))
			Else
				Fn_ReturnString="error"
			End If
		End If
		If Fn_Result Then Fn_ReturnString="ok"
		GuestBookDelete=Fn_ReturnString
	End Function
	
End Class
%>