<!--#include file="../../../config/config.main.asp"-->
<!--#include file="../../kfc.main.asp"-->
<!--#include file="../../kfc.common.asp"-->
<!--#include file="../../kfc.initialization.asp"-->
<%
Dim GetOrderData
Set GetOrderData = New Class_KnifeCMS_GetOrderData
Set GetOrderData = Nothing
Class Class_KnifeCMS_GetOrderData

	Private Pv_OrderID,Pv_UserID,Pv_UserName,Pv_Confirm,Pv_Createtime,Pv_IsTax,Pv_IsProtect
	Private Pv_Cost_Item,Pv_Cost_Freight,Pv_Cost_Protect,Pv_Cost_Tax,Pv_Pmt_Amount,Pv_Cost_Payment,Pv_Discount,Pv_Total_Amount,Pv_Final_Amount,Pv_Payed
	Private Pv_Tax_Company,Pv_Weight,Pv_ShippingID,Pv_Shipping,Pv_PaymentID,Pv_Payment,Pv_Currency,Pv_ItemNum,Pv_DeliveryCorpID
	Private Pv_ShippingArea,Pv_Ship_Name,Pv_Ship_Area,Pv_Ship_Address,Pv_Ship_Zip,Pv_Ship_Tel,Pv_Ship_Email,Pv_Ship_Mobile,Pv_Ship_Time,Pv_Mark_Text,Pv_Ship_AreaGrade,Pv_Ship_AreaArray
	Private Pv_Status,Pv_PayStatus,Pv_ShipStatus,Pv_PaymentData,Pv_ShippingData
	
	Private Sub Class_Initialize()
		IF KnifeCMS.CheckPost Then Init : Else Echo "ERROR"
	End Sub
	Private Sub Class_Terminate()
		Set OS = Nothing
	End Sub
	
	Private Sub Init()
		Select Case Ctl			
			Case "getorderdata"
				Pv_OrderID = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("get","orderid"),"[^0-9]",""),16)
				If Len(Pv_OrderID)<>16 Then
					Echo "ORDER_IS_ERROR"
				Else
					Echo GetOrderData
				End If
			Case Else
			    Echo "error"
		End Select
	End Sub
	
	Private Function GetOrderData()
		Dim Fn_Rs,Fn_TempString,Fn_CartGoodsString,Fn_TheSkusString,Fn_ThePacksString
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Order,Array("OrderID:"& Pv_OrderID),"")
		If Not(Fn_Rs.Eof) Then
			IsContentExist = True
			SysID           = Fn_Rs("SysID")
			Pv_UserID       = KnifeCMS.Data.CLng(Fn_Rs("UserID"))
			Pv_Createtime   = Fn_Rs("Createtime")
			Pv_Status       = KnifeCMS.Data.CLng(Fn_Rs("Status"))
			Pv_PayStatus    = KnifeCMS.Data.CLng(Fn_Rs("PayStatus"))
			Pv_ShipStatus   = KnifeCMS.Data.CLng(Fn_Rs("ShipStatus"))
			
			Pv_Cost_Item    = KnifeCMS.Data.FormatCurrency(Fn_Rs("Cost_Item"))    '商品总金额
			Pv_Cost_Freight = KnifeCMS.Data.FormatCurrency(Fn_Rs("Cost_Freight")) '配送费用
			Pv_Cost_Protect = KnifeCMS.Data.FormatCurrency(Fn_Rs("Cost_Protect")) '保价费
			Pv_Cost_Tax     = KnifeCMS.Data.FormatCurrency(Fn_Rs("Cost_Tax"))     '税金
			Pv_Pmt_Amount   = KnifeCMS.Data.FormatCurrency(Fn_Rs("Pmt_Amount"))   '促销优惠金额
			Pv_Cost_Payment = KnifeCMS.Data.FormatCurrency(Fn_Rs("Cost_Payment")) '支付手续费
			Pv_Discount     = KnifeCMS.Data.FormatCurrency(KnifeCMS.Data.FormatDouble(Fn_Rs("Discount")))       '订单折扣或涨价
			Pv_Total_Amount = KnifeCMS.Data.FormatCurrency(KnifeCMS.Data.FormatDouble(Fn_Rs("Total_Amount")))   '订单总金额(订单本来的总金额=商品总金额+配送费用+物流保价费+税金-促销优惠金额+订单折扣或涨价)
			Pv_Final_Amount = KnifeCMS.Data.FormatCurrency(KnifeCMS.Data.FormatDouble(Fn_Rs("Final_Amount")))   '订单总金额(订单最终的总金额=商品总金额+配送费用+物流保价费+税金-促销优惠金额+订单折扣或涨价+支付手续费)
			Pv_Payed        = KnifeCMS.Data.FormatDouble(Fn_Rs("Payed"))
			Pv_IsTax        = KnifeCMS.Data.CLng(Fn_Rs("IsTax"))
			Pv_Tax_Company  = KnifeCMS.Data.Escape(KnifeCMS.Data.HTMLDecode(Fn_Rs("Tax_Company")))      '发票抬头
			Pv_Weight       = Fn_Rs("Weight")       '总重量
			
			Pv_ShippingID   = Fn_Rs("ShippingID")   '配送方式ID
			Pv_PaymentID    = Fn_Rs("PaymentID")    '付款方式ID
			
			Pv_Shipping     = KnifeCMS.Data.Escape(Fn_Rs("Shipping"))   '配送方式
			Pv_Payment      = KnifeCMS.Data.Escape(Fn_Rs("Payment"))    '付款方式
			
			Pv_Currency     = Fn_Rs("Currency")     '货币
			Pv_ShippingArea = KnifeCMS.Data.Escape(Fn_Rs("ShippingArea")) '配送地区(广东-广州市-天河区)
			Pv_Ship_Name    = KnifeCMS.Data.Escape(Fn_Rs("Ship_Name"))    '收货人姓名
			Pv_Ship_Email   = KnifeCMS.Data.Escape(Fn_Rs("Ship_Email"))   '收货人Email
			Pv_Ship_Area    = KnifeCMS.Data.Escape(Fn_Rs("Ship_Area"))    '收货地区(,423,424,429,)
			Pv_Ship_Address = KnifeCMS.Data.Escape(KnifeCMS.Data.HTMLDecode(Fn_Rs("Ship_Address"))) '详细地址
			Pv_Ship_Zip     = KnifeCMS.Data.Escape(Fn_Rs("Ship_Zip"))     '邮政编码
			Pv_Ship_Tel     = KnifeCMS.Data.Escape(Fn_Rs("Ship_Tel"))     '联系电话
			Pv_Ship_Mobile  = KnifeCMS.Data.Escape(Fn_Rs("Ship_Mobile"))  '联系手机
			Pv_Ship_Time    = KnifeCMS.Data.Escape(Fn_Rs("Ship_Time"))    '送货日期-时间段
			Pv_Mark_Text    = KnifeCMS.Data.Escape(KnifeCMS.Data.HTMLDecode(Fn_Rs("Mark_Text")))    '订单附言
		Else
			IsContentExist = False
		End IF
		KnifeCMS.DB.CloseRs Fn_Rs
		If IsContentExist Then
			If Pv_UserID<>UserID Then
				Fn_TempString = "THIS_ORDER_IS_LIMIT_TO_YOU"
			Else
				Fn_TheSkusString  = ""
				Fn_ThePacksString = ""
				'获取订单商品信息
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_OrderGoods &":Config,PacksID",Array("OrderID:"& Pv_OrderID),"")
				Do While Not(Fn_Rs.Eof)
					If KnifeCMS.Data.CLng(Fn_Rs(1)) > 0 Then
						If KnifeCMS.Data.IsNul(Fn_ThePacksString) Then Fn_ThePacksString = Fn_Rs(0) : Else Fn_ThePacksString = Fn_ThePacksString &","& Fn_Rs(0)
					Else
						If KnifeCMS.Data.IsNul(Fn_TheSkusString) Then Fn_TheSkusString = Fn_Rs(0) : Else Fn_TheSkusString = Fn_TheSkusString &","& Fn_Rs(0)
					End If
					Fn_Rs.movenext
				Loop
				KnifeCMS.DB.CloseRs Fn_Rs
				
				Fn_CartGoodsString = "{""theskus"":["& Fn_TheSkusString &"],""thepacks"":["& Fn_ThePacksString &"],""weight"":"""& Pv_Weight &""",""amount"":"""& Pv_Cost_Item &"""}"
				
				Fn_TempString   = "{""orderid"":"""& Pv_OrderID &""",""userid"":"""& Pv_UserID &""",""createtime"":"""& Pv_Createtime &""",""status"":"""& Pv_Status &""",""shipping"":"""& Pv_Shipping &""",""payment"":"""& Pv_Payment &""",""cost_item"":"""& Pv_Cost_Item &""",""cost_freight"":"""& Pv_Cost_Freight &""",""cost_protect"":"""& Pv_Cost_Protect &""",""cost_tax"":"""& Pv_Cost_Tax &""",""total_amount"":"""& Pv_Total_Amount &""",""final_amount"":"""& Pv_Final_Amount &""",""weight"":"""& Pv_Weight &""",""istax"":"""& Pv_IsTax &""",""tax_company"":"""& Pv_Tax_Company &""",""shippingarea"":"""& Pv_ShippingArea &""",""ship_name"":"""& Pv_Ship_Name &""",""ship_email"":"""& Pv_Ship_Email &""",""ship_area"":"""& Pv_Ship_Area &""",""ship_address"":"""& Pv_Ship_Address &""",""ship_zip"":"""& Pv_Ship_Zip &""",""ship_tel"":"""& Pv_Ship_Tel &""",""ship_mobile"":"""& Pv_Ship_Mobile &""",""ship_time"":"""& Pv_Ship_Time &""",""mark_text"":"""& Pv_Mark_Text &""",""cartgoods"":["& Fn_CartGoodsString &"]}"
			End If
		Else
			Fn_TempString = "ORDER_IS_NOT_EXIST"
		End If
		GetOrderData = Fn_TempString
	End Function
	
	
	
End Class
%>