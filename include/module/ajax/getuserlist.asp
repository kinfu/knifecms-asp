<!--#include file="../../../config/config.main.asp"-->
<!--#include file="../../kfc.main.asp"-->
<!--#include file="../../kfc.common.asp"-->
<!--#include file="../../kfc.initialization.asp"-->
<%
Dim AjaxClass
Set AjaxClass = New Class_KnifeCMS_AjaxClass
Set AjaxClass = Nothing
Set OS        = Nothing

Class Class_KnifeCMS_AjaxClass
	Private Pv_ID,Pv_Username,Pv_HeadImg
	Private Sub Class_Initialize()
		IF KnifeCMS.CheckPost Then Init : Else Echo "ERROR"
	End Sub
	Private Sub Class_Terminate()
	End Sub
	Private Sub Init()
		If Logined<>1 Then
			Echo "NOT_LOGIN"
		Else
			Select Case Ctl
				Case "relateduser"
					Echo GetUserlist()
				Case Else
					Echo "ERROR"
			End Select
		End If
	End Sub
	Private Function GetUserlist()
		Dim Fn_Rs,Fn_ReturnString,Fn_TempString
		Select Case DB_Type
		Case 0
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Members &":24:ID,Username,HeadImg",Array("Disable:0","Recycle:0"),"ORDER BY Rnd(ID)")
		Case 1
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Members &":24:ID,Username,HeadImg",Array("Disable:0","Recycle:0"),"ORDER BY NEWID()")
		End Select
		Do While Not(Fn_Rs.Eof)
			Pv_ID       = Fn_Rs(0)
			Pv_Username = KnifeCMS.Data.Escape(Fn_Rs(1))
			Pv_HeadImg  = Fn_Rs(2)
			Fn_TempString = "{""id"":"""& Pv_ID &""",""username"":"""& Pv_Username &""",""headimg"":"""& Pv_HeadImg &"""}"
			If Fn_ReturnString = "" Then
				Fn_ReturnString = Fn_TempString
			Else
				Fn_ReturnString = Fn_ReturnString &","& Fn_TempString
			End If
			Fn_Rs.movenext
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		GetUserlist= "{""user"":["& Fn_ReturnString &"]}"
	End Function
End Class
%>