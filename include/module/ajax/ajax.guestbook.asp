<!--#include file="../../../config/config.main.asp"-->
<!--#include file="../../kfc.main.asp"-->
<!--#include file="../../kfc.common.asp"-->
<!--#include file="../../kfc.initialization.asp"-->
<%
Dim AjaxGuestbook
Set AjaxGuestbook = New Class_KnifeCMS_Ajax_Guestbook
Set AjaxGuestbook = Nothing
Set OS            = Nothing
Class Class_KnifeCMS_Ajax_Guestbook
	Private Pv_Rs
	Private S_TempHTML
	
	Private Sub Class_Initialize()
		'防止站外提交注入
		If KnifeCMS.CheckPost Then Init : Else Echo "error"
	End Sub
	Private Sub Class_Terminate()
	End Sub
	
	Private Sub Init()
		If Ctl="guestbook" Then
			Select Case Action
				Case "post"
					Echo PostGuestbook()
				Case "get"
					Echo GetGuestbook()
				Case Else
					Echo "error"
			End Select
		Else
			Echo "error"
		End If
	End Sub
	
	Private Function PostGuestbook()
		If Not(OS.ValidCheckCode) Then PostGuestbook="error_checkcode" : Exit Function
		
		Dim Fn_Rs,Fn_Result,Fn_IP
		Dim Fn_Content,Fn_UserID,Fn_Username,Fn_Email,Fn_Tel,Fn_Website
		
		Fn_Content  = KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(KnifeCMS.GetForm("post","content")),500)
		If KnifeCMS.Data.IsNul(Fn_Content) Then PostGuestbook = "error_content_is_null" : Exit Function
		
		Fn_Username = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","username")),50)
		Fn_Email    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","email")),50)
		Fn_Tel      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","tel")),50)
		Fn_Website  = KnifeCMS.Data.Left(KnifeCMS.Data.URLEncode(KnifeCMS.GetForm("post","website")),250)
		
		If Logined=1 Then
			Fn_UserID = UserID
			If KnifeCMS.Data.IsNul(Fn_Username) Then Fn_Username = Username
			If KnifeCMS.Data.IsNul(Fn_Email)    Then Fn_Email    = UserEmail
		Else
			Fn_UserID = 0
		End If
		
		SysID = KnifeCMS.CreateSysID
		Fn_IP = KnifeCMS.GetClientIP()
		
		Fn_Result = KnifeCMS.DB.AddRecord(DBTable_Guestbook,Array("SysID:"& SysID,"P_ID:0","ChildNum:0","Content:"& Fn_Content,"UserID:"& Fn_UserID,"Username:"& Fn_Username,"Tel:"& Fn_Tel,"Email:"& Fn_Email,"Website:"& Fn_Website,"AddTime:"& SysTime,"IP:"& Fn_IP,"Disabled:1"))
		If Fn_Result Then
			PostGuestbook = "ok"
		Else
			PostGuestbook = "error"
		End If
		
	End Function
	
	Private Function GetGuestbook()
	
		Dim Fn_ReturnString,Fn_DBTable,Fn_Sql,Fn_Rs,Fn_ID
		
		Dim Fn_Template,Fn_TempString,Fn_String,Fn_ReplyString,Fn_UserID,Fn_Username,Fn_HeadImg,Fn_IP,Fn_Website,Fn_HeadLinkURL,Fn_NameLinkURL,Fn_UserNameLink
		Fn_Template = "<div class=""guestbookitem"">"
        Fn_Template = Fn_Template &"<div class=""author"">{tpl:userheadlink}</div>"
        Fn_Template = Fn_Template &"<div class=""ctbody"">"
		Fn_Template = Fn_Template &"<div class=""uinfo""><span class=""guestbookid"">#{tpl:id}</span><span class=""name"">{tpl:usernamelink}</span><span class=""posttime"">{tpl:posttime}</span></div>"
        Fn_Template = Fn_Template &"<div class=""cttext"">{tpl:content}</div>{tpl:replys}</div></div>"
		
		Fn_Sql="SELECT a.ID,a.ChildNum,a.Content,a.UserID,a.Username,b.Username,b.HeadImg,a.Website,a.AddTime FROM ["& DBTable_Guestbook &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.UserID) WHERE a.P_ID=0 AND a.Disabled=0 Order By a.ID DESC"
		Dim Fn_PageSize,Fn_Page,Fn_PageString,Fn_CurrentUrl
		Fn_PageSize = 20
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
		If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then
			Dim Fn_Temp_i,Fn_Temp_j,Fn_Temp_k,Fn_a,Fn_RsCount,Fn_PageCount,Fn_DataArray
			Fn_RsCount=Fn_Rs.RecordCount : If Fn_Rs.RecordCount<1 then Fn_RsCount=0
			Fn_Rs.PageSize = Fn_PageSize
			Fn_PageCount = Fn_Rs.PageCount
			Fn_Page = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","page")) : Fn_Page = KnifeCMS.IIF(Not(Fn_Page>0),1,Fn_Page)
			'Die KnifeCMS.URLQueryString
			if Fn_Page > Fn_PageCount then
				Fn_Rs.AbsolutePage = Fn_PageCount
			else
				Fn_Rs.AbsolutePage = Fn_Page
			end if
			Fn_Page = Fn_Rs.AbsolutePage
			ReDim DataArray(Fn_Rs.Fields.Count,Fn_PageSize)
			
			For Fn_Temp_i=0 to Fn_PageSize-1
				If Fn_Rs.EOF Then Exit For
				Fn_UserID    = KnifeCMS.Data.CLng(Fn_Rs(3))
				Fn_Username  = ""
				If Fn_UserID>0 Then
					Fn_Username  = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_Rs(4)),Fn_Rs(6),Fn_Rs(4))
				Else
					Fn_Username  = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_Rs(4)),Lang_Guest,Fn_Rs(4))
				End If
				Fn_HeadImg    = KnifeCMS.Data.URLDecode(Fn_Rs(6))
				Fn_HeadImg    = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_HeadImg),TemplatePath &"images/userhead_s.jpg",Fn_HeadImg)
				Fn_Website    = KnifeCMS.Data.URLDecode(Fn_Rs(7))
				Fn_HeadLinkURL= "?user/"& Fn_UserID & FileSuffix
				If Not(KnifeCMS.Data.IsNul(Fn_Website)) Then
					Fn_NameLinkURL  = Fn_Website
					if KnifeCMS.Data.Left(Fn_NameLinkURL,7)<>"http://" Then Fn_NameLinkURL = "http://"& Fn_NameLinkURL
					Fn_UserNameLink = "<a href="""& Fn_NameLinkURL &""" target=""_blank"" >"& Fn_Username &"</a>"
				Else
					Fn_NameLinkURL  = Fn_HeadLinkURL
					If Fn_UserID>0 Then
						Fn_UserNameLink = "<a href="""& Fn_NameLinkURL &""" target=""_blank"" >"& Fn_Username &"</a>"
					Else
						Fn_UserNameLink = Fn_Username
					End If
				End If
				
				
				Fn_TempString = Fn_Template
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:id}",Fn_Rs(0))
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:headimg}",Fn_HeadImg)
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:userheadlink}",KnifeCMS.IIF(Fn_UserID>0,"<a href="""& Fn_HeadLinkURL &""" target=""_blank"" ><img src="""& Fn_HeadImg &""" /></a>","<img src="""& Fn_HeadImg &""" />"))
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:usernamelink}",Fn_UserNameLink)
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:content}",Fn_Rs(2))
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:posttime}",Fn_Rs(8))
				
				Fn_ReplyString = ""
				If KnifeCMS.Data.CLng(Fn_Rs("ChildNum"))>0 Then Fn_ReplyString = GetReplys(Fn_Rs(0))
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:replys}",Fn_ReplyString)
				
				Fn_String = Fn_String & Fn_TempString
				Fn_Rs.movenext
			Next
			Fn_PageString = "<div class=""mt5 mb5""><div class=pagestyle><span class=pages>"& Replace(Lang_Pages(0),"{tpl:num}",Fn_RsCount) &"</span>"
			If Fn_Page>1 Then Fn_PageString = Fn_PageString &"<a href=""javascript:KnifeCMS.C.Guestbook.Page("& Fn_Page-1 &")"">"& Lang_Pages(1) &"</a>"
			if Fn_Page>1 then Fn_a=1 : if Fn_Page>2 then Fn_a=2 : if Fn_Page>3 then Fn_a=3 : if Fn_Page>4 then Fn_a=4 : if Fn_Page>5 then Fn_a=5 : if Fn_Page>6 then Fn_a=6 : if Fn_Page>7 then Fn_a=7 : if Fn_Page>8 then Fn_a=8 : if Fn_Page>9 then Fn_a=9
			for Fn_Temp_i=Fn_Page-Fn_a to Fn_Page-1
				Fn_PageString = Fn_PageString & "<a href=""javascript:KnifeCMS.C.Guestbook.Page("&Fn_Temp_i&")"">"&Fn_Temp_i&"</a>"
			next
			Fn_PageString = Fn_PageString &"<span class=current>"&Fn_Page&"</span>"
			
			for Fn_Temp_i=Fn_Page+1 to Fn_Page+9
				if Fn_Temp_i > Fn_PageCount then Exit for
				Fn_PageString = Fn_PageString & "<a href=""javascript:KnifeCMS.C.Guestbook.Page("&Fn_Temp_i&")"">"&Fn_Temp_i&"</a>"
			next
			
			if Not(Fn_Page>=Fn_PageCount) then Fn_PageString = Fn_PageString & "<a href=""javascript:KnifeCMS.C.Guestbook.Page("& Fn_Page+1 &")"">"& Lang_Pages(2) &"</a>"
			Fn_PageString = Fn_PageString & "<span class=pages>"& Replace(Lang_Pages(3),"{tpl:num}",Fn_PageCount) &"</span><span class=pages>"& Replace(Lang_Pages(4),"{tpl:num}",Fn_PageSize) &"</span></div></div>"
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		Fn_ReturnString = Fn_PageString & Fn_String & Fn_PageString
		
		If Fn_ReturnString = "" Then
			GetGuestbook = "null"
		Else
			GetGuestbook = Fn_ReturnString
		End If
	End Function
	
	Private Function GetReplys(ByVal BV_ID)
		Dim Fn_Rs,Fn_Sql
		Dim Fn_Template,Fn_TempString,Fn_String
		
		Fn_Template = "<div class=""guestbookitemreply"" gid=""{tpl:id}"">"
        Fn_Template = Fn_Template &"<div class=""uinfo""><span class=""name"">"& Lang_Admin &"</span><span class=""rep"">"& Lang_ReplyIn &"</span><span class=""posttime"">{tpl:posttime}</span></div>"
        Fn_Template = Fn_Template &"<div class=""cttext"">{tpl:content}</div>"
		Fn_Template = Fn_Template &"</div>"
		
		Fn_Sql="SELECT a.ID,a.Content,a.AddTime FROM ["& DBTable_Guestbook &"] a WHERE a.P_ID="& BV_ID &" AND a.Disabled=0 ORDER BY a.ID ASC"
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
		Do While Not(Fn_Rs.Eof)
			Fn_TempString = Fn_Template
			Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:id}",Fn_Rs(0))
			Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:content}",Fn_Rs(1))
			Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:posttime}",Fn_Rs(2))
			Fn_String     = Fn_String & Fn_TempString
			Fn_Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		GetReplys = Fn_String
	End Function
	
	
End Class
%>