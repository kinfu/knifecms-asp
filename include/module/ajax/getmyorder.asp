<!--#include file="../../../config/config.main.asp"-->
<!--#include file="../../kfc.main.asp"-->
<!--#include file="../../kfc.common.asp"-->
<!--#include file="../../kfc.initialization.asp"-->
<%
Dim AjaxClass
Set AjaxClass = New Class_KnifeCMS_GetMyOrder
Set AjaxClass = Nothing
Set OS        = Nothing

Class Class_KnifeCMS_GetMyOrder
	Private Pv_OrderID,Pv_Createtime,Pv_Final_Amount,Pv_Payed
	Private Pv_Status,Pv_PayStatus,Pv_ShipStatus
	Private Sub Class_Initialize()
		IF KnifeCMS.CheckPost Then Init : Else Echo "ERROR"
	End Sub
	Private Sub Class_Terminate()
	End Sub
	Private Sub Init()
		If Logined=1 Then
			Echo GetMyOrderData()
		Else
			Echo "NOT_LOGIN"
		End If
	End Sub
	Private Function GetMyOrderData()
		Dim Fn_Rs,Fn_ReturnString,Fn_TempString
		Dim Fn_PageSize,Fn_Page,Fn_PageString,Fn_CurrentUrl
		Fn_PageSize = 20
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Order &":OrderID,Createtime,Status,PayStatus,ShipStatus,Final_Amount,Payed",Array("UserID:"& UserID,"Recycle:0"),"ORDER BY ID DESC")
		If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then
			Dim Fn_Temp_i,Fn_Temp_j,Fn_Temp_k,Fn_a,Fn_RsCount,Fn_PageCount,Fn_DataArray
			Fn_RsCount=Fn_Rs.RecordCount : If Fn_Rs.RecordCount<1 then Fn_RsCount=0
			Fn_Rs.PageSize = Fn_PageSize
			Fn_PageCount = Fn_Rs.PageCount
			Fn_Page = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","page")) : Fn_Page = KnifeCMS.IIF(Not(Fn_Page>0),1,Fn_Page)
			'Die KnifeCMS.URLQueryString
			if Fn_Page > Fn_PageCount then
				Fn_Rs.AbsolutePage = Fn_PageCount
			else
				Fn_Rs.AbsolutePage = Fn_Page
			end if
			Fn_Page = Fn_Rs.AbsolutePage
			ReDim DataArray(Fn_Rs.Fields.Count,Fn_PageSize)
			For Fn_Temp_i=0 to Fn_PageSize-1
				If Fn_Rs.EOF Then Exit For
				Pv_OrderID      = Fn_Rs("OrderID")
				Pv_Createtime   = Fn_Rs("Createtime")
				Pv_Status       = KnifeCMS.Data.CLng(Fn_Rs("Status"))
				Pv_PayStatus    = KnifeCMS.Data.CLng(Fn_Rs("PayStatus"))
				Pv_ShipStatus   = KnifeCMS.Data.CLng(Fn_Rs("ShipStatus"))
				Pv_Final_Amount = KnifeCMS.Data.FormatCurrency(KnifeCMS.Data.FormatDouble(Fn_Rs("Final_Amount")))
				Pv_Payed        = KnifeCMS.Data.FormatCurrency(KnifeCMS.Data.FormatDouble(Fn_Rs("Payed")))
				Fn_TempString = "{""orderid"":"""& Pv_OrderID &""",""createtime"":"""& Pv_Createtime &""",""status"":"""& Pv_Status &""",""paystatus"":"""& Pv_PayStatus &""",""shipstatus"":"""& Pv_ShipStatus &""",""finalamount"":"""& Pv_Final_Amount &""",""payed"":"""& Pv_Payed &"""}"
				If Fn_ReturnString = "" Then
					Fn_ReturnString = Fn_TempString
				Else
					Fn_ReturnString = Fn_ReturnString &","& Fn_TempString
				End If
				Fn_Rs.movenext
			Next
			Fn_PageString = "<div class=pagestyle><span class=pages>"& Replace(Lang_Pages(0),"{tpl:num}",Fn_RsCount) &"</span>"
			If Fn_Page>1 Then Fn_PageString = Fn_PageString &"<a href=""javascript:AjaxGetMyOrder("&Fn_Page-1&")"">"& Lang_Pages(1) &"</a>"
			if Fn_Page>1 then Fn_a=1 : if Fn_Page>2 then Fn_a=2 : if Fn_Page>3 then Fn_a=3 : if Fn_Page>4 then Fn_a=4 : if Fn_Page>5 then Fn_a=5 : if Fn_Page>6 then Fn_a=6 : if Fn_Page>7 then Fn_a=7 : if Fn_Page>8 then Fn_a=8 : if Fn_Page>9 then Fn_a=9
			for Fn_Temp_i=Fn_Page-Fn_a to Fn_Page-1
				Fn_PageString = Fn_PageString & "<a href=""javascript:AjaxGetMyOrder("&Fn_Temp_i&")"">"&Fn_Temp_i&"</a>"
			next
			Fn_PageString = Fn_PageString &"<span class=current>"&Fn_Page&"</span>"
			
			for Fn_Temp_i=Fn_Page+1 to Fn_Page+9
				if Fn_Temp_i > Fn_PageCount then Exit for
				Fn_PageString = Fn_PageString & "<a href=""javascript:AjaxGetMyOrder("&Fn_Temp_i&")"">"&Fn_Temp_i&"</a>"
			next
			
			if Not(Fn_Page>=Fn_PageCount) then Fn_PageString = Fn_PageString & "<a href=""javascript:AjaxGetMyOrder("&Fn_Page+1&")"">"& Lang_Pages(2) &"</a>"
			Fn_PageString = Fn_PageString &"<span class=pages>"& Replace(Lang_Pages(3),"{tpl:num}",Fn_PageCount) &"</span><span class=pages>"& Replace(Lang_Pages(4),"{tpl:num}",Fn_PageSize) &"</span></div>"
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		GetMyOrderData = "{""orderdata"":["& Fn_ReturnString &"],""pagestring"":"""& KnifeCMS.Data.Escape(Fn_PageString) &"""}"
	End Function
End Class
%>