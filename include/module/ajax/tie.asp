<!--#include file="../../../config/config.main.asp"-->
<!--#include file="../../kfc.main.asp"-->
<!--#include file="../../kfc.common.asp"-->
<!--#include file="../../kfc.initialization.asp"-->
<%
Dim AjaxClass
Set AjaxClass = New Class_KnifeCMS_Tie
Set AjaxClass = Nothing
Set OS        = Nothing

Class Class_KnifeCMS_Tie

    Private Pv_Rs,Pv_Array
	Private S_TempHTML,S_TieDepth
	
	Private Sub Class_Initialize()
		'防止站外提交注入
		If KnifeCMS.CheckPost Then Init : Else Echo "error"
	End Sub
	Private Sub Class_Terminate()
	End Sub
	Private Sub Init()
		S_TieDepth = 4 '评论允许最大级数
		Select Case Ctl
			Case "gettie"
				Echo GetTie
			Case "posttie"
				Echo PostTie
			Case Else
			    Echo "error"
		End Select
	End Sub
	Public Property Let Depth(ByVal BV_Depth)
		S_TieDepth = LCase(BV_Depth)
	End Property
	Public Property Get Depth()
		Depth = S_TieDepth
	End Property
	
	Public Function GetTie()
		Dim Fn_ReturnString,Fn_DBTable,Fn_Sql,Fn_Rs,Fn_ID
		If Not(ID>0) Then GetTie="error" : Exit Function
		Select Case SubSystem
			Case "goodscomment"
				Fn_DBTable = DBTable_GoodsComment
				Fn_ID = "goodscommenttie_ctbody_{tpl:id}"
				S_TieDepth = 2
			Case "goodsconsult"
				Fn_DBTable = DBTable_GoodsConsult
				Fn_ID = "goodsconsulttie_ctbody_{tpl:id}"
				S_TieDepth = 1
			Case Else
				Fn_DBTable = TablePre & "Content_"& SubSystem &"_Comment"
				If Not(IsContentSubSystemExist(SubSystem)) Then GetTie="error" : Exit Function
				Fn_ID = "tie_ctbody_{tpl:id}"
		End Select
		Dim Fn_TieTemplate,Fn_TempString,Fn_TieString,Fn_TieReplyString,Fn_UserID,Fn_Username,Fn_HeadImg,Fn_IP
		Fn_TieTemplate = "<div class=""reply"">"
        Fn_TieTemplate = Fn_TieTemplate &"<div class=""author""><a href=""{tpl:userhomeurl}"" title=""{tpl:ip}"" target=""_blank""><img src=""{tpl:headimg}"" /></a></div>"
        Fn_TieTemplate = Fn_TieTemplate &"<div class=""ctbody"" id="& Fn_ID &">"
        Fn_TieTemplate = Fn_TieTemplate &"<p class=""blue_a uinfo""><span class=""fr do"">{tpl:doreply}</span><span class=""name""><a href=""{tpl:userhomeurl}"" target=""_blank"" >{tpl:username}</a></span><span class=""postTime"">{tpl:posttime}</span></p>"
        Fn_TieTemplate = Fn_TieTemplate &"<div class=""cttext"">{tpl:content}</div>"
		Fn_TieTemplate = Fn_TieTemplate &"{tpl:replys}</div></div>"
		
		
		Fn_Sql="SELECT a.ID,a.P_ID,a.Path,a.ChildNum,a.Content,a.UserID,b.Username,b.HeadImg,a.AddTime,a.UpdateTime,a.IP,a.Depth FROM ["& Fn_DBTable &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.UserID) WHERE a.ContentID="&ID&" AND a.Depth=1 AND a.Disabled=0 Order By a.UpdateTime DESC"
		Dim Fn_PageSize,Fn_Page,Fn_PageString,Fn_CurrentUrl
		Fn_PageSize = 10
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
		If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then
			Dim Fn_Temp_i,Fn_Temp_j,Fn_Temp_k,Fn_a,Fn_RsCount,Fn_PageCount,Fn_DataArray
			Fn_RsCount=Fn_Rs.RecordCount : If Fn_Rs.RecordCount<1 then Fn_RsCount=0
			Fn_Rs.PageSize = Fn_PageSize
			Fn_PageCount = Fn_Rs.PageCount
			Fn_Page = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","page")) : Fn_Page = KnifeCMS.IIF(Not(Fn_Page>0),1,Fn_Page)
			'Die KnifeCMS.URLQueryString
			if Fn_Page > Fn_PageCount then
				Fn_Rs.AbsolutePage = Fn_PageCount
			else
				Fn_Rs.AbsolutePage = Fn_Page
			end if
			Fn_Page = Fn_Rs.AbsolutePage
			ReDim DataArray(Fn_Rs.Fields.Count,Fn_PageSize)
			
			For Fn_Temp_i=0 to Fn_PageSize-1
				If Fn_Rs.EOF Then Exit For
				Fn_UserID    = Fn_Rs(5)
				Fn_Username  = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_Rs(6)),Lang_Guest,Fn_Rs(6))
				Fn_HeadImg   = KnifeCMS.Data.URLDecode(Fn_Rs(7))
				Fn_HeadImg   = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_HeadImg),TemplatePath &"images/userhead_s.jpg",Fn_HeadImg)
				Fn_IP        = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_Rs(10)),"",Fn_Rs(10))
				Fn_TempString = Fn_TieTemplate
				Fn_TieReplyString = ""
				If Fn_Rs("Depth")+1 > S_TieDepth Then
					Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:doreply}","")
				Else
					Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:doreply}","<a href=""javascript:;"" onclick=""KnifeCMS.C.Tie.Reply(this,'"&SubSystem&"','"&ID&"','{tpl:id}');"">"& Lang_Reply &"</a>")
				End If
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:id}",Fn_Rs(0))
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:headimg}",Fn_HeadImg)
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:username}",Fn_Username)
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:userhomeurl}","?user/"& Fn_UserID & FileSuffix)
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:content}",Fn_Rs(4))
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:posttime}",Fn_Rs(9))
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:ip}",Fn_IP)
				If Fn_Rs("ChildNum") > 0 Then Fn_TieReplyString = GetTieReply(Fn_DBTable,ID,Fn_Rs(0))
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:replys}",Fn_TieReplyString)
				Fn_TieString = Fn_TieString & Fn_TempString
				Fn_Rs.movenext
			Next
			Fn_PageString = "<div class=""mt5 mb5""><div class=pagestyle><span class=pages>"& Replace(Lang_Pages(0),"{tpl:num}",Fn_RsCount) &"</span>"
			If Fn_Page>1 Then Fn_PageString = Fn_PageString &"<a href=""javascript:KnifeCMS.C.Tie.Page('"& SubSystem &"','"&ID&"',"&Fn_Page-1&")"">"& Lang_Pages(1) &"</a>"
			if Fn_Page>1 then Fn_a=1 : if Fn_Page>2 then Fn_a=2 : if Fn_Page>3 then Fn_a=3 : if Fn_Page>4 then Fn_a=4 : if Fn_Page>5 then Fn_a=5 : if Fn_Page>6 then Fn_a=6 : if Fn_Page>7 then Fn_a=7 : if Fn_Page>8 then Fn_a=8 : if Fn_Page>9 then Fn_a=9
			for Fn_Temp_i=Fn_Page-Fn_a to Fn_Page-1
				Fn_PageString = Fn_PageString & "<a href=""javascript:KnifeCMS.C.Tie.Page('"& SubSystem &"','"&ID&"',"&Fn_Temp_i&")"">"&Fn_Temp_i&"</a>"
			next
			Fn_PageString = Fn_PageString &"<span class=current>"&Fn_Page&"</span>"
			
			for Fn_Temp_i=Fn_Page+1 to Fn_Page+9
				if Fn_Temp_i > Fn_PageCount then Exit for
				Fn_PageString = Fn_PageString & "<a href=""javascript:KnifeCMS.C.Tie.Page('"& SubSystem &"','"&ID&"',"&Fn_Temp_i&")"">"&Fn_Temp_i&"</a>"
			next
			
			if Not(Fn_Page>=Fn_PageCount) then Fn_PageString = Fn_PageString & "<a href=""javascript:KnifeCMS.C.Tie.Page('"& SubSystem &"','"&ID&"',"&Fn_Page+1&")"">"& Lang_Pages(2) &"</a>"
			Fn_PageString = Fn_PageString & "<span class=pages>"& Replace(Lang_Pages(3),"{tpl:num}",Fn_PageCount) &"</span><span class=pages>"& Replace(Lang_Pages(4),"{tpl:num}",Fn_PageSize) &"</span></div></div>"
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		Fn_ReturnString = Fn_PageString & Fn_TieString & Fn_PageString
		If Fn_ReturnString = "" Then
			GetTie = "NULL"
		Else
			GetTie = Fn_ReturnString
		End If
	End Function
	
	Public Function GetTieReply(ByVal BV_DBTable,ByVal ContentID,ByVal TieID)
		Dim Fn_Rs,Fn_Sql
		Dim Fn_ReturnString
		Dim Fn_TieTemplate,Fn_TempString,Fn_TieString,Fn_TieReplyString,Fn_UserID,Fn_Username,Fn_HeadImg,Fn_IP,Fn_ID
		Dim Fn_SysUserID,Fn_GroupID,Fn_GroupName
		Select Case SubSystem
			Case "goodscomment"
				S_TieDepth = 2
				Fn_ID = "goodscommenttie_ctbody_{tpl:id}"
			Case "goodsconsult"
				S_TieDepth = 1
				Fn_ID = "goodsconsulttie_ctbody_{tpl:id}"
			Case Else
				Fn_ID = "tie_ctbody_{tpl:id}"
		End Select
		Fn_TieTemplate = "<div class=""reply rep"">"
        Fn_TieTemplate = Fn_TieTemplate &"<div class=""author""><a href=""{tpl:userhomeurl}"" title=""{tpl:ip}"" target=""_blank""><img src=""{tpl:headimg}"" /></a></div>"
        Fn_TieTemplate = Fn_TieTemplate &"<div class=""ctbody"" id="& Fn_ID &">"
        Fn_TieTemplate = Fn_TieTemplate &"<p class=""blue_a uinfo""><span class=""fr do"">{tpl:doreply}</span><span class=""name"">{tpl:groupname} <a href=""{tpl:userhomeurl}"" target=""_blank"" >{tpl:username}</a></span><span class=""postTime"">{tpl:posttime}</span></p>"
        Fn_TieTemplate = Fn_TieTemplate &"<div class=""cttext"">{tpl:content}</div>"
		Fn_TieTemplate = Fn_TieTemplate &"{tpl:replys}</div></div>"
		Fn_Sql="SELECT TOP 100 a.ID,a.P_ID,a.Path,a.ChildNum,a.Content,a.UserID,b.Username,b.HeadImg,a.AddTime,a.UpdateTime,a.IP,a.Depth,b.SysUserID FROM ["& BV_DBTable &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.UserID) WHERE a.ContentID="& ContentID &" AND a.P_ID="& TieID &" AND a.Disabled=0 ORDER BY a.ID ASC"
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
		Do While Not(Fn_Rs.Eof)
			Fn_SysUserID = Fn_Rs(12)
			Fn_GroupName  = ""
			If Fn_SysUserID>0 Then Fn_GroupID=KnifeCMS.DB.GetFieldByID(DBTable_SysUser,"GroupID",Fn_SysUserID)
			If Fn_GroupID>0 Then Fn_GroupName=KnifeCMS.DB.GetFieldByID(DBTable_SysUserGroup,"GroupName",Fn_GroupID)
			Fn_UserID    = Fn_Rs(5)
			Fn_Username  = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_Rs(6)),Lang_Guest,Fn_Rs(6))
			Fn_HeadImg   = KnifeCMS.Data.URLDecode(Fn_Rs(7))
			Fn_HeadImg   = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_HeadImg),TemplatePath &"images/userhead_s.jpg",Fn_HeadImg)
			Fn_IP        = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_Rs(10)),"",Fn_Rs(10))
			Fn_TempString = Fn_TieTemplate
			Fn_TieReplyString = ""
			If Fn_Rs("Depth")+1 > S_TieDepth Then
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:doreply}","")
			Else
				Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:doreply}","<a href=""javascript:;"" onclick=""KnifeCMS.C.Tie.Reply(this,'"&SubSystem&"','"&ID&"','{tpl:id}');"">"& Lang_Reply &"</a>")
			End If
			Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:id}",Fn_Rs(0))
			Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:headimg}",Fn_HeadImg)
			Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:groupname}",Fn_GroupName)
			Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:username}",Fn_Username)
			Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:userhomeurl}","?user/"& Fn_UserID & FileSuffix)
			Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:content}",Fn_Rs(4))
			Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:posttime}",Fn_Rs(9))
			Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:ip}",Fn_IP)
			If Fn_Rs("ChildNum") > 0 Then Fn_TieReplyString = GetTieReply(BV_DBTable,ID,Fn_Rs(0))
			Fn_TempString = KnifeCMS.Data.ReplaceString(Fn_TempString,"{tpl:replys}",Fn_TieReplyString)
			Fn_TieString = Fn_TieString & Fn_TempString
			Fn_Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		Fn_ReturnString = Fn_TieString
		GetTieReply = Fn_ReturnString
	End Function
	
	Public Function PostTie()
		'验证码
		If Not(OS.ValidCheckCode) Then PostTie="error_checkcode" : Exit Function
		
		Dim Fn_Result,Fn_IP,Fn_DBTable,Fn_DBTableContent,Fn_ID,Fn_Path,Fn_P_ID,Fn_P_Path,Fn_P_Depth,Fn_Array,Fn_Content,Fn_CommentSet
		Dim Fn_Disabled : Fn_Disabled=0
		Fn_IP      = KnifeCMS.GetClientIP()
		Fn_P_ID    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","p_id"))
		Fn_Content = KnifeCMS.Data.TextareaEncode(KnifeCMS.Data.UnEscape(KnifeCMS.GetForm("post","content")))
		Fn_Content = KnifeCMS.Data.Left(Fn_Content,1000)
		
		If KnifeCMS.Data.IsNul(Fn_IP) Or KnifeCMS.Data.IsNul(Fn_Content) Then PostTie="error" : Exit Function
		
		Select Case SubSystem
			Case "goodscomment"
				S_TieDepth = 2
				Fn_DBTable        = DBTable_GoodsComment
				Fn_DBTableContent = DBTable_Goods
				If IsGoodsCommentCheck Then Fn_Disabled = 1
			Case "goodsconsult"
				S_TieDepth = 1
				Fn_DBTable        = DBTable_GoodsConsult
				Fn_DBTableContent = DBTable_Goods
				If IsGoodsConsultCheck Then Fn_Disabled = 1
			Case Else
				Fn_DBTable        = TablePre & "Content_"& SubSystem &"_Comment"
				Fn_DBTableContent = TablePre & "Content_"& SubSystem
				If Not(IsContentSubSystemExist(SubSystem)) Then PostTie="error" : Exit Function
				If IsContentCommentCheck Then Fn_Disabled = 1
		End Select
		
		'检查ID对应的内容是否存在
		If KnifeCMS.Data.IsNul(KnifeCMS.DB.GetFieldByID(Fn_DBTableContent,"ID",ID)) Then PostTie="error" : Exit Function
		'检查评论设置(0:只允许会员评论,1:禁止评论,2:自由评论)
		Fn_CommentSet = GetContentCommentSet()
		If Fn_CommentSet=0 And ( KnifeCMS.Data.IsNul(UserID) Or KnifeCMS.Data.IsNul(Username) Or Not(Logined=1) ) Then PostTie="error" : Exit Function
		If Fn_CommentSet=1 Then PostTie="error" : Exit Function
		'If Fn_CommentSet=2 And Not(OS.ValidCheckCode) Then PostTie="error" : Exit Function
		'获取父级Path,Depth
		If Fn_P_ID > 0 Then
			Fn_Array = Get_P_PathAndDepth(Fn_DBTable,Fn_P_ID)
			Fn_P_Path = Fn_Array(0) : Fn_P_Depth = Fn_Array(1)
		End If
		If Fn_P_Depth+1 > S_TieDepth Then PostTie="error" : Exit Function
		SysID = KnifeCMS.CreateSysID
		
		Fn_Result = KnifeCMS.DB.AddRecord(Fn_DBTable,Array("SysID:"& SysID,"P_ID:"& Fn_P_ID,"Path:0" ,"Depth:"& Fn_P_Depth+1 ,"ChildNum:0","ContentID:"& ID, "Content:"& Fn_Content,"UserID:"& UserID,"Username:"& Username,"HeadImg:NULL" ,"AddTime:"& SysTime,"UpdateTime:"& SysTime,"IP:"& Fn_IP,"Disabled:"&Fn_Disabled))
		Fn_ID = KnifeCMS.DB.GetFieldBySysID(Fn_DBTable,"ID",SysID)
		Fn_Path = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_P_Path),","& Fn_ID &",",Fn_P_Path & Fn_ID &",")
		Call KnifeCMS.DB.UpdateRecord(Fn_DBTable,Array("Path:"&Fn_Path),Array("ID:"&Fn_ID))
		If Fn_P_ID > 0 Then Call KnifeCMS.DB.UpdateRecord(Fn_DBTable,Array("ChildNum:ChildNum+1","UpdateTime:"& SysTime),Array("ID:"&Fn_P_ID))
		If Fn_Result Then
			If Not(Fn_P_ID>0) Then
				'评论量加1(回复不算)
				If SubSystem<>"goodscomment" And SubSystem<>"goodsconsult" Then
					Call  KnifeCMS.DB.UpdateRecord(Fn_DBTableContent,Array("Comments:Comments+1"),Array("ID:"& ID))
				End If
			End If
			PostTie="ok"
		End If
	End Function
	
End Class
%>