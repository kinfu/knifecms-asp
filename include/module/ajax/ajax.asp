<!--#include file="../../../config/config.main.asp"-->
<!--#include file="../../kfc.main.asp"-->
<!--#include file="../../module/feeds/feeds.class.asp"-->
<!--#include file="../../module/friend/friend.class.asp"-->
<!--#include file="../../module/visitor/visitor.class.asp"-->
<!--#include file="../../module/notify/notify.class.asp"-->
<!--#include file="../../kfc.common.asp"-->
<!--#include file="../../kfc.initialization.asp"-->
<!--#include file="../../module/ajax/ajax.class.asp"-->
<!--#include file="../../../api/kfc.api.asp"-->
<%
Dim AjaxGet
Set AjaxGet = New Class_KnifeCMS_Ajax_Get
Set AjaxGet = Nothing
Class Class_KnifeCMS_Ajax_Get
	Private Pv_Rs,Pv_ContentSubSystem,Pv_TempArr,Pv_Ran
	Private S_TempHTML
	
	Private Sub Class_Initialize()
		'防止站外提交注入
		IF Not(KnifeCMS.CheckPost()) then
			Echo "error"
		Else
			Call Init()
		End If
	End Sub
	
	Private Sub Init()
		Select Case Ctl
			Case "addcontenthits"
				Echo Ajax.AddContentHits(SubSystem,ID)

			Case "logining"
				Echo Ajax.Logining
			Case "registering"
				Echo Ajax.Registering
				
			Case "checkemail"
				Echo OS.CheckEmail
			Case "checkusername"
				Echo OS.CheckUsername
				
			Case "checkislogined"
				If Logined=1 Then Echo "ok"
			Case "checkcommentset"
				Echo Ajax.CommentSet
			Case "getregion"
				Echo Ajax.GetRegion
			Case "postmood"
				Echo Ajax.PostMood
			Case "postguestbook"
				Echo Ajax.PostGuestBook
			Case "postreply"
				Echo Ajax.PostReply
			Case "repayfeeds"
				Echo Ajax.RepayFeeds
			Case "getfeeds"
				Echo Ajax.GetFeeds
			Case "feedsdelete"
				Echo Ajax.FeedsDelete
			Case "guestbookdelete"
				Echo Ajax.GuestBookDelete
				
			Case "addforfriend"
				Echo Ajax.AddForFriend
			Case "getfriendrequest"
				Echo Ajax.GetFriendRequest
			Case "friendrequestdo"
				Echo Ajax.FriendRequestDo
			Case "getmyfriends"
				Echo Ajax.GetFriends(1)
			Case "getmembersfriends"
				Echo Ajax.GetFriends(0)
			Case "frienddelete"
			    Echo Ajax.MyFriendDelete
			Case "getmygroup"
				Echo Ajax.GetMyGroup
			Case "groupadd"
			    Echo Ajax.MyGroupAdd
			Case "groupedit"
			    Echo Ajax.MyGroupEdit
			Case "groupdelete"
			
			    Echo Ajax.MyGroupDelete
			Case "getvisitor"
				Echo Ajax.GetVisitor
			Case "getbevisited"
				Echo Ajax.GetBeVisited
				
			Case "getnotify"
				Echo Ajax.GetMyNotify
			Case "notifydelete"
				Echo Ajax.NotifyDelete
			Case "myallnotifydelete"
				Echo Ajax.MyAllNotifyDelete
			Case "uploadhead"
				Echo Ajax.UploadHead()
			Case "saveinfo"
				Echo Ajax.SaveInfo()
			Case "saveaddress"
				Echo Ajax.SaveAddress()
			Case "changepassword"
				Echo Ajax.ChangePassword()
			Case "forgetpassword"
				Echo Ajax.ForGetPassWord()
			Case "resetpassword"
				Echo Ajax.ResetPassword()
			'商品
			Case "addgoodshits"
				Echo Ajax.AddGoodsHits(ID)
				
			'购物车
			Case "getcart"
				Echo Ajax.GetCart
			Case "getdeliverydata"
				Echo Ajax.GetDeliveryData
			Case "getpaymentslist"
				Echo Ajax.GetPaymentsList()
			Case "placeorder"
				Echo Ajax.PlaceOrder
			
			'投票
			Case "votedetail"
				Echo Ajax.VoteDetail
			Case "votepoll"
				Echo Ajax.VotePoll
			Case "addvotehits"
				Echo Ajax.AddVoteHits(ID)
			'友情链接
			Case "addlinkhits"
				Echo Ajax.AddLinkHits(ID)
			Case Else
			    Echo "error"
		End Select
	End Sub
	
	
	Private Sub Class_Terminate()
		Set OS = Nothing
	End Sub
End Class
%>