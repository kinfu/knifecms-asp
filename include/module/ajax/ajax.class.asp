<%
Dim Ajax
Set Ajax = New Class_Ajax
Class Class_Ajax
	
	Private Pv_Rs,Pv_Array
	Private S_TempHTML,S_CommentSet
	
	Private Sub Class_Initialize()
	End Sub
	Private Sub Class_Terminate()
	End Sub
	
	Public Function Logining()
		Logining = OS.UserLogining
	End Function
	
	Public Function Registering()
		Registering = OS.UserRegistering
	End Function

	Public Function CommentSet()
		CommentSet = GetContentCommentSet
	End Function
	
	Public Function GetRegion()
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_Rs
		Dim Fn_RegionPath  : Fn_RegionPath = KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","regionpath"),"[^0-9,]","")
		Dim Fn_P_Region_ID : Fn_P_Region_ID = KnifeCMS.Data.CLng(PickUpLastRegionID(Fn_RegionPath))
		If Not(Fn_P_Region_ID>0) Then Fn_P_Region_ID="NULL"
		Set Fn_Rs=KnifeCMS.DB.GetRecord(DBTable_Regions &":Region_Path,Local_Name,Region_Grade",Array("P_Region_ID:"& Fn_P_Region_ID),"AND Disabled=0 ORDER BY OrderNum ASC")
		Do While Not(Fn_Rs.Eof)
			Fn_ReturnString = Fn_ReturnString &","& KnifeCMS.Data.Escape(Fn_Rs("Region_Path")) &":"& KnifeCMS.Data.Escape(Fn_Rs("Local_Name")) &":"& KnifeCMS.Data.Escape(Fn_Rs("Region_Grade"))
			Fn_Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		GetRegion = Fn_ReturnString
	End Function
	
	Public Function PostMood()
		PostMood = Feeds.PostMood()
	End Function
	
	Public Function PostGuestBook()
		PostGuestBook = Feeds.PostGuestBook()
	End Function
	
	Public Function PostReply()
		PostReply = Feeds.PostReply()
	End Function
	
	Public Function RepayFeeds()
		RepayFeeds = Feeds.RepayFeeds()
	End Function
	
	Public Function GetFeeds()
		GetFeeds = Feeds.GetFeeds()
	End Function
	
	Public Function FeedsDelete()
		FeedsDelete = Feeds.FeedsDelete()
	End Function
	
	Public Function GuestBookDelete()
		GuestBookDelete = Feeds.GuestBookDelete()
	End Function
	
	Public Function AddForFriend()
	    AddForFriend = Friend.SendAddFriendNotify
	End Function
	
	Public Function GetFriendRequest()
	    GetFriendRequest = Friend.GetFriendRequest
	End Function
	Public Function FriendRequestDo()
	    FriendRequestDo = Friend.FriendRequestDo
	End Function
	Public Function GetFriends(ByVal BV_IsMy)
		GetFriends = Friend.GetFriends(BV_IsMy)
	End Function
	Public Function GetMyGroup()
		GetMyGroup = Friend.GetMyGroup()
	End Function
	Public Function MyGroupAdd()
		MyGroupAdd = Friend.MyGroupDoSave("add")
	End Function
	Public Function MyGroupEdit()
		MyGroupEdit = Friend.MyGroupDoSave("edit")
	End Function
	Public Function MyGroupDelete()
		MyGroupDelete = Friend.MyGroupDelete()
	End Function
	Public Function MyFriendDelete()
		MyFriendDelete = Friend.MyFriendDelete()
	End Function
	
	Public Function GetVisitor()
		 GetVisitor = Visitor.GetVisitor()
	End Function
	
	Public Function GetBeVisited()
		 GetBeVisited = Visitor.BeVisited()
	End Function
	
	Public Function GetMyNotify()
		 GetMyNotify = Notify.GetMyNotify()
	End Function
	Public Function NotifyDelete()
		 NotifyDelete = Notify.NotifyDelete("single")
	End Function
	Public Function MyAllNotifyDelete()
		 MyAllNotifyDelete = Notify.NotifyDelete("all")
	End Function
	
	Public Function GetCart()
		 GetCart = Cart.GetCart()
	End Function
	
	Public Function PlaceOrder()
		 PlaceOrder = Cart.PlaceOrder()
	End Function
	
	Public Function GetDeliveryData()
		GetDeliveryData = OS.GetDeliveryData()
	End Function
	
	Public Function GetPaymentsList()
		GetPaymentsList = GetCartPaymentsList(KnifeCMS.GetForm("both","orderid"))
	End Function
	
	Public Function UploadHead()
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_HeadType : Fn_HeadType = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","headtype"),"[^a-zA-Z]",""),20)
		Dim Fn_HeadUrl  : Fn_HeadUrl  = KnifeCMS.Data.HtmlTextTreat(KnifeCMS.Data.UnEscape(KnifeCMS.GetForm("post","headurl")),false,false,false)
		                  Fn_HeadUrl  = KnifeCMS.Data.Left(KnifeCMS.Data.URLEncode(Fn_HeadUrl),1024)
		If Not(Logined=1) Then
			Fn_ReturnString = "unlogined"
		Else
			If "bighead" = Fn_HeadType Then
				Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("BigHead:"&Fn_HeadUrl),Array("ID:"&UserID))
			ElseIf "smallhead" = Fn_HeadType Then
				Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("HeadImg:"&Fn_HeadUrl),Array("ID:"&UserID))
			Else
				Fn_ReturnString = "para_error"
			End If
		End If
		If Fn_Result Then Fn_ReturnString = "ok"
		UploadHead = Fn_ReturnString
	End Function
	
	Public Function SaveInfo()
		Dim Fn_Rs,Fn_Check,Fn_ID,Fn_Result,Fn_ReturnString
		Dim Fn_UserID,Fn_RealName,Fn_Gender,Fn_HomeTown,Fn_MySite,Fn_Email,Fn_Birthday,Fn_Mobile,Fn_Telephone,Fn_QQ,Fn_Height,Fn_Weight,Fn_MaritalStatus,Fn_BloodType,Fn_Religion,Fn_Smoke,Fn_Drink,Fn_FavTravel,Fn_FavLiving,Fn_FavWork,Fn_FavPartner,Fn_FavCar
		Fn_Check      = True
		Fn_UserID     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","userid"))
		If Not(1=Logined) Or Fn_UserID<>UserID Then Echo "unlogined" : Exit Function
		Fn_Email      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","email")),50)
		If Not(KnifeCMS.Data.IsEmail(Fn_Email)) Then Echo "email_error" : Exit Function
		'检查Email是否已经被别的会员使用
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Members&":ID",Array("Email:"&Fn_Email),"")
		If Not(Fn_Rs.Eof) Then
			If Fn_UserID <> Fn_Rs(0) Then Fn_Check = False
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		If Not(Fn_Check) Then Echo "email_used" : Exit Function
		
		Fn_RealName   = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","realname")),50)
		Fn_Gender     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","gender"))
		Fn_HomeTown   = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","myhometown"),"[^0-9,]",""),50)
		Fn_MySite     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","mysite"),"[^0-9,]",""),50)
		Fn_Birthday   = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","birthday"),"[^0-9\-]",""),50)
		
		Fn_Mobile     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","mobile"),"[^0-9,]",""),50)
		Fn_Telephone  = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","telephone"),"[^0-9\-,]",""),50)
		Fn_QQ         = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","qq"),"[^0-9,]",""),50)
		Fn_Height     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","height"))
		Fn_Weight     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","weight"))
		Fn_MaritalStatus = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","maritalstatus"))
		Fn_BloodType  = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","bloodtype"),"[^a-zA-Z]",""),5)
		Fn_Religion   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","religion"))
		Fn_Smoke      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","smoke"))
		Fn_Drink      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","drink"))
		Fn_FavTravel  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","favtravel")),250)
		Fn_FavLiving  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","favliving")),250)
		Fn_FavWork    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","favwork")),250)
		Fn_FavPartner = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","favpartner")),250)
		Fn_FavCar     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","favcar")),250)
		
		Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("Email:"&Fn_Email),Array("ID:"&Fn_UserID))
		If Fn_Result Then
			If KnifeCMS.Data.IsNul(KnifeCMS.DB.GetFieldByField(DBTable_Members_Info,"ID","UserID",Fn_UserID)) Then
				Fn_Result = KnifeCMS.DB.AddRecord(DBTable_Members_Info,Array("UserID:"&Fn_UserID,"RealName:"& Fn_RealName,"Gender:"& Fn_Gender,"HomeTown:"& Fn_HomeTown,"MySite:"& Fn_MySite,"Birthday:"& Fn_Birthday,"Mobile:"& Fn_Mobile,"Telephone:"& Fn_Telephone,"QQ:"& Fn_QQ,"Height:"& Fn_Height,"Weight:"& Fn_Weight,"MaritalStatus:"& Fn_MaritalStatus,"BloodType:"& Fn_BloodType,"Religion:"& Fn_Religion,"Smoke:"& Fn_Smoke,"Drink:"& Fn_Drink,"FavTravel:"& Fn_FavTravel,"FavLiving:"& Fn_FavLiving,"FavWork:"& Fn_FavWork,"FavPartner:"& Fn_FavPartner,"FavCar:"& Fn_FavCar))
			Else
				Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_Members_Info,Array("RealName:"& Fn_RealName,"Gender:"& Fn_Gender,"HomeTown:"& Fn_HomeTown,"MySite:"& Fn_MySite,"Birthday:"& Fn_Birthday,"Mobile:"& Fn_Mobile,"Telephone:"& Fn_Telephone,"QQ:"& Fn_QQ,"Height:"& Fn_Height,"Weight:"& Fn_Weight,"MaritalStatus:"& Fn_MaritalStatus,"BloodType:"& Fn_BloodType,"Religion:"& Fn_Religion,"Smoke:"& Fn_Smoke,"Drink:"& Fn_Drink,"FavTravel:"& Fn_FavTravel,"FavLiving:"& Fn_FavLiving,"FavWork:"& Fn_FavWork,"FavPartner:"& Fn_FavPartner,"FavCar:"& Fn_FavCar),Array("UserID:"&Fn_UserID))
			End If
		End If
		
		If Fn_Result Then Fn_ReturnString = "ok"
		SaveInfo = Fn_ReturnString
	End Function
	
	Public Function SaveAddress()
		SaveAddress = OS.SaveAddress(KnifeCMS.GetForm("post","userid"),KnifeCMS.GetForm("post","region"),KnifeCMS.GetForm("post","address"),KnifeCMS.GetForm("post","zip"),KnifeCMS.GetForm("post","name"),KnifeCMS.GetForm("post","telephone"),KnifeCMS.GetForm("post","mobile"))
	End Function
	
	Public Function ChangePassword()
		Dim Fn_Rs,Fn_Check,Fn_ID,Fn_Result,Fn_ReturnString
		Dim Fn_UserID,Fn_TempPassword,Fn_PasswordOld,Fn_PasswordNew,Fn_RePasswordNew
		Fn_UserID        = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","userid"))
		If Not(1=Logined) Or Fn_UserID<>UserID Then ChangePassword="unlogined" : Exit Function
		Fn_PasswordOld   = KnifeCMS.ParsePassWord(KnifeCMS.GetForm("post","passwordold"))
		Fn_PasswordNew   = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","passwordnew"),20)
		Fn_RePasswordNew = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","repasswordnew"),20)
		If KnifeCMS.Data.IsNul(Fn_PasswordNew) Or KnifeCMS.Data.IsNul(Fn_PasswordOld) Then ChangePassword="error" : Exit Function
		If Fn_PasswordNew <> Fn_RePasswordNew Then ChangePassword="error_pwdift" : Exit Function
		Fn_TempPassword = KnifeCMS.DB.GetFieldByField(DBTable_Members,"Password","ID",Fn_UserID)
		If KnifeCMS.Data.IsNul(Fn_TempPassword) Then
			Fn_ReturnString="unlogined"
		Else
			If Fn_TempPassword <> Fn_PasswordOld Then
				Fn_ReturnString="error_password"
			Else
				Fn_PasswordNew  = KnifeCMS.ParsePassWord(Fn_PasswordNew)
				Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("Password:"& Fn_PasswordNew),Array("ID:"&Fn_UserID))
				If Fn_Result Then
					OS.LoginOut
					Fn_ReturnString = "ok"
				End If
			End If
		End If
		Echo Fn_ReturnString
	End Function
	
	Public Function ForgetPassword()
		ForgetPassword = OS.ForgetPassword()
	End Function
	
	Public Function ResetPassword()
		ResetPassword = OS.ResetPassword()
	End Function
	
	'获取投票详细
	Public Function VoteDetail()
		Dim Fn_Temp,Fn_Rs,Fn_ReturnString
		Dim Fn_ID,Fn_ClassID,Fn_VoteTitle,Fn_VoteMode,Fn_VoteMode_More,Fn_Description,Fn_ResultDescribe,Fn_Hits,Fn_Joins,Fn_Addtime,Fn_Edittime
		Dim Fn_OptionsJSONString : Fn_OptionsJSONString = ""
		Dim Fn_IsVoteExist : Fn_IsVoteExist = False
		Fn_ID  = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","voteid"))
		If Fn_ID>0 Then
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM "& DBTable_Vote &" WHERE [ID]="& Fn_ID &" AND [Recycle]=0")
			If Not(Fn_Rs.Eof) Then
				Fn_IsVoteExist    = True
				Fn_VoteTitle      = KnifeCMS.Data.Escape(Fn_Rs("VoteTitle"))
				Fn_VoteMode       = KnifeCMS.Data.CLng(Fn_Rs("VoteMode"))
				Fn_VoteMode_More  = KnifeCMS.Data.CLng(Fn_Rs("VoteMode_More"))
				Fn_Description    = KnifeCMS.Data.Escape(Fn_Rs("Description"))
				Fn_ResultDescribe = KnifeCMS.Data.Escape(Fn_Rs("ResultDescribe"))
				Fn_Hits           = Fn_Rs("Hits")
				Fn_Joins          = Fn_Rs("Joins")
				Fn_Addtime        = Fn_Rs("Addtime")
			Else
				Fn_ReturnString="error"
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
		Else
			Fn_ReturnString = "error"
		End If
		If Fn_IsVoteExist Then
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM "& DBTable_VoteOption &" WHERE [VoteID]="& Fn_ID &"")
			Do While Not(Fn_Rs.Eof)
				Fn_Temp = "{""id"":"""& Fn_Rs("ID") &""",""content"":"""& KnifeCMS.Data.Escape(Fn_Rs("Content")) &""",""votenum"":"""& Fn_Rs("VoteNum") &"""}"
				Fn_OptionsJSONString = KnifeCMS.IIF(Fn_OptionsJSONString="",Fn_Temp,Fn_OptionsJSONString &","& Fn_Temp)
				Fn_Rs.Movenext()
			Loop
			KnifeCMS.DB.CloseRs Fn_Rs
			Fn_ReturnString = "{""votetitle"":"""& Fn_VoteTitle &""",""votemode"":"""& Fn_VoteMode &""",""votemode_mode"":"""& Fn_VoteMode_More &""",""description"":"""& Fn_Description &""",""resultdescribe"":"""& Fn_ResultDescribe &""",""hits"":"""& Fn_Hits &""",""joins"":"""& Fn_Joins &""",""addtime"":"""& Fn_Addtime &""",""vote_options"":["& Fn_OptionsJSONString &"]}"
		End If
		VoteDetail = Fn_ReturnString
	End Function
	
	'投票
	Public Function VotePoll()
		Dim Fn_ii,Fn_jj,Fn_Rs,Fn_Check,Fn_ID,Fn_Result,Fn_ReturnString
		Dim Fn_IsVoteExist : Fn_IsVoteExist = False
		Dim Fn_VoteID,Fn_OptionID,Fn_VoteMode,Fn_VoteMode_More,Fn_ArrayOptionID,Fn_Temp
		Fn_VoteID   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","voteid"))
		Fn_OptionID = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","optionid"),500)
		If Fn_VoteID>0 Then
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM "& DBTable_Vote &" WHERE [ID]="& Fn_VoteID &" AND [Recycle]=0")
			If Not(Fn_Rs.Eof) Then
				Fn_IsVoteExist   = True
				Fn_VoteMode      = KnifeCMS.Data.CLng(Fn_Rs("VoteMode"))
				Fn_VoteMode_More = KnifeCMS.Data.CLng(Fn_Rs("VoteMode_More"))
			Else
				Fn_ReturnString="error"
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
		Else
			Fn_ReturnString = "error"
		End If
		If Fn_IsVoteExist Then
			Fn_ArrayOptionID = Split(Fn_OptionID,",")
			Fn_OptionID      = ""
			Fn_Temp          = 0
			Fn_jj            = 0
			For Fn_ii=0 To Ubound(Fn_ArrayOptionID)
				Fn_Temp = KnifeCMS.Data.CLng(Fn_ArrayOptionID(Fn_ii))
				If Fn_Temp>0 Then
					If KnifeCMS.Data.IsNul(Fn_OptionID) Then
						Fn_OptionID = Fn_Temp
					Else
						If Fn_VoteMode>1 Then
							If Fn_jj<Fn_VoteMode_More Then Fn_OptionID = Fn_OptionID &","& Fn_Temp
						End If
					End If
					Fn_jj = Fn_jj + 1
				End If
			Next
			If Not(KnifeCMS.Data.IsNul(Fn_OptionID)) Then
				Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_VoteOption,Array("VoteNum=VoteNum+1"),"[VoteID]="& Fn_VoteID &" AND [ID] IN ("& Fn_OptionID &")")
				If Fn_Result Then
					Call KnifeCMS.DB.UpdateRecord(DBTable_Vote,Array("Joins=Joins+1"),"[ID]="& Fn_VoteID &"")
					Fn_ReturnString="ok"
				End If
			End If
		End If
		VotePoll=Fn_ReturnString
	End Function
	
	'更新内容浏览量+1
	Public Function AddContentHits(ByVal BV_SubSystem,ByVal BV_ContentID)
		Dim Fn_DBTable,Fn_Result : Fn_Result = 0
		BV_ContentID = KnifeCMS.Data.CLng(BV_ContentID)
		If BV_ContentID>0 And Trim(BV_SubSystem)<>"" Then
			If IsContentSubSystemExist(BV_SubSystem) Then
				Fn_DBTable = TablePre &"Content_"& SubSystem
				Fn_Result = KnifeCMS.DB.UpdateRecord(Fn_DBTable,Array("Hits=Hits+1"),"[ID]="& BV_ContentID &"")
			End If
		End If
		If Fn_Result Then
			AddContentHits = "ok"
		Else
			AddContentHits = "error"
		End If
	End Function
	
	'更新商品浏览量+1
	Public Function AddGoodsHits(ByVal BV_GoodsID)
		Dim Fn_Result : Fn_Result = 0
		BV_GoodsID = KnifeCMS.Data.CLng(BV_GoodsID)
		If BV_GoodsID>0 Then
			Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_Goods,Array("Hits=Hits+1"),"[ID]="& BV_GoodsID &"")
		End If
		If Fn_Result Then
			AddGoodsHits = "ok"
		Else
			AddGoodsHits = "error"
		End If
	End Function
	
	'更新投票点击量+1
	Public Function AddVoteHits(ByVal BV_ID)
		Dim Fn_Result : Fn_Result = 0
		BV_ID = KnifeCMS.Data.CLng(BV_ID)
		If BV_ID>0 Then
			Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_Vote,Array("Hits=Hits+1"),"[ID]="& BV_ID &"")
		End If
		If Fn_Result Then
			AddVoteHits = "ok"
		Else
			AddVoteHits = "error"
		End If
	End Function
	
	'更新友情链接点击量+1
	Public Function AddLinkHits(ByVal BV_ID)
		Dim Fn_Result : Fn_Result = 0
		BV_ID = KnifeCMS.Data.CLng(BV_ID)
		If BV_ID>0 Then
			Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_Link,Array("Hits=Hits+1"),"[ID]="& BV_ID &"")
		End If
		If Fn_Result Then
			AddLinkHits = "ok"
		Else
			AddLinkHits = "error"
		End If
	End Function
	
End Class
%>