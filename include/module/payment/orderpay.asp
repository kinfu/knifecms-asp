<!--#include file="../../../config/config.main.asp"-->
<!--#include file="../../kfc.main.asp"-->
<!--#include file="../../kfc.initialization.asp"-->
<%
'**
'订单支付
'filename: orderpay.asp
'copyright: KFC
'**
Dim OrderPay
Set OrderPay = New Class_OrderPay
Set OrderPay = Nothing
Class Class_OrderPay

	Private Pv_IsOrderExist,Pv_Final_Amount
	Private Pv_PayType,Pv_PayMethod,Pv_Fee,Pv_Poundage,Pv_PaymentID,Pv_PaymentName,Pv_OrderID
	
	Private Sub Class_Initialize()
		Pv_IsOrderExist = False
		Call Init()
	End Sub
	Private Sub Class_Terminate()
		Set OS = Nothing
	End Sub
	
	Public Sub Init()
		Pv_PaymentID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","payment"))
		Pv_OrderID   = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","orderid"),"[^0-9]",""),16)
		If Not(Logined=1) Then
			'未登陆
			Response.Redirect(SystemPath &"index.asp?login"& FileSuffix)
		Else
			If Pv_PaymentID>0 Then
				'检查支付方式是否存在
				Set Rs = KnifeCMS.DB.GetRecord(DBTable_Payments &":PaymentName,PayType,PayMethod,Fee",Array("ID:"& Pv_PaymentID),"")
				If Not(Rs.Eof) Then
					IsContentExist = True
					Pv_PaymentName = Rs("PaymentName")
					Pv_PayType     = Rs("PayType")
					Pv_PayMethod   = Rs("PayMethod")
					Pv_Fee         = Rs("Fee")
				Else
					IsContentExist = False
				End If
				KnifeCMS.DB.CloseRs Rs
				If IsContentExist Then
					'检查订单是否存在并且是操作者的订单
					Set Rs = KnifeCMS.DB.GetRecord(DBTable_Order &":Final_Amount",Array("OrderID:"& Pv_OrderID,"UserID:"& UserID,"Recycle:0"),"")
					If Not(Rs.Eof) Then
						Pv_IsOrderExist = True
						Pv_Final_Amount = Rs(0)
					End If
					KnifeCMS.DB.CloseRs Rs
					'手续费
					Pv_Poundage = KnifeCMS.Data.FormatCurrency(KnifeCMS.IIF(Pv_PayMethod=1,Pv_Final_Amount * Pv_Fee,Pv_Fee))
					
					If Pv_IsOrderExist Then
						'更新订单的支付方式和订单总额
						Call KnifeCMS.DB.UpdateRecord(DBTable_Order,Array("PaymentID:"& Pv_PaymentID,"Payment:"& Pv_PaymentName,"Cost_Payment:"& Pv_Poundage,"Final_Amount:"& Pv_Final_Amount + Pv_Poundage),Array("OrderID:"& Pv_OrderID,"UserID:"& UserID))
						If Pv_PayType="alipay" Then
						'alipay:支付宝
							Response.Redirect(""& SystemPath &"plugins/payment/pay.asp?ctl=orderpay&orderid="& Pv_OrderID)
						Else
						'offline:汇款/转帐/线下支付
						'cod:货到付款
							Response.Redirect(""& SystemPath &"index.asp?order/payresult/"& Pv_OrderID & FileSuffix)
						End If
					Else
						'订单号不正确
						Echo Lang_OrderIDIsError
					End If
				Else
					'支付方式不存在
					Echo Lang_PaymentNotExist
				End If
			End If
		End If
	End Sub
End Class
%>