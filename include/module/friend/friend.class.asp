<%
Dim Friend
Set Friend = New Class_Friend
Class Class_Friend
	
	Private Pv_Rs,Pv_Array
	Private S_TempHTML,S_AllowedDepth,S_ReplyTpl
	
	Private Sub Class_Initialize()
		S_AllowedDepth = 3 '评论允许最大级数
		
	End Sub
	Private Sub Class_Terminate()
	End Sub
	
	Public Property Let Depth(ByVal BV_Depth)
		S_AllowedDepth = LCase(BV_Depth)
	End Property
	Public Property Get Depth()
		Depth = S_AllowedDepth
	End Property
	
	'发送加为好友的消息
	Public Function SendAddFriendNotify()
		If Not(1=Logined) Then SendAddFriendNotify="unlogined" : Exit Function
		'检查是否恶意发布(15秒钟内不许再发布)
		Dim ForbidPostTime : ForbidPostTime = 15 '秒
		If CheckIsInForbidPostTime(ForbidPostTime,"SELECT TOP 1 NotifyTime FROM ["& DBTable_Notify &"] WHERE CNUserID="& UserID &" ORDER BY ID DESC ",SysTime) Then SendAddFriendNotify="inforbidtime_"& ForbidPostTime : Exit Function
		Dim Fn_Result
		Dim Fn_Rs,Fn_IsFriend,Fn_BeFriendID,Fn_Msg
		Fn_BeFriendID   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","userid"))
		If Not(Fn_BeFriendID>0) Then SendAddFriendNotify="error" : Exit Function
		Dim Fn_CNUserID,Fn_SNUserID,Fn_NotifyType,Fn_NotifyConfig,Fn_NotifyTime
		Fn_IsFriend     = false
		Fn_Msg          = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","msg")),250)
		SysID           = KnifeCMS.CreateSysID()
		Fn_CNUserID     = UserID         '主动要求加某人为好友的用户
		Fn_SNUserID     = Fn_BeFriendID  '被为好友的用户
		Fn_NotifyType   = "addfriend"    '消息类型 addfriend
		Fn_NotifyConfig = """cnuserid"":"""& Fn_CNUserID &""",""snuserid"":"""& Fn_SNUserID &""",""msg"":"""& Fn_Msg & """"
		Fn_NotifyTime   = SysTime        '消息发生时间
		If Fn_CNUserID=Fn_SNUserID Then SendAddFriendNotify="error" : Exit Function
		'判断是否已经是好友
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Friend &":ID",Array("UserID:"& Fn_CNUserID,"FriendID:"& Fn_SNUserID),"")
		If Not(Fn_Rs.Eof) Then Fn_IsFriend = true
		KnifeCMS.DB.CloseRs Fn_Rs
		If Fn_IsFriend Then SendAddFriendNotify="isfriend" : Exit Function
		Fn_Result = KnifeCMS.DB.AddRecord(DBTable_Notify,Array("SysID:"& SysID,"CNUserID:"& Fn_CNUserID,"SNUserID:"& Fn_SNUserID,"NotifyType:"& Fn_NotifyType,"NotifyConfig:"& Fn_NotifyConfig,"NotifyTime:"& Fn_NotifyTime,"TreatTime:","IsTreated:0"))
		If Fn_Result Then SendAddFriendNotify="ok"
	End Function
	
	'获取好友申请信息
	Public Function GetFriendRequest()
		If Not(1=Logined) Then GetFriendRequest="unlogined" : Exit Function
		Dim Fn_Sql,Fn_Rs
		Dim Fn_MemberID,Fn_TempString,Fn_ReturnString
		Dim Fn_ID,Fn_UserID,Fn_UserName,Fn_HeadImg,Fn_NotifyConfig,Fn_NotifyTime,Fn_Message
		    Fn_MemberID = UserID
		Fn_Sql = "SELECT a.ID,a.CNUserID,b.Username,b.HeadImg,a.NotifyConfig,a.NotifyTime FROM ["& DBTable_Notify &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.CNUserID) WHERE a.SNUserID="& Fn_MemberID &" AND a.NotifyType='addfriend' AND a.IsTreated=0 ORDER BY a.ID DESC"
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
		'Die Fn_Sql
		Do While Not(Fn_Rs.Eof)
				Fn_ID           = KnifeCMS.Data.Escape(Fn_Rs("ID"))
				Fn_UserID       = KnifeCMS.Data.Escape(Fn_Rs("CNUserID"))
				Fn_UserName     = KnifeCMS.Data.Escape(Fn_Rs("Username"))
				Fn_HeadImg      = KnifeCMS.Data.Escape(KnifeCMS.Data.URLDecode(Fn_Rs("HeadImg")))
				Fn_NotifyConfig = Fn_Rs("NotifyConfig")
				Fn_NotifyTime   = KnifeCMS.Data.Escape(Fn_Rs("NotifyTime"))
				Fn_Message      = KnifeCMS.Data.Escape(KnifeCMS.Data.RegSubMatches(Fn_NotifyConfig,"""msg"":""([\s\S]*?)""")(0,0))
				Fn_TempString   = """id"":"""& Fn_ID &""",""userid"":"""& Fn_UserID &""",""username"":"""& Fn_UserName &""",""headimg"":"""& Fn_HeadImg &""",""message"":"""& Fn_Message &""",""time"":"""& Fn_NotifyTime &""""
				If Fn_ReturnString = "" Then
					Fn_ReturnString = Fn_TempString
				Else
					Fn_ReturnString = Fn_ReturnString &";"& Fn_TempString
				End IF
				Fn_Rs.movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		GetFriendRequest = Fn_ReturnString
	End Function
	
	Public Function FriendRequestDo()
	    If Not(1=Logined) Then FriendRequestDo="unlogined" : Exit Function
		Dim Fn_Result,Fn_Rs
		Dim Fn_MemberID,Fn_TempString,Fn_ReturnString,Fn_IsFriend,Fn_FriendName
		Dim Fn_CNUserID,Fn_GroupID,Fn_NotifyType,Fn_NotifyConfig,Fn_Msg,Fn_NotifyTime
			Fn_MemberID = UserID
			Fn_IsFriend = false
			Fn_ReturnString = "error"
		If SubAction="all" Then
			Select Case Action
				Case "ignore"
					Fn_Result = KnifeCMS.DB.DeleteRecord(DBTable_Notify,Array("SNUserID:"& Fn_MemberID,"NotifyType:addfriend"))
					If Fn_Result Then Fn_ReturnString="ok"
				Case Else : Fn_ReturnString="error"
			End Select
		Else
			Fn_CNUserID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","cnuserid"))
			If Not(Fn_CNUserID>0) Then FriendRequestDo="error" : Exit Function
			'检查好友请求是否存在，不存在则说明是客户端伪造的信息
			Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Notify &":ID",Array("CNUserID:"& Fn_CNUserID,"SNUserID:"& Fn_MemberID,"NotifyType:addfriend"),"")
			If Fn_Rs.Eof Then Fn_ReturnString = "null"
			KnifeCMS.DB.CloseRs Fn_Rs
			If Fn_ReturnString="null" Then FriendRequestDo="null" : Exit Function
			
			'获取好友帐号,不存在则提示错误.
			Fn_FriendName = KnifeCMS.DB.GetFieldByID(DBTable_Members,"Username",Fn_CNUserID)
			If KnifeCMS.Data.IsNul(Fn_FriendName) Then FriendRequestDo="null" : Exit Function
			
			'判断对方是否已经是我的好友
			Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Friend &":ID",Array("UserID:"& Fn_MemberID,"FriendID:"& Fn_CNUserID),"")
			If Not(Fn_Rs.Eof) Then Fn_IsFriend = true
			KnifeCMS.DB.CloseRs Fn_Rs
			If Fn_IsFriend Then
				'判断我是否是对方的好友,若不是则添加为好友
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Friend &":ID",Array("UserID:"& Fn_CNUserID,"FriendID:"& Fn_MemberID),"")
				If Fn_Rs.Eof Then
					Call KnifeCMS.DB.AddRecord(DBTable_Friend,Array("UserID:"& Fn_CNUserID,"FriendID:"& Fn_MemberID,"GroupID:0","Remarks:","Disabled:0"))
				End If
				KnifeCMS.DB.CloseRs Fn_Rs
				'删除好友申请记录
				Call KnifeCMS.DB.DeleteRecord(DBTable_Notify,Array("CNUserID:"& Fn_CNUserID,"SNUserID:"& Fn_MemberID,"NotifyType:addfriend"))
				Call KnifeCMS.DB.DeleteRecord(DBTable_Notify,Array("CNUserID:"& Fn_MemberID,"SNUserID:"& Fn_CNUserID,"NotifyType:addfriend"))
				FriendRequestDo="isfriend" : Exit Function
			End If
			'若双方为非好友关系则继续操作
			Select Case Action
				Case "accept"
					If Not(Fn_IsFriend) Then
						'检查groupid分组是否存在
						Fn_GroupID=KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","groupid"))
						If Fn_GroupID>0 Then
							Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_FriendGroup &":SysID",Array("ID:"& Fn_GroupID,"UserID:"& Fn_MemberID),"")
							If Fn_Rs.Eof Then Fn_GroupID=0
							KnifeCMS.DB.CloseRs Fn_Rs
						End If
						'增加好友记录
						'增加对方为我的好友记录
						Fn_Result = KnifeCMS.DB.AddRecord(DBTable_Friend,Array("UserID:"& Fn_MemberID,"FriendID:"& Fn_CNUserID,"GroupID:"& Fn_GroupID,"Remarks:","Disabled:0"))
						If Fn_Result Then
							'增加我为对方的好友记录
							Fn_Result = KnifeCMS.DB.AddRecord(DBTable_Friend,Array("UserID:"& Fn_CNUserID,"FriendID:"& Fn_MemberID,"GroupID:0","Remarks:","Disabled:0"))
						End If
						If Fn_Result Then
							'删除好友申请记录
							Call KnifeCMS.DB.DeleteRecord(DBTable_Notify,Array("CNUserID:"& Fn_CNUserID,"SNUserID:"& Fn_MemberID,"NotifyType:addfriend"))
							Call KnifeCMS.DB.DeleteRecord(DBTable_Notify,Array("CNUserID:"& Fn_MemberID,"SNUserID:"& Fn_CNUserID,"NotifyType:addfriend"))
						End If
						Fn_Msg          = KnifeCMS.Data.HTMLEncode("<a href=""?user/"& Fn_CNUserID & FileSuffix &""" target=""_blank"">"&Fn_FriendName&"</a>接受了你的好友请求")
						SysID           = KnifeCMS.CreateSysID()
						Fn_NotifyType   = "acceptfriend"    '消息类型 addfriend
						Fn_NotifyConfig = """cnuserid"":"""& Fn_MemberID &""",""snuserid"":"""& Fn_CNUserID &""",""msg"":"""& Fn_Msg &""""
						Fn_NotifyTime   = SysTime        '消息发生时间
						Fn_Result = KnifeCMS.DB.AddRecord(DBTable_Notify,Array("SysID:"& SysID,"CNUserID:"& Fn_MemberID,"SNUserID:"& Fn_CNUserID,"NotifyType:"& Fn_NotifyType,"NotifyConfig:"& Fn_NotifyConfig,"NotifyTime:"& Fn_NotifyTime,"TreatTime:null","IsTreated:0"))
						Fn_ReturnString="ok"
					End If
				Case "reject"
					'删除好友申请记录
					Fn_Result = KnifeCMS.DB.DeleteRecord(DBTable_Notify,Array("CNUserID:"& Fn_CNUserID,"SNUserID:"& Fn_MemberID,"NotifyType:addfriend"))
					If Fn_Result Then
						'向对方发送拒绝信息
						Fn_Msg          = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","rejectmsg")),50)
						SysID           = KnifeCMS.CreateSysID()
						Fn_NotifyType   = "rejectfriend"    '消息类型 addfriend
						Fn_NotifyConfig = """cnuserid"":"""& Fn_MemberID &""",""snuserid"":"""& Fn_CNUserID &""",""msg"":"""& Fn_Msg & """"
						Fn_NotifyTime   = SysTime        '消息发生时间
						Fn_Result = KnifeCMS.DB.AddRecord(DBTable_Notify,Array("SysID:"& SysID,"CNUserID:"& Fn_MemberID,"SNUserID:"& Fn_CNUserID,"NotifyType:"& Fn_NotifyType,"NotifyConfig:"& Fn_NotifyConfig,"NotifyTime:"& Fn_NotifyTime,"TreatTime:null","IsTreated:0"))
						'Fn_Result = KnifeCMS.DB.AddRecord(DBTable_Friend,Array("CNUserID:"& ,"SNUserID:"& Fn_CNUserID,"GroupID:"& Fn_GroupID,"Remarks:","Disabled:0"))
					End If
					If Fn_Result Then Fn_ReturnString="ok"
				Case "ignore"
					'删除好友申请记录
					Fn_Result = KnifeCMS.DB.DeleteRecord(DBTable_Notify,Array("CNUserID:"& Fn_CNUserID,"SNUserID:"& Fn_MemberID,"NotifyType:addfriend"))
					If Fn_Result Then Fn_ReturnString="ok"
				Case Else : Fn_ReturnString="error"
			End Select
		End If
		FriendRequestDo = Fn_ReturnString
	End Function
	
	Public Function GetFriends(ByVal BV_IsMy)
		If Not(1=Logined) Then GetFriends="unlogined" : Exit Function
		If BV_IsMy = 0 Then
			GetFriends = GetMembersFriends()
		ElseIf BV_IsMy = 1 Then
			GetFriends = GetMyFriends()
		Else
			GetFriends = "error"
		End If
	End Function
	
	Public Function GetMyFriends()
		 If Not(UserID>0) Then GetMyFriends="error" : Exit Function
		 GetMyFriends = GetFriendsData(UserID)
	End Function
	
	Public Function GetMembersFriends()
		Dim Fn_MemberID
		Fn_MemberID =KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","memberid"))
		 If Not(Fn_MemberID>0) Then GetMembersFriends="error" : Exit Function
		GetMembersFriends = GetFriendsData(Fn_MemberID)
	End Function
	
	Public Function GetFriendsData(ByVal BV_UserID)
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_DBTable,Fn_Sql,Fn_Rs,Fn_SqlTop,Fn_SqlCondition,Fn_SqlOrderBy,Fn_TempGroupID,Fn_MemberID,Fn_TopNum
		Dim Fn_Template,Fn_TempString
		Dim Fn_ID,Fn_Name,Fn_HeadImg,Fn_GroupID,Fn_GroupName,Fn_LatestFeeds
		Dim Fn_IsPage,Fn_Page,Fn_PageSize,Fn_PageString
		    Fn_IsPage      = KnifeCMS.IIF(KnifeCMS.GetForm("post","pagesize")="unpage",False,True)
			Fn_MemberID    = BV_UserID
			Fn_TempGroupID = KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","groupid"),"[^0-9]","")
			Fn_TempGroupID = KnifeCMS.IIF(Fn_TempGroupID<>"",KnifeCMS.Data.CLng(Fn_TempGroupID),"")
			Fn_PageSize    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","pagesize"))
			Fn_PageSize    = KnifeCMS.IIF(Fn_PageSize>0,Fn_PageSize,50)
			Fn_TopNum      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","topnum")) : Fn_TopNum = KnifeCMS.IIF(Fn_TopNum>1000,1000,Fn_TopNum)
			Fn_SqlTop      = KnifeCMS.IIF(Fn_TopNum>0,"TOP "& Fn_TopNum,"")
		    Fn_SqlCondition= KnifeCMS.IIF(Fn_TempGroupID<>""," AND a.GroupID="& Fn_TempGroupID &"","")
		    Fn_SqlOrderBy  = " ORDER BY a.ID ASC "
			Select Case DB_Type
			Case 0
				Fn_Sql = "SELECT "& Fn_SqlTop &" a.FriendID,b.Username,b.HeadImg,a.GroupID,c.GroupName FROM (["& DBTable_Friend &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.FriendID)) LEFT JOIN ["& DBTable_FriendGroup &"] c ON (c.ID=a.GroupID) WHERE a.UserID="& Fn_MemberID &" AND a.Disabled=0 "& Fn_SqlCondition & Fn_SqlOrderBy &""
				'Set Rs=KnifeCMS.DB.GetRecordBySQL("SELECT a.Username,a.TruePassword,a.UserDisable,a.Recycle,a.GroupID,b.GroupName,b.Degree,c.ID,c.Username FROM (["& DBTable_SysUser &"] a LEFT JOIN ["& DBTable_SysUserGroup &"] b ON (b.ID=a.GroupID)) LEFT JOIN ["& DBTable_Members &"] c ON (c.ID=a.MemberID) WHERE a.ID="& SysUserID &"")
			Case 1
				 Fn_Sql = "SELECT "& Fn_SqlTop &" a.FriendID,b.Username,b.HeadImg,a.GroupID,c.GroupName FROM ["& DBTable_Friend &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.FriendID) LEFT JOIN ["& DBTable_FriendGroup &"] c ON (c.ID=a.GroupID) WHERE a.UserID="& Fn_MemberID &" AND a.Disabled=0 "& Fn_SqlCondition & Fn_SqlOrderBy &""
				'Set Rs=KnifeCMS.DB.GetRecordBySQL("SELECT a.Username,a.TruePassword,a.UserDisable,a.Recycle,a.GroupID,b.GroupName,b.Degree,c.ID,c.Username FROM ["& DBTable_SysUser &"] a LEFT JOIN ["& DBTable_SysUserGroup &"] b ON (b.ID=a.GroupID) LEFT JOIN ["& DBTable_Members &"] c ON (c.ID=a.MemberID) WHERE a.ID="& SysUserID &"")
			End Select
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
		If Fn_IsPage Then
			If Not(Fn_Rs.Eof) Then
				Dim Fn_Temp_i,Fn_Temp_j,Fn_Temp_k,Fn_a,Fn_RsCount,Fn_PageCount,Fn_DataArray
				Fn_RsCount=Fn_Rs.RecordCount : If Fn_Rs.RecordCount<1 then Fn_RsCount=0
				Fn_Rs.PageSize = Fn_PageSize
				Fn_PageCount = Fn_Rs.PageCount
				Fn_Page = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","page")) : Fn_Page = KnifeCMS.IIF(Not(Fn_Page>0),1,Fn_Page)
				'Die KnifeCMS.URLQueryString
				if Fn_Page > Fn_PageCount then
					Fn_Rs.AbsolutePage = Fn_PageCount
				else
					Fn_Rs.AbsolutePage = Fn_Page
				end if
				Fn_Page = Fn_Rs.AbsolutePage
				ReDim DataArray(Fn_Rs.Fields.Count,Fn_PageSize)
				
				For Fn_Temp_i=0 to Fn_PageSize-1
					If Fn_Rs.EOF Then Exit For
					Fn_ID          = KnifeCMS.Data.Escape(Fn_Rs("FriendID"))
					Fn_Name        = KnifeCMS.Data.Escape(Fn_Rs("Username"))
					Fn_HeadImg     = KnifeCMS.Data.Escape(KnifeCMS.Data.URLDecode(Fn_Rs("HeadImg")))
					Fn_GroupID     = KnifeCMS.Data.Escape(Fn_Rs("GroupID"))
					Fn_GroupName   = KnifeCMS.Data.Escape(Fn_Rs("GroupName"))
					Fn_LatestFeeds = KnifeCMS.Data.Escape(GetUserLatestFeeds(Fn_ID))
					Fn_TempString    = """id"":"""& Fn_ID &""",""name"":"""& Fn_Name &""",""headimg"":"""& Fn_HeadImg &""",""groupid"":"""& Fn_GroupID &""",""groupname"":"""& Fn_GroupName &""",""latestfeeds"":"""& Fn_LatestFeeds &""""
					If Fn_Temp_i=0 Then
						Fn_ReturnString  = Fn_TempString
					Else
						Fn_ReturnString  = Fn_ReturnString &";"& Fn_TempString
					End If
					Fn_Rs.movenext()
				Next
				Fn_PageString = "<div class=""mt5 mb5""><div class=pagestyle><span class=pages>"& Fn_RsCount &" 个好友</span>"
				If Fn_Page>1 Then Fn_PageString = Fn_PageString &"<a href=""javascript:Page('"& Fn_TempGroupID &"','"& Fn_PageSize &"',"&Fn_Page-1&");"">上一页</a>"
				if Fn_Page>1 then Fn_a=1 : if Fn_Page>2 then Fn_a=2 : if Fn_Page>3 then Fn_a=3 : if Fn_Page>4 then Fn_a=4 : if Fn_Page>5 then Fn_a=5
				for Fn_Temp_i=Fn_Page-Fn_a to Fn_Page-1
					Fn_PageString = Fn_PageString & "<a href=""javascript:Page('"& Fn_TempGroupID &"','"& Fn_PageSize &"',"&Fn_Temp_i&");"">"&Fn_Temp_i&"</a>"
				next
				Fn_PageString = Fn_PageString &"<span class=current>"&Fn_Page&"</span>"
				
				for Fn_Temp_i=Fn_Page+1 to Fn_Page+4
					if Fn_Temp_i > Fn_PageCount then Exit for
					Fn_PageString = Fn_PageString & "<a href=""javascript:Page('"& Fn_TempGroupID &"','"& Fn_PageSize &"',"&Fn_Temp_i&");"">"&Fn_Temp_i&"</a>"
				next
				
				if Not(Fn_Page>=Fn_PageCount) then Fn_PageString = Fn_PageString & "<a href=""javascript:Page('"& Fn_TempGroupID &"','"& Fn_PageSize &"',"&Fn_Page+1&");"">下一页</a>"
				Fn_PageString = Fn_PageString & "<span class=pages>共"& Fn_PageCount &"页</span><span class=pages>"& Fn_PageSize &"个/页</span></div></div>"
			End If
		Else
			Do While Not(Fn_Rs.Eof)
				Fn_ID        = KnifeCMS.Data.Escape(Fn_Rs("FriendID"))
				Fn_Name      = KnifeCMS.Data.Escape(Fn_Rs("Username"))
				Fn_HeadImg   = KnifeCMS.Data.Escape(KnifeCMS.Data.URLDecode(Fn_Rs("HeadImg")))
				Fn_GroupID   = KnifeCMS.Data.Escape(Fn_Rs("GroupID"))
				Fn_GroupName = KnifeCMS.Data.Escape(Fn_Rs("GroupName"))
				Fn_TempString    = """id"":"""& Fn_ID &""",""name"":"""& Fn_Name &""",""headimg"":"""& Fn_HeadImg &""",""groupid"":"""& Fn_GroupID &""",""groupname"":"""& Fn_GroupName &""""
				If Fn_ReturnString = "" Then
					Fn_ReturnString = Fn_TempString
				Else
					Fn_ReturnString = Fn_ReturnString &";"& Fn_TempString
				End IF
				Fn_Rs.movenext()
			Loop
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		If Not(Fn_ReturnString="") Then Fn_ReturnString ="{"& Fn_ReturnString &"}{""pagestring"":"""& KnifeCMS.Data.Escape(Fn_PageString) &"""}"
		GetFriendsData = Fn_ReturnString
	End Function
	
	Public Function GetMyGroup()
		If Not(1=Logined) Then GetMyGroup="unlogined" : Exit Function
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_Sql,Fn_Rs,Fn_MemberID
		Dim Fn_TempString
		Dim Fn_GroupID,Fn_GroupName,Fn_FriendNum
		    Fn_MemberID = UserID
		Fn_Sql = "SELECT a.ID,a.GroupName,(SELECT COUNT(*) FROM ["& DBTable_Friend &"] WHERE GroupID=a.ID) FROM ["& DBTable_FriendGroup &"] a WHERE a.UserID="& Fn_MemberID &""
		'Fn_Sql = "SELECT a.GroupID,b.GroupName,COUNT(*) FROM ["& DBTable_Friend &"] a LEFT JOIN ["& DBTable_FriendGroup &"] b ON (b.ID=a.GroupID) WHERE a.UserID="& Fn_MemberID &" Group BY a.GroupID,b.GroupName"
		'Die Fn_Sql
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
		Do While Not(Fn_Rs.Eof)
			Fn_GroupID   = KnifeCMS.Data.CLng(Fn_Rs(0))
			Fn_GroupName = KnifeCMS.Data.Escape(Fn_Rs(1))
			Fn_FriendNum = KnifeCMS.Data.CLng(Fn_Rs(2))
			Fn_TempString    = """groupid"":"""& Fn_GroupID &""",""groupname"":"""& Fn_GroupName &""",""friendnum"":"""& Fn_FriendNum &""""
			If Fn_ReturnString="" Then
				Fn_ReturnString = Fn_TempString
			Else
				Fn_ReturnString = Fn_ReturnString &";"& Fn_TempString
			End IF
			Fn_Rs.movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		If Not(Fn_ReturnString="") Then Fn_ReturnString ="{"& Fn_ReturnString &"}"
		GetMyGroup=Fn_ReturnString
	End Function
	
	Public Function MyGroupDoSave(ByVal BV_Action)
	    If Not(1=Logined) Then MyGroupDoSave="unlogined" : Exit Function
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_Sql,Fn_Rs,Fn_MemberID,Fn_TempUserID
		Dim Fn_GroupID,Fn_GroupName,Fn_FriendID,Fn_Array,Fn_i
		    Fn_GroupID  =KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","groupid"))
			Fn_GroupName=KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","groupname")),50)
			Fn_FriendID =KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","friendid"),"[^0-9,]","")
		If KnifeCMS.Data.IsNul(Fn_GroupName) Then MyGroupDoSave="error" : Exit Function
		If BV_Action="edit" Then
			If Not(Fn_GroupID>0) Then MyGroupDoSave="error" : Exit Function
			'检查分组GroupID是否是操作人的分组
			Fn_TempUserID=KnifeCMS.DB.GetFieldByField(DBTable_FriendGroup,"UserID","ID",Fn_GroupID)
			If Fn_TempUserID<>UserID Then MyGroupDoSave="error" : Exit Function
			Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_FriendGroup,Array("GroupName:"& Fn_GroupName),Array("ID:"& Fn_GroupID,"UserID:"& UserID))
			If Fn_Result Then
				Fn_Array=Split(Fn_FriendID,",")
				For Fn_i=0 To Ubound(Fn_Array)
					Fn_FriendID=Fn_Array(Fn_i)
					If Fn_FriendID>0 Then Call KnifeCMS.DB.UpdateRecord(DBTable_Friend,Array("GroupID:"& Fn_GroupID),Array("UserID:"& UserID,"FriendID:"& Fn_FriendID))
				Next
				Fn_ReturnString="ok"
			Else
				Fn_ReturnString="error"
			End If
		ElseIf BV_Action="add" Then
			Fn_GroupID = 0
			Set Fn_Rs  = KnifeCMS.DB.GetRecord(DBTable_FriendGroup&":ID",Array("UserID:"& UserID,"GroupName:"& Fn_GroupName),"")
			If Not(Fn_Rs.Eof) Then MyGroupDoSave="exist" : Exit Function
			KnifeCMS.DB.CloseRs Fn_Rs
			SysID      = KnifeCMS.CreateSysID
			Fn_Result  = KnifeCMS.DB.AddRecord(DBTable_FriendGroup,Array("SysID:"& SysID,"UserID:"& UserID,"GroupName:"& Fn_GroupName,"IsDefault:0"))
			If Fn_Result Then Fn_GroupID = KnifeCMS.DB.GetFieldBySysID(DBTable_FriendGroup,"ID",SysID)
			If Fn_Result And Fn_GroupID>0 Then
				Fn_Array=Split(Fn_FriendID,",")
				For Fn_i=0 To Ubound(Fn_Array)
					Fn_FriendID=Fn_Array(Fn_i)
					If Fn_FriendID>0 Then Call KnifeCMS.DB.UpdateRecord(DBTable_Friend,Array("GroupID:"& Fn_GroupID),Array("UserID:"& UserID,"FriendID:"& Fn_FriendID))
				Next
				Fn_ReturnString="ok"
			Else
				Fn_ReturnString="error"
			End If
		End If
		MyGroupDoSave = Fn_ReturnString
	End Function
	
	Public Function MyGroupDelete()
		If Not(1=Logined) Then MyGroupDelete="unlogined" : Exit Function
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_Sql,Fn_Rs,Fn_MemberID,Fn_TempUserID
		Dim Fn_GroupID
		    Fn_GroupID  = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","groupid"))
			Fn_MemberID = UserID
		If Not(Fn_GroupID>0) Then MyGroupDelete="error" : Exit Function
		'检查分组GroupID是否是操作人的分组
		Fn_TempUserID=KnifeCMS.DB.GetFieldByField(DBTable_FriendGroup,"UserID","ID",Fn_GroupID)
		If Fn_TempUserID<>Fn_MemberID Then MyGroupDelete="error" : Exit Function
		Fn_Result = KnifeCMS.DB.DeleteRecord(DBTable_FriendGroup,Array("ID:"& Fn_GroupID))
		If Fn_Result Then
			Call KnifeCMS.DB.UpdateRecord(DBTable_Friend,Array("GroupID:0"),Array("GroupID:"& Fn_GroupID))
			Fn_ReturnString="ok"
		Else
			Fn_ReturnString="error"
		End If
		MyGroupDelete = Fn_ReturnString
	End Function
	
	Public Function MyFriendDelete()
		If Not(1=Logined) Then MyFriendDelete="unlogined" : Exit Function
		Dim Fn_Result,Fn_ReturnString
		Dim Fn_Sql,Fn_Rs,Fn_MemberID,Fn_TempUserID
		Dim Fn_FriendID
		    Fn_FriendID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","friendid"))
			Fn_MemberID = UserID
		If Not(Fn_FriendID>0) Then MyFriendDelete="error" : Exit Function
		'从我的好友中删除
		Fn_Result = KnifeCMS.DB.DeleteRecord(DBTable_Friend,Array("UserID:"& Fn_MemberID,"FriendID:"& Fn_FriendID))
		If Fn_Result Then
			'从 被删除的好友 的好友中删除我
			Fn_Result = KnifeCMS.DB.DeleteRecord(DBTable_Friend,Array("UserID:"& Fn_FriendID,"FriendID:"& Fn_MemberID))
			Fn_ReturnString="ok"
		Else
			Fn_ReturnString="error"
		End If
		MyFriendDelete = Fn_ReturnString
	End Function
	
End Class






































%>