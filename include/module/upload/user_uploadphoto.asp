<!--#include file="../../../config/config.main.asp"-->
<!--#include file="../../kfc.main.asp"-->
<!--#include file="../../kfc.initialization.asp"-->
<!--#include file="upload.lib.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>upload photo</title>
<script type="text/javascript">
function uploadresult(){
	var ctl  = "<%=Ctl%>"
	var args = uploadresult.arguments;
	var argc = uploadresult.arguments.length;
	if(argc==1){
		if(window.parent){window.parent.uploadingfinish(ctl,args[0])}
	}else if(argc==2){
		if(window.parent){window.parent.uploadingfinish(ctl,args[0],args[1])}
	}
}
</script>
</head>
<body>
<%
If Not(Logined=1) Then
	Echo "<script type=""text/javascript"">uploadresult('NOT_LOGIN');</script>"
Else
	If Ctl="bighead" Or  Ctl="smallhead" Then
		Dim UploadSave
		Set UploadSave = New Class_UploadSave
			UploadSave.Init()
			Echo "<script type=""text/javascript"">uploadresult('"& UploadSave.Messsage &"','"& UploadSave.URL &"');</script>"
		Set UploadSave = Nothing
	End If
End If
Class Class_UploadSave
	Private Pv_Error,Pv_Messsage,Pv_URL,Pv_File,Pv_FormName,Pv_RelFilePath,Pv_AbsFilePath,Pv_FileName,Pv_UploadResult
	Private Upload,oFile
	Private Sub Class_Initialize
		Pv_Error = 0
	End Sub
	Private Sub Class_Terminate
		Set Upload = Nothing
	End Sub
	Public Property Get Error : Error=Pv_Error : End Property
	Public Property Get Messsage : Messsage=Pv_Messsage : End Property
	Public Property Get URL : URL=Pv_URL : End Property
	
	Public Sub Init()
		Pv_RelFilePath = "attachment/image/user/"& UserID
		Pv_AbsFilePath = KnifeCMS.FSO.CreateFolder(Pv_RelFilePath) &"\"
		Set Upload = New KnifeCMS_Class_Upload
		Upload.InceptMaxFile = 1
		If Ctl="bighead" Then
		Upload.MaxSize       = 200*1024 
		Else
		Upload.MaxSize       = 50*1024 
		End If
		Upload.MaxTotalSize  = 10000*1024
		Upload.AllowExt      = "jpg|jpeg|gif|png|bmp"
		Upload.UploadPath    = Pv_AbsFilePath
		Upload.GetUploadData()
		If Upload.IsError Then
			Pv_Messsage = Upload.ErrMessage
		Else
			If Not(IsNull(Upload.File)) Then
				For Each Pv_File In Upload.File
					Set oFile=Upload.File(Pv_File)
					Pv_FileName = KnifeCMS.Data.FormatDateTime(SysTime,8) & KnifeCMS.MakeRandom(8) & KnifeCMS.MakeRandom(8) & KnifeCMS.MakeRandom(8)
					Pv_UploadResult = Upload.SaveFile(Pv_File,Pv_FileName)
					If Upload.IsError Then
						Pv_Messsage = Upload.ErrMessage : Exit For
					Else
						If Pv_UploadResult<>False And Len(Pv_UploadResult)>1 Then
							Pv_URL = SystemPath & Pv_RelFilePath &"/"& Pv_UploadResult
						Else
							Pv_Messsage = "UPLOAD_FAIL"
						End If
					End If
				next
			End If
		End If
		If KnifeCMS.Data.IsNul(Pv_Messsage) Then
			Pv_Messsage = "FINISH"
		End If
	End Sub
End Class
%>
</body>
</html>