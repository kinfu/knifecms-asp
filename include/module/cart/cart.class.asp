<%
Dim Cart
Set Cart = New Class_Cart
Class Class_Cart
	
	Private Pv_Rs,Pv_Array
	
	Private Sub Class_Initialize()
	End Sub
	Private Sub Class_Terminate()
	End Sub
	
	'返回JSON格式
	Public Function GetCart()
		Dim Fn_ReturnString
		Dim Fn_CartJSONString,Fn_TheSkus,Fn_Skus,Fn_ThePacks,Fn_Packs
		Dim Fn_ID,Fn_Num,Fn_AttrID,Fn_TempAttrID,Fn_GoodsID,Fn_GoodsName,Fn_Thumbnail,Fn_GoodsPrice,Fn_GoodsBN,Fn_Price,Fn_Weight,Fn_AttrValue,Fn_TempAttrValue,Fn_AttrPrice,Fn_AttrPriceAmount,Fn_GoodsAttrConfig
		Dim Fn_SeoUrl,Fn_Packs_GoodsSeoUrl
		Dim Fn_Rs,Fn_SQL,Fn_ii,Fn_jj
		Dim Fn_TheSkusString,Fn_ThePacksString,Fn_CartWeight,Fn_Cost_Item
		Dim Fn_TempString
		Dim Fn_GoodsExit
		Dim Fn_DataArray,Fn_i
		Fn_CartWeight = 0
		Fn_Cost_Item = 0
		Fn_CartJSONString = KnifeCMS.Cookie.GetCookie("mycart")
		If KnifeCMS.Data.IsNul(Fn_CartJSONString) Then GetCart="" : Exit Function
		Fn_TheSkus  = KnifeCMS.Data.RegSubMatches(Fn_CartJSONString,"""theskus"":\[([\s\S]*?)\]")(0,0)
		Fn_ThePacks = KnifeCMS.Data.RegSubMatches(Fn_CartJSONString,"""thepacks"":\[([\s\S]*?)\]")(0,0)
		Fn_Skus  = KnifeCMS.Data.RegSubMatches(Fn_TheSkus,"\{([\s\S]*?)\}") 'Echo Ubound(Fn_Skus,2) &"<br>"
		Fn_Packs = KnifeCMS.Data.RegSubMatches(Fn_ThePacks,"\{([\s\S]*?)\}")	
		'计算单独商品
		For Fn_ii=0 To Ubound(Fn_Skus,2)
			Fn_ID   = KnifeCMS.Data.CLng(KnifeCMS.Data.RegSubMatches(Fn_Skus(0,Fn_ii),"""id"":""([\s\S]*?)""")(0,0))
			Fn_Num  = KnifeCMS.Data.CLng(KnifeCMS.Data.RegSubMatches(Fn_Skus(0,Fn_ii),"""num"":""([\s\S]*?)""")(0,0))
			Fn_AttrID = KnifeCMS.Data.RegSubMatches(Fn_Skus(0,Fn_ii),"""attr"":""([\s\S]*?)""")(0,0)
			ReDim Fn_GoodsAttrConfig(3)
			Fn_TempAttrID        = ""
			Fn_TempAttrValue     = "" '商品所有属性名称      形式：(M|黑色|皮带)
			Fn_GoodsPrice        = 0  '商品销售价
			Fn_AttrPriceAmount   = 0  '商品所有属性的价格总和
			Fn_Price             = 0  '商品价格（Fn_GoodsPrice + Fn_AttrPriceAmount）
			Fn_Weight            = 0
			Fn_GoodsName         = "" '商品名称(属性)
			Fn_GoodsID           = Fn_ID
			Fn_GoodsExit         = true
			If Fn_GoodsID>0 And Fn_Num>0 Then
				Select Case DB_Type
				Case 0
				Fn_SQL  = "SELECT GoodsName,Price,GoodsBN,IIF(LEN(Thumbnail)=0,FirstImg,Thumbnail),Weight,SeoUrl FROM ["& DBTable_Goods &"] WHERE ID="& Fn_GoodsID &""
				Case 1
				Fn_SQL  = "SELECT GoodsName,Price,GoodsBN,CASE WHEN LEN(Thumbnail)=0 THEN FirstImg ELSE Thumbnail END,Weight,SeoUrl FROM ["& DBTable_Goods &"] WHERE ID="& Fn_GoodsID &""
				End Select
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_SQL)
				If Not(Fn_Rs.Eof) Then 
					Fn_GoodsName  = Fn_Rs(0)
					Fn_GoodsPrice = Fn_Rs(1) '商品销售价
					Fn_GoodsBN    = Fn_Rs(2) '商品编号
					Fn_Thumbnail  = Fn_Rs(3) '商品缩略图
					Fn_Weight     = Fn_Rs(4) '商品重量
					Fn_SeoUrl     = KnifeCMS.Data.OKData(Fn_Rs(5))
				Else
					Fn_GoodsExit  = false
				End If
				KnifeCMS.DB.CloseRs Fn_Rs
				'如果主商品存在则继续
				If Fn_GoodsExit Then
					If Not(KnifeCMS.Data.IsNul(Fn_AttrID)) Then
						Fn_GoodsAttrConfig = GetGoodsAttrConfig(Fn_GoodsID,Fn_AttrID)
						Fn_TempAttrID      = Fn_GoodsAttrConfig(0)
						Fn_TempAttrValue   = Fn_GoodsAttrConfig(1)
						Fn_AttrPriceAmount = Fn_GoodsAttrConfig(2)
					End If
					Fn_Price = Fn_GoodsPrice + Fn_AttrPriceAmount '最后的商品价格（商品销售价+商品所有属性的价格总和）
					Fn_TempString = "{""goodsid"":"""& Fn_GoodsID &""",""seourl"":"""& KnifeCMS.Data.Escape(Fn_SeoUrl) &""",""goodsattr"":"""& Replace(Fn_TempAttrID,",","|") &""",""goodsattrvalue"":"""& KnifeCMS.Data.Escape(Fn_TempAttrValue) &""",""goodsname"":"""& KnifeCMS.Data.Escape(Fn_GoodsName) &""",""goodsbn"":"""& Fn_GoodsBN &""",""thumbnail"":"""& KnifeCMS.Data.Escape(Fn_Thumbnail) &""",""goodsweight"":"""& Fn_Weight &""",""goodsprice"":"""& KnifeCMS.Data.FormatCurrency(Fn_GoodsPrice) &""",""attrprice"":"""& KnifeCMS.Data.FormatCurrency(Fn_AttrPriceAmount) &""",""num"":"""& Fn_Num &""",""amount"":"""& KnifeCMS.Data.FormatCurrency(Fn_Price * Fn_Num) &""",""score"":""0""}"
					If Fn_TheSkusString = "" Then Fn_TheSkusString = Fn_TempString : Else Fn_TheSkusString = Fn_TheSkusString &","& Fn_TempString
				End If
				'如果主商品存在则继续:end
			End If
			Fn_CartWeight = Fn_CartWeight + KnifeCMS.Data.FormatDouble(Fn_Weight * Fn_Num)
			Fn_Cost_Item = Fn_Cost_Item + KnifeCMS.Data.FormatDouble(Fn_Price * Fn_Num)
		Next
		'计算套装
		Dim Fn_AdjunctExit,Fn_AdjunctID,Fn_AdjunctName,Fn_AdjunctType,Fn_MinNum,Fn_MaxNum,Fn_PriceType,Fn_AdjunctPrice,Fn_GoodsAdjunctPrice
		Dim Fn_PacksPrice,Fn_PacksWeight
		Dim Fn_SelectGoods,Fn_TempSelectGoodsID,Fn_Packs_SelectGoodsJson,Fn_Packs_GoodsJson,Fn_Packs_GoodsPrice,Fn_Packs_GoodsWeight
		Dim Fn_TempGoodsNum
		For Fn_ii=0 To Ubound(Fn_Packs,2)
			Fn_ID     = KnifeCMS.Data.CLng(KnifeCMS.Data.RegSubMatches(Fn_Packs(0,Fn_ii),"""id"":""([\s\S]*?)""")(0,0))
			Fn_Num    = KnifeCMS.Data.CLng(KnifeCMS.Data.RegSubMatches(Fn_Packs(0,Fn_ii),"""num"":""([\s\S]*?)""")(0,0))
			Fn_AttrID       = KnifeCMS.Data.RegSubMatches(Fn_Packs(0,Fn_ii),"""attr"":""([\s\S]*?)""")(0,0)
			Fn_SelectGoods  = KnifeCMS.Data.RegSubMatches(Fn_Packs(0,Fn_ii),"""selectgoodsid"":""([\s\S]*?)""")(0,0)
			ReDim Fn_GoodsAttrConfig(3)
			Fn_TempAttrID        = ""
			Fn_TempAttrValue     = "" '商品所有属性名称      形式：(M|黑色|皮带)
			Fn_GoodsPrice        = 0  '商品销售价
			Fn_AttrPriceAmount   = 0  '商品所有属性的价格总和
			Fn_Price             = 0  '商品价格（Fn_GoodsPrice + Fn_AttrPriceAmount）
			Fn_Weight            = 0
			Fn_GoodsName         = "" '商品名称(属性)
			Fn_AdjunctID         = Fn_ID
			Fn_GoodsID           = 0
			Fn_GoodsExit         = true
			Fn_AdjunctExit       = true
			Fn_Packs_SelectGoodsJson   = ""
			Fn_PacksWeight       = 0
			Fn_TempGoodsNum      = 0
			'获取配件组信息(即套装信息)
			If Fn_AdjunctID>0 AND Fn_SelectGoods<>"" Then
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT GoodsID,AdjunctName,AdjunctType,MinNum,MaxNum,PriceType,Price FROM ["& DBTable_GoodsAdjunct &"] WHERE ID="& Fn_AdjunctID &"")
				If Not(Fn_Rs.Eof) Then 
					Fn_GoodsID      = Fn_Rs(0)
					Fn_AdjunctName  = Fn_Rs(1) '配件组名称（套装名称）
					Fn_AdjunctType  = Fn_Rs(2) '配件组类型
					Fn_MinNum       = Fn_Rs(3) '最小购买量
					Fn_MaxNum       = Fn_Rs(4) '最大购买量
					Fn_PriceType    = Fn_Rs(5) '优惠方式
					Fn_AdjunctPrice = Fn_Rs(6) '优惠额度
				Else
					Fn_AdjunctExit = false
				End If
				KnifeCMS.DB.CloseRs Fn_Rs
			End If
			Fn_MinNum = KnifeCMS.Data.CLng(Fn_MinNum)
			Fn_MaxNum = KnifeCMS.Data.CLng(Fn_MaxNum)
			Fn_PriceType  = KnifeCMS.Data.CLng(Fn_PriceType)
			'如果配件组(套装)存在则继续
			If Fn_AdjunctExit And Fn_GoodsID>0 Then
				Select Case DB_Type
				Case 0
				Fn_SQL  = "SELECT GoodsName,Price,GoodsBN,IIF(LEN(Thumbnail)=0,FirstImg,Thumbnail),Weight,SeoUrl FROM ["& DBTable_Goods &"] WHERE ID="& Fn_GoodsID &""
				Case 1
				Fn_SQL  = "SELECT GoodsName,Price,GoodsBN,CASE WHEN LEN(Thumbnail)=0 THEN FirstImg ELSE Thumbnail END,Weight,SeoUrl FROM ["& DBTable_Goods &"] WHERE ID="& Fn_GoodsID &""
				End Select
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_SQL)
				If Not(Fn_Rs.Eof) Then 
					Fn_GoodsName  = Fn_Rs(0)
					Fn_GoodsPrice = Fn_Rs(1) '商品销售价
					Fn_GoodsBN    = Fn_Rs(2) '商品编号
					Fn_Thumbnail  = Fn_Rs(3) '商品缩略图
					Fn_Weight     = Fn_Rs(4) '商品重量
					Fn_SeoUrl     = Fn_Rs(5)
				Else
					Fn_GoodsExit  = false
				End If
				KnifeCMS.DB.CloseRs Fn_Rs
				'如果主商品存在则继续
				If Fn_GoodsExit Then
					If Not(KnifeCMS.Data.IsNul(Fn_AttrID)) Then
						Fn_GoodsAttrConfig = GetGoodsAttrConfig(Fn_GoodsID,Fn_AttrID)
						Fn_TempAttrID      = Fn_GoodsAttrConfig(0)
						Fn_TempAttrValue   = Fn_GoodsAttrConfig(1)
						Fn_AttrPriceAmount = Fn_GoodsAttrConfig(2)
					End If
					'Fn_GoodsPrice = Fn_GoodsPrice + Fn_AttrPriceAmount
					Fn_Price = Fn_GoodsPrice + Fn_AttrPriceAmount '最后的商品价格（商品销售价+商品所有属性的价格总和）
					'===========获取套装商品信息==========
					Fn_PacksPrice        = Fn_Price
					Fn_PacksWeight       = Fn_Weight
					Fn_SelectGoods       = Split(Fn_SelectGoods,"|")
					Fn_TempSelectGoodsID = ""
					For Fn_jj=0 To Ubound(Fn_SelectGoods)
						If Fn_TempSelectGoodsID="" Then Fn_TempSelectGoodsID = KnifeCMS.Data.CLng(Fn_SelectGoods(Fn_jj)) : Else Fn_TempSelectGoodsID = Fn_TempSelectGoodsID&","& KnifeCMS.Data.CLng(Fn_SelectGoods(Fn_jj))
					Next
					Fn_SelectGoods = Fn_TempSelectGoodsID
					Select Case DB_Type
					Case 0
					Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT b.ID,b.GoodsName,b.GoodsBN,b.Price,b.Weight,IIF(LEN(b.Thumbnail)=0,b.FirstImg,b.Thumbnail),b.SeoUrl FROM ["& DBTable_GoodsAdjunctGoods &"] a LEFT JOIN ["& DBTable_Goods &"] b ON (b.ID=a.AdjunctGoodsID) WHERE a.GoodsID = "& Fn_GoodsID &" AND a.AdjunctID="& Fn_AdjunctID &" AND a.AdjunctGoodsID IN("& Fn_SelectGoods &")")
					Case 1
					Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT b.ID,b.GoodsName,b.GoodsBN,b.Price,b.Weight,CASE WHEN LEN(b.Thumbnail)=0 THEN b.FirstImg ELSE b.Thumbnail END,b.SeoUrl FROM ["& DBTable_GoodsAdjunctGoods &"] a LEFT JOIN ["& DBTable_Goods &"] b ON (b.ID=a.AdjunctGoodsID) WHERE a.GoodsID = "& Fn_GoodsID &" AND a.AdjunctID="& Fn_AdjunctID &" AND a.AdjunctGoodsID IN("& Fn_SelectGoods &")")
					End Select
					
					If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then Fn_DataArray = Fn_Rs.GetRows() Else Fn_DataArray = ""
					KnifeCMS.DB.CloseRs Fn_Rs
					If IsArray(Fn_DataArray) Then
						For Fn_i=0 To Ubound(Fn_DataArray,2)
							Fn_TempGoodsNum     = Fn_TempGoodsNum + 1
							'如果选择的配件商品大于最大购买量，则选择前Fn_MaxNum个商品计算
							If Fn_TempGoodsNum>Fn_MaxNum And Fn_MaxNum>0 Then Exit For
							Fn_Packs_GoodsPrice = Fn_DataArray(3,Fn_i)
							If Fn_PriceType > 0 Then
								Fn_GoodsAdjunctPrice = Fn_Packs_GoodsPrice * Fn_AdjunctPrice '优惠价为某个折扣
							Else
								Fn_GoodsAdjunctPrice = Fn_Packs_GoodsPrice - Fn_AdjunctPrice '优惠一定金额
							End If
							Fn_PacksPrice        = Fn_PacksPrice + Fn_GoodsAdjunctPrice
							Fn_Packs_GoodsWeight = Fn_DataArray(4,Fn_i)
							Fn_PacksWeight       = Fn_PacksWeight + Fn_Packs_GoodsWeight
							Fn_Packs_GoodsSeoUrl = Fn_DataArray(6,Fn_i)
							Fn_Packs_GoodsJson   = "{""goodsid"":"""& Fn_DataArray(0,Fn_i) &""",""seourl"":"""& KnifeCMS.Data.Escape(Fn_Packs_GoodsSeoUrl) &""",""goodsname"":"""& KnifeCMS.Data.Escape(Fn_DataArray(1,Fn_i)) &""",""goodsbn"":"""& Fn_DataArray(2,Fn_i) &""",""goodsprice"":"""& KnifeCMS.Data.FormatCurrency(Fn_Packs_GoodsPrice) &""",""adjunctprice"":"""& KnifeCMS.Data.FormatCurrency(Fn_GoodsAdjunctPrice) &""",""goodsweight"":"""& Fn_Packs_GoodsWeight &""",""thumbnail"":"""& KnifeCMS.Data.Escape(Fn_DataArray(5,Fn_i)) &""",""score"":""0""}"
							If Fn_Packs_SelectGoodsJson="" Then Fn_Packs_SelectGoodsJson=Fn_Packs_GoodsJson : Else Fn_Packs_SelectGoodsJson=Fn_Packs_SelectGoodsJson &","& Fn_Packs_GoodsJson
						Next
					End If
					'=====================================
					'如果选择的配件商品小于最小购买量，则此配件（套装）无效
					If Fn_TempGoodsNum < Fn_MinNum And Fn_MinNum>0 Then
						Fn_TempString = ""
					Else
						Fn_TempString = "{""goodsid"":"""& Fn_GoodsID &""",""seourl"":"""& KnifeCMS.Data.Escape(Fn_SeoUrl) &""",""goodsattr"":"""& Replace(Fn_TempAttrID,",","|") &""",""goodsattrvalue"":"""& KnifeCMS.Data.Escape(Fn_TempAttrValue) &""",""goodsname"":"""& KnifeCMS.Data.Escape(Fn_GoodsName) &""",""goodsbn"":"""& Fn_GoodsBN &""",""thumbnail"":"""& KnifeCMS.Data.Escape(Fn_Thumbnail) &""",""goodsweight"":"""& Fn_Weight &""",""goodsprice"":"""& KnifeCMS.Data.FormatCurrency(Fn_GoodsPrice) &""",""attrprice"":"""& KnifeCMS.Data.FormatCurrency(Fn_AttrPriceAmount) &""",""packsid"":"""& Fn_AdjunctID &""",""packsname"":"""& KnifeCMS.Data.Escape(Fn_AdjunctName) &""",""packsweight"":"""& Fn_PacksWeight &""",""packsprice"":"""& KnifeCMS.Data.FormatCurrency(Fn_PacksPrice) &""",""num"":"""& Fn_Num &""",""amount"":"""& KnifeCMS.Data.FormatCurrency(Fn_PacksPrice * Fn_Num) &""",""score"":""0"",""selectgoodsid"":"""& Replace(Fn_SelectGoods,",","|") &""",""selectgoods"":["& Fn_Packs_SelectGoodsJson &"]}"
						If Fn_ThePacksString = "" Then Fn_ThePacksString = Fn_TempString : Else Fn_ThePacksString = Fn_ThePacksString &","& Fn_TempString
					End If
				End If
				'如果主商品存在则继续:end
			End If
			'如果配件组(套装)存在则继续:end
			Fn_CartWeight = Fn_CartWeight + (Fn_PacksWeight * Fn_Num)
			Fn_Cost_Item = Fn_Cost_Item + KnifeCMS.Data.FormatCurrency(Fn_PacksPrice * Fn_Num)
		Next
		
		Fn_ReturnString = "{""theskus"":["& Fn_TheSkusString &"],""thepacks"":["& Fn_ThePacksString &"],""weight"":"""& Fn_CartWeight &""",""amount"":"""& KnifeCMS.Data.FormatCurrency(Fn_Cost_Item) &"""}"
		
		GetCart = Fn_ReturnString
	End Function
	
	Public Function GetGoodsAttrConfig(ByVal BV_GoodsID, ByVal BV_AttrIDString)
		Dim Fn_jj,Fn_AttrID,Fn_AttrValue,Fn_AttrPrice,Fn_TempAttrID,Fn_TempAttrValue,Fn_AttrPriceAmount
		Dim Fn_Rs,Fn_SQL
		Dim Fn_ReturnArray(3)
		Fn_TempAttrID      = ""
		Fn_TempAttrValue   = ""
		Fn_AttrPriceAmount = 0
		'重新处理商品属性ID
		BV_AttrIDString = Split(BV_AttrIDString,"|")
		For Fn_jj=0 To Ubound(BV_AttrIDString)
			Fn_AttrID = KnifeCMS.Data.CLng(BV_AttrIDString(Fn_jj))
			If Fn_AttrID>0 Then
				Fn_SQL    = "SELECT AttrValue,AttrPrice FROM ["& DBTable_GoodsAttr &"] WHERE GoodsID="& BV_GoodsID &" AND ID="& Fn_AttrID &""
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_SQL)
				If Not(Fn_Rs.Eof) Then
					Fn_AttrValue = KnifeCMS.Data.UnEscape(Fn_Rs(0))
					Fn_AttrPrice = Fn_Rs(1)
					If Fn_TempAttrID   ="" Then Fn_TempAttrID    = Fn_AttrID    : Else Fn_TempAttrID    = Fn_TempAttrID &","& Fn_AttrID
					If Fn_TempAttrValue="" Then Fn_TempAttrValue = Fn_AttrValue : Else Fn_TempAttrValue = Fn_TempAttrValue &"|"& Fn_AttrValue
					'计算属性价格
					Fn_AttrPriceAmount = Fn_AttrPriceAmount + Fn_AttrPrice
				End If
				KnifeCMS.DB.CloseRs Fn_Rs
			End If
		Next
		Fn_ReturnArray(0)  = Fn_TempAttrID
		Fn_ReturnArray(1)  = Fn_TempAttrValue
		Fn_ReturnArray(2)  = Fn_AttrPriceAmount
		GetGoodsAttrConfig = Fn_ReturnArray
	End Function
	
	'提交订单
	Public Function PlaceOrder()
		Dim Fn_Rs,Fn_Sql,Fn_ii,Fn_kk,Fn_jj,Fn_Result,Fn_ReturnString,Fn_SysID,Fn_Temp,Fn_TempArray
		Dim Fn_Error(10)
		    Fn_Error(0) = "{""errorinfo"":""order_exist""}"
			Fn_Error(1) = "{""errorinfo"":""region_is_error""}"
			Fn_Error(2) = "{""errorinfo"":""address_is_null""}"
			Fn_Error(3) = "{""errorinfo"":""name_is_null""}"
			Fn_Error(4) = "{""errorinfo"":""tel_is_error""}"
			Fn_Error(5) = "{""errorinfo"":""shippingid_not_exist""}"
			Fn_Error(6) = "{""errorinfo"":""shipping_is_error""}"
			Fn_Error(7) = ""
			
		Fn_SysID = KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","cartsysid"),"[^0-9\-]","")
		If Len(Fn_SysID)<>32 Then PlaceOrder="" : Exit Function
		'==检查是否存在该订单(防止重刷新提交数据)
		If KnifeCMS.DB.CheckIsRecordExistByField(DBTable_Order,Array("SysID:"& Fn_SysID)) Then PlaceOrder=Fn_Error(0) : Exit Function
		
		'==获取并验证收货人信息
		Dim Fn_Ship_Area,Fn_Ship_Address,Fn_Ship_Zip,Fn_Ship_Name,Fn_Ship_Mobile,Fn_Ship_Tel,Fn_Ship_Email,Fn_Mark_Text
		Dim Fn_Ship_AreaGrade,Fn_Ship_AreaArray
		for each Fn_Temp in Request.Form("ship_area")
			Fn_Ship_Area = trim(Fn_Temp)
		next
		Fn_Ship_Area     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Fn_Ship_Area,"[^0-9,]",""),50)
		Fn_Ship_Area     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Fn_Ship_Area,"([,]+?)",","),50)
		Fn_Ship_Address  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ship_address")),250)
		Fn_Ship_Zip      = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ship_zip"),"[^0-9\-]",""),20)
		Fn_Ship_Name     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ship_name")),50)
		Fn_Ship_Tel      = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ship_tel"),"[^0-9\-]",""),50)
		Fn_Ship_Mobile   = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ship_mobile"),"[^0-9\-]",""),50)
		Fn_Ship_Email    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ship_email")),60)
		If Not(KnifeCMS.Data.IsEmail(Fn_Ship_Email)) Then Fn_Ship_Email=""
		Fn_Mark_Text  = KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(KnifeCMS.GetForm("post","mark_text")),250)
		
		If Len(Fn_Ship_Area)<3 Then PlaceOrder=Fn_Error(1) : Exit Function
		Fn_TempArray      = Split(Fn_Ship_Area,",")
		Fn_Ship_AreaGrade = Ubound(Fn_TempArray)-1
		If Fn_Ship_AreaGrade<1 Then PlaceOrder=Fn_Error(1) : Exit Function
		ReDim Fn_Ship_AreaArray(Fn_Ship_AreaGrade)
		for Fn_ii=0 To Fn_Ship_AreaGrade
			if Fn_ii=0 Then
				Fn_Ship_AreaArray(0) = ","& Fn_TempArray(Fn_ii+1) &","
			else
				Fn_Ship_AreaArray(Fn_ii) = Fn_Ship_AreaArray(Fn_ii-1) & Fn_TempArray(Fn_ii+1) &","
			end if
		Next
		Fn_Ship_Area = Fn_Ship_AreaArray(Fn_Ship_AreaGrade-1)
		If KnifeCMS.Data.IsNul(Fn_Ship_Address) Then PlaceOrder=Fn_Error(2) : Exit Function
		If KnifeCMS.Data.IsNul(Fn_Ship_Name) Then PlaceOrder=Fn_Error(3) : Exit Function
		If Len(Fn_Ship_Tel)<6 And Len(Fn_Ship_Mobile)<6 Then PlaceOrder=Fn_Error(4) : Exit Function
		
		'==是否保存收货人地址
		If KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","saveaddress"))=1 Then Call OS.SaveAddress(UserID,Fn_Ship_Area,Fn_Ship_Address,Fn_Ship_Zip,Fn_Ship_Name,Fn_Ship_Tel,Fn_Ship_Mobile)
		
		'==获取购物车内的商品信息,如果没有商品则退出
		Dim Fn_Cart,Fn_CartJSON,Fn_TempShipFee,Fn_DeliveryExist
		Fn_Cart = GetCart() ' :Die Fn_Cart
		If Len(Fn_Cart)<10 Then PlaceOrder="" : Exit Function
		Set Fn_CartJSON = KnifeCMS.JSON.Parse(Fn_Cart)
		If (Fn_CartJSON.theskus.length + Fn_CartJSON.thepacks.length)<1 Then PlaceOrder="" : Exit Function
		'有商品则继续
		Dim Fn_OrderID,Fn_ShippingID,Fn_Shipping,Fn_PaymentID,Fn_Payment,Fn_ShippingArea
			Fn_PaymentID    = 0
			Fn_ShippingArea = KnifeCMS.Data.Left(OS.GetRegionByPath(Fn_Ship_Area),250) '配送地区
			
		Dim Fn_CartWeight    : Fn_CartWeight   = 0  '商品总重量
		Dim Fn_Cost_Item     : Fn_Cost_Item    = 0  '商品总金额
		Dim Fn_Cost_Freight  : Fn_Cost_Freight = 0  '配送费用
		Dim Fn_Protect       : Fn_Protect      = 0  '是否保价
		Dim Fn_Cost_Protect  : Fn_Cost_Protect = 0  '物流保价费[商品总金额 * 保价费率]
		Dim Fn_Cost_Tax      : Fn_Cost_Tax     = 0  '税金[商品总金额 * 税率]
		Dim Fn_Total_Amount  : Fn_Total_Amount = 0  '订单总金额
		
		Fn_CartWeight = KnifeCMS.Data.FormatDouble(Fn_CartJSON.weight)
		Fn_Cost_Item  = KnifeCMS.Data.FormatDouble(Fn_CartJSON.amount)
		
		'==根据配送方式（送货方式）计算配送费用
		
		'Dim Fn_AreaFeeSet,Fn_SetAreaDefaultFee,Fn_Area,,Fn_ProtectRate,Fn_ProtectMinPrice,Fn_IsInDeliveryRegion
		'Dim Fn_DeliveryExist '所选择的配送方式是否合法存在
		    'Fn_DeliveryExist = false
		Fn_ShippingID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","delivery[shipping_id]"))
		If Not(Fn_ShippingID>0) Or Not(KnifeCMS.DB.CheckIsRecordExistByField(DBTable_Delivery,Array("ID:"& Fn_ShippingID))) Then PlaceOrder=Fn_Error(5) : Exit Function
		'配送方式ID有效并在数据库中存在则继续
		Fn_TempShipFee = OS.GetShippingFee(Fn_ShippingID,Fn_Cost_Item,Fn_CartWeight,Fn_Ship_AreaArray,Fn_Ship_AreaGrade)
		Fn_DeliveryExist = Fn_TempShipFee(0) '所选择的配送方式是否合法存在
		
		If Fn_DeliveryExist=false Then PlaceOrder=Fn_Error(6) : Exit Function
		Fn_Cost_Freight = Fn_TempShipFee(1)
		Fn_Protect      = Fn_TempShipFee(2)
		Fn_Cost_Protect = Fn_TempShipFee(3)
		'配送方式合法则继续
		'==取得配送方式名称
		Fn_Shipping = KnifeCMS.DB.GetFieldByID(DBTable_Delivery,"DeliveryName",Fn_ShippingID)
		'==计算发票税金
		Dim Fn_IsTax      : Fn_IsTax      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","payment[is_tax]"))
		Dim Fn_TaxCompany : Fn_TaxCompany = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","tax_company")),100) '发票抬头
		If Fn_IsTax=1 Then
			If SiteIsTax = 1 Then Fn_Cost_Tax = Fn_Cost_Item * KnifeCMS.Data.FormatDouble(SiteTaxRate) * 1/100
		End If
		'==计算订单总金额
		Fn_Total_Amount  = Fn_Cost_Item + Fn_Cost_Freight + Fn_Cost_Tax + Fn_Cost_Protect
		
		Fn_CartWeight    = KnifeCMS.Data.FormatCurrency(Fn_CartWeight)
		Fn_Cost_Item     = KnifeCMS.Data.FormatCurrency(Fn_Cost_Item)
		Fn_Cost_Freight  = KnifeCMS.Data.FormatCurrency(Fn_Cost_Freight)
		Fn_Cost_Protect  = KnifeCMS.Data.FormatCurrency(Fn_Cost_Protect)
		Fn_Cost_Tax      = KnifeCMS.Data.FormatCurrency(Fn_Cost_Tax)
		Fn_Total_Amount  = KnifeCMS.Data.FormatCurrency(Fn_Total_Amount)
		
		'生成唯一的订单ID(16位)
		Fn_OrderID = CreateOrderID()
		'====创建订单===========
		Dim Fn_DeliveryDay,Fn_DeliverySpecalDay,Fn_DeliveryTime
		Dim Fn_TheSkusNum,Fn_ThePacksNum,Fn_ItemNum,Fn_Tostr,Fn_ShipTime
		Fn_TheSkusNum  = KnifeCMS.DAta.CLng(Fn_CartJSON.theskus.length)   '单独商品数
		Fn_ThePacksNum = KnifeCMS.DAta.CLng(Fn_CartJSON.thepacks.length)  '套装商品数
		Fn_ItemNum     = 0  '商品数量(一个套装不算一个商品，而应该把套装内的商品数都算上)
		Fn_Tostr       = "" '商品描述
		Fn_DeliveryDay       = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(Trim(KnifeCMS.GetForm("post","delivery[day]"))),100)
		Fn_DeliverySpecalDay = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(Trim(KnifeCMS.GetForm("post","delivery[specal_day]"))),100)
		Fn_DeliveryTime      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(Trim(KnifeCMS.GetForm("post","delivery[time]"))),100)
		Fn_ShipTime    = KnifeCMS.IIF(Fn_DeliveryDay="specal", Fn_DeliverySpecalDay &"["& Fn_DeliveryTime &"]",Fn_DeliveryDay &"["& Fn_DeliveryTime &"]" ) '送货日期-时间段
		
		Fn_Result = KnifeCMS.DB.AddRecord(DBTable_Order,Array("SysID:"& Fn_SysID,"OrderID:"& Fn_OrderID,"UserID:"& UserID,"Confirm:0","Status:1","PayStatus:0","ShipStatus:0","UserStatus:0","IsDelivery:1","ShippingID:"& Fn_ShippingID,"Shipping:"& Fn_Shipping,"ShippingArea:"& Fn_ShippingArea,"PaymentID:"& Fn_PaymentID,"Payment:"& Fn_Payment,"Weight:"& Fn_CartWeight,"Tostr:"& Fn_Tostr,"ItemNum:0","Acttime:"& SysTime,"Createtime:"& SysTime,"IP:"& KnifeCMS.Data.ParseIP(KnifeCMS.GetClientIP),"Ship_Name:"& Fn_Ship_Name,"Ship_Area:"& Fn_Ship_Area,"Ship_Address:"& Fn_Ship_Address,"Ship_Zip:"& Fn_Ship_Zip,"Ship_Tel:"& Fn_Ship_Tel,"Ship_Mobile:"& Fn_Ship_Mobile,"Ship_Email:"& Fn_Ship_Email,"Ship_Time:"& Fn_ShipTime,"IsTax:"& Fn_IsTax,"Tax_Company:"& Fn_TaxCompany,"IsProtect:"& Fn_Protect,"Currency:CNY","Cur_Rate:1","Score_u:0","Score_g:0","Use_Pmt:","Cost_Item:"& Fn_Cost_Item,"Cost_Freight:"& Fn_Cost_Freight,"Cost_Protect:"& Fn_Cost_Protect,"Cost_Tax:"& Fn_Cost_Tax,"Pmt_Amount:0","Cost_Payment:0","Discount:0","Total_Amount:"& Fn_Total_Amount,"Final_Amount:"& Fn_Total_Amount,"Payed:0","Markstar:0","Memo:","Print_Status:0","Mark_Text:"& Fn_Mark_Text,"LastChangTime:"& SysTime,"Use_RegisterInfo:1","Mark_Type:b1","Recycle:0"))
		If Fn_Result Then
			'=======创建日志=========
			Dim Fn_LogText,Fn_BeHavior
			Fn_LogText  = Lang_OrderLogText(0)     '"创建订单"
			Fn_BeHavior = Lang_OrderLogBehavior(0) '"添加"
			Call KnifeCMS.DB.AddRecord(DBTable_OrderLog,Array("OrderID:"& Fn_OrderID,"UserID:"& UserID,"SysUserID:NULL","OperaterName:"& Username,"LogText:" & Fn_LogText,"AddTime:"& SysTime,"BeHavior:" & Fn_BeHavior,"Result:success"))
			
			'==保存订单商品记录===============
			'Die "--购物车数据="& Fn_Cart &"=========-<br>"
			'保存订单单独商品记录
			Dim Fn_Config,Fn_GoodsID,Fn_GoodsTypeKey,Fn_GoodsBN,Fn_GoodsName,Fn_GoodsAttr,Fn_GoodsAttrValue,Fn_GoodsPrice,Fn_AttrPrice,Fn_GoodsWeight
			Dim Fn_Cost,Fn_Price,Fn_Weight,Fn_Nums,Fn_Score,Fn_Amount,Fn_Delivery_Status
			Dim Fn_PacksID,Fn_PacksName,Fn_PacksWeight,Fn_PacksPrice
			For Fn_ii=0 To Fn_CartJSON.theskus.length-1
				Fn_Config         = KnifeCMS.JSON.Stringify(Fn_CartJSON.theskus.get(Fn_ii))
				
				Fn_GoodsID        = KnifeCMS.Data.CLng(Fn_CartJSON.theskus.get(Fn_ii).goodsid)
				Fn_GoodsTypeKey   = 1
				Fn_GoodsBN        = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Fn_CartJSON.theskus.get(Fn_ii).goodsbn,"[^0-9a-zA-Z]",""),32)
				Fn_GoodsName      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.Data.UnEscape(Fn_CartJSON.theskus.get(Fn_ii).goodsname)),250)
				Fn_GoodsAttr      = KnifeCMS.Data.UnEscape(Fn_CartJSON.theskus.get(Fn_ii).goodsattr)
				Fn_GoodsAttrValue = KnifeCMS.Data.HTMLEncode(KnifeCMS.Data.UnEscape(Fn_CartJSON.theskus.get(Fn_ii).goodsattrvalue))
				Fn_GoodsPrice     = KnifeCMS.Data.FormatDouble(Fn_CartJSON.theskus.get(Fn_ii).goodsprice)
				Fn_AttrPrice      = KnifeCMS.Data.FormatDouble(Fn_CartJSON.theskus.get(Fn_ii).attrprice)
				Fn_GoodsWeight    = KnifeCMS.Data.FormatDouble(Fn_CartJSON.theskus.get(Fn_ii).goodsweight)
				
				Fn_Cost           = 0
				Fn_Price          = Fn_GoodsPrice + Fn_AttrPrice
				Fn_Weight         = Fn_GoodsWeight
				Fn_Nums           = KnifeCMS.Data.CLng(Fn_CartJSON.theskus.get(Fn_ii).num)
				Fn_Amount         = KnifeCMS.Data.FormatDouble(Fn_CartJSON.theskus.get(Fn_ii).amount)
				Fn_Score          = KnifeCMS.Data.CLng(Fn_CartJSON.theskus.get(Fn_ii).score)
				Fn_Delivery_Status= 0
				If Fn_GoodsID > 0 Then
					'Fn_Cost   = KnifeCMS.Data.FormatDouble(KnifeCMS.DB.GetFieldByID(DBTable_Goods,"Cost",Fn_GoodsID))
					Fn_Result = KnifeCMS.DB.AddRecord(DBTable_OrderGoods,Array("OrderID:"& Fn_OrderID,"Config:"& Fn_Config,"GoodsID:"& Fn_GoodsID,"GoodsTypeKey:"& Fn_GoodsTypeKey,"GoodsBN:"& Fn_GoodsBN,"GoodsName:"& Fn_GoodsName,"GoodsAttr:"& Fn_GoodsAttr,"GoodsAttrValue:"& Fn_GoodsAttrValue,"GoodsPrice:"& Fn_GoodsPrice,"AttrPrice:"& Fn_AttrPrice,"GoodsWeight:"& Fn_GoodsWeight,"ItemNum:1","Cost:"& Fn_Cost,"Price:"& Fn_Price,"Weight:"& Fn_Weight,"Nums:"& Fn_Nums,"Amount:"& Fn_Amount,"Score:"& Fn_Score,"Delivery_Status:"& Fn_Delivery_Status,"SendNums:0"))
					Fn_ItemNum = Fn_ItemNum + 1
				End If
				'Echo "<br>===Fn_GoodsName:" & Fn_GoodsName &"<br>"
			Next
			'保存订单套装商品记录
			For Fn_ii=0 To Fn_CartJSON.thepacks.length-1
				Fn_Config         = KnifeCMS.JSON.Stringify(Fn_CartJSON.thepacks.get(Fn_ii))
				
				Fn_PacksID        = KnifeCMS.Data.CLng(Fn_CartJSON.thepacks.get(Fn_ii).packsid)
				Fn_PacksName      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.Data.UnEscape(Fn_CartJSON.thepacks.get(Fn_ii).packsname)),250)
				Fn_PacksWeight    = KnifeCMS.Data.FormatDouble(Fn_CartJSON.thepacks.get(Fn_ii).packsweight)
				Fn_PacksPrice     = KnifeCMS.Data.FormatDouble(Fn_CartJSON.thepacks.get(Fn_ii).packsprice)
				
				Fn_GoodsID        = KnifeCMS.Data.CLng(Fn_CartJSON.thepacks.get(Fn_ii).goodsid)
				Fn_GoodsTypeKey   = 1
				Fn_GoodsBN        = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Fn_CartJSON.thepacks.get(Fn_ii).goodsbn,"[^0-9a-zA-Z]",""),32)
				Fn_GoodsName      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.Data.UnEscape(Fn_CartJSON.thepacks.get(Fn_ii).goodsname)),250)
				Fn_GoodsAttr      = KnifeCMS.Data.UnEscape(Fn_CartJSON.thepacks.get(Fn_ii).goodsattr)
				Fn_GoodsAttrValue = KnifeCMS.Data.HTMLEncode(KnifeCMS.Data.UnEscape(Fn_CartJSON.thepacks.get(Fn_ii).goodsattrvalue))
				Fn_GoodsPrice     = KnifeCMS.Data.FormatDouble(Fn_CartJSON.thepacks.get(Fn_ii).goodsprice)
				Fn_AttrPrice      = KnifeCMS.Data.FormatDouble(Fn_CartJSON.thepacks.get(Fn_ii).attrprice)
				Fn_GoodsWeight    = KnifeCMS.Data.FormatDouble(Fn_CartJSON.thepacks.get(Fn_ii).goodsweight)
				
				Fn_Cost           = 0
				Fn_Price          = Fn_PacksPrice
				Fn_Weight         = Fn_PacksWeight
				Fn_Nums           = KnifeCMS.Data.CLng(Fn_CartJSON.thepacks.get(Fn_ii).num)
				Fn_Amount         = KnifeCMS.Data.FormatDouble(Fn_CartJSON.thepacks.get(Fn_ii).amount)
				Fn_Score          = KnifeCMS.Data.CLng(Fn_CartJSON.thepacks.get(Fn_ii).score)
				Fn_Delivery_Status= 0
				
				If Fn_GoodsID > 0 Then
					'Fn_Cost   = KnifeCMS.Data.FormatDouble(KnifeCMS.DB.GetFieldByID(DBTable_Goods,"Cost",Fn_GoodsID))
					Fn_Result = KnifeCMS.DB.AddRecord(DBTable_OrderGoods,Array("OrderID:"& Fn_OrderID,"Config:"& Fn_Config,"PacksID:"& Fn_PacksID,"PacksName:"& Fn_PacksName,"PacksWeight:"& Fn_PacksWeight,"PacksPrice:"& Fn_PacksPrice,"GoodsID:"& Fn_GoodsID,"GoodsTypeKey:"& Fn_GoodsTypeKey,"GoodsBN:"& Fn_GoodsBN,"GoodsName:"& Fn_GoodsName,"GoodsAttr:"& Fn_GoodsAttr,"GoodsAttrValue:"& Fn_GoodsAttrValue,"GoodsPrice:"& Fn_GoodsPrice,"AttrPrice:"& Fn_AttrPrice,"GoodsWeight:"& Fn_GoodsWeight,"ItemNum:"& Fn_CartJSON.thepacks.get(Fn_ii).selectgoods.length + 1,"Cost:"& Fn_Cost,"Price:"& Fn_Price,"Weight:"& Fn_Weight,"Nums:"& Fn_Nums,"Amount:"& Fn_Amount,"Score:"& Fn_Score,"Delivery_Status:"& Fn_Delivery_Status,"SendNums:0"))
					Fn_ItemNum = Fn_ItemNum + Fn_CartJSON.thepacks.get(Fn_ii).selectgoods.length
				End If
				'Echo "<br>===Fn_PacksName:" & Fn_PacksName &"<br>"
			Next
			'更新订单表商品数量ItemNum
			Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_Order,Array("ItemNum:"& Fn_ItemNum),Array("OrderID:"& Fn_OrderID))
			
			'清除购物车cookies记录
			Call KnifeCMS.Cookie.RemoveCookie("mycart")
			Dim Fn_IsCashOnDelivery : Fn_IsCashOnDelivery = KnifeCMS.Data.CLng(KnifeCMS.DB.GetFieldByID(DBTable_Delivery,"HasCod",Fn_ShippingID))
			Fn_ReturnString = "{""orderid"":"""& Fn_OrderID &""",""ordertime"":"""& SysTime &""",""iscod"":"""& Fn_IsCashOnDelivery &""",""weight"":"""& Fn_CartWeight &""",""cost_item"":"""& Fn_Cost_Item &""",""orderamount"":"""& Fn_Total_Amount &""",""errorinfo"":""""}"
		End If
		Set Fn_CartJSON = nothing
		PlaceOrder = Fn_ReturnString
		' "--PlaceOrder="& PlaceOrder &"保存订单完成-"
	End Function
	
	'生成唯一的订单ID(16位)
	Public Function CreateOrderID()
		Dim Fn_ii,Fn_OrderID
		Fn_OrderID = KnifeCMS.Data.FormatDateTime(SysTime,9) & KnifeCMS.MakeRandom(2)
		If Len(Fn_OrderID) <> 16 Then
			KnifeCMS.Error.Msg = Lang_Cart(10)
			KnifeCMS.Error.Raise 10
		End If
		For Fn_ii=0 To 100
			If Not(KnifeCMS.DB.CheckIsRecordExistByField(DBTable_Order,Array("OrderID:"& Fn_OrderID))) Then Exit For
			Fn_OrderID = KnifeCMS.Data.FormatDateTime(SysTime,9) & KnifeCMS.MakeRandom(2)
		Next
		CreateOrderID = Fn_OrderID
	End Function
	
	
	
End Class
%>