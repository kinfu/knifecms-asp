<%
Dim Notify
Set Notify = New Class_Notify
Class Class_Notify
	
	Private Pv_Sql,Pv_Rs,Pv_Array
	Private S_TempHTML
	
	Private Sub Class_Initialize()
	End Sub
	Private Sub Class_Terminate()
	End Sub

	Public Function GetMyNotify()
		If Not(1=Logined) Then GetMyNotify="unlogined" : Exit Function
		Dim Fn_Sql,Fn_Rs
		Dim Fn_MemberID,Fn_TempString,Fn_ReturnString
		Dim Fn_ID,Fn_UserID,Fn_UserName,Fn_HeadImg,Fn_NotifyType,Fn_NotifyConfig,Fn_NotifyTime,Fn_Message
		    Fn_MemberID = UserID
		Fn_Sql = "SELECT a.ID,a.CNUserID,b.Username,b.HeadImg,a.NotifyType,a.NotifyConfig,a.NotifyTime FROM ["& DBTable_Notify &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.CNUserID) WHERE a.SNUserID="& Fn_MemberID &" AND a.NotifyType<>'addfriend' AND a.IsTreated=0 ORDER BY a.NotifyType,a.ID DESC"
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_Sql)
		'Die Fn_Sql
		Do While Not(Fn_Rs.Eof)
				Fn_ID           = KnifeCMS.Data.Escape(Fn_Rs("ID"))
				Fn_UserID       = KnifeCMS.Data.Escape(Fn_Rs("CNUserID"))
				Fn_UserName     = KnifeCMS.Data.Escape(Fn_Rs("Username"))
				Fn_HeadImg      = KnifeCMS.Data.Escape(KnifeCMS.Data.URLDecode(Fn_Rs("HeadImg")))
				Fn_NotifyType   = Fn_Rs("NotifyType")
				Fn_NotifyConfig = Fn_Rs("NotifyConfig")
				Fn_NotifyTime   = KnifeCMS.Data.Escape(Fn_Rs("NotifyTime"))
				Fn_Message      = KnifeCMS.Data.Escape(KnifeCMS.Data.RegSubMatches(Fn_NotifyConfig,"""msg"":""([\s\S]*?)""")(0,0))
				Fn_TempString   = """id"":"""& Fn_ID &""",""userid"":"""& Fn_UserID &""",""username"":"""& Fn_UserName &""",""headimg"":"""& Fn_HeadImg &""",""message"":"""& Fn_Message &""",""notifytype"":"""& Fn_NotifyType &""",""time"":"""& Fn_NotifyTime &""""
				If Fn_ReturnString = "" Then
					Fn_ReturnString = Fn_TempString
				Else
					Fn_ReturnString = Fn_ReturnString &";"& Fn_TempString
				End IF
				Fn_Rs.movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		GetMyNotify = Fn_ReturnString
	End Function
	
	Public Function NotifyDelete(ByVal BV_Type)
		If Not(1=Logined) Then NotifyDelete="unlogined" : Exit Function
		Dim Fn_Result,Fn_ID,Fn_UserID
		If BV_Type="all" Then
			Fn_Result = KnifeCMS.DB.DeleteRecord(DBTable_Notify,"SNUserID="& UserID &" AND NotifyType<>'addfriend' AND IsTreated=0")
			If Fn_Result Then NotifyDelete="ok"
		Else
			Fn_ID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","id"))
			If Not(Fn_ID>0) Then NotifyDelete="error" : Exit Function
			Fn_UserID=KnifeCMS.DB.GetFieldByID(DBTable_Notify,"SNUserID",Fn_ID)
			If Fn_UserID<>UserID Then
				NotifyDelete="error" : Exit Function
			Else
				Fn_Result = KnifeCMS.DB.DeleteRecord(DBTable_Notify,Array("ID:"& Fn_ID))
				If Fn_Result Then NotifyDelete="ok"
			End If
		End If
	End Function
	
	
End Class






































%>