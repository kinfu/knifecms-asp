<!--#include file="config.admin.asp"-->
<%
Dim SystemBackup
Set SystemBackup = New Class_KnifeCMS_Admin_SystemBackup
Set SystemBackup = Nothing
Class Class_KnifeCMS_Admin_SystemBackup

	Private Pv_Result
	Private Pv_FilePath,Pv_Code,Pv_FileSuffix,Fn_SystemBackupName
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "systembackup.lang.asp"
		If Ctl="systembackup" Then
			Select Case Action
				Case "view"
					Header()
					Call BackupView()
					Footer()
				Case "backup"
					Header()
					Call SystemBackup()
					Footer()
				Case "restore"
					Header()
					Call SystemRestore()
					Footer()
				Case "del"
					Call DeleteBackupData()
			End Select
		Else
			Header()
			Admin.ShowMessage Lang_PraError,"goback",0
			Footer()
		End If
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Function GetSystemBackupData()
		GetSystemBackupData = KnifeCMS.FSO.FileList("json","data/backup")
	End Function

	'备份
	Function SystemBackup()
		Dim Fn_BackupFileName
		Fn_BackupFileName = KnifeCMS.Data.FileName(DB_AccessFilePath,False) &"_bak_"& KnifeCMS.Data.FormatDateTime(SysTime,9) &"."& KnifeCMS.Data.FileExName(DB_AccessFilePath)
		If KnifeCMS.FSO.CopyFile(DB_AccessFilePath,"data/backup/"& Fn_BackupFileName) Then
			Admin.ShowMessage Lang_SystemBackup(1),"?ctl="& Ctl &"&act=view",1
		Else
			Admin.ShowMessage Lang_SystemBackup(2),"?ctl="& Ctl &"&act=view",1
		End If
	End Function
	
	'还原
	Function SystemRestore()
		Dim Fn_FilePath
		Fn_FilePath = KnifeCMS.GetForm("post","filepath")
		If KnifeCMS.FSO.CopyFile(Fn_FilePath,DB_AccessFilePath) Then
			Admin.ShowMessage Lang_SystemBackup(3),"?ctl="& Ctl &"&act=view",1
		Else
			Admin.ShowMessage Lang_SystemBackup(4),"?ctl="& Ctl &"&act=view",1
		End If
	End Function
	
	'删除备份数据库
	Public Function DeleteBackupData()
		On Error Resume Next
		Dim Fn_ii,Fn_JsonData,Fn_Json,Fn_FilePath,Fn_FilesJson,Fn_Result
		Fn_JsonData = KnifeCMS.GetForm("post","jsondata")
		
		If Trim(Fn_JsonData) = "" Then Exit Function
		Set Fn_Json = KnifeCMS.JSON.Parse(Fn_JsonData)
		
		'删除文件
		For Fn_ii=0 To Fn_Json.files.length-1
			Fn_FilePath = Fn_Json.files.get(Fn_ii).filepath
			If Trim(Fn_FilePath)<>"" Then
				Fn_Result = KnifeCMS.FSO.DeleteFile(KnifeCMS.FSO.ABSPath(Fn_FilePath))
				If Fn_Result Then
					Msg = Msg & Lang_SystemBackup(5) &"{"& Fn_FilePath &"]}<br>"
				Else
					Msg = Msg & Lang_SystemBackup(6) &"{"& Fn_FilePath &"]}<br>"
				End If
				Fn_Result = KnifeCMS.IIF(Fn_Result=True,"true","false")
				If Fn_FilesJson = "" Then
					Fn_FilesJson = "{""filepath"":"""& Fn_FilePath &""",""deleteresult"":"""& Fn_Result &"""}"
				Else
					Fn_FilesJson = Fn_FilesJson &",{""filepath"":"""& Fn_FilePath &""",""deleteresult"":"""& Fn_Result &"""}"
				End If
			End If
		Next
		Fn_JsonData = "{""files"":["& Fn_FilesJson &"]}"
		Set Fn_Json = Nothing
		If Err.Number<>0 Then Err.Clear()
		Call Admin.SaveLog(Msg &"["& Lang_LocalUrl & KnifeCMS.GetUrl("") &"|"& Lang_FromUrl  & Cstr(Request.ServerVariables("HTTP_REFERER")) &"]")
		Echo Fn_JsonData
	End Function
	
	Function BackupView()
		Select Case DB_Type
		Case 0
			Call ACCESS_BackupView()
		Case 1
			Call MSSQL_BackupView()
		End Select
	End Function
	
	'查看备份文件列表
    Function ACCESS_BackupView()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
      <div class="filemanage_body">
          <div class="filemanage clear" id="filemanage">
          <form name="SaveForm" id="SaveForm" action="?ctl=<% Echo Ctl %>&act=restore" method="post">
              <input type="hidden" name="filepath" value="" />
              <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
              <td class="section">
              <div class="container" id="filecontainer">
                  <div class="menuline">
                  <%
				  Call Admin.PrintPermissionMenu(Array(Ctl,"del"),"<a class=""fbutton fbtn_delete"" name=""delete""><span class=""ficon_delete""></span>"& Lang_SystemBackup(8) &"</a>")
				  Call Admin.PrintPermissionMenu(Array(Ctl,"backup"),"<a class=""fbutton fbtn_backup"" name=""backup""><span class=""ficon_backup""></span>"& Lang_SystemBackup(7) &"</a>")
				  Call Admin.PrintPermissionMenu(Array(Ctl,"restore"),"<a class=""fbutton fbtn_restore"" name=""restore""><span class=""ficon_backup""></span>"& Lang_SystemBackup(16) &"</a>")
				  %>
                  </div>
                  <div class="filelist"></div>
                  <div class="pages"></div>
              </div>
              </td>
              </tr>
              </table>
          </form>
          </div>
          
      </div>
      </DIV>
    </DIV>
    <script type="text/javascript">
	/*
	* 系统备份管理jQuery插件
	* codeBy:CK
	* update:2013-04-09
	*/
	$.extend({
		systemBackupManage:function(opt){
			var json,jsonData = opt.data || "";
			var $this = $("#filemanage");
			var fileExName = function(fileName){
				var array = fileName.split(".");
				return array[array.length-1];
			}
			var init = function(){
				if(jsonData!=""){
					var dt,html,filesHTML,json;
					json = $.parseJSON(jsonData);
					//列表头
					dt = '<dt>'
				    +'<div class="checkbox"><input type="checkbox" name="selectall" value="" title="'+Lang_Js_SelectAll+'" /></div>'
				    +'<div class="name"><%=Lang_SystemBackup(9)%></div><div class="datetime"><%=Lang_SystemBackup(10)%></div>'
					+'<div class="size"><%=Lang_SystemBackup(11)%></div>'
					+'</dt>';
					//显示文件列表
					filesHTML = "";
					for(var i=0;i<json.files.length;i++){
						filesHTML = filesHTML +'<dd class="file" filepath="'+json.files[i].filepath+'">'
						+'<div class="checkbox"><input type="checkbox" name="file" value="'+json.files[i].filepath+'" title="'+Lang_Js_Select+'" /></div>'
						+'<div class="preview"></div>'
						+'<div class="name"><span class="icons_file icon_file icon_file_'+fileExName(json.files[i].filepath)+'"></span><a href="javascript:;" title="'+json.files[i].filename+'">'+json.files[i].filename+'</a></div>'
						+'<div class="datetime">'+json.files[i].datetime+'</div>'
						+'<div class="size">'+json.files[i].size+'</div>'
						+'</dd>'
					}
					$this.find(".filelist").append('<dl class="list">'+ dt + filesHTML +'</dl>');
					if(json.files.length==0){
						$this.find(".filelist").append('<div class="none"><%=Lang_SystemBackup(12)%></div>');
					}
					//点击列表文件夹和文件则选中
					$this.find("dd").click(function(){
						var checkBox = $(this).find("input:checkbox");
						if(checkBox.attr("checked")==true){
							checkBox.attr("checked",false);
							$(this).removeClass("selected");
						}else{
							checkBox.attr("checked",true);
							$(this).addClass("selected");
						}
					});
					//点击全选
					$this.find("input[name='selectall']").click(function(){
						if($(this).attr("checked")==true){
							$this.find("dd input:checkbox").attr("checked",true);
							$this.find("dd").addClass("selected");
						}else{
							$this.find("dd input:checkbox").attr("checked",false);
							$this.find("dd").removeClass("selected");
						}
					});
				}
			}
			//删除文件夹和文件
			var deleteBackupData= function(thisObj){
				var temp,
				foldersJsonData = "",
				filesJsonData = "",
				jsonData = "";
				$this.find("input[name='file']:checked").each(function(i){
					if(filesJsonData==""){
						filesJsonData = '{"filepath":"'+escape($(this).val())+'"}';
					}else{
						filesJsonData = filesJsonData +',{"filepath":"'+escape($(this).val())+'"}';
					}
				});
				if(filesJsonData==""){
					alert("<%=Lang_SystemBackup(13)%>");
				}else{
					if(confirm("<%=Lang_SystemBackup(15)%>")){
						var dialogID = KnifeCMS.DiaDoing("<%=Lang_SystemBackup(14)%>",true,true);
						jsonData = '{"files":['+filesJsonData+']}';
						var $url="kfc.systembackup.asp?ctl=systembackup&act=del&r="+parseInt(Math.random()*1000);
						$.ajax({type:"POST",dataType:"html",async:true,url:$url,data:"jsondata="+jsonData+"",
							error:function(){
								KnifeCMS.DiaWindowCloseByID(dialogID);
								alert(Lang_Js_Cue_ServerErrorAndTryLatter);
							},
							success:function(ajaxText){
								KnifeCMS.DiaWindowCloseByID(dialogID);
								if(ajaxText.indexOf('identify="message"')>0 || ajaxText=="Error"){
									var id = KnifeCMS.CreateID();
									KnifeCMS.CreateDiaWindow(720,250,KnifeCMS.CreateID(),Lang_Js_DiaTitle[0],'<div class="message">'+ajaxText+'<div>',true,true);
									$("div.DialogText").find("a").empty();
								}else{
									var json = $.parseJSON(ajaxText);
									for(var i=0; i<json.files.length; i++){
										if(json.files[i].deleteresult=="true"){
											$this.find(".file[filepath='"+json.files[i].filepath+"']").remove();
										}
									}
									KnifeCMS.DiaWindowCueMessage("<%=Lang_SystemBackup(5)%>",4,280,128);
								}
							}
						});
					}
				}
			}
			var restore = function(){
				var $checked = $this.find("input:checked");
				if($checked.length==0){
					alert("<%=Lang_SystemBackup(17)%>");
				}else{
					if($checked.length>1){
						alert("<%=Lang_SystemBackup(18)%>");
					}else{
						var $backupFilePath = $checked.val();
						var $tip = "<%=Lang_SystemBackup(19)%>";
						if(confirm($tip.replace("{tpl:filepath}",$backupFilePath))){
							var $form  = $("form[name='SaveForm']");
							var $input = $("input[name='filepath']");
							$input.val($backupFilePath);
							$form.submit();
						}
					}
				}
			}
			init();
			$this.find(".menuline>a[name='delete']").click(function(){deleteBackupData();});
			$this.find(".menuline>a[name='backup']").click(function(){window.location.href="?ctl=<%=Ctl%>&act=backup";});
			$this.find(".menuline>a[name='restore']").click(function(){restore();});
		}
	});
    $(document).ready(function(){
		var systemBackupData = '<%=GetSystemBackupData()%>';
		$.systemBackupManage({data:systemBackupData});
	});
    </script>
<%
    End Function
	'查看备份文件列表
    Function MSSQL_BackupView()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
      <div class="filemanage_body">
          <div class="filemanage clear" id="filemanage">
          <form name="SaveForm" id="SaveForm" action="?ctl=<% Echo Ctl %>&act=restore" method="post">
              <input type="hidden" name="filepath" value="" />
              <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
              <td class="section">
              <div class="container" id="filecontainer">
                  <div class="filelist"><div style="padding:15px;"><%=Lang_SystemBackup(20)%></div></div>
                  <div class="pages"></div>
              </div>
              </td>
              </tr>
              </table>
          </form>
          </div>
          
      </div>
      </DIV>
    </DIV>
    
<%
    End Function
End Class
%>
