<!--#include file="config.admin.asp"-->
<%
Dim GetFileType,FilePath,Ran
Dim Ajaxr
set Ajaxr=new Ajaxtext

FilePath=trim(request.QueryString("FilePath"))
Ran=int(trim(request.QueryString("r")))

If Ran<1 or Ran>1000 then
response.Write"error"
set Ajaxr=nothing
response.End()
End If


Select Case SubCtl
	Case "image","flash","media","music","file" : Ajaxr.GetFiles SubCtl,FilePath 
	Case Else : Echo "error"
End Select

Class Ajaxtext
	Public Rs,Sql
	Public Function GetFiles(FileType,StrFilePath)
		If Trim(StrFilePath) ="" Then 
			Exit Function
		End If
		If KnifeCMS.IsFSOInstalled=False then
			ErrMsg=Lang_ObjFSONotInstalled:Admin.ShowError(0)
		End If
		Dim FilePath,f,fold,FileItem,FileNum,i
		Dim Fn_FullFilePath
		FileNum=0
		i=0
		'FilePath=KnifeCMS.Data.URLDecode(StrFilePath)
		FilePath=StrFilePath
		If Left(FilePath,1) = "/" Then FilePath = Mid(FilePath,2)
		If Right(FilePath,1) <> "/" Then FilePath = FilePath &"/"
		'**过滤禁止的文件夹字符
		FilePath=Replace(FilePath,"\","")
		FilePath=Replace(FilePath,":","")
		FilePath=Replace(FilePath,"*","")
		FilePath=Replace(FilePath,"?","")
		FilePath=Replace(FilePath,"""","")
		FilePath=Replace(FilePath,"|","")
		FilePath=Replace(FilePath,"<","")
		FilePath=Replace(FilePath,">","")
		set f=server.createobject("scripting.filesystemobject")
		If f.FolderExists(Server.MapPath(SystemPath&FilePath))=False Then
			response.Write "ERROR_FILEPATH："&FilePath 
			exit function
		End If
		set fold=f.getfolder(Server.MapPath(SystemPath&FilePath))
		FileNum=fold.subfolders.Count + fold.files.Count
		If int(FileNum) > 0 Then
			Response.Write "<ul id=""Folders"">"
			for each FileItem in fold.subfolders
				response.write "<li><p class=""Folder"" ondblclick=""GetFiles('"& FileType &"','"& FilePath & FileItem.name &"')""></p><em>"&FileItem.name&"</em></li>"
			next
			Response.Write "</ul>"
			Response.Write "<ul id=""Files"">"
			
			for each FileItem in fold.files
				i=i+1
				If SiteFilePathType="absolute" Then
					Fn_FullFilePath = SiteURL & SystemPath & FilePath & FileItem.name
				Else
					Fn_FullFilePath = SystemPath & FilePath & FileItem.name
				End If
				Select Case FileType
					Case "image" : response.Write "<li onclick=""FileSelect('"&i&"')"" id=""li_"&i&"""><p class=""image"" title=""lastmodifiedTime:"&FileItem.datelastmodified&" Size:"&KnifeCMS.Data.CLng(FileItem.size/1024)&"K""><img src="""& Fn_FullFilePath &""" /></p><input type=hidden value="""& Fn_FullFilePath &"""><em>"&FileItem.name &"</em></li>"
					Case "flash" : response.Write "<li onclick=""FileSelect('"&i&"')"" id=""li_"&i&"""><p class=""flash"" title=""lastmodifiedTime:"&FileItem.datelastmodified&" Size:"&KnifeCMS.Data.CLng(FileItem.size/1024)&"K""></p><input type=hidden value="""& Fn_FullFilePath &"""><em>"&FileItem.name&"</em></li>"
					Case "music" : response.Write "<li onclick=""FileSelect('"&i&"')"" id=""li_"&i&"""><p class=""music"" title=""lastmodifiedTime:"&FileItem.datelastmodified&" Size:"&KnifeCMS.Data.CLng(FileItem.size/1024)&"K""></p><input type=hidden value="""& Fn_FullFilePath &"""><em>"&FileItem.name&"</em></li>"
					Case "media" : response.Write "<li onclick=""FileSelect('"&i&"')"" id=""li_"&i&"""><p class=""media"" title=""lastmodifiedTime:"&FileItem.datelastmodified&" Size:"&KnifeCMS.Data.CLng(FileItem.size/1024)&"K""></p><input type=hidden value="""& Fn_FullFilePath &"""><em>"&FileItem.name&"</em></li>"
					Case "file" : response.Write "<li onclick=""FileSelect('"&i&"')"" id=""li_"&i&"""><p class=""file"" title=""lastmodifiedTime:"&FileItem.datelastmodified&" Size:"&KnifeCMS.Data.CLng(FileItem.size/1024)&"K""></p><input type=hidden value="""& Fn_FullFilePath &"""><em>"&FileItem.name&"</em></li>"
					Case Else
						Response.Write("ERROR_CALL")
				End Select
			next
			Response.Write "</ul>"
		Else
			Response.Write(Lang_NoFileAndFolderExistInThisFolder)
		End If
		set fold=nothing
		set f=Nothing
	End Function
	
End Class

%>