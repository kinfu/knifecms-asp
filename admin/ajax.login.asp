<!--#include file="config.admin.asp"-->
<%
Admin.IncludeLanguageFile "sysuser.lang.asp"
IF Not(KnifeCMS.CheckPost()) then
	Call Admin.CheckPostShow()
Else
	If Ctl="sysuserlogin" Then
		Select Case Action
			Case "login"    : Echo Admin.SysUserLogining
			Case "loginout" : Admin.SysUserLoginOut()
			Case Else : Echo("error")
		End Select
	End If
	Call ClassObjectClose()
End If
%>