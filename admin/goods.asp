<!--#include file="config.admin.asp"-->
<%
Dim Goods
Set Goods = New Class_KnifeCMS_Admin_Goods
Set Goods = Nothing
Class Class_KnifeCMS_Admin_Goods

	Private Pv_ii,Pv_jj,tmpRs,tmpSql,Pv_TempID
	Private GoodsModelID,GoodsType,GoodsName,GoodsBN,GoodsSN,Marketable,GoodsPrice,GoodsCost,GoodsMktPrice,GoodsWeight,GoodsWeightUnit,GoodsStore,GoodsUnit,BrandID,Abstract,Thumbnail,GoodsFirstImg,GoodsImg,TextContent,GoodsAttr,GoodsAdjunct,LinkGoodsID
	Private Pv_Recommend,Pv_Hot,Pv_New,Pv_HomepageRecommend
	Private SeoTitle,SeoUrl,MetaKeywords,MetaDescription
	Private tmpSysID,Pv_TempHTML,VirtualClassID
	Private GoodsImg_i
	Private PriceFrom,PriceTo,CostFrom,CostTo,MktPriceFrom,MktPriceTo,StoreFrom,StoreTo
	Private Pv_Field,Pv_FieldName,Pv_FieldType,Pv_NotNull,Pv_FieldOptions,Pv_FieldTips,Pv_OrderNum,Pv_Recycle
	Private Pv_FieldValue,Pv_Array,Pv_Array2,Pv_Dictionary

	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "goods.lang.asp"
		IsContentExist = False
		ActionType = KnifeCMS.Data.Left(KnifeCMS.GetForm("both","acttype"),50)
		VirtualClassID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","virtualclassid"))
		Header()
		If Ctl = "goodsinvirtualclass" Then
			Select Case Action
				Case "view"
					VirtualClassGoodsView
				Case "add"
					VirtualClassGoodsDoSave
				Case "del"
					VirtualClassGoodsDel
			End Select
		ElseIf Ctl = "goods" Or Ctl = "goods_recycle" Then
			Select Case Action
				Case "view"
					GoodsGetSearchCondition
					If ActionType = "minilist" then
						 GoodsMiniListView
					Else
						GoodsView
					End IF
				Case "add","edit"
					If SubAction="save" Then
						GoodsDoSave
					Else
						If Action = "add" Then
							GoodsStore = 1000
							If ID>0 Then Call GetGoodsDetail()
							ID        = 0
							GoodsType = 1
							SysID     = KnifeCMS.CreateSysID
							Call GoodsDo()
						ElseIf Action = "edit" Then
							If SubAction = "revert" Or SubAction = "marketable" Or SubAction = "unmarketable" Then
								GoodsOtherEdit
							Else
								Call GetGoodsDetail()
								If IsContentExist Then Call GoodsDo() : Else Admin.ShowMessage Lang_Goods_Cue(7),"goback",0
							End If
						End IF
					End IF
				Case "del"  : GoodsDel
			End Select
		End If
		Footer()
	End Sub
	
	Private Function GetGoodsDetail()
		Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT SysID,GoodsType,ClassID,GoodsName,GoodsBN,GoodsSN,Marketable,Price,Cost,MktPrice,Weight,Store,Unit,BrandID,Thumbnail,FirstImg,Imgs,Abstract,Intro,GoodsModel,SeoTitle,SeoUrl,MetaKeywords,MetaDescription,Recommend,Hot,New,HomepageRecommend FROM ["& DBTable_Goods &"] WHERE ID="& ID &"")
		If Not(Rs.Eof) Then
			IsContentExist = True
			SysID         = Rs("SysID")
			GoodsType     = Rs("GoodsType")
			ClassID       = Rs("ClassID")
			GoodsName     = Rs("GoodsName")
			GoodsBN       = Rs("GoodsBN")
			GoodsSN       = Rs("GoodsSN")
			Marketable    = Rs("Marketable")
			GoodsPrice    = Rs("Price")
			GoodsCost     = Rs("Cost")
			GoodsMktPrice = Rs("MktPrice")
			GoodsWeight   = Rs("Weight")
			GoodsStore    = Rs("Store")
			GoodsUnit     = Rs("Unit")
			BrandID       = Rs("BrandID")
			Thumbnail     = Rs("Thumbnail")
			GoodsFirstImg = Rs("FirstImg")
			GoodsImg      = Rs("Imgs")
			Abstract      = Rs("Abstract")
			TextContent   = Rs("Intro")
			GoodsModelID  = Rs("GoodsModel")
			SeoTitle        = Rs("SeoTitle")
			SeoUrl          = Rs("SeoUrl")
			MetaKeywords    = Rs("MetaKeywords")
			MetaDescription = Rs("MetaDescription")
			Pv_Recommend    = Rs("Recommend")
			Pv_Hot          = Rs("Hot")
			Pv_New          = Rs("New")
			Pv_HomepageRecommend = Rs("HomepageRecommend")
		End IF
		KnifeCMS.DB.CloseRs Rs
	End Function
	
	Private Function GoodsGetSearchCondition()
		PriceFrom = KnifeCMS.GetForm("both","PriceFrom") : If not(KnifeCMS.Data.IsNul(PriceFrom)) Then PriceFrom = KnifeCMS.Data.FormatDouble(PriceFrom)
		PriceTo   = KnifeCMS.GetForm("both","PriceTo")   : If not(KnifeCMS.Data.IsNul(PriceTo))   Then PriceTo   = KnifeCMS.Data.FormatDouble(PriceTo)
		CostFrom  = KnifeCMS.GetForm("both","CostFrom")  : If not(KnifeCMS.Data.IsNul(CostFrom))  Then CostFrom  = KnifeCMS.Data.FormatDouble(CostFrom)
		CostTo    = KnifeCMS.GetForm("both","CostTo")    : If not(KnifeCMS.Data.IsNul(CostTo))    Then CostTo    = KnifeCMS.Data.FormatDouble(CostTo)
		MktPriceFrom = KnifeCMS.GetForm("both","MktPriceFrom") : If not(KnifeCMS.Data.IsNul(MktPriceFrom)) Then MktPriceFrom = KnifeCMS.Data.FormatDouble(MktPriceFrom)
		MktPriceTo   = KnifeCMS.GetForm("both","MktPriceTo")   : If not(KnifeCMS.Data.IsNul(MktPriceTo))   Then MktPriceTo   = KnifeCMS.Data.FormatDouble(MktPriceTo)
		StoreFrom = KnifeCMS.GetForm("both","StoreFrom") : If not(KnifeCMS.Data.IsNul(StoreFrom)) Then StoreFrom = KnifeCMS.Data.FormatDouble(StoreFrom)
		StoreTo   = KnifeCMS.GetForm("both","StoreTo")   : If not(KnifeCMS.Data.IsNul(StoreTo))   Then StoreTo   = KnifeCMS.Data.FormatDouble(StoreTo)
	End Function
	
	Private Function GoodsSearchSql()
		Dim Fn_TempSql
		Dim Fn_PriceFrom,Fn_PriceTo,Fn_CostFrom,Fn_CostTo,Fn_MktPriceFrom,Fn_MktPriceTo,Fn_StoreFrom,Fn_StoreTo
		Fn_PriceFrom    = KnifeCMS.Data.FormatDouble(PriceFrom)
		Fn_PriceTo      = KnifeCMS.Data.FormatDouble(PriceTo)
		Fn_CostFrom     = KnifeCMS.Data.FormatDouble(CostFrom)
		Fn_CostTo       = KnifeCMS.Data.FormatDouble(CostTo)
		Fn_MktPriceFrom = KnifeCMS.Data.FormatDouble(MktPriceFrom)
		Fn_MktPriceTo   = KnifeCMS.Data.FormatDouble(MktPriceTo)
		Fn_StoreFrom    = KnifeCMS.Data.FormatDouble(StoreFrom)
		Fn_StoreTo      = KnifeCMS.Data.FormatDouble(StoreTo)
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Fn_PriceFrom > 0 ," AND isnull(a.Price,0)>="& PriceFrom &" "," ")
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Fn_PriceTo   > 0 ," AND isnull(a.Price,0)<="& PriceTo &" "  ," ")
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Fn_CostFrom  > 0 ," AND isnull(a.Cost,0)>="& CostFrom &" "," ")
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Fn_CostTo    > 0 ," AND isnull(a.Cost,0)<="& CostTo &" "  ," ")
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Fn_MktPriceFrom > 0 ," AND isnull(a.MktPrice,0)>="& MktPriceFrom &" "," ")
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Fn_MktPriceTo   > 0 ," AND isnull(a.MktPrice,0)<="& MktPriceTo &" "  ," ")
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Fn_StoreFrom    > 0 ," AND isnull(a.Store,0)>="& StoreFrom &" "," ")
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Fn_StoreTo      > 0 ," AND isnull(a.Store,0)<="& StoreTo &" "  ," ")
		GoodsSearchSql = Fn_TempSql
	End Function
	
	Private Function GoodsList()
		Dim Fn_TempSql,Fn_ColumnNum
		If ActionType = "minilist" Then
			Fn_TempSql = "SELECT a.ID,a.GoodsBN,a.GoodsName,a.Price,a.Store FROM "&DBTable_Goods&" a "
			Fn_ColumnNum = 5
			PageUrlPara = "acttype="& ActionType &"&virtualclassid="& VirtualClassID
		Else
		    Select Case DB_Type
		    Case 0
				Fn_TempSql = "SELECT a.ID,a.GoodsBN,a.GoodsName,IIF(LEN(a.Thumbnail)=0,a.FirstImg,a.Thumbnail),d.TypeName,a.Price,a.Store,a.Marketable,b.ClassName,c.BrandName,a.Hits,a.Recommend,a.Hot,a.New,a.HomepageRecommend,a.AddTime FROM ((["&DBTable_Goods&"] a LEFT JOIN "&DBTable_GoodsClass&" b ON (b.ID=a.ClassID)) LEFT JOIN ["&DBTable_Brand&"] c ON (c.ID=a.BrandID)) LEFT JOIN ["&DBTable_GoodsType&"] d ON (d.ID=a.GoodsType) "
			Case 1
				Fn_TempSql = "SELECT a.ID,a.GoodsBN,a.GoodsName,CASE WHEN LEN(a.Thumbnail)=0 THEN a.FirstImg ELSE a.Thumbnail END,d.TypeName,a.Price,a.Store,a.Marketable,b.ClassName,c.BrandName,a.Hits,a.Recommend,a.Hot,a.New,a.HomepageRecommend,a.AddTime FROM "&DBTable_Goods&" a LEFT JOIN "&DBTable_GoodsClass&" b ON (b.ID=a.ClassID) LEFT JOIN "&DBTable_Brand&" c ON (c.ID=a.BrandID) LEFT JOIN "&DBTable_GoodsType&" d ON (d.ID=a.GoodsType) "
			End Select
			Fn_ColumnNum = 16
			Operation = "<span class=""sysBtn""><a href=""?ctl="&Ctl&"&act=edit&id={$ID}"" ><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
			Operation = Operation &"<span class=""sysBtn""><a href=""?ctl="&Ctl&"&act=add&id={$ID}"" ><b class=""icon icon_copy""></b>"& Lang_DoCopy &"</a></span>"
		End If
		If SubCtl = "recycle" Then
			Fn_TempSql = Fn_TempSql & "WHERE a.Recycle=1 ORDER BY a.ID DESC"
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,"")
		Else
			Fn_TempSql = Fn_TempSql & "WHERE a.Recycle=0 "
			If SubAction = "search" Then
				If Admin.Search.ClassID > 0 Then Admin.GetChildClass DBTable_GoodsClass,Admin.Search.ClassID
				tempCondition =                 KnifeCMS.IIF(Admin.Search.ClassID  > 0 ," AND a.ClassID IN("&Admin.Search.ClassID&Admin.ChildClass&") "," ")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.GoodsType> 0 ," AND a.GoodsType="&Admin.Search.GoodsType&" "," ")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.BrandID  > 0 ," AND a.BrandID="&Admin.Search.BrandID&" "," ")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.KeyWords <>""," AND a.GoodsBN like '%"& Admin.Search.KeyWords &"%' Or GoodsName like '%"& Admin.Search.KeyWords &"%' "," ")
				Select Case DB_Type
				Case 0
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.AddTime>=#"& Admin.Search.StartDate &"# ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.AddTime<=#"& Admin.Search.EndDate &"# ","")
				Case 1
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.AddTime>='"& Admin.Search.StartDate &"' ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.AddTime<='"& Admin.Search.EndDate &"' ","")
				End Select
				
				tempCondition = tempCondition & GoodsSearchSql
				PageUrlPara = PageUrlPara & "&goodstype="& Admin.Search.GoodsType &"&classid="& Admin.Search.ClassID &"&startdate="& Server.URLEncode(Admin.Search.StartDate) &"&enddate="& Server.URLEncode(Admin.Search.EndDate) &"&keywords="& Server.URLEncode(Admin.Search.KeyWords)&"&PriceFrom="&PriceFrom&"&PriceTo="&PriceTo&"&CostFrom="&CostFrom&"&CostTo="&CostTo&"&MktPriceFrom"&MktPriceFrom&"&MktPriceTo="&MktPriceTo&"&StoreFrom="&StoreFrom&"&StoreTo="&StoreTo&""
				Fn_TempSql = Fn_TempSql & tempCondition
			End If
			Fn_TempSql = Fn_TempSql &" ORDER BY a.ID DESC"
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
		End If
	End Function
	
	'$:虚拟分类商品列表
	Private Function VirtualClassGoodsList()
		Dim Fn_TempSql
		Dim Fn_Url : Fn_Url = KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))
		Select Case DB_Type
		Case 0
		Fn_TempSql = "SELECT a.ID,b.GoodsBN,b.GoodsName,IIF(LEN(b.Thumbnail)=0,b.FirstImg,b.Thumbnail),b.Price,b.Store,c.ClassName,d.ClassName FROM (((["& DBTable_GoodsInVirtualClass &"] a LEFT JOIN ["& DBTable_Goods &"] b ON (b.ID=a.GoodsID)) LEFT JOIN ["& DBTable_GoodsVirtualClass &"] c ON (c.ID=a.VirtualClassID)) LEFT JOIN ["& DBTable_GoodsClass &"] d ON (d.ID=b.ClassID)) WHERE a.VirtualClassID="& VirtualClassID &" ORDER BY d.ID ASC"
		Case 1
		Fn_TempSql = "SELECT a.ID,b.GoodsBN,b.GoodsName,CASE WHEN LEN(b.Thumbnail)=0 THEN b.FirstImg ELSE b.Thumbnail END,b.Price,b.Store,c.ClassName,d.ClassName FROM ["& DBTable_GoodsInVirtualClass &"] a LEFT JOIN ["& DBTable_Goods &"] b ON (b.ID=a.GoodsID) LEFT JOIN ["& DBTable_GoodsVirtualClass &"] c ON (c.ID=a.VirtualClassID) LEFT JOIN ["& DBTable_GoodsClass&"] d ON (d.ID=b.ClassID) WHERE a.VirtualClassID="&VirtualClassID&" ORDER BY d.ID ASC"
		End Select
		
		Operation = "<span class=""sysBtn""><a href=""?ctl="&Ctl&"&act=del&virtualclassid="&ID&"&id={$ID}&url="&Fn_Url&""" ><b class=""icon icon_del""></b>"& Lang_DoDelReject &"</a></span>"
		Call Admin.DataList(Fn_TempSql,20,8,Operation,PageUrlPara)
	End Function
	
	'$:获取商品类型属性
	Private Function GetGoodsModelAttribute(ByVal BV_GoodsID,ByVal BV_GoodsModelID)
		Dim Fn_AttrID,Fn_AttrName,Fn_AttrType,Fn_AttrInputType,Fn_AttrValues,Fn_TempArray,Fn_i
		Dim tmpString
		BV_GoodsID      = KnifeCMS.Data.CLng(BV_GoodsID)
		BV_GoodsModelID = KnifeCMS.Data.CLng(BV_GoodsModelID)
		If BV_GoodsID=0 Or BV_GoodsModelID=0 Then Exit Function
		'Set Rs = KnifeCMS.DB.GetRecord(TablePre&"GoodsModelAttribute:ID,AttrName,AttrType,AttrInputType,AttrValues",Array("GoodsModelID:"&BV_GoodsModelID),"ORDER BY AttrOrder ASC,ID DESC")
		Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,AttrName,AttrType,AttrInputType,AttrValues FROM ["& TablePre &"GoodsModelAttribute] WHERE GoodsModelID="& BV_GoodsModelID &" ORDER BY AttrOrder ASC,ID DESC")
		If (Rs.bof and Rs.eof) Then
			Echo("<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""infoTable""><body><tr><td class=""titletd""></td><td class=""infotd"">"& Lang_NoneAttr &"</td></tr></body></table>")
		Else
			Echo("<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""infoTable""><body>")
			do while not Rs.eof
				Fn_AttrID=Rs("ID")
				Fn_AttrName=Rs("AttrName")
				Fn_AttrType=Rs("AttrType")
				Fn_AttrInputType=Rs("AttrInputType")
				Fn_AttrValues=Rs("AttrValues")
				Echo"<tr><td class=""titletd"">"& Fn_AttrName &":</td><td class=""infotd"">"
				'Set tmpRs = KnifeCMS.DB.GetRecord(TablePre&"GoodsAttr:AttrValue,AttrPrice",Array("GoodsID:"&BV_GoodsID,"AttrID:"&Fn_AttrID),"ORDER BY ID ASC")
				Set tmpRs = KnifeCMS.DB.GetRecordBySQL("SELECT AttrValue,AttrPrice FROM ["& TablePre &"GoodsAttr] WHERE GoodsID="& BV_GoodsID &" AND AttrID="& Fn_AttrID &" ORDER BY ID ASC")
				Select Case Fn_AttrInputType
				Case 0
					If Fn_AttrType > 0 Then
						Echo"<table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""listTable""><tbody><tr class=top><th>"
						Echo"<div style=""width:90px;""><span class=sysBtn><a onclick=""Admin.Goods.AddGoodsAttr(this)"" href=""javascript:void(0);""><b class=""icon icon_add""></b>"& Lang_DoAddOne &"</a></span></div>"
						Echo"</th><th><div style=""width:100px;"">"& Lang_AttrValue &"</div></th><th><div style=""width:200px;"">"& Lang_AttrPrice &"</div></th></tr>"
						If (tmpRs.bof and tmpRs.eof) Then
							'Echo(tmpSql):response.End()
							Echo"<tr class=data><td style=""text-align:center;"">"& Lang_Default &"</td><td>"
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_AttrID&""" />"
							Echo"<input type=""text"" class=""TxtClass"" name=""Attr_Value_List[]"" value="""" />"
							Echo"</td><td><input type=""text"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""" /></td></tr>"
						Else
							Fn_i=0
							do while not tmpRs.eof
							Fn_i=Fn_i+1
							if Fn_i=1 Then
								Echo"<tr class=data><td style=""text-align:center;"">"& Lang_Default &"</td><td>"
							Else
								Echo"<tr class=data><td style=""text-align:center;""><a onclick=""DeleteCurrentRow(this)"" href=""javascript:void(0);""><b class=""icon icon_del""></b></a><td>"
							End If
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_AttrID&""" />"
							Echo"<input type=""text"" class=""TxtClass"" name=""Attr_Value_List[]"" value="""&KnifeCMS.Data.UnEscape(tmpRs(0))&""" />"
							Echo"</td><td><input type=""text"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""&tmpRs(1)&""" /></td></tr>"
							tmpRs.movenext
							loop
						End If
						Echo"<tbody></table>"
					Else
						If (tmpRs.bof and tmpRs.eof) Then
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_AttrID&""" />"
							Echo"<input type=""text"" class=""TxtClass"" name=""Attr_Value_List[]"" value="""" />"
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""" />"
						Else
							do while not tmpRs.eof
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_AttrID&""" />"
							Echo"<input type=""text"" class=""TxtClass"" name=""Attr_Value_List[]"" value="""&KnifeCMS.Data.UnEscape(tmpRs(0))&""" />"
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""&tmpRs(1)&""" />"
							tmpRs.movenext
							loop
						End If
					End If
				Case 1
					If Fn_AttrType > 0 Then
					  Echo"<table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""listTable""><tbody><tr class=top><th>"
					  Echo"<div style=""width:90px;""><span class=sysBtn><a onclick=""Admin.Goods.AddGoodsAttr(this)"" href=""javascript:void(0);""><b class=""icon icon_add""></b>"& Lang_DoAddOne &"</a></span></div>"
					  Echo"</th><th><div style=""width:100px;"">"& Lang_AttrValue &"</div></th><th><div style=""width:200px;"">"& Lang_AttrPrice &"</div></th></tr>"
					  If (tmpRs.bof and tmpRs.eof) Then
						  Echo"<tr class=data><td style=""text-align:center;"">"& Lang_Default &"</td><td>"
						  Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_AttrID&""" />"
						  Fn_TempArray=split(Fn_AttrValues,"\n")
						  Echo"<select name=""Attr_Value_List[]"" style=""font-size:13px;""><option value="""">"& Lang_PleaseSelect &"</option>"
						  for Fn_i=0 to Ubound(Fn_TempArray) : Echo"<option value='"&Fn_TempArray(Fn_i)&"'>"&Fn_TempArray(Fn_i)&"</option>" : next
						  Echo"</select>"
						  Echo"</td><td>"
						  Echo"<input type=""text"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""" /></td></tr>"
					  Else
						  Fn_i=0
						  do while not tmpRs.eof
						  Fn_i=Fn_i+1
						  if Fn_i=1 Then
							  Echo"<tr class=data><td style=""text-align:center;"">"& Lang_Default &"</td><td>"
						  Else
							  Echo"<tr class=data><td style=""text-align:center;""><a onclick=""DeleteCurrentRow(this)"" href=""javascript:void(0);""><b class=""icon icon_del""></b></a><td>"
						  End If
						  Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_AttrID&""" />"
						  Fn_TempArray=split(Fn_AttrValues,"\n")
						  Echo"<select name=""Attr_Value_List[]"" style=""font-size:13px;""><option value="""">"& Lang_PleaseSelect &"</option>"
						  for Fn_i=0 to Ubound(Fn_TempArray)
							  tmpString=KnifeCMS.Data.UnEscape(Trim(tmpRs(0)))
							  If tmpString=Trim(Fn_TempArray(Fn_i)) Then
								  Echo"<option selected value='"&tmpString&"'>"&tmpString&"</option>"
							  Else
								  Echo"<option value='"&Fn_TempArray(Fn_i)&"'>"&Fn_TempArray(Fn_i)&"</option>"
							  End If
						  next
						  Echo"</select>"
						  Echo"</td><td>"
						  Echo"<input type=""text"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""&tmpRs(1)&""" /></td></tr>"
						  tmpRs.movenext
						  loop
					  End If
					  Echo"<tbody></table>"
					Else
						If (tmpRs.bof and tmpRs.eof) Then
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_AttrID&""" />"
							Fn_TempArray=split(Fn_AttrValues,"\n")
							Echo"<select name=""Attr_Value_List[]"" style=""font-size:13px;""><option value="""">"& Lang_PleaseSelect &"</option>"
							for Fn_i=0 to Ubound(Fn_TempArray) : Echo"<option value='"&Fn_TempArray(Fn_i)&"'>"&Fn_TempArray(Fn_i)&"</option>" : next
							Echo"</select>"
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""" />"
						Else
							do while not tmpRs.eof
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_AttrID&""" />"
							Fn_TempArray=split(Fn_AttrValues,"\n")
							Echo"<select name=""Attr_Value_List[]"" style=""font-size:13px;""><option value="""">"& Lang_PleaseSelect &"</option>"
							for Fn_i=0 to Ubound(Fn_TempArray)
								tmpString=KnifeCMS.Data.UnEscape(Trim(tmpRs(0)))
								If tmpString=Trim(Fn_TempArray(Fn_i)) Then
									Echo"<option selected value='"&tmpString&"'>"&tmpString&"</option>"
								Else
									Echo"<option value='"&Fn_TempArray(Fn_i)&"'>"&Fn_TempArray(Fn_i)&"</option>"
								End If
							next
							Echo"</select>"
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""&tmpRs(1)&""" />"
							tmpRs.movenext
							loop
						End If
					End If
				Case 2
					If Fn_AttrType > 0 Then
					  Echo"<table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""listTable""><tbody><tr class=top><th>"
					  Echo"<div style=""width:90px;""><span class=sysBtn><a onclick=""Admin.Goods.AddGoodsAttr(this)"" href=""javascript:void(0);""><b class=""icon icon_add""></b>"& Lang_DoAddOne &"</a></span></div>"
					  Echo"</th><th><div style=""width:100px;"">"& Lang_AttrValue &"</div></th><th><div style=""width:200px;"">"& Lang_AttrPrice &"</div></th></tr>"
					  If (tmpRs.bof and tmpRs.eof) Then
						  Echo"<tr class=data><td style=""text-align:center;"">"& Lang_Default &"</td><td>"
						  Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_AttrID&""" />"
						  Echo"<textarea class=""mini"" name=""Attr_Value_List[]""></textarea>"
						  Echo"</td><td>"
						  Echo"<input type=""text"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""" /></td></tr>"
					  Else
						  Fn_i=0
						  do while not tmpRs.eof
						  Fn_i=Fn_i+1
						  if Fn_i=1 Then
							  Echo"<tr class=data><td style=""text-align:center;"">"& Lang_Default &"</td><td>"
						  Else
							  Echo"<tr class=data><td style=""text-align:center;""><a onclick=""DeleteCurrentRow(this)"" href=""javascript:void(0);""><b class=""icon icon_del""></b></a><td>"
						  End If
						  Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_AttrID&""" />"
						  Echo"<textarea class=""mini"" name=""Attr_Value_List[]"">"&KnifeCMS.Data.UnEscape(tmpRs(0))&"</textarea>"
						  Echo"</td><td>"
						  Echo"<input type=""text"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""&tmpRs(1)&""" /></td></tr>"
						  tmpRs.movenext
						  loop
					  End If
					  Echo"<tbody></table>"
					Else
						If (tmpRs.bof and tmpRs.eof) Then
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_AttrID&""" />"
							Echo"<textarea class=""mini"" name=""Attr_Value_List[]""></textarea>"
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""" />"
						Else
							do while not tmpRs.eof
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_AttrID&""" />"
							Echo"<textarea class=""mini"" name=""Attr_Value_List[]"">"&KnifeCMS.Data.UnEscape(tmpRs(0))&"</textarea>"
							Echo"<input type=""hidden"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""&tmpRs(1)&""" />"
							tmpRs.movenext
							loop
						End If
					End If
				Case else
					Echo(""&TablePre&"GoodsModelAttribute.AttrInputType in Database is Wrong!")
				End Select
				Set tmpRs=nothing
				Echo"</td></tr>"
				Rs.movenext
			loop
			Echo("</body></table>")
		End If
		Set Rs=nothing
	End Function
	
	'获取商品配件
	Private Function GetGoodsAdjunct(ByVal BV_GoodsID)
		BV_GoodsID=KnifeCMS.Data.CLng(BV_GoodsID)
		If BV_GoodsID=0 Then Exit Function
		Dim Fn_i : Fn_i=1
		Pv_TempHTML=""
		Dim Adjunct,Fn_AdjunctID,Fn_AdjunctName,Fn_AdjunctMinNum,Fn_AdjunctMaxNum,Fn_AdjunctPriceType,Fn_AdjunctPrice,Fn_AdjunctGoodsID,Fn_j
		Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,AdjunctName,MinNum,MaxNum,PriceType,Price FROM ["& TablePre &"GoodsAdjunct] WHERE GoodsID="& BV_GoodsID &"")
		If not(Rs.eof) Then
			Do while not Rs.eof
			Fn_AdjunctID        = Rs("ID")
			Fn_AdjunctName      = Rs("AdjunctName")
			Fn_AdjunctMinNum    = Rs("MinNum")
			Fn_AdjunctMaxNum    = Rs("MaxNum")
			Fn_AdjunctPriceType = Rs("PriceType")
			Fn_AdjunctPrice     = Rs("Price")
			Pv_TempHTML=Pv_TempHTML&"<div id=""Adjunct_"&Fn_i&""" class=""inbox Adjunct"">"
			Pv_TempHTML=Pv_TempHTML&"<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""infoTable""><tbody>"
			Pv_TempHTML=Pv_TempHTML&"<tr><td class=""titletd"">"& Lang_Goods(26) &"<!--配件组名称-->:</td><td class=""infotd"">"
			Pv_TempHTML=Pv_TempHTML&"<input type=""text"" class=""TxtClass"" adjunctid="& Fn_AdjunctID &" name=""AdjunctName_"&Fn_i&""" value="""& KnifeCMS.Data.HTMLDecode(Fn_AdjunctName) &""" />"
			Pv_TempHTML=Pv_TempHTML&"<span class=""sysBtn""><a href=""javascript:void(0);"" onClick=""Admin.Goods.delAdjunctGroup(this)""><b class=""icon icon_del""></b>"& Lang_Goods(25) &"<!--删除此组配件--></a></span>"
			Pv_TempHTML=Pv_TempHTML&"</td></tr>"
			Pv_TempHTML=Pv_TempHTML&"<tr><td class=""titletd"">"& Lang_Goods(27) &"<!--最小购买量-->:</td><td class=""infotd""><input type=""text"" class=""TxtClass Short"" name=""AdjunctMinNum_"&Fn_i&""" value="""&Fn_AdjunctMinNum&""" /> <label class=""Normal"">"& Lang_Goods_Cue(14) &"<!--0表示不限制--></label></td></tr>"
			Pv_TempHTML=Pv_TempHTML&"<tr><td class=""titletd"">"& Lang_Goods(28) &"<!--最大购买量-->:</td><td class=""infotd""><input type=""text"" class=""TxtClass Short"" name=""AdjunctMaxNum_"&Fn_i&""" value="""&Fn_AdjunctMaxNum&""" /> <label class=""Normal"">"& Lang_Goods_Cue(14) &"<!--0表示不限制--></label></td></tr>"
			Pv_TempHTML=Pv_TempHTML&"<tr><td class=""titletd"">"& Lang_Goods(29) &"<!--优惠方式-->:</td><td class=""infotd"">"
			If Not(Fn_AdjunctPriceType>0) Then
			Pv_TempHTML=Pv_TempHTML&"<input type=""radio"" name=""AdjunctPriceType_"&Fn_i&""" value=""0"" checked=""checked"" />"& Lang_Goods(30) &"<!--优惠一定金额--> &nbsp; <input type=""radio"" name=""AdjunctPriceType_"&Fn_i&""" value=""1"" />"& Lang_Goods(31) &"<!--优惠价为某个折扣-->"
			Else
			Pv_TempHTML=Pv_TempHTML&"<input type=""radio"" name=""AdjunctPriceType_"&Fn_i&""" value=""0"" />"& Lang_Goods(30) &"<!--优惠一定金额--> &nbsp; <input type=""radio"" name=""AdjunctPriceType_"&Fn_i&""" value=""1"" checked=""checked"" />"& Lang_Goods(31) &"<!--优惠价为某个折扣-->"
			End If
			Pv_TempHTML=Pv_TempHTML&"</td></tr>"
			Pv_TempHTML=Pv_TempHTML&"<tr><td class=""titletd"">"& Lang_Goods(32) &"<!--优惠额度-->:</td><td class=""infotd""><input type=""text"" class=""TxtClass Short"" name=""AdjunctPrice_"&Fn_i&""" value="""&Fn_AdjunctPrice&""" /> <label class=""Normal"">"& Lang_Goods_Cue(15) &"<!--(无优惠可不填；优惠100元就输入100，优惠折扣价为原来的8.8折就输入0.88)--></label></td></tr>"
			Pv_TempHTML=Pv_TempHTML&"<tr><td class=""titletd"" valign=""top"">"& Lang_Goods(33) &"<!--添加配件商品-->:</td><td class=""infotd"">"
			Pv_TempHTML=Pv_TempHTML&"<div class=""toparea""><span class=""sysBtn""><a href=""#"" onClick=""Admin.Goods.getGoods('Adjunct','AdjunctGoods_"&Fn_i&"','AdjunctGoodsID[]_"&Fn_i&"')""><b class=""icon icon_add""></b>"& Lang_Goods(34) &"<!--填加配件--></a></span></div>"
			Pv_TempHTML=Pv_TempHTML&"<div class=""tabledata2"">"
			Pv_TempHTML=Pv_TempHTML&"<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""listTable""><tbody id=""AdjunctGoods_"&Fn_i&""">"
			Pv_TempHTML=Pv_TempHTML&"<tr class=""top""><th width=""2%""><div style=""width:30px;"">"& Lang_DoDel &"<!--删除--></div></th><th width=""3%""><div style=""width:50px;"">"& Lang_ListTableColLine_ID &"<!--系统ID--></div></th><th width=""15%""><div style=""width:160px;"">"& Lang_Goods_ListTableColLine(0) &"<!--商品编号--></div></th><th width=""95%""><div style=""width:200px;"">"& Lang_Goods_ListTableColLine(1) &"<!--商品名称--></div></th></tr>"
			tmpSql="SELECT "&TablePre&"GoodsAdjunctGoods.AdjunctGoodsID,"&TablePre&"Goods.GoodsBN,"&TablePre&"Goods.GoodsName FROM ["&TablePre&"GoodsAdjunctGoods] LEFT JOIN "& TablePre &"Goods ON ("&TablePre&"Goods.ID="&TablePre&"GoodsAdjunctGoods.AdjunctGoodsID) WHERE "&TablePre&"GoodsAdjunctGoods.GoodsID="&BV_GoodsID&" AND "&TablePre&"GoodsAdjunctGoods.AdjunctID="&Fn_AdjunctID&""
			Set tmpRs = KnifeCMS.DB.GetRecordBySQL(tmpSql)
			If not(Rs.eof) Then
				Do while not tmpRs.eof
				Pv_TempHTML=Pv_TempHTML&"<tr class=""data""><td><a onclick=""DeleteCurrentRow(this)"" href=""javascript:;""><b class=""icon icon_del""></b></a></td><td><input type=""hidden"" name=""AdjunctGoodsID[]_"&Fn_i&""" value="""&tmpRs(0)&""">"&tmpRs(0)&"</td><td>"&tmpRs(1)&"</td><td>"&tmpRs(2)&"</td></tr>"
				tmpRs.movenext
				Loop
			End If
			Set tmpRs=nothing
			Pv_TempHTML=Pv_TempHTML&"</tbody></table>"
			Pv_TempHTML=Pv_TempHTML&"</div>"
			Pv_TempHTML=Pv_TempHTML&"</td></tr>"
			Pv_TempHTML=Pv_TempHTML&"</tbody></table>"
			Pv_TempHTML=Pv_TempHTML&"</div>"
			Fn_i=Fn_i+1
			Rs.movenext
			Loop
		End If
		Set Rs=nothing
		Echo Pv_TempHTML
	End Function
	
	Private Function GetGoodsLink(ByVal BV_GoodsID)
		BV_GoodsID=KnifeCMS.Data.CLng(BV_GoodsID)
		If BV_GoodsID=0 Then Exit Function
		Pv_TempHTML=""
		Sql="SELECT "&TablePre&"GoodsLink.LinkGoodsID,"&TablePre&"Goods.GoodsBN,"&TablePre&"Goods.GoodsName FROM "&TablePre&"GoodsLink LEFT JOIN "& TablePre &"Goods ON ("&TablePre&"Goods.ID="&TablePre&"GoodsLink.LinkGoodsID) WHERE "&TablePre&"GoodsLink.GoodsID="&BV_GoodsID&""
		Set Rs = KnifeCMS.DB.GetRecordBySQL(Sql)
		Do while not Rs.eof
			Pv_TempHTML = Pv_TempHTML & "<tr class=""data""><td><a onclick=""DeleteCurrentRow(this)"" href=""#""><b class=""icon icon_del""></b></a></td><td><input type=""hidden"" name=""LinkGoodsID[]"" value="""&Rs(0)&""">"&Rs(0)&"</td><td>"&Rs(1)&"</td><td>"&Rs(2)&"</td></tr>"
			Rs.movenext
		Loop
		KnifeCMS.DB.CloseRs Rs
		Echo Pv_TempHTML
	End Function
	
	'$:生成未被使用的唯一的商品编号。
	Private Function CreateGoodsBN()
		Dim Fn_i,Fn_Rs,Fn_TempBN,Fn_OK : Fn_OK = false
		KnifeCMS.MD5.MD5Bits = 16
		For Fn_i=0 to 100000
			Fn_TempBN = UCase("G"&KnifeCMS.MD5.Encrypt(KnifeCMS.CreateSysID()))
			If KnifeCMS.Data.IsNul(Fn_TempBN) Then Admin.ShowMessage "Create GoodsBN Wrong.","goback",0 : Exit Function
			'Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Goods,Array("GoodsBN:"&Fn_TempBN),"")
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_Goods &"] WHERE GoodsBN='"& Fn_TempBN &"'")
			If Fn_Rs.Eof Then
				Fn_OK = true
			Else
				Fn_TempBN = UCase("G"&KnifeCMS.MD5.Encrypt(KnifeCMS.CreateSysID()))
			End If
			If Fn_OK Then Exit For
		Next
		KnifeCMS.MD5.MD5Bits = 32
		CreateGoodsBN = Fn_TempBN
	End Function
	
	Private Function GoodsDoSave()
		Dim Fn_HaveUsered
		'获取数据并处理数据
		GoodsType     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","GoodsType"))
		ClassID       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","ClassID"))
		GoodsName     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","GoodsName")),250)
		GoodsBN       = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","GoodsBN"),"[^0-9a-zA-Z]",""),32)
		GoodsSN       = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","GoodsSN"),"[^0-9a-zA-Z]",""),32)
		Marketable    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Marketable"))
		GoodsPrice    = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","GoodsPrice"))
		GoodsCost     = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","GoodsCost"))
		GoodsMktPrice = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","GoodsMktPrice"))
		GoodsWeight   = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","GoodsWeight"))
		GoodsStore    = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","GoodsStore"))
		GoodsUnit     = Cstr(KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","GoodsUnit")),50))
		BrandID       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","BrandID"))
		Thumbnail     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Thumbnail")),250)
		GoodsFirstImg = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","GoodsImg[]FirstImg")),250)
		For Each GoodsImg_i In Request.Form("GoodsImg[]")
			GoodsImg  = KnifeCMS.IIF( GoodsImg = "" , KnifeCMS.Data.HTMLEncode(Trim(GoodsImg_i)) , GoodsImg &","& KnifeCMS.Data.HTMLEncode(Trim(GoodsImg_i)))
		Next
		Abstract      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Abstract")),250)
		
		TextContent   = KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","TextContent"))
		
		GoodsAttr     = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","GoodsAttr"),20000)
		GoodsModelID  = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","GoodsModel"))
		GoodsAdjunct  = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","GoodsAdjunct"),20000)
		LinkGoodsID   = KnifeCMS.GetForm("post","LinkGoodsID[]")
		
		'SEO优化内容
		SeoTitle            = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","SeoTitle")),250)        : If KnifeCMS.Data.IsNul(SeoTitle) Then SeoTitle=GoodsName
		SeoUrl              = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","SeoUrl")),250)
		MetaKeywords        = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","MetaKeywords")),250)
		MetaDescription     = KnifeCMS.Data.ParseMetaDescription(KnifeCMS.GetForm("post","MetaDescription"))
		
		'商品标签
		Pv_Recommend         = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Recommend"))
		Pv_Hot               = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Hot"))
		Pv_New               = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","New"))
		Pv_HomepageRecommend = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","HomepageRecommend"))
		
		'数据合法性检测并处理数据
		If GoodsType < 1                     Then ErrMsg = Lang_Goods_Cue(20) &"<br>"
		If ClassID   < 1                     Then ErrMsg = ErrMsg & Lang_Goods_Cue(21) &"<br>"
		If KnifeCMS.Data.IsNul(GoodsName)     Then ErrMsg = ErrMsg & Lang_Goods_Cue(22) &"<br>"
		If KnifeCMS.Data.IsNul(GoodsFirstImg) And Not(KnifeCMS.Data.IsNul(GoodsImg)) Then GoodsFirstImg = Split(GoodsImg,",")(0)
		If KnifeCMS.Data.IsNul(GoodsBN) Then
			GoodsBN = CreateGoodsBN
		Else
			Fn_HaveUsered = Admin.IsGoodsBNHaveUsered(GoodsBN,ID)
		End If
		If Fn_HaveUsered Then ErrMsg = ErrMsg & Replace(Lang_Goods_Cue(22),"{tpl:GoodsBN}",GoodsBN) &"<br>"

		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		IF Action="add" Then
			Result = KnifeCMS.DB.AddRecord(DBTable_Goods,Array("SysID:"&SysID,"GoodsType:"& GoodsType,"ClassID:"& ClassID,"GoodsName:"& GoodsName,"GoodsBN:"&GoodsBN,"GoodsSN:"& GoodsSN,"Marketable:"& Marketable,"Price:"& GoodsPrice,"Cost:"& GoodsCost,"MktPrice:"& GoodsMktPrice,"Weight:"& GoodsWeight,"Store:"& GoodsStore,"Unit:"& GoodsUnit,"BrandID:"& BrandID,"Thumbnail:"& Thumbnail,"FirstImg:"& GoodsFirstImg,"Imgs:"& GoodsImg,"Abstract:"& Abstract,"Intro:"& TextContent,"GoodsModel:"& GoodsModelID,"SeoTitle:"& SeoTitle,"SeoUrl:"& SeoUrl,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription,"Hits:0","Diggs:0","Recommend:"& Pv_Recommend,"Hot:"& Pv_Hot,"New:"& Pv_New,"HomepageRecommend:"& Pv_HomepageRecommend,"AddTime:"& SysTime,"Recycle:0"))
			ID  = Admin.GetIDBySysID(DBTable_Goods,SysID)
			Call AddGoodsAttr(ID,GoodsAttr)
			Call AddGoodsAdjunct(ID,GoodsAdjunct)
			Call AddGoodsLink(ID,LinkGoodsID)
			
			'添加自定义字段数据
			If Result And ID>0 Then Call SaveFieldSetValue()
			
			'更新缓存
			If IsCache Then
				If Marketable>0 Then
				'商品需要上架后才可以生成缓存,不然会出错!
					Print "<div class=""creating_cache"">"
					Call Admin.CreateCache_Init()
					Call Admin.CreateStaticHTML_Shop(False,ID,0)
					Call Admin.CreateCache_Terminate()
					Print "</div>"
				End If
			End If
			
			Msg = Lang_Goods_Cue(1) &"<br>["& Lang_Goods(3) &":"& GoodsName &"]<br>["& Lang_Goods(4) &":"& GoodsBN &"]<br>[ID:"& ID &"]"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		ElseIf Action="edit" then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Goods,Array("GoodsType:"& GoodsType,"ClassID:"& ClassID,"GoodsName:"& GoodsName,"GoodsBN:"&GoodsBN,"GoodsSN:"& GoodsSN,"Marketable:"& Marketable,"Price:"& GoodsPrice,"Cost:"& GoodsCost,"MktPrice:"& GoodsMktPrice,"Weight:"& GoodsWeight,"Store:"& GoodsStore,"Unit:"& GoodsUnit,"BrandID:"& BrandID,"Thumbnail:"& Thumbnail,"FirstImg:"& GoodsFirstImg,"Imgs:"& GoodsImg,"Abstract:"& Abstract,"Intro:"& TextContent,"GoodsModel:"& GoodsModelID,"SeoTitle:"& SeoTitle,"SeoUrl:"& SeoUrl,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription,"Recommend:"& Pv_Recommend,"Hot:"& Pv_Hot,"New:"& Pv_New,"HomepageRecommend:"& Pv_HomepageRecommend),Array("ID:"&ID))
			Call KnifeCMS.DB.DeleteRecord(DBTable_GoodsAdjunctGoods,Array("GoodsID:"&ID))
			Call KnifeCMS.DB.DeleteRecord(DBTable_GoodsLink,Array("GoodsID:"&ID))
			Call AddGoodsAttr(ID,GoodsAttr)
			Call AddGoodsAdjunct(ID,GoodsAdjunct)
			Call AddGoodsLink(ID,LinkGoodsID)
			
			'更新自定义字段数据
			If Result And ID>0 Then Call SaveFieldSetValue()

			'更新缓存
			If IsCache Then
				If Marketable>0 Then
				'商品需要上架后才可以生成缓存,不然会出错!
					Print "<div class=""creating_cache"">"
					Call Admin.CreateCache_Init()
					Call Admin.CreateStaticHTML_Shop(False,ID,0)
					Call Admin.CreateCache_Terminate()
					Print "</div>"
				End If
			End If
			
			Msg = Lang_Goods_Cue(2) & "<br>["&  Lang_Goods(3) &":"& GoodsName &"]<br>["&  Lang_Goods(4) &":"& GoodsBN &"]<br>[ID:"& ID &"]"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		End IF
	End Function
	
	'保存自定义表单值
	Public Function SaveFieldSetValue()
		Dim Fn_Rs
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_GoodsFieldSet &"] WHERE Recycle=0 ORDER BY OrderNum ASC")
	    Do While Not(Fn_Rs.Eof)
			Pv_Field      = Fn_Rs("Field")
			Pv_FieldType  = Fn_Rs("FieldType")
			Pv_FieldValue = Admin.ParseFieldSetValue(Pv_FieldType,Pv_Field)
			Call KnifeCMS.DB.UpdateRecord(DBTable_Goods,Array(Pv_Field &":"& Pv_FieldValue),Array("ID:"&ID))
			Fn_Rs.MoveNext
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
	End Function
	
	'$:插入商品属性到
	Private Function AddGoodsAttr(ByVal BV_ID, ByVal BV_GoodsAttr)
		BV_ID = KnifeCMS.Data.CLng(BV_ID)
		If KnifeCMS.Data.IsNul(BV_GoodsAttr) Or BV_ID < 1 Then
			AddGoodsAttr = 0
			Call KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_GoodsAttr &"] WHERE GoodsID="& BV_ID &"")
			Exit Function
		End If
		Dim Fn_Result,Fn_SQL,Fn_i,Fn_BigArr,Fn_SmlArr : Fn_BigArr=split(BV_GoodsAttr,"|")
		Dim Fn_AttrID,Fn_GoodsAttrID,Fn_GoodsAttrID_All,Fn_AttrValue,Fn_AttrPrice
		For Fn_i=0 to ubound(Fn_BigArr)
			Fn_SmlArr      = Split(Fn_BigArr(Fn_i),":")
			Fn_Result      = 0
			Fn_GoodsAttrID = 0
			If Ubound(Fn_SmlArr) = 2 Then
				Fn_AttrID    = KnifeCMS.Data.CLng(Fn_SmlArr(0))
				Fn_AttrValue = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.Data.UnEscape(Trim(Fn_SmlArr(1)))),250)
				Fn_AttrPrice = KnifeCMS.Data.FormatDouble(Trim(Fn_SmlArr(2)))
				If KnifeCMS.Data.IsNul(Fn_AttrPrice) Then Fn_AttrPrice="0"
				If Not(KnifeCMS.Data.IsNul(Fn_AttrValue)) And Fn_AttrID > 0 Then
					'检测属性是否存在，存在则更新，不存在则增加新记录
					Fn_GoodsAttrID = KnifeCMS.Data.CLng(KnifeCMS.DB.GetFieldByArrayField(DBTable_GoodsAttr,"ID",Array("GoodsID:"& BV_ID,"AttrID:"& Fn_AttrID,"AttrValue:"& Fn_AttrValue)))
					If Fn_GoodsAttrID>0 Then
						Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_GoodsAttr,Array("AttrPrice:"& Fn_AttrPrice),Array("GoodsID:"& BV_ID,"AttrID:"& Fn_AttrID,"AttrValue:"& Fn_AttrValue))
					End If
					If Fn_Result=0 Then
						Call KnifeCMS.DB.AddRecord(DBTable_GoodsAttr,Array("GoodsID:"&BV_ID,"AttrID:"&Fn_AttrID,"AttrValue:"&Fn_AttrValue,"AttrPrice:"& Fn_AttrPrice))
						Fn_GoodsAttrID = KnifeCMS.DB.GetFieldByArrayField(DBTable_GoodsAttr,"ID",Array("GoodsID:"& BV_ID,"AttrID:"& Fn_AttrID,"AttrValue:"& Fn_AttrValue))
					End If
					''''''''
				End If
			End If
			If KnifeCMS.Data.IsNul(Fn_GoodsAttrID_All) Then Fn_GoodsAttrID_All = Fn_GoodsAttrID : Else Fn_GoodsAttrID_All = Fn_GoodsAttrID_All &","& Fn_GoodsAttrID
		Next
		'删除已经不存在的属性
		Fn_SQL = "DELETE FROM ["& DBTable_GoodsAttr &"] WHERE GoodsID="& BV_ID &" AND ID NOT IN("& Fn_GoodsAttrID_All &")" ' : Echo Fn_SQL &"<br>"
		Call KnifeCMS.DB.Execute(Fn_SQL)
		AddGoodsAttr = 1
	End Function
	
	'$:插入商品配件
	Private Function AddGoodsAdjunct(ByVal BV_ID,ByVal BV_GoodsAdjunct)
		BV_ID = KnifeCMS.Data.CLng(BV_ID)
		If KnifeCMS.Data.IsNul(BV_GoodsAdjunct) Or BV_ID < 1 Then
			AddGoodsAdjunct = 0
			Call KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_GoodsAdjunct &"] WHERE GoodsID="& BV_ID &"")
			Exit Function 
		End If
		'die BV_GoodsAdjunct
		Dim Fn_Result
		Dim Fn_SQL,Fn_SysID,Fn_Arr,Fn_ArrAdjunct : Fn_ArrAdjunct=split(BV_GoodsAdjunct,"|")
		Dim Fn_Adjunct,Fn_AdjunctID,Fn_AdjunctID_All,Fn_AdjunctName,Fn_AdjunctType,Fn_AdjunctMinNum,Fn_AdjunctMaxNum,Fn_AdjunctPriceType,Fn_AdjunctPrice,Fn_AdjunctGoodsID
		Dim Fn_i,Fn_j
		For Fn_i=0 to Ubound(Fn_ArrAdjunct)
			Fn_Adjunct   = Split(Fn_ArrAdjunct(Fn_i),":")
			Fn_Result    = 0
			Fn_AdjunctID = 0
			If Ubound(Fn_Adjunct)=6 And not(KnifeCMS.Data.IsNul(Fn_Adjunct(6))) Then
				Fn_AdjunctID        = KnifeCMS.Data.CLng(Trim(Fn_Adjunct(0)))
				Fn_AdjunctName      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.Data.UnEscape(Trim(Fn_Adjunct(1)))),50)
				Fn_AdjunctType      = "packs"
				Fn_AdjunctMinNum    = KnifeCMS.Data.CLng(Fn_Adjunct(2))
				Fn_AdjunctMaxNum    = KnifeCMS.Data.CLng(Fn_Adjunct(3))' : If Fn_AdjunctMaxNum=0 then Fn_AdjunctMaxNum=""
				Fn_AdjunctPriceType = KnifeCMS.Data.CLng(Fn_Adjunct(4))
				Fn_AdjunctPrice     = KnifeCMS.Data.FormatDouble(Fn_Adjunct(5))
				Fn_AdjunctGoodsID   = Fn_Adjunct(6)
				Fn_SysID            = KnifeCMS.CreateSysID()
				'检查配件组是否存在，存在则更新，不存在则添加
				If Fn_AdjunctID>0 Then
					If KnifeCMS.DB.GetFieldByArrayField(DBTable_GoodsAdjunct,"ID",Array("ID:"& Fn_AdjunctID,"GoodsID:"& BV_ID))>0 Then
						Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_GoodsAdjunct,Array("AdjunctName:"& Fn_AdjunctName,"AdjunctType:"& Fn_AdjunctType,"MinNum:"& Fn_AdjunctMinNum,"MaxNum:"& Fn_AdjunctMaxNum,"PriceType:"& Fn_AdjunctPriceType,"Price:"& Fn_AdjunctPrice,"Disable:0" ),Array("ID:"& Fn_AdjunctID,"GoodsID:"& BV_ID))
					End If
				End If
				If Fn_Result=0 Then
					Call KnifeCMS.DB.AddRecord(DBTable_GoodsAdjunct,Array("SysID:"& Fn_SysID,"GoodsID:"& BV_ID,"AdjunctName:"& Fn_AdjunctName,"AdjunctType:"& Fn_AdjunctType,"MinNum:"& Fn_AdjunctMinNum,"MaxNum:"& Fn_AdjunctMaxNum,"PriceType:"& Fn_AdjunctPriceType,"Price:"& Fn_AdjunctPrice,"Disable:0" ))
					Fn_AdjunctID = Admin.GetIDBySysID(DBTable_GoodsAdjunct,Fn_SysID)
				End If
				''''''
				If Fn_AdjunctID > 0 then
					Fn_Arr=Split(Fn_AdjunctGoodsID,",")
					For Fn_j=0 to ubound(Fn_Arr)
						Fn_AdjunctGoodsID = KnifeCMS.Data.CLng(Fn_Arr(Fn_j))
						If Fn_AdjunctGoodsID > 0 then
							Call KnifeCMS.DB.AddRecord(DBTable_GoodsAdjunctGoods,Array("GoodsID:"& BV_ID,"AdjunctID:"&Fn_AdjunctID,"AdjunctGoodsID:"&Fn_AdjunctGoodsID))
						End If
					Next
				End If
			End If
			If KnifeCMS.Data.IsNul(Fn_AdjunctID_All) Then Fn_AdjunctID_All = Fn_AdjunctID : Else Fn_AdjunctID_All = Fn_AdjunctID_All &","& Fn_AdjunctID
		Next
		Fn_SQL = "DELETE FROM ["& DBTable_GoodsAdjunct &"] WHERE GoodsID="& BV_ID &" AND ID NOT IN("& Fn_AdjunctID_All &")" ' : Echo Fn_SQL &"<br>"
		Call KnifeCMS.DB.Execute(Fn_SQL)
		AddGoodsAdjunct = 1
	End Function
	
	'$:插入相关商品
	Private Function AddGoodsLink(ByVal BV_ID,ByVal Fn_LinkID)
		BV_ID = KnifeCMS.Data.CLng(BV_ID)
		If KnifeCMS.Data.IsNul(Fn_LinkID) Or BV_ID < 1 Then
			AddGoodsLink = 0
			Exit Function 
		End If
		Dim Fn_i,Fn_TempID,Fn_Arr : Fn_Arr=split(Fn_LinkID,",")
		For Fn_i=0 to ubound(Fn_Arr)
			Fn_TempID=KnifeCMS.Data.CLng(Fn_Arr(Fn_i))
			If Fn_TempID > 0 then
				Call KnifeCMS.DB.AddRecord(TablePre&"GoodsLink",Array("GoodsID:"&BV_ID,"LinkGoodsID:"&Fn_TempID))
			End If
		Next
		AddGoodsLink = 1
	End Function
	
	'$:虚拟分类商品添加保存
	Function VirtualClassGoodsDoSave()
		Dim Fn_ArrID,Fn_TempSql,Fn_i
		Dim Fn_Check : Fn_Check = true
		If VirtualClassID < 1 Then
			Admin.ShowMessage "Error: VirtualClassID < 1 .",Array("javascript:WinClose();",Lang_WinClose),0
			Exit Function
		End If
		If ActionType = "selectresult" Or ActionType = "searchresult" Then'将选择项加入
			Pv_TempID = KnifeCMS.GetForm("post","InputName")
			If ActionType = "searchresult" Then 
				Fn_TempSql = "SELECT a.ID FROM "&DBTable_Goods&" a WHERE a.Recycle=0"
					Admin.Search.ClassID      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","classid"))
					Admin.Search.CollectionID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","collectionid"))
					Admin.Search.GoodsType    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","goodstype"))
					Admin.Search.BrandID      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","brandid"))
					Admin.Search.KeyWords     = KnifeCMS.Data.Left(KnifeCMS.GetForm("both","keywords"),50)
					Admin.Search.StartDate    = KnifeCMS.Data.FormatDateTime(KnifeCMS.GetForm("both","startdate"),0)
					Admin.Search.EndDate      = KnifeCMS.Data.FormatDateTime(KnifeCMS.GetForm("both","enddate"),2)
					If Admin.Search.EndDate   = "" Then Admin.Search.EndDate = KnifeCMS.Data.FormatDateTime(SysTime,2)
					Admin.Search.EndDate = Admin.Search.EndDate &" 23:59:59"
					If DateDiff("d",Admin.Search.StartDate,Admin.Search.EndDate) < 0 Then Admin.ShowMessage Lang_Cue_SearchDateErr,"goback",0 : Die ""
					
					If Admin.Search.ClassID > 0 Then Admin.GetChildClass DBTable_GoodsClass,Admin.Search.ClassID
					tempCondition =                 KnifeCMS.IIF(Admin.Search.ClassID  > 0 ," AND a.ClassID IN("&Admin.Search.ClassID&Admin.ChildClass&") "," ")
					tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.GoodsType> 0 ," AND a.GoodsType="&Admin.Search.GoodsType&" "," ")
					tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.BrandID  > 0 ," AND a.BrandID="&Admin.Search.BrandID&" "," ")
					tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.KeyWords <>""," AND a.GoodsBN like '%"& Admin.Search.KeyWords &"%' Or GoodsName like '%"& Admin.Search.KeyWords &"%' "," ")
					Select Case DB_Type
					Case 0
					tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.AddTime>=#"& Admin.Search.StartDate &"# ","")
					tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.AddTime<=#"& Admin.Search.EndDate &"# ","")
					Case 1
					tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.AddTime>='"& Admin.Search.StartDate &"' ","")
					tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.AddTime<='"& Admin.Search.EndDate &"' ","")
					End Select
					
					
					tempCondition = tempCondition & GoodsSearchSql
					PageUrlPara = PageUrlPara & "&goodstype="& Admin.Search.GoodsType &"&classid="& Admin.Search.ClassID &"&startdate="& Server.URLEncode(Admin.Search.StartDate) &"&enddate="& Server.URLEncode(Admin.Search.EndDate) &"&keywords="& Server.URLEncode(Admin.Search.KeyWords)&"&PriceFrom="&PriceFrom&"&PriceTo="&PriceTo&"&CostFrom="&CostFrom&"&CostTo="&CostTo&"&MktPriceFrom"&MktPriceFrom&"&MktPriceTo="&MktPriceTo&"&StoreFrom="&StoreFrom&"&StoreTo="&StoreTo&""
					Fn_TempSql = Fn_TempSql & tempCondition
				Set Rs = KnifeCMS.DB.GetRecordBySQL(Fn_TempSql)
				Do While Not(Rs.Eof)
					Pv_TempID = KnifeCMS.IIF(Pv_TempID<>"",Pv_TempID&","&Rs(0),Rs(0))
					Rs.Movenext()
				Loop
				KnifeCMS.DB.CloseRs Rs
			End If
			Fn_ArrID  = Split(Pv_TempID,",")
			for Fn_i=0 to Ubound(Fn_ArrID)
				Pv_TempID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
				If Pv_TempID > 0 then
					Fn_Check  = true
					GoodsName = ""
					Result    = 0
					Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID FROM ["& DBTable_GoodsInVirtualClass &"] WHERE GoodsID="& Pv_TempID &" AND VirtualClassID="& VirtualClassID &" ")
					If Not(Rs.Eof) Then Fn_Check = false
					KnifeCMS.DB.CloseRs Rs
					If Fn_Check Then
						Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT GoodsName FROM ["& DBTable_Goods &"] WHERE ID="& Pv_TempID &"")
						If Not(Rs.Eof) Then
							GoodsName = Rs(0)
						Else
							Fn_Check  = false
						End If
						KnifeCMS.DB.CloseRs Rs
					End If
					If Fn_Check Then Result = KnifeCMS.DB.AddRecord(DBTable_GoodsInVirtualClass,Array("GoodsID:"&Pv_TempID,"VirtualClassID:"&VirtualClassID))
					If Result Then Msg = Msg & "[VirtualClassID="& VirtualClassID &"]"& Lang_GoodsInVirtualClass(1) &"{"& GoodsName &"[ID="& Pv_TempID &"]}<br>"
				End If
			next
			Admin.ReloadTopDialog
			If Msg = "" Then Msg = Lang_Goods_Cue(25)
			Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		End If
	End Function
	
	Private Function GoodsDel()
		GoodsSubDo
	End Function
	
	Private Function GoodsOtherEdit()
		GoodsSubDo
	End Function
	
	Private Sub GoodsSubDo()
		ID = KnifeCMS.GetForm("post","InputName")
		Dim Fn_ArrID,Fn_i
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			Result    = 0
			If ID > 0 Then
				Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT GoodsName FROM ["& DBTable_Goods &"] WHERE ID="& ID &"")
				If Not(Rs.Eof) Then GoodsName=Rs(0)
				KnifeCMS.DB.CloseRs Rs
				'开始执行操作
				If Action = "del" Then
					If SubAction = "completely" Then
						Result = KnifeCMS.DB.DeleteRecord(DBTable_GoodsAttr,Array("GoodsID:"&ID))
						Result = KnifeCMS.DB.DeleteRecord(DBTable_GoodsAdjunct,Array("GoodsID:"&ID))
						Result = KnifeCMS.DB.DeleteRecord(DBTable_GoodsAdjunctGoods,Array("GoodsID:"&ID))
						Result = KnifeCMS.DB.DeleteRecord(DBTable_GoodsLink,Array("GoodsID:"&ID))
						Result = KnifeCMS.DB.DeleteRecord(DBTable_GoodsLink,Array("LinkGoodsID:"&ID))
						Result = KnifeCMS.DB.DeleteRecord(DBTable_GoodsInVirtualClass,Array("GoodsID:"&ID))
						Result = KnifeCMS.DB.DeleteRecord(DBTable_Goods,Array("ID:"&ID))
						Msg    = Msg & Lang_Goods_Cue(4) &"{"& GoodsName &"[ID="& ID &"]}<br>"
					Else
						Result = KnifeCMS.DB.UpdateRecord(DBTable_Goods,Array("Recycle:1"),Array("ID:"&ID))
						Msg    = Msg & Lang_Goods_Cue(3) &"{"& GoodsName &"[ID="& ID &"]}<br>"
					End If
				ElseIf Action = "edit" Then
					If SubAction = "marketable" Then
						Result = KnifeCMS.DB.UpdateRecord(DBTable_Goods,Array("Marketable:1"),Array("ID:"&ID))
						Msg    = Msg & Lang_Goods_Cue(5) &"{"& GoodsName &"[ID="& ID &"]}<br>"
					ElseIf SubAction = "unmarketable" Then
						Result = KnifeCMS.DB.UpdateRecord(DBTable_Goods,Array("Marketable:0"),Array("ID:"&ID))
						Msg    = Msg & Lang_Goods_Cue(6) &"{"& GoodsName &"[ID="& ID &"]}<br>"
					ElseIf SubAction = "revert" Then
						Result = KnifeCMS.DB.UpdateRecord(DBTable_Goods,Array("Recycle:0"),Array("ID:"&ID))
						Msg    = Msg & Lang_Goods_Cue(0) &"{"& GoodsName &"[ID="& ID &"]}<br>"
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Sub
	
	Function VirtualClassGoodsDel()
		ID = KnifeCMS.GetForm("post","InputName")
		ID = KnifeCMS.IIF(ID<>"",ID,KnifeCMS.GetForm("get","id"))
		If VirtualClassID < 1 Then Exit Function
		Dim Fn_ArrID,Fn_GoodsID,Fn_i,Fn_Rs
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			Result    = 0
			If ID > 0 Then
				Fn_GoodsID = 0
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT GoodsID FROM ["& DBTable_GoodsInVirtualClass &"] WHERE ID="& ID &"")
				If Not(Fn_Rs.Eof) Then Fn_GoodsID=Fn_Rs(0)
				KnifeCMS.DB.CloseRs Fn_Rs
				If Fn_GoodsID > 0 Then
					Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT GoodsName FROM ["& DBTable_Goods &"] WHERE ID="& Fn_GoodsID &"")
					If Not(Fn_Rs.Eof) Then GoodsName=Fn_Rs(0)
					KnifeCMS.DB.CloseRs Fn_Rs
					Result = KnifeCMS.DB.DeleteRecord(DBTable_GoodsInVirtualClass,Array("GoodsID:"&Fn_GoodsID,"VirtualClassID:"&VirtualClassID))
					Msg    = Msg & Lang_GoodsInVirtualClass(2) &"{"& GoodsName &"[ID="& ID &"]}<br>"
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Function
	
	Function GoodsView()
%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <% If SubCtl = "recycle" Then %>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:revert','')"><b class="icon icon_revert"></b><% Echo Lang_DoRevert %></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  <% Else %>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=add" ><b class="icon icon_write"></b><% Echo Lang_DoAdd %></a></span>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:marketable','')"><b class="icon icon_marketable"></b><% Echo Lang_DoMarketable %></button>
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:unmarketable','')"><b class="icon icon_unmarketable"></b><% Echo Lang_DoUnMarketable %></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','')"><b class="icon icon_recycle"></b><% Echo Lang_DoDel%></button>
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			<table border="0" cellpadding="2" cellspacing="0">
			<tr><td class="upcell">
				<div class="align-center grey">
					<% Echo Lang_DistrictDoSearch %><!--搜索操作区-->
					<span class="AdvancedSearch"><a href="javascript:Admin.AdvancedSearch.Open(600,420,'AdvancedSearch')" ><% Echo Lang_AdvancedSearch %></a></span>
				</div>
				<DIV id="AdvancedSearch" style="display:none;">
					<form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                      <div class="diainbox">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                        <tr><td class="titletd"><% Echo Lang_Goods(1) %><!--商品形式-->:</td>
                            <td class="infotd"><select id="goodstype" name="goodstype"><% Echo Admin.GetGoodsTypeAsOption(0,Admin.Search.GoodsType) %></select>
                            </td></tr>
                        <tr><td class="titletd"><% Echo Lang_ClassSelect %>:</td>
                            <td class="infotd"><select id="classid" name="classid"><% Echo Admin.ShowClass(1,Admin.Search.ClassID,DBTable_GoodsClass)%></select>
                            </td></tr>
                        <tr><td class="titletd"><% Echo Lang_Goods(13) %><!--品牌-->:</td>
                            <td class="infotd"><select id="BrandID" name="BrandID"  style="font-size:13px;"><% Echo Admin.GetBrandAsOption(0,Admin.Search.BrandID) %></select>
                            </td></tr>
                        <tr><td class="titletd"><% Echo Lang_Keywords %>:</td>
                            <td class="infotd"><input type="text" class="TxtClass" id="keywords" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:160px;" /></td></tr>
                        <tr><td class="titletd"><% Echo Lang_Goods(7) %><!--销售价-->:</td>
                            <td class="infotd"><% Echo Lang_From %><!--从--> <input type="text" id="PriceFrom" name="PriceFrom" class="TxtClass" style="width:51px;" value="<% Echo PriceFrom %>" /> <% Echo Lang_FromTo %><!--到--> <input type="text" id="PriceTo" name="PriceTo" class="TxtClass" style="width:51px;" value="<% Echo PriceTo %>" /></td></tr>
                        <tr><td class="titletd"><% Echo Lang_Goods(8) %><!--成本价-->:</td>
                            <td class="infotd"><% Echo Lang_From %><!--从--> <input type="text" id="CostFrom" name="CostFrom" class="TxtClass" style="width:51px;" value="<% Echo CostFrom %>" /> <% Echo Lang_FromTo %><!--到--> <input type="text" id="CostTo" name="CostTo" class="TxtClass Short" style="width:51px;" value="<% Echo CostTo %>" /></td></tr>
                        <tr><td class="titletd"><% Echo Lang_Goods(9) %><!--市场价-->:</td>
                            <td class="infotd"><% Echo Lang_From %><!--从--> <input type="text" id="MktPriceFrom" name="MktPriceFrom" class="TxtClass" style="width:51px;" value="<% Echo MktPriceFrom %>" /> <% Echo Lang_FromTo %><!--到--> <input type="text" id="MktPriceTo" name="MktPriceTo" class="TxtClass" style="width:51px;" value="<% Echo MktPriceTo %>" /></td></tr>
                        <tr><td class="titletd"><% Echo Lang_Goods(11) %><!--库存-->:</td>
                            <td class="infotd"><% Echo Lang_From %><!--从--> <input type="text" id="StoreFrom" name="StoreFrom" class="TxtClass" style="width:51px;" value="<% Echo StoreFrom %>" /> <% Echo Lang_FromTo %><!--到--> <input type="text" id="StoreTo" name="StoreTo" class="TxtClass" style="width:51px;" value="<% Echo StoreTo %>" /></td></tr>
                        <tr><td class="titletd"><% Echo Lang_StartDate %>:</td>
                            <td class="infotd">
                            <%=Admin.Calendar(Array("","",""))%>
                            <input type="text" class="TxtSer dateStart" id="startdate" name="startdate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:160px;" value="<% Echo Admin.Search.StartDate%>" />
                            </td>
                        <tr><td class="titletd"><% Echo Lang_EndDate %>:</td>
                            <td class="infotd">
                            <input type="text" class="TxtSer dateEnd" id="enddate" name="enddate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:160px;" value="<% Echo Admin.Search.EndDate %>" />
                            </td>
                        </tr>
                        </table>
                      </div>
					  <% Echo AdvancedSearchBtnline %>
					</form>
				</DIV>
				</td></tr>
			<tr><td class="downcell">
				<form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
				<table border="0" cellpadding="0" cellspacing="0"><tr>
				<td><span><% Echo Lang_Keywords %>:</span></td>
				<td class="pl3"><input type="text" class="TxtClass" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:160px;" /></td>
				<td class="pl3"><button type="submit" class="sysBtn" ><b class="icon icon_search"></b><% Echo Lang_Btn_Search %></button></td>
				</tr></table>
				</form>
			</td></tr>
			</table>
		  </td>
		  <% End If %>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th><div><% Echo Lang_ListTableColLine_ID %></div></th><!--系统ID-->
			<th><div><% Echo Lang_Goods_ListTableColLine(0) %></div></th><!--商品编号-->
			<th><div><% Echo Lang_Goods_ListTableColLine(1) %></div></th><!--商品名称-->
			<th><div><% Echo Lang_Goods_ListTableColLine(2) %></div></th><!--缩略图-->
            <th><div><% Echo Lang_Goods_ListTableColLine(3) %></div></th><!--商品形式-->
            <th><div><% Echo Lang_Goods_ListTableColLine(4) %></div></th><!--销售价-->
			<th><div><% Echo Lang_Goods_ListTableColLine(5) %></div></th><!--库存-->
			<th><div><% Echo Lang_Goods_ListTableColLine(6) %></div></th><!--上架-->
			<th><div><% Echo Lang_Goods_ListTableColLine(7) %></div></th><!--商品分类-->
			<th><div><% Echo Lang_Goods_ListTableColLine(8) %></div></th><!--品牌-->
            <th><div><% Echo Lang_Goods_ListTableColLine(10) %></div></th><!--浏览量-->
            <th><div><% Echo Lang_GoodsLabel(1) %></div></th><!--推荐-->
            <th><div><% Echo Lang_GoodsLabel(2) %></div></th><!--热销-->
            <th><div><% Echo Lang_GoodsLabel(3) %></div></th><!--新品-->
            <th><div><% Echo Lang_GoodsLabel(4) %></div></th><!--首页推荐-->
			<th><div><% Echo Lang_ListTableColLine_AddTime %></div></th><!--添加时间-->
			<th><div><% Echo Lang_ListTableColLine_Do %></div></th><!--操作-->
			</tr>
			<% GoodsList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
	<script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	if(objExist(document.getElementById("keywords"))){ReplaceText(document.getElementById("keywords").value,1)}
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var TempHTML
		var img,fn_id,fn_label;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td0"){
					TempHTML = "<a href=\"goods.asp?ctl=goods&act=edit&id="+tds[i].title+"\" title=\"<%=Lang_DoEdit%>\">"+tds[i].innerHTML+"</a>";
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td1"){
					TempHTML = "<a href=\"goods.asp?ctl=goods&act=edit&id="+tds[i-1].title+"\" title=\"<%=Lang_DoEdit%>\">"+tds[i].innerHTML+"</a>";
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td2"){
					TempHTML = '<a href="<%=SystemPath%>?shop/'+tds[i-2].title+'/index<%=FileSuffix%>" title=\"<%=Lang_DoView%>\" target="_blank">'+tds[i].innerHTML+'</a>';
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td3" && tds[i].innerHTML!=""){
					TempHTML = '<span class="sImg"><a href="<%=SystemPath%>?shop/'+tds[i-3].title+'/index<%=FileSuffix%>" title="<%=Lang_DoView%>" target="_blank"><img src="'+ unescape(tds[i].innerHTML) +'" /></a></span>';
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td5" && tds[i].innerHTML!=""){
					TempHTML = "￥" + tds[i].innerHTML;
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td7"){
					TempHTML=tds[i].innerHTML;
					if(TempHTML=="1"){
						tds[i].innerHTML=Lang_Js_Yes;
					}else{
						tds[i].innerHTML=Lang_Js_No;
					}
				}
				if(tds[i].id=="td11" || tds[i].id=="td12" || tds[i].id=="td13" || tds[i].id=="td14"){
					if(tds[i].id=="td11"){
						fn_label = "recommend";
						fn_id    = tds[i-11].title;
					}else if(tds[i].id=="td12"){
						fn_label = "hot";
						fn_id    = tds[i-12].title;
					}else if(tds[i].id=="td13"){
						fn_label = "new";
						fn_id    = tds[i-13].title;
					}else if(tds[i].id=="td14"){
						fn_label = "homepagerecommend";
						fn_id    = tds[i-14].title;
					}
					img = document.createElement("img");
					img.setAttribute("class","pointer");
					if(parseInt(tds[i].innerHTML)==1){
						img.src="image/icon_yes.png";
						img.setAttribute("value",1);
					}else{
						img.src="image/icon_yes_gray.png";
						img.setAttribute("value",0);
					}
					img.setAttribute("id",fn_id);
					img.setAttribute("label",fn_label);
					img.onclick=function(){Admin.Goods.SetGoodsLabel(this)}
					tds[i].title="";
					tds[i].innerHTML="";
					tds[i].appendChild(img);
				}
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
	Function VirtualClassGoodsView()
	VirtualClassID=ID
%>
	<DIV id="MainFrame">
		<div class="tagLine"><% Echo NavigationLine %></div>
		<DIV class="cbody">
		  <div class="toparea">
			<table border="0" cellpadding="0" cellspacing="0">
			  <tr>
              <td class="tdcell">
                  <table border="0" cellpadding="2" cellspacing="0">
                  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
                  <tr><td class="downcell">
                      <span class="sysBtn"><a href="javascript:ShowModal('?ctl=goods&act=view&acttype=minilist&virtualclassid=<%=VirtualClassID%>',940,600);ShowModalReload();"><b class="icon icon_add"></b><% echo Lang_DoAdd %></a></span>
                      </td></tr>
                  </table>
              </td>
			  <td class="tdcell">
				  <table border="0" cellpadding="2" cellspacing="0">
				  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoBatchDel %><!--批量剔除--></div></td></tr>
				  <tr><td class="downcell">
					  <span class="sysBtn"><a href="javascript:;" onClick="Operate('listForm','InputName','del','<% Echo Lang_DoDelRejectConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelReject %></a></span>
					  </td></tr>
				  </table>
			  </td>
			  </tr>
			</table>
		  </div>
		  <div class="tabledata">
          <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&virtualclassid=<%= VirtualClassID %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl("")) %>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th width="1%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %></div></th><!--系统ID-->
			<th width="13%"><div style="width:75px"><% Echo Lang_Goods_ListTableColLine(0) %></div></th><!--商品编号-->
			<th width="20%"><div style="width:140px;"><% Echo Lang_Goods_ListTableColLine(1) %></div></th><!--商品名称-->
			<th width="5%"><div style="width:60px"><% Echo Lang_Goods_ListTableColLine(2) %></div></th><!--缩略图-->
            <th width="60px"><div style="width:60px"><% Echo Lang_Goods_ListTableColLine(4) %><!--销售价--></div></th>
            <th width="40px"><div style="width:40px"><% Echo Lang_Goods_ListTableColLine(5) %><!--库存--></div></th>
			<th width="100px"><div style="width:100px"><% Echo Lang_Goods_ListTableColLine(9) %><!--虚拟分类--></div></th>
			<th width="100px"><div style="width:100px"><% Echo Lang_Goods_ListTableColLine(7) %><!--商品分类--></div></th>
			<th><div style="width:60px;"><% Echo Lang_ListTableColLine_Do %></div></th><!--操作-->
			</tr>
			<% VirtualClassGoodsList() %>
		   </tbody>
		  </table>
		  </form>
		  </div>
		</DIV>
	</DIV>
	<form name="FormForReload" style="display:none;" method="post"></form>
	<script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	if(objExist(document.getElementById("keywords"))){ReplaceText(document.getElementById("keywords").value,1)}
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td3" && tds[i].innerHTML!=""){
				TempHTML = '<span class="sImg"><a href=javascript:;><img src="'+ unescape(tds[i].innerHTML) +'" /></a></span>';
				tds[i].innerHTML=TempHTML;
				}
			}
		}
	}
	ChangeTdText();
    </script>
<%
	End Function
	Function GoodsMiniListView()
%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
		<tr><td width="10%" valign="top">
			<div style="width:275px; padding-right:5px">
			<div class="inbox">
			<p class="pb3"><% Echo Lang_Goods(40) %><!--筛选条件--></p>
            <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search&acttype=minilist&virtualclassid=<% Echo VirtualClassID %>" method="post" style="margin:0px">
            <div class="diainbox">
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
              <tr><td class="titletd"><% Echo Lang_Goods(1) %><!--商品形式-->:</td>
                  <td class="infotd"><select id="goodstype" name="goodstype"><% Echo Admin.GetGoodsTypeAsOption(0,Admin.Search.GoodsType) %></select>
                  </td></tr>
              <tr><td class="titletd"><% Echo Lang_ClassSelect %>:</td>
                  <td class="infotd"><select id="classid" name="classid"><% Echo Admin.ShowClass(1,Admin.Search.ClassID,DBTable_GoodsClass)%></select>
                  </td></tr>
              <tr><td class="titletd"><% Echo Lang_Goods(13) %><!--品牌-->:</td>
                  <td class="infotd"><select id="BrandID" name="BrandID"  style="font-size:13px;"><% Echo Admin.GetBrandAsOption(0,Admin.Search.BrandID) %></select>
                  </td></tr>
              <tr><td class="titletd"><% Echo Lang_Keywords %>:</td>
                  <td class="infotd"><input type="text" class="TxtClass" id="keywords" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:160px;" /></td></tr>
              <tr><td class="titletd"><% Echo Lang_Goods(7) %><!--销售价-->:</td>
                  <td class="infotd"><% Echo Lang_From %><!--从--> <input type="text" id="PriceFrom" name="PriceFrom" class="TxtClass" style="width:51px;" value="<% Echo PriceFrom %>" /> <% Echo Lang_FromTo %><!--到--> <input type="text" id="PriceTo" name="PriceTo" class="TxtClass" style="width:51px;" value="<% Echo PriceTo %>" /></td></tr>
              <tr><td class="titletd"><% Echo Lang_Goods(8) %><!--成本价-->:</td>
                  <td class="infotd"><% Echo Lang_From %><!--从--> <input type="text" id="CostFrom" name="CostFrom" class="TxtClass" style="width:51px;" value="<% Echo CostFrom %>" /> <% Echo Lang_FromTo %><!--到--> <input type="text" id="CostTo" name="CostTo" class="TxtClass Short" style="width:51px;" value="<% Echo CostTo %>" /></td></tr>
              <tr><td class="titletd"><% Echo Lang_Goods(9) %><!--市场价-->:</td>
                  <td class="infotd"><% Echo Lang_From %><!--从--> <input type="text" id="MktPriceFrom" name="MktPriceFrom" class="TxtClass" style="width:51px;" value="<% Echo MktPriceFrom %>" /> <% Echo Lang_FromTo %><!--到--> <input type="text" id="MktPriceTo" name="MktPriceTo" class="TxtClass" style="width:51px;" value="<% Echo MktPriceTo %>" /></td></tr>
              <tr><td class="titletd"><% Echo Lang_Goods(11) %><!--库存-->:</td>
                  <td class="infotd"><% Echo Lang_From %><!--从--> <input type="text" id="StoreFrom" name="StoreFrom" class="TxtClass" style="width:51px;" value="<% Echo StoreFrom %>" /> <% Echo Lang_FromTo %><!--到--> <input type="text" id="StoreTo" name="StoreTo" class="TxtClass" style="width:51px;" value="<% Echo StoreTo %>" /></td></tr>
              <tr><td class="titletd"><% Echo Lang_StartDate %>:</td>
                  <td class="infotd">
                  <%=Admin.Calendar(Array("","",""))%>
                  <input type="text" class="TxtSer dateStart" id="startdate" name="startdate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:160px;" value="<% Echo Admin.Search.StartDate%>" />
                  </td>
              <tr><td class="titletd"><% Echo Lang_EndDate %>:</td>
                  <td class="infotd">
                  <input type="text" class="TxtSer dateEnd" id="enddate" name="enddate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:160px;" value="<% Echo Admin.Search.EndDate %>" />
                  </td>
              </tr>
              </table>
            </div>
            <% Echo AdvancedSearchBtnline %>
            </form>
			</div>
			</div>
		</td>
		<td valign="top">
		  <div class="inbox">
		  <p class="pb3"><% Echo Lang_Goods(41) %><!--筛选结果--></p>
		  <form name="listForm" id="listForm" method="post" action="?virtualclassid=<% Echo VirtualClassID %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
			 <tbody>
			  <tr class="top">
			   <th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="ctl" value="<% Echo Ctl %>" /><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /><input type="hidden" name="acttype" value="" /></div></th>
              <th width="1%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
              <th width="160px"><div style="width:150px"><% Echo Lang_Goods_ListTableColLine(0) %><!--商品编号--></div></th>
              <th width="20%"><div style="width:200px;"><% Echo Lang_Goods_ListTableColLine(1) %><!--商品名称--></div></th>
              <th width="60px"><div style="width:60px"><% Echo Lang_Goods_ListTableColLine(4) %><!--销售价--></div></th>
              <th width="40px"><div style="width:40px"><% Echo Lang_Goods_ListTableColLine(5) %><!--库存--></div></th>
			  <th><div style="width:40px;"><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			  </tr>
			  <% GoodsList %>
			 </tbody>
		  </table>
		  </form>
		  </div>
		</td></tr>
		</table>
		<div class="bottomSaveline" id="SubmitButtoms" style="text-align:center;">
		  <% if VirtualClassID > 0 then %>
		  <button type="button" class="sysSaveBtn" name="InsertBtn" id="SaveBtn" onclick="GoodsAddAll()"><b class="icon icon_save"></b><% Echo Lang_Btn_AddSearchResult %><!--将搜索结果全部加入--></button>
		  <button type="button" class="sysSaveBtn" name="InsertBtn" id="SaveBtn" onclick="GoodsAddSel()"><b class="icon icon_save"></b><% Echo Lang_Btn_AddSelectResult %><!--将选择项加入--></button>
		  <% else%>
		  <button type="button" class="sysSaveBtn" name="InsertBtn" id="SaveBtn" onclick="Admin.Goods.RebackSelectGoods('InputName')"><b class="icon icon_save"></b><% Echo Lang_Btn_Confirm %></button>
		  <% end if %>
		  <button type="reset" class="sysSaveBtn" id="CloseBtn" onclick="WinClose()"><b class="icon icon_reset"></b><% Echo Lang_Btn_Close %></button>
		</div>
	  </DIV>
	</DIV>
	<script language="javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	if(objExist(document.getElementById("keywords"))){ReplaceText(document.getElementById("keywords").value,1)}
	function GoodsAddAll(){
		var Ctl=document.forms["listForm"].ctl;
		var Action=document.forms["listForm"].act;
		var SubAction=document.forms["listForm"].subact;
		var ActionType=document.forms["listForm"].acttype;
		var SerachForm=document.forms["SerachForm"];
		var listForm=document.forms["listForm"];
		if(objExist(Ctl) && objExist(Action) && objExist(SubAction) && objExist(ActionType) && objExist(SerachForm) && objExist(listForm)){
			Ctl.value="goodsinvirtualclass"
			Action.value="add";
			SubAction.value="save";
			ActionType.value="searchresult";
			var NewDiv = document.createElement("div");
			NewDiv.style.display = "none";
			NewDiv.innerHTML = SerachForm.innerHTML;
			listForm.appendChild(NewDiv);
			listForm.submit();
		}else{
			alert("Error:GoodsAddAll()");
		}
	}
	function GoodsAddSel(){
		var Ctl=document.forms["listForm"].ctl;
		var ActionType=document.forms["listForm"].acttype;
		if(objExist(Ctl) && objExist(ActionType)){
			Ctl.value="goodsinvirtualclass";
			ActionType.value="selectresult";
			Operate('listForm','InputName','add:save','');
		}
	}
	</script>
<%
	End Function
	Function GoodsDo()
	Dim Fn_Html,Fn_EditorHtml,Fn_Temp,Fn_Temp2
	Dim Fn_Required,Fn_AttrNotNull
	
	Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_GoodsFieldSet &"] WHERE Recycle=0 ORDER BY OrderNum ASC,ID DESC")
	Do While Not(Rs.Eof)
		
		Pv_Field       = Rs("Field")
		Pv_FieldName   = Rs("FieldName")
		Pv_FieldType   = Rs("FieldType")
		Pv_FieldOptions= Rs("FieldOptions")
		Pv_FieldTips   = Rs("FieldTips")
		
		Pv_FieldValue  = ""
		If ID>0 Then Pv_FieldValue =  KnifeCMS.DB.GetFieldByID(DBTable_Goods,Pv_Field,ID)
		
		If KnifeCMS.Data.CLng(Rs("NotNull"))>0 Then
			Fn_Required    = "<span class=""required"">*</span>"
			Fn_AttrNotNull = "notnull=""true"""
		Else
			Fn_Required    = ""
			Fn_AttrNotNull = "notnull=""false"""
		End If
		
		Select Case Pv_FieldType
		Case "text"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><input type=""text"" fieldset=1 class=""TxtClass long"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" maxlength=""250"" value="""& Pv_FieldValue &""" /><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
		Case "textarea"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><textarea type=""text"" fieldset=1 class=""info"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" maxlength=""250"">"& KnifeCMS.Data.TextareaDecode(Pv_FieldValue) &"</textarea><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
			
		Case "editor"
			Fn_EditorHtml = Fn_EditorHtml &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><textarea type=""TextContent"" fieldset=1 class=""info"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &">"& Pv_FieldValue &"</textarea><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label><script type=""text/javascript"">var "& Pv_Field &"_Editor=UE.getEditor('"& Pv_Field &"');</script></td></tr>"
			
		Case "radio"
			Pv_Array = Split(Pv_FieldOptions,"|")
			Fn_Temp  = ""
			For Pv_ii=0 To Ubound(Pv_Array)
				Fn_Temp = Fn_Temp &"<input type=""radio"" fieldset=1 name="""& Pv_Field &""" "& Fn_AttrNotNull &" "& Admin.Input_Checked(Pv_Array(Pv_ii),Pv_FieldValue) &" value="""& Pv_Array(Pv_ii) &""">"& Pv_Array(Pv_ii)
			Next
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd"">"& Fn_Temp &"<label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
			
		Case "checkbox"
			Pv_Array  = Split(Pv_FieldOptions,"|")
			Pv_Array2 = Split(Pv_FieldValue,"|")
			Fn_Temp   = ""
			For Pv_ii=0 To Ubound(Pv_Array)
				Fn_Temp2 = ""
				For Pv_jj=0 To Ubound(Pv_Array2)
					If Pv_Array2(Pv_jj)=Pv_Array(Pv_ii) Then
						Fn_Temp2 = "checked=""checked="""
					End If
				Next
				Fn_Temp = Fn_Temp &"<input type=""checkbox"" fieldset=1 name="""& Pv_Field &""" "& Fn_AttrNotNull &" "& Fn_Temp2 &" value="""& Pv_Array(Pv_ii) &""">"& Pv_Array(Pv_ii)
			Next
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd"">"& Fn_Temp &"<label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
			
		Case "select"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><select fieldset=1 id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &"><option value="""">"& Lang_PleaseSelect &"</option>"& OS.CreateOption(Split(Pv_FieldOptions,"|"),Pv_FieldValue) &"</select><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
		Case "number"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><input type=""text"" fieldset=1 class=""TxtClass slong"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" maxlength=""250"" value="""& Pv_FieldValue &""" onblur=""KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g'))"" /><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
			
		Case "datetime"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><input type=""text"" fieldset=1 class=""dateTime slong"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" maxlength=""250"" value="""& Pv_FieldValue &""" />"
			Fn_Html = Fn_Html & Admin.Calendar(Array(Pv_Field,Pv_Field,"%Y-%m-%d %H:%M:%S"))
			Fn_Html = Fn_Html & "<label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
			
		Case "image"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd"">"
			Fn_Html = Fn_Html &"<div class=""image_grid""><table border=""0"" cellpadding=""0"" cellspacing=""0""><tr><td valign=""top"" style=""padding:0px;""><div class=""imageBox"" id="""& Pv_Field &"Box"">"

			If Not(KnifeCMS.Data.IsNul(Pv_FieldValue)) Then
				Fn_Html = Fn_Html &"<img class=""img"" src="""& Trim(KnifeCMS.Data.URLDecode(Pv_FieldValue))&""" />"
			Else
				Fn_Html = Fn_Html &"<img class=""img"" src=""image/image.jpg"" />"
			End If
            Fn_Html = Fn_Html &"</div><td valign=""top"" style=""padding:0px 0px 0px 6px;""><input type=""text"" fieldset=1 class=""TxtClass slong"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" onblur=""Admin.ChangeImageUrl('"& Pv_Field &"Box','"& Pv_Field &"','img','d_"& Pv_Field &"')"" style=""width:314px; border:1px dashed #ccc;"" maxlength=""250"" value="""& KnifeCMS.Data.URLDecode(Pv_FieldValue) &""" /><div style=""padding:5px 0px 0px 0px;""><span class=""sysBtn""><a onclick=""UploadImg('1','"& Pv_Field &"');Admin.ImageUrlChange('"& Pv_Field &"Box','"& Pv_Field &"','','d_"& Pv_Field &"')"" href=""javascript:void(0)""><b class=""icon icon_add""></b>"& Lang_Btn_SelectPics &"</a></span></div><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr></table></div>"
			Fn_Html = Fn_Html &"</td></tr>"
			
		Case "file"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><input type=""text"" fieldset=1 class=""TxtClass long"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" maxlength=""250"" value="""& Pv_FieldValue &""" /><span class=""sysBtn""><a onclick=""UploadFile('1','"& Pv_Field &"');Admin.ImageUrlChange('"& Pv_Field &"Box','"& Pv_Field &"','','d_"& Pv_Field &"')"" href=""javascript:void(0)""><b class=""icon icon_add""></b>"& Lang_Btn_SelectFile &"</a></span><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
		Case Else
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><input type=""text"" fieldset=1 class=""TxtClass long"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" maxlength=""250"" value="""& Pv_FieldValue &""" /><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
		End Select
	Rs.MoveNext
	Loop
	KnifeCMS.DB.CloseRs Rs
%>
    <script type="text/javascript" charset="utf-8">
	window.UEDITOR_HOME_URL = "<%=SystemPath%>plugins/ueditor/";
	</script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_config.asp"></script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_api.js"></script>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
      <div class="wbox">
          <form name="SaveForm" id="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post" onsubmit="return Admin.Goods.SaveGoodsCheck()">
          <ul id="ArticleTabs" class="TabsStyle">
             <li id="liTab_1" class="current" onclick=TabSwitch("MainFrame","li","Tabs",1)><% Echo Lang_Goods(0) %><!--基本信息--></li>
             <li id="liTab_2" onclick=TabSwitch("MainFrame","li","Tabs",2)><% Echo Lang_Goods(23) %><!--商品搭配销售--></li>
             <li id="liTab_3" onclick=TabSwitch("MainFrame","li","Tabs",3)><% Echo Lang_Goods(35) %><!--相关商品--></li>
          </ul>
          <div id="Tabs_1">
            <div class="inbox">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tbody>
                    <tr><td style="width:565px;" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                          <tbody>
                           <tr><td class="titletd"><span class="required">*</span><% Echo Lang_Goods(1) %><!--商品形式-->:</td>
                               <td class="infotd">
                                    <select id="GoodsType" name="GoodsType" style="font-size:13px;">
                                    <% Echo Admin.GetGoodsTypeAsOption(0,GoodsType) %>
                                    </select>
                                    <span><a href="javascript:void(0);" onclick="ShowHelpMsg(450,200,'<% Echo Lang_Goods(1) %>',Js_Goods_HelpMsg[0])"><b class="icon icon_help"></b></a></span>
                                    <label class="Normal" id="d_GoodsType"></label>
                               </td>
                           </tr>
                           <tr><td class="titletd"><span class="required">*</span><% Echo Lang_Goods(2) %><!--商品分类-->:</td>
                               <td class="infotd">
                                   <select id="ClassID" name="ClassID"  style="font-size:13px;">
                                   <% Echo Admin.ShowClass(1,ClassID,DBTable_GoodsClass)%>
                                   </select>
                                   <label class="Normal" id="d_ClassID"></label>
                               </td>
                           </tr>
                           <tr><td class="titletd"><span class="required">*</span><% Echo Lang_Goods(3) %><!--商品名称-->:</td>
                               <td class="infotd">
                               <input type="text" class="TxtClass long" id="GoodsName" name="GoodsName" onblur="Admin.AutoSeoTitle(KnifeCMS.$id('GoodsName'),KnifeCMS.$id('TempGoodsName'),KnifeCMS.$id('SeoTitle'));" value="<%=KnifeCMS.Data.HTMLDecode(GoodsName)%>" />
                               <input type="hidden" id="TempGoodsName" name="TempGoodsName" value="<%=KnifeCMS.Data.HTMLDecode(GoodsName)%>"/>
                               <div style="margin-top:4px;"><label class="Normal" id="d_GoodsName"></label></div>
                               </td>
                           </tr>
                           <tr><td class="titletd"><% Echo Lang_Goods(4) %><!--商品编号-->:</td>
                               <td class="infotd">
                               <input type="text" class="TxtClass" id="GoodsBN" name="GoodsBN" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9a-zA-Z]*/g','maxLength:32'))" value="<%=GoodsBN%>" style="width:250px;" />
                               <div style="margin-top:4px;"><label class="Normal" id="d_GoodsBN"><% Echo Lang_Goods_Cue(8) %><!--字母和数字的组合,长度不能超过32.<br />如果您不输入商品编号,系统将自动生成一个唯一的编号.--></label></div>
                               </td>
                           </tr>
                           <tr class="Separated"><td class="titletd"></td><td class="infotd"></td></tr>
                           <tr><td class="titletd"><% Echo Lang_Goods(5) %><!--是否上架销售-->:</td>
                               <td class="infotd"><input type="radio" <%=Admin.Input_Checked(Marketable,1)%> name="Marketable" checked="checked" value="1" /><% Echo Lang_Yes %> &nbsp; <input type="radio" <%=Admin.Input_Checked(Marketable,0)%> name="Marketable" value="0" /><% Echo Lang_No %></td>
                           </tr>
                           <tr><td class="titletd"><% Echo Lang_Goods(6) %><!--货号-->:</td>
                               <td class="infotd"><input type="text" class="TxtClass" id="GoodsSN" name="GoodsSN" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9a-zA-Z]*/g','maxLength:32'))"  value="<%=GoodsSN%>" style="width:250px;"/><br />
                               <label class="Normal"><% Echo Lang_Goods_Cue(9) %><!--字母和数字的组合,长度不能超过32.--></label></td>
                           </tr>
                           <tr><td class="titletd"><% Echo Lang_Goods(9) %><!--市场价-->:</td>
                               <td class="infotd"><input type="text" class="TxtClass" id="GoodsMktPrice" name="GoodsMktPrice" style="width:100px;" value="<%=GoodsMktPrice%>" /><label class="Normal"></label></td>
                           </tr>
                           <tr><td class="titletd"><% Echo Lang_Goods(7) %><!--价格(销售价)-->:</td>
                               <td class="infotd"><input type="text" class="TxtClass" id="GoodsPrice" name="GoodsPrice" style="width:100px;" value="<%=GoodsPrice%>" /></td>
                           </tr>
                           <tr><td class="titletd"><% Echo Lang_Goods(8) %><!--成本价-->:</td>
                               <td class="infotd"><input type="text" class="TxtClass" id="GoodsCost" name="GoodsCost" style="width:100px;" value="<%=GoodsCost%>" /><label class="Normal"> <% Echo Lang_Goods_Cue(10) %><!--前台不会显示，仅供后台参考使用.--></label></td>
                           </tr>
                           <tr><td class="titletd"><% Echo Lang_Goods(10) %><!--重量-->:</td>
                               <td class="infotd"><input type="text" class="TxtClass" id="GoodsWeight" name="GoodsWeight" style="width:60px;" value="<%=GoodsWeight%>" /> <% Echo Lang_Unit_g %><!--克(g)--></td>
                           </tr>
                           <tr><td class="titletd"><% Echo Lang_Goods(11) %><!--库存-->:</td>
                               <td class="infotd"><input type="text" class="TxtClass" id="GoodsStore" name="GoodsStore" style="width:60px;" value="<%=GoodsStore%>" /></td>
                           </tr>
                           <tr><td class="titletd"><% Echo Lang_Goods(12) %><!--计量单位-->:</td>
                               <td class="infotd"><input type="text" class="TxtClass" id="GoodsUnit" name="GoodsUnit" style="width:60px;" value="<%=GoodsUnit%>" /></td>
                           </tr>
                           <tr><td class="titletd"><% Echo Lang_Goods(13) %><!--品牌-->:</td>
                               <td class="infotd">
                               <select id="BrandID" name="BrandID"  style="font-size:13px;">
                               <% Echo Admin.GetBrandAsOption(0,BrandID) %>
                               </select><label class="Normal" id="d_BrandID"></label></td>
                           </tr>
                           <tr class="Separated"><td class="titletd"><div></div></td><td class="infotd"><div></div></td></tr>
                           <tr><td class="titletd"><% Echo Lang_Goods(19) %><!--简介-->:</td>
                               <td class="infotd"><textarea class="abstract" id="Abstract" name="Abstract"><%=KnifeCMS.Data.HTMLDecode(Abstract)%></textarea><br />
                               <label class="Normal"><% Echo Lang_Goods_Cue(11) %><!--简短的商品介绍,请不要超过255字.--></label></td>
                           </tr>
                           <tr><td class="titletd" valign="top"><% Echo Lang_GoodsLabel(0) %></td><td class="infotd">
                               <input id="Recommend" name="Recommend" type="checkbox" <%=Admin.InputChecked(Pv_Recommend)%> value="1"><% Echo Lang_GoodsLabel(1) %><!--推荐-->
                               <input id="Hot" name="Hot" type="checkbox" <%=Admin.InputChecked(Pv_Hot)%> value="1"><% Echo Lang_GoodsLabel(2) %><!--热销-->
                               <input id="New" name="New" type="checkbox" <%=Admin.InputChecked(Pv_New)%> value="1"><% Echo Lang_GoodsLabel(3) %><!--新品-->
                               <input id="HomepageRecommend" name="HomepageRecommend" type="checkbox" <%=Admin.InputChecked(Pv_HomepageRecommend)%> value="1"><% Echo Lang_GoodsLabel(4) %><!--首页推荐-->
                               <label class="Normal" id="d_GoodsLabel"></label>
                               </td>
                           </tr>
                          </tbody>
                        </table>
                    </td>
                    <td valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tbody><tr>
                            <td style="width:260px;" valign="top">
                              <div class="uploadimgArea" style="margin-left:8px;">
                                 <div class="top">
                                   <span class="sysBtn"><a onclick="AddImgs('imgslist','GoodsImg[]')" href="javascript:;"><b class="icon icon_add"></b><% Echo Lang_Goods(14) %><!--添加图片...--></a></span>
                                   <span class="sysBtn"><a onclick="ShowOrHidden('SetThumbnail',1)" href="javascript:;"><b class="icon icon_white_gear"></b><% Echo Lang_Goods(15) %><!--单独设置商品缩略图--></a></span>
                                 </div>
                                 <% If KnifeCMS.Data.IsNul(Thumbnail) Then %>
                                     <div id="SetThumbnail" class="thumbnailbox" style="display:none;">
                                 <% Else Echo "<div id=""SetThumbnail"" class=""thumbnailbox"">"%>
                                 <% End If %>
                                     <p style="padding:5px 5px 0px 5px;">
                                     <span style="float:right;"><a onclick="ShowOrHidden('SetThumbnail',0)" href="javascript:;"><b class="icon icon_del"></b><% Echo Lang_Goods(17) %><!--隐藏--></a></span>
                                     <span class="sysBtn"><a onclick="AddImgs('Thumbnail','Thumbnail')" href="javascript:;"><b class="icon icon_image_edit"></b><% Echo Lang_Goods(16) %><!--选择缩略图--></a></span>
                                     </p>
                                     <table border="0" cellpadding="0" cellspacing="0"><tr><td>
                                     <div class="thumbnail" id="Thumbnail"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td class="noneimage">
                                     <% If not(KnifeCMS.Data.IsNul(Thumbnail)) Then %><input type="hidden" name="Thumbnail" value="<%=Trim(Thumbnail)%>" /><img src="<%=Trim(Thumbnail)%>" /><% End If %></td></tr></table></div>
                                     </td><td valign="top"><p style="padding:5px 4px 0px 0px;"><% Echo Lang_Goods_Cue(12) %><!--商品缩略图默认由系统自动生成,您也可以单独设置.--></p></td>
                                     </tr></table>
                                 </div>
                                 
                                 <div class="bigimg" id="bigimg"><table border="0" cellpadding="0" cellspacing="0"><tr><td class="noneimage"><span class="imgwrap"><% If not(KnifeCMS.Data.IsNul(GoodsFirstImg)) Then Echo "<input type=""hidden"" name=""GoodsImg[]FirstImg"" value="""&GoodsFirstImg&""" /><img src="""&GoodsFirstImg&""">"%></span></td></tr></table></div>
                                 <ul class="imgslist" id="imgslist">
                                 <% If not(KnifeCMS.Data.IsNul(GoodsImg)) Then
                                       GoodsImg=Split(GoodsImg,",")
                                       For i=0 to Ubound(GoodsImg)
                                       Echo "<li title="& Lang_Js_SetAsCover &"><input type=""hidden"" name=""GoodsImg[]"" value="""&GoodsImg(i)&""" /><table border=""0"" cellpadding=""0"" cellspacing=""0""><tbody><tr><td class=""imgdiv""><a href=""javascript:;"" target=""_self""><img src="""&GoodsImg(i)&"""></a></td></tr></tbody></table><p><a onclick=""deleteCurrentPic(this)"" href=""javascript:;"" target=""_self""><b class=""icon icon_del"" title=""delete""></b></a><a onclick=""DiaWindowOpen_GetImgURL(this)"" title="""&GoodsImg(i)&""" href=""javascript:;"" target=""_self""><b class=""icon icon_imglink"" title=""get url""></b></a></p></li>"
                                       Next
                                    End If
                                  %>
                                 </ul>
                              </div>
                            </td></tr>
                          </tbody>
                        </table>
                    </td>
                    </tr>
                  </tbody>
                </table>
            </div>
            
            <!--自定义字段-->
			<% If Fn_Html<>"" Then %>
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
            <tbody>
            <%=Fn_Html%>
            </tbody>
            </table>
            </div>
            <% End If %>
            
            <!--商品扩展信息-->
            <div class="inbox"> 
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tbody>
                 <tr><td class="titletd"><% Echo Lang_Goods(22) %><!--商品类型-->:</td>
                     <td class="infotd">
                     <input type="hidden" id="GoodsAttr" name="GoodsAttr" value="" />
                     <select id="GoodsModel" name="GoodsModel" onchange="Admin.Goods.GetAttrList('GoodsModelArea')" style="font-size:13px;">
                     <% Echo Admin.GetGoodsModelAsOption(0,GoodsModelID) %>
                     </select>
                     <span><a href="javascript:void(0);" onclick="ShowHelpMsg(400,100,'<% Echo Lang_Goods(22) %>',Js_Goods_HelpMsg[1])"><b class="icon icon_help"></b></a></span>
                     <label class="Normal" id="d_GoodsModel"><% Echo Lang_Goods_Cue(13) %><!--请选择商品的所属类型，进而完善此商品的属性.--></label>
                     </td>
                 </tr>
                </tbody>
              </table>
              <div id="GoodsModelArea" style="padding-top:5px;">
              <% If Action="edit" Then Call GetGoodsModelAttribute(ID,GoodsModelID) %>
              </div>
            </div>
            
            <!--商品详细-->
            <div class="inbox"> 
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
              <tbody>
                 <tr><td class="titletd" valign="top"><%=Lang_Goods(20)%></td><td class="infotd">
                     <textarea class="TextContent" id="TextContent" name="TextContent"><%=KnifeCMS.Data.HTMLDecode(TextContent)%></textarea>
                     <label class="Normal" id="d_TextContent"></label>
                     <script type="text/javascript">var OSWebEditor=UE.getEditor('TextContent');</script>
                     </td>
                 </tr>
              </tbody>
              </table>
            </div>
            
            <!--自定义字段:编辑器-->
            <% If Fn_EditorHtml<>"" Then %>
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
            <tbody>
            <%=Fn_EditorHtml%>
            </tbody>
            </table>
            </div>
            <% End If %>
            
            <!--SEO优化-->
            <div class="inbox">
               <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
               <tbody>
                   <tr><td class="titletd" valign="top"><% Echo Lang_SeoTitle %></td><td class="infotd">
                       <input type="text" class="TxtClass long" name="SeoTitle" id="SeoTitle" maxlength="250" value="<%=KnifeCMS.Data.ReplaceString(SeoTitle,"&nbsp;",Chr(32))%>"/> <label class="Normal" id="d_SeoTitle"><% Echo Lang_SeoTitleCue %></label>
                       </td></tr>
                   <tr><td class="titletd" valign="top"><% Echo Lang_SeoUrl %></td><td class="infotd">
                       <input type="text" class="TxtClass readonly" name="url_pre" readonly="readonly" style="width:268px;" value="<% Echo SiteURL & SystemPath & "?goods/"& KnifeCMS.IIF(ID>0,ID,"{id}") &"/" %>"/>
                       <input type="text" class="TxtClass" name="SeoUrl" id="SeoUrl" maxlength="250"  style="width:228px;" value="<%=SeoUrl%>"/>
                       <input type="text" class="TxtClass readonly" name="url_suffix" readonly="readonly" style="width:40px;" value="<% Echo FileSuffix %>"/>
                       <label class="Normal" id="d_SeoUrl"><% Echo Lang_SeoUrlCue %></label>
                       </td></tr>
                   <tr><td class="titletd" valign="top"><% Echo Lang_MetaKeywords %></td><td class="infotd">
                       <input type="text" class="TxtClass long" name="MetaKeywords" id="MetaKeywords" maxlength="250" value="<%=KnifeCMS.Data.ReplaceString(MetaKeywords,"&nbsp;",Chr(32))%>"/> <label class="Normal" id="d_MetaKeywords"><% Echo Lang_MetaKeywordsCue %></label>
                       </td></tr>
                   <tr><td  class="titletd" valign="top"><% Echo Lang_MetaDescription %></td><td class="infotd">
                       <textarea class="metadescription" name="MetaDescription" id="MetaDescription" maxlength="250" onblur="KnifeCMS.onblur(this,'maxLength:250')" ><%=KnifeCMS.Data.ReplaceString(MetaDescription,"&nbsp;",Chr(32))%></textarea>
                       <label class="Normal" id="d_MetaDescription"><% Echo Lang_MetaDescriptionCue %></label>
                       </td>
                   </tr>
               </tbody>
               </table>
            </div>
          </div>
          <div id="Tabs_2" style="display:none;">
              <div class="inbox">
                <input type="hidden" name="GoodsAdjunct" id="GoodsAdjunct" />
                <div class="p5"><span class="sysBtn"><a href="javascript:;" onClick="Admin.Goods.addAdjunctGroup()"><b class="icon icon_add"></b><% Echo Lang_Goods(24) %><!--添加一个配件组--></a></span></div>
                <% If Action="edit" Then Call GetGoodsAdjunct(ID) %>
              </div>
              <div id="adjunct_box"></div>
          </div>
          <div id="Tabs_3" style="display:none;">
            <div class="inbox">
              <div class="toparea">
                <span class="sysBtn"><a href="javascript:;" onClick="Admin.Goods.getGoods('Link','LinkGoods','LinkGoodsID[]')"><b class="icon icon_add"></b><% Echo Lang_Goods(36) %><!--填加相关商品--></a></span>
              </div>
              <div class="tabledata">
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
                 <tbody id="LinkGoods">
                  <tr class="top">
                  <th width="2%"><div style="width:30px;"><% Echo Lang_DoDel %><!--删除--></div></th>
                  <th width="3%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
                  <th width="15%"><div style="width:160px;"><% Echo Lang_Goods_ListTableColLine(0) %><!--商品编号--></div></th>
                  <th width="80%"><div style="width:250px;"><% Echo Lang_Goods_ListTableColLine(1) %><!--商品名称--></div></th>
                  </tr>
                  <% If Action="edit" Then Call GetGoodsLink(ID) %>
                 </tbody>
              </table>
              </div>
            </div>
          </div>
          <input type="hidden" name="id" value="<% Echo ID %>" />
          <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
          <div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
              <button type="submit" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
          </div>
          </form>
      </div>
      </DIV>
    </DIV>
	<script type="text/javascript">
    selectFirstPic("imgslist","GoodsImg[]");
	Admin.AutoSeoTitle(KnifeCMS.$id('GoodsName'),KnifeCMS.$id('TempGoodsName'),KnifeCMS.$id('SeoTitle'));
    </script>
<%
	End Function
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
End Class
%>
