<%
'**
'后台Ajax操作类
'filename: admin.ajax.class.asp
'copyright: KFC
'**
Class Class_KnifeCMS_Admin_Ajax
	
	Private Pv_Rs,Pv_Array,Pv_Result,Pv_ReturnString
	Private Pv_Column,Pv_RedundDBTableArray,Pv_CheckRedundSQL
	Private S_TempHTML
	
	
	Private Sub Class_Initialize()
	End Sub
	
	Public Function CheckFieldIsExist()
		Dim Fn_Field,Fn_System,Fn_Array
		Fn_Field  = LCase(KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","Field"),"[^0-9a-zA-Z_]",""),30))
		Fn_System = LCase(KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","System"),"[^0-9a-zA-Z]",""),30))
		'判断字段是否已经存在
		If ID>0 Then
			Fn_Array=Array("Field:"& Fn_Field,"System:"& Fn_System,"ID<>"& ID)
		Else
			Fn_Array=Array("Field:"& Fn_Field,"System:"& Fn_System)
		End If
		If KnifeCMS.DB.CheckIsRecordExistByField(DBTable_FieldSet,Fn_Array) Then
			CheckFieldIsExist="error"
		Else
			CheckFieldIsExist="ok"
		End If
	End Function
	
	Public Function CheckGoodsFieldIsExist()
		Dim Fn_Field,Fn_Array
		Fn_Field  = LCase(KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","Field"),"[^0-9a-zA-Z_]",""),30))
		'判断字段是否已经存在
		If ID>0 Then
			Fn_Array=Array("Field:"& Fn_Field,"ID<>"& ID)
		Else
			Fn_Array=Array("Field:"& Fn_Field)
		End If
		If KnifeCMS.DB.CheckIsRecordExistByField(DBTable_GoodsFieldSet,Fn_Array) Then
			CheckGoodsFieldIsExist="error"
		Else
			CheckGoodsFieldIsExist="ok"
		End If
	End Function
	
	'获取附件列表数据
	Public Function GetAttachment()
		Dim Fn_FolderPath
		Fn_FolderPath = KnifeCMS.GetForm("get","folderpath")
		If Left(Fn_FolderPath,10)<>"attachment" Then
			GetAttachment = "{""error"":""true"",""error_message"":""folderpath_error""}"
		End If
		Select Case ActionType
		Case "folderlist"
			GetAttachment = KnifeCMS.FSO.FolderList("json",Fn_FolderPath)
		Case "filelist"
			GetAttachment = KnifeCMS.FSO.FileList("json",Fn_FolderPath)
		Case "subfolder_and_file_list"
			GetAttachment = "["& KnifeCMS.FSO.FolderList("json",Fn_FolderPath) &","& KnifeCMS.FSO.FileList("json",Fn_FolderPath) &",{""runtime"":"""& KnifeCMS.RunTime &"""}]"
		Case Else
			GetAttachment = "{""error"":""true"",""error_message"":""para[acttype]_error""}"
		End Select
	End Function
	
	'获取冗余文件夹和文件列表
	Public Function RedundantFoldersAndFiles()
		Dim Fn_FolderPath
		Fn_FolderPath = KnifeCMS.GetForm("get","folderpath")
		If Left(Fn_FolderPath,10)<>"attachment" Then
			RedundantFoldersAndFiles = "{""error"":""true"",""error_message"":""folderpath_error""}"
		End If
		RedundantFoldersAndFiles = "["& RedundantFolders(Fn_FolderPath) &","& RedundantFiles(Fn_FolderPath) &",{""runtime"":"""& KnifeCMS.RunTime &"""}]"
	End Function
	
	'获取冗余文件夹列表
	Public Function RedundantFolders(ByVal BV_FolderPath)
		Dim Fn_FSO,Fn_Folder,Fn_SubFolder,Fn_FolderPath,Fn_Json,Fn_html,Fn_Temp

		Fn_FolderPath = KnifeCMS.Data.FolderPath(BV_FolderPath)

		Set Fn_FSO = KnifeCMS.FSO.ObjectFSO
		If Fn_FSO.FolderExists(Server.MapPath(SystemPath & Fn_FolderPath))=False Then
			RedundantFolders = "{""foldername"":"""& KnifeCMS.Data.FolderName(Fn_FolderPath) &""",""size"":""0"",""datetime"":"""",""folderpath"":"""& Fn_FolderPath &""",""subfolderscount"":""0"",""subfolders"":[],""folderexists"":""false""}"
			Exit Function
		End If

		Set Fn_Folder=Fn_FSO.GetFolder(Server.MapPath(SystemPath & Fn_FolderPath))
		For Each Fn_SubFolder in Fn_Folder.SubFolders
			If (Fn_SubFolder.SubFolders.Count + Fn_SubFolder.Files.Count) = 0 Then
			'如果文件夹为空，则可以判定为冗余文件夹
				Fn_Temp = "{""foldername"":"""& Fn_SubFolder.name &""",""size"":"""& KnifeCMS.Data.FormatSize(Fn_SubFolder.Size) &""",""datetime"":"""& Fn_SubFolder.datelastmodified &""",""folderpath"":"""& Fn_FolderPath & Fn_SubFolder.name &""",""subfolderscount"":"""& Fn_SubFolder.SubFolders.Count &""",""subfolders"":[],""folderexists"":""true""}"
				If Fn_Json="" Then
					Fn_Json = Fn_Temp
				Else
					Fn_Json = Fn_Json &","& Fn_Temp
				End If
			Else
				If Fn_SubFolder.SubFolders.Count>0 Then
					Fn_Temp = SubRedundantFolders(Fn_FolderPath & Fn_SubFolder.name)
					If Fn_Temp<>"" Then
						If Fn_Json="" Then
							Fn_Json = Fn_Temp
						Else
							Fn_Json = Fn_Json &","& Fn_Temp
						End If
					End If
				End If
			End If
		Next
		Fn_Json = "{""foldername"":"""& KnifeCMS.Data.FolderName(Fn_FolderPath) &""",""size"":"""& KnifeCMS.Data.FormatSize(Fn_Folder.Size) &""",""datetime"":"""& Fn_Folder.datelastmodified &""",""folderpath"":"""& Fn_FolderPath &""",""subfolderscount"":"""& Fn_Folder.SubFolders.Count &""",""subfolders"":["& Fn_Json &"],""folderexists"":""true""}"
		
		Set Fn_Folder = Nothing
		Set Fn_FSO    = Nothing
		
		RedundantFolders = Fn_Json
	End Function
	
	'循环获取冗余子文件夹
	Public Function SubRedundantFolders(ByVal BV_FolderPath)
		Dim Fn_FSO,Fn_Folder,Fn_SubFolder,Fn_FolderPath,Fn_Json,Fn_html,Fn_Temp
		Fn_FolderPath = KnifeCMS.Data.FolderPath(BV_FolderPath)
		Set Fn_FSO = KnifeCMS.FSO.ObjectFSO
		Set Fn_Folder=Fn_FSO.GetFolder(Server.MapPath(SystemPath & Fn_FolderPath))
		For Each Fn_SubFolder in Fn_Folder.SubFolders
			If (Fn_SubFolder.SubFolders.Count + Fn_SubFolder.Files.Count) = 0 Then
				Fn_Temp = "{""foldername"":"""& Fn_SubFolder.name &""",""size"":"""& KnifeCMS.Data.FormatSize(Fn_SubFolder.Size) &""",""datetime"":"""& Fn_SubFolder.datelastmodified &""",""folderpath"":"""& Fn_FolderPath & Fn_SubFolder.name &""",""subfolderscount"":"""& Fn_SubFolder.SubFolders.Count &""",""subfolders"":[],""folderexists"":""true""}"
				If Fn_Json="" Then
					Fn_Json = Fn_Temp
				Else
					Fn_Json = Fn_Json &","& Fn_Temp
				End If
			Else
				If Fn_SubFolder.SubFolders.Count>0 Then
					Fn_Temp = SubRedundantFolders(Fn_FolderPath & Fn_SubFolder.name)
					If Fn_Temp<>"" Then
						If Fn_Json="" Then
							Fn_Json = Fn_Temp
						Else
							Fn_Json = Fn_Json &","& Fn_Temp
						End If
					End If
				End If
			End If
		Next
		Set Fn_Folder = Nothing
		Set Fn_FSO    = Nothing
		SubRedundantFolders = Fn_Json
	End Function
	
	'获取冗余文件列表
	Public Function RedundantFiles(BV_FolderPath)
		Dim Fn_FSO,Fn_File,Fn_Folder,Fn_FolderPath,Fn_SubFolder,Fn_Json,Fn_html,Fn_Temp

		Fn_FolderPath = KnifeCMS.Data.FolderPath(BV_FolderPath)

		Set Fn_FSO = KnifeCMS.FSO.ObjectFSO
		If Fn_FSO.FolderExists(Server.MapPath(SystemPath & Fn_FolderPath))=False Then
			RedundantFiles = "{""foldername"":"""& KnifeCMS.Data.FolderName(Fn_FolderPath) &""",""size"":""0"",""datetime"":"""",""folderpath"":"""& Fn_FolderPath &""",""filecount"":""0"",""files"":[],""folderexists"":""false""}"
			Exit Function
		End If
		Set Fn_Folder=Fn_FSO.GetFolder(Server.MapPath(SystemPath & Fn_FolderPath))
		For Each Fn_File in Fn_Folder.Files
			If IsRedundantFile(Fn_FolderPath & Fn_File.name) Then
				Fn_Temp = "{""filename"":"""& Fn_File.name &""",""size"":"""& KnifeCMS.Data.FormatSize(Fn_File.Size) &""",""datetime"":"""& Fn_File.datelastmodified &""",""filepath"":"""& Fn_FolderPath & Fn_File.name &"""}"
				If Fn_Json = "" Then
					Fn_Json = Fn_Temp
				Else
					Fn_Json = Fn_Json &","& Fn_Temp
				End If
			End If
		Next
		If Fn_Folder.SubFolders.Count>0 Then
			For Each Fn_SubFolder in Fn_Folder.SubFolders
				Fn_Temp = SubFolderRedundantFiles(Fn_FolderPath & Fn_SubFolder.name)
				If Fn_Temp<>"" Then
					If Fn_Json = "" Then
						Fn_Json = Fn_Temp
					Else
						Fn_Json = Fn_Json &","& Fn_Temp
					End If
				End If
			Next
		End If
		Fn_Json = "{""foldername"":"""& KnifeCMS.Data.FolderName(Fn_FolderPath) &""",""size"":"""& KnifeCMS.Data.FormatSize(Fn_Folder.Size) &""",""datetime"":"""& Fn_Folder.datelastmodified &""",""folderpath"":"""& Fn_FolderPath &""",""filecount"":"""& Fn_Folder.Files.Count &""",""files"":["& Fn_Json &"]}"
		
		Set Fn_Folder = Nothing
		Set Fn_FSO    = Nothing

		RedundantFiles = Fn_Json
	End Function
	
	'循环获取[子文件夹]冗余文件列表
	Public Function SubFolderRedundantFiles(BV_FolderPath)
		Dim Fn_FSO,Fn_File,Fn_Folder,Fn_FolderPath,Fn_SubFolder,Fn_Json,Fn_html,Fn_Temp
		Fn_FolderPath = KnifeCMS.Data.FolderPath(BV_FolderPath)
		Set Fn_FSO = KnifeCMS.FSO.ObjectFSO
		Set Fn_Folder=Fn_FSO.GetFolder(Server.MapPath(SystemPath & Fn_FolderPath))
		For Each Fn_File in Fn_Folder.Files
			If IsRedundantFile(Fn_FolderPath & Fn_File.name) Then
				Fn_Temp = "{""filename"":"""& Fn_File.name &""",""size"":"""& KnifeCMS.Data.FormatSize(Fn_File.Size) &""",""datetime"":"""& Fn_File.datelastmodified &""",""filepath"":"""& Fn_FolderPath & Fn_File.name &"""}"
				If Fn_Json = "" Then
					Fn_Json = Fn_Temp
				Else
					Fn_Json = Fn_Json &","& Fn_Temp
				End If
			End If
		Next
		If Fn_Folder.SubFolders.Count>0 Then
			For Each Fn_SubFolder in Fn_Folder.SubFolders
				Fn_Temp = SubFolderRedundantFiles(Fn_FolderPath & Fn_SubFolder.name)
				If Fn_Temp<>"" Then
					If Fn_Json = "" Then
						Fn_Json = Fn_Temp
					Else
						Fn_Json = Fn_Json &","& Fn_Temp
					End If
				End If
			Next
		End If
		Set Fn_Folder = Nothing
		Set Fn_FSO    = Nothing
		SubFolderRedundantFiles = Fn_Json
	End Function
	
	'获取需要检测冗余文件的数据库表
	Public Function GetRedundFilesDBTable()
		Dim Fn_ii,Fn_Rs,Fn_TableStr,Fn_TableArray,Fn_Temp
		If CloseModuleShop<>1 Then
			Fn_TableStr = "Goods,GoodsClass,GoodsVirtualClass,Delivery,DeliveryCorp,Payments,Brand,"
		End If
		If CloseModuleVote<>1 Then
			Fn_TableStr = Fn_TableStr &"VoteClass,"
		End If
		Fn_TableStr = Fn_TableStr &"CModule,Link,CollectionResult,Ads_Detail,MailTemplate,Members"
		Fn_Temp     = ""
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT System FROM ["& DBTable_ContentSubSystem &"] ORDER BY Orders ASC")
        Do While Not(Fn_Rs.Eof)
			If Fn_Temp = "" Then
				Fn_Temp = "Content_"& Fn_Rs(0) &","& "Content_"& Fn_Rs(0) &"_Class"
			Else
				Fn_Temp = Fn_Temp &",Content_"& Fn_Rs(0) &","& "Content_"& Fn_Rs(0) &"_Class"
			End If
		Fn_Rs.Movenext()
        Loop
        KnifeCMS.DB.CloseRs Fn_Rs
		If Fn_Temp<>"" Then
			Fn_TableStr = Fn_Temp &","& Fn_TableStr
		End If
		
		'获取所有可遍历的字段
		Fn_TableArray = Split(Fn_TableStr,",")
		Fn_TableStr   = ""
		For Fn_ii=0 To Ubound(Fn_TableArray)
			Fn_Temp = Fn_TableArray(Fn_ii) &"|"& GetCheckRedundFields(TablePre & Fn_TableArray(Fn_ii))
			If Fn_TableStr = "" Then
				Fn_TableStr = Fn_Temp
			Else
				Fn_TableStr = Fn_TableStr &","& Fn_Temp
			End If
		Next
		Pv_RedundDBTableArray = Split(Fn_TableStr,",")
		ReDim Pv_CheckRedundSQL(Ubound(Pv_RedundDBTableArray))
	End Function
	
	'获取需要检测冗余文件的数据库表字段
	Public Function GetCheckRedundFields(BV_DBTable)
		Dim Fn_Rs,Fn_ii,Fn_FieldType,Fn_FieldName,Fn_Result
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& BV_DBTable &"]")
		For Fn_ii=0 To Fn_Rs.Fields.Count-1 
			Fn_FieldType = Fn_Rs.Fields(Fn_ii).Type
			Fn_FieldName = Fn_Rs.Fields(Fn_ii).Name
			Select Case Fn_FieldType
			Case 129,200,201,130,202,203,8,133,134
				If Fn_Result="" Then
					Fn_Result = Fn_FieldName
				Else
					Fn_Result = Fn_Result &":"& Fn_FieldName
				End If
			End Select
		Next
		KnifeCMS.DB.CloseRs Fn_Rs
		GetCheckRedundFields = Fn_Result
	End Function
	
	Public Function CreateCheckRedundSQL()
		Dim Fn_ii,Fn_jj,Fn_Rs,Fn_SQL,Fn_Conditon,Fn_Array,Fn_DBTable,Fn_TableArray,Fn_Filed,Fn_FiledArray

		If Not(IsArray(Pv_RedundDBTableArray)) Then
			Call GetRedundFilesDBTable()
		End If
		Fn_TableArray = Pv_RedundDBTableArray
		For Fn_ii=0 To Ubound(Fn_TableArray)
			Fn_Array      = Split(Fn_TableArray(Fn_ii),"|")
			Fn_DBTable    = TablePre & Fn_Array(0)
			Fn_FiledArray = Split(Fn_Array(1),":")
			Fn_Conditon   = ""
			For Fn_jj=0 To Ubound(Fn_FiledArray)
				Fn_Filed    = Trim(Fn_FiledArray(Fn_jj))
				If Fn_Filed<>"" Then
					If Fn_Conditon = "" Then
						Fn_Conditon = " ["& Fn_Filed &"] LIKE '%{tpl:filepath}%' Or ["& Fn_Filed &"] LIKE '%{tpl:filepathencode}%' "
					Else
						Fn_Conditon = Fn_Conditon &" OR ["& Fn_Filed &"] LIKE '%{tpl:filepath}%' Or ["& Fn_Filed &"] LIKE '%{tpl:filepathencode}%' "
					End If
				End If
			Next
			'生成SQL语句
			Fn_SQL = "SELECT ID FROM ["& Fn_DBTable &"] WHERE "& Fn_Conditon &" ORDER BY ID ASC"
			Pv_CheckRedundSQL(Fn_ii) = Fn_SQL
		Next
	End Function
	
	'判断是否是冗余文件
	Public Function IsRedundantFile(ByVal BV_FilePath)
		Dim Fn_ii,Fn_Rs,Fn_SQL,Fn_SQLArray,Fn_Exists,Fn_Redundant
		Dim Fn_FilePath,Fn_FilePathEncode
		Fn_FilePath       = BV_FilePath
		Fn_FilePathEncode = KnifeCMS.Data.URLEncode(Fn_FilePath)
		Fn_Redundant = False
		'判断其他使用
		If Instr(SiteLogo,Fn_FilePath)>0 Or Instr(SiteLogo,Fn_FilePathEncode)>0 Then IsRedundantFile=False : Exit Function
		If Instr(SiteMyhomeLogo,Fn_FilePath)>0 Or Instr(SiteMyhomeLogo,Fn_FilePathEncode)>0 Then IsRedundantFile=False : Exit Function
		
		'开始判断
		If Not(IsArray(Pv_CheckRedundSQL)) Then
			Call CreateCheckRedundSQL()
		End If
		Fn_SQLArray  = Pv_CheckRedundSQL
		Fn_Redundant = True
		For Fn_ii=0 To Ubound(Fn_SQLArray)
			Fn_Redundant = False
			Fn_SQL = Fn_SQLArray(Fn_ii)
			Fn_SQL = Replace(Fn_SQL,"{tpl:filepath}",Fn_FilePath)
			Fn_SQL = Replace(Fn_SQL,"{tpl:filepathencode}",Fn_FilePathEncode)
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_SQL)
			If Fn_Rs.Bof And Fn_Rs.Eof Then
				Fn_Exists = False
			Else
				Fn_Exists = True
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
			If Fn_Exists = True Then
				Fn_Redundant = False
				Exit For
			Else
				Fn_Redundant = True
			End If
		Next
		IsRedundantFile = Fn_Redundant
	End Function
	
	'删除文件夹和文件
	Public Function DeleteFoldersAndFiles()
		On Error Resume Next
		Dim Fn_ii,Fn_JsonData,Fn_Json,Fn_FolderPath,Fn_FoldersJson,Fn_FilePath,Fn_FilesJson,Fn_Return,Fn_Result
		Fn_JsonData = KnifeCMS.GetForm("post","jsondata")
		
		If Trim(Fn_JsonData) = "" Then Exit Function
		Set Fn_Json = KnifeCMS.JSON.Parse(Fn_JsonData)
		'DeleteFoldersAndFiles = Fn_JsonData : Exit Function
		'删除文件夹
		For Fn_ii=0 To Fn_Json.folders.length-1
			Fn_FolderPath = Fn_Json.folders.get(Fn_ii).folderpath
			If Trim(Fn_FolderPath)<>"" Then
			
				Fn_Result = KnifeCMS.FSO.DeleteFolder(KnifeCMS.FSO.ABSPath(Fn_FolderPath))
				If Fn_Result Then
					Msg = Msg & Lang_DeleteFolderSuccess &"{"& Fn_FolderPath &"}<br>"
				Else
					Msg = Msg & Lang_DeleteFolderFail &"{"& Fn_FolderPath &"}<br>"
				End If
				Fn_Result = KnifeCMS.IIF(Fn_Result=True,"true","false")
				If Fn_FoldersJson = "" Then
					Fn_FoldersJson = "{""folderpath"":"""& Fn_FolderPath &""",""deleteresult"":"""& Fn_Result &"""}"
				Else
					Fn_FoldersJson = Fn_FoldersJson &",{""folderpath"":"""& Fn_FolderPath &""",""deleteresult"":"""& Fn_Result &"""}"
				End If
			End If
		Next
		
		'删除文件
		For Fn_ii=0 To Fn_Json.files.length-1
			Fn_FilePath = Fn_Json.files.get(Fn_ii).filepath
			If Trim(Fn_FilePath)<>"" Then
				Fn_Result = KnifeCMS.FSO.DeleteFile(KnifeCMS.FSO.ABSPath(Fn_FilePath))
				If Fn_Result Then
					Msg = Msg & Lang_DeleteFileSuccess &"{"& Fn_FilePath &"}<br>"
				Else
					Msg = Msg & Lang_DeleteFileFail &"{"& Fn_FilePath &"}<br>"
				End If
				Fn_Result = KnifeCMS.IIF(Fn_Result=True,"true","false")
				If Fn_FilesJson = "" Then
					Fn_FilesJson = "{""filepath"":"""& Fn_FilePath &""",""deleteresult"":"""& Fn_Result &"""}"
				Else
					Fn_FilesJson = Fn_FilesJson &",{""filepath"":"""& Fn_FilePath &""",""deleteresult"":"""& Fn_Result &"""}"
				End If
			End If
		Next
		Fn_JsonData = "{""folders"":["& Fn_FoldersJson &"],""files"":["& Fn_FilesJson &"]}"
		Fn_Return = Fn_JsonData
		Set Fn_Json = Nothing
		If Err.Number<>0 Then Err.Clear()
		Call Admin.SaveLog(Msg &"["& Lang_LocalUrl & KnifeCMS.GetUrl("") &"|"& Lang_FromUrl  & Cstr(Request.ServerVariables("HTTP_REFERER")) &"]")
		DeleteFoldersAndFiles = Fn_Return
	End Function
	
	'$:Ajax获取商品类型属性
	Public Function GetGoodsModelAttribute(ByVal BV_GoodsModelID)
		Dim Fn_ID,Fn_AttrName,Fn_AttrType,Fn_AttrInputType,Fn_AttrValues,Fn_TempArray,Fn_i
		BV_GoodsModelID=KnifeCMS.Data.CLng(BV_GoodsModelID)
		Set Pv_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,AttrName,AttrType,AttrInputType,AttrValues FROM ["& TablePre &"GoodsModelAttribute] WHERE GoodsModelID="& BV_GoodsModelID &" ORDER BY AttrOrder ASC,ID DESC")
		If (Pv_Rs.bof and Pv_Rs.eof) Then
			Response.Write("<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""infoTable""><body><tr><td class=""titletd""></td><td class=""infotd"">"& Lang_NoneAttr &"</td></tr></body></table>")
		Else
			Response.Write("<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""infoTable""><body>")
			do while not Pv_Rs.eof
				Fn_ID            = Pv_Rs("ID")
				Fn_AttrName      = Pv_Rs("AttrName")
				Fn_AttrType      = Pv_Rs("AttrType")
				Fn_AttrInputType = Pv_Rs("AttrInputType")
				Fn_AttrValues    = Pv_Rs("AttrValues")
				Response.Write"<tr><td class=""titletd"">"& Fn_AttrName &":</td><td class=""infotd"">"
				Select Case Fn_AttrInputType
				Case 0
					If Fn_AttrType > 0 Then
					  Response.Write"<table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""listTable""><tbody><tr class=top><th>"
					  Response.Write"<div style=""width:90px;""><span class=sysBtn><a onclick=""Admin.Goods.AddGoodsAttr(this)"" href=""javascript:void(0);""><b class=""icon icon_add""></b>"& Lang_DoAddOne &"</a></span></div>"
					  Response.Write"</th><th><div style=""width:100px;"">"& Lang_AttrValue &"</div></th><th><div style=""width:200px;"">"& Lang_AttrPrice &"</div></th></tr>"
					  Response.Write"<tr class=data><td style=""text-align:center;"">"& Lang_Default &"</td><td>"
					  
					  Response.Write"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_ID&""" />"
					  Response.Write"<input type=""text"" class=""TxtClass"" name=""Attr_Value_List[]"" value="""" />"
					  Response.Write"</td><td>"
					  Response.Write"<input type=""text"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""" />"
					  
					  Response.Write"</td></tr><tbody></table>"
					Else
					  Response.Write"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_ID&""" />"
					  Response.Write"<input type=""text"" class=""TxtClass"" name=""Attr_Value_List[]"" value="""" />"
					  Response.Write"<input type=""hidden"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""" />"
					End If
				Case 1
					If Fn_AttrType > 0 Then
					  Response.Write"<table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""listTable""><tbody><tr class=top><th>"
					  Response.Write"<div style=""width:90px;""><span class=sysBtn><a onclick=""Admin.Goods.AddGoodsAttr(this)"" href=""javascript:void(0);""><b class=""icon icon_add""></b>"& Lang_DoAddOne &"</a></span></div>"
					  Response.Write"</th><th><div style=""width:100px;"">"& Lang_AttrValue &"</div></th><th><div style=""width:200px;"">"& Lang_AttrPrice &"</div></th></tr>"
					  Response.Write"<tr class=data><td style=""text-align:center;"">"& Lang_Default &"</td><td>"
					  
					  Response.Write"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_ID&""" />"
					  Fn_TempArray=split(Fn_AttrValues,"\n")
					  Response.Write"<select name=""Attr_Value_List[]"" style=""font-size:13px;""><option value="""">"& Lang_PleaseSelect &"</option>"
					  for Fn_i=0 to Ubound(Fn_TempArray) : Response.Write"<option value='"&Fn_TempArray(Fn_i)&"'>"&Fn_TempArray(Fn_i)&"</option>" : next
					  Response.Write"</select>"
					  Response.Write"</td><td>"
					  Response.Write"<input type=""text"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""" />"
						
					  Response.Write"</td></tr><tbody></table>"
					Else
					  Response.Write"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_ID&""" />"
					  Fn_TempArray=split(Fn_AttrValues,"\n")
					  Response.Write"<select name=""Attr_Value_List[]"" style=""font-size:13px;""><option value="""">"& Lang_PleaseSelect &"</option>"
					  for Fn_i=0 to Ubound(Fn_TempArray) : Response.Write"<option value='"&Fn_TempArray(Fn_i)&"'>"&Fn_TempArray(Fn_i)&"</option>" : next
					  Response.Write"</select>"
					  Response.Write"<input type=""hidden"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""" />"
					End If
				Case 2
					If Fn_AttrType > 0 Then
					  Response.Write"<table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""listTable""><tbody><tr class=top><th>"
					  Response.Write"<div style=""width:90px;""><span class=sysBtn><a onclick=""Admin.Goods.AddGoodsAttr(this)"" href=""javascript:void(0);""><b class=""icon icon_add""></b>"& Lang_DoAddOne &"</a></span></div>"
					  Response.Write"</th><th><div style=""width:100px;"">"& Lang_AttrValue &"</div></th><th><div style=""width:200px;"">"& Lang_AttrPrice &"</div></th></tr>"
					  Response.Write"<tr class=data><td style=""text-align:center;"">"& Lang_Default &"</td><td>"
					  
					  Response.Write"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_ID&""" />"
					  Response.Write"<textarea class=""mini"" name=""Attr_Value_List[]""></textarea>"
					  Response.Write"</td><td>"
					  Response.Write"<input type=""text"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""" />"
						
					  Response.Write"</td></tr><tbody></table>"
					Else
					  Response.Write"<input type=""hidden"" class=""TxtClass"" name=""Attr_ID_List[]"" value="""&Fn_ID&""" />"
					  Response.Write"<textarea class=""mini"" name=""Attr_Value_List[]""></textarea>"
					  Response.Write"<input type=""hidden"" class=""TxtClass"" name=""Attr_Price_List[]"" style=""width:120px;"" value="""" />"
					End If
				Case else
					Response.Write(""&TablePre&"GoodsModelAttribute.AttrInputType in Database is Wrong!")
				End Select
				Response.Write"</td></tr>"
				Pv_Rs.movenext
			loop
			Response.Write("</body></table>")
		End If
		KnifeCMS.DB.CloseRs Pv_Rs
	End Function
	
	Public Function GetRegions(ByVal BV_Region_Path)
		Dim Fn_Rs, Fn_Rs2, Fn_P_Region_ID, Fn_TempHTML, Fn_i
		Dim Fn_OrderNum, Fn_Region_Path, Fn_Local_Name, Fn_Region_Grade , Fn_ChildNum
		'检查该地区是否存在
		Set Fn_Rs = KnifeCMS.DB.GetRecord(TablePre&"Regions:Local_Name",Array("Region_Path:"&BV_Region_Path),"")
		If Not(Fn_Rs.Eof) Then Fn_Local_Name = Fn_Rs(0)
		KnifeCMS.DB.CloseRs Fn_Rs
		If (Not(KnifeCMS.Data.IsNul(BV_Region_Path)) And BV_Region_Path <> ",") And KnifeCMS.Data.IsNul(Fn_Local_Name) Then
			Echo Lang_Region(5) '参数错误,无法获取下级地址.
			Exit Function
		End If
		Fn_Region_Path = Split(BV_Region_Path,",")
		If Ubound(Fn_Region_Path) >= 2 Then
			Fn_Region_Grade = KnifeCMS.Data.CLng(Ubound(Fn_Region_Path))
			Fn_P_Region_ID  = KnifeCMS.Data.CLng(Fn_Region_Path(Fn_Region_Grade-1))
			Set Fn_Rs = KnifeCMS.DB.GetRecord(TablePre&"Regions:Region_Path,Region_Grade,Local_Name,OrderNum,(SELECT COUNT(*) FROM ["&TablePre&"Regions] b WHERE b.Region_Path LIKE '"&"Region_Path"&"%') AS ChildNum",Array("P_Region_ID:"&Fn_P_Region_ID,"Region_Grade:"&Fn_Region_Grade),"ORDER BY OrderNum ASC,ID ASC")
		Else
			Set Fn_Rs = KnifeCMS.DB.GetRecord(TablePre&"Regions:Region_Path,Region_Grade,Local_Name,OrderNum,(SELECT COUNT(*) FROM ["&TablePre&"Regions] b WHERE b.Region_Path LIKE '"&"Region_Path"&"%') AS ChildNum",Array("P_Region_ID:NULL"),"ORDER BY OrderNum ASC,ID ASC")
		End If
		Fn_i        = 1
		Fn_TempHTML = "{"""&Fn_Local_Name&""":["
		Fn_Local_Name = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_Local_Name),"NULL",Fn_Local_Name)
		Do While Not(Fn_Rs.Eof)
			Fn_Region_Path = KnifeCMS.Data.Escape(Fn_Rs("Region_Path"))
			Fn_Region_Grade= Fn_Rs("Region_Grade")
			Fn_Local_Name  = KnifeCMS.Data.Escape(Fn_Rs("Local_Name"))
			Fn_OrderNum    = KnifeCMS.Data.CLng(Fn_Rs("OrderNum"))
			'获取子级地区数
			Pv_Array = Split(Fn_Rs("Region_Path"),",")
			Fn_P_Region_ID = Pv_Array(Ubound(Pv_Array)-1)
			Set Fn_Rs2 = KnifeCMS.DB.GetRecordBySQL("SELECT COUNT(*) FROM ["&TablePre&"Regions] WHERE P_Region_ID="&Fn_P_Region_ID&"")
			If Not(Fn_Rs2.Eof) Then Fn_ChildNum=Fn_Rs2(0)
			KnifeCMS.DB.CloseRs Fn_Rs2
			If Fn_i > 1 Then Fn_TempHTML = Fn_TempHTML &","
			Fn_TempHTML = Fn_TempHTML &"{""Region_Path"":"""&Fn_Region_Path&""",""Region_Grade"":"""&Fn_Region_Grade&""",""Local_Name"":"""&Fn_Local_Name&""",""OrderNum"":"""&Fn_OrderNum&""",""ChildNum"":"""&Fn_ChildNum&"""}"
			Fn_i = Fn_i +1
			Fn_Rs.Movenext()
		Loop
		Fn_TempHTML = Fn_TempHTML &"]}"
		KnifeCMS.DB.CloseRs Fn_Rs
		Echo Fn_TempHTML
	End Function
	
	Public Function RegionsDoSave()
		Dim Fn_Rs,Fn_Check,Fn_DBTable,Fn_MaxID,Fn_P_Region_Path,Fn_P_Region_ID,Fn_Local_Name,Fn_Region_Path,Fn_OrderNum
		Dim Fn_Arr,Fn_Region_Grade,Fn_P_Local_Name
		Fn_DBTable = TablePre&"Regions"
		Fn_Check   = True
		
		Fn_P_Region_Path = KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","P_Region_Path"),"[^0-9,]","")
		Fn_Region_Path   = KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","Region_Path"),"[^0-9,]","")
		Fn_Local_Name    = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","Local_Name"),50)
		Fn_OrderNum      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","OrderNum"))
		
		If Action = "add" Then
			'数据合法性检测并处理数据
			If KnifeCMS.Data.IsNul(Fn_P_Region_Path)  Then ErrMsg = Lang_Region(9) &"<br>"
			If KnifeCMS.Data.IsNul(Fn_Local_Name)     Then ErrMsg = ErrMsg & Lang_Region(10) &"<br>"
			If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"",0 : Exit Function
			
			Fn_Arr           = Split(Fn_P_Region_Path,",")
			Fn_Region_Grade  = KnifeCMS.Data.CLng(Ubound(Fn_Arr))
			Fn_P_Region_ID   = KnifeCMS.Data.CLng(Fn_Arr(Fn_Region_Grade-1))
			Fn_P_Region_ID   = KnifeCMS.IIF(Fn_P_Region_ID=0,"NULL",Fn_P_Region_ID)
	
			'获取最大的ID
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT Max(ID) FROM ["& Fn_DBTable  &"]")
			If Not(Fn_Rs.Eof) Then Fn_MaxID = Fn_Rs(0)
			KnifeCMS.DB.CloseRs Fn_Rs
			Fn_MaxID = KnifeCMS.Data.CLng(Fn_MaxID)+1
			
			'检查地区是否已经存在,存在则提示错误.
			Set Fn_Rs = KnifeCMS.DB.GetRecord(Fn_DBTable,Array("P_Region_ID:"&Fn_P_Region_ID,"Local_Name:"&Fn_Local_Name),"")
			If Not(Fn_Rs.Eof) Then Fn_Check = False
			KnifeCMS.DB.CloseRs Fn_Rs
			If Not(Fn_Check) Then Echo """"& Fn_Local_Name&""""&Lang_Region(11) : Exit Function
			
			'检查父级地区是否存在,不存在则提示错误.
			If Fn_P_Region_Path<>"," Then
				Set Fn_Rs = KnifeCMS.DB.GetRecord(Fn_DBTable&":Local_Name",Array("Region_Path:"&Fn_P_Region_Path),"")
				If Fn_Rs.Eof Then Fn_Check = False : Fn_P_Local_Name = Fn_Rs(0)
				KnifeCMS.DB.CloseRs Fn_Rs
				If Not(Fn_Check) Then Echo Lang_Region(12)&"[Region_Path="""&Fn_P_Region_Path&"""]" : Exit Function
			End If
			
			'开始保存数据操作
			Result = KnifeCMS.DB.AddRecord(Fn_DBTable,Array("ID:"&Fn_MaxID,"Package:mainland","P_Region_ID:"&Fn_P_Region_ID,"Region_Path:"&Fn_P_Region_Path&Fn_MaxID&",","Region_Grade:"&Fn_Region_Grade,"Local_Name:"&Fn_Local_Name,"En_Name:","P_1:","P_2:","OrderNum:"&Fn_OrderNum,"Disabled:0"))
			If Result Then Admin.ShowMessage "success<br>"& Lang_Region(2) &":"&Fn_Local_Name,"",1
		ElseIf Action = "edit" Then
			'数据合法性检测并处理数据
			If KnifeCMS.Data.IsNul(Fn_Region_Path)  Then ErrMsg = Lang_Region(17) &"<br>"
			If KnifeCMS.Data.IsNul(Fn_Local_Name)   Then ErrMsg = ErrMsg & Lang_Region(15) &"<br>"
			If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"",0 : Exit Function
			
			Fn_Arr           = Split(Fn_Region_Path,",")
			Fn_Region_Grade  = KnifeCMS.Data.CLng(Ubound(Fn_Arr))
			Fn_P_Region_ID   = KnifeCMS.Data.CLng(Fn_Arr(Fn_Region_Grade-2))
			Fn_P_Region_ID   = KnifeCMS.IIF(Fn_P_Region_ID=0,"NULL",Fn_P_Region_ID)
			
			'检查地区是否已经存在,存在则提示错误.
			Set Fn_Rs = KnifeCMS.DB.GetRecord(Fn_DBTable,Array("P_Region_ID:"&Fn_P_Region_ID,"Local_Name:"&Fn_Local_Name)," AND Region_Path<>'"& Fn_Region_Path &"'")
			If Not(Fn_Rs.Eof) Then Fn_Check = False
			KnifeCMS.DB.CloseRs Fn_Rs
			If Not(Fn_Check) Then Echo """"& Fn_Local_Name&""""&Lang_Region(11) : Exit Function
			
			Result = KnifeCMS.DB.UpdateRecord(Fn_DBTable,Array("Local_Name:"&Fn_Local_Name,"OrderNum:"&Fn_OrderNum),Array("Region_Path:"&Fn_Region_Path))
			If Result Then Admin.ShowMessage "success<br>"& Lang_Region(3) &":"&Fn_Local_Name,"",1
		End If
		
	End Function
	
	Public Function RegionsDel()
		Dim Fn_Rs,Fn_Arr,Fn_Region_Path,Fn_Local_Name
		Dim Fn_DBTable : Fn_DBTable = TablePre&"Regions"
		Dim Fn_Check   : Fn_Check   = True
		Fn_Region_Path = KnifeCMS.GetForm("post","Region_Path")
		Fn_Region_Path = KnifeCMS.Data.RegReplace(Fn_Region_Path,"[^0-9,]","")
		Fn_Arr         = Split(Fn_Region_Path,",")
		'数据合法性检测并处理数据
		If KnifeCMS.Data.IsNul(Fn_Region_Path) Or Ubound(Fn_Arr)<2 Then ErrMsg = Lang_Region(16)&"<br>"
		If ErrMsg<>"" Then Echo ErrMsg : Exit Function
		
		'检查地区是否存在,不存在则提示错误.
		Set Fn_Rs = KnifeCMS.DB.GetRecord(Fn_DBTable&":Local_Name",Array("Region_Path:"&Fn_Region_Path),"")
		If Fn_Rs.Eof Then
			Fn_Check = False
		Else
		    Fn_Local_Name = Fn_Rs(0)
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		If Not(Fn_Check) Then Echo Lang_Region(13) : Exit Function
		
		KnifeCMS.DB.Execute("DELETE FROM ["&Fn_DBTable&"] WHERE Region_Path LIKE '"&Fn_Region_Path&"%'")
		Echo "success"
		Admin.ShowMessage Replace(Lang_Region(4),"{$:Local_Name}",Fn_Local_Name),"",1
	End Function
	
	Public Function GetSubSystemClassList(ByVal BV_DefaultClassID, ByVal BV_System)
		Echo "<select id=""ClassID"" name=""ClassID"">"
		Echo "<option value=""0"" seourl="""" >"& Lang_PleaseSelect &"</option>"
		Echo Admin.ShowClass_Option(0,BV_DefaultClassID,TablePre&"Content_"&BV_System&"_Class")
		Echo "</select>"
	End Function
	
	Public Function GetContentSubSystemOptions()
		Dim Fn_i,Fn_Rs,Fn_System,Fn_SystemName,Fn_TempHTML
		Fn_i = 1
		Fn_TempHTML = "{"""&Fn_Local_Name&""":["
		Set Fn_Rs = KnifeCMS.DB.GetRecord(TablePre&"ContentSubSystem:System,SystemName","","")
		Do While Not(Fn_Rs.Eof)
			Fn_System     = Fn_Rs(0)
			Fn_SystemName = KnifeCMS.Data.Escape(Fn_Rs(1))
			If Fn_i > 1 Then Fn_TempHTML = Fn_TempHTML &","
			Fn_TempHTML   = Fn_TempHTML &"{""System"":"""&Fn_System&""",""SystemName"":"""&SystemName&"""}"
			Fn_i = Fn_i +1
			Fn_Rs.Movenext()
		Loop
		Fn_TempHTML = Fn_TempHTML &"]}"
		KnifeCMS.DB.CloseRs Fn_Rs
	End Function
	
	Public Function SaveUserPassword()
		Dim Fn_Result,Fn_ReturnString,Fn_UserID,Fn_PasswordNew,Fn_RePasswordNew
		Fn_UserID        = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","userid"))
		Fn_PasswordNew   = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","password"),20)
		Fn_RePasswordNew = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","repassword"),20)
		If KnifeCMS.Data.IsNul(Fn_PasswordNew) Then SaveUserPassword="error_new_password_is_null" : Exit Function
		If Fn_PasswordNew <> Fn_RePasswordNew Then SaveUserPassword="error_two_passwords_is_not_accord" : Exit Function
		Fn_PasswordNew  = KnifeCMS.ParsePassWord(Fn_PasswordNew)
		Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("Password:"& Fn_PasswordNew),Array("ID:"&Fn_UserID))
		If Fn_Result Then
			Fn_ReturnString = "ok"
		Else
			Fn_ReturnString = "error_sql_updaterecord_fail"
		End If
		Echo Fn_ReturnString
	End Function
	
	Public Function SetLinkLabel()
		Dim Fn_Label,Fn_Value,Fn_ID
		DBTable  = DBTable_Link
		Fn_ID    = ID
		Fn_Label = LCase(KnifeCMS.GetForm("post","label"))
		Fn_Value = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","value"))
		If Fn_Value>0 Then
			Fn_Value = 1
		Else
			Fn_Value = 0
		End If
		Select Case Fn_Label
		Case "recommend" : Pv_Column = "Recommend"
		End Select
		Pv_Result = KnifeCMS.DB.UpdateRecord(DBTable,Array(Pv_Column &":"& Fn_Value),Array("ID:"& Fn_ID))
		If Pv_Result Then
			Pv_ReturnString = "ok"
		Else
			Pv_ReturnString = "error_sql_updaterecord_fail"
		End If
		SetLinkLabel = Pv_ReturnString
	End Function
	
	Public Function SetCModuleLabel()
		Dim Fn_Label,Fn_Value,Fn_ID
		DBTable  = DBTable_CModule
		Fn_ID    = ID
		Fn_Label = LCase(KnifeCMS.GetForm("post","label"))
		Fn_Value = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","value"))
		If Fn_Value>0 Then
			Fn_Value = 1
		Else
			Fn_Value = 0
		End If
		Select Case Fn_Label
		Case "recycle" : Pv_Column = "Recycle"
		End Select
		Pv_Result = KnifeCMS.DB.UpdateRecord(DBTable,Array(Pv_Column &":"& Fn_Value),Array("ID:"& Fn_ID))
		If Pv_Result Then
			Pv_ReturnString = "ok"
		Else
			Pv_ReturnString = "error_sql_updaterecord_fail"
		End If
		SetCModuleLabel = Pv_ReturnString
	End Function
	
	Public Function SetGoodsLabel()
		Dim Fn_Label,Fn_Value,Fn_ID
		Fn_ID    = ID
		Fn_Label = LCase(KnifeCMS.GetForm("post","label"))
		Fn_Value = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","value"))
		If Fn_Value>0 Then
			Fn_Value = 1
		Else
			Fn_Value = 0
		End If
		Select Case Fn_Label
		Case "recommend"         : Pv_Column = "Recommend"
		Case "hot"               : Pv_Column = "Hot"
		Case "new"               : Pv_Column = "New"
		Case "homepagerecommend" : Pv_Column = "HomepageRecommend"
		End Select
		Pv_Result = KnifeCMS.DB.UpdateRecord(DBTable_Goods,Array(Pv_Column &":"& Fn_Value),Array("ID:"& Fn_ID))
		If Pv_Result Then
			Pv_ReturnString = "ok"
		Else
			Pv_ReturnString = "error_sql_updaterecord_fail"
		End If
		SetGoodsLabel = Pv_ReturnString
	End Function
	
	Public Function SetContentLabel()
		Dim Fn_Label,Fn_Value,Fn_ID
		Dim Fn_ContentSubSystem
		If Ubound(C_TempArr)>=1 Then
			Fn_ContentSubSystem = KnifeCMS.Data.Left(KnifeCMS.Data.FiltNotLetter(C_TempArr(1)),50)
		End If
		DBTable  = TablePre &"Content_"& Fn_ContentSubSystem
		Fn_ID    = ID
		Fn_Label = LCase(KnifeCMS.GetForm("post","label"))
		Fn_Value = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","value"))
		If Fn_Value>0 Then
			Fn_Value = 1
		Else
			Fn_Value = 0
		End If
		Select Case Fn_Label
		Case "topline" : Pv_Column = "Topline"
		Case "ontop"   : Pv_Column = "Ontop"
		Case "hot"     : Pv_Column = "Hot"
		Case "elite"   : Pv_Column = "Elite"
		End Select
		Pv_Result = KnifeCMS.DB.UpdateRecord(DBTable,Array(Pv_Column &":"& Fn_Value),Array("ID:"& Fn_ID))
		If Pv_Result Then
			Pv_ReturnString = "ok"
		Else
			Pv_ReturnString = "error_sql_updaterecord_fail"
		End If
		SetContentLabel = Pv_ReturnString
	End Function
	
	Public Function SetCategoryIsPageContent()
		Dim Fn_subSys,Fn_Label,Fn_Value,Fn_ID
		Fn_subSys = KnifeCMS.Data.Left(KnifeCMS.Data.FiltNotLetter(C_TempArr(0)),50)
		Select Case Fn_subSys
			Case "vote"
				DBTable = TablePre &"VoteClass"
			Case "goods"
				DBTable = TablePre &"GoodsClass"
			Case "goodsvirtual"
				DBTable = TablePre &"GoodsVirtualClass"
			Case Else
				'内容模型
				If Ubound(C_TempArr)>=1 Then
					Fn_subSys = KnifeCMS.Data.Left(KnifeCMS.Data.FiltNotLetter(C_TempArr(1)),50)
				End If
				DBTable = TablePre &"Content_"& Fn_subSys &"_Class"
		End Select
		Fn_ID    = ID
		Fn_Value = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","value"))
		If Fn_Value>0 Then
			Fn_Value = 1
		Else
			Fn_Value = 0
		End If
		Pv_Result = KnifeCMS.DB.UpdateRecord(DBTable,Array("ClassType:"& Fn_Value),Array("ID:"& Fn_ID))
		If Pv_Result Then
			Pv_ReturnString = "ok"
		Else
			Pv_ReturnString = "error_sql_updaterecord_fail"
		End If
		SetCategoryIsPageContent = Pv_ReturnString
	End Function
	
	Public Function SetCategoryIsShow()
		Dim Fn_subSys,Fn_Label,Fn_Value,Fn_ID
		Fn_subSys = KnifeCMS.Data.Left(KnifeCMS.Data.FiltNotLetter(C_TempArr(0)),50)
		Select Case Fn_subSys
			Case "vote"
				DBTable = TablePre &"VoteClass"
			Case "goods"
				DBTable = TablePre &"GoodsClass"
			Case "goodsvirtual"
				DBTable = TablePre &"GoodsVirtualClass"
			Case Else
				'内容模型
				If Ubound(C_TempArr)>=1 Then
					Fn_subSys = KnifeCMS.Data.Left(KnifeCMS.Data.FiltNotLetter(C_TempArr(1)),50)
				End If
				DBTable = TablePre &"Content_"& Fn_subSys &"_Class"
		End Select
		Fn_ID    = ID
		Fn_Value = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","value"))
		If Fn_Value>0 Then
			Fn_Value = 1
		Else
			Fn_Value = 0
		End If
		Pv_Result = KnifeCMS.DB.UpdateRecord(DBTable,Array("ClassHide:"& Fn_Value),Array("ID:"& Fn_ID))
		If Pv_Result Then
			Pv_ReturnString = "ok"
		Else
			Pv_ReturnString = "error_sql_updaterecord_fail"
		End If
		SetCategoryIsShow = Pv_ReturnString
	End Function
	
	Public Function SetNavigatorIsShow()
		Dim Fn_Value,Fn_ID
		Fn_ID    = ID
		Fn_Value = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","value"))
		If Fn_Value>0 Then
			Fn_Value = 1
		Else
			Fn_Value = 0
		End If
		Pv_Result = KnifeCMS.DB.UpdateRecord(DBTable_Navigator,Array("Disabled:"& Fn_Value),Array("ID:"& Fn_ID))
		If Pv_Result Then
			Pv_ReturnString = "ok"
		Else
			Pv_ReturnString = "error_sql_updaterecord_fail"
		End If
		SetNavigatorIsShow = Pv_ReturnString
	End Function
	
	
	Private Sub Class_Terminate()
	End Sub
End Class
%>