<!--#include file="config.admin.asp"-->
<%
Dim Navigator
Set Navigator = New Class_KnifeCMS_Admin_Navigator
Set Navigator = Nothing
Class Class_KnifeCMS_Admin_Navigator

	Private Pv_Len,Pv_i,Pv_Arr,Pv_TempHTML,Pv_AllowDepth
	Private NavDefault,NavType,NavName,NavUrl,P_NavID,P_NavPath,NavPath,NavDepth,OrderNum,Target,Disabled,MetaKeywords,MetaDescription
	Private Pv_CanAddSubNav
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "navigator.lang.asp"
		Pv_AllowDepth = 3
		Target        = "_self"
		P_NavPath     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","p_navpath"),"[^0-9,]",""),50)
		Header()
		If Ctl = "navigator" Then
			Select Case Action
				Case "view"
					NavigatorView
				Case "add","edit"
					If SubAction="save" Then
						NavigatorDoSave
					Else
						If Action = "add" Then
							NavigatorDo
						ElseIf Action = "edit" Then
								Set Rs = KnifeCMS.DB.GetRecord(DBTable_Navigator,Array("ID:"&ID),"")
								If Not(Rs.Eof) Then
									NavType     = KnifeCMS.Data.CLng(Rs("NavType"))
									NavName     = KnifeCMS.Data.HTMLDecode(Rs("NavName"))
									NavUrl      = Rs("NavUrl")
									NavPath     = Rs("NavPath")
									OrderNum    = Rs("OrderNum")
									Target      = Rs("Target")
									Disabled    = Rs("Disabled")
									MetaKeywords    = Rs("MetaKeywords")
									MetaDescription = Rs("MetaDescription")			
								Else
									Admin.ShowMessage Lang_Navigator_Cue(7),"goback",0 : Die ""
								End IF
								KnifeCMS.DB.CloseRs Rs
								P_NavPath = Replace(NavPath,ID&",","")
								NavigatorDo
						End IF
					End IF
				Case "batchedit" : If SubAction="save" Then SaveBatchEdit
				Case "del"  : NavigatorDel
			End Select
		Else
		End If
		Footer()
	End Sub
	
	'   BV_ParID: 父级分类ID
	'   DB_Table: 分类数据库表
	Private Function NavigatorList(ByVal BV_DB_Table, ByVal BV_P_NavID, ByVal BV_Other)
		Dim Fn_Rs,Fn_i,Fn_LastID,Fn_Len : Fn_Len = Len(SystemPath)
		Dim Fn_ID,Fn_NavDefault,Fn_NavType,Fn_NavName,Fn_NavUrl,Fn_NavPath,Fn_NavDepth,Fn_ChildNum,Fn_OrderNum,Fn_Target,Fn_Disabled
		
		BV_P_NavID  = KnifeCMS.Data.CLng(BV_P_NavID) : BV_P_NavID  = KnifeCMS.IIF(BV_P_NavID<0,0,BV_P_NavID)
		BV_DB_Table = CStr(BV_DB_Table)
		Set Fn_Rs = KnifeCMS.DB.GetRecord(BV_DB_Table&":ID,NavDefault,NavType,NavName,NavUrl,NavPath,NavDepth,ChildNum,OrderNum,Target,Disabled",Array("P_NavID:"&BV_P_NavID),"ORDER BY NavType,OrderNum,ID ASC")
		If Fn_Rs.Bof And Fn_Rs.Eof Then
			Echo "<tr class=solid><td colspan=9>none...</td></tr>"
		Else
			Fn_Rs.Movelast
			Fn_LastID = Fn_Rs(0)
			Fn_Rs.Movefirst
			Do While Not Fn_Rs.Eof'-----------------------------------------------------------
				Fn_ID        = KnifeCMS.Data.CLng(Fn_Rs(0))
				Fn_NavDefault= KnifeCMS.Data.CLng(Fn_RS("NavDefault"))
				Fn_NavType   = Fn_Rs("NavType")
				Fn_NavName   = KnifeCMS.Data.HTMLDecode(Fn_Rs("NavName"))
				Fn_NavUrl    = Fn_Rs("NavUrl")
				If Not(InStr(Fn_NavUrl,"http://")>0) And KnifeCMS.Data.Left(Fn_NavUrl,Fn_Len)<>SystemPath Then Fn_NavUrl = SystemPath & Fn_NavUrl
				Fn_NavPath   = Fn_Rs("NavPath")
				Fn_NavDepth  = Fn_Rs("NavDepth")
				Fn_ChildNum  = Fn_Rs("ChildNum")
				Fn_OrderNum  = Fn_Rs("OrderNum")
				Fn_Target    = Fn_Rs("Target")
				Fn_Disabled  = Fn_Rs("Disabled")
				Echo "<tr class=solid>"
				Echo "<td class=firstl><input type=""hidden"" name=""NavID"" value="""&Fn_ID&""" />"
				If Fn_NavType = 1 Then
					Pv_CanAddSubNav = False
					Echo "<span style=""color:#333;"">"& Lang_NavigatorType(1) &"</span>"
				ElseIf Fn_NavType = 2 Then
					Pv_CanAddSubNav = True
					Echo "<span style=""color:#333;"">"& Lang_NavigatorType(2) &"</span>"
				ElseIf Fn_NavType = 3 Then
					Pv_AllowDepth   = 2
					Pv_CanAddSubNav = True
					Echo "<span style=""color:#999;"">"& Lang_NavigatorType(3) &"</span>"
				ElseIf Fn_NavType = 4 Then
					Pv_CanAddSubNav = False
					Echo "<span style=""color:#666;"">"& Lang_NavigatorType(4) &"</span>"
				End If
				Echo "</td>"
				Echo "<td id="&Fn_ID&" class=imgText>"
				If Fn_NavDepth > 1 Then
					For Fn_i=2 to Fn_NavDepth
						If Fn_i=2 Then
							Echo "&nbsp;&nbsp;&nbsp;&nbsp;"
						Else
							Echo "<img src=""image/tree_l.gif"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
						End If
					Next
					If Fn_LastID=Fn_ID Then
						Echo "<img src=""image/tree_b.gif"">"
					else
						Echo "<img src=""image/tree_m.gif"">"
					end if
				End If
				Echo "<input type=""text"" class=""TxtClass"" name=""Order"" style=""width:20px;"" value="""&Fn_OrderNum&""" /> "
				Echo "<input type=""text"" class=""TxtClass"" name=""NavName"" style=""width:120px;"" "&KnifeCMS.IIF(Fn_Disabled,"style=""color:#888;""","")&" value="""&Fn_NavName&"""/></td>"
				Echo"<td>"&KnifeCMS.IIF(Fn_Disabled,"<img class=""pointer"" ctl="""& Ctl &""" id="""& Fn_ID &""" value="""& Fn_Disabled &""" onclick=""Admin.Navigator.SetIsShow(this)"" src=""image/icon_yes_gray.png"">","<img class=""pointer"" ctl="""& Ctl &""" id="""& Fn_ID &""" value="""& Fn_Disabled &""" onclick=""Admin.Navigator.SetIsShow(this)"" src=""image/icon_yes.png"">")&"</td>"
				Echo"<td>"
				If Pv_AllowDepth >= Fn_NavDepth+1 And Pv_CanAddSubNav Then
					Echo"<span class=""sysBtn""><a href=""?ctl="& Ctl &"&act=add&p_navpath="&Fn_NavPath&""" target=""_self""><b class=""icon icon_write""></b>"& Lang_DoAdd & Lang_Navigator(1) &"</a></span>"
				End If
				Echo"</td><td>"
				Echo"<span class=""sysBtn""><a href=""?ctl="& Ctl &"&act=edit&id="&Fn_ID&""" target=""_self""><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
				Echo"</td><td>"
				If Fn_NavDefault = 0 Then Echo "<span class=""sysBtn""><a href=""?ctl="& Ctl &"&act=del&id="&Fn_ID&""" onclick=""return(confirm('"&  Lang_DoDelConfirm &"'))"" target=""_self""><b class=""icon icon_del""></b>"& Lang_DoDel &"</a></span>"
				Echo"</td><td>"
				Echo "<span class=""sysBtn""><a href="""& Fn_NavUrl &""" target=""_blank""><b class=""icon icon_view""></b>"& Lang_DoView &"</a></span>"
				Echo"</td>"
				Echo"</tr>"
				if Fn_ChildNum > 0 then NavigatorList BV_DB_Table,Fn_ID,""
				Fn_Rs.movenext
			loop'------------------------------------------------------------------------
		end if
		KnifeCMS.DB.CloseRs Fn_Rs
	End Function
	
	Private Function NavigatorDoSave()
		Dim Fn_Rs,Fn_Arr
		Dim Fn_MaxID : Fn_MaxID = 0
		Dim Fn_Check : Fn_Check = true
		'获取数据并处理数据
		NavType     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","NavType"))
		NavName     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","NavName")),100)
		NavUrl      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","NavUrl")),250)
		P_NavPath   = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","P_NavPath"),"[^0-9,]",""),50)
		P_NavPath   = KnifeCMS.IIF(Trim(P_NavPath)<>"" And Trim(P_NavPath)<>"0",P_NavPath,",")
		OrderNum    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","OrderNum"))
		Target      = KnifeCMS.GetForm("post","Target")
		Target      = KnifeCMS.IIF(Target<>"_blank","_self","_blank")
		Disabled    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Disabled"))
		Disabled    = KnifeCMS.IIF(Disabled=0,0,1)
		MetaKeywords    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","MetaKeywords")),250)
		MetaDescription = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","MetaDescription")),250)
		
		'数据合法性检测并处理数据
		If KnifeCMS.Data.IsNul(NavName) Then ErrMsg = ErrMsg & Lang_Navigator_Cue(8) &"<br>"
		If KnifeCMS.Data.IsNul(NavUrl)  Then ErrMsg = ErrMsg & Lang_Navigator_Cue(9) &"<br>"
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		IF Action="add" Then
		
			Fn_Arr    = Split(P_NavPath,",")
			NavDepth  = KnifeCMS.Data.CLng(Ubound(Fn_Arr))
			P_NavID   = KnifeCMS.Data.CLng(Fn_Arr(NavDepth-1))
			
			'获取最大的ID
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT Max(ID) FROM ["& DBTable_Navigator &"]")
			If Not(Fn_Rs.Eof) Then Fn_MaxID = Fn_Rs(0)
			KnifeCMS.DB.CloseRs Fn_Rs
			Fn_MaxID = KnifeCMS.Data.CLng(Fn_MaxID)+1
			
			'检查父级地区是否存在,不存在则提示错误.
			If P_NavPath<>"," Then
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Navigator&":NavType",Array("NavPath:"&P_NavPath),"")
				If Fn_Rs.Eof Then
					Fn_Check = False
				Else
					NavType=Fn_Rs("NavType")
				End If
				KnifeCMS.DB.CloseRs Fn_Rs
				If Not(Fn_Check) Then Echo Lang_Navigator_Cue(10) &"[Region_Path="""&Fn_P_Region_Path&"""]" : Exit Function
			End If
			
			If Not(NavType>0)              Then ErrMsg = ErrMsg & Lang_Navigator_Cue(11) &"<br>"
			If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
			
			P_NavPath = P_NavPath & Fn_MaxID &","
			
			Result = KnifeCMS.DB.AddRecord(DBTable_Navigator,Array("ID:"& Fn_MaxID,"NavDefault:0","NavType:"& NavType,"NavName:"& NavName,"NavUrl:"& NavUrl,"P_NavID:"& P_NavID,"NavPath:"& P_NavPath,"NavDepth:"& NavDepth,"ChildNum:0","OrderNum:"& OrderNum,"Target:"& Target,"Disabled:"& Disabled,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription))
			Call KnifeCMS.DB.UpdateRecord(DBTable_Navigator,"ChildNum=ChildNum+1",Array("ID:"&P_NavID))
			Msg = Lang_Navigator_Cue(0) &":"&NavName&""
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		ElseIf Action="edit" Then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Navigator,Array("NavName:"& NavName,"NavUrl:"& NavUrl,"OrderNum:"&OrderNum,"Target:"& Target,"Disabled:"&Disabled,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription),Array("ID:"&ID))
			Msg = "[ID="&ID&"]"& Lang_Navigator_Cue(1) &":"&NavName&""
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		End IF
	End Function
	
	
	'$:批量执行编辑操作
	Private Function SaveBatchEdit()
		Dim Fn_ID,Fn_ArrID,Fn_Order,Fn_ArrOrder,Fn_NavName,Fn_ArrNavName,Fn_i
		Fn_i = 1
		For Each Fn_ID In Request.Form("NavID")
			Fn_ID = KnifeCMS.Data.CLng(Fn_ID)
			Fn_ArrID = KnifeCMS.IIF( Fn_i=1 , Fn_ID , Fn_ArrID &","& Fn_ID)
			Fn_i = Fn_i+1
		Next
		Fn_i = 1
		For Each Fn_Order In Request.Form("Order")
			Fn_Order    = KnifeCMS.Data.CLng(Fn_Order)
			Fn_ArrOrder = KnifeCMS.IIF( Fn_i=1 , Fn_Order , Fn_ArrOrder &","& Fn_Order)
			Fn_i = Fn_i+1
		Next
		Fn_i = 1
		For Each Fn_NavName In Request.Form("NavName")
			Fn_NavName    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(Fn_NavName),100)
			Fn_ArrNavName = KnifeCMS.IIF( Fn_i=1 , Fn_NavName , Fn_ArrNavName &","& Fn_NavName)
			Fn_i = Fn_i+1
		Next
		'Echo Fn_ArrID &"<br>"& Fn_ArrOrder &"<br>"& Fn_ArrNavName &"<br>"
		Fn_ArrID        = Split(Fn_ArrID,",")
		Fn_ArrOrder     = Split(Fn_ArrOrder,",")
		Fn_ArrNavName = Split(Fn_ArrNavName,",")
		'Echo Ubound(Fn_ArrID) &"-"& Ubound(Fn_ArrOrder) &"-"& Ubound(Fn_ArrNavName)
		If Ubound(Fn_ArrID) <> Ubound(Fn_ArrOrder) Or Ubound(Fn_ArrOrder) <> Ubound(Fn_ArrNavName) Then Admin.ShowMessage Lang_ClassCue(16),"?ctl="& Ctl &"&act=view",0 : Exit Function
		For Fn_i=0 To Ubound(Fn_ArrID)
			Fn_ID        = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			Fn_Order     = KnifeCMS.Data.CLng(Fn_ArrOrder(Fn_i))
			Fn_NavName = CStr(KnifeCMS.Data.Left(Fn_ArrNavName(Fn_i),100))
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Navigator,Array("NavName:"&Fn_NavName,"OrderNum:"&Fn_Order),Array("ID:"&Fn_ID))
			Msg    = Msg & Lang_Navigator_Cue(2) &"{"& Fn_NavName &"[ID="& Fn_ID  &"]}<br>"
		Next
		Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
	End Function
	
	Private Function NavigatorDel()
		Dim Fn_Rs,Fn_Check
		Fn_Check = False
		If ID > 0 And Action = "del" Then
			Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Navigator&":NavDefault,NavName,P_NavID",Array("ID:"&ID),"")
			If Not(Fn_Rs.Eof) Then
				NavDefault = Fn_Rs(0)
				NavName    = KnifeCMS.Data.HTMLDecode(Fn_Rs(1))
				P_NavID    = Fn_Rs(2)
				Fn_Check   = True
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
			NavDefault = KnifeCMS.Data.CLng(NavDefault)
			If NavDefault = 1 Then ErrMsg = Lang_Navigator_Cue(5)
			If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
			If Fn_Check And NavDefault=0 Then
				Result = KnifeCMS.DB.DeleteRecord(DBTable_Navigator,Array("ID:"&ID))
				If P_NavID > 0 Then KnifeCMS.DB.UpdateRecord DBTable_Navigator,"ChildNum=ChildNum-1",Array("ID:"&P_NavID)
				Msg    = Msg & Lang_Navigator_Cue(3) &"{"& NavName &"[ID="& ID &"]}<br>"
			End If
		End If
		If Not(KnifeCMS.Data.IsNul(Msg)) Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
	End Function
	
	Private Function NavigatorOptionsList(ByVal BV_P_NavPath, ByVal BV_NavPath)
		Dim Fn_Rs,Fn_i,Fn_Arr
		Dim Fn_NavPath,Fn_NavDepth,Fn_P_NavID
		Fn_Arr       = Split(BV_P_NavPath,",")
		Fn_NavDepth  = KnifeCMS.Data.CLng(Ubound(Fn_Arr))
		Fn_P_NavID   = KnifeCMS.Data.CLng(Fn_Arr(Fn_NavDepth-1))	
		Pv_TempHTML = ""
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Navigator&":ID,NavType,NavPath,NavName,NavDepth,ChildNum",Array("P_NavID:"&Fn_P_NavID),"")
		Do While Not(Fn_Rs.Eof)
			Fn_NavPath = Fn_Rs("NavPath")
			Echo "<option navtype="""& Fn_Rs("NavType") &""""
			if Len(BV_NavPath) > 1 and BV_NavPath = Fn_NavPath then Echo "selected=""selected"""
			Echo " value="""& Fn_NavPath &""" >"
			for Fn_i=2 to Fn_Rs("NavDepth")
				Echo "&nbsp;&nbsp;&nbsp;&nbsp;"
			next
			Echo "└"&KnifeCMS.Data.HTMLDecode(Trim(Fn_Rs("NavName"))) &"</option>"
			if Fn_Rs("ChildNum")>0 then NavigatorOptionsList Fn_NavPath,BV_NavPath
			Fn_Rs.movenext
		Loop
	End Function
	
	Private Function NavTypeOptions(ByVal BV_NavType)
		Dim Fn_Array : Fn_Array = Array("1","2","3","4")
		Dim Fn_Array_Lang : Fn_Array_Lang = Array(Lang_NavigatorType(1),Lang_NavigatorType(2),Lang_NavigatorType(3),Lang_NavigatorType(4))
		Dim Fn_i
		Pv_TempHTML = ""
		For Fn_i=0 To Ubound(Fn_Array)
			Pv_TempHTML = Pv_TempHTML & "<option "
			If Int(Fn_Array(Fn_i)) = Int(BV_NavType) Then Pv_TempHTML = Pv_TempHTML & "selected=""selected"""
			Pv_TempHTML = Pv_TempHTML & " value="&Fn_Array(Fn_i)&">"&Fn_Array_Lang(Fn_i)&"</option>"
		Next
		NavTypeOptions = Pv_TempHTML
	End Function
	
	
	Private Function NavigatorView()
%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="downcell">
                  <span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=add"><b class="icon icon_add"></b><% Echo Lang_DoAdd&Lang_Navigator(0) %></a></span>
				  </td></tr>
			  </table>
		  </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="SaveForm" id="SaveForm" action="?ctl=<% Echo Ctl %>&act=batchedit&subact=save" method="post">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
            <th width="80px"><div style="width:80px;"><%=Lang_Navigator_ListTableColLine(1)%><!--位置--></div></th>
			<th width="30%"><div style="width:350px;"><%=Lang_Navigator_ListTableColLine(2)%><!--排序|站点栏目名称--></div></th>
			<th width="60px"><div style="width:60px;"><%=Lang_Navigator_ListTableColLine(3)%><!--显示--></div></th>
            <th width="110px"><div style="width:110px;"><%=Lang_Navigator_ListTableColLine(4)%><!--添加--></div></th>
            <th width="60px"><div style="width:60px;"><%=Lang_Navigator_ListTableColLine(5)%><!--编辑--></div></th>
            <th width="60px"><div style="width:60px;"><%=Lang_Navigator_ListTableColLine(6)%><!--删除--></div></th>
            <th><div style="width:80px;"><%=Lang_Navigator_ListTableColLine(7)%><!--预览--></div></th>
			</tr>
			<% NavigatorList DBTable_Navigator,0,Array(Pv_AllowDepth) %>
		   </tbody>
		  </table>
		  <div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
			  <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Navigator.BatchEdit()"><b class="icon icon_save"></b><% Echo Lang_Btn_BatchEdit %></button>
			  <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
		  </div>
		  </form>
        </div>
		</DIV>
	</DIV>
<%
	End Function
	Private Function NavigatorDo()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
            <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable mb5">
              <tbody>
               <tr><td class="titletd"><span class="required">*</span><%=Lang_Navigator(3)%><!--位置-->:</td>
                   <td class="infotd">
                   <select id="NavType" name="NavType" <% If Action = "edit" Then Echo "disabled" %>>
                   <option value="-1"><%=Lang_PleaseSelect%><!--请选择...--></option>
                   <% Echo NavTypeOptions(NavType) %>
                   </select>
                   <input type="hidden" name="NavType" value="<% Echo NavType %>" />
                   <label id="d_NavType"></label>
                   </td>
               </tr>
               </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
              <tbody>
               <tr><td class="titletd"><span class="required">*</span><%=Lang_Navigator(4)%><!--上级导航-->:</td>
                   <td class="infotd">
                   <select id="P_NavPath" name="P_NavPath" onchange="Admin.Navigator.ParNavChange()" <% If Action = "edit" Then Echo "disabled" %>>
                   <option navtype="0" value=","><%=Lang_TopNavigator%><!--顶级导航--></option>
                   <% NavigatorOptionsList ",",P_NavPath %>
                   </select>
                   <label id="d_P_NavPath"></label>
                   </td>
               </tr>     
               <tr><td class="titletd"><span class="required">*</span><%=Lang_Navigator(2)%><!--导航名称-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="NavName" name="NavName" value="<% Echo NavName %>" />
                   <label id="d_NavName"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><span class="required">*</span><%=Lang_Navigator(5)%><!--导航内容URL-->:</td>
                   <td class="infotd">
                     <div class="inbox" id="CreateNavUrl" style="width:550px;">
                           <ul id="ContentTabs" class="TabsStyle" >
                                <li class="current" onclick="TabSwitch('ContentTabs','li','Tabs',1)"><%=Lang_Sys_Content%><!--内容系统--></li>
                                <li onclick="TabSwitch('ContentTabs','li','Tabs',2)" <% If CloseModuleShop Then Echo "style=""display:none;""" %>><%=Lang_Sys_Goods%><!--商品系统--></li>
                           </ul>
                           <span id="Tabs_1">
                               <select id="ContentSubSystem" name="ContentSubSystem" onchange="Admin.Content.GetSubSystemClassList('DefaultClassID');WriteUrl_Init();Admin.Navigator.ChangeUrl(this);" style="font-size:13px;">
							   <% Admin.GetContentSubSystemOption 0,"" %>
                               </select>
                               <span id="DefaultClassID"></span>
                           </span>                           
                           <span id="Tabs_2" style="display:none;">
                               <select id="GoodsClassID" name="GoodsClassID" onchange="Admin.Navigator.ChangeUrl(this);"  style="font-size:13px;">
                               <% Admin.ShowClass 1,0,DBTable_GoodsClass %>
                               </select>
                           </span>
                           <span class="ml3"><label class="Normal" id="d_Disabled"><%=Lang_Navigator(10)%><!--选择内容可自动生成下面的导航URL.--></label></span>
                     </div>
                     <input type="text" class="TxtClass" id="NavUrl" name="NavUrl" style="width:556px;" maxlength="250" value="<% Echo NavUrl %>" />
                     <label id="d_NavUrl"></label>
                     <div class="mt5"><label class="Normal" ><%=Lang_Navigator(11)%><!--站外地址必须以 " http:// 或 https:// " 开头.--></label></div>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_Navigator(6)%><!--是否隐藏-->:</td>
                   <td class="infotd">
                   <input type="checkbox" id="Disabled" name="Disabled" value="1" <% Echo Admin.Input_Checked(Disabled,"1") %> /><%=Lang_Hide%><!--隐藏-->
                   <label class="Normal" id="d_Disabled"><%=Lang_Navigator(12)%><!--隐藏即不显示该导航.--></label>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_Navigator(7)%><!--是否新窗口打开-->:</td>
                   <td class="infotd">
                   <input type="checkbox" id="Target" name="Target" value="_blank" <% Echo Admin.Input_Checked(Target,"_blank") %> /><%=Lang_Navigator(14)%><!--新窗口-->
                   <label class="Normal" id="d_Target"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_Navigator(8)%><!--排序-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="OrderNum" name="OrderNum" style="width:60px;" value="<% Echo OrderNum %>" />
                   <label class="Normal"><%=Lang_Navigator(13)%><!--数字越小越靠前.--></label>
                   </td>
               </tr>
               </tbody>
            </table>
            </div>
            <div class="inbox" style="display:none;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable mt5">
               <tr><td class="titletd" valign="top"><%=Lang_MetaKeywords%></td><td class="infotd">
                   <input type="text" class="TxtClass" name="MetaKeywords" id="MetaKeywords" maxlength="250" value="<%=KnifeCMS.Data.ReplaceString(MetaKeywords,"&nbsp;",Chr(32))%>" onblur="KnifeCMS.onblur(this,'maxLength:100')" style="width:360px;" /> <label class="Normal"></label>
                   </td>
               </tr>
               <tr><td  class="titletd" valign="top"><%=Lang_MetaDescription%></td><td class="infotd">
                   <textarea class="remark" name="MetaDescription" id="MetaDescription" maxlength="250" onblur="KnifeCMS.onblur(this,'maxLength:250')" ><%=KnifeCMS.Data.ReplaceString(MetaDescription,"&nbsp;",Chr(32))%></textarea>
                   <label class="Normal"></label>
                   </td>
               </tr>
            </table>
            </div>
            <input type="hidden" name="id" value="<%=ID%>" />
            <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
            <div class="bottomSaveline" id="SubmitButtoms">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Navigator.SaveNavigatorCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
              <button type="button" class="sysSaveBtn" id="ReSetBtn" onClick="if( confirm('<% Echo Lang_DoConfirmLeave %>') ){history.go(-1)}"><b class="icon icon_goback"></b><% Echo Lang_Goback %></button>
            </div>
            </form>
        </div>
      </DIV>
    </DIV>
	<script type="text/javascript">
	var Action     = "<%=Action %>";
	var SiteURL    = "<%=SiteURL %>";
	var SystemPath = "<%=SystemPath %>";
	var FileSuffix = "<%=FileSuffix %>";
	var URL_Pre    = "";
	function WriteUrl_Init(){
		var ContentClass=document.getElementById("ClassID");
		if('<%=SiteFilePathType%>'=='absolute'){URL_Pre=SiteURL}
		if(objExist(ContentClass)){
			if(isIE()){ContentClass.onchange=function(){Admin.Navigator.ChangeUrl(this)}}else{ContentClass.setAttribute("onchange","Admin.Navigator.ChangeUrl(this)");}
		}
		
	}
	Admin.Navigator.ParNavChange(Action);
    </script>
<%
	End Function
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
End Class
%>