<!--#include file="config.admin.asp"-->
<%
Function FileView()
%>
<style type="text/css">
body{ padding-top:0px;}
</style>
<DIV id="InsertFile">
    <div class="imageBox" style="padding:5px; margin-bottom:6px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr><td class="tdleft">
            <div class="Tabsh" id="ImgUpload">
                <ul id="NewsTabs" class="TabsStyle2"><li class="current"><a href="javascript:void(0)" onclick="TabSwitch('NewsTabs','li','ImgTab',1)"><% Echo Lang_AjaxUp(1) %></a></li><li><a href="javascript:void(0)" onclick="TabSwitch('NewsTabs','li','ImgTab',2)"><% Echo Lang_AjaxUp(2) %></a></li><li><a href="javascript:void(0)" onclick="TabSwitch('NewsTabs','li','ImgTab',3)"><% Echo Lang_AjaxUp(3) %></a></li>
                </ul>
                <!--本地上传-->
                <div class="fixTabText" id="ImgTab_1">
                    <div style="padding:10px 10px;">
                    <%
					Dim SavePath,Fn_RealPath
                    SavePath = KnifeCMS.Data.FormatDateTime(SysTime,5) & KnifeCMS.Data.FormatDateTime(SysTime,6) &"/"& KnifeCMS.Data.FormatDateTime(SysTime,7) '年月日文件夹
                    SavePath = "attachment/"& SubCtl &"/" & SavePath
                    Fn_RealPath = KnifeCMS.FSO.CreateFolder(SavePath)
                    If Fn_RealPath = False Then                            '如果创建文件夹失败则提示错误信息
                        Echo "<br>"& Lang_AjaxUp_Cue(1) & SavePath &"<br>"
                    Else
                        SavePath = SystemPath & SavePath &"/"
                        %>
                        <div style="padding:0px 0px 10px 0px;"><% Echo Temp_Cue_Type %></div><!--允许上传的文件格式-->
                        <script type="text/javascript" src="js/AjaxUpload/AnPlus.js"></script>
                        <script type="text/javascript" src="js/AjaxUpload/AjaxUploader.js"></script>
                        <!--下面是Ajax上传文件区-->
                        <div id="uploadContenter"></div>
                        <iframe name="AnUploader" style="display:none;"></iframe>
                        <div id="detialMsg" style ="padding:5px; border:1px #ddd solid; background-color:#eee; margin:5px 0px;"></div>
                        <script type="text/javascript">
                            var filesArray = new Array();
							var haddo     = false;
							var AjaxUp=new AjaxProcesser("uploadContenter");          //创建Uploader，参数uploadContenter字符串 上传控件的容器，程序自动给容易四面增加3px的padding
                            AjaxUp.target="AnUploader";                                //设置提交到的iframe名称
                            AjaxUp.url="fileupload.class.asp?ctl=<%=Ctl%>&act=add";    //上传处理页面
                            AjaxUp.savePath="<%=SavePath%>";                           //保存目录
                            AjaxUp.succeed=function(files)                            //上传成功时运行的程序
                            {
                                var info = Lang_Js_AjaxUp_Cue[1] + files.length + "<br />"
                                for(var i=0;i < files.length; i++){
									haddo = false;
									for(var jj=0;jj<filesArray.length;jj++){
										if(files[i].name == filesArray[jj]){
											haddo = true;
										}
									}
									if(!haddo){
										filesArray.push(files[i].name)
										var FileSize = files[i].size/1024;
										info+=((i+1) + Lang_Js_AjaxUp_Cue[2] + files[i].name + Lang_Js_AjaxUp_Cue[3] + FileSize + "KB<br />");
										InsertFileTo("SelectFiles",AjaxUp.savePath + files[i].name);//将已上传的文件显示在右边区#SelectFiles
									}
                                }
                                Ai("detialMsg").html(info); //类似于innerHTML方法
                            }
                            AjaxUp.faild=function(msg){alert(Lang_Js_AjaxUp_Cue[4] + unescape(msg))}//上传失败时运行的程序
                        </script>
                        <!--结束-->
                    <% End If %>
                    </div>
                </div>
                <div class="TabText" id="ImgTab_2" style="display:none;">
                    <script type="text/javascript" src="js/dtree.js"></script>
                    <div class="Folder">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
                      <td class="left">
                        <DIV class="FolderLeft">
                            <div class="dtree">
                            <p class="doline"><a href="javascript: d.openAll();"><% Echo Lang_AjaxUp(4) %></a> | <a href="javascript: d.closeAll();"><% Echo Lang_AjaxUp(5) %></a></p>
                            <% KnifeCMS.FSO.TreeFolderList "attachment/"& SubCtl &"/",SubCtl,SubCtl %>
                            </div>
                        </DIV>
                      </td><td  class="right"><DIV class="Files" id="Fileslist"></DIV></td></tr>
                      </table>
                    </div>
                </div>
                <!--网络添加-->
                <div class="fixTabText" id="ImgTab_3" style="display:none;">
                    <div style="padding:10px 10px;">
                    <form id="onNetUrls">
                    <div style="padding-left:4px;"><% Echo Lang_AjaxUp(6) %></div>
                    <table border="0" cellpadding="4" cellspacing="0">
                    <tr><td><input type="text" class="TxtClass upload" id="imageURL_1" name="imageURL" value="" /></td></tr>
                    <tr><td><% Echo Temp_Cue_Example %></td></tr><!--举例-->
                    <tr><td><input type="button" class="addbutton" value="添加" onclick="InsertNetUrls('SelectFiles')" style="margin:0px;" /></td></tr>
                    </table>
                    </form>
                    </div>
                </div>
                <!--ImgTab_3:end-->
            </div>
        </td>
        <td class="tdright">
            <div class="rtit"><% Echo Temp_Cue_SelectType %></div><!--准备插入的文件-->
            <div class="Rightbox"><table border="0" cellpadding="0" cellspacing="0" id="SelectFiles"><tbody></tbody></table></div>
        </td></tr>
        </table>
    </div>
    <div class="bottomSaveline" id="SubmitButtoms">
        <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="InsertFile()"><% Echo Lang_Btn_Confirm %></button>
        <button type="reset" class="sysSaveBtn" name="ReSetBtn" id="ReSetBtn" onClick="WinClose()"><% Echo Lang_Btn_Close %></button>
    </div>
</DIV>
<script type="text/javascript" src="js/upload.js"></script>
<script type="text/javascript">
FolderSelect();
</script>
<%
End Function

Dim Temp_Cue_Type,Temp_Cue_SelectType,Temp_Cue_Example
Admin.IncludeLanguageFile "file.lang.asp"
Header()
Select Case SubCtl
	Case "image" : Temp_Cue_Type=Lang_AjaxUp_Cue(2) : Temp_Cue_SelectType=Lang_AjaxUp(8)  : Temp_Cue_Example = Lang_AjaxUp_Cue(8) : FileView()
	Case "file"  : Temp_Cue_Type=Lang_AjaxUp_Cue(3) : Temp_Cue_SelectType=Lang_AjaxUp(9)  : Temp_Cue_Example = Lang_AjaxUp_Cue(9) : FileView()
	Case "flash" : Temp_Cue_Type=Lang_AjaxUp_Cue(4) : Temp_Cue_SelectType=Lang_AjaxUp(10) : Temp_Cue_Example = Lang_AjaxUp_Cue(10) : FileView()
	Case "music" : Temp_Cue_Type=Lang_AjaxUp_Cue(5) : Temp_Cue_SelectType=Lang_AjaxUp(11) : Temp_Cue_Example = Lang_AjaxUp_Cue(11) : FileView()
	Case "media" : Temp_Cue_Type=Lang_AjaxUp_Cue(6) : Temp_Cue_SelectType=Lang_AjaxUp(12) : Temp_Cue_Example = Lang_AjaxUp_Cue(12) : FileView()
	Case Else    : Admin.ShowMessage Lang_IllegalSysPra,"",0
End Select
Footer()
ClassObjectClose

%>