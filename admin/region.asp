<!--#include file="config.admin.asp"-->
<%
Dim Region
Set Region = New Class_KnifeCMS_Admin_Region
Set Region = Nothing
Class Class_KnifeCMS_Admin_Region

	Private RegionName
	Private DBTable_Goods
	
	Private Sub Class_Initialize()
		DBTable                     = TablePre&"Regions"
		Header()
		If Ctl = "regionset" Then
		Select Case Action
			Case "view"
				RegionView
			Case "edit"
				If SubAction="createjspage" Then CreateRegionJsFile
		End Select
		End If
		Footer()
	End Sub
	
	Private Function CreateRegionJsFile()
		Print Lang_Region(19)
		If "ok"=Admin.CreateRegionJsFile Then
			Msg    = Lang_Region(20)
			Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		End If
	End Function
	
	Private Function RegionView()
%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
        <div class="toparea">
		  <table bDelivery="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td class="tdcell">
            <span class="sysBtn"><a href="javascript:;" onclick="Admin.Region.AddTopRegion(',')"><b class="icon icon_add"></b><% Echo Lang_DoAdd & Lang_Region(1) %></a></span>
            <span class="sysBtn"><a href="region.asp?ctl=regionset&act=edit&subact=createjspage"><b class="icon icon_createjspage"></b><% Echo Lang_Region(18) %></a></span>
		  </td>
          <td class="tdcell"><span class="illustration"><% Echo "<b>"&Lang_SystemCue &"</b>:"&Lang_Region(14) %>:</span></td>
          </table>
		</div>
        <div class="wbox" id="Regions"></div>
      </DIV>
	</DIV>
    <script type="text/javascript">
    Admin.Region.GetRegions(",")
    </script>
<%
	End Function
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
End Class
%>