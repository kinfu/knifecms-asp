<!--#include file="../config/config.main.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>KnifeCMS后台登录</title>
<link href="image/login/User_Login.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="../language/admin/<%=SiteAdminLanguage%>/main.lang.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="../plugins/jquery/jquery.js"></script>
<script type="text/javascript" src="../include/javascript/kfc.js"></script>
<script type="text/javascript" src="js/login.js"></script>
</head>
<body id="userlogin_body">
<div id="browsertip"></div>
<DIV id=user_login>
<DL>
  <DD id=user_top>
  <UL>
    <LI class=user_top_l></LI>
    <LI class=user_top_c></LI>
    <LI class=user_top_r></LI></UL>
  <DD id=user_main>
  <UL>
    <LI class=user_main_l></LI>
    <LI class=user_main_c>
    <DIV class=user_main_box>
    <UL>
      <LI class=user_main_text>用户名： </LI>
      <LI class=user_main_input><INPUT style="margin-top:6px;" class=TxtUserNameCssClass id=username 
      maxLength=20 name=username> </LI></UL>
    <UL>
      <LI class=user_main_text>密　码： </LI>
      <LI class=user_main_input><INPUT style="margin-top:6px;" class=TxtPasswordCssClass id=password 
      type=password name=password> </LI></UL>
    <UL>
      <LI class=user_main_text>验证码： </LI>
      <LI class=user_main_input>
      <div style="width:65px;float:left">
      <INPUT id="checkcode_value" name="checkcode_value" type="text" maxlength="5" style="border:1px solid gray;float:left;width:40px;height:20px;"/></div><span id="checkcode_boxer" style="padding:0px 0px 0px 4px;float:left;"></span>
      </LI></UL>
	<UL>
      <LI class=user_main_text>记住用户</LI>
    	<LI class=user_main_input><input type="checkbox" name="remuser" id="remuser" checked="checked" value="1" /></LI></UL>
    </DIV></LI>
    <LI class=user_main_r><INPUT class=IbtnEnterCssClass id=IbtnEnter 
    style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px" 
    type=image src="image/login/user_botton.gif" name=IbtnEnter onclick="Logining()"> </LI></UL>
  <DD id=user_bottom>
  <UL>
    <LI class=user_bottom_l></LI>
    <LI class="user_bottom_c" style="position:relative">
			<div style="position:absolute;left:-120px;width:565px;top:48px;line-height:32px!important;text-align:center">
			<p>智富者科技 <A href="http://www.knifecms.com/" target="_blank">KnifeCMS.COM</A> 著作权登记号:2011SR102015</p>
			</div>
		</LI>
    <LI class=user_bottom_r></LI></UL></DD></DL></DIV>
    <SPAN id=ValrUserName style="DISPLAY: none; COLOR: red"></SPAN>
    <SPAN id=ValrPassword style="DISPLAY: none; COLOR: red"></SPAN>
    <SPAN id=ValrValidateCode style="DISPLAY: none; COLOR: red"></SPAN>
<DIV id=ValidationSummary1 style="COLOR: red">
	<div class="showmsg" id="showmsg"></div>
    <div class="ie6-tip" style="display:none;">
        <p>KnifeCMS团队提醒您：</p>
        <p>您现在使用的这个浏览器已经很老了，跑不动了，为了能更好地使用KnifeCMS后台，赶紧换个浏览器吧！ </p>
    </div>
</DIV>
<DIV></DIV>
<script type="text/javascript">
KnifeCMS.GetCodeImg("../");
var USERNAME = KnifeCMS.Cookie.GetCookie("rememberUser");
var USERNAME_BOX=document.getElementById("username");
if(USERNAME_BOX!=null && USERNAME!=null){USERNAME_BOX.value=USERNAME;}
/**检测键盘输入**/
function onKeyboardPress(evt){
	var KeyCode;
	evt = evt ? evt : (window.event ? window.event : null);
	var KeyCode = evt.keyCode?evt.keyCode:evt.which;
    if(KeyCode == 13){
        Logining();
     }
}
document.onkeydown=onKeyboardPress;
//browserTip();
</script>
</body>
</html>
