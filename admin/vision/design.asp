<!-- #include virtual="/Inc/conn.asp"-->
<!-- #include virtual="/Inc/md5.asp"-->
<!-- #include virtual="/Inc/function.asp"-->
<!-- #include virtual="/Inc/config.asp"-->
<!-- #include virtual="/Inc/Admin/common.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%
Dim tid:tid=clng(Request("ID"))
sql="select * from db_tpltemplate where id=" & tid
set rs = Server.CreateObject("adodb.recordset")
rs.Open sql,conn,1,1
%>
<title>模板设计 <%=rs("title")%></title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<META HTTP-EQUIV="pragma" CONTENT="no-cache"/>
<META HTTP-EQUIV="Cache-Control" CONTENT="no-store, must-revalidate"/>
<META HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT"/>
<META HTTP-EQUIV="expires" CONTENT="0"/>
<meta name="author" content="http://www.knifecms.com/"/>
<link rel="stylesheet" href="/Skin/css/Admin/screen.css" type="text/css" media="screen"/>
<link rel="stylesheet" type="text/css" href="/Skin/css/Admin/jquery.gritter.css"/>
<link rel="stylesheet" type="text/css" href="/Skin/css/Admin/jqModal.css"/>
<link rel="stylesheet" type="text/css" href="/Skin/css/Admin/start/jquery-ui-1.7.2.custom.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="/Skin/css/Admin/style<%=rs("style")%>.css"/>
<!--[if IE]>
	<link rel="stylesheet" href="/Skin/css/Admin/ie.css" type="text/css" media="screen"/>
<![endif]-->
</head>
<body class='normal'>
<div id="tool_bar" style="width:100%;border:1px solid gray;background:white">
	<b id="loading"></b><b id="menu_btn"></b>
	<!--button onclick="">更改布局</button>
	<button onclick="">更改样式</button-->
	<button onclick="$('#menu_btn').click()">添加组件</button>
	<button onclick="saveDesign()">保存模板</button>
	<!--i>
		<b id="maxAll" title="展开所有组件"></b>
		<b id="minAll" title="缩放所有组件"></b>
	</i-->
</div>
<%
sql="select * from db_tpllayout where id=" & rs("layout")
set rs1 = Server.CreateObject("adodb.recordset")
rs1.Open sql,conn,1,1
if not rs1.eof then

if rs1("inchead")=true then%>
<div id="header">
	<div id="logo"></div>
	<div id="title">网站标题</div>
	<ul id="main_nav">
		<li id="t1"><b class="hover"><%=rs("title")%></b><%=rs("title")%></li>
	</ul>
	<div id="info_bar"></div>
</div>
<%
end if
%>
<div id="content" class="container">
	<div id="module_menu" class="jqmWindow"></div>
<%
Dim col,cols,row,col_p,row_p,col_total,row_total,number
col=1:row=1:number=1:col_total=0:row_total=0
Dim col_ns,col_ps,col_w,row_ps
sql="select * from db_tpllayout where id=" & rs("layout")
set rs = Server.CreateObject("adodb.recordset")
rs.Open sql,conn,1,1
if not rs.eof then
	cols=rs("col")
	row=rs("row")
	col_p=rs("col_p")
	row_p=rs("row_p")
end if

col_ns=split(cols,":")

for i=1 to row
	col=col_ns(i-1)
	for j=1 to col
		Response.Write "<div id=""c" & number & """ class=""cc""></div>"
		number=number+1
	next
next
%>
</div>
<%if rs1("incfoot")=true then%>
<div id="footer">
<div id="footer_bar"></div>
Copyright 2012 &copy; KnifeCMS.COM
</div>
<%
end if
end if
rs1.close
%>
<!-- A series of hidden module templates carried within the page, which can be modified easily -->
<div class="module_template"><!-- A default template is required here without an ID -->
	<div class="moduleFrame">
		<div class='moduleHeader'>
			<div class='moduleTitle'></div>
			<div class='moduleActions'>
				<b title="Refresh" class="actionRefresh"></b>
				<b title="Collapse" class="actionMin"></b>
				<b title="Expand" class="actionMax"></b>
				<b title="Close" class="actionClose"></b>
			</div>
		</div>
		<div class='moduleContent'>
			<img src="img/loading.gif" alt="Loading..."/> Loading...
		</div>
	</div>
</div>
<div class="module_template" id="B"><!-- The ID 'B' will be used as the index to match all module layout definition with mt:'B' -->
	<div class="moduleFrame" style="border:6px groove red">
		<div class='moduleContent' style="background:#ffc">
			<img src="img/loading.gif" alt="Loading..."/> Loading...
		</div>
		<div class='moduleHeader'>
			<div class='moduleTitle'></div>
			<div class='moduleActions'>
				<b title="Collapse" class="actionMin"></b>
				<b title="Expand" class="actionMax"></b>
				<b title="Close" class="actionClose"></b>
			</div>
		</div>
	</div>
</div>
<div class="module_template" id="C"><!-- The ID 'C' will be used as the index to match all module layout definition with mt:'C' -->
	<div class="moduleFrame" style="border:6px double green">
		<div class='moduleContent' style="background:#cff">
			<img src="img/loading.gif" alt="Loading..."/> Loading...
		</div>
		<div class='moduleHeader'>
			<div class='moduleTitle'></div>
			<div class='moduleActions'>
				<b title="Collapse" class="actionMin"></b>
				<b title="Expand" class="actionMax"></b>
				<b title="Close" class="actionClose"></b>
			</div>
		</div>
	</div>
</div>
<%
rs.close
%>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="js/modules.asp?id=<%=tid%>"></script>
<script type="text/javascript" src="js/jquery.gritter.js"></script>
<script type="text/javascript" src="js/chain-0.2.pack.js"></script>
<script type="text/javascript" src="js/jquery.kwicks-1.5.1.pack.js"></script>
<script type="text/javascript" src="js/jquery.lavalamp.1.3.2-min.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="js/jpolite.core.js"></script>
<script type="text/javascript" src="js/jpolite.ext.asp?id=<%=tid%>"></script>
<script type="text/javascript">
jQuery.ajaxSetup ({
	cache: false
});

function saveDesign(){
	var design= $.cookie('knifecmslayout<%=tid%>');
	$.post("design.save.asp",{ID:<%=tid%>,content:design},function(data){
		alert(data);
	});
}
</script>
</body>
</html>