<!--#include file="config.admin.asp"-->
<%W
Dim templatePath
Dim reqskin
reqskin = request.QueryString("skin") 
if len(trim(reqskin))=0 then
	reqskin=Site.Skin
else
	reqskin = "/" & reqskin & "/"
end if
templatePath = "/Templates" & reqskin
templatePath = left(templatePath,len(templatePath)-1)
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>模板管理</title>
<link href="/Skin/css/Admin/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/Inc/manage/js/jquery.js"></script>
<script language="javascript">
jQuery.ajaxSetup ({
	cache: false
});

function ConfirmDel(){
	if(confirm("确定删除吗?")){
		return true;
	}else{
		return false;
	}
}
</script>
</head>
<body>
<table width="99%" border="0" align="center" cellpadding="3" cellspacing="1">
  <tr>
    <td class="EasySiteTitle">模板列表<a href="javascript:top.agent.speak('全站的模板列表！')"><img src="/agent/question_mark.gif" border="0" /></a></td>
  </tr>
  <tr>
    <td><table border="0" align="right" cellpadding="3" cellspacing="0">
        <tr>
          <td>
          	<a href="/Admin/portal/template/index.asp">模板列表</a> | 
          	<a href="/Admin/portal/template/add.asp">新建模板</a>
          </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cecfce">
        <tr>
          <td width="30%" height="25" bgcolor="#e7ebef" style="font-weight: bold">标题</td>
          <td width="50%" bgcolor="#e7ebef" style="font-weight: bold">类型</td>
          <td width="20%" bgcolor="#e7ebef" style="font-weight: bold">操作</td>
        </tr>
<%
sql="select * from db_tpltemplate order by id desc"
set rs = Server.CreateObject("adodb.recordset")
rs.Open sql,conn,1,1
if not rs.eof then
Do While Not rs.Eof
%>
        <tr bgcolor="#ffffff" onMouseOut="this.style.backgroundColor=''" onMouseOver="this.style.backgroundColor='#FAFAFA'">
          <td>
          	<img src='/Admin/images/template.gif' width='16' height='16' valign='absmiddle'>
          	<a href='/Admin/portal/design.asp?id=<%=rs("id")%>'><%=rs("title")%></a>
          </td>
          <td><%
          Select case rs("type")
          	case "index" Response.Write "首页模板"
          	case "page" Response.Write "单页模板"
          	case "list" Response.Write "列表模板"
          	case "cover" Response.Write "封面模板"
          	case "doc" Response.Write "正文模板"
          	case "guestbook" Response.Write "留言模板"
          	case "search" Response.Write "搜索模板"
          	case else Response.Write "自定义"
          End Select
          %></td>
          <td>
          	<a href="/Admin/portal/design.asp?id=<%=rs("id")%>">设计</a> | 
          	<a href="/Admin/portal/view.asp?id=<%=rs("id")%>" target="_blank">预览</a> | 
          	<a href="/Admin/portal/template/edit.asp?id=<%=rs("id")%>">编辑</a> | 
          	<a href="/Admin/portal/template/del.asp?id=<%=rs("id")%>" onClick="return ConfirmDel();">删除</a>
          </td>
        </tr>
<%
rs.MoveNext
Loop
Else
%>
	<tr><td colspan="3" bgcolor="white" align="center">暂无模板</td></tr>
<%
End If
%>
      </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<hr/>
<br>
<center>
  <span class=smallTxt><%=systemCopyright%></span><br>
</center>
</body>
</html>