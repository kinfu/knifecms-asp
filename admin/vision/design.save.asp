<!-- #include virtual="/Inc/conn.asp"-->
<!-- #include virtual="/Inc/md5.asp"-->
<!-- #include virtual="/Inc/function.asp"-->
<!-- #include virtual="/Inc/config.asp"-->
<!-- #include virtual="/Inc/Admin/common.asp"-->
<!-- #include virtual="/Inc/file.asp"-->
<%
'**************************************************************
' Software name: KnifeCMS
' Web: http://www.knifecms.com
' Copyright (C) 2007－2012 小刀出品 版权所有
'**************************************************************
if Trim(Request("ID"))="" then
	FoundErr=True
	Message=Message & "编号不能为空！"
end if

if FoundErr<>True then
	Dim cnDocID,styleID
	cnDocID=clng(Request("ID"))
	Set cmdTemp = Server.CreateObject("ADODB.Command")
	Set InsertCursor = Server.CreateObject("ADODB.Recordset")
	sql = "SELECT * FROM db_tpltemplate WHERE ID="&cnDocID
	cmdTemp.CommandText = sql
	cmdTemp.CommandType = 1
	Set cmdTemp.ActiveConnection = conn
	InsertCursor.Open cmdTemp, , 1, 3
	styleID=InsertCursor("style")
	InsertCursor("content") = trim(request("content"))
	InsertCursor.Update
	InsertCursor.close
	Set cmdTemp = nothing
	Set InsertCursor = nothing
	
	'生成样式
	tplcss=LoadFile_Stream("/Admin/portal/css/style" & styleID & ".css")
	tplcss=Replace(tplcss,"cursor:move;","")
	tplcss=Replace(tplcss,".moduleHeader:hover","")
	Call SaveFile_Stream("/Templates/default/css/style" & styleID & ".css",tplcss)
	
	'生成模板
	Dim tplcontent:tplcontent=""
	Dim tplmain:tplmain=""
	Dim tplmap:tplmap=""
	Dim tplmaps,tplkeys
	Dim moduleID:moduleID=""
	Dim tpldic,tpldicKeys,tpldicItems
	Set tpldic = CreateObject("Scripting.Dictionary")
	set rs=Server.CreateObject("ADODB.Recordset")
	rs.Open sql,conn,1,1
	if not rs.eof then
		tplcontent=LoadFile_Stream("/Templates/tpl/" & rs("type") & ".shtml")
		
		'替换样式地址
		tplcontent=Replace(tplcontent,"href=""/css/style.css""","href=""/css/style" & styleID & ".css""")
		'找到布局与内容的对应关系
		tplmap = Replace(left(rs("content"),len(rs("content"))-2),"{'t1':[","")
		tplmaps = split(tplmap,",")
		for i=0 to ubound(tplmaps)
			tplmap=replace(tplmaps(i),"'","")
			tplkeys=split(tplmap,":")
			tpldic.add tplkeys(0),tplkeys(1)
		next
		tpldicKeys = tpldic.Keys
		tpldicItems = tpldic.Items
		
		Dim col,cols,row,col_p,row_p,col_total,row_total,number,spannum
		col=1:row=1:number=1:col_total=0:row_total=0
		Dim col_ns,col_ps,col_w,row_ps
		sql="select * from db_tpllayout where id=" & rs("layout")
		set rs1 = Server.CreateObject("adodb.recordset")
		rs1.Open sql,conn,1,1
		if not rs1.eof then
			cols=rs1("col")
			row=rs1("row")
			col_p=rs1("col_p")
			row_p=rs1("row_p")
			if rs1("inchead")<>true then tplcontent=Replace(tplcontent,"<!--#include virtual=""/inc/head.htm""-->","")
			if rs1("incfoot")<>true then tplcontent=Replace(tplcontent,"<!--#include virtual=""/inc/foot.htm""-->","")
		end if
		rs1.close
		
		col_ps=split(col_p,"|")
		row_ps=split(row_p,":")
		col_ns=split(cols,":")
		
		For i=0 to ubound(row_ps)
			row_total=row_total+row_ps(i)
		Next

		Dim t_value

		for i=1 to row
			col_w=split(col_ps(i-1),":")
			col_total=0
			For k=0 to ubound(col_w)
				col_total=col_total+col_w(k)
			Next
			col=clng(col_ns(i-1))
			for j=1 to col
				spannum=clng(col_w(j-1))
				spannum=clng(24*spannum/col_total)
				tplmain = tplmain & "<div id=""c" & number & """ class=""cc span-"& spannum
				if j=col then tplmain = tplmain & " last"
				tplmain = tplmain & """>" &vbcrlf
				For m=0 to Ubound(tpldicItems)
				If tpldicItems(m)=("c" & number) Then
					moduleID=tpldicKeys(m)
				Else
					moduleID=""
				End If
				'Response.Write "取得的编号是：" & tpldicItems(m) & ",模型ID是" & moduleID
				if len(moduleID)>0 then
					moduleID = Replace(moduleID,"m","")
					sql1="select * from db_tplportlet where id=" & moduleID
					set rs1 = Server.CreateObject("adodb.recordset")
					rs1.Open sql1,conn,1,1
					if not rs1.eof then
						tplmain = tplmain & "	<div id=""m"&rs1("id")&""" class=""module"" style=""display:block"">" &vbcrlf
						tplmain = tplmain & "			<div class=""moduleFrame"">" &vbcrlf
						tplmain = tplmain & "				<div class='moduleHeader'>" &vbcrlf
						tplmain = tplmain & "					<div class='moduleTitle'>"&rs1("title")&"</div>" &vbcrlf
						tplmain = tplmain & "					<div class='moduleActions'>" &vbcrlf
						tplmain = tplmain & "						<a href=""#"">$lang.more&gt;&gt;</a>" &vbcrlf
						tplmain = tplmain & "					</div>" &vbcrlf
						tplmain = tplmain & "				</div>" &vbcrlf
						tplmain = tplmain & "				<div class='moduleContent'>" &vbcrlf
						if rs1("type")="html" then
							'html组件
							tplmain = tplmain & "					" & rs1("tvalue") & vbcrlf
						elseif rs1("type")="top" then
							'头条文章组件
							tplmain = tplmain & "					<ul>" & vbcrlf
							tplmain = tplmain & "					<!--%" & vbcrlf
							t_value=split(rs1("tvalue"),":")
							tplmain = tplmain & "					for each aDoc in Top("&t_value(0)&","&t_value(1)&","""","""")" & vbcrlf
							tplmain = tplmain & "					%-->" & vbcrlf
							tplmain = tplmain & "					<li><a href=""<!--%=aDoc.Url%-->""><!--%=left(aDoc.Title,"&t_value(2)&")%--></a></li>" & vbcrlf
							tplmain = tplmain & "					<!--%" & vbcrlf
							tplmain = tplmain & "					next" & vbcrlf
							tplmain = tplmain & "					%-->" & vbcrlf
        					tplmain = tplmain & "					</ul>" & vbcrlf
						elseif rs1("type")="pic" then
							'轮播图片组件
							t_value=split(rs1("tvalue"),":")
							tplmain = tplmain & "<script language=""javascript"">" & vbcrlf
							tplmain = tplmain & "	var focus_width="& t_value(3)& ";" & vbcrlf
							tplmain = tplmain & "	var focus_height="& t_value(4)& ";" & vbcrlf
							tplmain = tplmain & "	var text_height=18;" & vbcrlf
							tplmain = tplmain & "	var swf_height = focus_height+text_height;" & vbcrlf
							tplmain = tplmain & "	var imag=new Array();" & vbcrlf
							tplmain = tplmain & "	var link=new Array();" & vbcrlf
							tplmain = tplmain & "	var text=new Array();" & vbcrlf
							tplmain = tplmain & "	<!--%" & vbcrlf
							tplmain = tplmain & "	Dim i_"&number&":i_"&number&"=1" & vbcrlf
							tplmain = tplmain & "	for each aDoc in Top("&t_value(0)&","&t_value(1)&","""","""")" & vbcrlf
							tplmain = tplmain & "	%-->" & vbcrlf
							tplmain = tplmain & "	imag[<!--%=i_"&number&"%-->]=""<!--%=aDoc.Pic%-->"";" & vbcrlf
							tplmain = tplmain & "	link[<!--%=i_"&number&"%-->]=""<!--%=aDoc.Url%-->"";" & vbcrlf
							tplmain = tplmain & "	text[<!--%=i_"&number&"%-->]=""<!--%=left(aDoc.Title,"&t_value(2)&")%-->"";" & vbcrlf
							tplmain = tplmain & "	<!--%" & vbcrlf
							tplmain = tplmain & "	i_"&number&"=i_"&number&"+1" & vbcrlf
							tplmain = tplmain & "	next" & vbcrlf
							tplmain = tplmain & "	%-->" & vbcrlf
							tplmain = tplmain & "	var pics='', links='', texts='';" & vbcrlf
							tplmain = tplmain & "	for(var i=1; i<imag.length; i++){" & vbcrlf
							tplmain = tplmain & "		pics=pics+('|'+imag[i]);" & vbcrlf
							tplmain = tplmain & "		links=links+('|'+link[i]);" & vbcrlf
							tplmain = tplmain & "		texts=texts+('|'+text[i]);" & vbcrlf
							tplmain = tplmain & "	}" & vbcrlf
							tplmain = tplmain & "	pics=pics.substring(1);" & vbcrlf
							tplmain = tplmain & "	links=links.substring(1);" & vbcrlf
							tplmain = tplmain & "	texts=texts.substring(1);" & vbcrlf
							tplmain = tplmain & "	document.write('<object classid=""clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"" codebase=""http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"" width=""'+ focus_width +'"" height=""'+ swf_height +'"">');" & vbcrlf
							tplmain = tplmain & "	document.write('<param name=""allowScriptAccess"" value=""sameDomain""><param name=""movie"" value=""/swf/picviewer.swf""><param name=""quality"" value=""high""><param name=""bgcolor"" value=""#DADADA"">');" & vbcrlf
							tplmain = tplmain & "	document.write('<param name=""menu"" value=""false""><param name=""wmode"" value=""opaque"">');" & vbcrlf
							tplmain = tplmain & "	document.write('<param name=""FlashVars"" value=""pics='+pics+'&links='+links+'&texts='+texts+'&borderwidth='+focus_width+'&borderheight='+focus_height+'&textheight='+text_height+'"">');" & vbcrlf
							tplmain = tplmain & "	document.write('<embed src=""/swf/picviewer.swf"" wmode=""opaque"" FlashVars=""pics='+pics+'&links='+links+'&texts='+texts+'&borderwidth='+focus_width+'&borderheight='+focus_height+'&textheight='+text_height+'"" menu=""false"" bgcolor=""#DADADA"" quality=""high"" width=""'+ focus_width +'"" height=""'+ swf_height +'"" allowScriptAccess=""sameDomain"" type=""application/x-shockwave-flash"" pluginspage=""http://www.macromedia.com/go/getflashplayer"" />');" & vbcrlf
							tplmain = tplmain & "	document.write('</object>');" & vbcrlf
        					tplmain = tplmain & "	</script>" & vbcrlf
        				elseif rs1("type")="list" then
        					'文章列表组件
							tplmain = tplmain & "					<ul>" & vbcrlf
							tplmain = tplmain & "					<!--%" & vbcrlf
							t_value=split(rs1("tvalue"),":")
							tplmain = tplmain & "					for each aDoc in List(pageID,"&t_value(0)&","""","""")" & vbcrlf
							tplmain = tplmain & "					%-->" & vbcrlf
							tplmain = tplmain & "					<li><a href=""<!--%=aDoc.Url%-->""><!--%=left(aDoc.Title,"&t_value(1)&")%--></a>"
							if t_value(2)=1 then tplmain = tplmain & "<!--%=aDoc.PubDate%-->"
							tplmain = tplmain & "</li>" & vbcrlf
							tplmain = tplmain & "					<!--%" & vbcrlf
							tplmain = tplmain & "					next" & vbcrlf
							tplmain = tplmain & "					%-->" & vbcrlf
        					tplmain = tplmain & "					</ul>" & vbcrlf
        				elseif rs1("type")="page" then
        					'频道摘要组件
        					t_value=split(rs1("tvalue"),":")
							Dim p_title,p_pic,p_content
							p_title=false:p_pic=false:p_content=false
							for k=0 to ubound(t_value)
								select case t_value(k)
									case "title" p_title=true
									case "pic" p_pic=true
									case "content" p_content=true
								end select
							next
							if p_title = true then tplmain = tplmain & "					<h3><!--%=Channel(pageID,""pageName"")%--></h3>"
							if p_pic = true then tplmain = tplmain & "					<p><!--%Call SelPlay(Channel(pageID,""pic""),"""","""")%--></p>"
							if p_content = true then tplmain = tplmain & "					<div><!--%=Channel(pageID,""intro"")%--></div>"
        				elseif rs1("type")="doc" then
        					'文章正文组件,有待完善
        					t_value=split(rs1("tvalue"),":")
							Dim d_title,d_author,d_date,d_pic,d_self,d_content
							d_title=false:d_author=false:d_date=false:d_pic=false:d_self=false:d_content=false
							for k=0 to ubound(t_value)
								select case t_value(k)
									case "title" d_title=true
									case "author" d_author=true
									case "date" d_date=true
									case "pic" d_pic=true
									case "self" d_self=true
									case "content" d_content=true
								end select
							next
							if d_title = true then tplmain = tplmain & "					<h3><!--%=Doc(docID,""title"")%--></h3>"
							if d_author = true then tplmain = tplmain & "					<span><!--%=Doc(docID,""author"")%--></span>"
							if d_date = true then tplmain = tplmain & "					<span><!--%=Doc(docID,""date"")%--></span>"
							if d_pic = true then tplmain = tplmain & "					<p><!--%Call SelPlay(Doc(docID,""pic""),"""","""")%--></p>"
							if d_self = true then tplmain = tplmain & "					<p><!--%=Doc(docID,""self"")%--></p>"
							if d_content = true then tplmain = tplmain & "					<div><!--%=Content(docID)%--></div>"
						elseif rs1("type")="form" then
							tplmain = tplmain & "					<iframe src=""http://"&Request.ServerVariables("HTTP_HOST")&"/form/index.asp?id=" & rs1("tvalue") & """ width=""100%"" height=""100%"" frameborder=""0""></iframe>"
						elseif rs1("type")="vote" then
							tplmain = tplmain & "					<iframe src=""http://"&Request.ServerVariables("HTTP_HOST")&"/vote/index.asp?id=" & rs1("tvalue") & """ width=""100%"" height=""100%"" frameborder=""0""></iframe>"
						elseif rs1("type")="gbook" then
							tplmain = tplmain & "					<iframe src=""http://"&Request.ServerVariables("HTTP_HOST")&"/guestbook/index.asp"" width=""100%"" height=""100%"" frameborder=""0""></iframe>"
						end if
						tplmain = tplmain & "				</div>" &vbcrlf
						tplmain = tplmain & "			</div>" &vbcrlf
						tplmain = tplmain & "		</div>" &vbcrlf
					end if
					rs1.close
				end if
				Next
				tplmain = tplmain & "</div>" &vbcrlf
				number=number+1
			next
		next
		if len(tplmain)>0 then tplcontent=Replace(tplcontent,"<!--#include virtual=""/inc/main.htm""-->",tplmain)
		
		Call SaveFile_Stream("/Templates/default/" & rs("type") & ".shtml",tplcontent)
	end if
	rs.close
	
	conn.close
	Set conn = nothing
	
	'成功提示
	Response.Write "操作成功！"
end if
if FoundErr=True then
	Response.Write Message
end if
%>