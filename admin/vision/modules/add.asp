<!-- #include virtual="/Inc/conn.asp"-->
<!-- #include virtual="/Inc/md5.asp"-->
<!-- #include virtual="/Inc/function.asp"-->
<!-- #include virtual="/Inc/config.asp"-->
<!-- #include virtual="/Inc/Admin/common.asp"-->
<!-- #include virtual="/fckeditor/fckeditor.asp" -->
<%
'**************************************************************
' Software name: KnifeCMS
' Web: http://www.knifecms.com
' Copyright (C) 2007－2012 小刀出品 版权所有
'**************************************************************
If FoundErr<>True then
%>
<html>
<head>
<title>添加组件</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/Skin/css/Admin/style.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="/Skin/css/Admin/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" />
<script type="text/javascript" src="/Skin/js/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/Skin/js/jquery/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript" src="/Admin/finder/fpick.js"></script>
<script type="text/javascript">
jQuery.ajaxSetup ({
	cache: false
});

var ptype="html";

function showContent(t){
	ptype=t;
	$('.c').hide();
	$('#c_'+t).show();
}

function topPreview(){
	$('#t_preview').load("t_preview.asp",{cid:$('#t_cid').val(),num:$('#t_num').val(),words:$('#t_words').val()},function(data){
	});
}

function picPreview(){
	$.getScript("p_preview.asp?cid="+$('#p_cid').val()+"&num="+$('#p_num').val()+"&words="+$('#p_words').val()+"&width="+$('#p_width').val()+"&height="+$('#p_height').val());
	
	//$('#p_preview').load("p_preview.asp",{cid:$('#p_cid').val(),num:$('#p_num').val(),words:$('#p_words').val(),width:$('#p_width').val(),height:$('#p_height').val()},function(data){});
}

function formPreview(){
	$('#f_preview').load("f_preview.asp",{fid:$('#f_id').val()},function(data){
	});
}

function votePreview(){
	$('#v_preview').load("v_preview.asp",{vid:$('#v_id').val()},function(data){
	});
}

function gbookPreview(){
	$('#g_preview').load("g_preview.asp");
}

function prepareData(){
	if(ptype=="html"){
		var oEditor = FCKeditorAPI.GetInstance("html_value");
		$('#tvalue').val(oEditor.GetXHTML(true));
	}else if(ptype=="top"){
		$('#tvalue').val($('#t_cid').val()+":"+$('#t_num').val()+":"+$('#t_words').val());
	}else if(ptype=="pic"){
		$('#tvalue').val($('#p_cid').val()+":"+$('#p_num').val()+":"+$('#p_words').val()+":"+$('#p_width').val()+":"+$('#p_height').val());
	}else if(ptype=="list"){
		$('#tvalue').val($('#l_nums').val()+":"+$('#l_words').val()+":"+$("input:radio[name='l_date']:checked").val());
	}else if(ptype=="page"){
		var ret="";
		$("input:checkbox[name='p_value']:checked").each(function(){
			ret+=this.value + ':';
		});
		$('#tvalue').val(ret);
	}else if(ptype=="doc"){
		var ret="";
		$("input:checkbox[name='d_value']:checked").each(function(){
			ret+=this.value + ':';
		});
		$('#tvalue').val(ret);
	}else if(ptype=="form"){
		$('#tvalue').val($('#f_id').val());
	}else if(ptype=="vote"){
		$('#tvalue').val($('#v_id').val());
	}else if(ptype=="gbook"){
		$('#tvalue').val("");
	}
}

function showObject(obj){
	$('#d_'+obj).attr("checked")==true ? $('#dp_'+obj).show("slow") : $('#dp_'+obj).hide("slow");
}

function showPObject(obj){
	$('#p_'+obj).attr("checked")==true ? $('#pp_'+obj).show("slow") : $('#pp_'+obj).hide("slow");
}
</script>
</head>
<body>
<div style="margin:8px">
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
<form name="thisform" method="post" action="addSave.asp" onsubmit="return prepareData()">
<input id="tvalue" type="hidden" name="tvalue" />
<tr>
	<td><span class="EasySiteTitle">添加组件</span></td>
</tr>
<tr>
	<td>
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cecfce">
<tr>
	<td class="EasySiteText" bgcolor="#e7ebef" width="20%">组件名称<a href="javascript:top.agent.speak('组件名称，不超过150个汉字！')"><img src="/agent/question_mark.gif" border="0" /></a></td>
	<td bgcolor="#FFFFFF"><input name="title" id="title" value="" /></td>
</tr>
<tr>
	<td class="EasySiteText" bgcolor="#e7ebef">组件类型<a href="javascript:top.agent.speak('组件类型')"><img src="/agent/question_mark.gif" border="0" /></a></td>
	<td bgcolor="#FFFFFF">
		<input id="t_html" type="radio" name="type" value="html" onclick="showContent('html')" checked /><label for="t_html">Html内容</label>
		<input id="t_top" type="radio" name="type" value="top" onclick="showContent('top')" /><label for="t_top">文章头条</label>
		<input id="t_pic" type="radio" name="type" value="pic" onclick="showContent('pic')" /><label for="t_pic">图片轮播</label>
		<input id="t_list" type="radio" name="type" value="list" onclick="showContent('list')" /><label for="t_list">文章列表</label>
		<input id="t_page" type="radio" name="type" value="page" onclick="showContent('page')" /><label for="t_page">频道摘要</label>
		<input id="t_doc" type="radio" name="type" value="doc" onclick="showContent('doc')" /><label for="t_doc">文章正文</label>
		<input id="t_form" type="radio" name="type" value="form" onclick="showContent('form')" /><label for="t_form">前台表单</label>
		<input id="t_vote" type="radio" name="type" value="vote" onclick="showContent('vote')" /><label for="t_vote">投票调查</label>
		<input id="t_gbook" type="radio" name="type" value="gbook" onclick="showContent('gbook')" /><label for="t_gbook">用户反馈</label>
	</td>
</tr>
<tr>
	<td class="EasySiteText" bgcolor="#e7ebef" height="240">组件内容<a href="javascript:top.agent.speak('组件内容')"><img src="/agent/question_mark.gif" border="0" /></a></td>
	<td bgcolor="#FFFFFF" valign="top">
		<div id="c_html" class="c" style="display:block">
			<%
			Dim oFCKeditor
			Set oFCKeditor = New FCKeditor
			oFCKeditor.BasePath = "/fckeditor/" 
			oFCKeditor.ToolbarSet = "Default" 
			oFCKeditor.Width = "100%" 
			oFCKeditor.Height = "235" 
			oFCKeditor.Value = ""
			oFCKeditor.Create "html_value"
			%>
		</div>
		<div id="c_top" class="c" style="display:none">
			<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#f1f1f1">
				<tr>
					<td height="20">选择文章所属频道：<input id="t_cid" type="text" name="t_cid" /></td>
					<td>调用的文章数量：<input id="t_num" type="text" name="t_num" /></td>
					<td>文章标题截取字：<input id="t_words" type="text" name="t_words" /></td>
					<td>
						<input type="button" name="t_preview" value="预览" class="EasySiteButton" onclick="topPreview()" />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="t_preview" style="margin:8px;padding:8px;background:white">
						暂无预览
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div id="c_pic" class="c" style="display:none">
			<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#f1f1f1">
				<tr>
					<td height="20">所属频道：<input id="p_cid" type="text" name="p_cid" /></td>
					<td>文章数：<input id="p_num" type="text" name="p_num" size="3" /></td>
					<td>标题截取字：<input id="p_words" type="text" name="p_words" size="3" value="20" /></td>
					<td>图片尺寸：长<input id="p_width" name="p_width" type="text" size="3" value="200" /> 宽<input id="p_height" name="p_height" type="text" size="3" value="150" /></td>
					<td>
						<input type="button" name="p_preview" value="预览" class="EasySiteButton" onclick="picPreview()" />
					</td>
				</tr>
				<tr>
					<td colspan="5">
						<div id="p_preview" style="margin:8px;padding:8px;background:white">
							暂无预览
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div id="c_list" class="c" style="display:none">
			<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#f1f1f1">
				<tr>
					<td height="20">
						每页条数：<input id="l_nums" type="text" name="l_nums" />
						文章标题截取字：<input id="l_words" type="text" name="l_words" />
					</td>
					<td>是否显示日期：<input id="l_date_y" type="radio" name="l_date" value="1" />是 <input id="l_date_n" type="radio" name="l_date" value="0" checked />否</td>
					<td>
						<input type="button" name="l_preview" value="预览" class="EasySiteButton" onclick="listPreview()" />
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div id="l_preview" style="margin:8px;padding:8px;background:white">
							<ul>
								<li><a href="#">中创动网络科技有限公司推出KnifeCMS.ASP 1.8版</a></li>
								<li><a href="#">中创动网络科技有限公司推出KnifeCMS.ASP 1.8版</a></li>
								<li><a href="#">中创动网络科技有限公司推出KnifeCMS.ASP 1.8版</a></li>
								<li><a href="#">中创动网络科技有限公司推出KnifeCMS.ASP 1.8版</a></li>
								<li><a href="#">中创动网络科技有限公司推出KnifeCMS.ASP 1.8版</a></li>
							</ul>
							<div id="page">
								<a href="#">首页</a>
								<a href="#">1</a>
								<a href="#">2</a>
								<a href="#">3</a>
								……
								<a href="#">末页</a>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div id="c_page" class="c" style="display:none">
			<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#f1f1f1">
				<tr>
					<td height="20">
						<input type="checkbox" id="p_title" name="p_value" value="title" onclick="showPObject('title')" checked/>
						<label for="p_title">标题</label>
						<input type="checkbox" id="p_pic" name="p_value" value="pic" onclick="showPObject('pic')" checked/>
						<label for="p_pic">图片/媒体</label>
						<input type="checkbox" id="p_content" name="p_value" value="content" onclick="showPObject('content')" checked/>
						<label for="p_content">正文</label>
					</td>
				</tr>
				<tr>
					<td>
						<div id="p_preview" style="margin:8px;padding:8px;background:white">
							<h3 id="pp_title">文章标题</h3>
							<p id="pp_pic">[图片/媒体]</p>
							<p id="pp_content">[文章正文]KnifeCMS.ASP是中创动网络科技有限公司独立自主研发的一款业内领先的CMS内容管理系统，KnifeCMS.ASP界面友好直观，力争做到业内最好的内容管理系统...</p>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div id="c_doc" class="c" style="display:none">
			<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#f1f1f1">
				<tr>
					<td height="20">
						<input type="checkbox" id="d_title" name="d_value" value="title" onclick="showObject('title')" checked/>
						<label for="d_title">标题</label>
						<input type="checkbox" id="d_author" name="d_value" value="author" onclick="showObject('author')" checked/>
						<label for="d_author">作者</label>
						<input type="checkbox" id="d_date" name="d_value" value="date" onclick="showObject('date')" checked/>
						<label for="d_date">发布日期</label>
						<input type="checkbox" id="d_pic" name="d_value" value="pic" onclick="showObject('pic')" checked/>
						<label for="d_pic">图片/媒体</label>
						<input type="checkbox" id="d_content" name="d_value" value="content" onclick="showObject('content')" checked/>
						<label for="d_content">正文</label>
						<input type="checkbox" id="d_self" name="d_value" value="self" onclick="showObject('self')" checked/>
						<label for="d_self">自定义字段</label>
					</td>
				</tr>
				<tr>
					<td>
						<div id="d_preview" style="margin:8px;padding:8px;background:white">
							<h3 id="dp_title">文章标题</h3>
							<span id="dp_date">发布时间：2012-09-06</span> <span id="dp_author">文章作者：Smith</span>
							<p id="dp_pic">[图片/媒体]</p>
							<p id="dp_self">[自定义字段]</p>
							<p id="dp_content">[文章正文]KnifeCMS.ASP是中创动网络科技有限公司独立自主研发的一款业内领先的CMS内容管理系统，KnifeCMS.ASP界面友好直观，力争做到业内最好的内容管理系统...</p>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div id="c_form" class="c" style="display:none">
			<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#f1f1f1">
				<tr>
					<td height="20">
						表单编号：<input type="text" id="f_id" name="f_id" />
					</td>
					<td>
						<input type="button" name="f_preview" value="预览" class="EasySiteButton" onclick="formPreview()" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div id="f_preview" style="margin:8px;padding:8px;background:white">
							暂无预览
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div id="c_vote" class="c" style="display:none">
			<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#f1f1f1">
				<tr>
					<td height="20">
						投票编号：<input type="text" id="v_id" name="v_id" />
					</td>
					<td>
						<input type="button" name="v_preview" value="预览" class="EasySiteButton" onclick="votePreview()" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div id="v_preview" style="margin:8px;padding:8px;background:white">
							暂无预览
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div id="c_gbook" class="c" style="display:none">
			<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#f1f1f1">
				<tr>
					<td height="20">
						<input type="button" name="g_preview" value="预览" class="EasySiteButton" onclick="gbookPreview()" />
					</td>
				</tr>
				<tr>
					<td>
						<div id="g_preview" style="margin:8px;padding:8px;background:white">
							暂无预览
						</div>
					</td>
				</tr>
			</table>
		</div>
	</td>
</tr>
</table>
  </td>
</tr>
<tr>
    <td bgcolor="#FFFFFF"><input type="submit" name="Submit" value="提交(S)" class="EasySiteButton" /></td>
</tr>
</form>
</table>
</div>
</body>
</html>
<%
End If

if FoundErr=True then
	call ErrMsg(Message)
end if
%>