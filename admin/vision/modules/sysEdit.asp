<!-- #include virtual="/Inc/conn.asp"-->
<!-- #include virtual="/Inc/md5.asp"-->
<!-- #include virtual="/Inc/function.asp"-->
<!-- #include virtual="/Inc/config.asp"-->
<!-- #include virtual="/Inc/Admin/common.asp"-->
<!-- #include virtual="/fckeditor/fckeditor.asp" -->
<%
'**************************************************************
' Software name: KnifeCMS
' Web: http://www.knifecms.com
' Copyright (C) 2007－2012 小刀出品 版权所有
'**************************************************************
If FoundErr<>True then
	sql="select * from db_tplportlet where id=" & clng(Request("ID"))
	set rs = Server.CreateObject("adodb.recordset")
	rs.Open sql,conn,1,1
	%>
<html>
<head>
<title>编辑组件</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/Skin/css/Admin/style.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="/Skin/css/Admin/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" />
<script type="text/javascript" src="/Skin/js/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/Skin/js/jquery/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript" src="/Admin/finder/fpick.js"></script>
<script type="text/javascript">
jQuery.ajaxSetup ({
	cache: false
});

var ptype="<%=rs("type")%>";

function prepareData(){
	var oEditor = FCKeditorAPI.GetInstance("html_value");
	$('#tvalue').val(oEditor.GetXHTML(true));
}
</script>
</head>
<body>
<div style="margin:8px">
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
<form name="thisform" method="post" action="sysSave.asp" onsubmit="return prepareData()">
<tr>
	<td><span class="EasySiteTitle">编辑组件</span></td>
</tr>
<tr>
	<td>
	<input type="hidden" name="id" value="<%=rs("id")%>" />
	<input id="tvalue" type="hidden" name="tvalue" value="<%=Server.HTMLEncode(rs("tvalue"))%>" />
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cecfce">
<tr>
	<td class="EasySiteText" bgcolor="#e7ebef" width="20%">组件名称<a href="javascript:top.agent.speak('组件名称，不超过150个汉字！')"><img src="/agent/question_mark.gif" border="0" /></a></td>
	<td bgcolor="#FFFFFF"><input name="title" type="hidden" id="title" value="<%=rs("title")%>" /><%=rs("title")%></td>
</tr>
<tr>
	<td class="EasySiteText" bgcolor="#e7ebef">组件类型<a href="javascript:top.agent.speak('组件类型')"><img src="/agent/question_mark.gif" border="0" /></a></td>
	<td bgcolor="#FFFFFF">
		<input type="hidden" name="type" value="html" />系统组件
	</td>
</tr>
<tr>
	<td class="EasySiteText" bgcolor="#e7ebef" height="200">组件内容<a href="javascript:top.agent.speak('组件内容')"><img src="/agent/question_mark.gif" border="0" /></a></td>
	<td bgcolor="#FFFFFF" valign="top">
		<div id="c_html" class="c">
			<%
			if rs("type")="html" then
			Dim oFCKeditor
			Set oFCKeditor = New FCKeditor
			oFCKeditor.BasePath = "/fckeditor/"
			oFCKeditor.ToolbarSet = "Default"
			oFCKeditor.Width = "100%"
			oFCKeditor.Height = "235"
			oFCKeditor.Value = rs("tvalue")
			oFCKeditor.Create "html_value"
			end if
			%>
		</div>
	</td>
</tr>
</table>
	<%
	rs.Close
	set rs = Nothing
	%>
  </td>
</tr>
<tr>
    <td bgcolor="#FFFFFF"><input type="submit" name="Submit" value="提交(S)" class="EasySiteButton"></td>
</tr>
</form>
</table>
</div>
</body>
</html>
<%
End If

if FoundErr=True then
	call ErrMsg(Message)
end if
%>