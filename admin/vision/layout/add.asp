<!-- #include virtual="/Inc/conn.asp"-->
<!-- #include virtual="/Inc/md5.asp"-->
<!-- #include virtual="/Inc/function.asp"-->
<!-- #include virtual="/Inc/config.asp"-->
<!-- #include virtual="/Inc/Admin/common.asp"-->
<!-- #include virtual="/fckeditor/fckeditor.asp" -->
<%
'**************************************************************
' Software name: KnifeCMS
' Web: http://www.knifecms.com
' Copyright (C) 2007－2012 小刀出品 版权所有
'**************************************************************
If FoundErr<>True then
%>
<html>
<head>
<title>添加布局</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/Skin/css/Admin/style.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="/Skin/css/Admin/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" />
<style>
.row{
	position:relative;
	height:100px;
}
#edit{
display:block;
width:20px;height:20px;position:absolute;right:-20px;bottom:40px;border:1px solid gray;cursor:pointer;text-align:center;
background:url('/include/manage/images/edit.gif') center center no-repeat white;
}
#del{
display:block;
width:20px;height:20px;position:absolute;right:-20px;bottom:20px;border:1px solid gray;cursor:pointer;text-align:center;
background:url('/include/manage/images/del.gif') center center no-repeat white;
}
#add{
display:block;
width:20px;height:20px;position:absolute;right:-20px;bottom:0px;border:1px solid gray;cursor:pointer;text-align:center;
background:url('/include/manage/images/add.gif') center center no-repeat white;
}
</style>
<script type="text/javascript" src="/Skin/js/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/Skin/js/jquery/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript" src="/Admin/finder/fpick.js"></script>
<script type="text/javascript">
Array.prototype.remove=function(n) {
	if(n<0)
		return this;
	else
		return this.splice(n,1);
}

var rowNumber=1;
var totalrow=1;
var cols = new Array();
var col_proportions = new Array();
cols[rowNumber-1]="3";
col_proportions[rowNumber-1]="1:1:1";

function showHead(){
	$('#inchead').attr("checked")==true ? $('#head').show("slow") : $('#head').hide("slow");
}

function showFoot(){
	$('#incfoot').attr("checked")==true ? $('#foot').show("slow") : $('#foot').hide("slow");
}

//应用行属性
function applyRow(num){
	var showHtml="";
	var col=parseInt($('#col').val());
	if(isNaN(col)){col=1;}
	var row_height=parseInt($('#row_height').val());
	if(isNaN(row_height)){
		row_height=100;
	}else{
		row_height=parseInt(row_height/2);
	}
	var col_proportion=$('#col_proportion').val();
	var col_p= new Array();
	col_p=col_proportion.split(":");
	var col_total=0;
	for(var i=0;i<col_p.length;i++){
		if(!isNaN(parseInt(col_p[i]))){
			col_total+=parseInt(col_p[i]);
		}
	}
	var number=1;
	for(var j=0;j<col;j++){
		showHtml+="<td align=\"center\" width=\"" + (col_p[j]/col_total)*100 + "%\" height=\""+row_height+"\"><h3>R"+ num +"C"+number+"</h3></td>";
		number++;
	}
	$('#row'+num).height(row_height);
	$('#row_'+num).htm(showHtml);
	cols[num-1]=col;
	col_proportions[num-1]=col_proportion;
}

//编辑此行
function editrow(num){
	$('.rows').css("background","white");
	rowNumber=num;
	$('#row_'+rowNumber).css("background","yellow");
	$('#rownumber').htm(rowNumber);
	$('#row_height').val($('#row'+rowNumber).css("height").replace("px","")*2);
	$('#col').val(cols[rowNumber-1]);
	$('#col_proportion').val(col_proportions[rowNumber-1]);
}

//删除此行
function delrow(num){
	if(num==1){
		alert('第一行不允许删除！');
	}else{
		if(confirm("确定要删除此行吗？")){
			if(rowNumber==num && num>1){
				rowNumber=num-1;
			}
			$('#row'+num).remove();
			//后面相应进一
			for(var i=num+1;i<col_proportions.length;i++){
				var newnum=i-1;
				$('#row'+i).htm($('#row'+i).htm().replace(new RegExp("R"+i,"gm"),"R"+newnum));
				$('#row'+i).htm($('#row'+i).htm().replace("editrow("+i+")","editrow("+newnum+")"));
				$('#row'+i).htm($('#row'+i).htm().replace("delrow("+i+")","delrow("+newnum+")"));
				$('#row'+i).attr("id","row"+newnum);
				$('#row_'+i).attr("id","row"+newnum);
			}
			cols.remove(num);
			col_proportions.remove(num);
			totalrow--;
		}
	}
}

//增加一行
function addrow(){
	var showHtml;
	var newNumber;
	newNumber=totalrow+1;
	showHtml="";
	showHtml+="<div id=\"row"+newNumber+"\" class=\"row\">";
	showHtml+="<table width=\"100%\" cellpadding=\"0\" cellspacing=\"1\" border=\"0\">";
	showHtml+="	<tr bgcolor=\"white\" id=\"row_"+newNumber+"\" class=\"rows\">";
	showHtml+="		<td align=\"center\" height=\"100\"><h3>R"+newNumber+"C1</h3></td>";
	showHtml+="		<td align=\"center\"><h3>R"+newNumber+"C2</h3></td>";
	showHtml+="		<td align=\"center\"><h3>R"+newNumber+"C3</h3></td>";
	showHtml+="	</tr>";
	showHtml+="</table>";
	showHtml+="<div id=\"edit\" title=\"编辑此行\" onclick=\"editrow("+newNumber+")\"></div>";
	showHtml+="<div id=\"del\" title=\"删除此行\" onclick=\"delrow("+newNumber+")\"></div>";
	showHtml+="<div id=\"add\" title=\"增加一行\" onclick=\"addrow()\"></div>";
	showHtml+="</div>";
	$('#row'+totalrow).after(showHtml);
	cols[newNumber-1]="3";
	col_proportions[newNumber-1]="1:1:1";
	totalrow++;
}

//准备提交的数据
function prepareData(){
	$('#p_rows').val(totalrow);
	var p_rows_p,p_cols,p_cols_p;
	p_rows_p	= '';
	p_cols		= '';
	p_cols_p	= '';
	for(var i=1;i<=totalrow;i++){
		p_rows_p	+= $('#row'+i).css("height").replace("px","")*2 + ":";
		p_cols		+= cols[i-1]+":";
		p_cols_p	+= col_proportions[i-1]+"|";
	}
	p_rows_p	= p_rows_p.substr(0,p_rows_p.length-1);
	p_cols		= p_cols.substr(0,p_cols.length-1);
	p_cols_p	= p_cols_p.substr(0,p_cols_p.length-1);
	$('#p_rows_proportion').val(p_rows_p);
	$('#p_cols').val(p_cols);
	$('#p_cols_proportion').val(p_cols_p);
}
</script>
</head>
<body>
<div style="margin:8px">
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
<form name="thisform" method="post" action="addSave.asp" onsubmit="return prepareData()">
<input type="hidden" id="p_rows" name="p_rows" value="1" />
<input type="hidden" id="p_rows_proportion" name="p_rows_proportion" value="200" />
<input type="hidden" id="p_cols" name="p_cols" value="3" />
<input type="hidden" id="p_cols_proportion" name="p_cols_proportion" value="1:1:1" />
<tr>
	<td><span class="EasySiteTitle">添加布局</span></td>
</tr>
<tr>
	<td>
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cecfce">
<tr>
	<td class="EasySiteText" bgcolor="#e7ebef" width="20%">布局名称<a href="javascript:top.agent.speak('布局名称，不超过150个汉字！')"><img src="/agent/question_mark.gif" border="0" /></a></td>
	<td bgcolor="#FFFFFF" width="30%"><input name="title" id="title" value="" /></td>
	<td class="EasySiteText" bgcolor="#e7ebef" width="20%">公共设置<a href="javascript:top.agent.speak('是否包含页面头部、尾部等信息')"><img src="/agent/question_mark.gif" border="0" /></a></td>
	<td bgcolor="#FFFFFF">
		<input id="inchead" type="checkbox" name="inchead" value="1" checked onclick="showHead()" /><label for="inchead">包含页头</label>
		<input id="incfoot" type="checkbox" name="incfoot" value="1" checked onclick="showFoot()" /><label for="incfoot">包含页尾</label>
	</td>
</tr>
<tr>
	<td class="EasySiteText" bgcolor="#e7ebef">行 定 义<a href="javascript:top.agent.speak('中间区域每行的划分')"><img src="/agent/question_mark.gif" border="0" /></a></td>
	<td bgcolor="#FFFFFF" colspan="3">
		编辑第 <span id="rownumber">1</span> 行 &nbsp;
		行高<input type="text" id="row_height" name="row_height" value="200" size="5" />px &nbsp;
		列数<input type="text" id="col" name="col" value="3" size="5" /> &nbsp;
		列比例<input type="text" id="col_proportion" name="col_proportion" value="1:1:1" size="5" /> &nbsp;
		<input type="button" name="apply" value="应用" onclick="applyRow(rowNumber)" />
	</td>
</tr>
</table>
  </td>
</tr>
<tr>
	<td height="320" bgcolor="#999999">
	<div style="width:100%;height:320px;overflow-y:auto">
	<div id="preview" style="border:1px solid black;margin:8px;width:420px;height:240px;color:#666">
		<div id="head" style="background:white;height:20px;padding:4px">
			<h3>公共头部</h3>
		</div>
		<div id="layout" style="height:200px;">
			<div id="row1" class="row">
			<table width="100%" cellpadding="0" cellspacing="1" border="0">
				<tr bgcolor="yellow" id="row_1" class="rows">
					<td align="center" height="100"><h3>R1C1</h3></td>
					<td align="center"><h3>R1C2</h3></td>
					<td align="center"><h3>R1C3</h3></td>
				</tr>
			</table>
			<div id="edit" title="编辑此行" onclick="editrow(1)"></div>
			<div id="del" title="删除此行" onclick="delrow(1)"></div>
			<div id="add" title="增加一行" onclick="addrow()"></div>
			</div>
		</div>
		<div id="foot" style="background:white;height:20px;padding:4px">
			<h3>公共尾部</h3>
		</div>
	</div>
	</td>
</tr>
<tr>
    <td bgcolor="#FFFFFF">
    	<input type="submit" name="Submit" value="提交" class="EasySiteButton">
    </td>
</tr>
</form>
</table>
</div>
</body>
</html>
<%
End If

if FoundErr=True then
	call ErrMsg(Message)
end if
%>