<!--#include virtual="/Inc/conn.asp"-->
<!--#include virtual="/Inc/function.asp"-->
<!--#include virtual="/Inc/Admin/common.asp"-->
<%
'**************************************************************
' Software name: KnifeCMS
' Web: http://www.knifecms.com
' Copyright (C) 2007－2012 小刀出品 版权所有
'**************************************************************
Dim ZoneArea:ZoneArea=Trim(Request("zone"))
Dim Zcss:Zcss=Session("edit_css")
Dim result,results
if ZoneArea="" then
	Message=Message & "请您选择要提取的区域！"
	Response.Write Message
else
	call LoadZoneCSS()
end if

	Sub LoadZoneCSS()
		'导出信息
		results=getZoneCSS(Zcss,ZoneArea)
		If Len(results)>0 Then
			result=getColor(results,"background")
			result=result&getColor(results,"border")
			result=result&getColor(results,"color")
		End If
		Response.Write result
	End Sub
	
'取得区域的CSS
Function getZoneCSS(css,zone)
	Dim values,startp
	values=""
	startp=InStr(css,zone)
	If startp>=0 Then
		values=Mid(css,startp+Len(zone))
		values=split(values,"}")(0)
		values=Replace(values,"{","")
	End If
	getZoneCSS=values
End Function

'载入颜色值
Function getColor(ZoneCSS,prop)
	Dim values,startp
	values=""
	startp=InStr(ZoneCSS,prop)
	'找到属性
	If startp>=0 Then
		values=Mid(ZoneCSS,startp+Len(prop))
		values=split(values,";")(0)
		if InStr(values,":")>0 Then values=Mid(values,InStr(values,":"))
		'找到颜色值
		If InStr(values,"#")>0 Then
			values = Mid(values,InStr(values,"#"))
			values = split(values," ")(0)
		Else
			values="#FFF"
		End If
		'values=Replace(values,":","")
	Else
		values="#FFF"
	End If
	getColor=values
End Function
%>