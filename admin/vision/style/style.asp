<%
Dim addCSS:addCSS=Request("addcss")
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>KnifeCMS样式预览</title>
<meta name="keywords" content="KnifeCMS样式预览"/>
<meta name="description" content="KnifeCMS样式预览"/>
<meta name="author" content="http://www.knifecms.com/"/>
<link href="/Templates/default/css/screen.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/Templates/default/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
<link href="/Templates/default/css/jqModal.css" rel="stylesheet" type="text/css"/>
<link href="/Templates/default/css/start/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" media="screen"/>
<!--[if IE]>
	<link href="/Templates/default/css/ie.css" rel="stylesheet" type="text/css" media="screen"/>
<![endif]-->
<style type="text/css">
<%=Session("edit_css")%>
.editBox{
	background:yellow;
	border:1px solid black;
	display:block;
	width:40px;
	font-size:12px;
	padding:4px;
	text-align:center;
}
.editBox a{color:red;font-weight:bold}
<%=addcss%>
</style>
<script language="javascript">
function editZone(z){
	window.parent.editZone(z);
}
</script>
</head>
<body class='normal'>
	<div class="editBox"><a href="javascript:editZone('body')">全局</a></div>
	<div class="editBox"><a href="javascript:editZone('header')">头部区</a></div>
	<div id="header">
		<div id="logo"><a href="javascript:void(0)">Logo</a></div>
		<div id="title">网站标题</div>
		<ul id="main_nav">
			<li><b class="hover"><a href="javascript:void(0)">导航菜单1</a></b>导航菜单1</li>
			<li><b class="hover"><a href="javascript:void(0)">导航菜单2</a></b>导航菜单2</li>
		</ul>
		<div id="info_bar" style="height:28px">
			信息提示区
		</div>
	</div>
	
	<div class="editBox"><a href="javascript:editZone('content')">内容区</a></div>
	<div id="content" class="container">
		<div id="c1" class="cc span-24 last">
			<div id="m9" class="module" style="display:block">
				<div class="moduleFrame">
					<div class='moduleHeader'>
						<div class='moduleTitle'>组件标题</div>
						<div class='moduleActions'>
							<a href="#">链接样式</a>
						</div>
					</div>
					<div class='moduleContent'>
						组件内容
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="editBox"><a href="javascript:editZone('footer')">尾部区</a></div>
	<div id="footer">
		<div id="footer_bar">
			<a href="#">底部导航</a>
		</div>
		版权信息
	</div>
</body>
</html>