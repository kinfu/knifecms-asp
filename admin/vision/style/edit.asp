<!-- #include virtual="/Inc/conn.asp"-->
<!-- #include virtual="/Inc/md5.asp"-->
<!-- #include virtual="/Inc/function.asp"-->
<!-- #include virtual="/Inc/config.asp"-->
<!-- #include virtual="/Inc/Admin/common.asp"-->
<!-- #include virtual="/fckeditor/fckeditor.asp" -->
<%
'**************************************************************
' Software name: KnifeCMS
' Web: http://www.knifecms.com
' Copyright (C) 2007－2012 小刀出品 版权所有
'**************************************************************
ID=clng(Request("ID"))

If FoundErr<>True then
%>
<html>
<head>
<title>编辑样式</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/Skin/css/Admin/style.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="/Skin/css/Admin/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" />
<script type="text/javascript" src="/Skin/js/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/Skin/js/jquery/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript" src="/Admin/finder/fpick.js"></script>
</head>
<body>
<div style="margin:8px">
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
<form name="thisform" method="post" action="editSave.asp">
<tr>
	<td><span class="EasySiteTitle">编辑样式</span> <a href="design.asp?id=<%=ID%>">切换到设计模式</a></td>
</tr>
<tr>
	<td>
	<%
	sql="select * from db_tplstyle where id=" & ID
	set rs = Server.CreateObject("adodb.recordset")
	rs.Open sql,conn,1,1
	%>
	<input type="hidden" name="id" value="<%=rs("id")%>" />
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cecfce">
<tr>
	<td class="EasySiteText" bgcolor="#e7ebef" width="20%">样式名称<a href="javascript:top.agent.speak('样式名称，不超过150个汉字！')"><img src="/agent/question_mark.gif" border="0" /></a></td>
	<td bgcolor="#FFFFFF"><input name="title" id="title" value="<%=rs("title")%>" /></td>
</tr>
<tr>
	<td class="EasySiteText" bgcolor="#e7ebef">样式描述<a href="javascript:top.agent.speak('样式的描述')"><img src="/agent/question_mark.gif" border="0" /></a></td>
	<td bgcolor="#FFFFFF">
		<textarea name="description" style="width:90%;height:100px;border:1px solid gray"><%=rs("description")%></textarea>
	</td>
</tr>
<tr>
	<td class="EasySiteText" bgcolor="#e7ebef">样式内容<a href="javascript:top.agent.speak('样式内容')"><img src="/agent/question_mark.gif" border="0" /></a></td>
	<td bgcolor="#FFFFFF">
		<textarea name="css" style="width:90%;height:150px;border:1px solid gray"><%=rs("css")%></textarea>
	</td>
</tr>
</table>
	<%
	rs.Close
	set rs = Nothing
	%>
  </td>
</tr>
<tr>
    <td bgcolor="#FFFFFF"><input type="submit" name="Submit" value="提交(S)" class="EasySiteButton"></td>
</tr>
</form>
</table>
</div>
</body>
</html>
<%
End If

if FoundErr=True then
	call ErrMsg(Message)
end if
%>