<!-- #include virtual="/Inc/conn.asp"-->
<!-- #include virtual="/Inc/md5.asp"-->
<!-- #include virtual="/Inc/function.asp"-->
<!-- #include virtual="/Inc/page.asp"-->
<!-- #include virtual="/Inc/config.asp"-->
<!-- #include virtual="/Inc/Admin/common.asp"-->
<%
Call CheckPermission(10)
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/Skin/css/Admin/style.css" rel="stylesheet" type="text/css" />
<title>样式管理</title>
<script language="JavaScript" type="text/javascript">
function checkall(form){
	for (var i=0;i<form.elements.length;i++){
		var e = form.elements[i];
		if (e.name != 'chkall') e.checked = form.chkall.checked;
	}
}
</script>
</head>
<body>
<table width="99%" border="0" align="center" cellpadding="3" cellspacing="1">
	<tr>
		<td class="EasySiteTitle">样式管理</span></td>
	</tr>
	<tr>
		<td>
			[<a href="add.asp">新增样式</a>] &nbsp;
			[<a href="imp.asp">导入样式</a>] &nbsp;
			[<a href="exp.asp">导出样式</a>] &nbsp;
		</td>
	</tr>
	<form action="del.asp" method=post name="thisform" id="this_form">
	<tr>
		<td>
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cecfce">
	<tr>
		<th align="center" bgcolor="#e7ebef">样式名称</th>
		<th align="center" bgcolor="#e7ebef">样式描述</th>
		<th align="center" bgcolor="#e7ebef">操作</th>
	</tr>
	<%
	sql="select * from db_tplstyle"
	set rs = Server.CreateObject("adodb.recordset")
	rs.Open sql,conn,1,1
	dim a
	Set a=New PageList
	listLimit=20
	'模拟分页一页显示分页数
	pageLists=5
	'设置分页参数
	If rs.bof or rs.eof Then 
	%>
	<tr bgcolor="#C6E3F4">
		<td height="28" colspan="3" align="center" bgcolor="#FFFFFF">暂无记录！</td>
	</tr>
	<%
	else
		'分页循环开始
		totalList=rs.recordcount
		a.InitPara=array(totalList,listLimit,pageLists)
		a.PageList()
		rs.pagesize=listLimit
		page = request("page")
		If page="" Then
			page=1
		End If
		rs.absolutepage = page
		for i=1 to rs.pagesize
	%>
	<tr bgcolor="#C6E3F4">
		<td bgcolor="#FFFFFF" align="center"><%=rs("title")%></td>
		<td bgcolor="#FFFFFF" align="center"><%=rs("description")%></td>
		<td bgcolor="#FFFFFF" align="center">
			<a href="edit.asp?id=<%=rs("id")%>">编辑</a> | &nbsp;
			<a href="del.asp?id=<%=rs("id")%>">删除</a>
		</td>
	</tr>
	<%
		rs.movenext
		if rs.eof then exit for
		next
	End If
	%>
	</table>
	</td>
	</tr>
  <tr>
    <td><div align="right">
          <input id="chkall" type="checkbox" name="chkall" value="on" onClick="checkall(this.form)"><label for="chkall">全部选中</label>
          <input type="submit" name="action" onClick="{if(confirm('确定删除选定的纪录吗?')){document.getElementById('this_form').submit();return true;}return false;}" value="批量删除(D)" class="EasySiteButton">
    </div></td>
  </tr>
  <tr>
    <td><%=a.PageInfo%></td>
  </tr>
  </form>
</table>

<center>
<span class=smallTxt><%=systemCopyright%></span><br>
</center>
</body>
</html>