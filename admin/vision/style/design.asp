<!-- #include virtual="/Inc/conn.asp"-->
<!-- #include virtual="/Inc/md5.asp"-->
<!-- #include virtual="/Inc/function.asp"-->
<!-- #include virtual="/Inc/config.asp"-->
<!-- #include virtual="/Inc/Admin/common.asp"-->
<!-- #include virtual="/fckeditor/fckeditor.asp" -->
<%
'**************************************************************
' Software name: KnifeCMS
' Web: http://www.knifecms.com
' Copyright (C) 2007－2012 小刀出品 版权所有
'**************************************************************
ID=clng(Request("ID"))

If FoundErr<>True then
	sql="select * from db_tplstyle where id=" & ID
	set rs = Server.CreateObject("adodb.recordset")
	rs.Open sql,conn,1,1
	Session("edit_css")=rs("css")
%>
<html>
<head>
<title>编辑样式</title>
<META HTTP-EQUIV="pragma" CONTENT="no-cache"/>
<META HTTP-EQUIV="Cache-Control" CONTENT="no-store, must-revalidate"/>
<META HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT"/>
<META HTTP-EQUIV="expires" CONTENT="0"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/Skin/css/Admin/style.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="/Skin/css/Admin/smoothness/jquery-ui-1.8.custom.css" rel="stylesheet" />
<style>
.btn_ico{cursor:pointer}
</style>
<script type="text/javascript" src="/Skin/js/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/Skin/js/jquery/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript" src="/Skin/js/jquery/jquery.colorpicker.js"></script>
<script type="text/javascript" src="/Admin/finder/fpick.js"></script>
<script language="javascript">
jQuery.ajaxSetup ({
	cache: false
});

var editz='body';

$(function(){
	$("#background_ico").colorpicker({
		fillcolor:true,
		target:$("#background")
	});
	
	$("#border_ico").colorpicker({
		fillcolor:true,
		target:$("#border")
	});
	
	$("#color_ico").colorpicker({
		fillcolor:true,
		target:$("#color")
	});
	
	$("#link_ico").colorpicker({
		fillcolor:true,
		target:$("#link")
	});
	
	editZone(editz);
});

function editZone(z){
	if(z=="body"){
		$("#editarea").val('全局');
	}else if(z=="header"){
		$("#editarea").val('头部区');
	}else if(z=="content"){
		$("#editarea").val('内容区');
	}else if(z=="footer"){
		$("#editarea").val('尾部区');
	}
	editz=z;
	$.get("getZoneCSS.asp",{zone:z},function(data){
		if(data.length>0){
			$("#background").val('#'+data.split('#')[1]);
			$("#border").val('#'+data.split('#')[2]);
			$("#color").val('#'+data.split('#')[3]);
		}
	});
}

//预览样式
function previewCSS(){
	var zoneControl="#"+editz;
	if(editz=='body'){zoneControl=editz;}
	var addcss=encodeURIComponent(zoneControl + '{background-color:' + $("#background").val() + '!important;border-color:' + $("#border").val() + '!important;color:' + $("#color").val() + '!important;}');
	$("#preview")[0].src='style.asp?addcss='+addcss;
}

//应用样式
function applyCSS(){
	var css=$("#css").val();
	var patrn;
	var count=0;
	var zoneControl="#"+editz;
	if(editz=='body'){zoneControl=editz;}
	//找到对应的区域
	if(css.indexOf(zoneControl)>=0){
		//background
			patrn=new RegExp("("+zoneControl+"[\\W]*[^}]*background[^:#]*:[\\W]*)(#[0-9a-fA-F]{3,6})([^}]*\\})");
			count = css.search(patrn);
			if(count>0){
				css=css.replace(patrn, "$1 "+$("#background").val()+" $3");
			}else{
				patrn=new RegExp("("+zoneControl+"[\\W][^}]*{)([^}]*\\})");
				count = css.search(patrn);
				if(count>0){
					css=css.replace(patrn, "$1 background-color:"+$("#background").val()+"; $2");
				}else{
					css+=zoneControl + "{background-color:" + $("#background").val() + "}";
				}
			}
		//border
			patrn=new RegExp("("+zoneControl+"[\\W]*[^}]*border[^:#]*:[\\W]*)(#[0-9a-fA-F]{3,6})([^}]*\\})");
			count = css.search(patrn);
			if(count>0){
				css=css.replace(patrn, "$1"+$("#border").val()+"$3");
			}else{
				patrn=new RegExp("("+zoneControl+"[\\W][^}]*{)([^}]*\\})");
				count = css.search(patrn);
				if(count>0){
					css=css.replace(patrn, "$1 border-color:"+$("#border").val()+"; $2");
				}else{
					css+=zoneControl + "{border-color:" + $("#border").val() + "}";
				}
			}
		//color
			patrn=new RegExp("("+zoneControl+"[\\W]*[^}]*[^-]color[^:#]*:[\\W]*)(#[0-9a-fA-F]{3,6})([^}]*\\})");
			count = css.search(patrn);
			if(count>0){
				css=css.replace(patrn, "$1"+$("#color").val()+"$3");
			}else{
				patrn=new RegExp("("+zoneControl+"[\\W][^}]*{)([^}]*\\})");
				count = css.search(patrn);
				if(count>0){
					css=css.replace(patrn, "$1 color:"+$("#color").val()+"; $2");
				}else{
					css+=zoneControl + "{color:" + $("#color").val() + "}";
				}
			}
	}else{
		//不存在就添加
		css+=zoneControl + '{background-color:' + $("#background").val() + '!important;border-color:' + $("#border").val() + '!important;color:' + $("#color").val() + '!important;}';
	}
	//alert(css);
	$("#css").val(css);
}
</script>
</head>
<body>
<div style="margin:8px">
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
<form name="thisform" method="post" action="editSave.asp">
<tr>
	<td><span class="EasySiteTitle">编辑样式</span> <a href="edit.asp?id=<%=ID%>">切换到文本模式</a></td>
</tr>
<tr>
	<td>
	<input type="hidden" name="id" value="<%=rs("id")%>" />
	<input type="hidden" name="description" value="<%=rs("description")%>" />
	<div style="display:none"><textarea id="css" name="css"><%=Session("edit_css")%></textarea></div>
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cecfce">
<tr>
	<td class="EasySiteText" bgcolor="#e7ebef" colspan="2">样式名称<a href="javascript:top.agent.speak('样式名称，不超过150个汉字！')"><img src="/agent/question_mark.gif" border="0" /></a>
	<input name="title" id="title" value="<%=rs("title")%>" /></td>
</tr>
<tr>
	<td class="EasySiteText" bgcolor="#e7ebef" width="60%">
		样式预览<a href="javascript:top.agent.speak('样式预览')"><img src="/agent/question_mark.gif" border="0" /></a></td>
	<td class="EasySiteText" bgcolor="#e7ebef">
		控制面板
	</td>
</tr>
<tr>
	<td height="300" bgcolor="#FFFFFF">
		<iframe id="preview" src="style.asp" width="100%" height="100%" frameborder="0"></iframe>
	</td>
	<td bgcolor="#FFFFFF">
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
			<tr>
				<td>编辑范围</td>
				<td><input id="editarea" type="text" name="editarea" value="全局" /></td>
			</tr>
			<tr>
				<td>背景</td>
				<td><img id="background_ico" class="btn_ico" src="/Admin/portal/img/icons/colorpicker.png" border="0"/><input type="text" id="background" name="background" value="" /></td>
			</tr>
			<tr>
				<td>边框</td>
				<td><img id="border_ico" class="btn_ico" src="/Admin/portal/img/icons/colorpicker.png" border="0"/><input type="text" id="border" name="border" value="" /></td>
			</tr>
			<tr>
				<td>文字颜色</td>
				<td><img id="color_ico" class="btn_ico" src="/Admin/portal/img/icons/colorpicker.png" border="0"/><input type="text" id="color" name="color" value="" /></td>
			</tr>
			<tr>
				<td>链接颜色</td>
				<td><img id="link_ico" class="btn_ico" src="/Admin/portal/img/icons/colorpicker.png" border="0"/><input type="text" id="link" name="link" value="" /></td>
			</tr>
			<tr>
				<td colspan="2">注意：应用后，需提交保存才可看到效果</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="button" value="预览" onclick="previewCSS()" /> &nbsp; 
					<input type="button" value="应用" onclick="applyCSS()" />
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
	<%
	rs.Close
	set rs = Nothing
	%>
  </td>
</tr>
<tr>
    <td bgcolor="#FFFFFF"><input type="submit" name="Submit" value="提交(S)" class="EasySiteButton"></td>
</tr>
</form>
</table>
</div>
</body>
</html>
<%
End If

if FoundErr=True then
	call ErrMsg(Message)
end if
%>