<!--#include file="config.admin.asp"-->
<%
Dim Weixin
Set Weixin = New Class_KnifeCMS_Admin_Weixin
Set Weixin = Nothing
Class Class_KnifeCMS_Admin_Weixin

	Private Pv_UserID,Pv_Username,Pv_WeixinID,Pv_IP
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "weixin.lang.asp"
		IsContentExist = False
		Header()
		If Ctl="weixin" Then
			Select Case Action
				Case "view"
					Call WeixinGetSearchCondition()
					Call WeixinView()
				Case "del"  : WeixinDel
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"goback",0
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function WeixinGetSearchCondition()
		Pv_UserID     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","userid"))
		If Pv_UserID = 0 Then Pv_UserID=""
		Pv_Username   = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("both","username")),24)
		Pv_WeixinID  = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","weixinid"))
		If Pv_WeixinID = 0 AND KnifeCMS.GetForm("both","weixinid")<>"0" Then Pv_WeixinID=""
		Pv_IP         = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","ip"),"[^0-9.]",""),15)
	End Function
	Private Function WeixinSearchSql()
		Dim Fn_TempSql
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Pv_UserID <> ""   ," AND a.UserID="& Pv_UserID &" "," ")
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Pv_Username <>""  ," AND a.Username='"& Pv_Username &"' "," ")
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Pv_WeixinID <> ""," AND a.P_ID="& Pv_WeixinID &" "," ")
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Pv_IP <>""        ," AND a.IP='"& Pv_IP &"' "," ")
		WeixinSearchSql = Fn_TempSql
	End Function
	
	Private Function WeixinList()
		Dim Fn_TempSql,Fn_ColumnNum
			Fn_TempSql   = "SELECT a.ID,a.UserID,a.Username,a.HeadImg,a.Content,a.Path,a.AddTime,a.ChildNum FROM ["& DBTable_Members_Feeds &"] a"
			Fn_ColumnNum = 8
			Operation    = ""
			Fn_TempSql   = Fn_TempSql & " WHERE a.Disabled=0 "
			If SubAction = "search" Then
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.KeyWords <>""," AND a.Content like '%"& Admin.Search.KeyWords &"%' "," ")
				Select Case DB_Type
				Case 0
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.AddTime>=#"& Admin.Search.StartDate &"# ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.AddTime<=#"& Admin.Search.EndDate &"# ","")
				Case 1
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.AddTime>='"& Admin.Search.StartDate &"' ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.AddTime<='"& Admin.Search.EndDate &"' ","")
				End Select
				tempCondition = tempCondition & WeixinSearchSql
				PageUrlPara = PageUrlPara & "&startdate="& Server.URLEncode(Admin.Search.StartDate) &"&enddate="& Server.URLEncode(Admin.Search.EndDate) &"&keywords="& Server.URLEncode(Admin.Search.KeyWords)&"&userid="& Pv_UserID &"&username="& Pv_Username &"&weixin="& Pv_WeixinID &"&ip="& Pv_IP &""
				Fn_TempSql = Fn_TempSql & tempCondition
			End If
			Fn_TempSql = Fn_TempSql &" ORDER BY a.ID DESC"
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
	End Function
	
	
	Private Function WeixinDel()
		Call WeixinSubDo()
	End Function
	
	Private Sub WeixinSubDo()
		ID = KnifeCMS.GetForm("post","InputName")
		Dim Fn_Rs,Fn_ArrID,Fn_i,Fn_P_ID,Fn_Path
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			Fn_Path = ""
			ID      = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			If ID>0 Then
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Members_Feeds &":P_ID,Path",Array("ID:"&ID),"")
				If Not(Fn_Rs.Eof) Then
					Fn_P_ID       = KnifeCMS.Data.CLng(Fn_Rs(0))
					Fn_Path       = Fn_Rs(1)
					If Fn_P_ID > 0 Then Call KnifeCMS.DB.UpdateRecord(DBTable_Members_Feeds,Array("ChildNum:ChildNum-1"),Array("ID:"&Fn_P_ID))
					Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_Members_Feeds &"] WHERE Path LIKE '"& Fn_Path &"%'")
					If Result Then Msg = Msg & Lang_Weixin_Cue(4) &"[ID="& ID &"]<br>"
				End If
				KnifeCMS.DB.CloseRs Fn_Rs
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Sub
	
	Private Function WeixinView()
%>
	<%=Admin.Calendar(Array("","",""))%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
          <td class="tdcell">
              <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
          <td class="tdcell">
            <table border="0" cellpadding="2" cellspacing="0">
            <tr><td class="upcell">
                <div class="align-center grey">
                    <% Echo Lang_DistrictDoSearch %><!--搜索操作区-->
                    <span class="AdvancedSearch"><a href="javascript:Admin.AdvancedSearch.Open(600,324,'AdvancedSearch')" ><% Echo Lang_AdvancedSearch %></a></span>
                </div>
                <DIV id="AdvancedSearch" style="display:none;">
                    <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                    <div class="diainbox">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                      <tr><td class="titletd"><% Echo Lang_Weixin(1) %></td>
                          <td class="infotd"><input type="text" class="TxtSer" name="userid" style="width:220px;" value="<% Echo Pv_UserID %>" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_Weixin(2) %></td>
                          <td class="infotd"><input type="text" class="TxtSer" name="username" style="width:220px;" value="<% Echo Pv_Username %>" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_Weixin(3) %></td>
                          <td class="infotd"><input type="text" class="TxtSer" name="weixinid" style="width:220px;" value="<% Echo Pv_WeixinID %>" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_Keywords %></td>
                          <td class="infotd"><input type="text" class="TxtClass" id="keywords" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:220px;" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_StartDate %>:</td>
                          <td class="infotd"><input type="text" class="TxtSer dateStart" id="startdate" name="startdate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:200px;" value="<%=Admin.Search.StartDate%>" /></td>
                      <tr><td class="titletd"><% Echo Lang_EndDate %>:</td>
                          <td class="infotd"><input type="text" class="TxtSer dateEnd" id="enddate" name="enddate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:200px;" value="<%=Admin.Search.EndDate%>" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_Weixin(4) %></td>
                          <td class="infotd"><input type="text" class="TxtSer" name="ip" id="ip" style="width:220px;" value="<% Echo Pv_IP %>" /></td>
                      </tr>
                      </table>
                    </div>
                    <% Echo AdvancedSearchBtnline %>
                    </form>
                </DIV>
                </td></tr>
            <tr><td class="downcell">
                <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                <table border="0" cellpadding="0" cellspacing="0"><tr>
                <td><span><% Echo Lang_Keywords %>:</span></td>
                <td class="pl3"><input type="text" class="TxtClass" name="keywords" id="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:150px;" /></td>
                <td class="pl3"><button type="submit" class="sysBtn" ><b class="icon icon_search"></b><% Echo Lang_Btn_Search %></button></td>
                </tr></table>
                </form>
            </td></tr>
            </table>
          </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
            <th width="1%" ><div style="width:50px; "><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
			<th width="5%" ><div style="width:54px; "><% Echo Lang_Weixin_ListTableColLine(1) %><!--会员ID--></div></th>
            <th width="5%" ><div style="width:54px; "><% Echo Lang_Weixin_ListTableColLine(2) %><!--用户名--></div></th>
            <th width="5%" ><div style="width:54px; "><% Echo Lang_Weixin_ListTableColLine(3) %><!--会员头像--></div></th>
            <th width="60%"><div style="width:60px; "><% Echo Lang_Weixin_ListTableColLine(4) %><!--微博内容--></div></th>
            <th width="5%" ><div style="width:54px; "><% Echo Lang_Weixin_ListTableColLine(7) %><!--类别--></div></th>
            <th width="15%"><div style="width:60px;"><% Echo Lang_Weixin_ListTableColLine(5) %><!--发表时间--></div></th>
            <th width="5%" ><div style="width:56px; "><% Echo Lang_Weixin_ListTableColLine(6) %><!--回复数--></div></th>
			<th width="1%" ><div style="width:60px; "><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% WeixinList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
    <script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	if(objExist(document.getElementById("keywords"))){ReplaceText(document.getElementById("keywords").value,1)}
	function ChangeTdText(){
		var tds=document.getElementById("listForm").getElementsByTagName('td');
		var TempHTML;
		var _ScreenWidth  = ScreenWidth();if(_ScreenWidth>1440){_ScreenWidth=_ScreenWidth-200;}else{_ScreenWidth=_ScreenWidth-100;}
		var _ScreenHeight = parseInt(ScreenHeight())-100;
		if(tds!=null){
			var WeixinID,TempID;
			for(var i=0;i<tds.length-1;i++){
				WeixinID = ""
				if(tds[i].id=="td3" && tds[i].innerHTML!=""){
					TempHTML = "<span class=\"sImg\"><a href=\"../?user/"+tds[i-2].innerHTML+"\" target=\"_blank\"><img src='"+ unescape(tds[i].innerHTML) +"' /></a></span>";
					tds[i].innerHTML=TempHTML;
				}else if(tds[i].id=="td5"){
					TempHTML = tds[i].innerHTML.split(",")
					if(TempHTML.length>3){
						tds[i].innerHTML=TempHTML.length-3 + Lang_Js[13];
					}else{
						tds[i].innerHTML=Lang_Js[11];
					}
				}else if(tds[i].id=="td7" && tds[i].innerHTML!=""){
					WeixinID = tds[i-7].innerHTML;
					TempHTML = "<a href=\"javascript:ShowModal('?ctl=<%=Ctl%>&act=<%=Action%>&subact=search&weixinid="+ WeixinID +"',"+_ScreenWidth+","+_ScreenHeight+");ShowModalReload();\" title=\""+Lang_Js[3]+"\">"+ unescape(tds[i].innerHTML) + Lang_Js[8] +"</a></span>";
					tds[i].innerHTML=TempHTML;
				}
				
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
End Class
%>