<!--#include file="config.admin.asp"-->
<%
Dim SystemSet
Set SystemSet = New Class_KnifeCMS_Admin_SystemSet
Set SystemSet = Nothing
Class Class_KnifeCMS_Admin_SystemSet
    Private Pv_Rs,Pv_JSON
	Private Site_Language,Site_AdminLanguage,Site_Domain,Site_SiteURL,Site_SystemPath
	Private Site_SystemConfig,Site_UserCookieType,Site_AdminCookieType,Site_OnlineService
	Private Site_CloseModuleVote,Site_CloseModuleShop
	Private Site_FilePathType
	Private Site_IsDeBug
	Private Site_ContentCommentSet,Site_IsContentCommentCheck,Site_IsCache,Site_CacheTime,Site_FilterWords,Site_FilterIP,Site_Notice
	Private Site_GoodsConsultSet,Site_GoodsCommentSet,Site_IsGoodsConsultCheck,Site_IsGoodsCommentCheck
	Private Site_Name,Site_Logo,Site_MyhomeLogo,Site_BriefIntroduction,Site_Icp,Site_Copyright,Site_StatisticsCode
	Private Site_CompanyName,Site_Address,Site_Postcode,Site_Telephone,Site_Fax,Site_MobilePhone,Site_Email
	Private Site_IsTax,Site_TaxRate
	Private Site_Title,Site_MetaKeywords,Site_MetaDescription
	Private Shop_Title,Shop_MetaKeywords,Shop_MetaDescription
	Private Pv_MailConfig,Mail_smtp,Mail_mailserver_username,Mail_mailserver_password,Mail_from,Mail_fromname
	Private Site_Status,Site_ClosedReasons
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "systemset.lang.asp"
		Header()
		Site_GoodsCommentSet=0
		Site_IsGoodsCommentCheck=0
		Site_IsContentCommentCheck=0
		If Ctl<>"systemset" Then
			Admin.ShowMessage Lang_IllegalSysPra,"goback",0
		Else
			Select Case Action
				Case "view"
					Site_Language              = SiteLanguage
					Site_AdminLanguage         = SiteAdminLanguage
					Site_Domain                = Domain
					Site_SiteURL               = SiteURL
					Site_SystemPath            = SystemPath
					Site_UserCookieType        = KnifeCMS.Data.CLng(KnifeCMS.SysConfig("UserCookieType"))
					Site_AdminCookieType       = KnifeCMS.Data.CLng(KnifeCMS.SysConfig("AdminCookieType"))
					Site_OnlineService         = KnifeCMS.SysConfig("OnlineService")
					Site_CloseModuleVote       = CloseModuleVote
					Site_CloseModuleShop       = CloseModuleShop
					Site_FilePathType          = SiteFilePathType
					Site_ContentCommentSet     = ContentCommentSet
					Site_IsContentCommentCheck = IsContentCommentCheck
					Site_IsDeBug               = IsDeBug
					Site_IsCache               = IsCache
					Site_CacheTime             = CacheTime
					Site_FilterWords         = SiteFilterWords
					Site_FilterIP            = SiteFilterIP
					Site_Notice              = SiteNotice
					Site_StatisticsCode      = SiteStatisticsCode
					Site_Name                = SiteName
					Site_Title               = SiteTitle
					Site_MetaKeywords        = SiteMetaKeywords
					Site_MetaDescription     = SiteMetaDescription
					Site_Logo                = SiteLogo
					Site_MyhomeLogo          = SiteMyhomeLogo
					Site_BriefIntroduction   = SiteBriefIntroduction
					Site_Icp                 = SiteIcp
					Site_Copyright           = SiteCopyright
					Site_CompanyName         = SiteCompanyName
					Site_Address             = SiteAddress
					Site_Postcode            = SitePostcode
					Site_Telephone           = SiteTelephone
					Site_Fax                 = SiteFax
					Site_MobilePhone         = SiteMobilePhone
					Site_Email               = SiteEmail
					
					'网店系统与SEO优化设置
					Site_GoodsConsultSet     = GoodsConsultSet
					Site_GoodsCommentSet     = GoodsCommentSet
					Site_IsGoodsConsultCheck = IsGoodsConsultCheck
					Site_IsGoodsCommentCheck = IsGoodsCommentCheck
					Site_IsTax               = SiteIsTax
					Site_TaxRate             = SiteTaxRate
					Shop_Title               = ShopTitle
					Shop_MetaKeywords        = ShopMetaKeywords
					Shop_MetaDescription     = ShopMetaDescription
					
					'邮件发送设置
					Call GetMailConfig()
					
					'网站状态设置
					Site_Status              = SiteStatus
					Site_ClosedReasons       = SiteClosedReasons
					
					Call SystemSetView()
				Case "edit" : If SubAction="save" Then SystemSetDoSave
			End Select
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Sub GetMailConfig()
		On Error Resume Next
		Set Pv_JSON = KnifeCMS.JSON.Parse(MailConfig)
		Mail_smtp                = Pv_JSON.smtp
		Mail_mailserver_username = Pv_JSON.mailserver_username
		Mail_mailserver_password = KnifeCMS.Data.HTMLDecode(Pv_JSON.mailserver_password)
		Mail_from                = KnifeCMS.Data.HTMLDecode(Pv_JSON.from)
		Mail_fromname            = KnifeCMS.Data.HTMLDecode(Pv_JSON.fromname)
		Set Pv_JSON = Nothing
		Err.Clear()
	End Sub
	
	Private Sub ReformMailConfig()
		Mail_smtp                = KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Mail_smtp"))
		Mail_mailserver_username = KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Mail_mailserver_username"))
		Mail_mailserver_password = KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Mail_mailserver_password"))
		Mail_from                = KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Mail_from"))
		Mail_fromname            = KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Mail_fromname"))
		Pv_MailConfig = "{""smtp"":"""& Mail_smtp &""",""mailserver_username"":"""& Mail_mailserver_username &""",""mailserver_password"":"""& Mail_mailserver_password &""",""from"":"""& Mail_from &""",""fromname"":"""& Mail_fromname &"""}"
	End Sub
	
	Function SystemSetDoSave()
		'获取数据并处理数据
		Site_Language              = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","Site_Language"),"[^0-9a-zA-Z\-.]",""),20)
		Site_AdminLanguage         = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","Site_AdminLanguage"),"[^0-9a-zA-Z\-.]",""),20)
		
		Site_Domain                = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","Site_Domain"),"[^0-9a-zA-Z_\-.]",""),100)
		Site_SiteURL               = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","Site_SiteURL"),"[^0-9a-zA-Z_\-.\:\/]",""),100)
		Site_SystemPath            = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","Site_SystemPath"),"[^0-9a-zA-Z_\-.\/]",""),100)
		
		Site_UserCookieType        = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_UserCookieType"))
		Site_AdminCookieType       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_AdminCookieType"))
		Site_OnlineService         = Replace(KnifeCMS.SysConfig("OnlineService"),"""","""""")
		Site_SystemConfig          = "UserCookieType="& Site_UserCookieType &";AdminCookieType="& Site_AdminCookieType &";OnlineService="& Site_OnlineService  &""
					
		Site_CloseModuleVote       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_CloseModuleVote"))
		Site_CloseModuleShop       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_CloseModuleShop"))
		
		Site_IsDeBug               = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_IsDeBug"))
		Site_FilePathType          = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","Site_FilePathType"),"[^a-zA-Z]",""),20)
		
		Site_GoodsConsultSet       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_GoodsConsultSet"))
		Site_GoodsCommentSet       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_GoodsCommentSet"))
		Site_IsGoodsConsultCheck   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_IsGoodsConsultCheck"))
		Site_IsGoodsCommentCheck   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_IsGoodsCommentCheck"))
		Site_ContentCommentSet     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_ContentCommentSet"))
		Site_IsContentCommentCheck = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_IsContentCommentCheck"))
		Site_IsCache               = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_IsCache"))
		Site_CacheTime             = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_CacheTime"))
		Site_FilterWords           = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_FilterWords")),10000)
		Site_FilterIP              = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_FilterIP")),10000)
		Site_Notice                = KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(KnifeCMS.GetForm("post","Site_Notice")),10000)
		
		Site_Name                  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_Name")),100)
		
		Site_Title                 = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_Title")),100)
		Site_MetaKeywords          = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_MetaKeywords")),250)
		Site_MetaDescription       = KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(KnifeCMS.GetForm("post","Site_MetaDescription")),10000)
		Shop_Title                 = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Shop_Title")),100)
		Shop_MetaKeywords          = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Shop_MetaKeywords")),250)
		Shop_MetaDescription       = KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(KnifeCMS.GetForm("post","Shop_MetaDescription")),250)
		
		Site_Logo                  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_Logo")),250)
		Site_MyhomeLogo            = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_MyhomeLogo")),250)
		Site_BriefIntroduction     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_BriefIntroduction")),10000)
		Site_Icp                   = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_Icp")),50)
		Site_Copyright             = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_Copyright")),10000)
		Site_StatisticsCode        = KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(KnifeCMS.GetForm("post","Site_StatisticsCode")),10000)
		
		Site_CompanyName           = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_CompanyName")),100)
		Site_Address               = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_Address")),250)
		Site_Postcode              = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_Postcode")),50)
		Site_Telephone             = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_Telephone")),100)
		Site_Fax                   = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_Fax")),100)
		Site_MobilePhone           = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_MobilePhone")),100)
		Site_Email                 = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Site_Email")),250)
		
		Call ReformMailConfig()
		
		Site_Status                = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_Status"))
		Site_ClosedReasons         = KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(KnifeCMS.GetForm("post","Site_ClosedReasons")),10000)
		
		Site_IsTax                 = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Site_IsTax"))
		Site_TaxRate               = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","Site_TaxRate"))
						
		If Site_IsTax=0 Then Site_TaxRate=0
		
		Site_Status = KnifeCMS.IIF(Site_Status<>0,1,0)
		
		If KnifeCMS.Data.IsNul(Site_Language)      Then ErrMsg = ErrMsg &"Site_Language"& Lang_SystemSet(2) &"<br>"
		If KnifeCMS.Data.IsNul(Site_AdminLanguage) Then ErrMsg = ErrMsg &"Site_AdminLanguage"& Lang_SystemSet(2) &"<br>"
		
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		
		'开始保存数据操作
		If Action="edit" then

			'保存配置到config.main.asp
			Dim Fn_Text
			Fn_Text = KnifeCMS.Read(SystemPath & "config/config.main.asp")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteLanguage([\s]*?)=([\s]*?)(""\S*"")","SiteLanguage = """& Site_Language &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteAdminLanguage([\s]*?)=([\s]*?)(""\S*"")","SiteAdminLanguage = """& Site_AdminLanguage &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"Domain([\s]*?)=([\s]*?)(""\S*"")","Domain = """& Site_Domain &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteURL([\s]*?)=([\s]*?)(""\S*"")","SiteURL = """& Site_SiteURL &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SystemPath([\s]*?)=([\s]*?)(""\S*"")","SystemPath = """& Site_SystemPath &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SystemConfig([\s]*?)=([\s]*?)(""\S*"")","SystemConfig = """& Site_SystemConfig &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"CloseModuleVote([\s]*?)=([\s]*?)(\d+?)","CloseModuleVote = "& Site_CloseModuleVote &"")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"CloseModuleShop([\s]*?)=([\s]*?)(\d+?)","CloseModuleShop = "& Site_CloseModuleShop &"")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"IsDeBug([\s]*?)=([\s]*?)(\d+?)","IsDeBug = "& Site_IsDeBug &"")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteFilePathType([\s]*?)=([\s]*?)(""\S*"")","SiteFilePathType = """& Site_FilePathType &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteFilterWords([\s]*?)=([\s]*?)(""\S*"")","SiteFilterWords = """& Site_FilterWords &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteFilterIP([\s]*?)=([\s]*?)(""\S*"")","SiteFilterIP = """& Site_FilterIP &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteNotice([\s]*?)=([\s]*?)(""\S*"")","SiteNotice = """& Site_Notice &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteName([\s]*?)=([\s]*?)(""\S*"")","SiteName = """& Site_Name &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteTitle([\s]*?)=([\s]*?)(""\S*"")","SiteTitle = """& Site_Title &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteMetaKeywords([\s]*?)=([\s]*?)(""\S*"")","SiteMetaKeywords = """& Site_MetaKeywords &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteMetaDescription([\s]*?)=([\s]*?)(""\S*"")","SiteMetaDescription = """& Site_MetaDescription &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteLogo([\s]*?)=([\s]*?)(""\S*"")","SiteLogo = """& Site_Logo &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteMyhomeLogo([\s]*?)=([\s]*?)(""\S*"")","SiteMyhomeLogo = """& Site_MyhomeLogo &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteIcp([\s]*?)=([\s]*?)(""\S*"")","SiteIcp = """& Site_Icp &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteStatisticsCode([\s]*?)=([\s]*?)(""\S*"")","SiteStatisticsCode = """& Site_StatisticsCode &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteCompanyName([\s]*?)=([\s]*?)(""\S*"")","SiteCompanyName = """& Site_CompanyName &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteAddress([\s]*?)=([\s]*?)(""\S*"")","SiteAddress = """& Site_Address &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SitePostcode([\s]*?)=([\s]*?)(""\S*"")","SitePostcode = """& Site_Postcode &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteTelephone([\s]*?)=([\s]*?)(""\S*"")","SiteTelephone = """& Site_Telephone &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteFax([\s]*?)=([\s]*?)(""\S*"")","SiteFax = """& Site_Fax &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteMobilePhone([\s]*?)=([\s]*?)(""\S*"")","SiteMobilePhone = """& Site_MobilePhone &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteEmail([\s]*?)=([\s]*?)(""\S*"")","SiteEmail = """& Site_Email &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteStatus([\s]*?)=([\s]*?)(\d+?)","SiteStatus = "& Site_Status &"")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteClosedReasons([\s]*?)=([\s]*?)(""\S*"")","SiteClosedReasons = """& Site_ClosedReasons &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"GoodsConsultSet([\s]*?)=([\s]*?)(\d+?)","GoodsConsultSet = "& Site_GoodsConsultSet &"")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"GoodsCommentSet([\s]*?)=([\s]*?)(\d+?)","GoodsCommentSet = "& Site_GoodsCommentSet &"")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"IsGoodsConsultCheck([\s]*?)=([\s]*?)(\d+?)","IsGoodsConsultCheck = "& Site_IsGoodsConsultCheck &"")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"IsGoodsCommentCheck([\s]*?)=([\s]*?)(\d+?)","IsGoodsCommentCheck = "& Site_IsGoodsCommentCheck &"")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"ContentCommentSet([\s]*?)=([\s]*?)(\d+?)","ContentCommentSet = "& Site_ContentCommentSet &"")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"IsContentCommentCheck([\s]*?)=([\s]*?)(\d+?)","IsContentCommentCheck = "& Site_IsContentCommentCheck &"")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"IsCache([\s]*?)=([\s]*?)(\d+?)","IsCache = "& Site_IsCache &"")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"CacheTime([\s]*?)=([\s]*?)(([0-9]+)([0-9])*?)","CacheTime = "& Site_CacheTime &"")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteIsTax([\s]*?)=([\s]*?)(\d+?)","SiteIsTax = "& Site_IsTax &"")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SiteTaxRate([\s]*?)=([\s]*?)(""\S*"")","SiteTaxRate = """& Site_TaxRate &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"ShopTitle([\s]*?)=([\s]*?)(""\S*"")","ShopTitle = """& Shop_Title &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"ShopMetaKeywords([\s]*?)=([\s]*?)(""\S*"")","ShopMetaKeywords = """& Shop_MetaKeywords &"""")
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"ShopMetaDescription([\s]*?)=([\s]*?)(""\S*"")","ShopMetaDescription = """& Shop_MetaDescription &"""")
			
			Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"MailConfig([\s]*?)=([\s]*?)(""\S*"")","MailConfig = """& Replace(Pv_MailConfig,"""","""""") &"""")

			If KnifeCMS.FSO.SaveTextFile("config/config.main.asp",Fn_Text)=True Then
				If Site_Domain<>Domain Then
				'更改网站导航链接
					Call UpdateDomain(Domain,Site_Domain)
				End If
				
				Admin.ShowMessage Lang_SystemSet(4),Array("javascript:window.top.location='index.asp?ctl=systemindex&act=view';",Lang_Fresh),1
			Else
				Admin.ShowMessage Lang_SystemSet(3),"?ctl="& Ctl &"&act=view",1
			End If
		End If

	End Function
	
	Function UpdateDomain(ByVal BV_Old,ByVal BV_New)
		Dim Fn_Old,Fn_New,Fn_Rs,Fn_ID,Fn_Temp
		Fn_Old = BV_Old
		Fn_New = BV_New
		'Echo "UpdateDomain(ByVal """& Fn_Old &""",ByVal """& Fn_New &""")<br>"
		Select Case DB_Type
		Case 0
			'更新网站导航
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,NavUrl FROM ["& DBTable_Navigator &"] ORDER BY ID ASC")
			Do While Not Fn_Rs.Eof
				Fn_ID    = Fn_Rs(0)
				Fn_Temp  = KnifeCMS.Data.ReplaceString(Fn_Rs(1),Fn_Old,Fn_New)
				Call KnifeCMS.DB.UpdateRecord(DBTable_Navigator,Array("NavUrl:"& Fn_Temp),Array("ID:"& Fn_ID))
			Fn_Rs.Movenext
			Loop
			KnifeCMS.DB.CloseRs Fn_Rs
		Case 1
			Call KnifeCMS.DB.Execute("UPDATE ["& DBTable_Navigator &"] SET [NavUrl]=REPLACE([NavUrl],'"& Fn_Old &"','"& Fn_New &"')")
		End Select
	End Function
	
	Function SystemSetView()
	%>
	<style type="text/css">body{ padding-bottom:37px;}</style>
	<DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
	  <div class="wbox">
		<form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=edit&subact=save" method="post">
		<ul id="ContentTabs" class="TabsStyle">
		   <li class="current" onclick=TabSwitch("ContentTabs","li","Tabs",1)><%=Lang_SystemSet(5)%></li>
           <li onclick=TabSwitch("ContentTabs","li","Tabs",2)><%=Lang_SystemSet(7)%></li>
           <li onclick=TabSwitch("ContentTabs","li","Tabs",3) <% If Site_CloseModuleShop Then Echo "style=""display:none;""" %>><%=Lang_SystemSet(9)%></li>
           <li onclick=TabSwitch("ContentTabs","li","Tabs",4)><%=Lang_SystemSet(11)%></li>
		   <li onclick=TabSwitch("ContentTabs","li","Tabs",5)><%=Lang_SystemSet(10)%></li>
		</ul>
		<div id="Tabs_1">
        
            <!--语言设置-->
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
              <tr><td class="titletd"><%=Lang_LanguageSet%></td>
                   <td class="infotd">
                   <select name="Site_Language" id="Site_Language">
                   <option value="zh-cn" <%=KnifeCMS.IIF(Site_Language="zh-cn","selected=""selected""","")%>><%=Lang_Language(0)%></option>
                   <!--<option value="zh-tw" <%=KnifeCMS.IIF(Site_Language="zh-tw","selected=""selected""","")%>><%=Lang_Language(1)%></option>
                   <option value="en-uk" <%=KnifeCMS.IIF(Site_Language="en-uk","selected=""selected""","")%>><%=Lang_Language(2)%></option>-->
                   </select>
                   </td>
              </tr>
              <tr><td class="titletd"><%=Lang_AdminLanguageSet%></td>
                   <td class="infotd">
                   <select name="Site_AdminLanguage" id="Site_AdminLanguage">
                   <option value="zh-cn" <%=KnifeCMS.IIF(Site_AdminLanguage="zh-cn","selected=""selected""","")%>><%=Lang_Language(0)%></option>
                   <!--<option value="zh-tw" <%=KnifeCMS.IIF(Site_AdminLanguage="zh-tw","selected=""selected""","")%>><%=Lang_Language(1)%></option>
                   <option value="en-uk" <%=KnifeCMS.IIF(Site_AdminLanguage="en-uk","selected=""selected""","")%>><%=Lang_Language(2)%></option>-->
                   </select>
                   </td>
              </tr>
            </table>

            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
              <tr><td class="titletd"><%=Lang_DomainSet(1)%></td>
                   <td class="infotd">
                   <input type="text" class="TxtClass slong" id="Site_Domain" name="Site_Domain" value="<%=Site_Domain%>"/>
                   <label class="Normal" id="d_Site_Domain"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_DomainSet(2)%></td>
                   <td class="infotd">
                   <input type="text" class="TxtClass slong" id="Site_SiteURL" name="Site_SiteURL" value="<%=Site_SiteURL%>"/>
                   <label class="Normal" id="d_Site_SiteURL"><%=Lang_DomainSet(4)%></label>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_DomainSet(3)%></td>
                   <td class="infotd">
                   <input type="text" class="TxtClass slong" id="Site_SystemPath" name="Site_SystemPath" value="<%=Site_SystemPath%>"/>
                   <label class="Normal" id="d_Site_SystemPath"><%=Lang_DomainSet(5)%></label>
                   </td>
               </tr>
            </table>
            </div>
            
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
               <tr><td class="titletd"><%=Lang_UserSet(1)%></td>
                   <td class="infotd">
                   <input type="radio" value="0" name="Site_UserCookieType" <%=Admin.Input_Checked(Site_UserCookieType,0)%> /><%=Lang_UserSet(2)%>
                   <input type="radio" value="1" name="Site_UserCookieType" <%=Admin.Input_Checked(Site_UserCookieType,1)%> /><%=Lang_UserSet(3)%>
                   <span class="ml10"><label class="Normal"><%=Lang_UserSet(4)%></label></span>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_UserSet(5)%></td>
                   <td class="infotd">
                   <input type="radio" value="0" name="Site_AdminCookieType" <%=Admin.Input_Checked(Site_AdminCookieType,0)%> /><%=Lang_UserSet(2)%>
                   <input type="radio" value="1" name="Site_AdminCookieType" <%=Admin.Input_Checked(Site_AdminCookieType,1)%> /><%=Lang_UserSet(3)%>
                   <span class="ml10"><label class="Normal"><%=Lang_UserSet(4)%></label></span>
                   </td>
               </tr>
            </table>
            </div>
            
            <!--系统功能模块-->
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
             <tbody>
               <tr><td class="titletd"><%=Lang_SystemModule(0)%></td>
                   <td class="infotd">
                   <div class="pb5">
                   <input type="radio" value="0" name="Site_CloseModuleVote" <%=Admin.Input_Checked(Site_CloseModuleVote,0)%> /><%=Lang_Open%>
                   <input type="radio" value="1" name="Site_CloseModuleVote" <%=Admin.Input_Checked(Site_CloseModuleVote,1)%> /><%=Lang_Close%>
                   <span class="ml10"><%=Lang_SystemModule(2)%></span>
                   <label class="Normal"></label>
                   </div>
                   <div class="pt5 pb5" style="border-top:1px solid #eee;">
                   <input type="radio" value="0" name="Site_CloseModuleShop" <%=Admin.Input_Checked(Site_CloseModuleShop,0)%> /><%=Lang_Open%>
                   <input type="radio" value="1" name="Site_CloseModuleShop" <%=Admin.Input_Checked(Site_CloseModuleShop,1)%> /><%=Lang_Close%>
                   <span class="ml10"><%=Lang_SystemModule(3)%></span>
                   <label class="Normal"></label>
                   </div>
                   </td>
              </tr>
             </table>
            </div>
            
            <!--评论设置-->
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
             <tbody>
              <tr><td class="titletd"><%=Lang_SystemSet(51)%></td>
                  <td class="infotd">
                  <input type="radio" name="Site_ContentCommentSet" value="0" <%=Admin.Input_Checked(Site_ContentCommentSet,0)%> />
                  <label><%=Lang_CommentAllowUser %></label>
                  <input type="radio" name="Site_ContentCommentSet" value="1" <%=Admin.Input_Checked(Site_ContentCommentSet,1)%>/>
                  <label><%=Lang_CommentForbid%></label>
                  </td>
              </tr>
              <tr><td class="titletd"><%=Lang_SystemSet(52)%></td>
                  <td class="infotd">
                  <input type="radio" name="Site_IsContentCommentCheck" value="0" <%=Admin.Input_Checked(Site_IsContentCommentCheck,0)%> /><label><%=Lang_UnNeedCheck%></label>
                  <input type="radio" name="Site_IsContentCommentCheck" value="1" <%=Admin.Input_Checked(Site_IsContentCommentCheck,1)%>/>
                  <label><%=Lang_NeedCheck%></label>
                  </td>
              </tr>
              </tbody>
            </table>
            </div>
            
            <!--系统缓存-->
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
             <tbody>
              <tr><td class="titletd"><%=Lang_SystemSet(19)%></td>
                  <td class="infotd">
                  <input type="radio" name="Site_IsCache" value="1" <%=Admin.Input_Checked(Site_IsCache,1)%> /><label><%=Lang_SystemSet(20)%></label>
                  <input type="radio" name="Site_IsCache" value="0" <%=Admin.Input_Checked(Site_IsCache,0)%> /><label><%=Lang_SystemSet(21)%></label>
                  
                  <span><a href="javascript:void(0);" onclick="ShowHelpMsg(400,160,'<% Echo Lang_SystemSet(19) %>',Js_HelpMsg[3])"><b class="icon icon_help"></b></a></span>
                  </td>
              </tr>
              <tr><td class="titletd"><%=Lang_SystemSet(22)%></td>
                  <td class="infotd">
                  <input type="text" class="TxtClass" name="Site_CacheTime" maxlength="12" style="width:100px;" value="<%=Site_CacheTime%>"/> <label class="Normal"><%=Lang_SystemSet(23)%></label>
                  </td>
              </tr>
              <tr style="display:none;"><td class="titletd"><%=Lang_SystemSet(24)%></td>
                  <td class="infotd">
                  <textarea class="remark" name="Site_FilterWords" id="Site_FilterWords"><%=Site_FilterWords%></textarea><br />
                  <label class="Normal"><%=Lang_SystemSet(25)%></label>
                  </td>
              </tr>
             </tbody>
            </table>
            </div>
            
            <!--调试模式-->
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
             <tbody>
             <tr><td class="titletd"><%=Lang_SystemSet(63)%></td>
                 <td class="infotd">
                 <input type="checkbox" id="Site_IsDeBug" name="Site_IsDeBug" <%=Admin.InputChecked(Site_IsDeBug)%> value="1"><%=Lang_SystemSet(64)%>
                 <label class="Normal"><%=Lang_SystemSet(65)%></label>
                 </td>
             </tr>
             <tr><td class="titletd"><%=Lang_SystemSet(62)%></td>
                  <td class="infotd">
                  <input type="radio" name="Site_FilePathType" value="relative" <%=Admin.Input_Checked(Site_FilePathType,"relative")%> /><label><%=Lang_TelativePath%></label>
                  <input type="radio" name="Site_FilePathType" value="absolute" <%=Admin.Input_Checked(Site_FilePathType,"absolute")%>/><label><%=Lang_AbsolutePath%></label>
                  </td>
              </tr>
            </table>
            </div>
            
            <!--网站公告-->
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
             <tbody>
             <tr><td class="titletd"><%=Lang_SystemSet(26)%></td>
                  <td class="infotd">
                  <textarea class="remark" name="Site_Notice" id="Site_Notice"><%=KnifeCMS.Data.HTMLDecode(Site_Notice)%></textarea><br />
                  <label class="Normal"><%=Lang_AllowHTML%></label>
                  </td>
              </tr>
            </table>
            </div>
            
        </div>
		<div id="Tabs_2" style="display:none;">
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
             <tbody>
               <tr><td class="titletd"><%=Lang_SystemSet(29)%></td>
                   <td class="infotd"><input type="text" class="TxtClass" id="Site_Name" name="Site_Name" style="width:360px;" value="<%=KnifeCMS.Data.HTMLDecode(Site_Name)%>"/></td>
               </tr>
               <tr><td class="titletd"><%=Lang_SystemSet(34)%><!--网站LOGO-->:</td>
                       <td class="infotd">
                       <div id="LogoBox" class="mb5"><% if not(KnifeCMS.Data.IsNul(Site_Logo)) Then Response.Write("<img src="""&Site_Logo&""" />") %></div>
                       <input type="text" class="TxtClass" id="Site_Logo" name="Site_Logo" onblur="Admin.ImageUrlChange('LogoBox','Site_Logo','','d_Site_Logo')" style="width:360px;" value="<%=Site_Logo%>" />
                       <span class="sysBtn"><a href="javascript:void(0)" onclick="UploadLogo('LogoBox','Site_Logo')"><b class="icon icon_add"></b><%=Lang_Btn_SelectPics%></a></span>
                       <label id="d_Site_Logo"></label></td>
               </tr>
               <tr><td class="titletd"><%=Lang_SystemSet(35)%><!--网站LOGO-->:</td>
                       <td class="infotd">
                       <div id="MyhomeLogoBox" class="mb5"><% if not(KnifeCMS.Data.IsNul(Site_MyhomeLogo)) Then Response.Write("<img src="""&Site_MyhomeLogo&""" />") %></div>
                       <input type="text" class="TxtClass" id="Site_MyhomeLogo" name="Site_MyhomeLogo" onblur="Admin.ImageUrlChange('MyhomeLogoBox','Site_MyhomeLogo','','d_Site_MyhomeLogo')" style="width:360px;" value="<%=Site_MyhomeLogo%>" />
                       <span class="sysBtn"><a href="javascript:void(0)" onclick="UploadLogo('MyhomeLogoBox','Site_MyhomeLogo')"><b class="icon icon_add"></b><%=Lang_Btn_SelectPics%></a></span>
                       <label id="d_Site_MyhomeLogo"></label></td>
               </tr>
               <!--<tr><td class="titletd"><%=Lang_SystemSet(36)%></td>
                  <td class="infotd">
				  <textarea class="remark" id="Site_BriefIntroduction" name="Site_BriefIntroduction"><%=KnifeCMS.Data.HTMLDecode(Site_BriefIntroduction)%></textarea>
                  </td>
               </tr>-->
               <tr><td class="titletd"><%=Lang_SystemSet(37)%></td>
                   <td class="infotd"><input type="text" class="TxtClass" id="Site_Icp" name="Site_Icp" value="<%=KnifeCMS.Data.HTMLDecode(Site_Icp)%>"/></td>
               </tr>
               <!--<tr><td class="titletd"><%=Lang_SystemSet(38)%></td>
                  <td class="infotd">
				  <textarea class="remark" id="Site_Copyright" name="Site_Copyright"><%=KnifeCMS.Data.HTMLDecode(Site_Copyright)%></textarea>
                  </td>
               </tr>-->
               <tr><td class="titletd"><%=Lang_SystemSet(27)%></td>
                      <td class="infotd"><textarea class="remark" name="Site_StatisticsCode" id="Site_StatisticsCode"><%=KnifeCMS.Data.HTMLDecode(Site_StatisticsCode)%></textarea></td>
               </tr>
             </tbody>
            </table>
            </div>
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
               <tbody>
               <tr><td class="titletd"><%=Lang_SystemSet(30)%></td>
                   <td class="infotd"><input type="text" class="TxtClass" id="Site_Name" name="Site_Title" maxlength="250" style="width:360px;" value="<%=KnifeCMS.Data.HTMLDecode(Site_Title)%>"/>
                   <label class="Normal"><%=Lang_SystemSet(31)%></label></td>
               </tr>
               
               <tr><td class="titletd"><%=Lang_MetaKeywords%><!--网站关键字-->:</td>
                   <td class="infotd"><input type="text" class="TxtClass" id="Site_MetaKeywords" name="Site_MetaKeywords" maxlength="250" style="width:360px;" value="<%=KnifeCMS.Data.HTMLDecode(Site_MetaKeywords)%>"/>
                   <label class="Normal"><%=Lang_SystemSet(32)%></label></td>
               </tr>
               <tr><td class="titletd"><%=Lang_MetaDescription%><!--网站描述-->:</td>
                   <td class="infotd"><textarea class="remark" name="Site_MetaDescription" id="Site_MetaDescription" maxlength="250"><%=KnifeCMS.Data.HTMLDecode(Site_MetaDescription)%></textarea>
                   <label class="Normal"><%=Lang_SystemSet(33)%></label></td>
               </tr>
               </tbody>
            </table>
            </div>
        </div>
        
        <div id="Tabs_3" style="display:none;">
            <div class="inbox">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                 <tbody>
                  <tr><td class="titletd"><%=Lang_SystemSet(14)%></td>
                      <td class="infotd">
                      <input type="radio" value="0" name="Site_GoodsConsultSet" <%=Admin.Input_Checked(Site_GoodsConsultSet,0)%> /><% Echo Lang_ConsultAllowUser %><!--只允许会员咨询-->
                      <input type="radio" value="1" name="Site_GoodsConsultSet" <%=Admin.Input_Checked(Site_GoodsConsultSet,1)%> /><% Echo Lang_ConsultForbid %><!--禁止咨询-->
                      <!--<input type="radio" value="2" name="Site_GoodsConsultSet" <%=Admin.Input_Checked(Site_GoodsConsultSet,2)%> /><% Echo Lang_ConsultFreely %>--><!--自由咨询-->
                      </td>
                  </tr>
                  <tr><td class="titletd"><%=Lang_SystemSet(15)%></td>
                      <td class="infotd">
                      <input type="radio" value="0" name="Site_GoodsCommentSet" <%=Admin.Input_Checked(Site_GoodsCommentSet,0)%> /><% Echo Lang_CommentAllowUser %><!--只允许会员评论-->
                      <input type="radio" value="1" name="Site_GoodsCommentSet" <%=Admin.Input_Checked(Site_GoodsCommentSet,1)%> /><% Echo Lang_CommentForbid %><!--禁止评论-->
                      <!--<input type="radio" value="2" name="Site_GoodsCommentSet" <%=Admin.Input_Checked(Site_GoodsCommentSet,2)%> /><% Echo Lang_CommentFreely %>--><!--自由评论-->
                      </td>
                  </tr>
                  <tr><td class="titletd"><%=Lang_SystemSet(16)%></td>
                      <td class="infotd">
                      <input type="radio" name="Site_IsGoodsConsultCheck" value="0" <%=Admin.Input_Checked(Site_IsGoodsConsultCheck,0)%> /><label><%=Lang_UnNeedCheck%></label>
                      <input type="radio" name="Site_IsGoodsConsultCheck" value="1" <%=Admin.Input_Checked(Site_IsGoodsConsultCheck,1)%>/><label><%=Lang_NeedCheck%></label>
                      </td>
                  </tr>
                  <tr><td class="titletd"><%=Lang_SystemSet(17)%></td>
                      <td class="infotd">
                      <input type="radio" name="Site_IsGoodsCommentCheck" value="0" <%=Admin.Input_Checked(Site_IsGoodsCommentCheck,0)%> /><label><%=Lang_UnNeedCheck%></label>
                      <input type="radio" name="Site_IsGoodsCommentCheck" value="1" <%=Admin.Input_Checked(Site_IsGoodsCommentCheck,1)%>/><label><%=Lang_NeedCheck%></label>
                      </td>
                  </tr>
                  <tr><td class="titletd"><%=Lang_SystemSet(46)%></td>
                      <td class="infotd">
                      <input type="radio" name="Site_IsTax" id="IsTax_1" value="1" <%=Admin.Input_Checked(Site_IsTax,1)%> /><label for="IsTax_1"><%=Lang_Yes%></label>
                      <input type="radio" name="Site_IsTax" id="IsTax_0" value="0" <%=Admin.Input_Checked(Site_IsTax,0)%> /><label for="IsTax_0"><%=Lang_No%></label>
                      </td>
                  </tr>
                  <tr><td class="titletd"><%=Lang_SystemSet(47)%></td>
                      <td class="infotd">
                      <input type="text" class="TxtClass" name="Site_TaxRate" style="width:60px;" value="<%=Site_TaxRate%>"/>% <label class="Normal"><%=Lang_SystemSet(48)%></label>
                      </td>
                  </tr>
                 </tbody>
                </table>
                </div>
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
               <tbody>
               <tr><td class="titletd"><%=Lang_SystemSet(58)%></td>
                   <td class="infotd"><input type="text" class="TxtClass" id="Shop_Title" name="Shop_Title" maxlength="250" value="<%=KnifeCMS.Data.HTMLDecode(Shop_Title)%>"/>
                   <label class="Normal"><%=Lang_SystemSet(59)%></label></td>
               </tr>
               
               <tr><td class="titletd"><%=Lang_MetaKeywords%><!--网站关键字-->:</td>
                   <td class="infotd"><input type="text" class="TxtClass" id="Shop_MetaKeywords" name="Shop_MetaKeywords" maxlength="250" style="width:360px;" value="<%=KnifeCMS.Data.HTMLDecode(Shop_MetaKeywords)%>"/>
                   <label class="Normal"><%=Lang_SystemSet(60)%></label></td>
               </tr>
               <tr><td class="titletd"><%=Lang_MetaDescription%><!--网站描述-->:</td>
                   <td class="infotd"><textarea class="remark" name="Shop_MetaDescription" id="Shop_MetaDescription" maxlength="250"><%=KnifeCMS.Data.HTMLDecode(Shop_MetaDescription)%></textarea>
                   <label class="Normal"><%=Lang_SystemSet(61)%></label></td>
               </tr>
               </tbody>
            </table>
            </div>
		</div>
        
        <div id="Tabs_4" style="display:none;">
            <div class="inbox">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                 <tbody>
                  <tr><td class="titletd"><%=Lang_SystemSet(66)%></td>
                      <td class="infotd">
                      <input type="text" class="TxtClass slong" name="Mail_smtp" value="<%=Mail_smtp%>"/>
                      </td>
                  </tr>
                  <tr><td class="titletd"><%=Lang_SystemSet(67)%></td>
                      <td class="infotd">
                      <input type="text" class="TxtClass slong" name="Mail_mailserver_username" value="<%=Mail_mailserver_username%>"/>
                      </td>
                  </tr>
                  <tr><td class="titletd"><%=Lang_SystemSet(68)%></td>
                      <td class="infotd">
                      <input type="text" class="TxtClass slong" name="Mail_mailserver_password" value="<%=Mail_mailserver_password%>"/>
                      </td>
                  </tr>
                  <tr><td class="titletd"><%=Lang_SystemSet(69)%></td>
                      <td class="infotd">
                      <input type="text" class="TxtClass slong" name="Mail_from" value="<%=Mail_from%>"/>
                      </td>
                  </tr>
                  <tr><td class="titletd"><%=Lang_SystemSet(70)%></td>
                      <td class="infotd">
                      <input type="text" class="TxtClass slong" name="Mail_fromname" value="<%=Mail_fromname%>"/>
                      </td>
                  </tr>
                 </tbody>
                </table>
                </div>
		</div>
        
		<div class="inbox" id="Tabs_5" style="display:none;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
             <tbody>
               <tr><td class="titletd"><%=Lang_SystemSet(49)%></td>
                   <td class="infotd">
                    <input type="radio" id="Site_Status0" name="Site_Status" <%=Admin.Input_Checked(Site_Status,0)%> value="0" /><label><%=Lang_CloseWebSite%></label>
                    <input type="radio" id="Site_Status1" name="Site_Status" <%=Admin.Input_Checked(Site_Status,1)%> value="1" /><label><%=Lang_OpenWebSite%></label>
                    </td>
              </tr>
              <tr><td class="titletd"><%=Lang_SystemSet(50)%></td>
                  <td class="infotd">
                  <textarea class="remark" name="Site_ClosedReasons" id="Site_ClosedReasons"><%=KnifeCMS.Data.HTMLDecode(Site_ClosedReasons)%></textarea>
                  </td>
              </tr>
             </tbody>
            </table>
		</div>
		<div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
		<button type="submit" class="sysSaveBtn" id="SaveBtn" name="SaveBtn" ><b class="icon icon_save"></b><%=Lang_Btn_Submit%></button>
		<button  type="reset" class="sysSaveBtn" id="ReSetBtn" name="ReSetBtn"><b class="icon icon_reset"></b><%=Lang_Btn_Reset%></button>
		</div>
        <%=Admin.CStatistics()%>
		</form>
	  </div>
	  </DIV>
	</DIV>
    <script type="text/javascript">
	function UploadLogo(LogoBox,LogoInput){
		var NewWindow;
		var TargetArea=document.getElementById(LogoBox);
		var Input=document.getElementById(LogoInput);
		if(TargetArea!=null && Input!=null){
			NewWindow=window.showModalDialog('fileupload.asp?ctl=file_image&act=view&insertnum=1&inserttype=url',window,'dialogWidth:'+fileManageWinWidth+'px;dialogHeight:'+fileManageWinHeight+'px;center:Yes;help:no;resizable:yes;scroll:yes;');
			if(NewWindow!=null){
				TargetArea.innerHTML="";
				var img=document.createElement("img");
				img.src=unescape(NewWindow);
				//img.className="BrandLogo";
				TargetArea.appendChild(img);
				Input.value=unescape(NewWindow);
				}
			}else{
				alert("Alert:UploadLogo(){ Can't Found Tag With id="+ LogoBox +" Or id="+ LogoInput +"! }");
		}
	}
    </script>
<%
	End Function
End Class
%>
