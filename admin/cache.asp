<!--#include file="config.admin.asp"-->
<%
Dim ClassCache
Set ClassCache = New Class_KnifeCMS_Admin_Cache
Set ClassCache = Nothing
Class Class_KnifeCMS_Admin_Cache
	
	Private Pv_Temp
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "cache.lang.asp"
		Header()
		If Ctl="cache" Then
			Select Case Action
				Case "view" : Call CacheView()
				Case "edit" : Call CacheEdit()
				Case "del"  : Call CacheDel()
				Case Else   : Admin.ShowMessage Lang_PraError,"?ctl="& Ctl &"&act=view",1
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"?ctl="& Ctl &"&act=view",1
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function CacheList(ByVal BV_Path)
		Dim Fn_String : Fn_String = KnifeCMS.GetForm("get","cachepath")
		Dim Fn_Path
		If KnifeCMS.Data.IsNul(Fn_String) Then 
			Fn_Path = KnifeCMS.Cache.CacheFilePath
		Else
			Fn_Path = Fn_String
		End If
		Call GetFiles(Fn_Path)
	End Function
	
	Private Function GetFiles(ByVal StrFilePath)
		If Trim(StrFilePath) = "" Or KnifeCMS.Data.IsNul(KnifeCMS.Data.ReplaceString(StrFilePath,"/","")) Then Exit Function
		If KnifeCMS.Cache.CacheFilePath <> Left(StrFilePath,Len(KnifeCMS.Cache.CacheFilePath)) Then Exit Function
		
		If KnifeCMS.IsFSOInstalled=False then
			ErrMsg=Lang_ObjFSONotInstalled:Admin.ShowError(0)
		End If

		Dim FilePath,Fn_FSO,Fn_Fold,FileItem,Fn_FileNum,Fn_i
		Dim Fn_Time_Day,Fn_Time_Hour,Fn_Time_Second,Fn_DateDiff
		Fn_FileNum=0
		Fn_i=0
		
		FilePath=StrFilePath
		FilePath = KnifeCMS.Data.RegReplace(FilePath,"[/]+","/")
		
		If Left(FilePath,1) = "/" Then FilePath = Mid(FilePath,2)
		If Right(FilePath,1) <> "/" Then FilePath = FilePath &"/"
		'**过滤禁止的文件夹字符
		FilePath=Replace(FilePath,"\","")
		FilePath=Replace(FilePath,":","")
		FilePath=Replace(FilePath,"*","")
		FilePath=Replace(FilePath,"?","")
		FilePath=Replace(FilePath,"""","")
		FilePath=Replace(FilePath,"|","")
		FilePath=Replace(FilePath,"<","")
		FilePath=Replace(FilePath,">","")
		
		Echo CreateFolderTagLine(FilePath)
		
		Set Fn_FSO = KnifeCMS.FSO.ObjectFSO
		If Fn_FSO.FolderExists(Server.MapPath(SystemPath & FilePath))=False Then
			Echo "ERROR_FILEPATH："& FilePath 
			Exit function
		End If
		Set Fn_Fold=Fn_FSO.getfolder(Server.MapPath(SystemPath&FilePath))
		Fn_FileNum=Fn_Fold.subfolders.Count + Fn_Fold.files.Count
		If int(Fn_FileNum) > 0 Then
			Echo "<ul class=""caches"">"
			for each FileItem in Fn_Fold.subfolders
				Fn_i=Fn_i+1
				Fn_Time_Day    = DateDiff("d",CDate(FileItem.datelastmodified),Now())
				Fn_Time_Hour   = DateDiff("h",CDate(FileItem.datelastmodified),Now())
				Fn_Time_Second = DateDiff("s",CDate(FileItem.datelastmodified),Now())
				Fn_DateDiff    = KnifeCMS.IIF(Fn_Time_Day>2,Fn_Time_Day & Lang_Cache(7),KnifeCMS.IIF(Fn_Time_Hour>1,Fn_Time_Hour & Lang_Cache(8),Fn_Time_Second & Lang_Cache(9)))
				
				Echo "<li class=""folder"" id=""folder_"& Fn_i &"""><input type=""checkbox"" name=""InputName"" value="""& StrFilePath & FileItem.name &""" /><span class=""icon""></span><div class=""info""><div class=""tline""><span class=""name""><a href=""cache.asp?ctl=cache&act=view&cachepath="& StrFilePath & FileItem.name &"/"" target=""_self"">"& FileItem.name &"</a></span><span class=""size"">" &KnifeCMS.Data.FormatCurrency(FileItem.size/1024) &"k</span></div><div class=""bline""><span class=""time"">"& FileItem.datelastmodified &"</span><span class=""datediff"">"& Fn_DateDiff &"</span></div></div></li>"
			next
			for each FileItem in Fn_Fold.files
				Fn_i=Fn_i+1
				Fn_Time_Day    = DateDiff("d",CDate(FileItem.datelastmodified),Now())
				Fn_Time_Hour   = DateDiff("h",CDate(FileItem.datelastmodified),Now())
				Fn_Time_Second = DateDiff("s",CDate(FileItem.datelastmodified),Now())
				Fn_DateDiff    = KnifeCMS.IIF(Fn_Time_Day>2,Fn_Time_Day & Lang_Cache(7),KnifeCMS.IIF(Fn_Time_Hour>1,Fn_Time_Hour & Lang_Cache(8),Fn_Time_Second & Lang_Cache(9)))
				Echo "<li class=""file"" id=""file_"& Fn_i &"""><input type=""checkbox"" name=""InputName"" value="""& StrFilePath & FileItem.name &""" /><span class=""icon""></span><div class=""info""><div class=""tline""><span class=""name"">"& FileItem.name &"</span><span class=""size"">"& KnifeCMS.Data.FormatCurrency(FileItem.size/1024) &"k</span></div><div class=""bline""><span class=""time"">"& FileItem.datelastmodified &"</span><span class=""datediff"">"& Fn_DateDiff &"</span></div></div></li>"
			next
			Echo "</ul>"
		Else
			Echo(Lang_Cache(6))
		End If
		set Fn_Fold=nothing
		set Fn_FSO=Nothing
	End Function
	
	Private Function CreateFolderTagLine(ByVal BV_Path)
		Dim Fn_Array,Fn_i,Fn_String,Fn_Folder,Fn_FolderPath
		Fn_Array = Split(BV_Path,"/")
		For Fn_i=0 To Ubound(Fn_Array)-1
			Fn_Folder     = Fn_Array(Fn_i)
			Fn_FolderPath = Fn_FolderPath & Fn_Folder &"/"
			If KnifeCMS.Data.IsNul(Fn_String) Then
				Fn_String = "<span class=""folderroot"">"& Lang_FilePath &"</span>"
			Else
				Fn_String = Fn_String &"<span class=""folder""><a href=""cache.asp?ctl=cache&act=view&cachepath="& Fn_FolderPath &""" target=""_self"">"& Fn_Folder &"</a></span><span class=""raquo"">/</span>"
			End If
		Next
		Fn_String = "<div class=""folderTagline"">"& Fn_String
		Fn_String = Fn_String &"</div>"
		CreateFolderTagLine = Fn_String
	End Function
	
	Private Function CacheUpdating()
		Server.ScriptTimeOut = 36000
		Pv_Temp = Ctl
		Dim Fn_Rs,Fn_DataArray,Fn_CateArray,Fn_Array,Fn_i,Fn_j,Fn_Result
		Dim Fn_CacheName,Fn_Url,Fn_ID,Fn_SeoUrl
		Dim Fn_System,Fn_SystemName
		Dim Fn_Temp
		
		Call Admin.CreateCache_Init()
		Print Lang_Cache(23)&"<br><br>"
		
		'首页/登陆/注册/购物车/在线留言/404错误页
		Call Admin.CreateStaticHTML_Index()
		Call Admin.CreateStaticHTML_Login("")
		Call Admin.CreateStaticHTML_Reg("")
		Call Admin.CreateStaticHTML_MyCart("")
		Call Admin.CreateStaticHTML_Guestbook()
		Call Admin.CreateStaticHTML_Error404()
		'内容系统
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT System,SystemName,SeoUrl FROM ["& TablePre &"ContentSubSystem] ORDER BY Orders ASC")
		If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then Fn_DataArray = Fn_Rs.GetRows() : Else Fn_DataArray = ""
		KnifeCMS.DB.CloseRs Fn_Rs
		If IsArray(Fn_DataArray) Then
			For Fn_i=0 To Ubound(Fn_DataArray,2)
				Fn_System     = Fn_DataArray(0,Fn_i)
				Fn_SystemName = Fn_DataArray(1,Fn_i)
				Fn_SeoUrl     = Fn_DataArray(2,Fn_i)
				
				Print KnifeCMS.Data.ReplaceString(Lang_Cache(11),"{tpl:systemname}",Fn_SystemName) &"<br>"
				'主页
				Call Admin.CreateStaticHTML_ContentSystem(Fn_System,False,0,0)
				
				'分类
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,SeoUrl FROM ["& TablePre &"Content_"& Fn_System &"_Class] WHERE Recycle=0 ORDER BY ID ASC")
				If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then Fn_CateArray = Fn_Rs.GetRows() : Else Fn_CateArray = ""
				KnifeCMS.DB.CloseRs Fn_Rs
				If IsArray(Fn_CateArray) Then
					For Fn_j=0 To Ubound(Fn_CateArray,2)
						Fn_ID        = Fn_CateArray(0,Fn_j)
						Fn_SeoUrl    = Fn_CateArray(1,Fn_j)
						Call Admin.CreateStaticHTML_ContentSystem(Fn_System,True,Fn_ID,0)
					Next
				End If
				
				'详细内容
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,SeoUrl,UseLinkUrl FROM ["& TablePre &"Content_"& Fn_System &"] WHERE Status<>0 AND Recycle=0 ORDER BY ID ASC")
				If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then Fn_Array = Fn_Rs.GetRows() : Else Fn_Array = ""
				KnifeCMS.DB.CloseRs Fn_Rs
				If IsArray(Fn_Array) Then
					For Fn_j=0 To Ubound(Fn_Array,2)
						Fn_ID        = Fn_Array(0,Fn_j)
						Fn_SeoUrl    = Fn_Array(1,Fn_j)
						If Not(KnifeCMS.Data.CLng(Fn_Array(2,Fn_j))>0) Then
							Call Admin.CreateStaticHTML_ContentSystem(Fn_System,False,Fn_ID,0)
						End If
					Next
				End If
				Print KnifeCMS.Data.ReplaceString(Lang_Cache(12),"{tpl:systemname}",Fn_SystemName)&"<br><br>"
			Next
		End If
		
		'投票系统
		If CloseModuleVote<>1 Then
			Print Lang_Cache(13)&"<br>"
			'主页
			Call Admin.CreateStaticHTML_Vote(False,0,0)
			'分类页
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,SeoUrl FROM ["& TablePre &"VoteClass] WHERE Recycle=0 ORDER BY ID ASC")
			If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then Fn_CateArray = Fn_Rs.GetRows() : Else Fn_CateArray = ""
			KnifeCMS.DB.CloseRs Fn_Rs
			If IsArray(Fn_CateArray) Then
				For Fn_j=0 To Ubound(Fn_CateArray,2)
					Fn_ID     = Fn_CateArray(0,Fn_j)
					Fn_SeoUrl = Fn_CateArray(1,Fn_j)
					Call Admin.CreateStaticHTML_Vote(True,Fn_ID,0)
				Next
			End If
			'详细页
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,SeoUrl FROM ["& TablePre &"Vote] WHERE Recycle=0 ORDER BY ID ASC")
			If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then Fn_Array = Fn_Rs.GetRows() : Else Fn_Array = ""
			KnifeCMS.DB.CloseRs Fn_Rs
			If IsArray(Fn_Array) Then
				For Fn_j=0 To Ubound(Fn_Array,2)
					Fn_ID     = Fn_Array(0,Fn_j)
					Fn_SeoUrl = Fn_Array(1,Fn_j)
					Call Admin.CreateStaticHTML_Vote(False,Fn_ID,0)
				Next
			End If
			Print Lang_Cache(14)&"<br><br>"
		End If
				
		
		'更新网店系统
		If CloseModuleShop<>1 Then
			Print Lang_Cache(15)&"<br>"
			'主页
			Call Admin.CreateStaticHTML_Shop(False,0,0)
			'商品分类页
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,SeoUrl FROM ["& TablePre &"GoodsClass] WHERE Recycle=0 ORDER BY ID ASC")
			If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then Fn_CateArray = Fn_Rs.GetRows() : Else Fn_CateArray = ""
			KnifeCMS.DB.CloseRs Fn_Rs
			If IsArray(Fn_CateArray) Then
				For Fn_j=0 To Ubound(Fn_CateArray,2)
					Fn_ID     = Fn_CateArray(0,Fn_j)
					Fn_SeoUrl = Fn_CateArray(1,Fn_j)
					Call Admin.CreateStaticHTML_Shop(True,Fn_ID,0)
				Next
			End If
			'商品详细页
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,SeoUrl FROM ["& TablePre &"Goods] WHERE Recycle=0 ORDER BY ID ASC")
			If Not(Fn_Rs.Bof AND Fn_Rs.Eof) Then Fn_Array = Fn_Rs.GetRows() : Else Fn_Array = ""
			KnifeCMS.DB.CloseRs Fn_Rs
			If IsArray(Fn_Array) Then
				For Fn_j=0 To Ubound(Fn_Array,2)
					Fn_ID     = Fn_Array(0,Fn_j)
					Fn_SeoUrl = Fn_Array(1,Fn_j)
					Call Admin.CreateStaticHTML_Shop(False,Fn_ID,0)
				Next
			End If
			Print Lang_Cache(16)&"<br><br>"
		End If
		
		Call Admin.CreateCache_Terminate()
		Msg       = Lang_UpdateCacheFinished
		Ctl       = Pv_Temp
		Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
	End Function
	
	Private Function CacheDelete()
		Dim Fn_ArrayPath : Fn_ArrayPath = KnifeCMS.GetForm("post","InputName")
		Dim Fn_Array : Fn_Array = Split(Fn_ArrayPath,",")
		Dim Fn_i,Fn_Path
		Dim Fn_IsFile
		For Fn_i=0 To Ubound(Fn_Array)
			Fn_Path = Trim(Fn_Array(Fn_i))
			Fn_IsFile = False
			If Right(Fn_Path,Len(FileSuffix)) = FileSuffix Then Fn_IsFile = True
			If KnifeCMS.Cache.CacheFilePath = Left(Fn_Path,Len(KnifeCMS.Cache.CacheFilePath)) Then
				If Fn_IsFile = True Then
					Result = KnifeCMS.FSO.DeleteFile(KnifeCMS.FSO.ABSPath(Fn_Path))
					If Result Then
						Msg    = Msg & Lang_Cache(2) &"["& KnifeCMS.Data.RegReplace(Fn_Path,KnifeCMS.Cache.CacheFilePath,"") &"]<br>"
					Else
						Msg    = Msg & Lang_Cache(3) &"["& KnifeCMS.Data.RegReplace(Fn_Path,KnifeCMS.Cache.CacheFilePath,"") &"]<br>"
					End If
				Else
					Result = KnifeCMS.FSO.DeleteFolder(KnifeCMS.FSO.ABSPath(Fn_Path))
					If Result Then
						Msg    = Msg & Lang_Cache(4) &"["& KnifeCMS.Data.RegReplace(Fn_Path,KnifeCMS.Cache.CacheFilePath,"") &"]<br>"
					Else
						Msg    = Msg & Lang_Cache(5) &"["& KnifeCMS.Data.RegReplace(Fn_Path,KnifeCMS.Cache.CacheFilePath,"") &"]<br>"
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
	End Function
	
	Private Function CacheView()
%>
<DIV id="MainFrame">
  <div class="tagLine"><% Echo NavigationLine %></div>
  <DIV class="cbody">
  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%=KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
    <div class="toparea">
      <table bDelivery="0" cellpadding="0" cellspacing="0">
        <tr>
          <td class="tdcell"><input name="SelectAll" type="checkbox" onClick="CheckAllCaches('listForm','SelectAll','InputName')" title="<% Echo Lang_DoSelectAll %>"><% Echo Lang_DoSelectAll %></td>
          <td class="tdcell"><button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_Btn_ClearCache %></button></td>
          <td class="tdcell"><span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=edit&url=<%=KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>" ><b class="icon icon_write"></b><% Echo Lang_Btn_UpdateCache %></a></span></td>
        </tr>
      </table>
    </div>
    <div class="wbox" id="caches">
      <input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" />
      <% Call CacheList("") %>
    </div>
  </form>
  </DIV>
</DIV>
<script type="text/javascript">
function CheckAllCaches(FormId,SelectAllName,CheckInputName){
	var inputs=document.getElementsByName(CheckInputName);
	if(inputs!=null){
		if(document.forms[FormId].elements[SelectAllName].checked == true){
			for (var i=0;i<inputs.length;i++){inputs[i].checked = true;}
		}else{
			for (var i=0;i<inputs.length;i++){inputs[i].checked = false;}
		}
	}
}
</script>
<%
	End Function
	Private Function CacheEdit()
%>
<script type="text/javascript">
KnifeCMS.BodyScroll();
</script>
<DIV id="MainFrame">
  <div class="tagLine"><% Echo NavigationLine %></div>
  <DIV class="cbody">
    <div class="toparea">
      <table bDelivery="0" cellpadding="0" cellspacing="0">
        <tr>
          <td class="tdcell"><input name="SelectAll" type="checkbox" onClick="" title="<% Echo Lang_DoSelectAll %>"><% Echo Lang_DoSelectAll %></td>
          <td class="tdcell"><button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_Btn_ClearCache %></button></td>
          <td class="tdcell"><span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=edit&url=<%=KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>" ><b class="icon icon_write"></b><% Echo Lang_Btn_UpdateCache %></a></span></td>
        </tr>
      </table>
    </div>
    <div class="wbox" id="caches">
    <%
		Call CacheUpdating()
	%>
    </div>
  </DIV>
</DIV>
<script type="text/javascript">
KnifeCMS.StopScroll=true;
</script>
<%
	End Function
	Private Function CacheDel()
		Call CacheDelete()
	End Function
End Class
%>
