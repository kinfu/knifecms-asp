<%
Public Function Header()
	HeadTitle = HeadTitle & "-"& Admin.SystemCtlName
	Dim Fn_HTML
	Fn_HTML = Fn_HTML &"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"&vbCrLf
	Fn_HTML = Fn_HTML &"<html xmlns=""http://www.w3.org/1999/xhtml"">"&vbCrLf
	Fn_HTML = Fn_HTML &"<meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" />"&vbCrLf
	Fn_HTML = Fn_HTML &"<head><title>"& HeadTitle &"</title>"&vbCrLf
	Fn_HTML = Fn_HTML &"<meta name=""generator"" content=""K"&"n"&"if"&"e"&"C"&"M"&"S "& Sys_Version &""" />"&vbCrLf
	Fn_HTML = Fn_HTML &"<meta name=""author"" content=""K"&"n"&"i"&"f"&"e"&"C"&"MS Team""  />"&vbCrLf
	Fn_HTML = Fn_HTML &"<meta http-equiv=""Expires"" CONTENT=""0"">"&vbCrLf
	Fn_HTML = Fn_HTML &"<meta http-equiv=""Cache-Control"" CONTENT=""no-cache"">"&vbCrLf
	Fn_HTML = Fn_HTML &"<meta http-equiv=""Pragma"" CONTENT=""no-cache"">"&vbCrLf
	Fn_HTML = Fn_HTML &"<link rel=""stylesheet"" type=""text/css"" href=""skin/default/css/style.css""/>"&vbCrLf
	Fn_HTML = Fn_HTML &"<script type=""text/javascript"" src=""../language/admin/"& Admin.Language &"/main.lang.js""></script>"&vbCrLf
	Fn_HTML = Fn_HTML &"<script type=""text/javascript"" src=""js/common.js""></script>"&vbCrLf
	Fn_HTML = Fn_HTML &"<script type=""text/javascript"" src=""../include/javascript/kfc.js""></script>"&vbCrLf
	Fn_HTML = Fn_HTML &"<script type=""text/javascript"" src=""js/admin.main.js""></script>"&vbCrLf
	Fn_HTML = Fn_HTML &"<script type=""text/javascript"" src=""../plugins/jquery/jquery.js""></script>"&vbCrLf
	Fn_HTML = Fn_HTML &"<script type=""text/javascript"">"&vbCrLf
	Fn_HTML = Fn_HTML &"var Lang_NoPermissions = """& Lang_NoPermissions &""";"&vbCrLf
	Fn_HTML = Fn_HTML &"</script>"&vbCrLf
	Fn_HTML = Fn_HTML &"<base target=""_self"" />"
	Fn_HTML = Fn_HTML &"</head><body>"
	Echo Fn_HTML
End Function
	
Public Function Footer()
	Dim Fn_HTML
	Fn_HTML = Fn_HTML &"<script type=""text/javascript"">Admin.Init()</script>"
	Fn_HTML = Fn_HTML &"<div class=""Bottom"" id=""systemruninfo""><div class=BotInfo>"
	Fn_HTML = Fn_HTML & PoweredBy &" "& CertificateLink &" Processed in "& KnifeCMS.Data.CheckSystemRuntime(FormatNumber(KnifeCMS.RunTime,5))&" second(s), "& KnifeCMS.DB.SqlQueryNum &" queries<br>"
	Fn_HTML = Fn_HTML &"</div></div>"&vbCrLf
	Fn_HTML = Fn_HTML &"</body></html>"
	Echo Fn_HTML
	Set KnifeCMS = Nothing
	Set Admin = Nothing
End Function

Function SelectFontColor(ByVal Sel_Name , ByVal Sel_Color)
	Dim tmpHTML
	Sel_Color=KnifeCMS.Data.Color(Sel_Color)
	Dim Colors : Colors=Array("#000000","#ffffff","#008000","#800000","#808000","#000080","#800080","#808080","#ffff00","#00ff00","#00ffff","#ff00ff","#ff0000","#0000ff","#008080")
	tmpHTML="<select id="""&Sel_Name&""" name="""&Sel_Name&""">"
	tmpHTML=tmpHTML&"<option value="""">"& Lang_DefaultColor &"</option>"
	For i=0 To Ubound(Colors)
		tmpHTML=tmpHTML&"<option style=""background:"&Trim(Colors(i))&";"" "
		If Sel_Color=Trim(Colors(i)) Then tmpHTML=tmpHTML&"selected=""selected"""
		tmpHTML=tmpHTML&"value="""&Trim(Colors(i))&"""></option>"
	Next
	tmpHTML=tmpHTML&"</select>"
	Response.Write tmpHTML
End Function

Function SelectFontType(ByVal Sel_Name , ByVal Sel_Type)
	Dim tmpHTML
	Sel_Type=KnifeCMS.Data.CLng(Sel_Type)
	Dim Types(2,3)
	Types(0,0)=1
	Types(0,1)=2
	Types(0,2)=3
	Types(1,0)=Lang_FontBold'"粗体"
	Types(1,1)=Lang_FontItalic'"斜体"
	Types(1,2)=Lang_FontBoldItalic'"粗+斜"
	tmpHTML="<select id="""&Sel_Name&""" name="""&Sel_Name&""">"
	tmpHTML=tmpHTML&"<option value=""0"">"& Lang_FontNormal &"</option>"
	For i=0 To 2
		tmpHTML=tmpHTML&"<option "
		If Sel_Type=Types(0,i) Then tmpHTML=tmpHTML&"selected=""selected"""
		tmpHTML=tmpHTML&"value="""&Types(0,i)&""">"&Types(1,i)&"</option>"
	Next
	tmpHTML=tmpHTML&"</select>"
	Response.Write tmpHTML
End Function

Function SelectPaginationType(ByVal Sel_Name , ByVal Sel_Type)
	Dim tmpHTML
	Sel_Type=KnifeCMS.Data.CLng(Sel_Type)
	Dim Types(2,3)
	Types(0,0)=0
	Types(0,1)=1
	Types(0,2)=2
	Types(1,0)=Lang_NoPagination'"不分页"
	Types(1,1)=Lang_PagingByHand'"手动分页"
	Types(1,2)=Lang_PagingAuto'"系统自动分页"
	tmpHTML="<select id="""&Sel_Name&""" name="""&Sel_Name&""">"
	For i=0 To 2
		tmpHTML=tmpHTML&"<option "
		If Sel_Type=Types(0,i) Then tmpHTML=tmpHTML&"selected=""selected"""
		tmpHTML=tmpHTML&"value="""&Types(0,i)&""">"&Types(1,i)&"</option>"
	Next
	tmpHTML=tmpHTML&"</select>"
	Response.Write tmpHTML
End Function

Function ClassObjectClose()
	On Error Resume Next
	'关闭前后台共用程序进程（注销所有类、注销所有对象）
	Call ProcessClose()
	'关闭后台独用程序进程（注销所有类、注销所有对象）
	Set Admin   = nothing
	If Err.Number <> 0 Then
		Err.Clear
	End If
End Function
%>
