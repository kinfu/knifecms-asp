<!--#include file="config.admin.asp"-->
<%
Dim Guestbook
Set Guestbook = New Class_KnifeCMS_Admin_Guestbook
Set Guestbook = Nothing
Class Class_KnifeCMS_Admin_Guestbook

	Private Pv_UserID,Pv_Username,Pv_GuestbookID,Pv_IP,Pv_Content
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "guestbook.lang.asp"
		IsContentExist = False
		Header()
		If Ctl="guestbook" Then
			Select Case Action
				Case "view"
					Call GuestbookGetSearchCondition()
					Call GuestbookView()
				Case "add"
					If SubAction="save" Then
						GuestbookDoSave
					Else
						Set Rs = KnifeCMS.DB.GetRecord(DBTable_Guestbook&":SysID,Content",Array("ID:"&ID),"")
						If Not(Rs.Eof) Then
							IsContentExist = True
							SysID         = Rs(0)
							Pv_Content    = Rs(1)
						End IF
						KnifeCMS.DB.CloseRs Rs
						If IsContentExist Then GuestbookDo : Else Admin.ShowMessage Lang_Guestbook_Cue(0),"goback",0
					End If
				Case "edit"
					GuestbookEdit
				Case "del"  : GuestbookDel
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"?ctl="& Ctl &"&act=view",1
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function GuestbookGetSearchCondition()
		Pv_UserID     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","userid"))
		If Pv_UserID  = 0 Then Pv_UserID=""
		Pv_Username   = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("both","username")),24)
		Pv_IP         = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","ip"),"[^0-9.]",""),15)
	End Function
	Private Function GuestbookSearchSql()
		Dim Fn_TempSql
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Pv_UserID <> ""    ," AND a.UserID="& Pv_UserID &" "," ")
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Pv_Username <>""   ," AND a.Username='"& Pv_Username &"' "," ")
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(Pv_IP <>""         ," AND a.IP='"& Pv_IP &"' "," ")
		GuestbookSearchSql = Fn_TempSql
	End Function
	
	Private Function GuestbookList()
		Dim Fn_TempSql,Fn_ColumnNum
			Fn_TempSql   = "SELECT a.ID,a.UserID,a.Username,b.HeadImg,a.Disabled,a.ChildNum,a.Content,a.Tel,a.Email,a.Website,a.AddTime,a.IP FROM ["& DBTable_GuestBook &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.UserID)"
			Fn_ColumnNum = 12
			Operation    = ""
			Fn_TempSql   = Fn_TempSql & " WHERE a.P_ID=0 "
			If SubAction = "search" Then
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.KeyWords <>""," AND a.Content like '%"& Admin.Search.KeyWords &"%' "," ")
				Select Case DB_Type
				Case 0
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.AddTime>=#"& Admin.Search.StartDate &"# ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.AddTime<=#"& Admin.Search.EndDate &"# ","")
				Case 1
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.AddTime>='"& Admin.Search.StartDate &"' ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.AddTime<='"& Admin.Search.EndDate &"' ","")
				End Select
				tempCondition = tempCondition & GuestbookSearchSql
				PageUrlPara = PageUrlPara & "&startdate="& Server.URLEncode(Admin.Search.StartDate) &"&enddate="& Server.URLEncode(Admin.Search.EndDate) &"&keywords="& Server.URLEncode(Admin.Search.KeyWords)&"&userid="& Pv_UserID &"&username="& Pv_Username &"&ip="& Pv_IP &""
				Fn_TempSql = Fn_TempSql & tempCondition
			End If
			Fn_TempSql = Fn_TempSql &" ORDER BY a.ID DESC"
			Operation = Operation &"<span class=""sysBtn""><a href=""javascript:;"" onClick=""javascript:ShowModal('?ctl="&Ctl&"&act=add&id={$ID}',780,460);ShowModalReload();""><b class=""icon icon_writemsg""></b>"& Lang_DoReply &"</a></span>"
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
	End Function
	
	Private Function GuestbookEdit()
		Dim Fn_Rs,Fn_ArrID,Fn_i,Fn_Disabled,Fn_Msg
		If SubAction="checked" Then 
			Fn_Disabled=0
			Fn_Msg = Lang_Guestbook_Cue(7)
		ElseIf SubAction="unchecked" Then 
			Fn_Disabled=1
			Fn_Msg = Lang_Guestbook_Cue(8)
		Else
			Admin.ShowMessage Lang_IllegalSysPra,"",0 : Exit Function
		End If
		ID = KnifeCMS.GetForm("post","InputName")
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Guestbook,Array("Disabled:"& Fn_Disabled),Array("ID:"&ID))
			If Result Then Msg = Msg & Fn_Msg &"[GuestbookID="& ID &"]<br>"
		Next
		Admin.ShowMessage Msg,Url,1
	End Function
	
	Private Function GuestbookDoSave()
		Dim Fn_Rs,Fn_Result,Fn_ID,Fn_ReplyContent,Fn_IP
		Fn_ReplyContent = KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(KnifeCMS.GetForm("post","replycontent")),1000)
		If KnifeCMS.Data.IsNul(Fn_ReplyContent) Then Admin.ShowMessage Lang_Guestbook_Cue(6),Array("javascript:WinClose();",Lang_WinClose),0 : Exit Function
		
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Guestbook,Array("ID:"& ID),"")
		If Not(Fn_Rs.Eof) Then
			IsContentExist = True
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		If IsContentExist<>True Then Admin.ShowMessage Lang_Guestbook_Cue(0),Array("javascript:WinClose();",Lang_WinClose),0 : Exit Function
		
		Fn_IP           = KnifeCMS.GetClientIP()
		Fn_Result = KnifeCMS.DB.AddRecord(DBTable_Guestbook,Array("SysID:"& SysID,"P_ID:"& ID,"ChildNum:0","Content:"& Fn_ReplyContent,"UserID:"& Admin.UserID,"Username:"& Admin.Username,"AddTime:"& SysTime,"IP:"& Fn_IP,"Disabled:0"))
		Call KnifeCMS.DB.UpdateRecord(DBTable_Guestbook,Array("ChildNum:ChildNum+1"),Array("ID:"&ID))
		Fn_ID = Admin.GetIDBySysID(DBTable_Guestbook,SysID)
		
		Msg = Lang_Guestbook_Cue(1) &"<br>[GuestbookReplyID:"& Fn_ID &"]<br>[P_ID:"& ID &"]"
		If Fn_Result Then Admin.ReloadTopDialog : Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
	End Function
	
	Private Function GuestbookDel()
		ID = KnifeCMS.GetForm("post","InputName")
		Dim Fn_Rs,Fn_ArrID,Fn_i,Fn_P_ID
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID   = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			If ID>0 Then
				Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_GuestBook &"] WHERE ID="& ID &" OR P_ID="& ID &"")
				If Result Then Msg = Msg & Lang_Guestbook_Cue(4) &"[ID="& ID &"]<br>"
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Function
	
	Private Function GuestbookView()
%>
	<%=Admin.Calendar(Array("","",""))%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
          <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:checked','<% Echo Lang_DoCheckedConfirm %>')"><b class="icon icon_checked"></b><% Echo Lang_DoChecked%></button>
                  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:unchecked','<% Echo Lang_DoUnCheckedConfirm %>')"><b class="icon icon_unchecked"></b><% Echo Lang_DoUnChecked%></button>
				  </td></tr>
			  </table>
		  </td>
          <td class="tdcell">
              <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
          <td class="tdcell">
            <table border="0" cellpadding="2" cellspacing="0">
            <tr><td class="upcell">
                <div class="align-center grey">
                    <% Echo Lang_DistrictDoSearch %><!--搜索操作区-->
                    <span class="AdvancedSearch"><a href="javascript:Admin.AdvancedSearch.Open(600,324,'AdvancedSearch')" ><% Echo Lang_AdvancedSearch %></a></span>
                </div>
                <DIV id="AdvancedSearch" style="display:none;">
                    <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                    <div class="diainbox">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                      <tr><td class="titletd"><% Echo Lang_Guestbook(1) %></td>
                          <td class="infotd"><input type="text" class="TxtSer" name="userid" style="width:220px;" value="<% Echo Pv_UserID %>" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_Guestbook(2) %></td>
                          <td class="infotd"><input type="text" class="TxtSer" name="username" style="width:220px;" value="<% Echo Pv_Username %>" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_Keywords %></td>
                          <td class="infotd"><input type="text" class="TxtClass" id="keywords" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:220px;" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_StartDate %>:</td>
                          <td class="infotd">
                          <input type="text" class="TxtSer dateStart" id="startdate" name="startdate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:220px;" value="<%=Admin.Search.StartDate%>" />
                          </td>
                      <tr><td class="titletd"><% Echo Lang_EndDate %>:</td>
                          <td class="infotd">
                          <input type="text" class="TxtSer dateEnd" id="enddate" name="enddate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:220px;" value="<%=Admin.Search.EndDate%>" />
                          </td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_Guestbook(3) %></td>
                          <td class="infotd"><input type="text" class="TxtSer" name="ip" id="ip" style="width:220px;" value="<% Echo Pv_IP %>" /></td>
                      </tr>
                      </table>
                    </div>
                    <% Echo AdvancedSearchBtnline %>
                    </form>
                </DIV>
                </td></tr>
            <tr><td class="downcell">
                <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                <table border="0" cellpadding="0" cellspacing="0"><tr>
                <td><span><% Echo Lang_Keywords %>:</span></td>
                <td class="pl3"><input type="text" class="TxtClass" name="keywords" id="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:150px;" /></td>
                <td class="pl3"><button type="submit" class="sysBtn" ><b class="icon icon_search"></b><% Echo Lang_Btn_Search %></button></td>
                </tr></table>
                </form>
            </td></tr>
            </table>
          </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
            <th width="1%" ><div style="width:50px; "><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
			<th width="5%" ><div style="width:54px; "><% Echo Lang_Guestbook_ListTableColLine(1) %><!--ID--></div></th>
            <th width="5%" ><div style="width:54px; "><% Echo Lang_Guestbook_ListTableColLine(2) %><!--昵称--></div></th>
            <th width="5%" ><div style="width:54px; "><% Echo Lang_Guestbook_ListTableColLine(3) %><!--头像--></div></th>
            <th width="4%" ><div style="width:54px; "><% Echo Lang_Guestbook_ListTableColLine(4) %><!--审核--></div></th>
            <th width="4%" ><div style="width:54px; "><% Echo Lang_Guestbook_ListTableColLine(5) %><!--回复--></div></th>
            <th width="40%"><div style="width:60px; "><% Echo Lang_Guestbook_ListTableColLine(6) %><!--内容--></div></th>
            <th width="10%"><div style="width:56px; "><% Echo Lang_Guestbook_ListTableColLine(7) %><!--联系电话--></div></th>
            <th width="10%"><div style="width:56px; "><% Echo Lang_Guestbook_ListTableColLine(8) %><!--Email--></div></th>
            <th width="10%"><div style="width:56px; "><% Echo Lang_Guestbook_ListTableColLine(13) %><!--Website--></div></th>
            <th width="10%"><div style="width:56px; "><% Echo Lang_Guestbook_ListTableColLine(9) %><!--时间--></div></th>
            <th width="10%"><div style="width:56px; "><% Echo Lang_Guestbook_ListTableColLine(10)%><!--IP--></div></th>
			<th width="1%" ><div style="width:66px; "><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% GuestbookList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
    <form name="FormForReload" style="display:none;" method="post"></form>
    <script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	if(objExist(document.getElementById("keywords"))){ReplaceText(document.getElementById("keywords").value,1)}
	function ChangeTdText(){
		var tds=document.getElementById("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			var GuestbookID,TempID;
			for(var i=0;i<tds.length-1;i++){
				GuestbookID = ""
				if(tds[i].id=="td3" && tds[i].innerHTML!=""){
					TempHTML = "<span class=\"sImg\"><a href=\"../?user/"+tds[i-2].innerHTML+"\" target=\"_blank\"><img src='"+ unescape(tds[i].innerHTML) +"' /></a></span>";
					tds[i].innerHTML=TempHTML;
				}else if(tds[i].id=="td4"){
					if(parseInt(tds[i].title)>0){
						tds[i].innerHTML = "<span class=\"red\">"+ Lang_Js[5] +"</span>";
					}else{
						tds[i].innerHTML = "<span class=\"green\">"+ Lang_Js[6] +"</span>";
					}
				}else if(tds[i].id=="td5"){
					if(parseInt(tds[i].title)>0){
						tds[i].innerHTML = "<span class=\"green\">"+ tds[i].title + Lang_Js[8] +"</span>";
					}else{
						tds[i].innerHTML = "<span class=\"red\">"+ Lang_Js[14] +"</span>";
					}
				}else if(tds[i].id=="td9" && tds[i].innerHTML!=""){
					tds[i].innerHTML=KnifeCMS.URLDecode(tds[i].innerHTML);
				}
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
	Function GuestbookDo()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
          <div class="inbox">
            <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable" id="Tabs_1">
              <tbody>
               <tr><td class="titletd"><%=Lang_Guestbook_ListTableColLine(6) %><!--内容-->:</td>
                   <td class="infotd">
                   <div><% Echo Pv_Content %></div>
                   <label id="d_BrandName"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_Guestbook_ListTableColLine(12) %><!--回复-->:</td>
                   <td class="infotd">
                   <style type="text/css">
				   .guestbookitemreply{ background:#ecf3ff; border:1px dotted #cddef8; color:#4b74b6; padding:10px;}
				   .guestbookitemreply .uinfo{ border-bottom:1px solid #e4ecf9; color:#6b95d8; padding:0px 0px 4px 0px;}
				   .guestbookitemreply .uinfo .name{ color:#6b95d8;}
				   .guestbookitemreply .uinfo .rep{ border:none; background:none; color:#6b95d8; margin:0px 0px 0px 8px;}
				   .guestbookitemreply .cttext{ color:#4b74b6; line-height:180%; margin:6px 0px 4px 0px;}
                   </style>
                   <div style="padding-bottom:2px;">
                   <%
				   Dim Fn_Content,Fn_AddTime
				   Set Rs = KnifeCMS.DB.GetRecord(DBTable_Guestbook&":ID,Content,UserID,Username,AddTime,IP",Array("P_ID:"&ID),"")
				   Do While Not(Rs.Eof)
					   Echo "<div gid="""& Rs(0) &""" class=""guestbookitemreply""><div class=""uinfo""><span class=""name"">"& Rs(3) &"</span><span class=""posttime"">"& Rs(4) &"</span></div><div class=""cttext"">"& Rs(1) &"</div></div>"
					   Rs.Movenext
				   Loop
				   KnifeCMS.DB.CloseRs Rs
				   %>
                   </div>
                   <textarea class="text" name="replycontent" id="replycontent" style="width:99%; height:106px;"></textarea>
                   <label id="d_replycontent"></label>
                   </td>
               </tr>
              </tbody>
            </table>
            <input type="hidden" name="id" value="<%=ID%>" />
            <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
            <div class="bottomSaveline" id="SubmitButtoms">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="if(!KnifeCMS.IsNull(KnifeCMS.$id('replycontent').value)){document.forms['SaveForm'].submit()}else{KnifeCMS.$id('replycontent').focus()}"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
            </div>
            </form>
          </div>
        </div>
      </DIV>
    </DIV>
<%
	End Function
End Class
%>