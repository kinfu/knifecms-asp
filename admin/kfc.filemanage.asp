<!--#include file="config.admin.asp"-->
<%
Dim Template
Set Template = New Class_KnifeCMS_Admin_Template
Set Template = Nothing
Class Class_KnifeCMS_Admin_Template

	Private Pv_RunFooter
	Private Pv_Result
	Private Pv_FilePath,Pv_Code,Pv_FileSuffix,Fn_TemplateName
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "file.lang.asp"
		Header()
		Pv_RunFooter = True
		If Ctl="file_image" Or Ctl="file_file" Or Ctl="file_flash" Or Ctl="file_music" Or Ctl="file_media" Then
			Select Case Action
				Case "view"
					Call FileManage()
				Case "check"
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"goback",0
		End If
		If Pv_RunFooter Then Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
    Function FileManage()
	Dim Fn_FolderPath
	Select Case Ctl
	Case "file_image" : Fn_FolderPath = "attachment/image"
	Case "file_file"  : Fn_FolderPath = "attachment/file"
	Case "file_flash" : Fn_FolderPath = "attachment/flash"
	Case "file_music" : Fn_FolderPath = "attachment/music"
	Case "file_media" : Fn_FolderPath = "attachment/media"
	End Select
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
      <div class="filemanage_body">
          <ul class="TabsStyle">
              <li class="a <%=KnifeCMS.IIF(Ctl="file_image","current","")%>"><a href="kfc.filemanage.asp?ctl=file_image&act=view" target="_self"><%=Lang_FileManage(6)%></a></li>
              <li class="a <%=KnifeCMS.IIF(Ctl="file_file","current","")%>"><a href="kfc.filemanage.asp?ctl=file_file&act=view" target="_self"><%=Lang_FileManage(7)%></a></li>
              <li class="a <%=KnifeCMS.IIF(Ctl="file_flash","current","")%>"><a href="kfc.filemanage.asp?ctl=file_flash&act=view" target="_self"><%=Lang_FileManage(8)%></a></li>
              <li class="a <%=KnifeCMS.IIF(Ctl="file_music","current","")%>"><a href="kfc.filemanage.asp?ctl=file_music&act=view" target="_self"><%=Lang_FileManage(9)%></a></li>
              <li class="a <%=KnifeCMS.IIF(Ctl="file_media","current","")%>"><a href="kfc.filemanage.asp?ctl=file_media&act=view" target="_self"><%=Lang_FileManage(10)%></a></li>
          </ul>
          <div class="filemanage clear" id="filemanage">
          <form>
              <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
              <td class="asider">
              <div class="foldertree" id="foldertree">
              </div>
              </td>
              <td class="section">
              <div class="container" id="filecontainer">
                  <div class="menuline"><ul class="listtype"><li class="current" listtype="list"><a href="javascript:;" class="list"></a></li><li listtype="thumb"><a href="javascript:;" class="thumb"></a></li></ul><span class="hide" id="selectall"></span>
				  <%
				  Call Admin.PrintPermissionMenu(Array(Ctl,"del"),"<a class=""fbutton fbtn_delete"" name=""delete""><span class=""ficon_delete""></span>"& Lang_DoDel &"</a>")
				  Call Admin.PrintPermissionMenu(Array(Ctl,"check"),"<a class=""fbutton fbtn_filecheck"" name=""filecheck""><span class=""ficon_check""></span>"& Lang_FileManage(4) &"</a>")
				  %>
                  </div>
                  <div class="dirpath"></div>
                  <div class="operationtip"></div>
                  <div class="filelist"></div>
                  <div class="pages"></div>
              </div>
              </td>
              </tr>
              </table>
          </form>
          </div>
          
      </div>
      </DIV>
    </DIV>
    <script type="text/javascript" src="js/filemanage.js"></script>
    <script type="text/javascript">
	var folderJsonData = '<%=KnifeCMS.FSO.FolderList("json",Fn_FolderPath)%>';
    $(document).ready(function(){
		$("#foldertree").folderTree({ctl:"<%=Ctl%>",data:folderJsonData,init:true});
		$("#filecontainer").fileManage({folderpath:"<%=Fn_FolderPath%>",ctl:"<%=Ctl%>"});
	});
    </script>
<%
    End Function
End Class
%>
