<!--#include file="config.admin.asp"-->
<%
Dim AdminMainPanel
Set AdminMainPanel = New Class_KnifeCMS_Admin_MainPanel
Set AdminMainPanel = Nothing
Class Class_KnifeCMS_Admin_MainPanel
    Private Pv_CPUs,Pv_CPU,Pv_OS,Pv_ObjectVersion,Pv_DirSize
	
	Private Sub Class_Initialize()
		IsContentExist = False
		Header()
		If Ctl="systeminfo" Then
			Select Case Action
				Case "view"
					If SubAction="componentcheck" Then
						ComponentCheck()
					ElseIf SubAction="filewriteablecheck" Then
						FileWriteableCheck()
						Echo Admin.UCSString()
					Else
						Call GetSysInfo()
						Call MainPanel()
					End If
				Case Else
					Admin.ShowMessage Lang_PraError,"goback",0
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"goback",0
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Function CacheReminds()
		Dim Fn_Html
		If IsCache=0 Then
			Fn_Html = "<div class=""remind_item"">"& Lang_SysReminds(2) &"</div>"
		End If
		CacheReminds = Fn_Html
	End Function

	'获取服务器常用参数
	Sub GetSysInfo()
		On Error Resume Next
		Dim Fn_FSO,Fn_Dir,Fn_WshShell,Fn_WshSysEnv
		Set Fn_FSO = KnifeCMS.FSO.ObjectFSO
		Set Fn_Dir = Fn_FSO.GetFolder(Server.MapPath(SystemPath))
		Set Fn_WshShell  = Server.CreateObject("WScript.Shell")
		Set Fn_WshSysEnv = Fn_WshShell.Environment("SYSTEM")
		Pv_OS   = Cstr(Fn_WshSysEnv("OS"))
		Pv_CPUs = Cstr(Fn_WshSysEnv("NUMBER_OF_PROCESSORS"))
		Pv_CPU  = Cstr(Fn_WshSysEnv("PROCESSOR_IDENTIFIER"))
		Pv_DirSize = KnifeCMS.Data.FormatSize(Fn_Dir.Size)
		if isempty(Pv_CPUs) then
		  Pv_CPUs = Request.ServerVariables("NUMBER_OF_PROCESSORS")
		end if
		if Pv_CPUs & "" = "" then Pv_CPUs = "("& Lang_Unknown &")"
		if Pv_OS & "" = "" then Pv_OS = "("& Lang_Unknown &")"
	End Sub
	
	Function OK()
		Echo "<span class=""cue_ok"">"& Lang_Support &"</span>"
	End Function
	
	Function Wrong()
		Echo "<span class=""cue_err"">"& Lang_UnSupport &"</span>"
	End Function
	
	Function Writeable()
		Echo "<span class=""cue_ok"">"& Lang_Writeable &"</span>"
	End Function
	Function UnWriteable()
		Echo "<span class=""cue_err"">"& Lang_UnWriteable &"</span>"
	End Function
	
	Sub MainPanel()
	%>
        <SCRIPT language="JScript" runat="server">
		function getJVer(){
		  //获取JScript 版本
		  return ScriptEngineMajorVersion() +"."+ScriptEngineMinorVersion()+"."+ ScriptEngineBuildVersion();
		}
		</SCRIPT>
        <DIV id="MainFrame">
          <div class="tagLine"><% echo NavigationLine %></div>
          <DIV class="cbody">
          <div class="wbox">
              <ul id="ContentTabs" class="TabsStyle">
                  <li class="a current"><a href="?ctl=systeminfo&act=view" target="_self"><%=Lang_Main(0)%></a></li>
                  <li class="a"><a href="?ctl=systeminfo&act=view&subact=componentcheck" target="_self"><%=Lang_Main(1)%></a></li>
                  <li class="a"><a href="?ctl=systeminfo&act=view&subact=filewriteablecheck" target="_self"><%=Lang_Main(2)%></a></li>
              </ul>
              
              <div class="desktop-grid">
                  <h3 class="title"><%=Lang_SysReminds(0)%></h3>
                  <div class="remindlist" id="remindlist">
                  <%=CacheReminds()%>
                  </div>
                  <script type="text/javascript">
				  var $remindlist = KnifeCMS.$id("remindlist");
                  if(trim($remindlist.innerHTML) == ""){
					  $remindlist.innerHTML = '<div class="none"><%=Lang_SysReminds(1)%></div>';
				  }
                  </script>
              </div>
              <div class="desktop-grid">
                  <h3 class="title"><%=Lang_SystemInfo%></h3>
                  <div class="Table">
                    <table width="100%" cellpadding="0" cellspacing="0">
                     <tbody>
                       <tr class="info">
                       <td width="100px" class="first"><%=Lang_SystemVersion%>:</td><td>
                       <div class="underline">
                       <%=Sys_Version%><span class="ml5">(<%=Sys_VersionTime%>)</span>
                       </div>
                       </td></tr>
                       <tr class="info"><td class="first"><%=Lang_ServerSystem%>:</td><td><%=Pv_OS%></td></tr>
                       <tr class="info"><td class="first"><%=Lang_ServerSoftware%>:</td><td><%=Request.ServerVariables("SERVER_SOFTWARE")%></td></tr>
                       <tr class="info"><td class="first"><%=Lang_ServerAddr%>:</td><td><%=Request.ServerVariables("SERVER_NAME")%>:<%=Request.ServerVariables("SERVER_PORT")%>(IP:<%=KnifeCMS.GetServerIP%>)</td></tr>
                       <tr class="info"><td class="first"><%=Lang_ServerSysTime%>:</td><td><%=SysTime%></td></tr>
                       <tr class="info"><td class="first"><%=Lang_ServerScriptTimeout%>:</td><td><%=Server.ScriptTimeout%> s</td></tr>
                       <tr class="info"><td class="first"><%=Lang_ServerScriptEngine%>:</td><td><%=ScriptEngine & "/"& ScriptEngineMajorVersion &"."&ScriptEngineMinorVersion&"."& ScriptEngineBuildVersion %> , <%="JScript/" & getjver()%></td></tr>
                       <tr class="info"><td class="first"><%=Lang_ServerCPUs%>:</td><td><%=Pv_CPUs%></td></tr>
                       <tr class="info"><td class="first"><%=Lang_DirSize%>:</td><td><%=Pv_DirSize%></td></tr>
                     </tbody>
                    </table>
                  </div>
              </div>
          </div>
          </DIV>
        </DIV>
        <script type="text/javascript">
		window.UEDITOR_HOME_URL = "<%=SystemPath%>plugins/ueditor/";
		</script>
		<script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_config.asp"></script>
		<script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_api.js"></script>
	<%
	End Sub
	Sub ComponentCheck()
	%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
	  <div class="wbox">
          <ul id="ContentTabs" class="TabsStyle">
              <li class="a"><a href="?ctl=systeminfo&act=view" target="_self"><%=Lang_Main(0)%></a></li>
              <li class="a current"><a href="?ctl=systeminfo&act=view&subact=componentcheck" target="_self"><%=Lang_Main(1)%></a></li>
              <li class="a"><a href="?ctl=systeminfo&act=view&subact=filewriteablecheck" target="_self"><%=Lang_Main(2)%></a></li>
          </ul>
          <div class="mt5">
              <table border="0" cellpadding="0" cellspacing="0" class="listTable">
              <tr class="top"><th><div style="width:320px;"><%=Lang_Object(0)%></div></th>
                              <th><div style="width:180px;"><%=Lang_Main(3)%></div></th>
                              <th style="width:80%;"><div style="width:180px;"><%=Lang_Main(4)%></div></th>
                              </tr>
              <tr class="data"><td><div class="p5">Adodb.Recordset</div></td>
                               <td><% if KnifeCMS.IsObjInstalled("Adodb.Recordset") Then OK : Else Wrong : End If %></td>
                               <td><%=Lang_Support%></td>
              </tr>
              <tr class="data"><td><div class="p5">Adodb.Connection<p class="exinfo">(<%=Lang_Object(1)%>)</p></div></td>
                               <td><% if KnifeCMS.IsObjInstalled("Adodb.Connection") Then OK : Else Wrong : End If %></td>
                               <td><%=Lang_Support%></td>
              </tr>
              <tr class="data"><td><div class="p5">Adodb.Stream<p class="exinfo">(<%=Lang_Object(2)%>)</p></div></td>
                               <td><% if KnifeCMS.IsObjInstalled("Adodb.Stream") Then OK : Else Wrong : End If %></td>
                               <td><%=Lang_Support%></td>
              </tr>
              <tr class="data"><td><div class="p5">Scripting.Dictionary<p class="exinfo">(<%=Lang_Object(3)%>)</p></div></td>
                               <td><% if KnifeCMS.IsObjInstalled("Scripting.Dictionary") Then OK : Else Wrong : End If %></td>
                               <td><%=Lang_Support%></td>
              </tr>
              <tr class="data"><td><div class="p5">Microsoft.XMLHTTP<p class="exinfo">(<%=Lang_Object(4)%>)</p></div></td>
                               <td><% if KnifeCMS.IsObjInstalled("Microsoft.XMLHTTP") Then OK : Else Wrong : End If %></td>
                               <td><%=Lang_Support%></td>
              </tr>
              <tr class="data"><td><div class="p5">Scripting.FileSystemObject<p class="exinfo">(<%=Lang_Object(5)%>)</p></div></td>
                               <td><% if KnifeCMS.IsObjInstalled("Scripting.FileSystemObject") Then OK : Else Wrong : End If %></td>
                               <td><%=Lang_Support%></td>
              </tr>
              </table>
          </div>
          <div class="mt5">
              <table border="0" cellpadding="0" cellspacing="0" class="listTable">
              <tr class="top"><th><div style="width:320px;"><%=Lang_Object(7)%></div></th>
                              <th><div style="width:180px;"><%=Lang_Main(3)%></div></th>
                              <th style="width:80%;"><div style="width:180px;"><%=Lang_Main(4)%></div></th>
                              </tr>
              <tr class="data"><td><div class="p5">JMail.SmtpMail<p class="exinfo">(<%=Lang_Object(6)%>)</p></div></td>
                               <td><% if KnifeCMS.IsObjInstalled("JMail.SmtpMail") Then OK : Else Wrong : End If %></td>
                               <td><%=Lang_SuggestSupport%></td>
              </tr>
              </table>
          </div>
	  </div>
	  </DIV>
	</DIV>
    <%
	End Sub
	Sub FileWriteableCheck()
	%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
	  <div class="wbox">
          <ul id="ContentTabs" class="TabsStyle">
              <li class="a"><a href="?ctl=systeminfo&act=view" target="_self"><%=Lang_Main(0)%></a></li>
              <li class="a"><a href="?ctl=systeminfo&act=view&subact=componentcheck" target="_self"><%=Lang_Main(1)%></a></li>
              <li class="a current"><a href="?ctl=systeminfo&act=view&subact=filewriteablecheck" target="_self"><%=Lang_Main(2)%></a></li>
          </ul>
          <div class="mt5">
          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
          <tr class="top"><th><div style="width:320px;"><%=Lang_Main(5)%></div></th>
                          <th><div style="width:180px;"><%=Lang_Main(3)%></div></th>
                          <th style="width:80%;"><div style="width:180px;"><%=Lang_Main(4)%></div></th>
                          </tr>
          <tr class="data"><td><div class="p5">attachment/</div></td>
                           <td><% if KnifeCMS.FSO.FolderWriteable(Server.MapPath("../attachment/")) Then Writeable : Else UnWriteable : End If %></td>
                           <td><%=Lang_Writeable%></td>
          </tr>
          <tr class="data">
                           <td><div class="p5">config/config.main.asp</div></td>
                           <td><% if KnifeCMS.FSO.FileWriteable(Server.MapPath("../config/config.main.asp")) Then Writeable : Else UnWriteable : End If %></td>
                           <td><%=Lang_Writeable%></td>
          </tr>
          <tr class="data"><td><div class="p5">data/</div></td>
                           <td><% if KnifeCMS.FSO.FolderWriteable(Server.MapPath("../data/")) Then Writeable : Else UnWriteable : End If %></td>
                           <td><%=Lang_Writeable%></td>
          </tr>
          <tr class="data"><td><div class="p5">data/backup/</div></td>
                           <td><% if KnifeCMS.FSO.FolderWriteable(Server.MapPath("../data/backup/")) Then Writeable : Else UnWriteable : End If %></td>
                           <td><%=Lang_Writeable%></td>
          </tr>
          <tr class="data"><td><div class="p5">data/cache/</div></td>
                           <td><% if KnifeCMS.FSO.FolderWriteable(Server.MapPath("../data/cache/")) Then Writeable : Else UnWriteable : End If %></td>
                           <td><%=Lang_Writeable%></td>
          </tr>
          <tr class="data"><td><div class="p5">data/javascript/region.json.js</div></td>
                           <td><% if KnifeCMS.FSO.FileWriteable(Server.MapPath("../data/javascript/region.json.js")) Then Writeable : Else UnWriteable : End If %></td>
                           <td><%=Lang_Writeable%></td>
          </tr>
          <tr class="data"><td><div class="p5">template/</div></td>
                           <td><% if KnifeCMS.FSO.FolderWriteable(Server.MapPath("../template/")) Then Writeable : Else UnWriteable : End If %></td>
                           <td><%=Lang_Writeable%></td>
          </tr>
          </table>
          </div>
	  </div>
	  </DIV>
	</DIV>
    <%
	End Sub 
End Class
%>
