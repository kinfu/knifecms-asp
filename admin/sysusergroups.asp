<!--#include file="config.admin.asp"-->
<%
Dim SysUserGroup
Set SysUserGroup = New Class_KnifeCMS_Admin_SysUserGroup
Set SysUserGroup = Nothing
Class Class_KnifeCMS_Admin_SysUserGroup

	Private Founder,GroupName,Degree,Remarks,Permission,Pv_IsFounder
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "sysuser.lang.asp"
		IsContentExist = False
		Pv_IsFounder  = 0
		Header()
		If Ctl="sysusergroup" Then
			Select Case Action
				Case "view" : SystemUserGroupsView
				Case "add"
					If SubAction="save" Then
						SystemUserGroupsDoSave
					Else
						SystemUserGroupsDo
					End If
				Case "edit"
					Call GetSystemUserGroupDetailData()
					If SubAction="save" Then
						If IsContentExist Then SystemUserGroupsDoSave : Else Admin.ShowMessage Lang_SysUserGroup(11),"goback",0
					Else
						If IsContentExist Then SystemUserGroupsDo : Else Admin.ShowMessage Lang_SysUserGroup(11),"goback",0
					End If
				Case "del" : SystemUserGroupsDel
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"goback",0
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function GetSystemUserGroupDetailData()
		Set Rs = KnifeCMS.DB.GetRecord(DBTable_SysUserGroup &":Founder,GroupName,Degree,Remarks",Array("ID:"&ID),"")
		If Not(Rs.Eof) Then
			IsContentExist = true
			Pv_IsFounder = KnifeCMS.Data.CLng(Rs(0))
			GroupName    = Rs(1)
			Degree       = Rs(2)
			Remarks      = Rs(3)
		Else
			IsContentExist = false
		End If
		KnifeCMS.DB.CloseRs Rs
	End Function
	
	Function SystemUserGroupsList()
		Sql="SELECT ID, Founder, GroupName, Degree, Remarks FROM "&TablePre&"SysUserGroup ORDER BY Degree DESC, ID ASC"
		Operation="<span class=""sysBtn""><a href=""?ctl="& Ctl &"&act=edit&id={$ID}"" ><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		Call Admin.DataList(Sql,20,5,Operation,"")
	End Function
	
	Function SystemUserGroupsDoSave()
		GroupName  = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","GroupName"),100)
		Degree     = KnifeCMS.Data.Left(KnifeCMS.Data.Int(KnifeCMS.GetForm("post","Degree")),4)
		Remarks    = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","Remarks"),250)
		Permission = KnifeCMS.GetForm("post","Permission")
		If GroupName = "" Or Degree ="" Or Degree < 0 Then 
			Admin.ShowMessage Lang_SysUserGroup(2),"goback",1 : Exit Function
		End If
		If Action = "add" Then
			Result = KnifeCMS.DB.AddRecord(DBTable_SysUserGroup,Array("SysID:"&SysID,"Founder:0","GroupName:"&GroupName,"Degree:"&Degree,"Remarks:"&Remarks))
			Msg    = Lang_SysUserGroup(0) &"{"& GroupName &"[SysID="& SysID &"]}"
		ElseIf Action = "edit" Then
			'创始人组不可编辑
			If KnifeCMS.Data.CLng(KnifeCMS.DB.GetFieldByField(DBTable_SysUserGroup,"Founder","ID",ID))=1 Then
				Result = KnifeCMS.DB.UpdateRecord(DBTable_SysUserGroup,Array("GroupName:"&GroupName,"Remarks:"&Remarks),Array("ID:"&ID))
			Else
				Result = KnifeCMS.DB.UpdateRecord(DBTable_SysUserGroup,Array("GroupName:"&GroupName,"Degree:"&Degree,"Remarks:"&Remarks),Array("ID:"&ID))
			End If
			Msg = Lang_SysUserGroup(1) &"{"& GroupName &"[ID="& ID &"]}"
		End If
		If Result=1 Then
			If Action="add" Or (Action="edit" And PV_IsFounder=0) Then
				If Action = "add" Then ID = KnifeCMS.Data.CLng(KnifeCMS.DB.GetFieldByField(DBTable_SysUserGroup,"ID","SysID",SysID))
				If ID>0 Then
					Dim ArrPms : ArrPms = Split(Permission,",")
					Dim Temp_ArrPms,Temp_SystemCtl,Temp_SystemAction
					Result = KnifeCMS.DB.DeleteRecord(DBTable_SysUserPermission,Array("GroupID:"&ID))
					For i=0 To Ubound(ArrPms)
						Temp_ArrPms       = Split(ArrPms(i),":")
						Temp_SystemCtl    = KnifeCMS.Data.Left(Temp_ArrPms(0),50)
						Temp_SystemAction = KnifeCMS.Data.Left(Temp_ArrPms(1),50)
						Result = KnifeCMS.DB.AddRecord(DBTable_SysUserPermission,Array("GroupID:"&ID,"SystemCtl:"&Temp_SystemCtl,"SystemAction:"&Temp_SystemAction,"IsAllow:1"))
					Next
				End If
			End If
			Admin.ShowMessage Msg,"sysusergroups.asp?ctl=sysusergroup&act=view",1
		End If
	End Function
	
	Function SystemUserGroupsDel()
		ID = KnifeCMS.GetForm("post","InputName")
		Dim ArrID : ArrID = Split(ID,",")
		For i=0 To Ubound(ArrID)
			ID = KnifeCMS.Data.Int(ArrID(i))
			Founder   = 0
			GroupName = ""
			Result    = 0
			If ID>0 Then
				Set Rs = KnifeCMS.DB.GetRecord(TablePre&"SysUserGroup:Founder,GroupName",Array("ID:"&ID),"")
				If Not(Rs.Eof) Then
					Founder = Rs(0)
					GroupName = Rs(1)
				End If
				KnifeCMS.DB.CloseRs Rs
				If Founder Then
					'不能删除创始人组
					Admin.ShowMessage Lang_SysUserGroup(4) &"{"& GroupName &"[ID="& ID &"]}",Url,1
				Else
					Result = KnifeCMS.DB.DeleteRecord(DBTable_SysUserPermission,Array("GroupID:"&ID))
					Result = KnifeCMS.DB.DeleteRecord(DBTable_SysUserGroup,Array("ID:"&ID))
				End If
			End If
			If Result And GroupName <> "" Then Admin.ShowMessage Lang_SysUserGroup(3) &"{"& GroupName &"[ID="& ID &"]}",Url,1
		Next
	End Function
	
    Function SystemUserGroupsView()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="toparea">
          <table border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td class="tdcell">
              <table border="0" cellpadding="2" cellspacing="0">
              <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
              <tr><td class="downcell">
                  <span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=add" ><b class="icon icon_usergroup_add"></b><% echo Lang_DoAdd %></a></span>
                  </td></tr>
              </table>
          </td>
          <td class="tdcell">
              <table border="0" cellpadding="2" cellspacing="0">
              <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
              <tr><td class="downcell">
                  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDel%></button>
                  </td></tr>
              </table>
          </td>
          </tr>
          </table>
        </div>
        <div class="tabledata">
          <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
           <tbody>
            <tr class="top">
            <th class="first"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /></th>
            <th><div><% Echo Lang_ListTableColLine_ID %></div></th><!--系统编号-->
            <th><div><% Echo Lang_SysUser_ColLine(0) %></div></th><!--特别组-->
            <th><div><% Echo Lang_SysUser_ColLine(1) %></div></th><!--管理员组名-->
            <th><div><% Echo Lang_SysUser_ColLine(2) %></div></th><!--等级 ( 数值越大等级越高 )-->
            <th><div><% Echo Lang_ListTableColLine_Remarks %></div></th><!--备注-->
            <th><div><% Echo Lang_ListTableColLine_Do %></div></th><!--操作-->
            </tr>
            <% SystemUserGroupsList %>
           </tbody>
          </table>
          </form>
        </div>
      </DIV>
    </DIV>
    <script type="text/javascript">
    TrBgChange("listForm","InputName","oliver","data","select");
    function ChangeTdText(){
        var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
        var content;
        if(tds!=null){
            for(var i=1;i<tds.length;i++){
                if(tds[i].id=="td1" && tds[i].innerHTML==1){
                    tds[i].innerHTML='√';
                }
                if(tds[i].id=="td1" && tds[i].innerHTML==0){
                    tds[i].innerHTML='';
                }
                if(tds[i].id=="td4"){
                    content=tds[i].innerHTML;
                    tds[i].innerHTML="<div class=\"height22 line-height22 overhid\">"+content+"</div>";
                }
            }
        }
    }
    ChangeTdText();
    </script>
<%
    End Function
    
    Function SystemUserGroupsDo()
%>
    <style type="text/css">body{ padding-bottom:37px;}</style>
    <DIV id="MainFrame">
      <div id="wrap">
        <div class="tagLine"><% echo NavigationLine %></div>
        <Div class="cbody">
            <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
                <div class="inbox">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                    <tbody>
                       <tr><td class="titletd"><span class="required">*</span><% Echo Lang_SysUserGroup(5) %></td>
                           <td class="infotd">
                           <INPUT type="text" class="TxtClass" name="GroupName" id="GroupName" maxLength="100" value="<% Echo GroupName %>" style="width:360px;"/>
                           <label class="Normal" id="d_GroupName"></label>
                           </td></tr>
                       <tr><td class="titletd"><% Echo Lang_SysUserGroup(6) %></td>
                           <td class="infotd">
                           <INPUT type="text" class="TxtClass" name="Degree" id="Degree" onfocus="KnifeCMS.onfocus(this,'int')" onblur="KnifeCMS.onblur(this,'int')" <% Echo KnifeCMS.IIF(Pv_IsFounder=1,"disabled=""disabled""","")%> maxLength="4" value="<% Echo Degree %>" style="width:160px;" />
                           <label class="Normal" id="d_email"><% Echo Lang_SysUserGroup(7) %></label>
                           </td></tr>
                       <tr><td class="titletd"><% Echo Lang_SysUserGroup(8) %></td>
                           <td class="infotd"><textarea class="remark" onfocus="KnifeCMS.onfocus(this,'maxLength:250')" onblur="KnifeCMS.onblur(this,'maxLength:250')" name="Remarks" id="Remarks"><% Echo Remarks %></textarea>
                           <label class="Normal"></label></td>
                       </tr>
                     </tbody>
                    </table>
                    <div class="permission_grid">
                        <div class="heading_grid">
                        <% Echo Lang_SysUserGroup(9) %>
                        <input type="checkbox" name="selectall" onclick="Admin.SysUser.SelectAllPermission(this)" <%=KnifeCMS.IIF(PV_IsFounder=1,"disabled=""disabled""","")%> value="1" /><%=Lang_DoSelectAll%>
                        </div>
                        <div class="permission_list_grid">
                          <%
                          Dim Rs2,TempStr,TempIsAllow,Permissions
                          Dim Fn_i
                              TempIsAllow=0
                          If Action = "edit" Then
                              Set Permissions = Server.CreateObject("Scripting.Dictionary")
                              Set Rs = KnifeCMS.DB.GetRecord(DBTable_SysUserPermission &":SystemCtl,SystemAction,IsAllow",Array("GroupID:"& ID),"")
                              Do While Not(Rs.Eof)
                                  Permissions(Trim(Rs(0))&":"&Trim(Rs(1))) = Rs(2)
                              Rs.Movenext()
                              Loop
                              KnifeCMS.DB.CloseRs Rs
                          End If
                          
                          Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT SystemCtl,SystemCtlName FROM ["& DBTable_SystemCtlAction &"] Group BY Orders,SystemCtl,SystemCtlName")
                          Do While Not(Rs.Eof)
                          Fn_i = Fn_i+1
                          %>
                          <div class="permission">
                              <div class="order"><% Echo "<span>#"& Fn_i &"</span>" %></div>
                              <div class="heading">
                              <input type="checkbox" class="ml5" first="1" effect="pms" ctl="<% Echo Trim(Rs(0)) %>" name="Pms_SystemCtl" id="<% Echo Trim(Rs(0)) %>" <%=KnifeCMS.IIF(PV_IsFounder=1,"disabled=""disabled""","")%> value="1" /><%=Trim(Rs(1))%>
                              </div>
                              <ul>
                               <%
                               Set Rs2 = KnifeCMS.DB.GetRecordBySQL("SELECT ID,SystemAction,SystemActionName FROM ["& DBTable_SystemCtlAction &"] WHERE SystemCtl='"& Trim(Rs(0)) &"'")
                               Do While Not(Rs2.Eof)
                                   TempIsAllow=0
                                   If Action = "edit" Then
                                       C_TempStr = Trim(Rs(0))&":"&Trim(Rs2(1))
                                       If Permissions.Exists(C_TempStr) Then TempIsAllow = KnifeCMS.Data.Int(Permissions(C_TempStr))
                                   End If
                                   TempStr = "<li id="""& Rs2("ID") &""">"
                                   TempStr = TempStr &"<input type=""checkbox"" pms_id="""& Rs2("ID") &""" first=""0"" effect=""pms"" ctl="""& Trim(Rs(0)) &""" name=""Permission"" "&KnifeCMS.IIF(TempIsAllow=1,"checked=""checked""","")&" "&KnifeCMS.IIF(PV_IsFounder=1,"disabled=""disabled""","")&" value="""& Trim(Rs(0)) &":"& Trim(Rs2(1)) &""" />"& Trim(Rs2(2)) &""
                                   TempStr = TempStr &"</li>"
                                   Echo TempStr
                               Rs2.Movenext()
                               Loop
                               KnifeCMS.DB.CloseRs Rs2
                               %>
                               </ul>
                          </div>
                          <%
                          Rs.Movenext()
                          Loop
                          KnifeCMS.DB.CloseRs Rs 
                          Set Permissions = Nothing
                          %>
                          </div>
                    </div>
                </div>
                <div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
                    <input type="hidden" name="id" value="<% Echo ID %>" />
                    <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
                    <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.SysUser.SaveGroupCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
                    <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
                </div>
            </form>
        </Div>
      </div>  
    </DIV>
    
    <script type="text/javascript">
	Admin.SysUser.PermissionInputInit();
    Admin.SysUser.PermissionSelectAll();
    </script>
<%
    End Function
End Class
%>
