
//*** 检测用户名 **//
function out_uname(){
	var obj=document.getElementById("d_uname");
	var str=document.getElementById("uname").value;
	var strLen=sl(document.getElementById("uname").value);
	var chk=true;
	var sta;
	if (strLen<4 || strLen>16){chk=false;}
	if (chk){
		obj.className="d_loading";
		obj.innerHTML='正在检测....';
		var username=escape(str);
		var rn=parseInt(Math.random()*1000);
		var usrurl="Inc/AjaxText.cs.asp?do=CheckUserName&sys=SysUser&text="+username+"&r="+rn;
		//document.write(usrurl);
		sta=getResponseText(usrurl);
		if(sta==false){
			chk=true;
			obj.className="d_ok";
			obj.innerHTML='该呢称未能检测，可以尝试使用。';
		}else{
			if(sta.indexOf("ok")!=-1){
				chk=true;
				obj.className="d_ok";
				obj.innerHTML='该呢称可以使用。';
			}else{
				chk=false;
				obj.className="d_err";
				obj.innerHTML='该呢称已经被注册。';
			}
		}
	}else{
		obj.className="d_err";
		obj.innerHTML="用户名不正确";
	}
	return chk;
}

//*** 检测Email **//
function out_Email(){
	var obj=document.getElementById("d_Email");
	var str=document.getElementById("Email").value;
	var chk=true;
	str=str.replace(/ /g,"")
	if (str==''|| !str.match(/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/ig)){chk=false}
	if (chk){
		obj.className="d_loading";
		obj.innerHTML='正在检测....';
		var userEmail=str;
		var rn=parseInt(Math.random()*1000);
		var usrurl="Inc/AjaxText.cs.asp?do=CheckUserEmail&sys=SysUser&text="+userEmail+"&r="+rn;
		sta=getResponseText(usrurl);
		if(sta==false){
			chk=true;
			obj.className="d_ok";
			obj.innerHTML='该邮件未能检测，可以尝试注册。';
		}else{
			if(sta.indexOf("ok")!=-1){
				chk=true;
				obj.className="d_ok";
				obj.innerHTML='可以使用该Email进行注册。';
			}else{
				chk=false;
				obj.className="d_err";
				obj.innerHTML='该邮件地址已在使用,请用另一个邮件地址。';
			}
		}
	}else{
		obj.className="d_err";
		obj.innerHTML="请输入正确的电子邮箱地址。";
	}
	return chk;
}

//*** 检测Email **//
function out_Email_noajax(){
	var obj=document.getElementById("d_Email");
	var str=document.getElementById("Email").value;
	var chk=true;
	obj.className="d_loading";
	obj.innerHTML='正在检测....';
	str=str.replace(/ /g,"")
	if (str==''|| !str.match(/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/ig)){chk=false}
	if (!chk){
		obj.className="d_err";
		obj.innerHTML="请输入正确的电子邮箱地址。";
	}
	else{
		obj.className="d_ok";
		obj.innerHTML='可以使用该Email进行注册';
	}
	return chk;
}

//*** 检测密码 **//
function out_upwd1(){
	var obj=document.getElementById("d_upwd1");
	var str=document.getElementById("upwd").value;
	var chk=true;
	if (str=='' || str.length<5 || str.length>16){chk=false;}
	if (chk){
		obj.className="d_ok";
		obj.innerHTML='密码已经输入。';
	}else{
		obj.className="d_err";
		obj.innerHTML="请输入5-16位字符，英文、数字的组合";
	}
	return chk;
}
//*** 检测第二次输入的密码 **//
function out_upwd2(){
	var obj=document.getElementById("d_upwd2");
	var str=document.getElementById("RePassword").value;
	var chk=true;
	if (str!=document.getElementById("upwd").value||str==''){chk=false;}
	if (chk){
		obj.className="d_ok";
		obj.innerHTML='';
	}else{
		obj.className="d_err";
		obj.innerHTML="两次输入的密码不一致";
	}
	return chk;
}
//*** 检测用户组 **//
function out_Degree(){
	var obj=document.getElementById("d_Degree");
	var str=document.getElementById("Degree").value;
	var chk=true;
	if (str==''){chk=false;}
	if (chk){
		obj.className="d_ok";
		obj.innerHTML="ok";
	}else{
		obj.className="d_err";
		obj.innerHTML="请选择一个用户组";
	}
	return chk;
}
/**/
function chk_reg(){
	var chk=true
	if (!out_upwd1()){chk=false}
	if (!out_upwd2()){chk=false}
	if (!out_Email()){chk=false}
	if (!out_uname()){chk=false}
	if (!out_Degree()){chk=false}
	if(chk){
	document.getElementById('regbotton').disabled="disabled";
	return(true);
	}
	return(false);
}
/** 编辑时调用 **/
function chk_editreg(){
	var chk=true
	if (document.getElementById("Password").value!=''){
		if(!out_upwd1()){chk=false}
		if(!out_upwd2()){chk=false}
	}
	if (!out_Degree()){chk=false}
	if(chk){
	document.getElementById('regbotton').disabled="disabled";
	return(true);
	}
	return(false);
}