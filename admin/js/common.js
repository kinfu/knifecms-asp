
var fileManageWinWidth  = 750;//ScreenWidth()-260;
var fileManageWinHeight = 550;//ScreenHeight()-200

//$:弹出窗口showModalDialog Windows
function ShowModal(Url,Width,Height){
	try{window.showModalDialog(Url,window,'dialogHeight:'+Height+'px; dialogWidth:'+Width+'px; edge:Raised; center:Yes; help:No; resizable:yes; status:yes; scroll:auto;');}
	catch(e){}
}
//$:调用ShowModal()后刷新父窗口
function ShowModalReload(){
	window.location.reload();
}
//$:关闭窗口
function WinClose(){
	window.close();
}
function CloseWithOpenNewWin(url){
	if(url!=null && url!=""){
		window.location.href=url;
	}else{
		alert("Error: OpenNewWin(url) url is null！");
	}
}

//$:过滤非字母字符
function RepalceNoLetter(str){
	var string=str;
	if(str!=""){string = str.replace(/[^a-zA-Z]*/g,"")}
	return string;
}
//$:过滤非数字字符
function RepalceNoInt(str){
	var string=str;
	if(str!=""){string = str.replace(/[^0-9]*/g,"")}
	return string;
}
//$:过滤非数字字符
function RepalceNoIntOrLetter(str){
	var string=str;
	if(str!=""){string = str.replace(/[^0-9a-zA-Z]*/g,"")}
	return string;
}
//$:是否是合法的密码字符串
function IsPassword(str){
	var Result=true;
	if(str!=null && str!=""){
		if(str.indexOf(";")>=0){Result=false}
		if(str.indexOf("=")>=0){Result=false}
		if(str.indexOf("&")>=0){Result=false}
		if(str.indexOf("<")>=0){Result=false}
		if(str.indexOf(">")>=0){Result=false}
		if(str.indexOf("%")>=0){Result=false}
		if(str.indexOf("#")>=0){Result=false}
		if(str.indexOf("/")>=0){Result=false}
		if(str.indexOf('\\')>0){Result=false}
		if(str.indexOf('"')>=0){Result=false}
		if(str.indexOf("'")>=0){Result=false}
	}
	return Result;
}

/***************************
/ table 换行变色 选中变色
/ TrBgChange("from区域id & name","input的name","奇数行背景样式","偶数行背景样式","选中栏的背景样式")
****************************/
function TrBgChange(){
	var args   = TrBgChange.arguments;
	var argc   = TrBgChange.arguments.length;
	var FormId = args[0];
	var trs=KnifeCMS.$id(FormId).getElementsByTagName("tr");
	if(argc==5){
		//TrBgChange("listForm","InputName","oliver","data","select");
		var CheckInputName = args[1];
		var OddClass       = args[2];
		var EvenClass      = args[3];
		var SelectClass    = args[4];
		var inputs=document.forms[FormId].elements[CheckInputName];//var inputs=KnifeCMS.$id(FormId).elements[CheckInputName];
		if(inputs!=null){
			for(var i=1;i<trs.length;i++){
				trs[i].className=(trs[i].sectionRowIndex%2==0)?OddClass:EvenClass;
				trs[i].onclick=function(){
					var ei=this.sectionRowIndex-1;
					if(inputs[ei]){
						if(inputs[ei].checked==true){
							this.className=SelectClass;
						}else{
							inputs[ei].checked==false;
							this.className=(this.sectionRowIndex%2==0)?OddClass:EvenClass;
						}
					}
				}
			}
		}
	}else{
		//TrBgChange("goodslist","oliver","data");
		var OddClass       = args[1];
		var EvenClass      = args[2];
		for(var i=1;i<trs.length;i++){
			trs[i].className=(trs[i].sectionRowIndex%2==0)?OddClass:EvenClass;
		}
	}
}

/***************************
/ 复选框 全部选中/取消
/ CheckAll("区域fromID","全选input的name","input的name","奇数行背景样式","偶数行背景样式","选中栏的背景样式");
****************************/
function CheckAll(FormId,SelectAllName,CheckInputName,OddClass,EvenClass,SelectClass){
	var trs=KnifeCMS.$id(FormId).getElementsByTagName('tr');
	//var inputs=document.forms[FormId].elements[CheckInputName];
	var inputs=document.getElementsByName(CheckInputName);
	//alert(inputs.length);
	if(inputs!=null){
		if(document.forms[FormId].elements[SelectAllName].checked == true){
			for (var i=0;i<inputs.length;i++)
			{
				inputs[i].checked = true;
				trs[i+1].className=SelectClass;
			}
		}
		else{
			for (var i=0;i<inputs.length;i++)
			{
				inputs[i].checked = false;
				trs[i+1].className=(trs[i+1].sectionRowIndex%2==0)?OddClass:EvenClass;
			 }
		}
	}
}
//$:执行操作确认
function Chk_idBatch(FormId,CheckInputName,Msg){
	var bCheck=false;
	//var inputs=document.forms[FormId].elements[CheckInputName];
	var inputs=document.getElementsByName(CheckInputName);
	if(inputs!=null){
		for (var i=0;i<inputs.length;i++){
			var e=inputs[i];
			if (e.checked==1){
				bCheck=true;
				break;
			 }
		}
		if(bCheck==false){
			alert(Lang_Js_Cue_HaveSelectNothing)
			return false;
		}else{
			//如果Msg为空时,表示不需要弹出确认对话框; 否则弹出确认对话框。
			if(Msg!=""){return confirm(Msg);}
			return true;
		}
	}
}
//$:点击按钮执行函数
function Operate(FormId,CheckInputName,Operation,Msg){
	if(Chk_idBatch(FormId,CheckInputName,Msg)==true){
		if( Operation.indexOf(":") > 0){
			document.forms[FormId].act.value=Operation.split(":")[0];
			document.forms[FormId].subact.value=Operation.split(":")[1];
		}else{
			document.forms[FormId].act.value=Operation;
		}
		document.forms[FormId].submit();
	}
}

/*
 $ 标签内容切换函数 TabSwitch()
   AreaId: 区域id
   TagName:    标签的类型,如div,li,a等
   idpre:  切换的各个内容区id号的前缀,如"content_1,content_2,content_3"的idpre为"content"
   id:     切换的各个内容区id号的后缀,如"content_1,content_2,content_3"的id分别为"1,2,3"
 */

function TabSwitch(AreaId,TagName,idpre,id){
	var divs;
	var lis=document.getElementById(AreaId).getElementsByTagName(TagName);
	for(var i=1;i<lis.length+1;i++){
		divs=document.getElementById(idpre+'_'+i);
		if(divs!=null){
			if(i==id){
				lis[i-1].className="current";
				divs.style.display="";
			}
			else{
				lis[i-1].className="";
				divs.style.display="none";
			}
		}
	}
}

//$:弹出帮助窗口
function ShowHelpMsg(Width,Height,DiaWindowTitle,text){
	var date = new Date();
	var DiaWindowID="Help_"+date.getHours()+"_"+date.getMinutes()+"_"+date.getSeconds()+"_"+date.getMilliseconds();
	//alert(DiaWindowID);
	if(DiaWindowID!=null){
		var Help=KnifeCMS.CreateDiaWindow(Width,Height,DiaWindowID,"["+Lang_Js_Help+"] "+DiaWindowTitle,"<div class=\"p10\"><div>"+text+"</div></div>",false);
	}else{
		alert("JS : Date object error, can not create DiaWindowID.(Error:var date = new Date())");
	}
}

/*
 $ 点击checkbox时显示/隐藏某区域
   ShowCheckbox("1:1",boxid) 表示当checkbox为checked时显示boxid
   ShowCheckbox("1:0",boxid) 表示当checkbox为checked时隐藏boxid
 */
function ShowCheckbox(this_,type,boxid){
	var div=document.getElementById(boxid);
	var fn_arr = type.split(":")
	if(objExist(div)){
		if(fn_arr[1]=="0"){
			if(this_.checked==true){div.style.display="none";}else{div.style.display="";}
		}
		else if(fn_arr[1]=="1"){
			if(this_.checked==true){div.style.display="";}else{div.style.display="none";}
		}
	}
}

/*
   作用:设置文本框为禁用
   Style:Style=0 表示当CheckBoxId的checked==true时,objId的disabled=false;
         Style=1 表示当CheckBoxId的checked==true时,objId的disabled=true;
		 Style=其他值 报错;
   CheckBoxId:控制主体的id
   objId:被控制主体的id;
           可以为多个对象(各个对象之间用","隔开),如objId="id1,id2,id3,id4 ......"
 */
function SetInputDisabled(Style,CheckBoxId,objId){
	var CheckBox;
	var Inputs=new Array();
	Inputs=objId.split(",");
	CheckBox=document.getElementById(CheckBoxId);
	if(CheckBox!=null){
		if(Style==1){
			if(CheckBox.checked==true){
				for(i=0;i<Inputs.length;i++){
					Input=document.getElementById(Inputs[i]);
					if(objExist(Input)){Input.disabled=true}
				}
			}else{
				for(i=0;i<Inputs.length;i++){
					Input=document.getElementById(Inputs[i]);
					if(objExist(Input)){Input.disabled=false}
				}
			}
		}else if(Style==0){
			if(CheckBox.checked==true){
				for(i=0;i<Inputs.length;i++){
					Input=document.getElementById(Inputs[i]);
					if(objExist(Input)){Input.disabled=false}
				}
			}else{
				for(i=0;i<Inputs.length;i++){
					Input=document.getElementById(Inputs[i]);
					if(objExist(Input)){Input.disabled=true}
				}
			}
		}else{
			alert("ShowHelpMsg(Style,id){Style is wrong!}");
		}		
	}
}

/*
   作用: 添加[幻灯片添加文本框]
   AreaId: 区域id
   num: 增加数量
 */
function AddSlidePic(AreaId,num){
	var Area;
	var Trs;
	var LastTrId;
	var newTr;
	Area=document.getElementById(AreaId);
	if(Area!=null){
		for(n=0;n<num;n++){
			var iid
			Trs=Area.rows.length;
			if(Trs==null||Trs==0){
				Trs=-1;
				LastTrId=0;
			}else{
				iid=Area.rows[Trs-1].id;
				LastTrId=iid.substring(4);
			}
			LastTrId++;
			newTr=Area.insertRow(Trs);
			//newTr.className="info";
			newTr.id="pic_"+LastTrId;
			//添加两列
			for (i=1;i<=2;i++){
				 var newCell=newTr.insertCell(-1);
				 if(i==1){
					 newCell.className="titletd";
					 newCell.innerHTML=Lang_Js_Image+LastTrId+":";
				 }else{
					 newCell.className="infotd";
					 newCell.innerHTML="<input type=\"text\" class=\"TxtClass long\" id=\"SlidesPic_"+LastTrId+"\" name=\"SlidesPic\"/> <input type=\"button\" class=\"sbutton\" name=\"btnBorder\" id=\"AddSlidesPic_"+LastTrId+"\" value=\""+Lang_Js_UploadImage+"\" onclick=\"UploadImg('1','SlidesPic_"+LastTrId+"')\" /><label class=\"Normal\"></label> <a href=\"javascript:void(0)\" onclick='DeleteCurrentRow(this)' class=\"reduce\"><img src=\"Image/reduce.gif\" /></a>";
				 }
			}
			Trs++;
			//RowOrder_Sort(AreaId,0);
		}
	}
}
/*
 $ 函数: DeleteCurrentRow(obj)
   作用: 删除当前行
   tbodyId: tbody的id
   obj: 当前对象，通常为this
 */
function DeleteCurrentRow(obj)
{
	//获取obj上一结点的上一结点:tr
	var tr=obj.parentNode.parentNode;
	var tbody=tr.parentNode;
	tbody.removeChild(tr);
}

/*
   作用: 添加[投票选项添加文本框]
   AreaId: 区域id
   num: 增加数量
 */
function AddVoteOptions(AreaId,num){
	var Area;
	var Trs;
	var LastTrId;
	var newTr;
	Area=document.getElementById(AreaId);
	if(Area!=null){
		for(n=0;n<num;n++){
			var iid
			Trs=Area.rows.length;
			if(Trs==null||Trs==0){
				Trs=-1;
				LastTrId=0;
			}else{
				iid=Area.rows[Trs-1].id;
				LastTrId=iid.substring(5);
			}
			LastTrId++;
			newTr=Area.insertRow(Trs);
			newTr.className="info";
			newTr.id="Vote_"+LastTrId;
			//添加两列
			for (i=1;i<=2;i++){
				 var newCell=newTr.insertCell(-1);
				 if(i==1){
					 newCell.className="titletd";
					 newCell.innerHTML=Lang_Js_Option+LastTrId+":";
				 }else{
					 newCell.className="infotd";
					 newCell.innerHTML="<input type=\"text\" class=\"TxtClass long\" id=\"VoteOption_"+LastTrId+"\" name=\"VoteOption\"/><a href=\"javascript:void(0)\" onclick='DeleteCurrentRow(this);ChangeVoteMode_More();' class=\"reduce\"><img src=\"Image/reduce.gif\" /></a><label class=\"Normal\"></label>"
				 }
			}
			Trs++;
			//RowOrder_Sort(AreaId,0);
			ChangeVoteMode_More();
		}
	}
}

function ChangeVoteMode_More(){
	var obj=document.getElementById("VoteMode_MoreArea");
	var VoteMode_More=document.getElementById("VoteMode_More");
	//var inputNas=document.forms["SaveForm"].elements["VoteOption"];
	var inputNas=document.getElementsByName("VoteOption");
	var SelectHTML;
	if(VoteMode_More.disabled==true){
		SelectHTML="<select id=\"VoteMode_More\" name=\"VoteMode_More\" disabled=\"disabled\" style=\"font-size:13px;\">";
	}else{
		SelectHTML="<select id=\"VoteMode_More\" name=\"VoteMode_More\" style=\"font-size:13px;\">";
	}
	for(var i=2 ; i<=inputNas.length ; i++){
		if(i!=inputNas.length){
			SelectHTML=SelectHTML+"<option value="+i+">"+replace(Lang_Js_MaxCanSelectNOption,"{tpl:num}",i)+"</option>";
		}else{
			SelectHTML=SelectHTML+"<option value="+i+" selected=\"selected\">"+replace(Lang_Js_MaxCanSelectNOption,"{tpl:num}",i)+"</option>";
		}
	}
	SelectHTML=SelectHTML+"</select>";
	obj.innerHTML=SelectHTML;
}

/*
 $ 函数: CollectionRowAdd(AreaId,Type)
   作用: 添加[采集器 栏目添加]
   参数: {AreaId} 区域id
         {Type}   增加类型
 */
function CollectionRowAdd(AreaId,Type){
	var Area;
	Area=document.getElementById(AreaId);
	if(Area!=null){
		var iid
		var Trs;
	    var newTr;
		Trs=Area.rows.length;
		if(Trs==null||Trs==0){Trs=-1;}
		newTr=Area.insertRow(Trs);
		newTr.className="info";
		var newCell=newTr.insertCell(-1);
		if(Type=="AddRepLink"){
			newCell.innerHTML="<input type=\"text\" class=\"TxtClass TwoColumn\" name=\"RepLink\" style=\"margin-right:9px;\"/>"+Lang_Js_ReplaceFor+"<!--替换为--><input type=\"text\" class=\"TxtClass TwoColumn\" name=\"RepLinkTo\" style=\"margin-left:9px;\"/><a href=\"#\" onclick='DeleteCurrentRow(this)' class=\"reduce\"><img src=\"Image/reduce.gif\" /></a>";
		}else if(Type=="AddHandUrl"){
			newCell.innerHTML="<input type=\"text\" class=\"TxtClass long\" name=\"HandUrl\" id=\"HandUrl\" /><a href=\"#\" onclick='DeleteCurrentRow(this)' class=\"reduce\"><img src=\"Image/reduce.gif\" /></a>";
		}else if(Type=="AddRepTitle"){
			newCell.innerHTML="<input type=\"text\" class=\"TxtClass TwoColumn\" name=\"RepTitle\" style=\"margin-right:9px;\"/>"+Lang_Js_ReplaceFor+"<!--替换为--><input type=\"text\" class=\"TxtClass TwoColumn\" name=\"RepTitleTo\" style=\"margin-left:9px;\"/><a href=\"#\" onclick='DeleteCurrentRow(this)' class=\"reduce\"><img src=\"Image/reduce.gif\" /></a>";
		}else if(Type=="AddRepThumb"){
			newCell.innerHTML="<input type=\"text\" class=\"TxtClass TwoColumn\" name=\"RepThumb\" style=\"margin-right:9px;\"/>"+Lang_Js_ReplaceFor+"<!--替换为--><input type=\"text\" class=\"TxtClass TwoColumn\" name=\"RepThumbTo\" style=\"margin-left:9px;\"/><a href=\"#\" onclick='DeleteCurrentRow(this)' class=\"reduce\"><img src=\"Image/reduce.gif\" /></a>";
		}else if(Type=="AddRepContent"){
			newCell.innerHTML="<tr class=\"info\"><td><input type=\"text\" class=\"TxtClass TwoColumn\" name=\"RepContent\" style=\"margin-right:9px;\"/>"+Lang_Js_ReplaceFor+"<!--替换为--><input type=\"text\" class=\"TxtClass TwoColumn\" name=\"RepContentTo\" style=\"margin-left:9px;\"/><a href=\"#\" onclick='DeleteCurrentRow(this)' class=\"reduce\"><img src=\"Image/reduce.gif\" /></a></td></tr>"
		}else if(Type=="AddRepPageUrl"){
			newCell.innerHTML="<input type=\"text\" class=\"TxtClass TwoColumn\" name=\"RepPageUrl\" style=\"margin-right:9px;\"/>"+Lang_Js_ReplaceFor+"<!--替换为--><input type=\"text\" class=\"TxtClass TwoColumn\" name=\"RepPageUrlTo\" style=\"margin-left:9px;\"/><a href=\"#\" onclick='DeleteCurrentRow(this)' class=\"reduce\"><img src=\"Image/reduce.gif\" /></a>";
		}
	}
}

/**$:显示/隐藏某区域 { Id:要显示/隐藏的区域Id, IsShow:1显示 0隐藏 }
     onclick="ShowOrHidden('PayMethod_1:PayMethod_2','1:0')"
*/
function ShowOrHidden(Id,IsShow){
	if(IsShow==1){IsShow="1"}else if(IsShow==0){IsShow="0"}
	var arrId=Id.split(":");
	var arrIsShow=IsShow.split(":");
	if(arrId.length!=arrIsShow.length){
		alert("ShowOrHidden(Id,IsShow){ Error! }");
	}else{
		for(var i=0;i<arrId.length;i++){
			var obj=document.getElementById(arrId[i]);
			if(obj!=null){if(parseInt(arrIsShow[i])){obj.style.display="";}else{obj.style.display="none";}}
		}
	}
}

/**
 $ 函数: ReplaceText(Str,Type)
   作用: 替换内容
   参数: {Str} 要替换的内容
         {Type} 替换方式;1:表示内容变成红色
   @author phenex 2009-08-03 19:11
 */
function ReplaceText(Str,Type){
	if(Str!=""){
		if(isIE()){
			var Key = Str.split('|');
			for(var i=0;i<Key.length;i++){
				var Rng = document.body.createTextRange();
				while(Rng.findText(Str)){
					if(Type==1){ Rng.pasteHTML('<font style=\"color:#FD1A02\">' + Rng.text + '</font>');}
				}
			}
		}
	}
}

/*
 $ 函数: ButDisable(ButID)
   作用: 提交内容时设置提交和重置按钮为disable
   参数: {ButID} 按钮ID , 若多个按钮则以|分隔
   @author phenex 2009-09-19 10:56
 */
function ButDisable(ButID){
	if(ButID!=""){
		var Key = ButID.split('|');
		for(var i=0;i<Key.length;i++){
			var Rng = document.getElementById(Key[i]);
			if(Rng!=""){Rng.disabled=true;}
		}
	}
}

//$选择文件
function UploadFile(Num,TargetID){
	var NewWindow;
	var TargetArea=document.getElementById(TargetID);
	if(!isNaN(Num)){
		if(TargetArea!=null){
			NewWindow=window.showModalDialog('fileupload.asp?ctl=file_file&act=view&insertnum='+Num+'&inserttype=url',window,'dialogWidth:740px;dialogHeight:550px;center:Yes;help:no;resizable:yes;scroll:yes;');
			if(NewWindow!=null){TargetArea.value=unescape(NewWindow);}//把返回值插入到TargetID
		}else{alert("Alert: TargetID["+TargetID+"] was not existing in UploadFile(Num,TargetID)!");}
	}else{alert("Alert:Num must be number in UploadFile(Num,TargetID)!");}
}

//$选择图片
function UploadImg(Num,TargetID){
	var NewWindow;
	var TargetArea=document.getElementById(TargetID);
	if(!isNaN(Num)){
		if(TargetArea!=null){
			NewWindow=window.showModalDialog('fileupload.asp?ctl=file_image&act=view&insertnum='+Num+'&inserttype=url',window,'dialogWidth:740px;dialogHeight:550px;center:Yes;help:no;resizable:yes;scroll:yes;');
			if(NewWindow!=null){TargetArea.value=unescape(NewWindow);}//把返回值插入到TargetID
		}else{alert("Alert: TargetID["+TargetID+"] was not existing in UploadImg(Num,TargetID)!");}
	}else{alert("Alert:Num must be number in UploadImg(Num,TargetID)!");}
}

//$:商品/内容添加系列图片
function AddImgs(TargetID,ElementName){
	var NewWindow,Fn_Num;
	if(TargetID=="imgslist"){ Fn_Num = 999 }else{ Fn_Num = 1 }
	var TargetArea=document.getElementById(TargetID);
	if(TargetArea!=null){
		NewWindow=window.showModalDialog("fileupload.asp?ctl=file_image&act=view&insertnum="+Fn_Num+"&inserttype=url",window,"dialogWidth:740px;dialogHeight:550px;center:Yes;help:no;resizable:yes;scroll:yes;");
		if(NewWindow!=null){
			NewWindow=NewWindow.split(",");
			for(var i=0; i<NewWindow.length; i++){
				if(TargetID=="imgslist"){
					var li=document.createElement("li");
					li.setAttribute("title",Lang_Js_SetAsCover);
					if(TargetArea.getElementsByTagName("li").length<1){
						li.className="current";
					}
					li.innerHTML="<input type=\"hidden\" name=\""+ElementName+"\" value=\""+unescape(NewWindow[i])+"\" /><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td class=\"imgdiv\"><a href=\"javascript:;\" target=\"_self\"><img src="+unescape(NewWindow[i])+" /></a></td></tr></table><p><a onclick=\"deleteCurrentPic(this)\" href=\"javascript:;\" target=\"_self\"><b class=\"icon icon_del\" title=\""+Lang_Js_DelThisImage+"\"></b></a><a onclick=\"DiaWindowOpen_GetImgURL(this)\" title=\""+unescape(NewWindow[i])+"\" href=\"javascript:;\" target=\"_self\"><b class=\"icon icon_imglink\" title=\""+Lang_Js_GetImageUrl+"\"></b></a></p>";
					TargetArea.appendChild(li);
				}else if(TargetID=="Thumbnail"){
					var td=TargetArea.getElementsByTagName("td");
					td[0].className="";
					td[0].innerHTML="<input type=\"hidden\" name=\""+ElementName+"\" value=\""+unescape(NewWindow[i])+"\" /><img src=\""+unescape(NewWindow[i])+"\" />";
				}else{
					alert("Error: AddImgs(TargetID){ Can't Found Tag With id="+TargetID+" }");
				}
			}
		}
		selectFirstPic("imgslist",ElementName);
		setGoodsFirstImg("imgslist",ElementName);
	}
	else{
		alert("Alert: Can't find Tag with id="+TargetID+".");
	}
}
//$:点击图片则作为默认选中
function selectFirstPic(parentNodeID,ElementName){
	var ul=document.getElementById(parentNodeID);
	var img=ul.getElementsByTagName("img");
	var pli;
	for(var i=0; i<img.length; i++){
		img[i].onclick=function(){
			pli=this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
			var tImg=ul.getElementsByTagName("li");
			for(var j=0; j<tImg.length; j++){
				tImg[j].className="";
			}
			pli.className="current";
			var td=document.getElementById("bigimg").getElementsByTagName("td");
			td[0].className="";
			td[0].innerHTML="<span class=\"imgwrap\"><input type=\"hidden\" name=\""+ElementName+"FirstImg\" value=\""+this.src+"\" /><img src=\""+this.src+"\" /></span>";
		}
	}
}

//$:如果列表图片中没有选中的则设置第一张图片为默认选中
function setGoodsFirstImg(parentNodeID,ElementName){
	var lis=document.getElementById(parentNodeID).getElementsByTagName("li");
	var inputs=document.getElementById(parentNodeID).getElementsByTagName("input");
	if(inputs.length>0){
		var BigPic=document.getElementById("bigimg").getElementsByTagName("img");
		if(BigPic.length<1){
			var td=document.getElementById("bigimg").getElementsByTagName("td");
			lis[0].className="current";
			td[0].className="";
			td[0].innerHTML="<span class=\"imgwrap\"><input type=\"hidden\" name=\""+ElementName+"FirstImg\" value=\""+inputs[0].value+"\" /><img src=\""+inputs[0].value+"\" /></span>";
		}
	}
}

//$:获取图片路径
function DiaWindowOpen_GetImgURL(obj){
	var imgURL=obj.title;
	var text="<div class=\"padding10\">"
	text=text+"<div class=\"inbox\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"infoTable\">";
	text=text+"<tr><td class=\"titletd\" style=\"width:90px;\">"+ Lang_Js_ImageURL +"<!--图片路径-->:</td><td class=\"infotd\"><input type=\"text\" class=\"TxtClass long\" id=\"ImgURL\" name=\"ImgURL\" value=\""+imgURL+"\" /></td></tr>";
	text=text+"</table></div>";
	text=text+"</div>";
	var a=KnifeCMS.CreateDiaWindow(600,200,"GetImgURL",Lang_Js_ImageURL,text,true);
}

//$:删除当前图片(如果当前图片为选中状态则在删除后设置第一张图片为默认选中)
function deleteCurrentPic(obj){
	var li=obj.parentNode.parentNode;
	var ul=li.parentNode;
	ul.removeChild(li);
	if(li.className=="current"){
		var td=document.getElementById("bigimg").getElementsByTagName("td");
		td[0].innerHTML="";
	}
	setGoodsFirstImg("imgslist");
}

//$:设置提交重置等按钮状态为Disabled或是解除Disabled
function SetSubmitButtomsDisabled(Type,SubmitButtomsAreaId){
	var ButtomsArea=document.getElementById(SubmitButtomsAreaId);
	if(ButtomsArea!=null){
		var Buttoms=ButtomsArea.getElementsByTagName("button");
		if(Buttoms!=null){
			for(var i=0; i<Buttoms.length; i++){
				if(Type==true){ Buttoms[i].disabled=true;} else { Buttoms[i].disabled=false;}
			}
		}
	}
}

//选择已经存在的银行帐户
function SelectAccount(selectid,bankid,accountid){
	var Select = KnifeCMS.$id(selectid);
	var bank   = KnifeCMS.$id(bankid)
	var account= KnifeCMS.$id(accountid)
	if(objExist(Select)){
		Select.onchange = function(){
			if(objExist(bank)){bank.value    = Select.options[Select.selectedIndex].getAttribute("bank");}
			if(objExist(account)){account.value = Select.options[Select.selectedIndex].getAttribute("no");}
		}
	}
}