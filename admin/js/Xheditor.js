/**
*$ 调用编辑器
*/
function cwsEditor(width,height){
	var S_xheditorHeight=120;
	$(pageInit(width,height));
}
function pageInit(EditorWidth,EditorHeight)
{
	var editor=$('#TextContent').xheditor(true,{
		plugins:{
			InsertImgs:{c:'xhEdtBtnImage',t:'插入图片',e:function(){
				var _this=this;
				var jTest=window.showModalDialog('fileupload.asp?ctl=file_image&act=view&insertnum=999&inserttype=view',window,'dialogWidth:740px;dialogHeight:550px;center:Yes;help:no;resizable:yes;scroll:yes;');
				if(jTest!=null){
					_this.focus();
					_this.pasteHTML(jTest);
			}}},
			InsertFlash:{c:'xhEdtBtnFlash',t:'插入Flash',e:function(){
				var _this=this;
				var jTest=window.showModalDialog('fileupload.asp?ctl=file_flash&act=view&insertnum=999&inserttype=view',window,'dialogWidth:740px;dialogHeight:550px;center:Yes;help:no;resizable:yes;scroll:yes;');
				if(jTest!=null){
					_this.focus();
					_this.pasteHTML(jTest);
			}}},
			InsertMusic:{c:'xhEdtBtnMusic',t:'插入音乐',e:function(){
				var _this=this;
				var jTest=window.showModalDialog('fileupload.asp?ctl=file_music&act=view&insertnum=999&inserttype=view',window,'dialogWidth:740px;dialogHeight:550px;center:Yes;help:no;resizable:yes;scroll:yes;');
				if(jTest!=null){
					_this.focus();
					_this.pasteHTML(jTest);
			}}},
			InsertMedia:{c:'xhEdtBtnMedia',t:'插入视频',e:function(){
				var _this=this;
				var jTest=window.showModalDialog('fileupload.asp?ctl=file_media&act=view&insertnum=999&inserttype=view',window,'dialogWidth:740px;dialogHeight:550px;center:Yes;help:no;resizable:yes;scroll:yes;');
				if(jTest!=null){
					_this.focus();
					_this.pasteHTML(jTest);
			}}},
			InsertFile:{c:'xhEdtBtnFile',t:'插入文件',e:function(){
				var _this=this;
				var jTest=window.showModalDialog('fileupload.asp?ctl=file_file&act=view&insertnum=999&inserttype=view',window,'dialogWidth:740px;dialogHeight:550px;center:Yes;help:no;resizable:yes;scroll:yes;');
				if(jTest!=null){
					_this.focus();
					_this.pasteHTML(jTest);
			}}}
				},
		width:EditorWidth,
		height:EditorHeight,
		tools:'GStart,Cut,Copy,Paste,GEnd,Separator,GStart,Pastetext,Blocktag,Fontface,FontSize,Bold,Italic,Underline,Strikethrough,FontColor,BackColor,Removeformat,GEnd,Separator,GStart,Align,List,Outdent,Indent,Link,Unlink,GEnd,Separator,GStart,InsertImgs,InsertFlash,InsertMusic,InsertMedia,InsertFile,GEnd,Separator,GStart,Table,Source,Preview,GEnd'
		});
}