
var timer;

//本段JS实现网址后缀判断选择
var Rurl = location.search.substr(1);//获取地址参数
var urlquerys = Rurl.split("&");
var querysl,querysr,FileType,InsertNum,InsertType;
InsertNum=0;
if(urlquerys){
	for (var i=0; i<urlquerys.length; i++){
		querysl=urlquerys[i].substring(0,urlquerys[i].indexOf("="));
		querysr=urlquerys[i].substring(urlquerys[i].indexOf("=")+1);
		if( querysl == "ctl"){
			FileType=querysr.split("_")[1];
		}
		if( querysl == "insertnum"){
			InsertNum=querysr;
		}
		if( querysl == "inserttype"){
			InsertType=querysr;
		}
	}
}

/**
  $函数: InsertFile()
   作用: 插入文件
*/
function InsertFile(){
	var Files,FileUrl,FileUrls;
	var Filesbox=document.getElementById("SelectFiles");
	if(Filesbox!=null){
		if(FileType=="image"){
			Files=KnifeCMS.$id(Filesbox).getElementsByTagName('img');
			for(var i=0;i<Files.length;i++){
				if((InsertNum>0 && InsertNum>i)||InsertNum==0){
					if(InsertType!="url"){
						FileUrl="<img src=\""+Files[i].getAttribute("src")+"\"/>";
						if(i==0){FileUrls=FileUrl;}else{FileUrls=FileUrls+'<br>'+FileUrl;}
					}else
					{
						FileUrl=Files[i].getAttribute("src");
						if(i==0){FileUrls=FileUrl;}else{FileUrls=FileUrls+','+FileUrl;}
					}
				}
			}
		}
		else{
			Files=KnifeCMS.$id(Filesbox).getElementsByTagName('tr');
			for(var i=0;i<Files.length;i++){
				if((InsertNum>0 && InsertNum>i)||InsertNum==0){
					if(InsertType!="url"){
						if(FileType=="flash"){FileUrl="<embed type=\"application/x-shockwave-flash\" height=\"400\" width=\"480\" src=\""+Files[i].id+"\" loop=\"true\" play=\"true\" menu=\"false\" quality=\"high\" wmode=\"opaque\" classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-4445535400000\" />";}
						//else if(FileType=="media"){FileUrl="<embed type=\"application/x-mplayer2\" height=\"400\" width=\"480\" src=\""+Files[i].id+"\" autostart=\"false\" enablecontextmenu=\"false\" classid=\"clsid:6bf52a52-394a-11d3-b153-00c04f79faa6\" />";}
						else if(FileType=="media"){FileUrl="<embed type=\"application/x-shockwave-flash\" class=\"edui-faked-video\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" src=\""+Files[i].id+"\" wmode=\"transparent\" play=\"true\" loop=\"false\" menu=\"false\" allowscriptaccess=\"never\" allowfullscreen=\"true\" align=\"none\" height=\"400\" width=\"480\" />";}
						else if(FileType=="music"){FileUrl="<embed type=\"application/x-mplayer2\" class=\"edui-faked-music\" height=\"95\" width=\"400\" src=\""+Files[i].id+"\" autostart=\"false\" enablecontextmenu=\"false\" classid=\"clsid:6bf52a52-394a-11d3-b153-00c04f79faa6\" />";}
						//else if(FileType=="music"){FileUrl="<embed type=\"application/x-shockwave-flash\" class=\"edui-faked-music\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" src=\""+Files[i].id+"\" wmode=\"transparent\" play=\"true\" loop=\"false\" menu=\"false\" allowscriptaccess=\"never\" allowfullscreen=\"true\" align=\"none\" height=\"95\" width=\"400\" />";}
						else{FileUrl="<a href=\""+Files[i].id+"\" target=_balnk >点击下载</a>";}
						if(i==0){FileUrls=FileUrl;}else{FileUrls=FileUrls+'<br>'+FileUrl;}
					}else{
						FileUrl=escape(Files[i].id);
						if(i==0){FileUrls=FileUrl;}else{FileUrls=FileUrls+','+FileUrl;}
					}
				}
			}
		}
	}
	parent.window.returnValue=FileUrls;
	WinClose();
}

/*
 $ 插入文件到右边"准备插入的文件"区 InsertFileTo(AreaID,FileUrl)
   AreaID: 右边"准备插入的文件"区的 ID
   FileUrl: 文件路径
 */
function InsertFileTo(AreaID,FileUrl){
	var Area,iid,Trs,newTr;
	var strFileUrl,FileName;
	strFileUrl=FileUrl;
	Area=document.getElementById(AreaID);
	if(Area!=null&&FileUrl!=null)
	{
		Trs=Area.rows.length;
		if(Trs==null||Trs==0){Trs=-1;}
		newTr=Area.insertRow(Trs);
		newTr.id=FileUrl;
		var newCell=newTr.insertCell(-1);
		newCell.className="list";
		if(FileType=="image"){
			newCell.innerHTML="<div><img src=\""+ FileUrl +"\" /></div><a class=\"del\" onclick=\"DeleteCurrentRow(this)\" href=\"javascript:;\"></a>";
		}else{
			FileName=strFileUrl.substring(strFileUrl.lastIndexOf("/")+1);
			newCell.innerHTML="<div class=listn><p>"+ FileName +"</p></div><a class=del onclick=\"DeleteCurrentRow(this)\" href=\"javascript:;\"></a>";
		}
	}
}

/*
 $ 插入网络文件到右边"准备插入的文件"区 InsertNetUrls(InsertToAreaID)
   InsertToAreaID: 右边"准备插入的文件"区的 ID
 */
function InsertNetUrls(InsertToAreaID){
	var Area,inputs,FileUrl;
	Area=document.getElementById("onNetUrls");
	if(Area!=null){
		inputs=Area.getElementsByTagName("input")
		for(var i=0 ; i < inputs.length ; i++)
		{
			if(inputs[i].name=="imageURL"){
				FileUrl=inputs[i].value;
				if(FileUrl.length > 1){
					InsertFileTo(InsertToAreaID,FileUrl)
					inputs[i].value=""
				}
			}
		}
	}
}

/*
 $ Ajax取得文件列表 GetFiles(FileType,FilePath)
   FilePath: 文件路径 前面不要加"/", 以 "/"结束 如 attachment/image/201001/08/
 */
function GetFiles(FileType,FilePath){
	var obj=document.getElementById("Fileslist");
	var chk=true;
	var sta;
	if (FilePath=="/"){chk=false;}
	if (sl(FilePath)<1){chk=false;}
	if (chk){
		obj.innerHTML="";
		var LoadingBar=document.createElement("div");//创建一个加载进度条
		LoadingBar.className="loading_big";
		LoadingBar.id="ImgLoading";
		LoadingBar.innerHTML=Lang_Js_Cue_DataLoading;    //正在加载数据......
		obj.appendChild(LoadingBar);
		var ImgPath=escape(FilePath);
		var rn=parseInt(Math.random()*1000);
		var usrurl="ajax.getattachmentfiles.asp?ctl=file_"+FileType+"&act=view&filepath="+ImgPath+"&r="+rn;
		//window.open(usrurl);
		sta=getResponseText(usrurl);
		if(sta==false){
			chk=false;
			obj.innerHTML=Lang_Js_Cue_LoadingFail;    //数据加载失败
		}else{
			obj.innerHTML=sta;
			FolderSelect();
		}
	}else{
		obj.innerHTML="GetFiles(FileType,FilePath): FilePath is wrong!";
	}
}

/*
 $ 文件夹选择函数 FolderSelect()
 */
function FolderSelect(){
	var menu=document.getElementById("Folders");
	if(menu!=null){
		var menu_li=menu.getElementsByTagName("li");
		for(var i=0; i<menu_li.length; i++){
			menu_li[i].className="";
			menu_li[i].onclick=function(){
				var menu_list=menu.getElementsByTagName("li");
				for(var j=0; j<menu_list.length; j++){
					menu_list[j].className="";
				}
				this.className="select";
			}
		}
	}
}

/*
 $ 文件选择函数 FileSelect()
 */
function FileSelect(ID){
	var Area=document.getElementById("Files");
	var Files,FileUrl;
	if(Area!=null){
		var lis=Area.getElementsByTagName("li");
		for(var i=0; i<lis.length; i++){
			if(lis[i].id == "li_"+ID)
			{
				if(FileType == "image"){
					//Files=lis[i].getElementsByTagName("img");
					//FileUrl=Files[0].src;
					Files=lis[i].getElementsByTagName("input");
					FileUrl=Files[0].value;
					if(lis[i].className == "select")
					{
						lis[i].className=""
						var tr=document.getElementById(FileUrl);//把右边"准备插入的图片"区对应的图片删掉
						if(tr!=null){
						var tbody=tr.parentNode;
						tbody.removeChild(tr);
						}
					}
					else{
						lis[i].className="select"
						InsertFileTo("SelectFiles",FileUrl);//插入图片到右边"准备插入的图片"区
					}
				}
				else
				{
					Files=lis[i].getElementsByTagName("input");
					FileUrl=Files[0].value;
					if(lis[i].className == "select")
					{
						lis[i].className=""
						var tr=document.getElementById(FileUrl);//把右边"准备插入的文件"区对应的文件删掉
						if(tr!=null){
						var tbody=tr.parentNode;
						tbody.removeChild(tr);
						}
					}
					else{
						lis[i].className="select";
						InsertFileTo("SelectFiles",FileUrl);//插入文件到右边"准备插入的文件"区
					}
				}
			}
		}
	}
}