/*
 $ 顶部主导航栏选择切换函数 MainmenuSelect()
 */
function MainmenuSelect(id)
{
	var divs;
	var MmenuID="topmenu";
	var idpre="leftmenu_";
	var Mmenu=document.getElementById(MmenuID);
	var menus=Mmenu.getElementsByTagName("li");
	for(var i=1;i<menus.length+1;i++){
		divs=document.getElementById(idpre+i);
		if(i==id){
			menus[i-1].className="current";
			if(divs!=null){divs.style.display="block";}
			}
		else{
			menus[i-1].className="";
			if(divs!=null){divs.style.display="none";}
			}
	}
	var leftPanel=document.getElementById("leftPanel");
	if(leftPanel!=null)
	{
		leftPanel.style.display="";
		document.getElementById("showmenu").className="arrowleft";
		document.getElementById("show_leftmenu").style.left="130";
	}
}
/*
 $ 左边导航栏选择函数 menuSelect()
 */
function menuSelect(url){
	var menu=document.getElementById("leftPanel");
	var menu_li=menu.getElementsByTagName("li");
	for(var i=0; i<menu_li.length; i++){
		if(url!=null){
			if(url.indexOf(menu_li[i].childNodes[0].href)!=-1){
				menu_li[i].className="current";
			}else{
				menu_li[i].className="";
			}
		}else{
			menu_li[i].className="";
		}
		menu_li[i].onclick=function(){
			var menu_list=menu.getElementsByTagName("li");
			for(var j=0; j<menu_list.length; j++){
				menu_list[j].className="";
			}
			this.className="current";
		}
	}
}

/*
 $ 展开或者收缩 (多个对象之间用","隔开)
   调用方式: ExpandShrink(this,'SendGuestbook,MyGuestbookList'); SendGuestbook和MyGuestbookList表示ID名，E表示展开，S表示收缩
 */
function ExpandShrink(thisobj,id){
	var ids=id.split(",");
	if(thisobj.className=='shrink'){
		for(var i=0 ; i<ids.length ; i++){
			if(document.getElementById(ids[i])!=null)
			{
				document.getElementById(ids[i]).style.display="";
			}
		}
		thisobj.className="expand";
	}else{
		for(var i=0 ; i<ids.length ; i++){
			if(document.getElementById(ids[i])!=null)
			{
				document.getElementById(ids[i]).style.display="none";
			}
		}
		thisobj.className="shrink";
	}
}

/*
 $ 隐藏/显示左边区 showbox(type,boxid)
 */
function showleftmenu()
{
	var div,obj;
	div=document.getElementById("showmenu");
	obj=document.getElementById("show_leftmenu");
	if(div.className=="arrowleft")
	{
		document.getElementById("leftPanel").style.display="none";
		obj.style.left="0";
		div.className="arrowright";
	}
	else
	{
		document.getElementById("leftPanel").style.display="";
		obj.style.left="130";
		div.className="arrowleft";
	}
}

/*
 $ 隐藏/显示某区域 showbox(type,boxid)
 */
function showbox(type,boxid)
{
	var div;
	div=document.getElementById(boxid);
	if(div!=null)
	{
		if(type==0){
			div.style.display="none";
			}
		else{
			div.style.display="";
			}
	}
}

/*
 $ 检查iframe是否加载了自身
 */
function autoFixIfm(){
	var obj=frames['mainFrame'];
	var url,urlArr,urlLastpra;
	if(obj!=null){
		url=obj.location.href;
		url=url.toLowerCase()
		urlArr=url.split("/");
		urlLastpra=urlArr[urlArr.length-1];
		menuSelect(url);
		if(url.indexOf("index.asp")!=-1){
			obj.location.href="main.asp?ctl=systeminfo&act=view";
		}
	}
}