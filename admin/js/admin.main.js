function class_Admin(){
	this.SysUser        = new class_SysUser();
	this.AdvancedSearch = new class_AdvancedSearch(); //高级搜索
	this.Content        = new class_Content();        //内容系统
	this.Vote           = new class_Vote();           //投票系统
	this.Class          = new class_Class();          //分类系统
	this.Collection     = new class_Collection();     //采集系统
	this.Goods          = new class_Goods();          //商品系统
	this.Region         = new class_Region();         //地区
	this.Payment        = new class_Payment();        //支付方式
	this.Delivery       = new class_Delivery();       //配送方式
	this.Tree           = new class_Tree();           //地区树
	this.Navigator      = new class_Navigator();      //站点导航
	this.Ads            = new class_Ads();            //广告
	this.Comment        = new class_Comment();        //评论
	this.Order          = new class_Order();          //订单
	this.User           = new class_User();           //会员
	this.UC             = new class_UC();             //UC
	//$:检查是否已经选择
	this.isHaveSelect = function(SelectID,msgboxID,errMsg){
		var obj=document.getElementById(SelectID);
		var msgbox=document.getElementById(msgboxID);
		var Check=false;
		if(objExist(obj) && objExist(msgbox)){
			if(obj.value==0 || obj.length==""){
				Check=false;
				msgbox.className="d_err";
				msgbox.innerHTML=errMsg;
			}else{
				Check=true;
				msgbox.className="d_ok";
				msgbox.innerHTML="ok"
			}
		}
		return Check;
	}
	//$:检查是否已经选择
	this.isHaveSelectForNone = function(SelectID,msgboxID,errMsg){
		var obj=document.getElementById(SelectID);
		var msgbox=document.getElementById(msgboxID);
		var Check=false;
		if(objExist(obj) && objExist(msgbox)){
			if(!(parseInt(obj.value)>=0)){
				Check=false;
				msgbox.className="d_err";
				msgbox.innerHTML=errMsg;
			}else{
				Check=true;
				msgbox.className="d_ok";
				msgbox.innerHTML="ok"
			}
		}
		return Check;
	}
	//$:检查函数,objID的value为空则在msgboxID提示错误信息(errMsg)
	//if(!Admin.CheckItem("Title","d_Title",Lang_Js_ThisCannotEmpty)){Check=false}
	this.CheckItem = function(objID,msgboxID,errMsg){
		var inputs,obj,obj_ID = objID.split("|");
		var str="";
		var Check=true;
		var msgbox=document.getElementById(msgboxID);
		for(var i=0; i<obj_ID.length; i++){
			inputs=document.getElementsByName(obj_ID[i]);//检测是否有多个name相同的元素
			if(inputs.length>1){
			//多个name相同的表单,如radio/checkbox
				if(inputs[0].getAttribute("type")=="radio"|| inputs[0].getAttribute("type")=="checkbox"){
					for(var j=0; j<inputs.length; j++){
						if(inputs[j].checked==true){
							str="true";break;
						}
					}
				}else{
					for(var j=0; j<inputs.length; j++){
						obj=inputs[j];
						if(obj!=null){str=[str,trim(obj.value)].join('');}
					}
				}
			}else{
			//唯一的id
				obj=document.getElementById(obj_ID[i]);
				if(obj!=null){str = [str,trim(obj.value)].join('');}
			}
		}
		if(msgbox!=null){
			if(str==""){
				Check=false;
				msgbox.className="d_err";
				msgbox.innerHTML=errMsg;
			}else{
				Check=true;
				msgbox.className="d_ok";
				msgbox.innerHTML="ok"
			}
		}
		return Check;
	}
	/**$:正在加载数据......**/
	this.DataLoading = function(obj){
		var LoadingBar = document.createElement("span");
		LoadingBar.className = "loading_big";
		LoadingBar.id        = "ImgLoading";
		LoadingBar.innerHTML = Lang_Js_Cue_DataLoading;
		obj.appendChild(LoadingBar);
	}
	/**$:创建一个信息显示区(正在提交传输数据......)**/
	this.DataSubmiting = function(){
		var LoadingBar=document.createElement("span");
			LoadingBar.className="loading_big";
			LoadingBar.id="ImgLoading";
			LoadingBar.innerHTML='<font style="color:#136CBF;">'+Lang_Js_Cue_DataSubmiting+'</font>';
		var ButtomsArea=document.getElementById("SubmitButtoms");
			ButtomsArea.appendChild(LoadingBar);
		SetSubmitButtomsDisabled(true,"SubmitButtoms");
	}
	/**$:获取选择的相关投票/内容/商品等**/
	this.GetSelectItems = function(type,area,areaInputName,url){
		var url,NewWindow,arrDatai,arrDataj,newTr,newCell0,newCell1,newCell2,inputs;
		var check=true;
		var TargetArea=document.getElementById(area);
		if(url!=null && TargetArea!=null && areaInputName!=null && url!=null)
		{
			NewWindow=window.showModalDialog(url,window,'dialogWidth:940px; dialogHeight:600px; center:Yes; help:No; resizable:yes; status:yes; scroll:auto;');
			//alert("-"+NewWindow+"-");
			if(NewWindow!=null && NewWindow!="")
			{
				//NewWindow=NewWindow.substring(2);
				//NewWindow=NewWindow.substring(0,NewWindow.length-2);
				arrDatai=NewWindow.split(",");
				inputs=TargetArea.getElementsByTagName("input");
				for(var i=0; i<arrDatai.length; i++){
					var Trs=TargetArea.rows.length;
					arrDataj=arrDatai[i].split(":");
					for(var j=0; j<inputs.length; j++){
						//alert(inputs[j].value);
						if(inputs[j].value==unescape(arrDataj[0])){check=false; break;}
					}
					if(check){
						newTr=TargetArea.insertRow(Trs);
						newTr.className="data";
						newCell0=newTr.insertCell(-1);
						newCell0.innerHTML="<a onclick=\"DeleteCurrentRow(this)\" href=\"javascript:;\"><b class=\"icon icon_del\"></b></a>";
						newCell1=newTr.insertCell(-1);
						newCell1.innerHTML="<input type=\"hidden\" name=\""+areaInputName+"\" value=\""+unescape(arrDataj[0])+"\" />"+unescape(arrDataj[0]);
						newCell2=newTr.insertCell(-1);
						newCell2.innerHTML=unescape(arrDataj[1]);
					}
					check=true;
				}
			}
		}
	}
	/**$:向父窗口返回选择的(选择相关投票/内容/商品....)**/
	this.RebackSelectItems = function(InputName){
		var Data="";
		var tmpdata,ID,Name,tr,tds;
		var inputs=document.getElementsByName(InputName);
		if(objExist(inputs)){
			for (var i=0;i<inputs.length;i++){
				if(inputs[i].checked == true){
					tr=inputs[i].parentNode.parentNode;
					tds=tr.getElementsByTagName("td");
					ID=escape(inputs[i].value);
					Name=escape(tds[2].innerHTML);
					tmpdata=ID+":"+Name;
					if(Data==""){Data=tmpdata;}else{Data=Data+","+tmpdata;}
				}
			}
		}
		//alert(Data)
		window.returnValue=Data;
		WinClose();
	}
	//$:获取内容模型
	this.GetContentSubSystemOptions = function(){
		var ajaxString = ""
		var ajaxUrl    = "ajax.get.asp?ctl=navigator&act=add&subact=getcontentsubsystem&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl); //window.open(ajaxUrl);
		if(ajaxSta==false){
			div_MsgBox.className = "d_err";
			div_MsgBox.innerHTML = Lang_Js_DoDel+Lang_Js[9]+Lang_Js[10];
		}else{
			if(ajaxSta.toLowerCase().indexOf("error")!=-1 || ajaxSta.toLowerCase().indexOf("knifecms system message")!=-1){
				div_zoom.innerHTML = Lang_Js_Cue_LoadingFail + Lang_Js_Cue_CheckPermission;
			}else{
				regions = ajaxSta.substring(ajaxSta.indexOf(":")+2,ajaxSta.length-2);
			}
		}
	}
	//$:系统设置里 设置系统Logo 改变Logo url时随即改变图象
	this.ChangeImageUrl = function(ObjId,InputBoxId,ImageClassName,d_Msg){
		var obj=document.getElementById(ObjId);
		var Input=document.getElementById(InputBoxId);
		var d_ImageUrl=document.getElementById(d_Msg)
		if(objExist(obj) && objExist(Input)){
			obj.innerHTML = "";
			var img=document.createElement("img");
			if(Input.value==""){
				img.src="image/image.jpg";
			}else{
				img.src=Input.value;
			}
			img.className=ImageClassName;
			obj.appendChild(img);
			d_ImageUrl.className = "d_ok";
			d_ImageUrl.innerHTML = "ok";
		}
	}
	//$:系统设置里 设置系统Logo 改变Logo url时随即改变图象
	this.ImageUrlChange = function(ObjId,InputBoxId,ImageClassName,d_Msg){
		var obj=document.getElementById(ObjId);
		var Input=document.getElementById(InputBoxId);
		var d_ImageUrl=document.getElementById(d_Msg)
		if(objExist(obj) && objExist(Input)){
			obj.innerHTML = "";
			var img=document.createElement("img");
			img.src=Input.value;
			img.className=ImageClassName;
			obj.appendChild(img);
			d_ImageUrl.className = "d_ok";
			d_ImageUrl.innerHTML = "ok";
		}
	}
	this.AutoSeoTitle = function(Title,TempTitle,SeoTitle){
		if(SeoTitle.value==""){
			SeoTitle.value = Title.value;
		}else{
			if(SeoTitle.value==TempTitle.value){
				SeoTitle.value = Title.value;
			}
		}
		TempTitle.value = Title.value;
	}
	this.Init = function(){
		var $uc_ajax = KnifeCMS.$id("uc_ajax")
		if(objExist($uc_ajax)){Admin.UC.ucAjax();}
	}
}
function class_SysUser(){
	//添加编辑系统用户提交表单时进行检查
	this.SaveUserCheck = function(action){
		var Check=true;
		var cueObj=document.getElementById("d_RePassword")
		if(!Admin.CheckItem("Username","d_Username",Lang_Js_ThisCannotEmpty)){Check=false}
		if(!Admin.CheckItem("RealName","d_RealName",Lang_Js_ThisCannotEmpty)){Check=false}
		if(!Admin.CheckItem("Email","d_Email",Lang_Js_ThisCannotEmpty)){Check=false}
		if(!Admin.CheckItem("MemberID","d_MemberID",Lang_Js_ThisCannotEmpty)){Check=false}
		if(action=="add"){
			if(!Admin.CheckItem("Password","d_Password",Lang_Js_ThisCannotEmpty)){Check=false}
			if(!Admin.CheckItem("RePassword","d_RePassword",Lang_Js_ThisCannotEmpty)){Check=false}
		}
		if(!Admin.isHaveSelect("GroupID","d_GroupID",Lang_Js_HaveNotSelect)){Check=false}
		if(document.getElementById("Password").value != document.getElementById("RePassword").value){
			cueObj.className="d_err";
			cueObj.innerHTML=Lang_Js_PasswordNotAccord;
			Check=false;
		}
		if(Check){document.forms["SaveForm"].submit()}
	}
	//添加编辑系统用户组提交表单时进行检查
	this.SaveGroupCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("GroupName","d_GroupName",Lang_Js_ThisCannotEmpty)){Check=false}
		if(Check){document.forms["SaveForm"].submit()}
	}
	this.SelectAllPermission = function(_this){
		var allNode=document.getElementsByTagName("input");
		var input=[];
		for(var i=0; i<allNode.length; i++){
			if(allNode[i].getAttribute("effect")=="pms"){
				input.push(allNode[i]);	
			}
		}
		if(_this.checked==true){
			for(var i=0;i<input.length;i++){
				input[i].checked = true;
				if(parseInt(input[i].getAttribute("first"))==0){
				document.getElementById(input[i].getAttribute("pms_id")).className = "select";
				}
			}
		}else{
			for(var i=0;i<input.length;i++){
				input[i].checked = false;
				if(parseInt(input[i].getAttribute("first"))==0){
				document.getElementById(input[i].getAttribute("pms_id")).className = "notselect";
				}
			}
		}
	}
	this.PermissionInputInit = function(){
		var allNode=document.getElementsByTagName("input");
		var input=[];
		var same;
		var firstchecked;
		for(var i=0; i<allNode.length; i++){
			if(allNode[i].getAttribute("effect")=="pms"){
				input.push(allNode[i]);	
			}
		}
		for(var i=0; i<input.length; i++){
			if(parseInt(input[i].getAttribute("first"))==0){
				if(input[i].checked!=true){
					document.getElementById(input[i].getAttribute("pms_id")).className = "notselect";
				}else{
					document.getElementById(input[i].getAttribute("pms_id")).className = "select";
				}
				firstchecked = false;
				same=[];
				for(var j=0; j<allNode.length; j++){
					if(allNode[j].getAttribute("ctl")==input[i].getAttribute("ctl")){same.push(allNode[j]);}
				}
				for(var k=1; k<same.length; k++){
					if(same[k].checked==true){
						firstchecked = true;
					}
				}
				document.getElementById(input[i].getAttribute("ctl")).checked = firstchecked;
			}
		}
	}
	this.PermissionSelectAll = function (){
		var allNode=document.getElementsByTagName("input");
		var meAll=[];
		var same=[];
		var me_ctl;
		var me;
		var selectAllInput;
		for(var i=0; i<allNode.length; i++){
			if(allNode[i].getAttribute("effect")=="pms"){
				meAll.push(allNode[i]);	
			}
		}
		//alert(meAll.length);
		for(var i=0; i<meAll.length; i++){
			meAll[i].onclick = function(){
			//触发点击效果
				me    =this;
				me_ctl=me.getAttribute("ctl");
				if(me.getAttribute("first")=="1"){
				//如果是全选复选框
					allNode=document.getElementsByTagName("input");
					same=[];
					for(var j=0; j<allNode.length; j++){
						if(allNode[j].getAttribute("ctl")==me_ctl){ same.push(allNode[j]);}
					}
					if(me.checked == true){
						for(var k=1; k<same.length; k++){
							same[k].checked=true;
							document.getElementById(same[k].getAttribute("pms_id")).className = "select";
						}
					}else{
						for(var k=1; k<same.length; k++){
							same[k].checked=false;
							document.getElementById(same[k].getAttribute("pms_id")).className = "notselect";
						}
					}
				}else if(me.getAttribute("first")=="0"){
				//如果不是全选复选框
					if(me.checked!=true){
						document.getElementById(me.getAttribute("pms_id")).className = "notselect";
					}else{
						document.getElementById(me.getAttribute("pms_id")).className = "select";
					}
					var firstchecked = false;
					allNode=document.getElementsByTagName("input");
					same=[];
					for(var j=0; j<allNode.length; j++){
						if(allNode[j].getAttribute("ctl")==me_ctl){same.push(allNode[j]);}
					}
					for(var k=1; k<same.length; k++){
						if(same[k].checked==true){
							firstchecked = true;
						}
					}
					document.getElementById(me.getAttribute("ctl")).checked = firstchecked;
				}
			}
		}
	}
}
function class_AdvancedSearch(){
	this.Open = function(width,height,id){
		var HTML = document.getElementById(id).innerHTML;
		var date = new Date();
		var DiaWindowID="alert_"+date.getHours()+"_"+date.getMinutes()+"_"+date.getSeconds()+"_"+date.getMilliseconds();
		if(DiaWindowID!=null){
			KnifeCMS.CreateDiaWindow(width,height,DiaWindowID,Lang_Js_AdvancedSearch,HTML,true);
		}
	}
}
function class_Content(){
	/**$:添加编辑内容模型时检查**/
	this.SaveSubSystemCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("ContentSubSystem","d_ContentSubSystem",Lang_Js_ThisCannotEmpty)){Check=false}
		if(!Admin.CheckItem("ContentSubSystemName","d_ContentSubSystemName",Lang_Js_ThisCannotEmpty)){Check=false}
		if(!Admin.isHaveSelect("IndexTemplate","d_IndexTemplate",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.isHaveSelect("CategoryTemplate","d_CategoryTemplate",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.isHaveSelect("ContentTemplate","d_ContentTemplate",Lang_Js_HaveNotSelect)){Check=false}
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
	/**$:添加编辑内容时检查**/
	this.SaveContentCheck = function(Status){
		var Check=true;
		var fn_name;
		var Openness=document.getElementsByName("Openness");
		var OpennessValue;
		var Password,RePassword
		var Password_=document.getElementById("Password");
		var RePassword_=document.getElementById("RePassword");
		
		if(!Admin.isHaveSelect("ClassID","d_ClassID",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.CheckItem("Title","d_Title",Lang_Js_ThisCannotEmpty)){Check=false}
		
		for(var i=0; i<Openness.length; i++){
			if(Openness[i].checked==true){
				OpennessValue=Openness[i].value;
			}
		}
		if(OpennessValue=="2"){
			obj=document.getElementById("d_Password");
			Password=trim(Password_.value);
			if(!IsPassword(Password)){
				obj.className="d_err";
				obj.innerHTML=Lang_Js_Cue[2];
				document.getElementById("Password").focus();
				Check=false;
			}else if(Password==null || Password==""){
				obj.className="d_err";
				obj.innerHTML=Lang_Js_Cue[1];
				document.getElementById("Password").focus();
				Check=false;
			}else{
				obj=document.getElementById("d_Password");
				obj.className="d_ok";
				obj.innerHTML="ok";
			}
		}
		//检查自定义字段
		var field = getElementsByTagNameAndAttribute(document,"input","notnull","true");
		for(var i=0; i<field.length; i++){
			fn_name = field[i].getAttribute("name");
			if(!Admin.CheckItem(fn_name,"d_"+fn_name,Lang_Js_ThisCannotEmpty)){Check=false}
		}
		field = getElementsByTagNameAndAttribute(document,"textarea","notnull","true");
		for(var i=0; i<field.length; i++){
			fn_name = field[i].getAttribute("name");
			if(!Admin.CheckItem(fn_name,"d_"+fn_name,Lang_Js_ThisCannotEmpty)){Check=false}
		}
		field = getElementsByTagNameAndAttribute(document,"select","notnull","true");
		for(var i=0; i<field.length; i++){
			fn_name = field[i].getAttribute("name");
			if(!Admin.isHaveSelect(fn_name,"d_"+fn_name,Lang_Js_HaveNotSelect)){Check=false}
		}
		if(Check){
			Admin.DataSubmiting();
			//OSWebEditor.sync();
			//document.forms["SaveForm"].submit()
			return true;
		}else{
			return false;
		}
	}
	/**$:获取子系统分类**/
	this.GetSubSystemClassList = function(innerHTMLArea){
		var obj    = document.getElementById(innerHTMLArea);//放置数据的地方
		var Select = document.getElementById("ContentSubSystem"); //document.forms[Form].elements['ContentSubSystem'];
		if(Select != undefined)
		{
			var SelectedOption = escape(Select.options[Select.selectedIndex].value);
			if(SelectedOption != "" && SelectedOption != "0"){
				obj.innerHTML = "";
				Admin.DataLoading(obj);
				var rn     = parseInt(Math.random()*1000);
				var usrurl = "ajax.get.asp?ctl=content_"+SelectedOption+"_class&act=view&system="+SelectedOption+"&r="+rn;
				var ajaxSta=getResponseText(usrurl);
				//window.open(usrurl);
				if(ajaxSta==false){
					obj.innerHTML = Lang_Js_Cue_LoadingFail;
				}else{
					if(ajaxSta.toLowerCase().indexOf("error")!=-1){
						obj.innerHTML = Lang_Js_Cue_LoadingFail;
					}else{
						obj.innerHTML = ajaxSta;
					}
				}
			}
			else
			{
				obj.innerHTML="";
			}
		}	
	}
	this.SetContentLabel = function(_this,subsystem){
		var id,label,value,imgsrc,imgnewsrc,newvalue;
		id        = _this.getAttribute("id");
		label     = _this.getAttribute("label");
		imgsrc    = _this.src;
		value     = parseInt( _this.getAttribute("value"));
		_this.src = "image/loading.gif";
		label = _this.getAttribute("label");
		if(value>0){
			newvalue     = 0;
			imgnewsrc = "image/icon_yes_gray.png";
		}else{
			newvalue     = 1;
			imgnewsrc = "image/icon_yes.png";
		}
		var ajaxString = "label="+label+"&value="+newvalue;
		var ajaxUrl    = "ajax.get.asp?ctl=content_"+subsystem+"&act=edit&id="+id+"&subact=setlabel&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		if(ajaxSta=="ok"){
			_this.setAttribute("value",newvalue);
			_this.src = imgnewsrc;
		}else{
			if(ajaxSta.indexOf(Lang_NoPermissions)>0){alert(Lang_NoPermissions);}
			_this.src = imgsrc;
		}
	}
}
function class_Vote(){
	//投票添加/编辑提交保存时检查
	this.SaveVoteCheck = function(){
		var inputNas=document.getElementsByName("VoteOption");
		var inputValue;
		var OptionNum=0;
		var Check=true;
		if(inputNas!=null){
			Check=false;
			for (var i=0;i<inputNas.length;i++)
			{
				inputValue=inputNas[i].value;
				//inputValue=inputValue.replace(/[,]/g,"%2C");
				inputNas[i].value=trim(inputValue);
				if(trim(inputNas[i].value)!=""){OptionNum=OptionNum+1;}
			}
			Check=true;
		}
		if(!Admin.CheckItem("VoteTitle","d_VoteTitle",Lang_Js_ThisCannotEmpty)){Check=false}
		if(!Admin.isHaveSelect("ClassID","d_ClassID",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.CheckItem("VoteOption","d_VoteOptions",Lang_Js_Cue_NotWriteVoteOptions)){Check=false}
		if( OptionNum < 2 ){
			document.getElementById("d_VoteOptions").className="d_err";
			document.getElementById("d_VoteOptions").innerHTML=VoteOptionsCannotLessThanTwo;
			Check=false;
		}
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
}
function class_Class(){
	//分类添加/编辑提交保存时检查
	this.SaveClassCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("ClassName","d_ClassName",Lang_Js_ThisCannotEmpty)){Check=false};
		if($("#TemplateInherit").attr("checked")==false){
			if(!Admin.isHaveSelect("CategoryTemplate","d_CategoryTemplate",Lang_Js_HaveNotSelect)){Check=false}
			if(!Admin.isHaveSelect("ContentTemplate","d_ContentTemplate",Lang_Js_HaveNotSelect)){Check=false}
			if(Check==false){$("#TemplateSet").show();}
		}
		if(Check){
			OSWebEditor.sync();
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
	//分类批量编辑时对表单数据检查
	this.BatchEdit = function(){
		var inputOrs=document.forms["SaveForm"].elements["Order"];
		var inputNas=document.forms["SaveForm"].elements["ClassName"];
		var inputValue;
		var Check=false;
		var Check1=false;
		var Check2=false;
		Admin.DataSubmiting();
		if(inputOrs!=null){
			for (var i=0;i<inputOrs.length;i++)
			{
				inputValue=inputOrs[i].value;
				inputValue=inputValue.replace(/[^0-9]*/g,"");
				inputOrs[i].value=inputValue;
			}
			Check1=true;
		}
		if(inputNas!=null){
			for (var i=0;i<inputNas.length;i++)
			{
				inputValue=inputNas[i].value;
				inputValue=inputValue.replace(/[,]*/g,"");
				inputNas[i].value=inputValue;
			}
			Check2=true;
		}
		Check=Check1&&Check2;
		if(Check){
			//alert("ok!")
			document.forms["SaveForm"].submit()
		}
	}
	this.SetIsPageContent = function(_this){
		var id,label,value,imgsrc,imgnewsrc,newvalue;
		id        = _this.getAttribute("id");
		imgsrc    = _this.src;
		value     = parseInt( _this.getAttribute("value"));
		_this.src = "image/loading.gif";
		label = _this.getAttribute("label");
		if(value>0){
			newvalue     = 0;
			imgnewsrc = "image/icon_yes_gray.png";
		}else{
			newvalue     = 1;
			imgnewsrc = "image/icon_yes.png";
		}
		var ajaxString = "value="+newvalue;
		var ajaxUrl    = "ajax.get.asp?ctl="+_this.getAttribute("ctl")+"&act=edit&id="+id+"&subact=setispagecontent&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		if(ajaxSta=="ok"){
			_this.setAttribute("value",newvalue);
			_this.src = imgnewsrc;
		}else{
			if(ajaxSta.indexOf(Lang_NoPermissions)>0){alert(Lang_NoPermissions);}
			_this.src = imgsrc;
		}
	}
	this.SetIsShow = function(_this){
		var id,label,value,imgsrc,imgnewsrc,newvalue;
		id        = _this.getAttribute("id");
		imgsrc    = _this.src;
		value     = parseInt( _this.getAttribute("value"));
		_this.src = "image/loading.gif";
		label = _this.getAttribute("label");
		if(value>0){
			newvalue     = 0;
			imgnewsrc = "image/icon_yes.png";
		}else{
			newvalue     = 1;
			imgnewsrc = "image/icon_yes_gray.png";
		}
		var ajaxString = "value="+newvalue;
		var ajaxUrl    = "ajax.get.asp?ctl="+_this.getAttribute("ctl")+"&act=edit&id="+id+"&subact=setisshow&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		if(ajaxSta=="ok"){
			_this.setAttribute("value",newvalue);
			_this.src = imgnewsrc;
		}else{
			if(ajaxSta.indexOf(Lang_NoPermissions)>0){alert(Lang_NoPermissions);}
			_this.src = imgsrc;
		}
	}
}
function class_Collection(){
	//采集器添加/编辑提交保存时检查
	this.SaveCollectionCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("CollectionName","d_CollectionName",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("HandUrl|AutoUrls","d_Urls",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("TitleRule","d_TitleRule",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("ContentRule","d_ContentRule",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
	//采集结果编辑提交保存时检查
	this.SaveCollectionResultCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("CollectionResult_Title","d_CollectionResult_Title",Lang_Js_ThisCannotEmpty)){Check=false};
		//if(!Admin.CheckItem("TextContent","d_TextContent",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
			OSWebEditor.sync();
			document.forms["SaveForm"].submit()
		}
	}
}
function class_Goods(){
	//商品添加/编辑提交保存时检查
	this.SaveGoodsCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("GoodsName","d_GoodsName",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.isHaveSelect("GoodsType","d_GoodsType",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.isHaveSelect("ClassID","d_ClassID",Lang_Js_HaveNotSelect)){Check=false}
		var msg02=Lang_Js_Cue_GoodsBNHasBeenUsed;
		var GoodsBN=trim(document.forms["SaveForm"].GoodsBN.value);
		var GoodsID=trim(document.forms["SaveForm"].id.value)
		if(GoodsBN!=""){
			var obj=document.getElementById("d_GoodsBN");
			obj.innerHTML = "";
			Admin.DataLoading(obj);
			var rn     = parseInt(Math.random()*1000);
			var usrurl = "ajax.get.asp?ctl=goods&act=view&subact=checkgoodsbn&GoodsBN="+GoodsBN+"&id="+GoodsID+"&r="+rn;
			var ajaxSta=getResponseText(usrurl);
			//window.open(usrurl);
			if(ajaxSta==false){
				Check=false;
				obj.className="err"
				obj.innerHTML = Lang_Js_Cue_LoadingFail;
			}else{
				if(ajaxSta.toLowerCase().indexOf("error")!=-1){
					Check=false;
					obj.className="d_err"
					obj.innerHTML = msg02;
				}else{
					obj.className="d_ok"
					obj.innerHTML = Lang_Js_Cue_CanbeUse;
				}
			}
		}else{
			var obj=document.getElementById("d_GoodsBN");
			obj.innerHTML = "";
		}
		//检查自定义字段
		var field = getElementsByTagNameAndAttribute(document,"input","notnull","true");
		for(var i=0; i<field.length; i++){
			fn_name = field[i].getAttribute("name");
			if(!Admin.CheckItem(fn_name,"d_"+fn_name,Lang_Js_ThisCannotEmpty)){Check=false}
		}
		field = getElementsByTagNameAndAttribute(document,"textarea","notnull","true");
		for(var i=0; i<field.length; i++){
			fn_name = field[i].getAttribute("name");
			if(!Admin.CheckItem(fn_name,"d_"+fn_name,Lang_Js_ThisCannotEmpty)){Check=false}
		}
		field = getElementsByTagNameAndAttribute(document,"select","notnull","true");
		for(var i=0; i<field.length; i++){
			fn_name = field[i].getAttribute("name");
			if(!Admin.isHaveSelect(fn_name,"d_"+fn_name,Lang_Js_HaveNotSelect)){Check=false}
		}
		if(Check){
			Admin.DataSubmiting();
			this.PostAdjunctSet();
			this.PostAttrSet("SaveForm");
			return true;
		}else{
			return false;
		}
		//if(Check){
//			Admin.DataSubmiting();
//			this.PostAdjunctSet();
//			this.PostAttrSet("SaveForm");
//			OSWebEditor.sync();
//			document.forms["SaveForm"].submit()
//		}
	}
	/**$:配件处理**/
	this.PostAdjunctSet = function(){
		var Adjunct=document.getElementsByClassName("div","inbox Adjunct");
		var AdjunctString="";
		var IDString="";
		if(Adjunct.length>0){
			for(var i=0; i<Adjunct.length; i++)
			{
				var idArr=Adjunct[i].id.split("_");
				var AdjunctName=document.getElementsByName("AdjunctName_"+idArr[1]);
				var AdjunctMinNum=document.getElementsByName("AdjunctMinNum_"+idArr[1]);
				var AdjunctMaxNum=document.getElementsByName("AdjunctMaxNum_"+idArr[1]);
				var AdjunctPriceType=document.getElementsByName("AdjunctPriceType_"+idArr[1]);
				var AdjunctPrice=document.getElementsByName("AdjunctPrice_"+idArr[1]);
				var AdjunctGoodsID=getElementsByTagNameAndName("input","AdjunctGoodsID[]_"+idArr[1]);//document.getElementsByName();
				//alert(AdjunctGoodsID.length);
				var AdjunctPriceTypeValue=0;
				for(var j=0;j<AdjunctPriceType.length;j++){if(AdjunctPriceType[j].checked==true){AdjunctPriceTypeValue=parseInt(AdjunctPriceType[j].value);}}
				if(AdjunctName!=null && AdjunctMinNum!=null && AdjunctMaxNum!=null && AdjunctPriceType!=null && AdjunctPrice!=null && AdjunctGoodsID!=null){
					IDString = "";
					for(var j=0; j<AdjunctGoodsID.length; j++)
					{
						if(trim(IDString)!=""){
							IDString=IDString+","+escape(AdjunctGoodsID[j].value);
						}
						else{
							IDString=escape(AdjunctGoodsID[j].value);
						}
					}
					if(trim(AdjunctString)!=""){
						AdjunctString=AdjunctString+"|"+escape(AdjunctName[0].getAttribute("adjunctid"))+":"+escape(AdjunctName[0].value)+":"+escape(AdjunctMinNum[0].value)+":"+escape(AdjunctMaxNum[0].value)+":"+escape(AdjunctPriceTypeValue)+":"+escape(AdjunctPrice[0].value)+":"+IDString;
					}else{
						AdjunctString=escape(AdjunctName[0].getAttribute("adjunctid"))+":"+escape(AdjunctName[0].value)+":"+escape(AdjunctMinNum[0].value)+":"+escape(AdjunctMaxNum[0].value)+":"+escape(AdjunctPriceTypeValue)+":"+escape(AdjunctPrice[0].value)+":"+IDString;
					}
				}
			}
			//alert(AdjunctString);
			var input=document.getElementById("GoodsAdjunct");
			if(input!=null){input.value=AdjunctString;}else{alert("Error:"+Js_Goods[23]+"");}
		}
		//alert(AdjunctString);
	}
	/**$:商品类型属性处理**/
	this.PostAttrSet = function(FormName){
		var Attr,tmpID,tmpValue,tmpPrice;
		var AttrString="";
		var arrID = [];
		var arrValue = [];
		var arrPrice = [];
		var Form=document.forms[FormName];
		Attr=Form.elements;
		if(Attr!=null)
		{
			for(var i=0 ; i<Attr.length ; i++)
			{
				if(Attr[i].name=='Attr_ID_List[]'){ arrID.push(Attr[i]); }
				if(Attr[i].name=='Attr_Value_List[]'){ arrValue.push(Attr[i]); }
				if(Attr[i].name=='Attr_Price_List[]'){ arrPrice.push(Attr[i]); }
			}
		}
		if(arrID.length>0 && arrID.length==arrValue.length && arrID.length==arrPrice.length)
		{
			for(var i=0 ; i<arrID.length ; i++)
			{
				tmpID=trim(arrID[i].value);
				tmpValue=trim(arrValue[i].value);
				tmpPrice=trim(arrPrice[i].value);
				if(tmpValue.length > 0){
					if(trim(AttrString)!=""){
						AttrString=AttrString+"|"+escape(tmpID)+":"+escape(tmpValue)+":"+escape(tmpPrice);
					}else{
						AttrString=escape(tmpID)+":"+escape(tmpValue)+":"+escape(tmpPrice);
					}
				}
				arrID[i].parentNode.removeChild(arrID[i]);
				arrValue[i].parentNode.removeChild(arrValue[i]);
				arrPrice[i].parentNode.removeChild(arrPrice[i]);
			}
		}
		var input=document.getElementById("GoodsAttr");
		if(input!=null){input.value=AttrString;}else{alert("Error:"+Js_Goods[22]+"");}
	}
	/**$:添加一个属性值**/
	this.AddGoodsAttr = function(obj){
		var table=obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
		var tds=table.getElementsByTagName("td");
		if(tds!=null){
			var Trs=table.rows.length;
			var newTr=table.insertRow(Trs);
			var newCell_1=newTr.insertCell(-1);
			var newCell_2=newTr.insertCell(-1);
			var newCell_3=newTr.insertCell(-1);
			newTr.className="data";
			newCell_1.style.textAlign="center";
			newCell_1.innerHTML="<a onclick=\"DeleteCurrentRow(this)\" href=\"javascript:void(0);\"><b class=\"icon icon_del\"></b></a>";
			newCell_2.innerHTML=tds[1].innerHTML;
			newCell_3.innerHTML=tds[2].innerHTML;
		}
	}
	/**$:添加一个配件组**/
	var adjGroupi=0;
	this.addAdjunctGroup = function(){
		var Adjunct=document.getElementsByClassName("div","inbox Adjunct");
		if(Adjunct.length>0){
			adjGroupi=parseInt(Adjunct[Adjunct.length-1].id.split("_")[1])+1;
		}else{
			adjGroupi++;
		}
		var divParent=document.getElementById("adjunct_box");
		var div=document.createElement("div");
		div.className="inbox Adjunct";
		div.id="Adjunct_"+adjGroupi;
		var divHTML;
		divHTML="<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"infoTable\"><tbody>";
		divHTML=divHTML+"<tr><td class=\"titletd\">"+Js_Goods[1]+"<!--配件组名称-->:</td><td class=\"infotd\">";
		divHTML=divHTML+"<input type=\"text\" class=\"TxtClass\" adjunctid=\"0\" name=\"AdjunctName_"+adjGroupi+"\" value=\"\" />";
		divHTML=divHTML+"<span class=\"sysBtn\"><a href=\"javascript:void(0);\" onClick=\"Admin.Goods.delAdjunctGroup(this)\"><b class=\"icon icon_del\"></b>"+Js_Goods[2]+"<!--删除此组配件--></a></span>";
		divHTML=divHTML+"</td></tr>";
		divHTML=divHTML+"<tr><td class=\"titletd\">"+Js_Goods[3]+"<!--最小购买量-->:</td><td class=\"infotd\"><input type=\"text\" class=\"TxtClass Short\" name=\"AdjunctMinNum_"+adjGroupi+"\" value=\"0\" /> <label class=\"Normal\">"+Js_Goods[5]+"<!--0表示不限制--></label></td></tr>";
		divHTML=divHTML+"<tr><td class=\"titletd\">"+Js_Goods[4]+"<!--最大购买量-->:</td><td class=\"infotd\"><input type=\"text\" class=\"TxtClass Short\" name=\"AdjunctMaxNum_"+adjGroupi+"\" value=\"0\" /> <label class=\"Normal\">"+Js_Goods[5]+"<!--0表示不限制--></label></td></tr>";
		divHTML=divHTML+"<tr><td class=\"titletd\">"+Js_Goods[6]+"<!--优惠方式-->:</td><td class=\"infotd\">";
		divHTML=divHTML+"<input type=\"radio\" name=\"AdjunctPriceType_"+adjGroupi+"\" value=\"0\" checked=\"checked\" />"+Js_Goods[7]+"<!--优惠一定金额--> &nbsp; <input type=\"radio\" name=\"AdjunctPriceType_"+adjGroupi+"\" value=\"1\" />"+Js_Goods[8]+"<!--优惠价为某个折扣-->";
		divHTML=divHTML+"</td></tr>";
		divHTML=divHTML+"<tr><td class=\"titletd\">"+Js_Goods[9]+"<!--优惠额度-->:</td><td class=\"infotd\"><input type=\"text\" class=\"TxtClass Short\" name=\"AdjunctPrice_"+adjGroupi+"\" value=\"\" /> <label class=\"Normal\">"+Js_Goods[10]+"<!--(优惠100元就输入100，优惠折扣价为原来的8.8折就输入0.88)(无优惠的设置方式：优惠方式选择优惠一定金额、优惠额度填写0)--></label></td></tr>";
		divHTML=divHTML+"<tr><td class=\"titletd\" valign=\"top\">"+Js_Goods[11]+"<!--添加配件商品-->:</td><td class=\"infotd\">";
		divHTML=divHTML+"<div class=\"toparea\"><span class=\"sysBtn\"><a href=\"javascript:;\" onClick=\"Admin.Goods.getGoods(\'Adjunct\',\'AdjunctGoods_"+adjGroupi+"\',\'AdjunctGoodsID[]_"+adjGroupi+"\')\"><b class=\"icon icon_add\"></b>"+Js_Goods[12]+"<!--填加配件--></a></span></div>";
		divHTML=divHTML+"<div class=\"tabledata2\">";
		divHTML=divHTML+"<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"listTable\"><tbody id=\"AdjunctGoods_"+adjGroupi+"\">";
		divHTML=divHTML+"<tr class=\"top\"><th width=\"2%\"><div style=\"width:30px;\">"+Lang_Js_DoDel+"</div></th><th width=\"3%\"><div style=\"width:50px;\">"+Lang_Js_ListTableColLine_ID+"</div></th><th width=\"15%\"><div style=\"width:160px;\">"+Js_Goods[13]+"<!--商品编号--></div></th><th width=\"80%\"><div style=\"width:250px;\">"+Js_Goods[14]+"<!--商品名称--></div></th></tr>";
		divHTML=divHTML+"</tbody></table>";
		divHTML=divHTML+"</div>";
		divHTML=divHTML+"</td></tr>";
		divHTML=divHTML+"</tbody></table>";
		div.innerHTML=divHTML;
		divParent.appendChild(div);
	}
	/**$:删除当前配件组**/
	this.delAdjunctGroup = function(obj){
		var div=obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
		var divParent=div.parentNode;
		if(div!=null){
			divParent.removeChild(div); 
		}
	}
	
	/**$:向父窗口返回选择的(选择相关投票/内容/商品....)**/
	this.RebackSelectGoods = function(InputName){
		var Data="";
		var tmpdata,ID,GoodsBN,Name,tr,tds;
		var inputs=document.getElementsByName(InputName);
		if(objExist(inputs)){
			for (var i=0;i<inputs.length;i++)
			{
				if(inputs[i].checked == true)
				{
					tr=inputs[i].parentNode.parentNode;
					tds=tr.getElementsByTagName("td");
					ID=escape(inputs[i].value);
					GoodsBN=escape(tds[2].innerHTML);
					Name=escape(tds[3].innerHTML);
					tmpdata=ID+":"+GoodsBN+":"+Name;
					if(Data==""){
						Data=tmpdata;
					}else{
						Data=Data+","+tmpdata;
					}
				}
			}
		}
		//alert(Data)
		window.returnValue=Data;
		WinClose();
	}
	/**$:添加相关商品/添加配件商品 Link:相关 Adjunct:配件**/
	this.getGoods = function(type,area,areaInputName){
		var url,NewWindow,arrDatai,arrDataj,newTr,newCell0,newCell1,newCell2,newCell3,inputs;
		var check=true;
		var TargetArea=document.getElementById(area);
		if(type=="Link"){
			url="goods.asp?ctl=goods&act=view&acttype=minilist&returnresult=linked";
		}else if(type=="Adjunct"){
			url="goods.asp?ctl=goods&act=view&acttype=minilist&returnresult=adjunct";
		}
		if(url!=null && TargetArea!=null && areaInputName!=null)
		{
			NewWindow=window.showModalDialog(url,window,'dialogWidth:940px; dialogHeight:600px; center:Yes; help:No; resizable:yes; status:yes; scroll:auto;');
			//alert("-"+NewWindow+"-");
			if(NewWindow!=null && NewWindow!="")
			{
				//NewWindow=NewWindow.substring(2);
				//NewWindow=NewWindow.substring(0,NewWindow.length-2);
				arrDatai=NewWindow.split(",");
				inputs=TargetArea.getElementsByTagName("input");
				for(var i=0; i<arrDatai.length; i++){
					var Trs=TargetArea.rows.length;
					arrDataj=arrDatai[i].split(":");
					for(var j=0; j<inputs.length; j++)
					{
						//alert(inputs[j].value);
						if(inputs[j].value==unescape(arrDataj[0]))
						{
							check=false;
							break;
						}
					}
					if(check){
						newTr=TargetArea.insertRow(Trs);
						newTr.className="data";
						newCell0=newTr.insertCell(-1);
						newCell0.innerHTML="<a onclick=\"DeleteCurrentRow(this)\" href=\"javascript:;\"><b class=\"icon icon_del\"></b></a>";
						newCell1=newTr.insertCell(-1);
						newCell1.innerHTML="<input type=\"hidden\" name=\""+areaInputName+"\" value=\""+unescape(arrDataj[0])+"\" />"+unescape(arrDataj[0]);
						newCell2=newTr.insertCell(-1);
						newCell2.innerHTML=unescape(arrDataj[1]);
						newCell3=newTr.insertCell(-1);
						newCell3.innerHTML=unescape(arrDataj[2]);
					}
					check=true;
				}
			}
		}
	}
	/**$:Ajax选择商品类型列出属性**/
	this.GetAttrList = function(innerHTMLArea){
	    var obj    = document.getElementById(innerHTMLArea);//放置数据的地方
		var Select = document.getElementById("GoodsModel");
		if(objExist(Select))
		{
			var SelectedOption = escape(Select.options[Select.selectedIndex].value);
			if(SelectedOption != "" && SelectedOption != "0"){
				obj.innerHTML = "";
				Admin.DataLoading(obj);
				var rn     = parseInt(Math.random()*1000);
				var usrurl = "ajax.get.asp?ctl=goodsmodel&act=view&subact=getgoodsmodelattribute&id="+SelectedOption+"&r="+rn;
				var ajaxSta=getResponseText(usrurl);
				//window.open(usrurl);
				if(ajaxSta==false){
					obj.innerHTML = Lang_Js_Cue_LoadingFail;
				}else{
					ajaxSta+="";
					if(ajaxSta.toLowerCase().indexOf("error")!=-1){
						obj.innerHTML = Lang_Js_Cue_LoadingFail;
					}else{
						obj.innerHTML = ajaxSta;
					}
				}
			}
			else
			{
				obj.innerHTML="";
			}
		}	
	}
	//$:商品类型添加/编辑提交保存时检查
	this.SaveGoodsModelCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("GoodsModelName","d_GoodsModelName",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
	//$:商品类型属性添加/编辑提交保存时检查
	this.SaveAttributeCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("AttrName","d_AttrName",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
    //$:品牌添加/编辑提交保存时检查
	this.SaveBrandCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("BrandName","d_BrandName",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
			OSWebEditor.sync();
			document.forms["SaveForm"].submit()
		}
	}
	this.GetOrder = function(type){
		var type_ = parseInt(type)
		var url = [
				   "order.asp?ctl=order&act=view&all",
				   "order.asp?ctl=order&act=view&order=2&subact=search&status[]=1&paystatus[]=0",
				   "order.asp?ctl=order&act=view&order=3&subact=search&status[]=1&paystatus[]=1",
				   "order.asp?ctl=order&act=view&order=4&subact=search&status[]=1&shipstatus[]=0",
				   "order.asp?ctl=order&act=view&order=5&subact=search&status[]=1&shipstatus[]=1",
				   "order.asp?ctl=order&act=view&order=6&subact=search&status[]=1&paystatus[]=5",
				   "order.asp?ctl=order&act=view&order=7&subact=search&status[]=1&shipstatus[]=4"
				   ];
		window.location.href=url[type_-1];
	}
	this.SetGoodsLabel = function(_this){
		var id,label,value,imgsrc,imgnewsrc,newvalue;
		id        = _this.getAttribute("id");
		label     = _this.getAttribute("label");
		imgsrc    = _this.src;
		value     = parseInt( _this.getAttribute("value"));
		_this.src = "image/loading.gif";
		label = _this.getAttribute("label");
		if(value>0){
			newvalue     = 0;
			imgnewsrc = "image/icon_yes_gray.png";
		}else{
			newvalue     = 1;
			imgnewsrc = "image/icon_yes.png";
		}
		var ajaxString = "label="+label+"&value="+newvalue;
		var ajaxUrl    = "ajax.get.asp?ctl=goods&act=edit&id="+id+"&subact=setlabel&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		if(ajaxSta=="ok"){
			_this.setAttribute("value",newvalue);
			_this.src = imgnewsrc;
		}else{
			if(ajaxSta.indexOf(Lang_NoPermissions)>0){alert(Lang_NoPermissions);}
			_this.src = imgsrc;
		}
	}
}
function class_Region(){
	var Region=this;
	//$:添加地区
	this.AddRegion = function(Region_Path){
		Region.AddRegion_Sub(Region_Path,false);
	}
	//$:添加顶级地区
	this.AddTopRegion = function(Region_Path){
		Region.AddRegion_Sub(Region_Path,true);
	}
	this.AddRegion_Sub = function(Region_Path,TopRegion){
		var this_ = document.getElementById(Region_Path);
		if(TopRegion){this_=true}
		if(objExist(this_)){
			var fn_P_Local_Name;
			if(TopRegion){fn_P_Local_Name = Lang_Js_Region[0] }else{fn_P_Local_Name = this_.getElementsByTagName("a")[0].innerHTML;}
			var fn_contentHTML = "<form name=\"SaveForm\" id=\"SaveForm\" action=\"?ctl=regionset&act=add&subact=save\" method=\"post\"><div class=\"diainbox\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"infoTable\"><tbody>";

			fn_contentHTML = fn_contentHTML + "<tr><td class=\"titletd\">"+Lang_Js_Region[1]+":</td><td class=\"infotd\">"+fn_P_Local_Name+"<input type=\"hidden\" id=\"P_Region_Path\" name=\"P_Region_Path\" value=\""+Region_Path+"\" /></td></tr>";
			
            fn_contentHTML = fn_contentHTML + "<tr><td class=\"titletd\">"+Lang_Js_Region[2]+":</td><td class=\"infotd\"><input type=\"text\" class=\"TxtClass\" id=\"Local_Name\" name=\"Local_Name\" value=\"\" /><label id=\"d_Local_Name\"></label></td></tr>";
			
			fn_contentHTML = fn_contentHTML + "<tr><td class=\"titletd\">"+Lang_Js_Region[3]+":</td><td class=\"infotd\"><input type=\"text\" class=\"TxtClass\" id=\"OrderNum\" name=\"OrderNum\" onblur=\"KnifeCMS.onblur(this,Array('replace','/[^0-9]*/g','maxLength:5'))\" style=\"width:60px;\" value=\"99\" /> <label class=\"Normal\">"+Lang_Js_Region[4]+"</label></td></tr>";
			
			fn_contentHTML = fn_contentHTML + "</tbody></table></div><input type=\"hidden\" id=\"act\" name=\"act\" value=\"add\" /><div class=\"actline mt5\"><button type=\"button\" class=\"sysSaveBtn\" name=\"SubmitBtn\" id=\"SubmitBtn\" onclick=\"Admin.Region.SaveRegion()\"><b class=\"icon icon_save\"></b>"+ Lang_Js_Btn_Submit+"</button><button type=\"reset\" class=\"sysSaveBtn\" id=\"ReSetBtn\" onclick=\"KnifeCMS.DiaWindowCloseByID('Dia_AddRegion')\"><b class=\"icon icon_close\"></b>"+Lang_Js_Btn_Close+"</button></div></form>";

			fn_contentHTML = "<div id=\"Dia_In_AddRegion\">"+ fn_contentHTML +"</div>";
			KnifeCMS.CreateDiaWindow(600,300,"Dia_AddRegion",Lang_Js_Region[5],fn_contentHTML,true);
		}
	}
	//$:编辑地区
	this.EditRegion = function(Region_Path){
		var this_ = document.getElementById(Region_Path);
		if(objExist(this_)){
			var fn_Local_Name = this_.getElementsByTagName("a")[0].innerHTML;
			var fn_OrderNum   = this_.getElementsByTagName("span")[0].innerHTML;
			var fn_contentHTML = "<form name=\"SaveForm\" id=\"SaveForm\" action=\"?ctl=regionset&act=edit&subact=save\" method=\"post\"><div class=\"diainbox\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"infoTable\"><tbody>";
			
			fn_contentHTML = fn_contentHTML + " <tr><td class=\"titletd\">"+Lang_Js_Region[8]+":</td><td class=\"infotd\"><input type=\"text\" class=\"TxtClass\" id=\"Local_Name\" name=\"Local_Name\" value=\""+fn_Local_Name+"\" /><input type=\"hidden\" id=\"Region_Path\" name=\"Region_Path\" value=\""+Region_Path+"\" /><label id=\"d_Local_Name\"></label></td></tr>";
			fn_contentHTML = fn_contentHTML + "<tr><td class=\"titletd\">"+Lang_Js_Region[3]+":</td><td class=\"infotd\"><input type=\"text\" class=\"TxtClass\" id=\"OrderNum\" name=\"OrderNum\" onblur=\"KnifeCMS.onblur(this,Array('replace','/[^0-9]*/g','maxLength:5'))\" style=\"width:60px;\" value=\""+fn_OrderNum+"\" /> <label class=\"Normal\">"+Lang_Js_Region[4]+"</label></td></tr>";
			
			fn_contentHTML = fn_contentHTML + "</tbody></table></div><input type=\"hidden\" id=\"act\" name=\"act\" value=\"edit\" /><div class=\"actline mt5\"><button type=\"button\" class=\"sysSaveBtn\" name=\"SubmitBtn\" id=\"SubmitBtn\" onclick=\"Admin.Region.SaveRegion()\"><b class=\"icon icon_save\"></b>"+ Lang_Js_Btn_Submit+"</button><button type=\"reset\" class=\"sysSaveBtn\" id=\"ReSetBtn\" onclick=\"KnifeCMS.DiaWindowCloseByID('Dia_AddRegion')\"><b class=\"icon icon_close\"></b>"+Lang_Js_Btn_Close+"</button></div></form>";
			
			fn_contentHTML = "<div id=\"Dia_In_AddRegion\">"+ fn_contentHTML +"</div>";
			KnifeCMS.CreateDiaWindow(600,300,"Dia_AddRegion",Lang_Js_Region[6],fn_contentHTML,true);
		}
	}
	//$:Ajax保存地区数据
	this.SaveRegion = function(){
		var Check = true;
		if(!Admin.CheckItem("Local_Name","d_Local_Name",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check && objExist(document.forms["SaveForm"])){
			var ActionLang;
			var Action         = document.forms["SaveForm"].act.value;
			var fn_Local_Name  = escape(document.forms["SaveForm"].Local_Name.value);
			var fn_OrderNum    = parseInt(document.forms["SaveForm"].OrderNum.value);
			if(Action=="add"){
				ActionLang = Lang_Js_Add;
				var fn_P_Region_Path = escape(document.forms["SaveForm"].P_Region_Path.value);
				var fn_Region_Path;
			}else if(Action=="edit"){
				ActionLang = Lang_Js_Edit;
				var fn_P_Region_Path;
				var fn_Region_Path   = escape(document.forms["SaveForm"].Region_Path.value);
			}
			var SubmitBtn = document.forms["SaveForm"].SubmitBtn; SubmitBtn.disabled=true;
			var Dia_In_AddRegion = document.getElementById("Dia_In_AddRegion");
			//创建消息显示区
			var div_MsgBox = document.getElementById("ajax_msgbox");
			if(!objExist(div_MsgBox)){
				div_MsgBox           = document.createElement("div");
				div_MsgBox.id        = "ajax_msgbox";
				Dia_In_AddRegion.appendChild(div_MsgBox);
			}
			Admin.DataLoading(div_MsgBox);
			var ajaxString = "P_Region_Path="+fn_P_Region_Path+"&Region_Path="+fn_Region_Path+"&Local_Name="+fn_Local_Name+"&OrderNum="+fn_OrderNum;
			var ajaxUrl    = "ajax.get.asp?ctl=regionset&act="+Action+"&subact=save&r="+parseInt(Math.random()*1000);
			var ajaxSta    = posthttp(ajaxString,ajaxUrl); //window.open(ajaxUrl);
			if(ajaxSta==false){
				div_MsgBox.className = "d_err";
				div_MsgBox.innerHTML = ActionLang+Lang_Js[9]+Lang_Js[10];
			}else{
				if(ajaxSta.toLowerCase().indexOf("success")!=-1){
					div_MsgBox.className = "d_ok";
					fn_P_Region_Path = unescape(fn_P_Region_Path);
					fn_Region_Path   = unescape(fn_Region_Path);
					fn_Local_Name    = unescape(fn_Local_Name)
					if(Action=="add"){
						div_MsgBox.innerHTML = Lang_Js_Region[11]+":"+fn_Local_Name;
						document.getElementById("Local_Name").value = "";
						document.getElementById("d_Local_Name").innerHTML = "";
						document.getElementById("d_Local_Name").className = "";
						Admin.Region.GetRegions(fn_P_Region_Path);
					}else if(Action=="edit"){
						div_MsgBox.innerHTML = Lang_Js_Region[12]+":"+fn_Local_Name;
						var this_ = document.getElementById(fn_Region_Path);
						if(objExist(this_)){
							this_.getElementsByTagName("a")[0].innerHTML=fn_Local_Name;
							this_.getElementsByTagName("span")[0].innerHTML=fn_OrderNum;
						}
					}
				}else{
					div_MsgBox.className = "d_err";
					div_MsgBox.innerHTML = ActionLang+Lang_Js[9]+ajaxSta;
				}
			}
			SubmitBtn.disabled=false;
		}
	}
	//$:删除地区
	this.DelRegion = function(Region_Path){
		div_MsgBox           = document.createElement("div");
		div_MsgBox.id        = "ajax_msgbox";
		KnifeCMS.CreateDiaWindow(460,160,"Dia_DelRegion",Lang_Js_Region[7],"<div id=Dia_In_DelRegion class=p10></div>",true);
		var Dia_In_DelRegion = document.getElementById("Dia_In_DelRegion");
		Dia_In_DelRegion.appendChild(div_MsgBox);
		Admin.DataLoading(div_MsgBox);
		var fn_Local_Name = document.getElementById(Region_Path).getElementsByTagName("a")[0].innerHTML;
		var ajaxString = "Region_Path="+Region_Path
		var ajaxUrl    = "ajax.get.asp?ctl=regionset&act=del&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl); //window.open(ajaxUrl);
		if(ajaxSta==false){
			div_MsgBox.className = "d_err";
			div_MsgBox.innerHTML = Lang_Js_DoDel+Lang_Js[9]+Lang_Js[10];
		}else{
			if(ajaxSta.toLowerCase().indexOf("success")!=-1){
				div_MsgBox.className = "d_ok";
				div_MsgBox.innerHTML = Lang_Js_Region[13]+":"+unescape(fn_Local_Name);
				var this_=document.getElementById(Region_Path);
				if(objExist(this_)){
					this_.parentNode.removeChild(this_)
				}
				var inboxs = document.getElementsByClassName("div","inbox");
				var inboxs_Check = false;
				for(var i=0 ; i<inboxs.length ; i++){
					if(inboxs[i].name == Region_Path){ inboxs_Check = true;}
					if(inboxs_Check){inboxs[i].parentNode.removeChild(inboxs[i])}
				}
			}else{
				div_MsgBox.className = "d_err";
				div_MsgBox.innerHTML = Lang_Js_DoDel+Lang_Js[9]+ajaxSta;
			}
		}
		//KnifeCMS.DiaWindowCloseByID('Dia_DelRegion');
	}
	/**$:Ajax获取下级地区**/
	this.GetRegions = function(Region_Path){
	    var obj    = document.getElementById("Regions");//放置数据的地方
		if(objExist(obj))
		{
			//var this_id = this_.parentNode.parentNode.id;
			if(Region_Path!=""){
				var this_id = Region_Path;
				var regions=getElementsByTagNameAndName("div","regionlist");
				for(var i=0; i<regions.length; i++){
					if(regions[i].id==this_id){
						regions[i].className="regionlist current";
					}else{
						regions[i].className="regionlist";
					}
				}
				var Region_Grade = Region_Path.split(",").length - 1;
				if(!objExist(document.getElementById("inbox_"+Region_Grade))){
					var div_inbox = document.createElement("div");
					div_inbox.id         = "inbox_"+Region_Grade;
					div_inbox.className  = "inbox";
					div_inbox.name       = Region_Path;
					var div_zoom = document.createElement("div");
					div_zoom.id         = "zoom_"+Region_Grade;
					div_zoom.className  = "zoom";
					div_inbox.appendChild(div_zoom);
					obj.appendChild(div_inbox);
					
				}else{
					var div_zoom = document.getElementById("zoom_"+Region_Grade);
					var div_inbox=div_zoom.parentNode;
					div_inbox.name = Region_Path;
					for(var j=1 ; j<10 ; j++){
						if(objExist(document.getElementById("inbox_"+parseInt(Region_Grade+j)))){
							obj.removeChild(document.getElementById("inbox_"+parseInt(Region_Grade+j)))
						}
					}
				}
			}else{
				var Region_Grade = 1;
				var div_inbox = document.createElement("div");
					div_inbox.id         = "inbox_"+Region_Grade;
					div_inbox.className  = "inbox";
					div_inbox.name       = Region_Path;
				var div_zoom = document.createElement("div");
					div_zoom.id         = "zoom_"+Region_Grade;
					div_zoom.className  = "zoom";
					div_inbox.appendChild(div_zoom);
					obj.appendChild(div_inbox);
			}
			Admin.DataLoading(div_zoom);
			//start获取数据
			var fn_Region_Path = escape(Region_Path);
			var rn     = parseInt(Math.random()*1000);
			var usrurl = "ajax.get.asp?ctl=regionset&act=view&subact=getregions&regionpath="+fn_Region_Path+"&r="+rn;
			var ajaxSta=getResponseText(usrurl);				//window.open(usrurl);
			if(ajaxSta==false){
				div_zoom.innerHTML = Lang_Js_Cue_LoadingFail;
			}else{
				if(ajaxSta.toLowerCase().indexOf("error")!=-1 || ajaxSta.toLowerCase().indexOf("knifecms system message")!=-1){
					div_zoom.innerHTML = Lang_Js_Cue_LoadingFail+Lang_Js_Cue_CheckPermission;
				}else{
					//alert(ajaxSta);
					var Fn_TempHTML;
					var Fn_Region_Path;
					var Fn_Local_Name;
					var Fn_OrderNum;
					var fn_ParRegion = ajaxSta.substring(2,ajaxSta.indexOf(":")-1);
					if(fn_ParRegion=="NULL"){fn_ParRegion=Lang_Js_Region[0]}
					Fn_TempHTML = "<div class=tagnav>"+ Lang_Js_Region[15] +":"+ fn_ParRegion +"</div>";
					var regions = ajaxSta.substring(ajaxSta.indexOf(":")+2,ajaxSta.length-2);
					if(regions!=""){
						
						regions=regions.substring(1,regions.length-1)
						var a = regions.split("},{");
						for(var i=0;i<a.length;i++){
							b=a[i].split(",");
							Fn_Region_Path = b[0].split(":")[1];
							Fn_Region_Path = unescape(Fn_Region_Path.substring(1,Fn_Region_Path.length-1));
							Fn_Local_Name  = b[2].split(":")[1];
							Fn_Local_Name  = unescape(Fn_Local_Name.substring(1,Fn_Local_Name.length-1));
							Fn_OrderNum    = b[3].split(":")[1];
							Fn_OrderNum    = unescape(Fn_OrderNum.substring(1,Fn_OrderNum.length-1));
							Fn_TempHTML = Fn_TempHTML + "<div class=\"regionlist\" name=\"regionlist\" id=\""+ Fn_Region_Path +"\">";
							Fn_TempHTML = Fn_TempHTML + "<p class=\"p1\"><a href=\"javascript:;\" onclick=\"Admin.Region.GetRegions(\'"+ Fn_Region_Path +"\')\">"+ Fn_Local_Name +"</a></p>";
							Fn_TempHTML = Fn_TempHTML + "<p class=\"p2\"><span class=font10>"+ Fn_OrderNum +"</span> <a href=\"javascript:;\" onclick=\"Admin.Region.AddRegion(\'"+ Fn_Region_Path +"\')\">"+ Lang_Js_Add +"</a> | <a href=\"javascript:;\" onclick=\"Admin.Region.EditRegion(\'"+ Fn_Region_Path +"\')\">"+ Lang_Js_Edit +"</a> | <a href=\"javascript:;\" onclick=\"if(confirm(\'"+ Lang_Js_Region[14].replace("{$:Local_Name}",Fn_Local_Name)+"\')){Admin.Region.DelRegion(\'"+ Fn_Region_Path +"\')}\">"+ Lang_Js_Del +"</a></p>";
							Fn_TempHTML = Fn_TempHTML + "</div>";
						}
					}else{
						Fn_TempHTML = Fn_TempHTML + "<span class=\"m5\">none<span>";
					}
					div_zoom.innerHTML = Fn_TempHTML;
				}
			}
			//end
		}	
	}
}
function class_Payment(){
	//$:选择支付方式
	this.GetPayment = function(this_){
		var Select = document.getElementById("PayType");
		var obj    = document.getElementById("payment_config");
		var TempHTML;
		if(objExist(Select) && objExist(obj))
		{
			var SelectedOption = Select.options[Select.selectedIndex].value;
			if(SelectedOption=="alipay"){
				TempHTML="<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"infoTable\"><tbody>";
				TempHTML=TempHTML+"<tr><td class=\"titletd\">"+Lang_Js_Payment[0]+":</td><td class=\"infotd\">"+Lang_Js_Payment[1]+"</td></tr>";
				TempHTML=TempHTML+"<tr><td class=\"titletd\">"+Lang_Js_Payment[8]+":</td><td class=\"infotd\"><input type=\"text\" class=\"TxtClass\" id=\"SellerEmail\" name=\"SellerEmail\" style=\"width:220px;\" value=\"\" /><label id=\"d_SellerEmail\"></label></td></tr>";
				TempHTML=TempHTML+"<tr><td class=\"titletd\">"+Lang_Js_Payment[2]+":</td><td class=\"infotd\"><input type=\"text\" class=\"TxtClass\" id=\"Partner\" name=\"Partner\" style=\"width:220px;\" value=\"\" /><label id=\"d_Partner\"></label></td></tr>";
                TempHTML=TempHTML+"<tr><td class=\"titletd\">"+Lang_Js_Payment[3]+":</td><td class=\"infotd\"><input type=\"text\" class=\"TxtClass\" id=\"PrivateKey\" name=\"PrivateKey\" style=\"width:300px;\" value=\"\" /><label id=\"d_PrivateKey\"></label></td> </tr>";
				TempHTML=TempHTML+"<tr><td class=\"titletd\">"+Lang_Js_Payment[4]+":</td><td class=\"infotd\"><SELECT type=\"select\" name=\"InterFaceType\" ><option value=0>"+Lang_Js_Payment[5]+"</option><option value=1>"+Lang_Js_Payment[6]+"</option><option value=2>"+Lang_Js_Payment[7]+"</option></SELECT><label id=\"d_InterFaceType\"></label></td></tr>";
				TempHTML=TempHTML+"</tbody></table>";
				obj.innerHTML=TempHTML;
			}else if(SelectedOption=="offline"){
				obj.innerHTML="";
			}else{
				obj.innerHTML="";
			}
		}
	}
	//$:保存支付方式
	this.SavePaymentCheck = function(){
		var Check=true;
		if(!Admin.isHaveSelect("PayType","d_PayType",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.CheckItem("PaymentName","d_PaymentName",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
			OSWebEditor.sync();
			document.forms["SaveForm"].submit()
		}
	}
}
function class_Delivery(){
	var Delivery = this;
	this.Area_i = 0;
	//$:选择配送地区
	this.SelectDeliveryArea = function(this_){
		var order=this_.parentNode.parentNode.getElementsByTagName("input")[0].getAttribute("order");
		var tempValue=this_.parentNode.parentNode.getElementsByTagName("input")[1].value;
		if(tempValue != "" && order != ""){
			Admin.Tree.order=order;
			Admin.Tree.edit=true;
			Admin.Tree.value=tempValue;
		}else{
			Admin.Tree.order="";
			Admin.Tree.edit=false;
			Admin.Tree.value="";
		}
		var fn_DialogID   ="Dialog_AreaSelect";
		var fn_DialogTitle=Lang_Js_AreaSelect;
		var fn_contentHTML="<form name=\"SaveForm\" action=\"#\" method=\"get\"><div style=\"padding:5px 4px 5px 10px;\"><div class=\"tree\" id=\"tree\"></div></div></form>"
		var fn_Btn = "<button type=\"button\" class=\"sysSaveBtn\" name=\"SubmitBtn\" id=\"SubmitBtn\" onclick=\"Admin.Delivery.SaveSelectArea('"+order+"','"+fn_DialogID+"')\"><b class=\"icon icon_confirm\"></b>"+Lang_Js_Btn_Confirm+"</button><button type=\"reset\" class=\"sysSaveBtn\" id=\"ReSetBtn\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"')\"><b class=\"icon icon_close\"></b>"+Lang_Js_Btn_Close+"</button>"
		KnifeCMS.CreateDiaWindow(300,460,fn_DialogID,fn_DialogTitle,Array(fn_contentHTML,fn_Btn),true);
		Admin.DataLoading(document.getElementById("tree"));
		Admin.Tree.CreateFirstTree();
	}
	this.GetSubSelectArea = function(pnodeid){
		var checkboxs=getElementsByTagNameAndNameAndAttrbute("input","areanode[]","pid:"+pnodeid);
		var value = "";
		for(var ii=0;ii<checkboxs.length;ii++){
			if(checkboxs[ii].checked==true){
				if(value==""){
					value = checkboxs[ii].getAttribute("lcname");
				}else{
					value = value +"、"+ checkboxs[ii].getAttribute("lcname");
				}
			}
		}
		return value;
	}
	this.SaveSelectArea = function(order,DialogID){
		var inputs=document.getElementsByTagName("input");
		var checkbox = [];
		var Area;
		var AreaGroupID;
		var Area_Value="";
		var AreaGroupID_Value="";
		var TempJSONString = "";
		var SubAreaName = "";
		var PathArray   = [];
		for(var i=0;i<inputs.length;i++){
			if(inputs[i].getAttribute("name")=="Area[]" && inputs[i].getAttribute("order")==order){Area=inputs[i];}
			if(inputs[i].getAttribute("name")=="AreaGroupID[]" && inputs[i].getAttribute("order")==order){AreaGroupID=inputs[i];}
			if(inputs[i].name=="areanode[]" && inputs[i].checked==true){checkbox.push(inputs[i]);}
		}
		//获取选中的地区
		if(checkbox.length>0){
			for(var i=0;i<checkbox.length;i++){
				PathArray = checkbox[i].value.split(",");
				if(i==0){
					if(PathArray.length==3){
						Area_Value  = checkbox[i].getAttribute("lcname");
						SubAreaName = Delivery.GetSubSelectArea(PathArray[1]);
						Area_Value  = Area_Value +"("+SubAreaName+")";
					}
					AreaGroupID_Value = checkbox[i].getAttribute("nid");
				}else{
					if(PathArray.length==3){
						SubAreaName = Delivery.GetSubSelectArea(PathArray[1]);
						Area_Value  = Area_Value +"；"+ checkbox[i].getAttribute("lcname") +"("+SubAreaName+")";
					}
					AreaGroupID_Value = AreaGroupID_Value +","+ checkbox[i].getAttribute("nid");
				}
			}
		}
		if(objExist(Area) && objExist(AreaGroupID)){
			Area.value=Area_Value;
			if(Admin.Tree.JSONValue.length>0){TempJSONString=KnifeCMS.JSON.toJSONString(Admin.Tree.JSONValue)}
			AreaGroupID.value=TempJSONString;
		}
		KnifeCMS.DiaWindowCloseByID(DialogID);
	}
	//$:为指定的地区设置运费
	this.AddDeliveryAreaFee = function(){
		var div_inbox;
		var fn_TempHTML;
		var obj=document.getElementById("AreaFees");
		if(objExist(obj)){
			Delivery.Area_i=Delivery.Area_i+1;
			fn_TempHTML = "<a class=\"reduce\" onclick=\"Admin.Delivery.DeleteDeliveryAreaFee(this)\"; href=\"javascript:;\"><img src=\"image/reduce.gif\"></a><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"infoTable\">";
			fn_TempHTML = fn_TempHTML + "<tr><td class=\"titletd\">"+ Lang_Js_Delivery[6] +":</td><td class=\"infotd\"><input type=\"text\" class=\"TxtClass mr5\" readonly=\"readonly\" name=\"Area[]\" order=\"a"+Delivery.Area_i+"\" style=\"width:300px;\" value=\"\" /><input type=\"hidden\" order=\"a"+Delivery.Area_i+"\" name=\"AreaGroupID[]\" value='' /><span class=\"sysBtn\"><a href=\"javascript:;\" onclick=\"Admin.Delivery.SelectDeliveryArea(this)\"><b class=\"icon icon_edit\"></b>"+Lang_Js_Btn_Select+"</a></span></td></tr>";
			fn_TempHTML = fn_TempHTML + "<tr><td class=\"titletd\">"+ Lang_Js_Delivery[7] +":</td><td class=\"infotd\"><span>"+ Lang_Js_Delivery[8] +"</span><input type=\"text\" class=\"TxtClass Short\" name=\"AreaFirstPrice[]\" onblur=\"KnifeCMS.onblur(this,Array(\'replace\',\'/[^0-9.]*/g\',\'maxLength:9\'))\" value=\"0\" /><span class=\"ml20\">"+ Lang_Js_Delivery[9] +"</span><input type=\"text\" class=\"TxtClass Short\" name=\"AreaContinuePrice[]\" onblur=\"KnifeCMS.onblur(this,Array(\'replace\',\'/[^0-9.]*/g\',\'maxLength:9\'))\" value=\"0\" />";
			fn_TempHTML = fn_TempHTML + "</td></tr></table>";
			div_inbox = document.createElement("div")
			div_inbox.className="inbox";
			div_inbox.innerHTML=fn_TempHTML;
			obj.appendChild(div_inbox);
		}
	}
	//$:删除当前地区设置运费
	this.DeleteDeliveryAreaFee = function(this_){
		var parentNode=this_.parentNode.parentNode;
		parentNode.removeChild(this_.parentNode);
	}
	//$:保存配送方式
	this.SaveDeliveryCheck = function(){
		var Check=true;
		var AreaFeeSet=0;
		var Array_ = [];
		var UCheck = false;
		var Radio_AreaFeeSet=getElementsByTagNameAndName("input","AreaFeeSet");
		for(var i=0;i<Radio_AreaFeeSet.length;i++){if(Radio_AreaFeeSet[i].checked==true){AreaFeeSet=parseInt(Radio_AreaFeeSet[i].value);}}
		if(!(KnifeCMS.$id("DeliveryName").value.length>0)){
			KnifeCMS.$id("d_DeliveryName").className = "d_err";
			KnifeCMS.$id("d_DeliveryName").innerHTML = Lang_Js_Delivery[1];//"请填写配送方式名称.";
			Check=false;
		}else{
			KnifeCMS.$id("d_DeliveryName").className = "d_ok";
			KnifeCMS.$id("d_DeliveryName").innerHTML = "ok";
		}
		if(AreaFeeSet==0){
			document.getElementById("d_AreaFeeSet").className = "d_err";
			document.getElementById("d_AreaFeeSet").innerHTML = Lang_Js_Delivery[2];//"请选择配送地区费用的设置方式.";
			Check=false;
		}else{
			document.getElementById("d_AreaFeeSet").className = "d_ok";
			document.getElementById("d_AreaFeeSet").innerHTML = "ok";
		}
		if(AreaFeeSet==2){if(!Admin.CheckItem("Area[]","d_Area[]",Lang_Js_Delivery[3])){Check=false};}//Lang_Js_Delivery[3]="请指定配送地区."
		//首重重量\续重单位 必须选择
		if(!Admin.isHaveSelect("FirstUnit","d_FirstUnit",Lang_Js_Delivery[4])){Check=false}//Lang_Js_Delivery[4]="请选择首重重量."
		if(!Admin.isHaveSelect("ContinueUnit","d_ContinueUnit",Lang_Js_Delivery[5])){Check=false}//Lang_Js_Delivery[5]="请选择续重单位."
		for(var i=0;i<getElementsByTagNameAndName("input","AreaContinuePrice[]").length;i++){
			if(parseFloat(getElementsByTagNameAndName("input","AreaContinuePrice[]")[i].value)>0){UCheck=true;}
		}
		if(Check){
			Admin.DataSubmiting();
			OSWebEditor.sync();
			document.forms["SaveForm"].submit()
		}
	}
	//$:保存物流公司
	this.SaveDeliveryCorpCheck = function(){
		var Check=true;
		if(!(KnifeCMS.$id("CorpName").value.length>0)){
			KnifeCMS.$id("d_CorpName").className = "d_err";
			KnifeCMS.$id("d_CorpName").innerHTML = Lang_Js_DeliveryCorp[1];//"请填写物流公司名称.";
			Check=false;
		}else{
			KnifeCMS.$id("d_CorpName").className = "d_ok";
			KnifeCMS.$id("d_CorpName").innerHTML = "ok";
		}
		if(Check){
			Admin.DataSubmiting();
			OSWebEditor.sync();
			document.forms["SaveForm"].submit()
		}
	}
}
function class_Tree(){
	//author : linx
	//website: http://www.knifecms.com
	//qq     : 1071322670
	var Tree = this;
	this.edit = false;
	this.order = "";
	this.value = "";
	this.arrValue  = []; //[Array对象]
	this.JSONValue = []; //[JSON对象]选择地区时保存地区path数据
	
	this.CreateTree = function(obj,Region_Path,IsFirstTree){
		var P_Region_ID,Region_ID,Fn_Region_Path,Fn_Local_Name,Fn_OrderNum,Fn_ChildNum,regions,subnode_,node_,node_line,node_handle;
		var ii,div_node_line,span_node_handle,span_node_icon,span_node_name;
		var divs           = obj.getElementsByTagName("div");
		var checkboxs      = obj.getElementsByTagName("input");	
		var p_checkbox     = false; if(!IsFirstTree){ p_checkbox = checkboxs[0].checked;}
		for(var jj=0;jj<divs.length;jj++){if(divs[jj].className=="subnode"){subnode_=divs[jj];break;}}
		//Tree.arrValue=Tree.value.split(",");
		if(!KnifeCMS.IsNull(Tree.value)){Tree.JSONValue = KnifeCMS.JSON.parseJSON(Tree.value)}else{if(IsFirstTree){Tree.JSONValue=[]}}
		//子结点集合不存在则加载，存在则打开
		if(!objExist(subnode_)){
			KnifeCMS.GetRegion(Region_Path,region.regions);
			var ajaxSta = KnifeCMS.TempRegion;
			if(ajaxSta==false){alert(Lang_Js_Cue_LoadingFail);}
			else{
				subnode_=document.createElement("div");
				subnode_.className="subnode";
				obj.appendChild(subnode_);
				regions = ajaxSta;
				if(regions.length>0){
					var a = regions;
					var input_checkbox = [];
					var allUnSelect    = true;
					if(IsFirstTree){obj.innerHTML="";}
					for(ii=0;ii<a.length;ii++){
						Fn_Region_Path = a[ii].p;
						Fn_Local_Name  = a[ii].n;
						Fn_OrderNum    = a[ii].o;
						Fn_ChildNum    = a[ii].cn;
						P_Region_ID    = Fn_Region_Path.split(",")[Fn_Region_Path.split(",").length-3]
						Region_ID      = Fn_Region_Path.split(",")[Fn_Region_Path.split(",").length-2]
						
						node_=document.createElement("div"); node_.className = "node"; node_.id = Fn_Region_Path;
						div_node_line=document.createElement("div"); div_node_line.className="node_line";
						span_node_handle=document.createElement("span");span_node_handle.className="node_handle";
						if(isIE()){span_node_handle.onclick=function(){Admin.Tree.Handle(this)}}else{span_node_handle.setAttribute("onclick","Admin.Tree.Handle(this)");}
						span_node_handle.setAttribute("nid",Region_ID);
						span_node_handle.setAttribute("hasc",Fn_ChildNum);
						span_node_handle.setAttribute("pid",P_Region_ID);
						
						input_checkbox[ii]=document.createElement("input");
						input_checkbox[ii].type="checkbox";
						input_checkbox[ii].name = "areanode[]";
						input_checkbox[ii].setAttribute("btnode","false");
						input_checkbox[ii].setAttribute("nid",Region_ID);
						input_checkbox[ii].setAttribute("hasc",Fn_ChildNum);
						input_checkbox[ii].setAttribute("pid",P_Region_ID);
						input_checkbox[ii].setAttribute("lcname",Fn_Local_Name);
						input_checkbox[ii].value = Fn_Region_Path;
						
						span_node_icon = document.createElement("span");span_node_icon.className="node_icon";
						span_node_name = document.createElement("span");span_node_name.className="node_name";
						span_node_name.innerHTML=Fn_Local_Name;
						
						div_node_line.appendChild(span_node_handle);
						div_node_line.appendChild(input_checkbox[ii]);
						div_node_line.appendChild(span_node_icon);
						div_node_line.appendChild(span_node_name);
						node_.appendChild(div_node_line);
						if(IsFirstTree){obj.appendChild(node_);}else{subnode_.appendChild(node_);}
						
						//如果是最底级的地区则重新设置其样式
						if(!(Fn_ChildNum>0)){
							if(isIE()){span_node_handle.onclick=function(){}}else{span_node_handle.setAttribute("onclick","");}
							div_node_line.className="node_line none";
							input_checkbox[ii].setAttribute("btnode","true");
							span_node_icon.className="node_icon page";
						}
						
						//如果上级地区a为选中状态，则a下的所有子地区为选中
						if(p_checkbox==true){
							input_checkbox[ii].checked=true;
						}
						//设置input_checkbox是否为选中状态
						KnifeCMS.IsInRegion=false;
						KnifeCMS.CheckIsInRegion(Fn_Region_Path,Tree.JSONValue)
						if(KnifeCMS.IsInRegion==true){
							input_checkbox[ii].checked=true;
							allUnSelect=false;
							//alert("Fn_Region_Path="+Fn_Region_Path + "  input_checkbox["+ii+"].checked="+input_checkbox[ii].checked+";"+"  input_checkbox["+ii+"].value="+input_checkbox[ii].value);
						}else{
							input_checkbox[ii].checked=false;
						}
						if(input_checkbox[ii].checked){Tree.AddRegionToJSONValue(input_checkbox[ii].value);}
						
						
					}//for循环结束
					//设置当前结点为打开状态
					if(!IsFirstTree){node_line=divs[0];node_line.className = "node_line open";}
					//如果父级为选中，并且其下级都未选中，则设置其下级为都选中
					if(p_checkbox==true && allUnSelect==true){
						for(var kk=0;kk<ii;kk++){
							input_checkbox[kk].checked=true;
							Tree.AddRegionToJSONValue(input_checkbox[kk].value);
						}
					}
					//document.getElementById("treejsonvaluec").innerHTML="allUnSelect="+allUnSelect;
					//document.getElementById("treejsonvalue").innerHTML=KnifeCMS.JSON.toJSONString(Tree.JSONValue);
					//alert(ii);
				}else{
					alert("no regions")
				}
			}
		}else{
			node_line=divs[0];
			if(node_line.className=="node_line"){
				node_line.className   = "node_line open";
				subnode_.style.display= "";
			}else{
				node_line.className   = "node_line";
				subnode_.style.display= "none";
			}
		}
		
	}
	this.CreateFirstTree = function(){
		var obj=document.getElementById("tree");
		if(objExist(obj)){
			Tree.CreateTree(obj,",",true);
			Tree.SetLastNode();
			Tree.SetCheckboxInput();
			//alert(Tree.order);alert(Tree.value);
		}
	}
	this.Handle = function(this_){
		var obj=this_.parentNode.parentNode;
		if(objExist(obj)){
			var loading=document.createElement("span");
			loading.className="loading";
			obj.appendChild(loading);
			Tree.CreateTree(KnifeCMS.$id(obj.id),obj.id,false);
			obj.removeChild(loading);
			Tree.SetLastNode();
			Tree.SetCheckboxInput();
		}
	}
	//$:设置每一个树叉里最后一个树接点的图标
	this.SetLastNode = function(){
		var divs,node_divs,subnode,last_subnode,last_node_line,className,p_subnode,is_last;
		var lastnode_line = [];
		var node_line = [];
		divs=document.getElementById("tree").getElementsByTagName("div");
		for(var j=0;j<divs.length;j++){if(divs[j].className.indexOf("node_line") != -1){node_line.push(divs[j]);}}
		for(var i=0;i<node_line.length;i++){
			is_last=true;
			p_subnode=node_line[i].parentNode.parentNode;
			for(var k=i+1;k<node_line.length;k++){
				if(p_subnode == node_line[k].parentNode.parentNode){is_last=false;}
			}
			if(is_last){
				node_handle = node_line[i].getElementsByTagName("span")[0];
				node_handle.className = "node_handle last";
				//检查其下是否有子结点，有则清除其下div.subnode的背景
				if(objExist(node_line[i].parentNode)){
					node_divs=document.getElementById(node_line[i].parentNode.id).getElementsByTagName("div");
					if(node_divs.length>1){
						last_subnode=node_divs[1];
						last_subnode.style.backgroundImage="url()";
					}
				}
			}
		}
	}
	//$:Checkbox初始化
	this.SetCheckboxInput = function(){
		var p_nodes,tempcheckbox,checked,value,Check;
		var checkbox = document.getElementById("tree").getElementsByTagName("input");
		for(var i=0; i<checkbox.length;i++){
			checkbox[i].onclick=function(){
				checked=this.checked;
				if(this.getAttribute("btnode")=="false" || this.getAttribute("btnode")=="true"){
					p_nodes=this.value.split(",");
					tempcheckbox=document.getElementById("tree").getElementsByTagName("input");
					if(checked){
						//把选中的地区添加到Tree.JSONValue
						Tree.AddRegionToJSONValue(this.value);
						
						//点击当前结点选中 则设置其所有直属上级结点为选中
						for(var j=1;j<p_nodes.length-2;j++){
							for(var k=0; k<tempcheckbox.length; k++){if(tempcheckbox[k].getAttribute("nid")==p_nodes[j]){tempcheckbox[k].checked=true;}}
						}
						//点击当前结点选中 并且当前结点下还有子结点 则设置其下属结点全部为选中
						if(this.getAttribute("btnode")=="false"){
							for(var k=0; k<tempcheckbox.length; k++){
								if(tempcheckbox[k].value.substring(0,this.value.length) == this.value){
									tempcheckbox[k].checked=true;
									Tree.AddRegionToJSONValue(tempcheckbox[k].value);
								}
							}
						}
						//alert(this.value);
					}else{
						//从Tree.JSONValue中删除选中的地区
						Tree.DeleteRegionFromJSONValue(this.value);
						
						//点击当前结点非选中 并且当前结点下还有子结点 则设置其下属结点全部为非选中
						if(this.getAttribute("btnode")=="false"){
							for(var k=0; k<tempcheckbox.length; k++){if(tempcheckbox[k].value.substring(0,this.value.length) == this.value){tempcheckbox[k].checked=false;}}
						}
						//检查当前结点的所有直属上级结点下的结点是否有选中的，若所有都为未选中时则设置其也为未选中
						var tempvalue=this.value;
						for(var j=p_nodes.length-2;j>1;j--){
							tempvalue=tempvalue.replace(p_nodes[j]+",","");
							value = tempvalue;
							Check = false;
							//检查所有下属结点是否有选中的，有选中的则Check=true;
							for(var k=0; k<tempcheckbox.length; k++){
								if(tempcheckbox[k].value.substring(0,value.length) == value && tempcheckbox[k].value != value && tempcheckbox[k].checked==true){Check=true;}
							}
							//如果所有下属结点都未选中,则设置其为未选中
							if(!Check){
								for(var k=0; k<tempcheckbox.length; k++){
									if(tempcheckbox[k].value == value){
										tempcheckbox[k].checked=false;
										///从Tree.JSONValue中删除此父结点的地区数据
										Tree.DeleteRegionFromJSONValue(tempcheckbox[k].value);
									}
								}
							}
						}
						
					}
					//从新保存数据到Tree.value
					Tree.value = KnifeCMS.JSON.toJSONString(Tree.JSONValue);
				}
				//document.getElementById("treejsonvalue").innerHTML=KnifeCMS.JSON.toJSONString(Tree.JSONValue);
			}
		}
	}
	
	//$:添加选中的地区到Tree.JSONValue
	this.AddRegionToJSONValue = function(Region_Path){
		if(!(Region_Path=="," || Region_Path===false || Region_Path===true || KnifeCMS.IsNull(Region_Path))){
			var arrPath  = Region_Path.split(",");
			var pPath    = "";
			var tempPath = ",";
			var PathJSON = "";
			for(var ii=1;ii<arrPath.length-1;ii++){
				pPath    = tempPath;
				tempPath = tempPath + arrPath[ii] +",";
				PathJSON = {"p":""+tempPath+"","c":[]};
				if(ii==1){
					KnifeCMS.IsInRegion=false;
					KnifeCMS.CheckIsInRegion(tempPath,Tree.JSONValue);
					if(KnifeCMS.IsInRegion!=true){
						//alert("KnifeCMS.IsInRegion="+ KnifeCMS.IsInRegion +"  tempPath="+tempPath);
						Tree.JSONValue.push(PathJSON);
					}
				}else{
					KnifeCMS.IsInRegion=false;
					KnifeCMS.CheckIsInRegion(tempPath,Tree.JSONValue);
					if(KnifeCMS.IsInRegion!=true){
						//alert("KnifeCMS.IsInRegion="+ KnifeCMS.IsInRegion +"  tempPath="+tempPath   +"PathJSON="+ KnifeCMS.JSON.toJSONString(PathJSON));
						Tree.AddRegionJSONToJSONValue(pPath,Tree.JSONValue,PathJSON);
					}
				}
			}//alert(KnifeCMS.JSON.toJSONString(Tree.JSONValue));
		}
	}
	//$:添加选中的地区JSON格式数据到Tree.JSONValue(json操作)
	this.AddRegionJSONToJSONValue = function(Region_Path,BV_Region,RegionJSON){
		for(var kk=0;kk<BV_Region.length;kk++){
			if(BV_Region[kk].p==Region_Path){
				BV_Region[kk].c.push(RegionJSON);
				break;
			}else{
				if(Region_Path.indexOf(BV_Region[kk].p)>=0){Tree.AddRegionJSONToJSONValue(Region_Path,BV_Region[kk].c,RegionJSON);}
			}
		}
	}
	this.DeleteRegionFromJSONValue = function(Region_Path){
		Tree.DeleteRegionJSONFromJSONValue(Region_Path,Tree.JSONValue);//alert(KnifeCMS.JSON.toJSONString(Tree.JSONValue));
	}
	//从Tree.JSONValue中删除选中的地区JSON格式数据(json操作)
	this.DeleteRegionJSONFromJSONValue = function(Region_Path,BV_Region){
		for(var kk=0;kk<BV_Region.length;kk++){
			if(BV_Region[kk].p==Region_Path){
				KnifeCMS.JSON.Delete(kk,BV_Region)
				break;
			}else{
				if(Region_Path.indexOf(BV_Region[kk].p)>=0){Tree.DeleteRegionJSONFromJSONValue(Region_Path,BV_Region[kk].c);}
			}
		}
	}
	
}
function class_Navigator(){
	this.ChangeUrl = function(this_){
		var NavUrl=document.getElementById("NavUrl");
		if(objExist(NavUrl)){
			var Num=this_.options[this_.selectedIndex].value;
			var ContentSubSystem,ClassObject,SubSystem,SeoUrl,URL;
			if(this_.id=="ContentSubSystem"){
				ContentSubSystem = document.getElementById("ContentSubSystem");
				SubSystem        = ContentSubSystem.options[ContentSubSystem.selectedIndex].value;
				SeoUrl           = ContentSubSystem.options[ContentSubSystem.selectedIndex].getAttribute("seourl");
				NavUrl.value = URL_Pre + SystemPath + "?content/"+SubSystem+"/"+SeoUrl+FileSuffix;
			}else if(this_.id=="ClassID"){
				ContentSubSystem = document.getElementById("ContentSubSystem");
				SubSystem        = ContentSubSystem.options[ContentSubSystem.selectedIndex].value;
				ClassObject      = document.getElementById("ClassID");
				SeoUrl           = ClassObject.options[ClassObject.selectedIndex].getAttribute("seourl");
				if(SeoUrl==""){
					SeoUrl=ContentSubSystem.options[ContentSubSystem.selectedIndex].getAttribute("seourl");
				}
				if(!(Num>0)){
					URL=SubSystem+"/"+SeoUrl+FileSuffix;
				}else{
					URL=SubSystem+"/category/"+Num+"/"+SeoUrl+FileSuffix;
				}
				NavUrl.value = URL_Pre + SystemPath + "?content/"+URL;
			}else if(this_.id=="GoodsClassID"){
				ClassObject   = document.getElementById("GoodsClassID")
				SeoUrl        = ClassObject.options[ClassObject.selectedIndex].getAttribute("seourl");
				if(KnifeCMS.IsNull(SeoUrl)){SeoUrl="";}
				if(Num>0){
					URL="/category/"+Num+"/"+SeoUrl+FileSuffix;
				}else{
					URL=SeoUrl+FileSuffix;
				}
				NavUrl.value  = URL_Pre + SystemPath + "?shop"+URL;
			}
		}
	}
	//$:保存
	this.SaveNavigatorCheck = function(){
		var Check=true;
		if(!Admin.isHaveSelectForNone("NavType","d_NavType",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.CheckItem("NavName","d_NavName",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("NavUrl","d_NavUrl",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
	//分类批量编辑时对表单数据检查
	this.BatchEdit = function(){
		var inputOrs=document.forms["SaveForm"].elements["Order"];
		var inputNas=document.forms["SaveForm"].elements["NavName"];
		var inputValue;
		var Check=false;
		var Check1=false;
		var Check2=false;
		Admin.DataSubmiting();
		if(inputOrs!=null){
			for (var i=0;i<inputOrs.length;i++)
			{
				inputValue=inputOrs[i].value;
				inputValue=inputValue.replace(/[^0-9]*/g,"");
				inputOrs[i].value=inputValue;
			}
			Check1=true;
		}
		if(inputNas!=null){
			for (var i=0;i<inputNas.length;i++)
			{
				inputValue=inputNas[i].value;
				inputValue=inputValue.replace(/[,]*/g,"");
				inputNas[i].value=inputValue;
			}
			Check2=true;
		}
		Check=Check1&&Check2;
		if(Check){
			document.forms["SaveForm"].submit()
		}
	}
	this.ParNavChange = function(Action){
		var this_=document.getElementById("P_NavPath");
		var this_navtype = this_.options[this_.selectedIndex].getAttribute("navtype");
		var NavType = document.getElementById("NavType");
		if(parseInt(this_navtype)>0){
			for(var i=0;i<NavType.options.length;i++){
				if(parseInt(NavType.options[i].value)==parseInt(this_navtype)){
					NavType.options[i].selected=true;
				}else{
					NavType.options[i].selected=false;
				}
			}
			NavType.disabled=true;
		}else{
			if(Action!="edit"){NavType.disabled=false;}
		}
	}
	this.SetIsShow = function(_this){
		var id,label,value,imgsrc,imgnewsrc,newvalue;
		id        = _this.getAttribute("id");
		imgsrc    = _this.src;
		value     = parseInt( _this.getAttribute("value"));
		_this.src = "image/loading.gif";
		label = _this.getAttribute("label");
		if(value>0){
			newvalue     = 0;
			imgnewsrc = "image/icon_yes.png";
		}else{
			newvalue     = 1;
			imgnewsrc = "image/icon_yes_gray.png";
		}
		var ajaxString = "value="+newvalue;
		var ajaxUrl    = "ajax.get.asp?ctl="+_this.getAttribute("ctl")+"&act=edit&id="+id+"&subact=setisshow&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		if(ajaxSta=="ok"){
			_this.setAttribute("value",newvalue);
			_this.src = imgnewsrc;
		}else{
			if(ajaxSta.indexOf(Lang_NoPermissions)>0){alert(Lang_NoPermissions);}
			_this.src = imgsrc;
		}
	}
	
}
function class_Ads(){
	var Ads = this;
	//$:保存广告位
	this.SaveAdsGroupCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("AdsGroupName","d_AdsGroupName",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("Identify","d_Identify",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("StartTime","d_StartTime",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("EndTime","d_EndTime",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("Identify","d_Width",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("Identify","d_Height",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
	//$:保存
	this.SaveAdsDetailCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("ImageUrl","d_ImageUrl",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("Identify","d_Identify",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("StartTime","d_StartTime",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("EndTime","d_EndTime",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("Identify","d_Width",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("Identify","d_Height",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
			OSWebEditor.sync();
			document.forms["SaveForm"].submit()
		}
	}
	this.AdsImageUrlChange = function(){
		Admin.ImageUrlChange('AdsImageBox','ImageUrl','AdsImage','d_ImageUrl');
	}
	this.SelectAdsImage = function(){
		var NewWindow;
		var Input=document.getElementById("ImageUrl");
		if(objExist(Input)){
			NewWindow=window.showModalDialog('fileupload.asp?ctl=file_image&act=view&insertnum=1&inserttype=url',window,'dialogWidth:740px;dialogHeight:550px;center:Yes;help:no;resizable:yes;scroll:yes;');
			if(NewWindow!=""){
				Input.value=unescape(NewWindow);
				Ads.AdsImageUrlChange();
			}
		}
		else{
			alert("Error:SelectAdsImage(){id=ImageUrl}");
		}
	}
	this.ImageView_GetUrl = function(this_){
		var Images = this_.getElementsByTagName("img");
		if(Images.length>0){
			var URL=Images[0].src;
			var fn_DialogID   ="ImageView_GetUrl";
			var fn_DialogTitle=Lang_Js_ImageViewAndGetUrl;
			var fn_contentHTML="<div class=\"fixbox\" style=\"width:994px; height:530px; padding:5px 5px 5px 5px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"infoTable\"><tbody><tr><td class=\"titletd\">"+Lang_Js_ImageURL+":</td><td class=\"infotd\" colspan=\"3\"><input type=\"text\" class=\"TxtClass\" style=\"width:560px;\" value=\""+URL+"\" /></td></tr><tr><td class=\"titletd\">"+Lang_Js_ImageWidth+":</td><td class=\"infotd\"><div id=imageWidth></div></td><td class=\"titletd\">"+Lang_Js_ImageHeight+":</td><td class=\"infotd\"><div id=imageHeight></div></td></tr></table><div class=mt5><img src=\""+URL+"\" id=imageView /></div></div><div class=\"actline mt5\"><button type=\"reset\" class=\"sysSaveBtn\" id=\"ReSetBtn\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"')\"><b class=\"icon icon_close\"></b>"+Lang_Js_Btn_Close+"</button></div>"
			KnifeCMS.CreateDiaWindow(1020,600,fn_DialogID,fn_DialogTitle,fn_contentHTML,true);
			var imageView = document.getElementById("imageView");
			var imageWidth = document.getElementById("imageWidth");
			var imageHeight = document.getElementById("imageHeight");
			if(objExist(imageView) && objExist(imageWidth) && objExist(imageHeight)){
				imageWidth.innerHTML=imageView.width + "px";
				imageHeight.innerHTML=imageView.height + "px";
			}
		}
	}
}
function class_Comment(){
	this.SaveCommentCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("commenttextarea","d_commenttextarea",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
}
function class_Order(){
	//$:
	this.OrderGoodsDelete = function(_this,orderid,itemid){
		var fn_DialogID = KnifeCMS.DoingDiaWindow();
		var ajaxString = "itemid="+itemid+"&orderid="+orderid;
		var ajaxUrl    = "ajax.get.asp?ctl=order&act=edit&subact=goodsdel&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		if(ajaxSta=="ok"){
			KnifeCMS.DiaWindowCloseByID(fn_DialogID);
			DeleteCurrentRow(_this);
			alert("ok");
		}else{
			KnifeCMS.DiaWindowCloseByID(fn_DialogID);
			alert("wrong");
		}
	}
	this.OrderPayWindow = function(ID){
		var fn_DialogID = KnifeCMS.CreateID();
		var fn_Title    = Js_Order[1];
		var fn_HTML     = "";
		var fn_Btn      = "<input type=\"button\" class=\"sysBtn ml5\" onclick=\"Admin.Order.OrderPaySave()\" value="+Lang_Js_Btn_Confirm+"><input type=\"button\" class=\"sysBtn ml5\" onclick=\"window.location.reload();KnifeCMS.DiaWindowCloseByID('"+fn_DialogID+"');\" value="+Lang_Js_Btn_Close+">";
		KnifeCMS.CreateDiaWindow(800,520,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,true);
		
	}
	//$:订单支付提交保存时检查
	this.SaveOrderPayCheck = function(){
		var Check=true;
		if(!Admin.isHaveSelect("paytype","d_paytype",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.isHaveSelect("paymentid","d_paymentid",Lang_Js_HaveNotSelect)){Check=false}
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
	//$:订单退款提交保存时检查
	this.SaveOrderRefundCheck = function(){
		var Check=true;
		if(!Admin.isHaveSelect("paytype","d_paytype",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.isHaveSelect("paymentid","d_paymentid",Lang_Js_HaveNotSelect)){Check=false}
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
	//$:订单发货提交保存时检查
	this.SaveOrderShipCheck = function(){
		var Check=true;
		if(!Admin.isHaveSelect("shippingid","d_shippingid",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.isHaveSelect("logi_id","d_logi_id",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.CheckItem("ship_name","d_ship_name",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("ship_address","d_ship_address",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
	//$:订单退货提交保存时检查
	this.SaveOrderReShipCheck = function(){
		var Check=true;
		if(!Admin.isHaveSelect("shippingid","d_shippingid",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.isHaveSelect("logi_id","d_logi_id",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.CheckItem("ship_name","d_ship_name",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("ship_address","d_ship_address",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
}
function class_UC(){
	this.ucAjax = function(){}
}
function class_User(){
	this.SaveUserCheck = function(){
		var Check=true;
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
	this.EditPassword = function(UserID){
		var fn_DialogID = KnifeCMS.CreateID();
		var fn_Title    = Lang_Js_ChangePassword;
		var fn_HTML     = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"infoTable\"><tr><td class=\"titletd\">"+ Lang_Js_InputNewPassword +":</td><td class=\"infotd\"><input type=\"password\" maxlength=\"20\" class=\"TxtClass\" id=\"password\" name=\"password\" value=\"\" /><label class=\"Normal\" id=\"d_password\"></label></td></tr><tr><td class=\"titletd\">"+ Lang_Js_NewPasswordConfirm +":</td><td class=\"infotd\"><input type=\"password\" maxlength=\"20\" class=\"TxtClass\" id=\"repassword\" name=\"repassword\" value=\"\" /><label class=\"Normal\" id=\"d_repassword\"></label></td></tr></table>";
		var fn_Btn      = "<input type=\"button\" class=\"sysSaveBtn\" onclick=\"Admin.User.SavePassword('"+ UserID +"','"+ fn_DialogID +"')\" value="+Lang_Js_Btn_Confirm+"><input type=\"button\" class=\"sysSaveBtn ml5\" onclick=\"KnifeCMS.DiaWindowCloseByID('"+ fn_DialogID +"')\" value="+Lang_Js_Btn_Close+">";
		KnifeCMS.CreateDiaWindow(600,200,fn_DialogID,fn_Title,Array(fn_HTML,fn_Btn),true,true);
	}
	this.SavePassword = function(UserID,DialogID){
		var password    = KnifeCMS.$id("password").value;
		var repassword  = KnifeCMS.$id("repassword").value;
		var Check=true;
		if(!Admin.CheckItem("password","d_password",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("repassword","d_repassword",Lang_Js_ThisCannotEmpty)){Check=false};
		if(password!=repassword){
			Check=false;
			KnifeCMS.$id("d_repassword").className = "d_err";
			KnifeCMS.$id("d_repassword").innerHTML = Lang_Js_TwoPasswordsIsNotAccord ;
		}
		if(password.length<6 || password.length>20){
			Check=false;
			KnifeCMS.$id("d_password").className = "d_err";
			KnifeCMS.$id("d_password").innerHTML = Lang_Js_PasswordLengthWrong ;
		}
		if(Check){
			var fn_DialogID = KnifeCMS.DoingDiaWindow();
			var ajaxString  = "userid="+UserID+"&password="+password+"&repassword="+repassword;
			var ajaxUrl     = "ajax.get.asp?ctl=user&act=edit&subact=savepassword&r="+parseInt(Math.random()*1000);
			var ajaxSta     = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
			if(ajaxSta=="ok"){
				KnifeCMS.DiaWindowCloseByID(fn_DialogID);
				KnifeCMS.DiaWindowCloseByID(DialogID);
				alert(Lang_Js_ChangePasswordSuccess);
			}else{
				KnifeCMS.DiaWindowCloseByID(fn_DialogID);
				alert(Lang_Js_ChangePasswordFail + "("+ ajaxSta +")");
			}
		}
	}
}
var Admin = new class_Admin()




























