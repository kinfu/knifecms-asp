<!--#include file="config.admin.asp"-->
<%
Dim AjaxGet
Set AjaxGet = New Class_KnifeCMS_Admin_Ajax_Get
Set AjaxGet = Nothing
Class Class_KnifeCMS_Admin_Ajax_Get
	
	Private Pv_Rs,Pv_ContentSubSystem,Pv_IsCategory,Pv_TempArr,Pv_Ran
	Private S_TempHTML
	
	Private Sub Class_Initialize()
		Pv_TempArr=Split(Ctl,"_")
		If Ubound(Pv_TempArr) > 0 Then Pv_ContentSubSystem = KnifeCMS.Data.Left(KnifeCMS.Data.FiltNotLetter(Pv_TempArr(1)),50)
		If Ubound(Pv_TempArr) > 1 Then
			If Pv_TempArr(1)="class" Then Pv_IsCategory = True
		End If
		Pv_Ran = KnifeCMS.Data.CLng(KnifeCMS.GetForm("get","r"))
		If Pv_Ran < 1 Or Pv_Ran > 1000 Then Exit Sub
		If Left(Ctl,7) = "content" Then
			If Action="edit" Then
				If SubAction = "setlabel" Then
					Echo Admin.Ajax.SetContentLabel()
				ElseIf SubAction = "setispagecontent" Then
					Echo Admin.Ajax.SetCategoryIsPageContent()
				ElseIf SubAction = "setisshow" Then
					Echo Admin.Ajax.SetCategoryIsShow()
				End If
			Else
				Admin.Ajax.GetSubSystemClassList 0,Pv_ContentSubSystem
			End If
		Else
			Select Case Ctl
				Case "file_image","file_file","file_flash","file_music","file_media"
					If Action="view" Then
						Echo Admin.Ajax.GetAttachment()
					ElseIf Action="del" Then
						Echo Admin.Ajax.DeleteFoldersAndFiles()
					ElseIf Action="check" Then
						Echo Admin.Ajax.RedundantFoldersAndFiles()
					End If
				Case "systembackup"
					If Action="del" Then
						Echo Admin.Ajax.DeleteBackupData()
					End If
				Case "fieldset"
					If Action="view" Then
						If SubAction = "checkfield" Then
							Echo Admin.Ajax.CheckFieldIsExist()
						End If
					End If
				Case "goodsfieldset"
					If Action="view" Then
						If SubAction = "checkfield" Then
							Echo Admin.Ajax.CheckGoodsFieldIsExist()
						End If
					End If
				Case "navigator"
					If Action="edit" Then
						If SubAction = "setisshow" Then
							Echo Admin.Ajax.SetNavigatorIsShow()
						End If
					End If
				Case "vote_class"
					If Action="edit" Then
						If SubAction = "setispagecontent" Then
							Echo Admin.Ajax.SetCategoryIsPageContent()
						ElseIf SubAction = "setisshow" Then
							Echo Admin.Ajax.SetCategoryIsShow()
						End If
					End If
				Case "link"
					If Action = "edit" Then
						If SubAction = "setlabel" Then
							Echo Admin.Ajax.SetLinkLabel()
						End If
					End If
				Case "cmodule"
					If Action = "edit" Then
						If SubAction = "setlabel" Then
							Echo Admin.Ajax.SetCModuleLabel()
						End If
					End If
				Case "goods"
					If SubAction = "checkgoodsbn" Then 
						Dim HaveUsered
						HaveUsered = Admin.IsGoodsBNHaveUsered(KnifeCMS.GetForm("get","GoodsBN"),KnifeCMS.GetForm("get","id"))
						If HaveUsered Then
							Echo "Error"
						Else
							Echo "ok"
						End If
					End If
					If Action = "edit" Then
						If SubAction = "setlabel" Then
							Echo Admin.Ajax.SetGoodsLabel()
						End If
					End If
				Case "goods_class","goodsvirtual_class"
					If Action="edit" Then
						If SubAction = "setispagecontent" Then
							Echo Admin.Ajax.SetCategoryIsPageContent()
						ElseIf SubAction = "setisshow" Then
							Echo Admin.Ajax.SetCategoryIsShow()
						End If
					End If
				Case "goodsmodel"
					If SubAction = "getgoodsmodelattribute" Then Admin.Ajax.GetGoodsModelAttribute(KnifeCMS.GetForm("get","id"))
				Case "regionset"
					If SubAction = "getregions" Then Admin.Ajax.GetRegions(KnifeCMS.GetForm("get","regionpath"))
					If (Action ="add" Or Action ="edit") And SubAction = "save" Then Admin.Ajax.RegionsDoSave
					If Action ="del" Then Admin.Ajax.RegionsDel
				Case "user"
					If Action = "edit" And SubAction = "savepassword" Then Echo Admin.Ajax.SaveUserPassword()
				Case Else : Echo "Error"
			End Select
		End If
	End Sub
	
	
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
End Class
%>