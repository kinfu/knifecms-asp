<!--#include file="config.admin.asp"-->
<%
Dim RefundBill
Set RefundBill = New Class_KnifeCMS_Admin_RefundBill
Set RefundBill = Nothing
Class Class_KnifeCMS_Admin_RefundBill

	Private Pv_OrderID,Pv_UserID,Pv_Currency,Pv_Status,Pv_PaymentID
	Private Pv_Username,Pv_SysUsername
	Private Pv_BillID,Pv_Bank,Pv_Account,Pv_PayAccount,Pv_Money,Pv_PayCost,Pv_CurMoney,Pv_PayType,Pv_PaymentName,Pv_OperaterID,Pv_IP,Pv_TReady,Pv_TSent,Pv_Remarks,Pv_Disabled,Pv_TradeNo
	Private Pv_PaymentData
	Private Pv_KeywordsType,Pv_PriceFrom,Pv_PriceTo
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "RefundBill.lang.asp"
		IsContentExist = False
		Header()
		If Ctl = "refundbill" Then
			Pv_BillID = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","billid"),"[^0-9]",""),16)
			Select Case Action
				Case "view"
					If SubAction = "detail" Then
						Call GetRefundBillDetailData()
						If IsContentExist Then RefundBillDetailView : Else Admin.ShowMessage Lang_RefundBill_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
					Else
						Call RefundBillGetSearchCondition()
						Call RefundBillView()
					End If
				Case "del"
				    Call RefundBillDel()
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"?ctl="& Ctl &"&act=view",1
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function GetRefundBillDetailData()
		Set Rs = KnifeCMS.DB.GetRecord(DBTable_RefundBill,Array("ID:"& ID),"")
		If Not(Rs.Eof) Then
			IsContentExist = True
			SysID           = Rs("SysID")
			Pv_BillID       = Rs("BillID")
			Pv_OrderID      = Rs("OrderID")
			Pv_UserID       = KnifeCMS.Data.CLng(Rs("UserID"))
			Pv_Bank         = Rs("Bank")
			Pv_Account      = Rs("Account")
			Pv_PayAccount   = Rs("PayAccount")
			Pv_Currency     = Rs("Currency")
			Pv_Money        = Rs("Money")
			Pv_PayType      = Rs("PayType")
			Pv_PaymentName  = Rs("PaymentName")
			Pv_OperaterID   = Rs("OperaterID")
			Pv_IP           = Rs("IP")
			Pv_TReady       = Rs("TReady")
			Pv_TSent        = Rs("TSent")
			Pv_Status       = Rs("Status")
			Pv_Remarks      = Rs("Remarks")
			Pv_Disabled     = Rs("Disabled")
		Else
			IsContentExist = False
		End If
		If Pv_UserID>0 Then Pv_Username = KnifeCMS.DB.GetFieldByID(DBTable_Members,"Username",Pv_UserID)
		If Pv_OperaterID>0 Then Pv_SysUsername = KnifeCMS.DB.GetFieldByID(DBTable_SysUser,"Username",Pv_OperaterID)
	End Function
	
	Private Function RefundBillGetSearchCondition()
		Pv_PriceFrom = KnifeCMS.GetForm("both","PriceFrom") : If Not(KnifeCMS.Data.IsNul(Pv_PriceFrom)) Then Pv_PriceFrom = KnifeCMS.Data.FormatDouble(Pv_PriceFrom)
		Pv_PriceTo   = KnifeCMS.GetForm("both","PriceTo")   : If Not(KnifeCMS.Data.IsNul(Pv_PriceTo))   Then Pv_PriceTo   = KnifeCMS.Data.FormatDouble(Pv_PriceTo)
		Pv_PaymentID    = KnifeCMS.GetForm("both","paymentid[]")
		Pv_KeywordsType = KnifeCMS.GetForm("both","keywordstype")
	End Function
	Private Function RefundBillSearchSql()
		Dim Fn_TempSql
		Pv_PaymentID = ParseSearchData(Pv_PaymentID)
		Pv_PriceFrom = KnifeCMS.Data.FormatDouble(Pv_PriceFrom)
		Pv_PriceTo  = KnifeCMS.Data.FormatDouble(Pv_PriceTo)
		Fn_TempSql  = Fn_TempSql & KnifeCMS.IIF(KnifeCMS.Data.IsNul(Pv_PaymentID) Or Pv_PaymentID="any",""," AND a.PaymentID IN ("& Pv_PaymentID &")" )
		Fn_TempSql  = Fn_TempSql & KnifeCMS.IIF(Pv_PriceFrom > 0 ," AND isnull(a.Money,0)>="& Pv_PriceFrom &" "," ")
		Fn_TempSql  = Fn_TempSql & KnifeCMS.IIF(Pv_PriceTo   > 0 ," AND isnull(a.Money,0)<="& Pv_PriceTo &" "  ," ")
		Select Case Pv_KeywordsType
			Case "billid"
				Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Admin.Search.KeyWords,"[^a-zA-Z0-9]",""),20)
				Fn_TempSql = Fn_TempSql & " AND a.BillID LIKE '%"& Admin.Search.KeyWords &"%'"
			Case "orderid"
				Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Admin.Search.KeyWords,"[^0-9]",""),20)
				Fn_TempSql = Fn_TempSql & " AND a.OrderID LIKE '%"& Admin.Search.KeyWords &"%'"
			Case "bank"
				Fn_TempSql = Fn_TempSql & " AND a.Bank LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%'"
			Case "account"
				Fn_TempSql = Fn_TempSql & " AND a.Account LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%'"
			Case "payaccount"
				Fn_TempSql = Fn_TempSql & " AND a.PayAccount LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%'"
			Case "membername"
				Fn_TempSql = Fn_TempSql & " AND a.UserID IN (SELECT ID FROM ["& DBTable_Members &"] WHERE Username LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%')"
			Case "operater"
				Fn_TempSql = Fn_TempSql & " AND a.OperaterID IN (SELECT ID FROM ["& DBTable_SysUser &"] WHERE Username LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%')"
		End Select
		RefundBillSearchSql = Fn_TempSql
	End Function
	Private Function ParseSearchData(ByVal BV_Data)
		Dim Fn_ii,Fn_ReturnString,Fn_TempArray,Fn_Temp
		Fn_TempArray    = Split(BV_Data,",")
		Fn_ReturnString = ""
		For Fn_ii=0 To Ubound(Fn_TempArray)
			If Fn_TempArray(Fn_ii)="any" Then
				Fn_ReturnString="any"
				Exit For
			Else
				Fn_Temp = KnifeCMS.Data.Clng(Fn_TempArray(Fn_ii))
				If Fn_Temp>=0 Then
					If Fn_ReturnString="" Then Fn_ReturnString = Fn_Temp : Else Fn_ReturnString = Fn_ReturnString &","& Fn_Temp
				End If
			End If
		Next
		ParseSearchData = Fn_ReturnString
	End Function
	
	Private Function RefundBillList()
		Dim Fn_TempSql,Fn_ColumnNum
		Select Case DB_Type
		Case 0
		Fn_TempSql = "SELECT a.ID,a.BillID,a.Money,a.Currency,a.OrderID,a.PaymentName,a.Bank,a.Account,a.TSent,b.Username,a.PayAccount,c.Username FROM (["& DBTable_RefundBill &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.UserID)) LEFT JOIN ["& DBTable_SysUser &"] c ON (c.ID=a.OperaterID)"
		Case 1
		Fn_TempSql = "SELECT a.ID,a.BillID,a.Money,a.Currency,a.OrderID,a.PaymentName,a.Bank,a.Account,a.TSent,b.Username,a.PayAccount,c.Username FROM ["& DBTable_RefundBill &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.UserID) LEFT JOIN ["& DBTable_SysUser &"] c ON (c.ID=a.OperaterID)"
		End Select
			
			Fn_ColumnNum = 12
			Operation = Operation & "<span class=""sysBtn""><a href=""javascript:;"" onClick=""ShowModal('?ctl="&Ctl&"&act=view&subact=detail&id={$ID}',800,480);""><b class=""icon icon_view""></b>"& Lang_DoView &"</a></span>"
		If SubCtl = "recycle" Then
			Fn_TempSql = Fn_TempSql & " WHERE a.Recycle=1 ORDER BY a.ID DESC"
			Operation = ""
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,"")
		Else
			Fn_TempSql = Fn_TempSql & " WHERE a.Recycle=0 "
			If SubAction = "search" Then
			
				Select Case DB_Type
				Case 0
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.TSent>=#"& Admin.Search.StartDate &"# ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.TSent<=#"& Admin.Search.EndDate &"# ","")
				Case 1
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.TSent>='"& Admin.Search.StartDate &"' ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.TSent<='"& Admin.Search.EndDate &"' ","")
				End Select
				
				tempCondition = tempCondition & RefundBillSearchSql
				PageUrlPara = PageUrlPara & "&startdate="& Server.URLEncode(Admin.Search.StartDate) &"&enddate="& Server.URLEncode(Admin.Search.EndDate) &"&keywords="& Server.URLEncode(Admin.Search.KeyWords)&"&keywordstype="& Pv_KeywordsType &"&PriceFrom="& Pv_PriceFrom &"&PriceTo="& Pv_PriceTo &""
				Fn_TempSql = Fn_TempSql & tempCondition
			End If
			Fn_TempSql = Fn_TempSql &" ORDER BY a.ID DESC"
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
		End If
	End Function
	
	Private Function RefundBillDel()
		Call RefundBillSubDo()
	End Function
	
	'删除/彻底删除/还原
	Private Sub RefundBillSubDo()
		Dim Fn_ArrID,Fn_i,Fn_BillID
		ID       = KnifeCMS.GetForm("post","InputName")
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			Result     = 0
			Fn_BillID  = ""
			If ID > 0 Then
				Fn_BillID = KnifeCMS.DB.GetFieldByID(DBTable_RefundBill,"BillID",ID)
				If Not(KnifeCMS.Data.IsNul(Fn_BillID)) Then
					If Action = "del" Then
						If SubAction = "completely" Then
							Result = KnifeCMS.DB.DeleteRecord(DBTable_RefundBill,Array("ID:"&ID))
							If 1=Result Then Msg    = Msg & Lang_RefundBill_Cue(4) &"{[billid="& Fn_BillID &"]}<br>"
						End If
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Sub
	
	Private Function RefundBillView()
%>
	<%=Admin.Calendar(Array("","",""))%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
          <td class="tdcell">
            <table border="0" cellpadding="2" cellspacing="0">
            <tr><td class="upcell">
                <div class="align-center grey">
                    <% Echo Lang_DistrictDoSearch %><!--搜索操作区-->
                    <span class="AdvancedSearch"><a href="javascript:Admin.AdvancedSearch.Open(600,324,'AdvancedSearch')" ><% Echo Lang_AdvancedSearch %></a></span>
                </div>
                <DIV id="AdvancedSearch" style="display:none;">
                    <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                    <div class="diainbox">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                      <tr><td class="titletd"><% Echo Lang_Keywords %>:</td>
                          <td class="infotd">
                          <select name="keywordstype">
                          <% Echo OS.CreateOption(Array("billid:"& Lang_RefundBill_ListTableColLine(1),"orderid:"& Lang_RefundBill_ListTableColLine(4),"bank:"& Lang_RefundBill_ListTableColLine(6),"account:"& Lang_RefundBill_ListTableColLine(7),"membername:"& Lang_RefundBill_ListTableColLine(9),"payaccount:"& Lang_RefundBill_ListTableColLine(10),"operater:"& Lang_RefundBill_ListTableColLine(11)),Split(Pv_KeywordsType,",")) %>
                          </select>
                          <input type="text" class="TxtClass" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:306px;" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_RefundBill_ListTableColLine(5) %><!--支付方式-->:</td><td class="infotd">
                          <% 
						  Dim Fn_PaymentString : Fn_PaymentString = Admin.GetListAsArrayList(DBTable_Payments &":ID,PaymentName","Disabled=0 ","ORDER BY OrderNum ASC,ID ASC")
						  If KnifeCMS.Data.IsNul(Fn_PaymentString) Then
							  Execute("Pv_PaymentData = Array(""any:"& Lang_AnyPayment &""")")
						  Else
						      Execute("Pv_PaymentData = Array(""any:"& Lang_AnyPayment &""","& Fn_PaymentString &")")
						  End If
						  %>
                          <select name="paymentid[]" id="paymentid[]" multiple="multiple" size="5" style="width:200px;">
                          <% Echo OS.CreateOption(Pv_PaymentData,Split(Pv_PaymentID,",")) %>
                          </select>
                          </td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_RefundBill_ListTableColLine(2) %><!--支付金额-->:</td>
                            <td class="infotd"><% Echo Lang_From %><!--从--> <input type="text" id="PriceFrom" name="PriceFrom" class="TxtClass" style="width:51px;" value="<% Echo Pv_PriceFrom %>" /> <% Echo Lang_FromTo %><!--到--> <input type="text" id="PriceTo" name="PriceTo" class="TxtClass" style="width:51px;" value="<% Echo Pv_PriceTo %>" /></td></tr>
                      <tr><td class="titletd"><% Echo Lang_StartDate %>:</td>
                          <td class="infotd"><input type="text" class="TxtSer dateStart" id="startdate" name="startdate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' value="<%=Admin.Search.StartDate%>" style="width:200px;" /></td>
                      <tr><td class="titletd"><% Echo Lang_EndDate %>:</td>
                          <td class="infotd"><input type="text" class="TxtSer dateEnd" id="enddate" name="enddate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' value="<%=Admin.Search.EndDate%>" style="width:200px;" /></td>
                      </tr>
                      </table>
                      </div>
                    <% Echo AdvancedSearchBtnline %>
                    </form>
                </DIV>
                </td></tr>
            <tr><td class="downcell">
                <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                <table border="0" cellpadding="0" cellspacing="0"><tr>
                <td><span><% Echo Lang_Keywords %>:</span></td>
                <td class="pl3">
                <select name="keywordstype">
                <% Echo OS.CreateOption(Array("billid:"& Lang_RefundBill_ListTableColLine(1),"orderid:"& Lang_RefundBill_ListTableColLine(4),"bank:"& Lang_RefundBill_ListTableColLine(6),"account:"& Lang_RefundBill_ListTableColLine(7),"membername:"& Lang_RefundBill_ListTableColLine(9),"payaccount:"& Lang_RefundBill_ListTableColLine(10),"operater:"& Lang_RefundBill_ListTableColLine(11)),Split(Pv_KeywordsType,",")) %>
                </select>
                </td>
                <td class="pl3"><input type="text" class="TxtClass" name="keywords" id="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:150px;" /></td>
                <td class="pl3"><button type="submit" class="sysBtn" ><b class="icon icon_search"></b><% Echo Lang_Btn_Search %></button></td>
                </tr></table>
                </form>
            </td></tr>
            </table>
          </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th width="1%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
			<th width="14%"><div style="width:60px;"><% Echo Lang_RefundBill_ListTableColLine(1) %><!--支付单号--></div></th>
            <th width="8%"><div style="width:56px; "><% Echo Lang_RefundBill_ListTableColLine(2) %><!--支付金额--></div></th>
            <th width="8%"><div style="width:28px; "><% Echo Lang_RefundBill_ListTableColLine(3) %><!--货币--></div></th>
            <th width="14%"><div style="width:56px; "><% Echo Lang_RefundBill_ListTableColLine(4) %><!--订单号--></div></th>
            <th width="8%"><div style="width:56px; "><% Echo Lang_RefundBill_ListTableColLine(5) %><!--支付方式--></div></th>
            <th width="8%"><div style="width:56px; "><% Echo Lang_RefundBill_ListTableColLine(6) %><!--收款银行--></div></th>
            <th width="10%"><div style="width:56px; "><% Echo Lang_RefundBill_ListTableColLine(7) %><!--收款账号--></div></th>
            <th width="10%"><div style="width:80px; "><% Echo Lang_RefundBill_ListTableColLine(8) %><!--支付完成时间--></div></th>
            <th width="8%"><div style="width:66px; "><% Echo Lang_RefundBill_ListTableColLine(9) %><!--会员用户名--></div></th>
            <th width="10%"><div style="width:56px; "><% Echo Lang_RefundBill_ListTableColLine(10) %><!--支付账户--></div></th>
            <th width="8%"><div style="width:56px; "><% Echo Lang_RefundBill_ListTableColLine(11) %><!--操作员--></div></th>
			<th width="8%"><div style="width:60px; "><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% RefundBillList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
    <script type="text/javascript">
	KnifeCMS.SetSelectIndexNumFontWeight(0,KnifeCMS.$id("paymentid[]"));
	TrBgChange("listForm","InputName","oliver","data","select");
	if(objExist(document.getElementById("keywords"))){ReplaceText(document.getElementById("keywords").value,1)}
	function ChangeTdText(){
		var tds=document.getElementById("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			var IsEdit = true;
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td2"){
					TempHTML=KnifeCMS.FormatCurrency(tds[i].innerHTML);
					tds[i].innerHTML = '<% Echo MoneySb %>'+TempHTML
				}
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
	Private Function RefundBillDetailView()
%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
          <div class="obox">
            <div class="tit"><% Echo Lang_RefundBill(1) %><!--收款单信息--></div>
            <div class="inbox">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tbody>
                <tr><td class="titletd"><% Echo Lang_RefundBill(2) %><!--支付单号-->:</td><td class="infotd"><% Echo Pv_BillID %></td><td class="titletd"><% Echo Lang_RefundBill(3) %><!--订单号-->:</td><td class="infotd"><% Echo Pv_OrderID %></td></tr>
                <tr><td class="titletd"><% Echo Lang_RefundBill(4) %><!--收款银行-->:</td><td class="infotd"><% Echo Pv_Bank %></td><td class="titletd"><% Echo Lang_RefundBill(5) %><!--收款帐号-->:</td><td class="infotd"><% Echo Pv_Account %></td></tr>
                <tr><td class="titletd"><% Echo Lang_RefundBill(6) %><!--付款金额-->:</td><td class="infotd"><% Echo MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Money) %></td><td class="titletd"><% Echo Lang_RefundBill(7) %><!--付款人-->:</td><td class="infotd"><% Echo Pv_PayAccount %></td></tr>
                <tr><td class="titletd"><% Echo Lang_RefundBill(8) %><!--收款方式-->:</td><td class="infotd">
				    <select id="paytype" name="paytype">
                    <% Echo OS.CreateOption(Array("0:"& Lang_PleaseSelect &"","alipay:"& Lang_Payment_Alipay(0) &"","offline:"& Lang_Payment_OffLine(0) &"","cod:"& Lang_Payment_COD(0)),Split(Pv_PayType,",")) %>
                    </select>
                    <script type="text/javascript">
					function ShowSelectValue(){
						var selects = document.getElementsByTagName("select");
						var Select,SelectedOption;
						for(var ii=0;ii<selects.length;ii++){
							Select = selects[ii];
							SelectedOption = Select.options[Select.selectedIndex].text;
							Select.parentNode.innerHTML = SelectedOption;
						}
					}
					ShowSelectValue();
                    </script>
					</td><td class="titletd"><% Echo Lang_RefundBill(9) %><!--支付方式-->:</td><td class="infotd"><% Echo Pv_PaymentName %></td></tr>
                <tr><td class="titletd"><% Echo Lang_RefundBill(10) %><!--生成时间-->:</td><td class="infotd"><% Echo Pv_TReady %></td><td class="titletd"><% Echo Lang_RefundBill(11) %><!--付款时间-->:</td><td class="infotd"><% Echo Pv_TSent %></td></tr>
                <tr class="Separated"><td class="titletd"><div></div></td><td class="infotd"><div></div></td><td class="titletd"><div></div></td><td class="infotd"><div></div></td></tr>
                <tr><td class="titletd"><% Echo Lang_RefundBill(12) %><!--会员用户名-->:</td><td class="infotd"><% Echo Pv_Username %></td><td class="titletd"><% Echo Lang_RefundBill(13) %><!--操作员-->:</td><td class="infotd"><% Echo Pv_SysUsername %></td></tr>
                <tr><td class="titletd"><% Echo Lang_RefundBill(14) %><!--备注-->:</td><td class="infotd" colspan="3"><% Echo Pv_Remarks %></td></tr>
                </tbody>
                <tbody>
                <tr class="Separated"><td class="titletd"><div></div></td><td class="infotd"><div></div></td><td class="titletd"><div></div></td><td class="infotd"><div></div></td></tr>
                <tr><td class="titletd"><% Echo Lang_RefundBill(15) %><!--货币-->:</td><td class="infotd"><% Echo Pv_Currency %></td><td class="titletd"><% Echo Lang_RefundBill(18) %><!--支付状态-->:</td><td class="infotd"><% Echo Pv_Status %></td></tr>
                <tr><td class="titletd"><% Echo Lang_RefundBill(19) %><!--IP-->:</td><td class="infotd"><% Echo Pv_IP %></td><td class="titletd"><% Echo Lang_RefundBill(20) %><!--是否有效-->:</td><td class="infotd"><% Echo KnifeCMS.IIF(Pv_Disabled=0,Lang_Yes,Lang_No) %></td></tr>
                </tbody>
                </table>
            </div>
          </div>
      </DIV>
    </DIV>
<%
	End Function
End Class
%>
	