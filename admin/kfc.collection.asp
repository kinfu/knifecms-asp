<!--#include file="config.admin.asp"-->
<%
Function CollectionView()
%>
<DIV id="MainFrame">
  <div class="tagLine"><% Echo NavigationLine %></div>
  <DIV class="cbody">
    <div class="toparea">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td class="tdcell">
        <table border="0" cellpadding="2" cellspacing="0">
        <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
        <tr><td class="downcell">
            <span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=add" ><b class="icon icon_write"></b><% echo Lang_DoAdd %></a></span>
            </td></tr>
        </table>
    </td>
    <td class="tdcell">
        <table border="0" cellpadding="2" cellspacing="0">
        <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
        <tr><td class="downcell">
            <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDel%></button>
            </td></tr>
        </table>
    </td>
    </tr>
    </table>
    </div>
    <div class="tabledata">
    <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
     <tbody>
      <tr class="top">
      <th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
      <th width="3%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %></div></th><!--系统ID-->
      <th width="20%"><div style="width:140px;"><% Echo Lang_Collection_ListTableColLine(0) %></div></th><!--采集器名称-->
      <th width="13%"><div style="width:65px;"><% Echo Lang_Collection_ListTableColLine(1) %></div></th><!--默认导入到-->
      <th width="12%"><div style="width:135px;"><% Echo Lang_ListTableColLine_AddTime %></div></th><!--添加时间-->
      <th width="7%"><div style="width:60px;"><% Echo Lang_Collection_ListTableColLine(2) %></div></th><!--采集结果-->
      <th><div style="width:320px;"><% Echo Lang_ListTableColLine_Do %></div></th><!--操作-->
      </tr>
      <% CollectionList() %>
     </tbody>
    </table>
    </form>
    </div>
  </DIV>
</DIV>
<script type="text/javascript">
TrBgChange("listForm","InputName","oliver","data","select");
</script>
<%
End Function
Function CollectionDo()
%>
<style type="text/css">body{ padding-bottom:37px;}</style>
<DIV id="MainFrame">
  <div class="tagLine"><% Echo NavigationLine %></div>
  <DIV class="cbody">
    <div class="wbox">
    <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
    <ul class="TabsStyle">
       <li class="current" onclick=TabSwitch("MainFrame","li","Tabs",1)><% Echo Lang_Collection(1) %></li><!--采集器基本信息-->
       <li onclick=TabSwitch("MainFrame","li","Tabs",2)><% Echo Lang_Collection(2) %></li><!--列表页面采集设置-->
       <li onclick=TabSwitch("MainFrame","li","Tabs",3)><% Echo Lang_Collection(3) %></li><!--内容页面采集设置-->
    </ul>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable" id="Tabs_1">
      <tbody>
        <tr><td class="titletd"><% Echo Lang_Collection(4) %><!--采集器名称--></td><td class="infotd">
            <input type="text" class="TxtClass slong" id="CollectionName" name="CollectionName" value="<%=KnifeCMS.Data.HTMLDecode(CollectionName)%>" /><label class="Normal" id="d_CollectionName"></label>
        </td></tr>
        <tr><td class="titletd"><% Echo Lang_Collection(5) %><!--最大采集总数--></td><td class="infotd">
            <input type="text" class="TxtClass" id="AllNum" name="AllNum" value="<%=AllNum%>" /><label class="Normal" id="d_AllNum"></label>
            <span><a href="javascript:ShowHelpMsg(400,150,'<% Echo Lang_Collection(5) %>',Js_HelpMsg[4]);"><b class="icon icon_help"></b></a></span>
            <label class="Normal" id="d_AllNum"></label>
        </td></tr>
        <tr><td class="titletd"><% Echo Lang_Collection(6) %><!--默认导入到--></td><td class="infotd">
            <select id="ContentSubSystem" name="ContentSubSystem" onchange="Admin.Content.GetSubSystemClassList('DefaultClassID')" style="font-size:13px;">
            <% Admin.GetContentSubSystemOption 0,DefaultSystem %>
            </select>
            <span id="DefaultClassID">
			<%
			
			If Action = "edit" then
				Dim Fn_Check : Fn_Check = True
				Set Rs = KnifeCMS.DB.GetRecord(TablePre&"ContentSubSystem",Array("System:"&DefaultSystem),"")
				If Rs.Eof Then Fn_Check = false
				KnifeCMS.DB.CloseRs Rs
				If Fn_Check Then Admin.Ajax.GetSubSystemClassList DefaultClassID,DefaultSystem
			End If
			%>
            </span>
            <label class="Normal" id="d_DefaultClassID"><% Echo Lang_Collection_Cue(13) %></label>
        </td></tr>
        <tr class="Separated"><td class="titletd"><div></div></td><td class="infotd"><div></div></td></tr>
        <tr><td class="titletd"><% Echo Lang_Collection(7) %><!--预定义发布时间--></td><td class="infotd">
            <input type="text" class="TxtSer dateTime" id="DefaultDatetime" name="DefaultDatetime" style="width:200px;" value="<%=DefaultDatetime%>" />
            <label class="Normal" id="d_DefaultDatetime"></label>
            <%=Admin.Calendar(Array("DefaultDatetime","DefaultDatetime","%Y-%m-%d %H:%M:%S"))%>
        </td></tr>
      </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable" id="Tabs_2" style="display:none;">
      <tbody>
        <tr><td class="titletd"><% Echo Lang_Collection(8) %><!--手工列表页URL--></td>
          <td class="infotd">
          <table border="0" cellpadding="0" cellspacing="0">
            <tbody id="HandUrls">
            <%
			If Trim(HandUrls)<>"" Then
			ArrHandUrls=Split(HandUrls,",")
			For Arri=0 to Ubound(ArrHandUrls)
			%>
            <tr><td><input type="text" class="TxtClass long" name="HandUrl" id="HandUrl" value="<%=KnifeCMS.Data.URLDecode(Trim(ArrHandUrls(Arri)))%>" /><a href="javascript:;" onclick='DeleteCurrentRow(this)' class="reduce"><img src="image/reduce.gif" /></a></td></tr>
            <%
			Next
			Else
			%>
            <tr><td><input type="text" class="TxtClass long" name="HandUrl" id="HandUrl" value="" /><a href="javascript:;" onclick='DeleteCurrentRow(this)' class="reduce"><img src="image/reduce.gif" /></a></td></tr>
            <%
			End if
			%>
            </tbody>
            <tbody><tr><td><a href="javascript:CollectionRowAdd('HandUrls','AddHandUrl')" class="add_01"><% Echo Lang_DoAddOne %><!--增加一项--></a></td></tr>
            </tbody>
           </table>
          </td>
        </tr>
      </tbody>
      <tbody>
        <tr id="HandUrl_1"><td class="titletd"><% Echo Lang_Collection(9) %><!--自动列表页URL--></td><td class="infotd">
          <input type="text" class="TxtClass long" id="AutoUrls" name="AutoUrls" value="<%=AutoUrls%>" /><br />
          <label class="Normal" id="d_AutoUrls"><% Echo Lang_ForExample %>: http://www.cwssoft.com.cn/Article_[page].htm</label><br />
          [page]从:<input type="text" class="TxtClass Short" id="AutoUrls_Start" name="AutoUrls_Start" value="<%=AutoUrls_Start%>" /><% Echo Lang_FromTo %><!--到--><input type="text" class="TxtClass Short" id="AutoUrls_End" name="AutoUrls_End" value="<%=AutoUrls_End%>" />
          <input type="button" class="sbutton" onclick="DebugShowUrls('showurls_area')" value="<% Echo Lang_Collection_Cue(14) %>"/><!--显示连接-->
          <div class="showurls_area" id="showurls_area"></div>
          </td></tr>
        <tr><td colspan="2"><label id="d_Urls"></label></td></tr>
      </tbody>
      <tbody>
        <tr class="Separated"><td class="titletd"><div></div></td><td class="infotd"><div></div></td></tr>
        <tr><td class="titletd"><% Echo Lang_Collection(10) %><!--网页编码--></td><td class="infotd">
            <input type="text" class="TxtClass slong" id="HtmlCoding" name="HtmlCoding" value="<%=HtmlCoding%>" style="width:50px;" />
            <label class="Normal" id="d_HtmlCoding"><% Echo Lang_Collection_Cue(16) %><!--网页编码通常有 gb2312 , utf-8 , GBK , big5 四种格式--></label>
        </td></tr>
        <tr class="Separated"><td class="titletd"><div></div></td><td class="infotd"><div></div></td></tr>
        <tr><td class="titletd"><% Echo Lang_Collection(11) %><!--列表区域识别规则--></td><td class="infotd">
            <textarea class="cction" id="SbjUrlRule"  name="SbjUrlRule"><%=SbjUrlRule%></textarea><br />
            <label class="Normal" id="d_SbjUrlRule"><% Echo Lang_Collection_Cue(17) %><!--文章列表区域用[list]代替，如&lt;div id=Article&gt;[list]&lt;/div&gt;--></label>
            <span><a href="javascript:ShowHelpMsg(600,320,'<% Echo Lang_Collection(11) %>',Js_HelpMsg[5]);"><b class="icon icon_help"></b></a></span>
            </td></tr>
        <tr><td class="titletd"><% Echo Lang_Collection(12) %><!--文章链接URL识别规则--></td><td class="infotd">
            <textarea class="cction" id="SbjUrlLinkRule"  name="SbjUrlLinkRule"><%=SbjUrlLinkRule%></textarea><br />
            <label class="Normal" id="d_SbjUrlLinkRule"><% Echo Lang_Collection_Cue(18) %><!--文章链接URL用[url]代替，如&lt;a href="[url]" target="_blank"&gt;--></label>
            <span><a href="javascript:ShowHelpMsg(600,300,'<% Echo Lang_Collection(12) %>',Js_HelpMsg[6]);"><b class="icon icon_help"></b></a></span>
            </td></tr>
        <tr><td class="titletd"><% Echo Lang_Collection(13) %><!--文章链接URL字符替换--></td><td class="infotd">
            <table border="0" cellpadding="0" cellspacing="0">
            <tbody id="RepLinks">
            <%
			If Trim(RepLinks)<>"" Then
			RepLinks=Split(RepLinks,"}{")
			For Arri=0 to Ubound(RepLinks)
				ArrRepLinks=Split(RepLinks(Arri),"][")
				RepLink=ArrRepLinks(0)
				RepLinkTo=ArrRepLinks(1)
			%>
            <tr><td><input type="text" class="TxtClass TwoColumn" name="RepLink" value="<%=RepLink%>" style="margin-right:9px;"/><% Echo Lang_ReplaceFor %><!--替换为--><input type="text" class="TxtClass TwoColumn" name="RepLinkTo" value="<%=RepLinkTo%>" style="margin-left:9px;"/><a href="#" onclick='DeleteCurrentRow(this)' class="reduce"><img src="image/reduce.gif" /></a></td></tr>
            <%
			Next
			Else
			%>
            <tr><td><input type="text" class="TxtClass TwoColumn" name="RepLink" style="margin-right:9px;"/><% Echo Lang_ReplaceFor %><!--替换为--><input type="text" class="TxtClass TwoColumn" name="RepLinkTo" style="margin-left:9px;"/><a href="#" onclick='DeleteCurrentRow(this)' class="reduce"><img src="image/reduce.gif" /></a></td></tr>
            <%End If%>
            </tbody>
            <tbody><tr><td><a href="javascript:CollectionRowAdd('RepLinks','AddRepLink')" class="add_01"><% Echo Lang_DoAddOne %><!--增加一项--></a></td></tr></tbody>
            </table>
            </td></tr>
        <tr class="Separated"><td class="titletd"><div></div></td><td class="infotd"><div></div></td></tr>
        <tr><td class="titletd"><% Echo Lang_Collection(14) %><!--文章缩略图识别规则--></td><td class="infotd">
            <textarea class="cction" id="ThumbnailRule"  name="ThumbnailRule"><%=ThumbnailRule%></textarea><br />
            <input type="checkbox" id="SaveThumbnail" name="SaveThumbnail" value="1" <%=Admin.InputChecked(SaveThumbnail)%>/><% Echo Lang_Collection_Cue(19) %><!--保存到本地-->
            <label class="Normal" id="d_ThumbnailRule"><% Echo Lang_Collection_Cue(20) %><!--文章缩略图用[img]代替，如&lt;img src="[img]" &gt;--></label>
            <span><a href="javascript:ShowHelpMsg(400,100,'<% Echo Lang_Collection(14) %>',Js_HelpMsg[7]);"><b class="icon icon_help"></b></a></span>
            </td></tr>
        <tr><td class="titletd"><% Echo Lang_Collection(15) %><!--缩略图字符替换--></td><td class="infotd">
            <table border="0" cellpadding="0" cellspacing="0">
            <tbody id="RepThumbs">
            <%
			If Trim(RepThumbs)<>"" Then
			RepThumbs=Split(RepThumbs,"}{")
			For Arri=0 to Ubound(RepThumbs)
				ArrRepThumbs=Split(RepThumbs(Arri),"][")
				RepThumb=ArrRepThumbs(0)
				RepThumbTo=ArrRepThumbs(1)
			%>
            <tr><td><input type="text" class="TxtClass TwoColumn" name="RepThumb" value="<%=RepThumb%>" style="margin-right:9px;"/><% Echo Lang_ReplaceFor %><!--替换为--><input type="text" class="TxtClass TwoColumn" name="RepThumbTo" value="<%=RepThumbTo%>" style="margin-left:9px;"/><a href="#" onclick='DeleteCurrentRow(this)' class="reduce"><img src="image/reduce.gif" /></a></td></tr>
            <%
			Next
			Else
			%>
            <tr><td><input type="text" class="TxtClass TwoColumn" name="RepThumb" style="margin-right:9px;"/><% Echo Lang_ReplaceFor %><!--替换为--><input type="text" class="TxtClass TwoColumn" name="RepThumbTo" style="margin-left:9px;"/><a href="#" onclick='DeleteCurrentRow(this)' class="reduce"><img src="image/reduce.gif" /></a></td></tr>
            <%End If%>
            </tbody>
            <tbody><tr><td><a href="javascript:CollectionRowAdd('RepThumbs','AddRepThumb')" class="add_01"><% Echo Lang_DoAddOne %><!--增加一项--></a></td></tr></tbody>
            </table>
            </td></tr>
      </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable" id="Tabs_3" style="display:none;">
      <tbody>
      <tr><td class="titletd"><% Echo Lang_Collection(16) %><!--文章标题识别规则--></td><td class="infotd">
          <textarea class="cction" id="TitleRule" name="TitleRule"><%=TitleRule%></textarea><label id="d_TitleRule"></label><br />
          <label class="Normal" id="h_TitleRule"><% Echo Lang_Collection_Cue(21) %><!--文章标题用[title]代替，如&lt;div id=ArtTitle&gt;[title]&lt;/div&gt;--></label>
          </td></tr>
       <tr><td class="titletd"><% Echo Lang_Collection(17) %><!--文章标题字符替换--></td><td class="infotd">
            <table border="0" cellpadding="0" cellspacing="0">
            <tbody id="RepTitles">
            <%
			If Trim(RepTitles)<>"" Then
			RepTitles=Split(RepTitles,"}{")
			For Arri=0 to Ubound(RepTitles)
				ArrRepTitles=Split(RepTitles(Arri),"][")
				RepTitle=ArrRepTitles(0)
				RepTitleTo=ArrRepTitles(1)
			%>
            <tr><td><input type="text" class="TxtClass TwoColumn" name="RepTitle" value="<%=RepTitle%>" style="margin-right:9px;"/><% Echo Lang_ReplaceFor %><!--替换为--><input type="text" class="TxtClass TwoColumn" name="RepTitleTo" value="<%=RepTitleTo%>" style="margin-left:9px;"/><a href="#" onclick='DeleteCurrentRow(this)' class="reduce"><img src="image/reduce.gif" /></a></td></tr>
            <%
			Next
			Else
			%>
            <tr><td><input type="text" class="TxtClass TwoColumn" name="RepTitle" style="margin-right:9px;"/><% Echo Lang_ReplaceFor %><!--替换为--><input type="text" class="TxtClass TwoColumn" name="RepTitleTo" style="margin-left:9px;"/><a href="#" onclick='DeleteCurrentRow(this)' class="reduce"><img src="image/reduce.gif" /></a></td></tr>
            <%End If%>
            </tbody>
            <tbody><tr><td><a  href="javascript:CollectionRowAdd('RepTitles','AddRepTitle')" class="add_01"><% Echo Lang_DoAddOne %><!--增加一项--></a></td></tr></tbody>
            </table>
            </td></tr>
      <tr><td class="titletd"><% Echo Lang_Collection(18) %><!--文章内容识别规则--></td><td class="infotd">
          <textarea class="cction" id="ContentRule"  name="ContentRule"><%=ContentRule%></textarea><label id="d_ContentRule"></label><br />
          <label class="Normal" id="h_ContentRule"><% Echo Lang_Collection_Cue(22) %><!--文章内容用[content]代替，如&lt;div id=ArtContent&gt;[content]&lt;/div&gt;--></label>
          </td></tr>
      <tr><td class="titletd"><% Echo Lang_Collection(19) %><!--文章内容替换--></td><td class="infotd">
          <table border="0" cellpadding="0" cellspacing="0">
          <tbody id="RepContents">
          <%
			If Trim(RepContents)<>"" Then
			RepContents=Split(RepContents,"}{")
			For Arri=0 to Ubound(RepContents)
				ArrRepContents=Split(RepContents(Arri),"][")
				RepContent=ArrRepContents(0)
				'RepContent=Replace(RepContent,"","&#34")
				RepContentTo=ArrRepContents(1)
		  %>
          <tr><td><input type="text" class="TxtClass TwoColumn" name="RepContent" style="margin-right:9px;" value="<%=RepContent%>"/><% Echo Lang_ReplaceFor %><!--替换为--><input type="text" class="TxtClass TwoColumn" name="RepContentTo" value="<%=RepContentTo%>" style="margin-left:9px;"/><a href="#" onclick='DeleteCurrentRow(this)' class="reduce"><img src="image/reduce.gif" /></a></td></tr>
          <%
			Next
			Else
		  %>
          <tr><td><input type="text" class="TxtClass TwoColumn" name="RepContent" style="margin-right:9px;"/><% Echo Lang_ReplaceFor %><!--替换为--><input type="text" class="TxtClass TwoColumn" name="RepContentTo" style="margin-left:9px;"/><a href="#" onclick='DeleteCurrentRow(this)' class="reduce"><img src="image/reduce.gif" /></a></td></tr>
          <%End If%>
          </tbody>
          <tbody><tr><td><a  href="javascript:CollectionRowAdd('RepContents','AddRepContent')" class="add_01"><% Echo Lang_DoAddOne %><!--增加一项--></a></td></tr></tbody>
          </table>
          </td></tr>
      <tr><td class="titletd"><% Echo Lang_Collection(20) %><!--文章内容格式化--></td><td class="infotd">
          <input type="radio" id="ContentFormat" name="ContentFormat" value="0" <%=Admin.Input_Checked(ContentFormat,0)%>/><% Echo Lang_Collection(31) %><!--不格式化-->
          <input type="radio" id="ContentFormat" name="ContentFormat" value="3" <%=Admin.Input_Checked(ContentFormat,3)%>/><% Echo Lang_Collection(32) %><!--格式化(纯文本)-->
          <input type="radio" id="ContentFormat" name="ContentFormat" value="2" <%=Admin.Input_Checked(ContentFormat,2)%>/><% Echo Lang_Collection(33) %><!--格式化(保留段落)-->
          <input type="radio" id="ContentFormat" name="ContentFormat" value="1" <%=Admin.Input_Checked(ContentFormat,1)%>/><% Echo Lang_Collection(34) %><!--格式化(保留段落和图片)-->
          <span><a href="javascript:ShowHelpMsg(400,150,'<% Echo Lang_Collection(20) %>',Js_HelpMsg[8]);"><b class="icon icon_help"></b></a></span>
          </td></tr>
      <tr class="Separated"><td class="titletd"><div></div></td><td class="infotd"><div></div></td></tr>
      <tr><td class="titletd"><% Echo Lang_Collection(21) %><!--是否有内容分页--></td><td class="infotd">
          <input type="radio" id="ContentPageType0" name="ContentPageType" onClick="ShowOrHidden('PageRuleArea',0)" value="0" <%=Admin.Input_Checked(ContentPageType,0)%>/><% Echo Lang_Collection(35) %><!--无分页-->
          <input type="radio" id="ContentPageType1" name="ContentPageType" onClick="ShowOrHidden('PageRuleArea',1)" value="1" <%=Admin.Input_Checked(ContentPageType,1)%>/><% Echo Lang_Collection(36) %><!--有分页-->
          </td></tr>
      <tbody id="PageRuleArea" style="display:none;">
      <tr><td class="titletd"><% Echo Lang_Collection(22) %><!--分页区域识别规则--></td><td class="infotd">
          <textarea class="cction" id="ContentPageRule"  name="ContentPageRule"><%=ContentPageRule%></textarea><br />
          <label class="Normal" id="d_ContentPageRule"><% Echo Lang_Collection_Cue(23) %><!--分页区域用[pagearea]代替，如&lt;div id=ArtPage&gt;[pagearea]&lt;/div&gt;--></label>
          </td></tr>
      <tr><td class="titletd"><% Echo Lang_Collection(23) %><!--分页链接识别规则--></td><td class="infotd">
          <textarea class="cction" id="ContentPageUrlRule"  name="ContentPageUrlRule"><%=ContentPageUrlRule%></textarea><br />
          <label class="Normal" id="d_ContentPageUrlRule"><% Echo Lang_Collection_Cue(24) %><!--分页链接用[page]代替，如&lt;a href="[page]"&gt;--></label>
          </td></tr>
      <tr><td class="titletd"><% Echo Lang_Collection(24) %><!--分页链接字符替换--></td><td class="infotd">
          <table border="0" cellpadding="0" cellspacing="0">
          <tbody id="RepPageUrls">
          <%
			If Trim(RepPageUrls)<>"" Then
			RepPageUrls=Split(RepPageUrls,"}{")
			For Arri=0 to Ubound(RepPageUrls)
				ArrRepPageUrls=Split(RepPageUrls(Arri),"][")
				RepPageUrl=ArrRepPageUrls(0)
				RepPageUrlTo=ArrRepPageUrls(1)
		  %>
          <tr><td><input type="text" class="TxtClass TwoColumn" name="RepPageUrl" value="<%=RepPageUrl%>" style="margin-right:9px;"/><% Echo Lang_ReplaceFor %><!--替换为--><input type="text" class="TxtClass TwoColumn" name="RepPageUrlTo" value="<%=RepPageUrlTo%>" style="margin-left:9px;"/><a href="#" onclick='DeleteCurrentRow(this)' class="reduce"><img src="image/reduce.gif" /></a></td></tr>
          <%
			Next
			Else
		  %>
          <tr><td><input type="text" class="TxtClass TwoColumn" name="RepPageUrl" style="margin-right:9px;"/><% Echo Lang_ReplaceFor %><!--替换为--><input type="text" class="TxtClass TwoColumn" name="RepPageUrlTo" style="margin-left:9px;"/><a href="#" onclick='DeleteCurrentRow(this)' class="reduce"><img src="image/reduce.gif" /></a></td></tr>
          <%End If%>
          </tbody>
          <tbody><tr><td><a  href="javascript:CollectionRowAdd('RepPageUrls','AddRepPageUrl')" class="add_01"><% Echo Lang_DoAddOne %><!--增加一项--></a></td></tr></tbody>
          </table>
          </td></tr>
       </tbody>
       <tr class="Separated"><td class="titletd"><div></div></td><td class="infotd"><div></div></td></tr>
       <tr><td class="titletd"><% Echo Lang_Collection(25) %><!--信息来源识别规则--></td><td class="infotd">
          <textarea class="cction" id="FromRule"  name="FromRule"><%=FromRule%></textarea><br />
          <label class="Normal" id="d_FromRule"><% Echo Lang_Collection_Cue(25) %><!--信息来源用[from]代替，如&lt;div id=Artfrom&gt;[from]&lt;/div&gt;--></label>
          </td></tr>
       <tr><td class="titletd"><% Echo Lang_Collection(26) %><!--作者识别规则--></td><td class="infotd">
          <textarea class="cction" id="AuthorRule"  name="AuthorRule"><%=AuthorRule%></textarea><br />
          <label class="Normal" id="d_AuthorRule"><% Echo Lang_Collection_Cue(26) %><!--信息来源用[author]代替，如&lt;div id=ArtAuthor&gt;[author]&lt;/div&gt;--></label>
          </td></tr>
       <tr><td class="titletd"><% Echo Lang_Collection(27) %><!--发布时间识别规则--></td><td class="infotd">
          <textarea class="cction" id="AddTimeRule"  name="AddTimeRule"><%=AddTimeRule%></textarea><br />
          <label class="Normal" id="d_AddTimeRule"><% Echo Lang_Collection_Cue(27) %><!--发布时间用[time]代替，如&lt;div id=ArtTime&gt;[time]&lt;/div&gt;--></label>
          </td></tr>
       <tr><td class="titletd"><% Echo Lang_Collection(28) %><!--保存文件到本地--></td><td class="infotd">
          <input type="checkbox" id="SaveRemotePic" name="SaveRemotePic" value="1" <%=Admin.InputChecked(SaveRemotePic)%>/>图片<label class="Normal" id="d_SaveRemotePic"></label>
          <input type="checkbox" id="SaveRemoteFlash" name="SaveRemoteFlash" value="1" <%=Admin.InputChecked(SaveRemoteFlash)%>/>Flash<label class="Normal" id="d_SaveRemoteFlash"></label>
          <input type="checkbox" id="SaveRemoteFile" name="SaveRemoteFile" value="1" <%=Admin.InputChecked(SaveRemoteFile)%>/>(zip/rar)文件<label class="Normal" id="d_SaveRemoteFile"></label><br />
          <label class="Normal" id="d_SaveRemoteFile"><% Echo Lang_Collection_Cue(28) %><!--保存文件到本地会占用大量系统资源和降低采集速率,请慎重使用.--></label>
          </td></tr>
      </tbody>
    </table>
    <div class="Area-bottom"></div>
    <div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
        <input type="hidden" name="id" value="<% Echo ID %>" />
        <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
        <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Collection.SaveCollectionCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
        <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
        <button type="button" class="sysSaveBtn" id="ReSetBtn" onClick="if( confirm('<% Echo Lang_DoConfirmLeave %>') ){CloseWithOpenNewWin('?ctl=<% Echo Ctl %>&act=view');}"><b class="icon icon_close"></b><% Echo Lang_Btn_Close %></button>
    </div>
    </form>
    </div>
  </DIV>
</DIV>
<script type="text/javascript">
//显示分页规则区
var divPage=document.getElementById("PageRuleArea");
var inputPage=document.getElementById("ContentPageType1");
if(divPage&&inputPage){if(inputPage.checked==true){divPage.style.display="";}}
//End
function DebugShowUrls(BV_Area){
	var area = KnifeCMS.$id(BV_Area);
	var fn_Tempurl; AutoUrls = KnifeCMS.$id("AutoUrls").value;
	var AutoUrls_Start = parseInt(KnifeCMS.$id("AutoUrls_Start").value);
	var AutoUrls_End   = parseInt(KnifeCMS.$id("AutoUrls_End").value);
	var ul = document.createElement("ul");
	if(AutoUrls_End>=AutoUrls_Start){
		for(var ii=AutoUrls_Start;ii<=AutoUrls_End;ii++){
			fn_Tempurl = AutoUrls;
			var li = document.createElement("li");
			var a  = document.createElement("a")
			a.target = "_blank";
			a.href   = fn_Tempurl.replace("[page]",ii);
			a.innerHTML = a.href;
			li.appendChild(a);
			ul.appendChild(li);
		}
	}
	area.innerHTML = "";
	area.appendChild(ul);
}
</script>
<%
End Function
Function CollectionResultView()
%>
<%=Admin.Calendar(Array("","",""))%>
<DIV id="MainFrame">
  <div class="tagLine"><% Echo NavigationLine %></div>
  <DIV class="cbody">
    <div class="toparea">
      <table border="0" cellpadding="0" cellspacing="0">
      <tr>
      <td class="tdcell">
          <table border="0" cellpadding="2" cellspacing="0">
          <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
          <tr><td class="downcell">
              <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDel%></button>
              </td></tr>
          </table>
      </td>
      <td class="tdcell">
        <table border="0" cellpadding="2" cellspacing="0">
        <tr><td class="upcell">
            <div class="align-center grey">
				<% Echo Lang_DistrictDoSearch %><!--搜索操作区-->
                <span class="AdvancedSearch"><a href="javascript:Admin.AdvancedSearch.Open(600,320,'AdvancedSearch')" ><% Echo Lang_AdvancedSearch %></a></span>
            </div>
            <DIV id="AdvancedSearch" style="display:none;">
                <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                <div class="diainbox">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                  <tr><td class="titletd"><% Echo Lang_Keywords %></td>
                      <td class="infotd"><input type="text" class="TxtClass" id="keywords" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:280px;" /></td>
                  </tr>
                  <tr><td class="titletd"><% Echo Lang_Collection(0) %><!--采集器--></td>
                            <td class="infotd"><select name="collectionid"><% Echo Admin.GetCollectionAsOption(0,Admin.Search.CollectionID)%></select></td>
                  </tr>    
                  <tr><td class="titletd"><% Echo Lang_StartDate %>:</td>
                      <td class="infotd"><input type="text" class="TxtSer dateStart" id="startdate" name="startdate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:200px;" value="<%=Admin.Search.StartDate%>" /></td>
                  <tr><td class="titletd"><% Echo Lang_EndDate %>:</td>
                      <td class="infotd"><input type="text" class="TxtSer dateEnd" id="enddate" name="enddate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:200px;" value="<%=Admin.Search.EndDate%>" /></td>
                  </tr>
                  </table>
                  </div>
                  <% Echo AdvancedSearchBtnline %>
                </form>
            </DIV>
            </td></tr>
        <tr><td class="downcell">
            <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
            <table border="0" cellpadding="0" cellspacing="0"><tr>
            <td class="pl3"><select name="collectionid"><% Echo Admin.GetCollectionAsOption(0,Admin.Search.CollectionID)%></select></td>
            <td class="pl3"><input type="text" class="TxtClass" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:120px;" /></td>
            <td class="pl3"><button type="submit" class="sysBtn" ><b class="icon icon_search"></b><% Echo Lang_Btn_Search %></button></td>
            </tr></table>
            </form>
        </td></tr>
        </table>
      </td>
      <td class="tdcell">
          <table border="0" cellpadding="2" cellspacing="0">
          <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoExport %><!--导出数据操作区--></div></td></tr>
          <tr><td class="downcell">
              <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','export',Lang_Js_Cue[6])"><b class="icon icon_import"></b><% Echo Lang_DoExportToDefault %><!--导出到默认系统--></button>
              <input type="checkbox" id="InsertOther" name="InsertOther" onclick='SetInputDisabled(0,"InsertOther","ContentSubSystem,ClassID,ConfirmInsert")' value="1"/><label><% Echo Lang_DoExportToOther %><!--导出到其他系统--></label>
              <select id="ContentSubSystem" name="ContentSubSystem" onchange="Admin.Content.GetSubSystemClassList('DefaultClassID')" disabled="disabled" style="font-size:13px;">
              <% Admin.GetContentSubSystemOption 0,DefaultSystem %>
              </select>
              <span id="DefaultClassID"></span>
              <button type="button" class="sysBtn" id="ConfirmInsert" name="ConfirmInsert" onClick="InsertSelectData()" disabled="disabled"><b class="icon icon_confirm"></b><% Echo Lang_Btn_Confirm %></button>
              </td></tr>
          </table>
      </td>
      </tr>
      </table>
    </div>
    <div class="tabledata">
      <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
      <input type="hidden" id="SelectSubSystem" name="SelectSubSystem" value="" />
      <input type="hidden" id="SelectClassID" name="SelectClassID" value="" />
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
       <tbody>
        <tr class="top">
        <th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
        <th width="1%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %></div></th><!--系统ID-->
        <th width="30%"><div style="width:200px;"><% Echo Lang_Collection_ListTableColLine(5) %></div></th><!--文章标题-->
        <th width="15%"><div style="width:100px;"><% Echo Lang_Collection_ListTableColLine(0) %></div></th><!--采集器名称-->
        <th width="10%"><div style="width:100px;"><% Echo Lang_Collection_ListTableColLine(1) %></div></th><!--默认导入到-->
        <th width="10%"><div style="width:60px;"><% Echo Lang_Collection_ListTableColLine(3) %></div></th><!--缩略图-->
        <th width="10%"><div style="width:135px;"><% Echo Lang_Collection_ListTableColLine(4) %></div></th><!--采集时间-->
        <th><div style="width:120px;"><% Echo Lang_ListTableColLine_Do %></div></th><!--操作-->
        </tr>
        <% CollectionResultList() %>
       </tbody>
      </table>
      </form>
    </div>
  </DIV>
</DIV>
<script language="javascript">
TrBgChange("listForm","InputName","oliver","data","select");
if(objExist(document.getElementById("keywords"))){ReplaceText(document.getElementById("keywords").value,1)}
function InsertSelectData()
{
	if(Chk_idBatch('listForm',"InputName",Lang_Js_Cue[4])==true)
	{
		var SelectSubSystem=document.getElementById("ContentSubSystem").value;
		var SelectClassID;
		if(objExist(document.getElementById("ClassID"))){SelectClassID=document.getElementById("ClassID").value}
		if(SelectSubSystem.length > 1 && SelectClassID > 0){
			document.forms['listForm'].SelectSubSystem.value=SelectSubSystem;
			document.forms['listForm'].SelectClassID.value=SelectClassID;
			document.forms['listForm'].act.value="export";
			document.getElementById("ConfirmInsert").disabled=true;
			document.forms['listForm'].submit();
		}
		else
		{
			alert(Lang_Js_Cue[5]);
		}
	}
}
function ChangeTdText(){
	var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
	var content,TempHTML;
	if(tds!=null){
		for(var i=1;i<tds.length;i++){
			if(tds[i].id=="td4" && tds[i].innerHTML!=""){
				TempHTML = "<span class=\"sImg\"><a href=javascript:;><img src='"+ unescape(tds[i].innerHTML) +"' /></a></span>";
				tds[i].innerHTML=TempHTML;
			}
		}
	}
}
ChangeTdText();
</script>
</script>
<%
End Function
Function CollectionResultDetailView()
%>
<DIV id="MainFrame">
  <div class="tagLine"><% Echo NavigationLine %></div>
  <DIV class="cbody">
    <div class="wbox">
    <div class="Table">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Tabs_1">
      <tbody>
        <tr class="info"><td class="first"><label class="ifn"><% Echo KnifeCMS.Data.HTMLDecode(Lang_Collection(40)) %><!--文章标题-->:</label></td><td class="sec"><% Echo CollectionResult_Title%></td></tr>
        <tr class="info"><td class="first"><label class="ifn"><% Echo Lang_Collection(41) %><!--来源-->:</label></td><td class="sec"><% Echo CollectionResult_From%></td></tr>
        <tr class="info"><td class="first"><label class="ifn"><% Echo Lang_Collection(42) %><!--作者-->:</label></td><td class="sec"><% Echo CollectionResult_Author%></td></tr>
        <tr class="info"><td class="first"><label class="ifn"><% Echo Lang_Collection(43) %><!--发布时间-->:</label></td><td class="sec"><% Echo CollectionResult_AddTime%></td></tr>
        <tr class="info"><td class="first"><label class="ifn"><% Echo Lang_Collection(44) %><!--文章缩略图-->:</label></td><td class="sec">
		<%
		If CollectionResult_Thumbnail <> "" Then Echo "<img src="&KnifeCMS.Data.URLDecode(CollectionResult_Thumbnail)&"><br>"
		Echo KnifeCMS.Data.URLDecode(CollectionResult_Thumbnail)
		%>
        </td></tr>
        <tr><td class="first" colspan="2"><div class="Separated"></div></td></tr>
        <tr class="info"><td class="first"><label class="ifn"><% Echo Lang_Collection(45) %><!--详细内容-->:</label></td><td class="sec"><% Echo KnifeCMS.Data.HTMLDecode(CollectionResult_Content) %>
        </td></tr>
      </tbody>
    </table>
    </div>
    </div>
  </DIV>
</DIV>
<%
End Function
Function CollectionResultDetailDo()
%>
<DIV id="MainFrame">
  <div class="tagLine"><% Echo NavigationLine %></div>
  <DIV class="cbody">
    <div class="wbox">
    <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
      <tbody>
        <tr><td class="titletd"><label class="ifn"><% Echo Lang_Collection(40) %><!--文章标题-->:</label></td>
            <td class="infotd"><input type="text" class="TxtClass long" id="CollectionResult_Title" name="CollectionResult_Title" value="<%=CollectionResult_Title%>" /><label class="Normal" id="d_CollectionResult_Title"></label>
        </td></tr>
        <tr><td class="titletd"><label class="ifn"><% Echo Lang_Collection(41) %><!--来源-->:</label></td>
            <td class="infotd"><input type="text" class="TxtClass slong" id="CollectionResult_From" name="CollectionResult_From" value="<%=CollectionResult_From%>" /><label class="Normal" id="d_CollectionResult_From"></label>
        </td></tr>
        <tr><td class="titletd"><label class="ifn"><% Echo Lang_Collection(42) %><!--作者-->:</label></td>
            <td class="infotd"><input type="text" class="TxtClass slong" id="CollectionResult_Author" name="CollectionResult_Author" value="<%=CollectionResult_Author%>" /><label class="Normal" id="d_CollectionResult_Author"></label>
        </td></tr>
        <tr><td class="titletd"><label class="ifn"><% Echo Lang_Collection(43) %><!--发布时间-->:</label></td>
            <td class="infotd"><input type="text" class="TxtClass slong" id="CollectionResult_AddTime" name="CollectionResult_AddTime" value="<%=CollectionResult_AddTime%>" /><label class="Normal" id="d_CollectionResult_AddTime"></label>
        </td></tr>
        <tr><td class="titletd"><label class="ifn"><% Echo Lang_Collection(44) %><!--文章缩略图-->:</label></td>
            <td class="infotd"><input type="text" class="TxtClass slong" id="CollectionResult_Thumbnail" name="CollectionResult_Thumbnail" value="<% Echo KnifeCMS.Data.URLDecode(CollectionResult_Thumbnail) %>"/>
               <input type="button" class="sbutton" name="UploadThumbnail" id="UploadThumbnail" value="<% Echo Lang_UploadImg %>" onclick="UploadImg('1','CollectionResult_Thumbnail')" />
               <label class="Normal" id="d_CollectionResult_Thumbnail"></label>
        </td></tr>
        <tr><td class="titletd"><label class="ifn"><% Echo Lang_Collection(45) %><!--文章内容-->:</label></td>
            <td class="infotd">
               <label class="Normal" id="d_TextContent"></label><br />
               <textarea class="TextContent" id="TextContent" name="TextContent"><%=KnifeCMS.Data.HTMLDecode(CollectionResult_Content)%></textarea>
        </td></tr>
      </tbody>
    </table>
    <div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
        <input type="hidden" name="id" value="<% Echo ID %>" />
        <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
        <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Collection.SaveCollectionResultCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
        <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
        <button type="button" class="sysSaveBtn" id="ReSetBtn" onClick="if( confirm('<% Echo Lang_DoConfirmLeave %>') ){history.go(-1)}"><b class="icon icon_close"></b><% Echo Lang_Btn_Close %></button>
    </div>
    </form>
    </div>
  </DIV>
</DIV>
<script type="text/javascript" charset="utf-8">
window.UEDITOR_HOME_URL = "<%=SystemPath%>plugins/ueditor/";
</script>
<script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_config.asp"></script>
<script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_api.js"></script>
<script type="text/javascript">
var OSWebEditor=UE.getEditor('TextContent');
</script>
<!--<script type="text/javascript">
cwsEditor(690,webScreenHeight()-100);
</script>-->
<%
End Function
%>
<%
Dim CollectionName,AllNum,DefaultSystem,DefaultClassID,HandUrls,AutoUrls,AutoUrls_Start,AutoUrls_End,HtmlCoding,SbjUrlRule,SbjUrlLinkRule,RepLink,RepLinkTo,RepLinks,ThumbnailRule,TitleRule,RepTitle,RepTitleTo,RepTitles,Thumbnail,SaveThumbnail,RepThumb,RepThumbTo,RepThumbs,ContentRule,RepContent,RepContentTo,RepContents,ContentFormat,ContentPagetype,ContentPageRule,ContentPageUrlRule,RepPageUrl,RepPageUrlTo,RepPageUrls,FromRule,AuthorRule,AddTimeRule,DefaultDatetime,SaveRemotePic,SaveRemoteFlash,SaveRemoteFile,AddTime
Dim Arri,Arrj,ArrHandUrls,ArrRepLinks,ArrRepTitles,ArrRepThumbs,ArrRepContents,ArrRepPageUrls
Dim strHtml,strTitle,strContent,strContentAll,strContentPage,strFrom,strAuthor,strAddTime
Dim HandUrl
Dim Try_i,Try_Max '采集失败后重新采集(最大次数)
Dim CollectionResult_Title,CollectionResult_Thumbnail,CollectionResult_From,CollectionResult_Author,CollectionResult_AddTime,CollectionResult_Content

Admin.IncludeLanguageFile "collection.lang.asp"
Header()
DBTable = TablePre&"Collection"
Dim DBTableResult : DBTableResult = DBTable&"Result"
If Ctl = "collection" Then
	Select Case Action
		Case "view"    : CollectionView
		Case "add","edit"
			If SubAction="save" Then
				CollectionDoSave
			Else
				If Action = "add" Then
					CollectionDo
				ElseIf Action = "edit" Then
					Set Rs = KnifeCMS.DB.GetRecord(DBTable,Array("ID:"&ID),"")
					If Not(Rs.Eof) Then
						CollectionName  = Rs("CollectionName")
						AllNum          = Rs("AllNum")
						DefaultSystem   = Rs("DefaultSystem")
						DefaultClassID  = Rs("DefaultClassID")
						HandUrls        = Rs("HandUrls")
						AutoUrls        = Rs("AutoUrls")
						AutoUrls_Start  = Rs("AutoUrls_Start")
						AutoUrls_End    = Rs("AutoUrls_End")
						HtmlCoding      = Rs("HtmlCoding")
						SbjUrlRule      = Rs("SbjUrlRule")
						SbjUrlLinkRule  = Rs("SbjUrlLinkRule")
						RepLinks        = Rs("RepLinks")
						ThumbnailRule   = Rs("ThumbnailRule")
						SaveThumbnail   = Rs("SaveThumbnail")
						RepThumbs       = Rs("RepThumbs")
						TitleRule       = Rs("TitleRule")
						RepTitles       = Rs("RepTitles")
						ContentRule     = Rs("ContentRule")
						RepContents     = Rs("RepContents")
						ContentFormat   = KnifeCMS.Data.CLng(Rs("ContentFormat"))
						ContentPagetype = KnifeCMS.Data.CLng(Rs("ContentPagetype"))
						ContentPageRule = Rs("ContentPageRule")
						ContentPageUrlRule = Rs("ContentPageUrlRule")
						RepPageUrls     = Rs("RepPageUrls")
						FromRule        = Rs("FromRule")
						AuthorRule      = Rs("AuthorRule")
						AddTimeRule     = Rs("AddTimeRule")
						DefaultDatetime = Rs("DefaultDatetime")
						SaveRemotePic   = Cbool(KnifeCMS.Data.CLng(Rs("SaveRemotePic")))
						SaveRemoteFlash = Cbool(KnifeCMS.Data.CLng(Rs("SaveRemoteFlash")))
						SaveRemoteFile  = Cbool(KnifeCMS.Data.CLng(Rs("SaveRemoteFile")))
					Else
						Admin.ShowMessage Lang_CollectionCue(7),"goback",0 : Die ""
					End IF
					KnifeCMS.DB.CloseRs Rs
					CollectionDo
				End IF
			 End IF
		Case "del"     : CollectionDel
		Case "copy"    : CollectionCopy
		Case "cmd"
			If SubAction = "debug" Then
				CollectData(1)
			ElseIf SubAction = "collect" Then
				CollectData(0)
			End If
	End Select
ElseIf Ctl = "collection_result" Then
	Select Case Action
		Case "view"
			If SubAction="detail" Then
				Set Rs = KnifeCMS.DB.GetRecord(DBTableResult,Array("ID:"&ID),"")
				If Not(Rs.Eof) Then
					CollectionResult_Title     = Rs("Title")
					CollectionResult_Thumbnail = Rs("Thumbnail")
					CollectionResult_From      = Rs("From")
					CollectionResult_Author    = Rs("Author")
					CollectionResult_AddTime   = Rs("AddTime")
					CollectionResult_Content   = Rs("Content")
				Else
					Admin.ShowMessage Lang_CollectionResult_Cue(7),"goback",0 : Die ""
				End IF
				KnifeCMS.DB.CloseRs Rs
				CollectionResultDetailView
			Else
				CollectionResultView
			End If
		Case "edit"
		    If SubAction="save" Then
				CollectionResultDetailDoSave
			Else
				Set Rs = KnifeCMS.DB.GetRecord(DBTableResult,Array("ID:"&ID),"")
				If Not(Rs.Eof) Then
					CollectionResult_Title     = Rs("Title")
					CollectionResult_Thumbnail = Rs("Thumbnail")
					CollectionResult_From      = Rs("From")
					CollectionResult_Author    = Rs("Author")
					CollectionResult_AddTime   = Rs("AddTime")
					CollectionResult_Content   = Rs("Content")
				Else
					Admin.ShowMessage Lang_CollectionResult_Cue(7),"goback",0 : Die ""
				End IF
				KnifeCMS.DB.CloseRs Rs
				CollectionResultDetailDo
			End If
		Case "export" : CollectionResultExport
		Case "del"    : CollectionResultDel
	End Select
End If
Footer()

'$:采集器(分页)列表
Function CollectionList()
	Dim Operation
	Select Case DB_Type
	Case 0
	Sql="SELECT a.ID,a.CollectionName,b.SystemName,a.AddTime,iif(IsNull(num),0,num) FROM (["& DBTable &"] a LEFT JOIN ["& TablePre &"ContentSubSystem] b ON (a.DefaultSystem=b.System)) LEFT JOIN (SELECT CollectionID,count(CollectionID) AS num from "& DBTableResult &" c Group BY CollectionID) c ON (a.ID=c.CollectionID) ORDER BY a.ID ASC"
	Case 1
	Sql="SELECT a.ID,a.CollectionName,b.SystemName,CONVERT(varchar(100),a.AddTime,20),num FROM ["& DBTable &"] a LEFT JOIN ["& TablePre &"ContentSubSystem] b ON (a.DefaultSystem=b.System) LEFT JOIN (SELECT CollectionID,count(CollectionID) AS num from "& DBTableResult &" c Group BY CollectionID) c ON (a.ID=c.CollectionID) ORDER BY a.ID ASC"
	End Select
	
	Operation = "<span class=""sysBtn""><a href=""?ctl="&Ctl&"&act=edit&id={$ID}"" ><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
	Operation = Operation & "<span class=""sysBtn""><a href=""?ctl="&Ctl&"&act=cmd&subact=debug&id={$ID}"" ><b class=""icon icon_debug""></b>"& Lang_DoDebug &"</a></span>"
	Operation = Operation & "<span class=""sysBtn""><a href=""?ctl="&Ctl&"&act=cmd&subact=collect&id={$ID}"" ><b class=""icon icon_collectdata""></b>"& Lang_DoCollectData &"</a></span>"
	Operation = Operation & "<span class=""sysBtn""><a href=""?ctl=collection_result&act=view&subact=search&collectionid={$ID}"" ><b class=""icon icon_view_list""></b>"& Lang_DoSeeResult &"</a></span>"
	Operation = Operation & "<span class=""sysBtn""><a href=""?ctl=collection_result&act=export&collectionid={$ID}"" ><b class=""icon icon_import""></b>"& Lang_DoExportData &"</a></span>"
	Operation = Operation & "<span class=""sysBtn""><a href=""?ctl=collection_result&act=del&id={$ID}"" ><b class=""icon icon_del""></b>"& Lang_DoClearData &"</a></span>"
	Call Admin.DataList(Sql,20,5,Operation,"")
End Function
'$:采集结果(分页)列表
Function CollectionResultList()
	Dim Temp_Sql
	Select Case DB_Type
	Case 0
	Temp_Sql = "SELECT a.ID, a.Title, b.CollectionName, c.SystemName, a.Thumbnail, a.CollectTime FROM (["& DBTableResult &"] a LEFT JOIN ["& DBTable &"] b ON (a.CollectionID=b.ID)) LEFT JOIN ["& TablePre &"ContentSubSystem] c ON (a.DefaultSystem=c.System) WHERE 1=1 "
	Case 1
	Temp_Sql = "SELECT a.ID, a.Title, b.CollectionName, c.SystemName, a.Thumbnail, a.CollectTime FROM ["& DBTableResult &"] a LEFT JOIN ["& DBTable &"] b ON (a.CollectionID=b.ID) LEFT JOIN ["& TablePre &"ContentSubSystem] c ON (a.DefaultSystem=c.System) WHERE 1=1 "
	End Select
	If SubAction = "search" Then
		tempCondition = KnifeCMS.IIF(Admin.Search.CollectionID>0 ," AND CollectionID="&Admin.Search.CollectionID&" "," ")
		tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.KeyWords <>""," AND Title like '%"& Admin.Search.KeyWords &"%' "," ")
		
		Select Case DB_Type
		Case 0
		tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND CollectTime>=#"& Admin.Search.StartDate &"# ","")
		tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND CollectTime<=#"& Admin.Search.EndDate &"# ","")
		Case 1
		tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND CollectTime>='"& Admin.Search.StartDate &"' ","")
		tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND CollectTime<='"& Admin.Search.EndDate &"' ","")
		End Select
				
		PageUrlPara = "collectionid="& Admin.Search.ClassID &"&startdate="& Server.URLEncode(Admin.Search.StartDate) &"&enddate="& Server.URLEncode(Admin.Search.EndDate) &"&keywords="& Server.URLEncode(Admin.Search.KeyWords) &""
		Temp_Sql = Temp_Sql & tempCondition
	End If
	Temp_Sql = Temp_Sql &" ORDER BY a.ID DESC"
	Operation = "<span class=""sysBtn""><a href=""javascript:;"" onclick=""window.showModalDialog('?ctl="&Ctl&"&act=view&subact=detail&id={$ID}',window,'dialogWidth:780px;dialogHeight:650px;status:yes;center:yes;help:no;resizable:yes;scroll:yes;Minimize=yes;Maximize=yes;')"" ><b class=""icon icon_view""></b>"& Lang_DoView &"</a></span>"
	Operation = Operation & "<span class=""sysBtn""><a href=""?ctl="&Ctl&"&act=edit&subact=detail&id={$ID}"" ><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
	Call Admin.DataList(Temp_Sql,50,6,Operation,PageUrlPara)
End Function


'$:保存采集器
Function CollectionDoSave()
	Server.ScriptTimeOut = 3600
	'获取数据并处理数据
	CollectionName  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","CollectionName")),250)
	AllNum          = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","AllNum"))
	DefaultSystem   = KnifeCMS.Data.Left(KnifeCMS.Data.FiltNotLetter(KnifeCMS.GetForm("post","ContentSubSystem")),50)
	DefaultClassID  = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","ClassID"))
	DefaultDatetime = KnifeCMS.Data.FormatDateTime(KnifeCMS.GetForm("post","DefaultDatetime"),0)
	for each HandUrl in Request.Form("HandUrl")
		HandUrls = KnifeCMS.IIF( HandUrls = "" , KnifeCMS.Data.URLEncode(HandUrl) , HandUrls &","& KnifeCMS.Data.URLEncode(HandUrl))
	next
	AutoUrls        = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","AutoUrls")),250)
	AutoUrls_Start  = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","AutoUrls_Start"))
	AutoUrls_End    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","AutoUrls_End"))
	HtmlCoding      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","HtmlCoding")),10)
	SbjUrlRule      = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","SbjUrlRule"),1000)
	SbjUrlLinkRule  = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","SbjUrlLinkRule"),1000)
	
	Dim RepLink_i,RepLinkTo_i
	for each RepLink_i in Request.Form("RepLink")
		RepLink   = KnifeCMS.IIF( RepLink = "" , KnifeCMS.Data.HTMLEncode(RepLink_i) , RepLink &","& KnifeCMS.Data.HTMLEncode(RepLink_i))
	next
	for each RepLinkTo_i in Request.Form("RepLinkTo")
		RepLinkTo = KnifeCMS.IIF( RepLinkTo = "" , KnifeCMS.Data.HTMLEncode(RepLinkTo_i) , RepLinkTo &","& KnifeCMS.Data.HTMLEncode(RepLinkTo_i))
	next
	
	ThumbnailRule   = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","ThumbnailRule"),1000)
	SaveThumbnail   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","SaveThumbnail"))
	
	Dim RepThumb_i,RepThumbTo_i
	i=1
	for each RepThumb_i in Request.Form("RepThumb")
		RepThumb = KnifeCMS.IIF( i=1 , KnifeCMS.Data.HTMLEncode(RepThumb_i) , RepThumb &","& KnifeCMS.Data.HTMLEncode(RepThumb_i))
		i=i+1
	next
	i=1
	for each RepThumbTo_i in Request.Form("RepThumbTo")
		RepThumbTo = KnifeCMS.IIF( i=1 , KnifeCMS.Data.HTMLEncode(RepThumbTo_i) , RepThumbTo &","& KnifeCMS.Data.HTMLEncode(RepThumbTo_i))
		i=i+1
	next
	
	TitleRule       = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","TitleRule"),1000)
	Dim RepTitle_i,RepTitleTo_i
	i=1
	for each RepTitle_i in Request.Form("RepTitle")
		RepTitle = KnifeCMS.IIF( i=1 , KnifeCMS.Data.HTMLEncode(RepTitle_i) , RepTitle &","& KnifeCMS.Data.HTMLEncode(RepTitle_i))
		i=i+1
	next
	i=1
	for each RepTitleTo_i in Request.Form("RepTitleTo")
		RepTitleTo = KnifeCMS.IIF( i=1 , KnifeCMS.Data.HTMLEncode(RepTitleTo_i) , RepTitleTo &","& KnifeCMS.Data.HTMLEncode(RepTitleTo_i))
		i=i+1
	next
	
	ContentRule     = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","ContentRule"),1000)
	Dim RepContent_i,RepContentTo_i
	i=1
	for each RepContent_i in Request.Form("RepContent")
		RepContent = KnifeCMS.IIF( i=1 , KnifeCMS.Data.HTMLEncode(RepContent_i) , RepContent &","& KnifeCMS.Data.HTMLEncode(RepContent_i))
		i=i+1
	next
	i=1
	for each RepContentTo_i in Request.Form("RepContentTo")
		RepContentTo = KnifeCMS.IIF( i=1 , KnifeCMS.Data.HTMLEncode(RepContentTo_i) , RepContentTo &","& KnifeCMS.Data.HTMLEncode(RepContentTo_i))
		i=i+1
	next
	
	ContentFormat   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","ContentFormat"))
	
	ContentPagetype = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","ContentPagetype"))
	ContentPageRule = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","ContentPageRule"),1000)
	ContentPageUrlRule = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","ContentPageUrlRule"),1000)
	Dim RepPageUrl_i,RepPageUrlTo_i
	i=1
	for each RepPageUrl_i in Request.Form("RepPageUrl")
		RepPageUrl = KnifeCMS.IIF( i=1 , KnifeCMS.Data.HTMLEncode(RepPageUrl_i) , RepPageUrl &","& KnifeCMS.Data.HTMLEncode(RepPageUrl_i))
		i=i+1
	next
	i=1
	for each RepPageUrlTo_i in Request.Form("RepPageUrlTo")
		RepPageUrlTo = KnifeCMS.IIF( i=1 , KnifeCMS.Data.HTMLEncode(RepPageUrlTo_i) , RepPageUrlTo &","& KnifeCMS.Data.HTMLEncode(RepPageUrlTo_i))
		i=i+1
	next
	
	FromRule        = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","FromRule"),1000)
	AuthorRule      = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","AuthorRule"),1000)
	AddTimeRule     = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","AddTimeRule"),50)
	SaveRemotePic   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","SaveRemotePic"))
	SaveRemoteFlash = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","SaveRemoteFlash"))
	SaveRemoteFile  = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","SaveRemoteFile"))
	AddTime         = KnifeCMS.Data.FormatDateTime(SysTime,0)
	
	'数据合法性检测并处理数据
	If CollectionName="" then ErrMsg=Lang_Collection_Cue(8) &"<br>"
	If KnifeCMS.Data.IsNul(HandUrls) and KnifeCMS.Data.IsNul(AutoUrls) then ErrMsg=ErrMsg & Lang_Collection_Cue(9) &"<br>"
	RepLinks        = RepItems(RepLink,RepLinkTo,"[url]",Lang_Collection_Cue(12))
	RepThumbs       = RepItems(RepThumb,RepThumbTo,"[img]",Lang_Collection_Cue(12))
	If KnifeCMS.Data.IsNul(TitleRule) then ErrMsg=ErrMsg & Lang_Collection_Cue(10) &"<br>"
	RepTitles       = RepItems(RepTitle,RepTitleTo,"[title]",Lang_Collection_Cue(12))
	If KnifeCMS.Data.IsNul(ContentRule) then ErrMsg=ErrMsg & Lang_Collection_Cue(11) &"<br>"
	RepContents     = RepItems(RepContent,RepContentTo,"[content]",Lang_Collection_Cue(12))
	RepPageUrls     = RepItems(RepPageUrl,RepPageUrlTo,"[page]",Lang_Collection_Cue(12))
	
	If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
	'开始保存数据操作
	If Action="add" Then
		Result = KnifeCMS.DB.AddRecord(DBTable,Array("SysID:"&SysID,"CollectionName:"& CollectionName,"AllNum:"& AllNum,"DefaultSystem:"& DefaultSystem,"DefaultClassID:"& DefaultClassID,"HandUrls:"&  HandUrls,"AutoUrls:"& AutoUrls,"AutoUrls_Start:"& AutoUrls_Start,"AutoUrls_End:"& AutoUrls_End,"HtmlCoding:"& HtmlCoding,"SbjUrlRule:"& SbjUrlRule,"SbjUrlLinkRule:"& SbjUrlLinkRule,"RepLinks:"& RepLinks,"ThumbnailRule:"& ThumbnailRule,"SaveThumbnail:"& SaveThumbnail,"RepThumbs:"& RepThumbs,"TitleRule:"& TitleRule,"RepTitles:"& RepTitles,"ContentRule:"& ContentRule,"RepContents:"& RepContents,"ContentFormat:"& ContentFormat,"ContentPagetype:"& ContentPagetype,"ContentPageRule:"& ContentPageRule,"ContentPageUrlRule:"& ContentPageUrlRule,"RepPageUrls:"& RepPageUrls,"FromRule:"& FromRule,"AuthorRule:"& AuthorRule,"AddTimeRule:"& AddTimeRule,"DefaultDatetime:"& DefaultDatetime,"SaveRemotePic:"& SaveRemotePic,"SaveRemoteFlash:"& SaveRemoteFlash,"SaveRemoteFile:"& SaveRemoteFile,"AddTime:"& SysTime))
		ID = Admin.GetIDBySysID(DBTable,SysID)
		Msg    = Lang_Collection_Cue(1) &"{"& CollectionName &"[ID="& ID &"]}"
		If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
	Else
		Result = KnifeCMS.DB.UpdateRecord(DBTable,Array("CollectionName:"& CollectionName,"AllNum:"& AllNum,"DefaultSystem:"& DefaultSystem,"DefaultClassID:"& DefaultClassID,"HandUrls:"&  HandUrls,"AutoUrls:"& AutoUrls,"AutoUrls_Start:"& AutoUrls_Start,"AutoUrls_End:"& AutoUrls_End,"HtmlCoding:"& HtmlCoding,"SbjUrlRule:"& SbjUrlRule,"SbjUrlLinkRule:"& SbjUrlLinkRule,"RepLinks:"& RepLinks,"ThumbnailRule:"& ThumbnailRule,"SaveThumbnail:"& SaveThumbnail,"RepThumbs:"& RepThumbs,"TitleRule:"& TitleRule,"RepTitles:"& RepTitles,"ContentRule:"& ContentRule,"RepContents:"& RepContents,"ContentFormat:"& ContentFormat,"ContentPagetype:"& ContentPagetype,"ContentPageRule:"& ContentPageRule,"ContentPageUrlRule:"& ContentPageUrlRule,"RepPageUrls:"& RepPageUrls,"FromRule:"& FromRule,"AuthorRule:"& AuthorRule,"AddTimeRule:"& AddTimeRule,"DefaultDatetime:"& DefaultDatetime,"SaveRemotePic:"& SaveRemotePic,"SaveRemoteFlash:"& SaveRemoteFlash,"SaveRemoteFile:"& SaveRemoteFile),Array("ID:"&ID))
		Msg    = Lang_Collection_Cue(2) &"{"& CollectionName &"[ID="& ID &"]}"
		If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
	End If
End Function

Function CollectionResultDetailDoSave()
	'获取数据并处理数据
	CollectionResult_Title     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","CollectionResult_Title")),250)
	CollectionResult_Thumbnail = KnifeCMS.Data.Left(KnifeCMS.Data.URLEncode(KnifeCMS.GetForm("post","CollectionResult_Thumbnail")),250)
	CollectionResult_From      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","CollectionResult_From")),250)
	CollectionResult_Author    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","CollectionResult_Author")),50)
	CollectionResult_AddTime   = KnifeCMS.Data.FormatDateTime(KnifeCMS.GetForm("post","CollectionResult_AddTime"),0)
	CollectionResult_Content   = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","TextContent")),200000)
	'数据合法性检测并处理数据
	If KnifeCMS.Data.IsNul(CollectionResult_Title)   then ErrMsg = Lang_CollectionResult_Cue(8) &"<br>"
	If KnifeCMS.Data.IsNul(CollectionResult_Content) then ErrMsg = ErrMsg & Lang_CollectionResult_Cue(9) &"<br>"
	If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
	'开始保存数据操作
	Result = KnifeCMS.DB.UpdateRecord(DBTableResult,Array("Title:"& CollectionResult_Title,"Thumbnail:"& CollectionResult_Thumbnail,"From:"& CollectionResult_From,"Author:"& CollectionResult_Author,"AddTime:"& CollectionResult_AddTime,"Content:"& CollectionResult_Content),Array("ID:"&ID))
	
	Msg    = Lang_CollectionResult_Cue(1) &"{"& CollectionResult_Title &"[ID="& ID &"]}"
	If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
End Function

'$:导出采集数据
Function CollectionResultExport()
	'Dim TempCollectionID : TempCollectionID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","collectionid"))
	Dim Fn_Check           : Fn_Check = true
	Dim Fn_CollectionID    : Fn_CollectionID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("get","collectionid"))
	Dim Fn_SelectSubSystem : Fn_SelectSubSystem = KnifeCMS.Data.Left(KnifeCMS.Data.FiltNotLetter(KnifeCMS.GetForm("post","SelectSubSystem")),50)
	Dim Fn_SelectClassID   : Fn_SelectClassID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","SelectClassID"))
	Dim Fn_IsByCollection,Fn_IsBySelected
	Dim Fn_Thumbnail,Fn_Abstract
	Dim Fn_Rs,Fn_Rs2,Fn_Rs3,Fn_Table,Fn_Table_Class,Fn_Num,Fn_SucNum
	Dim Fn_ID_i,Fn_ID
	for each Fn_ID_i in Request.Form("InputName")
		Fn_ID = KnifeCMS.IIF( Fn_ID = "" , KnifeCMS.Data.CLng(Fn_ID_i) , Fn_ID &","& KnifeCMS.Data.CLng(Fn_ID_i))
	next
	Fn_IsByCollection = KnifeCMS.IIF(Fn_CollectionID>0,true,false)
	Fn_IsBySelected   = KnifeCMS.IIF(KnifeCMS.Data.CLng(Fn_ID)>0,true,false)
	
	If Fn_IsBySelected Then
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTableResult,"ID IN("&Fn_ID&")","")
	ElseIf Fn_IsByCollection Then
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTableResult,Array("CollectionID:"&Fn_CollectionID ),"")
	Else
		Exit Function
	End If
	
	Do While Not(Fn_Rs.Eof)
		SysID                      = Fn_Rs("SysID")
		DefaultSystem              = KnifeCMS.IIF(Fn_SelectSubSystem<>"", Fn_SelectSubSystem, Trim(Fn_Rs("DefaultSystem")))
		DefaultClassID             = KnifeCMS.IIF(Fn_SelectClassID > 0  , Fn_SelectClassID,   Fn_Rs("DefaultClassID"))
		CollectionResult_Title     = Fn_Rs("Title")
		CollectionResult_Thumbnail = Fn_Rs("Thumbnail")
		CollectionResult_From      = Fn_Rs("From")
		CollectionResult_Author    = Fn_Rs("Author")
		CollectionResult_AddTime   = Fn_Rs("AddTime")
		CollectionResult_Content   = KnifeCMS.Data.HTMLDecode(Fn_Rs("Content"))

		'采用详细内容的第一张图片作为缩略图
		If KnifeCMS.Data.IsNul(CollectionResult_Thumbnail) Then
			CollectionResult_Thumbnail = KnifeCMS.GetFirstImgURL(CollectionResult_Content)
		End If
		'采用详细内容的前250个字作为内容摘要
		Fn_Abstract    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.Data.HtmlTextTreat(CollectionResult_Content,false,false,false)),250)
		'-------
		Fn_Table       = TablePre & "Content_"& DefaultSystem
		Fn_Table_Class = TablePre & "Content_"& DefaultSystem &"_Class"
		Fn_Num         = Fn_Num + 1
		'如果内容模型存在则...
		Set Fn_Rs2 = KnifeCMS.DB.GetRecord(TablePre&"ContentSubSystem",Array("System:"&DefaultSystem),"")
		If Fn_Rs2.Eof Then Fn_Check = false
		KnifeCMS.DB.CloseRs Fn_Rs2

		If Fn_Check Then
			Set Fn_Rs3 = KnifeCMS.DB.GetRecord(Fn_Table_Class,Array("ID:"&DefaultClassID),"")
			If Fn_Rs3.Eof Then Fn_Check = false
			KnifeCMS.DB.CloseRs Fn_Rs3
		End If
		
		If Fn_Check Then
			CollectionResult_Content = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(CollectionResult_Content),200000)
			
			Result = KnifeCMS.DB.AddRecord(Fn_Table,Array("SysID:"& SysID,"ClassID:"& DefaultClassID,"Title:"& CollectionResult_Title,"TitleFontColor:","TitleFontType:0","BriefTitle:","BriefTitleFontColor:","BriefTitleFontType:0","Author:"&CollectionResult_Author,"From:"& CollectionResult_From,"PublicTime:"& CollectionResult_AddTime,"Thumbnail:"& CollectionResult_Thumbnail,"UseLinkUrl:0","LinkUrl:","Tags:","PaginationType:0","Abstract:"& Fn_Abstract,"Content:"& CollectionResult_Content,"Hits:0","Comments:0","TopLine:0","OnTop:0","Hot:0","Elite:0","CommentSet:0","Openness:0","Password:","SlidesFirstImg:","SlidesImgs:","Status:1","AddTime:"& SysTime,"Diggs:0","Recycle:0"))
			ID = Admin.GetIDBySysID(Fn_Table,SysID)
			Call KnifeCMS.DB.DeleteRecord(DBTableResult,Array("SysID:"&SysID))
			Fn_SucNum = Fn_SucNum + 1
			If Result Then Msg = Msg & Lang_CollectionResult_Cue(3) & CollectionResult_Title &"["& Fn_Table &":ID="& ID &"]<br>"
		Else
			Msg = Msg & Lang_CollectionResult_Cue(4) & CollectionResult_Title &"["& Fn_Table &":ID="& ID &"]<br>"
		End If
	Fn_Rs.Movenext()
	Loop
	KnifeCMS.DB.CloseRs Fn_Rs
	Admin.ShowMessage Lang_CollectionResult_Cue(3) & KnifeCMS.Data.CLng(Fn_SucNum) &"条<br>"&  Lang_CollectionResult_Cue(4) & KnifeCMS.Data.CLng(Fn_Num-Fn_SucNum) &"条<br><br>"& Msg ,"?ctl="& KnifeCMS.IIF(Fn_IsByCollection,"collection",Ctl)&"&act=view",1
End Function

'$:替换内容处理函数
Function RepItems(RepItem,RepItemTo,ItemType,ErrInfo)
	RepItems=""
	If RepItem<>"" Then
		RepItem=Split(RepItem,",")
		If RepItemTo<>"" Then
			RepItemTo=Split(RepItemTo,",")
		Else
		  Redim RepItemTo(1)
		End If
		RepItems=Trim(RepItem(0)) &"]["& Trim(RepItemTo(0))
		If Trim(RepItem(0))="" Then ErrMsg=ErrMsg & ItemType & ErrInfo &"<br>"
		If Ubound(RepItem)>=1 Then
			For Arri=1 To Ubound(RepItem)
				If Trim(RepItem(Arri))<>"" Then
				RepItems=RepItems &"}{"& Trim(RepItem(Arri)) &"]["& Trim(RepItemTo(Arri))
				End If
				If (Trim(RepItem(Arri))="" And Trim(RepItemTo(Arri))<>"") Then
				ErrMsg = ErrMsg & ItemType & ErrInfo &"<br>"
				End If
			Next
		End If
		RepItems = KnifeCMS.Data.ReplaceQuot(RepItems)
	End If
End Function

'$:采集器彻底删除
Function CollectionDel()
	ID        = KnifeCMS.GetForm("post","InputName")
	C_TempArr = Split(ID,",")
	For i=0 To Ubound(C_TempArr)
		ID     = KnifeCMS.Data.Int(C_TempArr(i))
		Result = 0
		If ID > 0 Then
			Set Rs = KnifeCMS.DB.GetRecord(DBTable&":CollectionName",Array("ID:"&ID),"")
			If Not(Rs.Eof) Then
				CollectionName = Rs(0)
				Result = KnifeCMS.DB.DeleteRecord(DBTable,Array("ID:"&ID))
				If Result Then Admin.ShowMessage Lang_Collection_Cue(4) &"{"& CollectionName &"[ID="& ID &"]}",Url,1
			End If
			KnifeCMS.DB.CloseRs Rs
		End If
	Next
End Function

'$:采集结果彻底删除
Function CollectionResultDel()
	If ID>0 Then
		Set Rs = KnifeCMS.DB.GetRecord(DBTable&":CollectionName",Array("ID:"&ID),"")
		If Not(Rs.Eof) Then
			CollectionName = Rs(0)
		End If
		KnifeCMS.DB.CloseRs Rs
		Result = KnifeCMS.DB.DeleteRecord(DBTableResult,Array("CollectionID:"&ID))
		If Result Then Admin.ShowMessage Lang_CollectionResult_Cue(2) &"{"& CollectionName &"[CollectionID="& ID &"]}",Url,1
	Else
		ID        = KnifeCMS.GetForm("post","InputName")
		C_TempArr = Split(ID,",")
		For i=0 To Ubound(C_TempArr)
			ID     = KnifeCMS.Data.Int(C_TempArr(i))
			Result = 0
			If ID > 0 Then
				Set Rs = KnifeCMS.DB.GetRecord(DBTableResult&":Title",Array("ID:"&ID),"")
				If Not(Rs.Eof) Then
					C_TempStr = Rs(0)
					Result = KnifeCMS.DB.DeleteRecord(DBTableResult,Array("ID:"&ID))
					If Result Then Admin.ShowMessage Lang_CollectionResult_Cue(2) &"{"& C_TempStr &"[ID="& ID &"]}",Url,1
				End If
				KnifeCMS.DB.CloseRs Rs
			End If
		Next
	End If
End Function


' $函数：采集数据
'  作用：CollectData(Debug)
'  参数: {Debug}(false/true 或 0/1) 是否是调式模式 0/false非调试模式 1/true调试模式
Function CollectData(Debug)
	Server.ScriptTimeOut = 3600
	Try_Max = 5
	Debug   = CBool(Debug)
	'获得采集器信息
	Set Rs = KnifeCMS.DB.GetRecord(DBTable,Array("ID:"&ID),"")
	If Rs.Bof and Rs.Eof then
		Admin.ShowMessage Lang_Collection_Cue(7),"goback",0 : Exit Function
	Else
		CollectionName  = Rs("CollectionName")
		AllNum          = KnifeCMS.Data.CLng(Rs("AllNum"))
		DefaultSystem   = Rs("DefaultSystem")
		DefaultClassID  = Rs("DefaultClassID")
		HandUrls        = Rs("HandUrls")
		AutoUrls        = Rs("AutoUrls")
		AutoUrls_Start  = Rs("AutoUrls_Start")
		AutoUrls_End    = Rs("AutoUrls_End")
		HtmlCoding      = Rs("HtmlCoding")
		SbjUrlRule      = Rs("SbjUrlRule")
		SbjUrlLinkRule  = Rs("SbjUrlLinkRule")
		RepLinks        = KnifeCMS.Data.ReturnQuot(Rs("RepLinks"))
		ThumbnailRule   = Rs("ThumbnailRule")
		SaveThumbnail   = KnifeCMS.Data.CLng(Rs("SaveThumbnail"))
		RepThumbs       = KnifeCMS.Data.ReturnQuot(Rs("RepThumbs"))
		TitleRule       = Rs("TitleRule")
		RepTitles       = KnifeCMS.Data.ReturnQuot(Rs("RepTitles"))
		ContentRule     = Rs("ContentRule")
		RepContents     = KnifeCMS.Data.ReturnQuot(Rs("RepContents"))
		ContentFormat   = KnifeCMS.Data.CLng(Rs("ContentFormat"))
		ContentPagetype = KnifeCMS.Data.CLng(Rs("ContentPagetype"))
		ContentPageRule = Rs("ContentPageRule")
		ContentPageUrlRule = Rs("ContentPageUrlRule")
		RepPageUrls     = KnifeCMS.Data.ReturnQuot(Rs("RepPageUrls"))
		FromRule        = Rs("FromRule")
		AuthorRule      = Rs("AuthorRule")
		AddTimeRule     = Rs("AddTimeRule")
		DefaultDatetime = Rs("DefaultDatetime")
		SaveRemotePic   = Cbool(KnifeCMS.Data.CLng(Rs("SaveRemotePic")))
		SaveRemoteFlash = Cbool(KnifeCMS.Data.CLng(Rs("SaveRemoteFlash")))
		SaveRemoteFile  = Cbool(KnifeCMS.Data.CLng(Rs("SaveRemoteFile")))
	End if
	Set Rs=nothing
	Dim httpHtmlCoding,regEx,regExi,UrlLinks_i,RepLinks_i,PageUrls_i,Thumbnail_i,RepThumbs_i,RepTitles_i,RepContents_i,RepPageUrls_i
	Dim CollectUrls,SbjUrlArea,tmpSbjUrlArea,SbjUrlLinks,SbjUrlLink,Thumbnails,Url,Urls,Urls_Num,Urls_i,UrlLinks,HandUrls_Num,AutoUrls_Num,PageUrls,PageUrl,ContentPageUrls
	Dim SbjUrlRuleStart,SbjUrlRuleEnd,SbjUrlLinkRuleStart,SbjUrlLinkRuleEnd,ThumbnailRuleStart,ThumbnailRuleEnd,TitleRuleStart,TitleRuleEnd,ContentRuleStart,ContentRuleEnd,ContentPageRuleStart,ContentPageRuleEnd,ContentPageUrlRuleStart,ContentPageUrlRuleEnd,FromRuleStart,FromRuleEnd,AuthorRuleStart,AuthorRuleEnd,AddTimeRuleStart,AddTimeRuleEnd
	Dim AutoUrls_Start_Len,AutoUrls_End_Len,tmp_AutoUrls_Start
	Dim LocalFileName,SavedOK
	Dim CollectArtsNum,CollectArtsSucNum
		CollectArtsNum=0
		CollectArtsSucNum=0
		HandUrls_Num=0
		AutoUrls_Num=0
		Urls_Num=0
		'HtmlCoding=KnifeCMS.Http.HtmlCharset(HtmlCoding)
	If Trim(HandUrls)<>"" Then
		HandUrls=Split(HandUrls,",")
		HandUrls_Num=Ubound(HandUrls)+1
	End If
	
	If KnifeCMS.Data.CLng(AutoUrls_End) < KnifeCMS.Data.CLng(AutoUrls_Start) Then
		tmp_AutoUrls_Start=AutoUrls_Start
		AutoUrls_Start=AutoUrls_End
		AutoUrls_End=tmp_AutoUrls_Start
	End If
	AutoUrls_Start_Len=Len(AutoUrls_Start)
	AutoUrls_End_Len=Len(AutoUrls_End)
	
	If Trim(AutoUrls)<>"" Then
		AutoUrls_Num=KnifeCMS.Data.CLng(AutoUrls_End-AutoUrls_Start)+1
	End If
	
	Urls_Num=KnifeCMS.Data.CLng(HandUrls_Num+AutoUrls_Num)
	Redim Urls(Urls_Num)

	'把手动网址存入数组Urls
	If HandUrls_Num > 0 Then
		For Urls_i=0 To HandUrls_Num-1
			Urls(Urls_i)=KnifeCMS.Data.URLDecode(HandUrls(Urls_i))
		Next
	End If
	'把自动网址存入数组Urls
	If AutoUrls_Num > 0 And Trim(AutoUrls)<>"" Then
		For Urls_i=1 To AutoUrls_Num
			Url=Replace(AutoUrls,"[page]",KnifeCMS.Data.FormatToFixNumber(AutoUrls_Start_Len,AutoUrls_Start+Urls_i-1))
			Urls(HandUrls_Num+Urls_i-1)=Url
		Next
	End If
	'调试信息 采集网址列表(手工列表页URL+自动列表页URL)
	If Debug Then
		Echo "<div class=""Debug m10""><div class=CtDebug_head>"& Lang_Collection_Cue(30) &"<!--采集网址列表(手工列表页URL+自动列表页URL)--></div><textarea class=""CtDebug low"">"
		For Urls_i=1 To Urls_Num
			Echo Urls_i &"."& Server.HTMLEncode(Urls(Urls_i-1)) & vbCrLf
		Next
		Echo "</textarea></div>"
	End If
	
	'调试信息 如果是调试状态 则以第1个网页信息作为调式对象
	If Debug Then AllNum=1
	
	'============ 采集开始(循环) ==============
	If Urls_Num > 0 Then
		For Urls_i=1 To Urls_Num
			'如果 目前采集个数 大于或等于 设定的采集总个数 则停止采集
			If KnifeCMS.Data.CLng(CollectArtsNum) >= KnifeCMS.Data.CLng(AllNum) Then Exit For
			
			Url=Urls(Urls_i-1)
			Echo "<dl class=Collecting><dt>"& Url &"</dt><dd>"& Lang_Collection_Cue(31) &"<!--正在采集......--><br><br>" : Response.Flush()

			strHtml=KnifeCMS.Http.GetHttpPage(Url,HtmlCoding)
			For Try_i=1 To Try_Max
				If Trim(strHtml) = "" Or Trim(strHtml)="$False$" Then strHtml=KnifeCMS.Http.GetHttpPage(Url,HtmlCoding)
			Next
			
			'获取采集到的网页的编码
			httpHtmlCoding=Trim(KnifeCMS.Http.GetBody(strHtml, "charset=", """", False, False))
			If Trim(httpHtmlCoding)<>"" And Trim(httpHtmlCoding) <> Trim(HtmlCoding) Then
				HtmlCoding=httpHtmlCoding
				strHtml=KnifeCMS.Http.GetHttpPage(Url,HtmlCoding)
			End If
			'调试信息 页面HTML源码
			If Debug Then Echo "<div class=Debug><div class=CtDebug_head><span class=Url>"& Server.HTMLEncode(Url) &"</span>"& Lang_Collection_Cue(32) &"<!--页面HTML源码--></div><textarea class=CtDebug>"& Server.HTMLEncode(strHtml) &"</textarea></div>"
			
			
			'==第一级== 存在该地址则采集内容 ====
			If Trim(strHtml)<>"" And trim(strHtml)<>"$False$" Then
			
				'==== 获取文章链接URL ====
				If Trim(SbjUrlLinkRule)<>"" Then
					'SbjUrlRuleStart=GetRuleStart(SbjUrlRule,"[list]")
'					SbjUrlRuleEnd=GetRuleEnd(SbjUrlRule,"[list]")
'					SbjUrlArea=KnifeCMS.Http.GetBody(strHtml, SbjUrlRuleStart, SbjUrlRuleEnd, True, True)
					SbjUrlArea = KnifeCMS.Data.GetStringByMatch(strHtml,SbjUrlRule,"[list]")
					
					'调试信息 列表区域
					If Debug Then Echo "<div class=Debug><div class=CtDebug_head><span class=Url>"& Lang_Collection_Cue(33) &"<!--列表区域识别规则-->:"& Server.HTMLEncode(SbjUrlRule) &"</span>"& Lang_Collection_Cue(34) &"<!--列表区域--></div><textarea class=CtDebug>"& Server.HTMLEncode(SbjUrlArea) &"</textarea></div>"
					SbjUrlLinkRuleStart=GetRuleStart(SbjUrlLinkRule,"[url]")
					SbjUrlLinkRuleEnd=GetRuleEnd(SbjUrlLinkRule,"[url]")
					Set regEx=new RegExp
					regEx.IgnoreCase =False
					regEx.Global=True
					regEx.Pattern = "("& SbjUrlLinkRuleStart &")"
					Set SbjUrlLinks = regEx.Execute(SbjUrlArea)
					Redim UrlLinks(SbjUrlLinks.Count)
					tmpSbjUrlArea=SbjUrlArea
					For UrlLinks_i=1 To SbjUrlLinks.Count
						'SbjUrlLink=KnifeCMS.Http.GetBody(tmpSbjUrlArea, SbjUrlLinkRuleStart, SbjUrlLinkRuleEnd, False, False)'取得的url
						SbjUrlLink = KnifeCMS.Data.GetStringByMatch(tmpSbjUrlArea,SbjUrlLinkRule,"[url]")
						
						tmpSbjUrlArea=Replace(tmpSbjUrlArea,SbjUrlLinkRuleStart&SbjUrlLink&SbjUrlLinkRuleEnd,"")'去掉已经取得的url
						'SbjUrlLink=SbjUrlLink
						'Echo ""&Server.HTMLEncode(SbjUrlLink)&"<br><br>" : response.Flush()
						RepLinks_i=RepLinks
						If Trim(RepLinks_i)<>"" Then
							RepLinks_i=Split(RepLinks_i,"}{")
							For Arrj=0 to Ubound(RepLinks_i)
								ArrRepLinks=Split(RepLinks_i(Arrj),"][")
								SbjUrlLink=Replace(SbjUrlLink,ArrRepLinks(0),ArrRepLinks(1))'文章链接URL字符替换
								'Echo Server.HTMLEncode(SbjUrlLink)&"<br><br>" : response.Flush()
							Next
						End If
						UrlLinks(UrlLinks_i-1)=SbjUrlLink
					Next
				Else
					Redim UrlLinks(1)
					UrlLinks(0)=Url
				End If
				'调试信息 文章列表URL
				If Debug Then
					Echo "<div class=Debug><div class=CtDebug_head><span class=Url>"& Lang_Collection_Cue(35) &"<!--文章链接URL识别规则-->:"& Server.HTMLEncode(SbjUrlLinkRule) &"</span>"& Lang_Collection_Cue(36) &"<!--文章列表URL--></div><textarea class=""CtDebug low"">"
					For UrlLinks_i=0 To Ubound(UrlLinks)-1
						Echo UrlLinks_i+1 &"."& Server.HTMLEncode(UrlLinks(UrlLinks_i)) & vbCrLf
					Next
					Echo "</textarea></div>"
				End If
				
				'==== 获取缩略图 url ====
				If Trim(ThumbnailRule)<>"" Then
					ThumbnailRuleStart=GetRuleStart(ThumbnailRule,"[img]")
					ThumbnailRuleEnd=GetRuleEnd(ThumbnailRule,"[img]")
					tmpSbjUrlArea=SbjUrlArea
					If Trim(SbjUrlLinkRule)<>"" Then'如果存在文章列表区则在文章列表区采集缩略图
						regEx.Pattern = "("& ThumbnailRuleStart &")"
						Set Thumbnails = regEx.Execute(SbjUrlArea)
						Redim ThumbnailUrls(Thumbnails.Count)
							'Echo server.HTMLEncode(regEx.Pattern) &"[=]"& Thumbnails.Count&"<br><br>" : response.Flush()
						For Thumbnail_i=1 To Thumbnails.Count
							'Thumbnail=KnifeCMS.Http.GetBody(tmpSbjUrlArea, ThumbnailRuleStart, ThumbnailRuleEnd, False, False)'取得img
							Thumbnail = KnifeCMS.Data.GetStringByMatch(tmpSbjUrlArea,ThumbnailRule,"[img]")
							tmpSbjUrlArea=Replace(tmpSbjUrlArea,ThumbnailRuleStart&Thumbnail&ThumbnailRuleEnd,"")'去掉已经取得的img
							RepThumbs_i=RepThumbs
							If Trim(RepThumbs_i)<>"" Then
								RepThumbs_i=Split(RepThumbs_i,"}{")
								For Arrj=0 to Ubound(RepThumbs_i)
									ArrRepThumbs=Split(RepThumbs_i(Arrj),"][")
									Thumbnail=Replace(Thumbnail,ArrRepThumbs(0),ArrRepThumbs(1))'缩略图URL字符替换
									'Echo Server.HTMLEncode(SbjUrlLink)&"<br><br>" : response.Flush()
								Next
							End If
							ThumbnailUrls(Thumbnail_i-1)=Thumbnail
							'Echo Server.HTMLEncode(Thumbnail)&"<br>" : response.Flush()
						Next
					Else'如果不存在文章列表区则表示直接从手工/自动列表页中采集详细文章内容，所以缩略图也在其中采集
						'Thumbnail=KnifeCMS.Http.GetBody(strHtml, ThumbnailRuleStart, ThumbnailRuleEnd, False, False)'取得img
						Thumbnail = KnifeCMS.Data.GetStringByMatch(strHtml,ThumbnailRule,"[img]")
						Redim ThumbnailUrls(1)
						ThumbnailUrls(0)=Thumbnail
					End If
					'调试信息 文章列表缩略图URL
					If Debug Then
						Echo "<div class=Debug><div class=CtDebug_head><span class=Url>"& Lang_Collection_Cue(37) &"<!--文章缩略图识别规则-->:"& Server.HTMLEncode(ThumbnailRule) &"</span>"& Lang_Collection_Cue(38) &"<!--文章列表缩略图URL--></div><textarea class=""CtDebug low"">"
						For Thumbnail_i=0 To Ubound(ThumbnailUrls)-1
							Echo Thumbnail_i+1 &"."& Server.HTMLEncode(ThumbnailUrls(Thumbnail_i)) & vbCrLf
						Next
						Echo "</textarea></div>"
					End If
				End If

				'==== 另一种方式 获取缩略图 url ====
				  '				If Trim(ThumbnailRule)<>"" Then
				  '					ThumbnailRuleStart=GetRuleStart(ThumbnailRule,"[img]")
				  '					ThumbnailRuleEnd=GetRuleEnd(ThumbnailRule,"[img]")
				  '					tmpSbjUrlArea=SbjUrlArea
				  '					    'Echo("<pre>"&Server.HTMLEncode(tmpSbjUrlArea)&"</pre>")
				  '					If Trim(SbjUrlLinkRule)<>"" Then
				  '						Dim ExitThumbnail
				  '						    ExitThumbnail = True
				  '						Do While ExitThumbnail
				  '							Thumbnail=KnifeCMS.Http.GetBody(tmpSbjUrlArea, ThumbnailRuleStart, ThumbnailRuleEnd, False, False)'取得img
				  '							tmpSbjUrlArea=Replace(tmpSbjUrlArea,ThumbnailRuleStart&Thumbnail&ThumbnailRuleEnd,"")'去掉已经取得的img
				  '								Echo server.HTMLEncode(ThumbnailRuleStart&Thumbnail&ThumbnailRuleEnd) &"[=]"& Thumbnail &"<br><br>"
				  '								response.Flush()
				  '							If Thumbnail = "$False$" Then
				  '								ExitThumbnail = False
				  '							Else
				  '								RepThumbs_i=RepThumbs
				  '								If Trim(RepThumbs_i)<>"" Then
				  '									RepThumbs_i=Split(RepThumbs_i,"}{")
				  '									For Arrj=0 to Ubound(RepThumbs_i)
				  '										ArrRepThumbs=Split(RepThumbs_i(Arrj),"][")
				  '										Thumbnail=Replace(Thumbnail,ArrRepThumbs(0),ArrRepThumbs(1))'缩略图URL字符替换
				  '										'Echo Server.HTMLEncode(SbjUrlLink)&"<br><br>"
				  '										'response.Flush()
				  '									Next
				  '								End If
				  '							End If
				  '							If Trim(Thumbnails) <> "" then
				  '								If Thumbnail <> "$False$" Then
				  '									Thumbnails=Thumbnails &"}{"& Thumbnail
				  '								End If
				  '							Else
				  '								Thumbnails = Thumbnail
				  '							End If
				  '						Loop
				  '						
				  '						Thumbnails=Split(Thumbnails,"}{")
				  '						Redim ThumbnailUrls(Ubound(Thumbnails))
				  '						For Thumbnail_i=0 to Ubound(Thumbnails)
				  '							ThumbnailUrls(Thumbnail_i)=Thumbnails(Thumbnail_i)
				  '						Next
				  '						
				  '					Else
				  '						Thumbnail=KnifeCMS.Http.GetBody(strHtml, ThumbnailRuleStart, ThumbnailRuleEnd, False, False)'取得img
				  '						Redim ThumbnailUrls(1)
				  '						ThumbnailUrls(0)=Thumbnail
				  '					End If
				  '				End If
	
				'==== 开始采集文章(循环) ====
				For UrlLinks_i=0 To Ubound(UrlLinks)-1
					'如果 目前采集个数 大于或等于 设定的采集总个数 则停止采集
					If KnifeCMS.Data.CLng(CollectArtsNum) >= KnifeCMS.Data.CLng(AllNum) Then Exit For
					
					SysID=KnifeCMS.CreateSysID
					
					'采集缩略图
					Thumbnail=""
					IF Trim(ThumbnailRule)<>"" Then
						Echo "<br><br>"& CollectArtsNum+1 &". Get Thumbnail......<br>" : Response.Flush()
						If UrlLinks_i <= Ubound(ThumbnailUrls) Then
							If SaveThumbnail = 1 Then '保存缩略图到本地
								Thumbnail=KnifeCMS.Http.ReplaceRemoteUrl(ThumbnailUrls(UrlLinks_i),1,0,0,Debug)
							Else
								Thumbnail = ThumbnailUrls(UrlLinks_i)
							End If
						End If
					    Echo Server.HTMLEncode(Thumbnail)&"<br>" : Response.Flush()
					End IF
					
					
					'获取内容页面HTML源码
					'Echo "<br>url: "& UrlLinks(UrlLinks_i) &"<br>" : response.Flush()
					strHtml=KnifeCMS.Http.GetHttpPage(UrlLinks(UrlLinks_i),HtmlCoding)
					'获取采集到的网页的编码
					httpHtmlCoding=Trim(KnifeCMS.Http.GetBody(strHtml, "charset=", """", False, False))
					'Echo "httpHtmlCoding="& httpHtmlCoding : response.End()
					If Trim(httpHtmlCoding)<>"" And Trim(httpHtmlCoding)<>Trim(HtmlCoding) Then
						HtmlCoding=httpHtmlCoding
						strHtml=KnifeCMS.Http.GetHttpPage(UrlLinks(UrlLinks_i),HtmlCoding)
					End If
					
					'调试信息 内容页面-HTML源码
					If Debug Then Echo "<div class=Debug><div class=CtDebug_head><span class=Url>"& Server.HTMLEncode(UrlLinks(UrlLinks_i)) &"</span><b>内容页面-HTML源码</b></div><textarea class=""CtDebug"">"& Server.HTMLEncode(strHtml) &"</textarea></div>"
					
					
					'采集文章标题
					TitleRuleStart=GetRuleStart(TitleRule,"[title]")
					TitleRuleEnd=GetRuleEnd(TitleRule,"[title]")
					'strTitle=KnifeCMS.Http.GetBody(strHtml, TitleRuleStart, TitleRuleEnd, False, False)'取得文章标题title
					strTitle = KnifeCMS.Data.GetStringByMatch(strHtml,TitleRule,"[title]")
					For i=0 to 10
						If strTitle = "$False$" Then
							'strTitle=KnifeCMS.Http.GetBody(strHtml, TitleRuleStart, TitleRuleEnd, False, False)'取得文章标题title
							strTitle = KnifeCMS.Data.GetStringByMatch(strHtml,TitleRule,"[title]")
						End If
					Next
					'调试信息 内容页面-文章标题
					If Debug Then Echo "<div class=Debug><div class=CtDebug_head><span class=Url>"& Lang_Collection_Cue(39) &"<!--文章标题识别规则-->:"& Server.HTMLEncode(TitleRule) &"</span><b>"& Lang_Collection_Cue(40) &"<!--内容页面-文章标题(文章标题字符替换前的)--></b></div><textarea class=""CtDebug minilow"">"& Server.HTMLEncode(strTitle) &"</textarea></div>"
					
					RepTitles_i=RepTitles
					If Trim(RepTitles_i)<>"" Then
						RepTitles_i=Split(RepTitles_i,"}{")
						For Arrj=0 to Ubound(RepTitles_i)
							ArrRepTitles=Split(RepTitles_i(Arrj),"][")
							strTitle=Replace(strTitle,ArrRepTitles(0),ArrRepTitles(1))'文章链接URL字符替换
						Next
					End If
					Echo "<p>"& CollectArtsNum+1 &". "& Server.HTMLEncode(strTitle)&"</p>"
					Response.Flush()
					strTitle=KnifeCMS.Data.ReplaceQuot(strTitle)
					
					'如果采集文章标题成功则继续采集 文章内容\来源\发布时间\作者\图片等.....
					If Trim(strTitle)<>"$False$" Then
						
						'采集文章内容
						ContentRuleStart=GetRuleStart(ContentRule,"[content]")
						ContentRuleEnd=GetRuleEnd(ContentRule,"[content]")
						'strContent=KnifeCMS.Http.GetBody(strHtml, ContentRuleStart, ContentRuleEnd, False, False)'取得文章内容
						strContent = KnifeCMS.Data.GetStringByMatch(strHtml,ContentRule,"[content]")
						strContentAll=strContent
						'调试信息 内容页面-第一页文章内容
						If Debug Then Echo "<div class=Debug><div class=CtDebug_head><span class=Url>"& Lang_Collection_Cue(41) &"<!--文章标题-->:"& Server.HTMLEncode(strTitle) &"</span><b>"& Lang_Collection_Cue(42) &"<!--内容页面-第一页文章内容(文章内容替换前的)--></b></div><textarea class=""CtDebug"">"& Server.HTMLEncode(strContentAll) &"</textarea></div>"
						
						'采集各分页内容
						If ContentPagetype > 0 Then
							If Trim(ContentPageRule) <> "" Then
									ContentPageRuleStart=GetRuleStart(ContentPageRule,"[pagearea]")
									ContentPageRuleEnd=GetRuleEnd(ContentPageRule,"[pagearea]")
									ContentPageUrlRuleStart=GetRuleStart(ContentPageUrlRule,"[page]")
									ContentPageUrlRuleEnd=GetRuleEnd(ContentPageUrlRule,"[page]")
								
								    Dim PageAllUrls
									Redim PageAllUrls(2000)'这里定义最大分页数为2000
									    PageAllUrls(0)=UrlLinks(UrlLinks_i)
									Dim PageUrls_Num,PageUrls_tmpNum
									    PageUrls_Num=0
									Dim PageUrls_j

									'strContentPage=KnifeCMS.Http.GetBody(strHtml, ContentPageRuleStart, ContentPageRuleEnd, False, False)
									strContentPage = KnifeCMS.Data.GetStringByMatch(strHtml,ContentPageRule,"[pagearea]")
									'Echo Server.HTMLEncode(strContentPage) &"<br><br>"
									
									Set regExi=new RegExp
									regExi.IgnoreCase =False
									regExi.Global=True
									regExi.Pattern = "("& ContentPageUrlRuleStart &")"
									Set ContentPageUrls = regExi.Execute(strContentPage)
									
									Redim PageUrls(ContentPageUrls.Count)
									
									'从第一页取得分页网址
									For PageUrls_i=1 To ContentPageUrls.Count
										'PageUrl=KnifeCMS.Http.GetBody(strContentPage, ContentPageUrlRuleStart, ContentPageUrlRuleEnd, False, False)'取得的url
										PageUrl = KnifeCMS.Data.GetStringByMatch(strContentPage,ContentPageUrlRule,"[page]")
										strContentPage=Replace(strContentPage,ContentPageUrlRuleStart&PageUrl&ContentPageUrlRuleEnd,"")'去掉已经取得的url
										RepPageUrls_i=RepPageUrls
										If Trim(RepPageUrls_i)<>"" Then
											RepPageUrls_i=Split(RepPageUrls_i,"}{")
											For Arrj=0 to Ubound(RepPageUrls_i)
												ArrRepPageUrls=Split(RepPageUrls_i(Arrj),"][")
												PageUrl=Replace(PageUrl,ArrRepPageUrls(0),ArrRepPageUrls(1))'链接URL字符替换
											Next
										End If
										PageUrls(PageUrls_i-1)=PageUrl
										'Echo PageUrls(PageUrls_i-1) &"<br>" : response.Flush()
									Next
									
									'把PageUrls中的Urls存入PageAllUrls,并去除相同的网址.
									For PageUrls_i=0 To Ubound(PageUrls)
										For PageUrls_j=0 To PageUrls_Num 'PageUrls_Num=0
											If PageAllUrls(PageUrls_j) = PageUrls(PageUrls_i) Then
												Exit For
											Else
												If PageUrls_j = PageUrls_Num And PageUrls(PageUrls_i) <> "$False$" Then
													PageAllUrls(PageUrls_Num+1) = PageUrls(PageUrls_i)
													PageUrls_tmpNum=PageUrls_Num+1
												End If
											End If
											'Echo Ubound(PageAllUrls) &"-1-<br>"
										Next
										If PageUrls_tmpNum > PageUrls_Num Then
											PageUrls_Num=PageUrls_tmpNum
										End If
									Next
									    'Echo PageUrls_Num &"<br>"
									
									'取得各分页中的分页URL
									Dim strPagesHtml,tmpPages_ii
									For PageUrls_i=1 To Ubound(PageAllUrls)
										PageUrls_tmpNum=PageUrls_Num
										PageUrls_j=PageUrls_i
										For tmpPages_ii=PageUrls_i To PageUrls_tmpNum-1
											If tmpPages_ii > PageUrls_j Then
												Exit For
											End If
											'获取各分页HTML源码
											If Not(PageAllUrls(PageUrls_j) = "" Or isNull(PageAllUrls(PageUrls_j))) Then
												strPagesHtml=KnifeCMS.Http.GetHttpPage(PageAllUrls(PageUrls_j),HtmlCoding)
												'获取采集到的网页的编码
												httpHtmlCoding=Trim(KnifeCMS.Http.GetBody(strPagesHtml, "charset=", """", False, False))
												'Echo "httpHtmlCoding="& httpHtmlCoding : response.End()
												If Trim(httpHtmlCoding)<>"" And Trim(httpHtmlCoding)<>Trim(HtmlCoding) Then
													HtmlCoding=httpHtmlCoding
													strPagesHtml=KnifeCMS.Http.GetHttpPage(PageAllUrls(PageUrls_j),HtmlCoding)
												End If
												'strContent=KnifeCMS.Http.GetBody(strPagesHtml, ContentRuleStart, ContentRuleEnd, False, False)'取得文章内容
												strContent = KnifeCMS.Data.GetStringByMatch(strPagesHtml,ContentRule,"[content]")
												If Trim(strContent) <> "$False$" Then
													strContentAll=strContentAll &"<!--[nextpage]-->"& strContent
													'strContentPage=KnifeCMS.Http.GetBody(strPagesHtml, ContentPageRuleStart, ContentPageRuleEnd, False, False)
													strContentPage = KnifeCMS.Data.GetStringByMatch(strPagesHtml,ContentPageRule,"[pagearea]")
													Set ContentPageUrls = regExi.Execute(strContentPage)
													Redim PageUrls(ContentPageUrls.Count)
													'取得分页网址
													Dim tmpPageUrls_i,tmpPageUrls_j
													For tmpPageUrls_i=1 To ContentPageUrls.Count
														'PageUrl=KnifeCMS.Http.GetBody(strContentPage, ContentPageUrlRuleStart, ContentPageUrlRuleEnd, False, False)'取得的url
														PageUrl = KnifeCMS.Data.GetStringByMatch(strContentPage,ContentPageUrlRule,"[page]")
														strContentPage=Replace(strContentPage,ContentPageUrlRuleStart&PageUrl&ContentPageUrlRuleEnd,"")'去掉已经取得的url
														RepPageUrls_i=RepPageUrls
														If Trim(RepPageUrls_i)<>"" Then
															RepPageUrls_i=Split(RepPageUrls_i,"}{")
															For Arrj=0 to Ubound(RepPageUrls_i)
																ArrRepPageUrls=Split(RepPageUrls_i(Arrj),"][")
																PageUrl=Replace(PageUrl,ArrRepPageUrls(0),ArrRepPageUrls(1))'链接URL字符替换
															Next
														End If
														PageUrls(tmpPageUrls_i-1)=PageUrl
														    'Echo PageUrls(tmpPageUrls_i-1) &"<br>" : response.Flush()
													Next
													'把PageUrls中的Urls存入PageAllUrls,并去除相同的网址.
													For tmpPageUrls_i=0 To Ubound(PageUrls)
														For tmpPageUrls_j=0 To PageUrls_Num 'PageUrls_Num=0
															If PageAllUrls(tmpPageUrls_j) = PageUrls(tmpPageUrls_i) Then
																Exit For
															Else
																If tmpPageUrls_j = PageUrls_Num And PageUrls(tmpPageUrls_i) <> "$False$" Then
																	PageAllUrls(PageUrls_Num) = PageUrls(tmpPageUrls_i)
																	PageUrls_tmpNum=PageUrls_Num+1
																End If
															End If
															     'Echo Ubound(PageAllUrls) &"-1-<br>"
														Next
														If PageUrls_tmpNum > PageUrls_Num Then
															PageUrls_Num=PageUrls_tmpNum
														End If
													Next
												End If
											End If
										Next
									Next
									
									'分页urls
									'For PageUrls_i=0 To PageUrls_Num-1 'Ubound(PageAllUrls)
										'Echo "PageAllUrls("&PageUrls_i&")="& PageAllUrls(PageUrls_i) &"<br>" : response.Flush()
									'Next
									
									'Echo PageUrls_Num' : response.End()
								    '把分页链接存入数组PageUrls ， 再循环采集分页内容
									
									'调试信息 内容页面-分页地址列表
									If Debug Then
										Echo "<div class=Debug><div class=CtDebug_head><span class=Url>"& Lang_Collection_Cue(43) &"<!--分页链接识别规则-->:"& Server.HTMLEncode(SbjUrlLinkRule) &"</span><b>"& Lang_Collection_Cue(44) &"<!--内容页面-分页地址列表(分页链接字符替换后的)--></b></div><textarea class=""CtDebug low"">"
										For tmpPageUrls_i=0 To Ubound(PageAllUrls)
											If PageAllUrls(tmpPageUrls_i)="" Then Exit For
											Echo tmpPageUrls_i+1 &"."& Server.HTMLEncode(PageAllUrls(tmpPageUrls_i)) & vbCrLf
										Next
										Echo "</textarea></div>"
									End If
							End If
						End If
						
						'内容替换
						If Trim(RepContents)<>"" Then
							RepContents_i=RepContents
							RepContents_i=Split(RepContents_i,"}{")
							For Arrj=0 to Ubound(RepContents_i)
								ArrRepContents=Split(RepContents_i(Arrj),"][")
								strContentAll=Replace(strContentAll,ArrRepContents(0),ArrRepContents(1))'文章链接URL字符替换
							Next
						End If
						'调试信息 内容页面-文章内容(包括所有分页的文章内容替换后的)
						If Debug Then Echo "<div class=Debug><div class=CtDebug_head><span class=Url>"& Server.HTMLEncode(UrlLinks(UrlLinks_i)) &"</span><b>"& Lang_Collection_Cue(45) &"<!--内容页面-文章内容(包括所有分页的文章内容替换后的)--></b></div><textarea class=""CtDebug"">"& Server.HTMLEncode(strContentAll) &"</textarea></div>"
						
						Select Case ContentFormat
							Case 1 : strContentAll=KnifeCMS.Data.HtmlTextTreat(strContentAll,1,1,1)
							Case 2 : strContentAll=KnifeCMS.Data.HtmlTextTreat(strContentAll,1,1,0)
							Case 3 : strContentAll=KnifeCMS.Data.HtmlTextTreat(strContentAll,0,0,0)
							'Case Else : strContentAll=strContentAll
						End Select
						
						If ContentFormat > 0 Then
							'调试信息 内容页面-文章内容(包括所有分页的文章内容替换后的)
							If Debug Then Echo "<div class=Debug><div class=CtDebug_head><span class=Url>"& Server.HTMLEncode(UrlLinks(UrlLinks_i)) &"</span><b>"& Lang_Collection_Cue(46) &"<!--内容页面-文章内容(格式化后的)--></b></div><textarea class=""CtDebug"">"& Server.HTMLEncode(strContentAll) &"</textarea></div>"
						End If
						
						'采集信息来源
						FromRuleStart=GetRuleStart(FromRule,"[from]")
						FromRuleEnd=GetRuleEnd(FromRule,"[from]")
						'strFrom=KnifeCMS.Http.GetBody(strHtml, FromRuleStart, FromRuleEnd, False, False)
						strFrom = KnifeCMS.Data.GetStringByMatch(strHtml,FromRule,"[from]")
						strFrom=Replace(strFrom,"$False$","")
						'调试信息 内容页面-信息来源
						If Debug Then Echo "<div class=Debug><div class=CtDebug_head><span class=Url>"& Lang_Collection_Cue(47) &"<!--信息来源识别规则-->:"& Server.HTMLEncode(FromRule) &"</span><b>"& Lang_Collection_Cue(48) &"<!--内容页面-信息来源--></b></div><textarea class=""CtDebug minilow"">"& Server.HTMLEncode(strFrom) &"</textarea></div>"
						'strFrom=strFrom
						
						'采集作者
						AuthorRuleStart=GetRuleStart(AuthorRule,"[author]")
						AuthorRuleEnd=GetRuleEnd(AuthorRule,"[author]")
						'strAuthor=KnifeCMS.Http.GetBody(strHtml, AuthorRuleStart, AuthorRuleEnd, False, False)
						strAuthor = KnifeCMS.Data.GetStringByMatch(strHtml,AuthorRule,"[author]")
						strAuthor=Replace(strAuthor,"$False$","")
						'调试信息 内容页面-作者
						If Debug Then Echo "<div class=Debug><div class=CtDebug_head><span class=Url>"& Lang_Collection_Cue(49) &"<!--作者识别规则-->:"& Server.HTMLEncode(AuthorRule) &"</span><b>"& Lang_Collection_Cue(50) &"<!--内容页面-作者--></b></div><textarea class=""CtDebug minilow"">"& Server.HTMLEncode(strAuthor) &"</textarea></div>"
						'strAuthor=strAuthor
						
						'采集发布时间
						AddTimeRuleStart=GetRuleStart(AddTimeRule,"[time]")
						AddTimeRuleEnd=GetRuleEnd(AddTimeRule,"[time]")
						'strAddTime=KnifeCMS.Http.GetBody(strHtml, AddTimeRuleStart, AddTimeRuleEnd, False, False)
						strAddTime = KnifeCMS.Data.GetStringByMatch(strHtml,AddTimeRule,"[time]")
						strAddTime=KnifeCMS.Data.FormatDateTime(Trim(Replace(strAddTime,"$False$","")),0)
						If Trim(strAddTime)="" Then strAddTime=SysTime
						strAddTime=KnifeCMS.Data.FormatDateTime(strAddTime,0)
						'调试信息 内容页面-发布时间
						If Debug Then Echo "<div class=Debug><div class=CtDebug_head><span class=Url>"& Lang_Collection_Cue(51) &"<!--发布时间识别规则-->:"& Server.HTMLEncode(AddTimeRule) &"</span><b>"& Lang_Collection_Cue(52) &"<!--内容页面-发布时间--></b></div><textarea class=""CtDebug minilow"">"& strAddTime &"</textarea></div>"
						'strAddTime=strAddTime
						
						'保存图片/rar&zip/Flash到本地
						strContentAll=KnifeCMS.Http.ReplaceRemoteUrl(strContentAll,SaveRemotePic,SaveRemoteFile,SaveRemoteFlash,Debug)
						
						'把采集结果保存到数据库
						If Not(Debug) Then
							strFrom       = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(strFrom),250)
							strAuthor     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(strAuthor),50)
							strTitle      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(strTitle),250)
							Thumbnail     = KnifeCMS.Data.Left(KnifeCMS.Data.URLEncode(Thumbnail),250)
							strContentAll = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(strContentAll),200000)
							
							Result = KnifeCMS.DB.AddRecord(DBTableResult,Array("SysID:"&SysID,"CollectionID:"&ID,"DefaultSystem:"&DefaultSystem,"DefaultClassID:"&DefaultClassID,"Title:"& strTitle,"Thumbnail:"& Thumbnail,"From:"& strFrom,"Author:"& strAuthor,"AddTime:"& strAddTime,"Content:"& strContentAll,"CollectTime:"&SysTime))
						End If
						If Result Then CollectArtsSucNum=CollectArtsSucNum+1
					End If
					CollectArtsNum=CollectArtsNum+1
				Next
				Echo "<br>"& Lang_Collection_Cue(53) &"<!--本项采集完成.--></dd></dl>"
				Response.Flush()
				'==== 采集文章结束(循环开始采集下一篇文章) ===
			'==第一级== 该地址不存在则 ====
			Else
				Echo "<br>"& Lang_Collection_Cue(54) &"<!--本采集链接无效.--></dd></dl>"
				Response.Flush()
			End If
		Next
		If Debug Then
			Echo "<div class=CollectEnd>"& Lang_Collection_Cue(55) &"<!--调式结束.--></div>"
		Else
			Echo "<div class=CollectEnd>"& Lang_Collection_Cue(56) &"<!--总采集结束.--></div>"
		End If
		'============ 总采集结束(循环) ==============
	End If
	If Debug Then
		Msg = Lang_ListTableColLine_ID &":"&ID&"; <br>"& Lang_Collection(4) &":"&CollectionName
		Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
	Else
		Msg= Lang_ListTableColLine_ID &":"&ID&"; <br>"& Lang_Collection(4) &":"&CollectionName&"; <br>"& Lang_Collection_Cue(58) &"<!--共采集文章成功个数-->:"&CollectArtsSucNum&"; <br>"& Lang_Collection_Cue(59) &"<!--采集文章失败个数-->:"&CollectArtsNum-CollectArtsSucNum&";"
		Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
	End If
End Function

Function GetRuleStart(Rule,Label)
	GetRuleStart=""
	If Rule="" or IsNull(Rule) or Label="" or IsNull(Label) or Len(Label)>Len(Rule) Then
		Exit Function
	End IF
	Dim Starti
	Starti=InStr(1,Rule,Label,vbBinaryCompare)
	If Starti>1 Then GetRuleStart=Left(Rule,Starti-1)
	GetRuleStart=Trim(GetRuleStart)
End Function

Function GetRuleEnd(Rule,Label)
	GetRuleEnd=""
	If Rule="" or IsNull(Rule) or Label="" or IsNull(Label) or Len(Label)>Len(Rule) Then
		Exit Function
	End IF
	Dim Endi
	Endi=InStrRev(Rule,Label,-1,vbBinaryCompare)
	If Endi>1 Then GetRuleEnd=Right(Rule,(Len(Rule)-Endi-Len(Label))+1)
	GetRuleEnd=Trim(GetRuleEnd)
End Function


ClassObjectClose

%>
