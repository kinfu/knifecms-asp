<!--#include file="config.admin.asp"-->
<%
Dim OnlineService
Set OnlineService = New Class_KnifeCMS_Admin_OnlineService
Set OnlineService = Nothing
Class Class_KnifeCMS_Admin_OnlineService

	Private Site_SystemConfig,Site_UserCookieType,Site_AdminCookieType,Site_OnlineService
	Private KF_Show
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "onlineservice.lang.asp"
		Header()
		If Ctl="onlineservice" Then
			Select Case Action
				Case "view"
					Site_OnlineService = KnifeCMS.SysConfig("OnlineService")
					Call OnlineService()
				Case "edit"
					If SubAction="save" Then
						Call OnlineServiceDoSave()
					End If
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"goback",0
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function OnlineServiceDoSave()
		Site_UserCookieType  = KnifeCMS.Data.CLng(KnifeCMS.SysConfig("UserCookieType"))
		Site_AdminCookieType = KnifeCMS.Data.CLng(KnifeCMS.SysConfig("AdminCookieType"))
		Site_OnlineService   = Replace(KnifeCMS.GetForm("post","OnlineService"),"""","""""")
		Site_OnlineService   = Replace(Site_OnlineService," ","")
		Site_OnlineService   = Replace(Site_OnlineService,Chr(32),"")
		Site_SystemConfig    = "UserCookieType="& Site_UserCookieType &";AdminCookieType="& Site_AdminCookieType &";OnlineService="& Site_OnlineService  &""
		Dim Fn_Text
		Fn_Text = KnifeCMS.Read(SystemPath & "config/config.main.asp")
		Fn_Text = KnifeCMS.Data.RegReplace(Fn_Text,"SystemConfig([\s]*?)=([\s]*?)(""\S*"")","SystemConfig = """& Site_SystemConfig &"""")
		
		If KnifeCMS.FSO.SaveTextFile("config/config.main.asp",Fn_Text)=True Then
			Admin.ShowMessage Lang_OnlineService(11),"?ctl="& Ctl &"&act=view",1
		Else
			Admin.ShowMessage Lang_OnlineService(12),"?ctl="& Ctl &"&act=view",1
		End If
	End Function
	
    Function OnlineService()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
      <div class="wbox">
      <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=edit&subact=save" method="post">
          <input type="hidden" name="OnlineService" value="" />
          <div class="inbox">
          <table border="0" cellpadding="0" cellspacing="0" class="infoTable" width="100%">
          <tbody>
          <tr><td class="titletd"><%=Lang_OnlineService(0)%></td>
              <td class="infotd">
              <div id="OnlineService_Show" style="padding:0px 0px 10px 0px;">
              <input type="radio" value="1" name="OnlineService_Show" <%=Admin.Input_Checked(KF_Show,1)%> /><%=Lang_OnlineService(1)%>
              <input type="radio" value="2" name="OnlineService_Show" <%=Admin.Input_Checked(KF_Show,2)%> /><%=Lang_OnlineService(2)%>
              <label class="Normal" style="padding-left:20px;"><%=Lang_OnlineService(10)%> {kfc:onlineservice}</label>
              </div>
              <div id="onlineServiceGrid"></div>
              <div style="margin-top:5px;"><span class="sysBtn"><a id="onlineServiceAdd" href="javascript:;"><b class="icon icon_add"></b><%=Lang_OnlineService(3)%></a></span></div>
              </td>
          </tr>
          <style type="text/css">
          .onlineservice{ background:#fafafa; border:1px solid #ddd; display:inline-block; padding:8px 12px; position:relative; width:420px;}
		  .onlineservice dt{ position:absolute; margin-top:2px; left:315px;}
		  .onlineservice dd{ margin:2px 0px; padding:2px 0px;}
          </style>
          </tbody>
          </table>
          </div>
          <div class="inbox">
          <table border="0" cellpadding="0" cellspacing="0" class="infoTable" width="100%">
          <tbody>
          <tr><td class="titletd"><%=Lang_OnlineService(9)%></td>
              <td class="infotd">
              <div id="KF_Show" style="padding:0px 0px 10px 0px;">
              <input type="radio" value="1" name="KF_Show" <%=Admin.Input_Checked(KF_Show,1)%> /><%=Lang_OnlineService(1)%>
              <input type="radio" value="2" name="KF_Show" <%=Admin.Input_Checked(KF_Show,2)%> /><%=Lang_OnlineService(2)%>
              <label class="Normal" style="padding-left:20px;"><%=Lang_OnlineService(10)%> {kfc:kf}</label>
              </div>
              <textarea class="remark" name="kf" style="height:120px; width:600px;"></textarea><br />
              </td>
          </tr>
          </tbody>
          </table>
          </div>
          <div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
          <button type="button" class="sysSaveBtn" id="SaveBtn" name="SaveBtn" ><b class="icon icon_save"></b><%=Lang_Btn_Submit%></button>
          <button  type="reset" class="sysSaveBtn" id="ReSetBtn" name="ReSetBtn"><b class="icon icon_reset"></b><%=Lang_Btn_Reset%></button>
          </div>
      </form>
      </div>
      </DIV>
    </DIV>
    <script type="text/javascript">
    $.extend({
		onlineServiceSet:function(opt){
		    var $onlineServiceGrid = $("#onlineServiceGrid");
			var deleteThis = function(){
				$onlineServiceGrid.find("a[name='delete']").unbind("click");
				$onlineServiceGrid.find("a[name='delete']").click(function(){
					$(this).parent().parent().parent().remove();
				});
			}
			var init = function(){
				var json,jsonData = opt.data || '{"onlineservice_show":"2","onlineservice":[],"kf_show":"2","kf":""}';;
				if(jsonData!=""){
					json = $.parseJSON(jsonData);
					var OnlineService_Show = parseInt(json.onlineservice_show) || 1;
					var OnlineService = json.onlineservice;
					var KF_Show = parseInt(json.kf_show) || 1;
					var KF = json.kf;
					var ii,html,name,tel,qq,wangwang,skype,text;
					html = '<dl class="onlineservice">'
						  +'<dt><span class="sysBtn"><a name="delete" href="javascript:;"><b class="icon icon_del"></b><%=Lang_OnlineService(4)%></a></span></dt>'
						  +'<dd><input type="text" class="TxtClass readonly" style="width:60px;" value="<%=Lang_OnlineService(5)%>" /><input type="text" class="TxtClass" name="name" maxlength="100" style="width:220px;" value="{ptl:name}" /></dd>'
						  +'<dd><input type="text" class="TxtClass readonly" style="width:60px;" value="<%=Lang_OnlineService(6)%>" /><input type="text" class="TxtClass" name="tel" maxlength="100" style="width:220px;" value="{ptl:tel}" /></dd>'
						  +'<dd><input type="text" class="TxtClass readonly" style="width:60px;" value="<%=Lang_OnlineService(7)%>" /><input type="text" class="TxtClass" name="qq" maxlength="100" style="width:220px;" value="{ptl:qq}" /></dd>'
						  +'<dd><input type="text" class="TxtClass readonly" style="width:60px;" value="<%=Lang_OnlineService(8)%>" /><input type="text" class="TxtClass" name="wangwang" maxlength="100" style="width:220px;" value="{ptl:wangwang}" /></dd>'
						  +'<dd><input type="text" class="TxtClass readonly" style="width:60px;" value="<%=Lang_OnlineService(13)%>" /><input type="text" class="TxtClass" name="skype" maxlength="100" style="width:220px;" value="{ptl:skype}" /></dd>'
						  +'<dd><input type="text" class="TxtClass readonly" style="width:60px;" value="<%=Lang_OnlineService(14)%>" /><input type="text" class="TxtClass" name="text" maxlength="200" style="width:220px;" value="{ptl:text}" /></dd>'
						  +'</dl>';
					$("#OnlineService_Show").find("input[value='"+OnlineService_Show+"']").attr("checked",true);
					$("#KF_Show").find("input[value='"+KF_Show+"']").attr("checked",true);
					$("textarea[name='kf']").val(unescape(KF));
					for(ii=0;ii<json.onlineservice.length;ii++){
						name     = unescape(json.onlineservice[ii].name || "");
						tel      = unescape(json.onlineservice[ii].tel || "");
						qq       = unescape(json.onlineservice[ii].qq || "");
						wangwang = unescape(json.onlineservice[ii].wangwang || "");
						skype    = unescape(json.onlineservice[ii].skype || "");
						text     = unescape(json.onlineservice[ii].text || "");
						$onlineServiceGrid.append(
						html.replace("{ptl:name}",name).
						     replace("{ptl:tel}",tel).
							 replace("{ptl:qq}",qq).
							 replace("{ptl:wangwang}",wangwang).
							 replace("{ptl:skype}",skype).
							 replace("{ptl:text}",text)
					    );
					}
					if(ii==0){
						$onlineServiceGrid.append(
						html.replace("{ptl:name}","").
						     replace("{ptl:tel}","").
							 replace("{ptl:qq}","").
							 replace("{ptl:wangwang}","").
							 replace("{ptl:skype}","").
							 replace("{ptl:text}","")
					    );
					}
					$("#onlineServiceAdd").click(function(){
						$onlineServiceGrid.append(
						html.replace("{ptl:name}","").
						     replace("{ptl:tel}","").
							 replace("{ptl:qq}","").
							 replace("{ptl:wangwang}","").
							 replace("{ptl:skype}","").
							 replace("{ptl:text}","")
					    );
						deleteThis();
					});
					deleteThis();
				}
			}
			init();
			$("#SaveBtn").click(function(){Save();});
			var Save = function(){
				var jsonData = "";
				var jsons = "";
				var OnlineService_Show = parseInt($("#OnlineService_Show").find("input:checked").val()) || 1;
				var KF_Show = parseInt($("#KF_Show").find("input:checked").val()) || 1;
				var KF = $("textarea[name='kf']").val();
				$onlineServiceGrid.find("dl").each(function(){
					var name     = $(this).find("input[name='name']").val();
					var tel      = $(this).find("input[name='tel']").val();
					var qq       = $(this).find("input[name='qq']").val();
					var wangwang = $(this).find("input[name='wangwang']").val();
					var skype    = $(this).find("input[name='skype']").val();
					var text     = $(this).find("input[name='text']").val();
					var json     = "";
					if(name!="" || tel!="" || qq!="" || wangwang!="" || skype!="" || text!=""){
						json = '{"name":"'+escape(name.replace('"',''))+'","tel":"'+escape(tel.replace('"',''))+'","qq":"'+escape(qq.replace('"',''))+'","wangwang":"'+escape(wangwang.replace('"',''))+'","skype":"'+escape(skype.replace('"',''))+'","text":"'+escape(text.replace('"',''))+'"}';
						if(jsons!=""){
							jsons = jsons +","+json;
						}else{
							jsons = json;
						}
					}
				});
				jsonData = '{"onlineservice_show":"'+OnlineService_Show+'","onlineservice":['+jsons+'],"kf_show":"'+KF_Show+'","kf":"'+escape(KF)+'"}';
				$("input[name='OnlineService']").val(jsonData);
				$("form:first").submit();
			}
		}
	});
	var onlineServiceJson = '<%=Site_OnlineService%>';
	$(document).ready(function(){
		$.onlineServiceSet({data:onlineServiceJson});
	});
    </script>
<%
    End Function
End Class
%>
