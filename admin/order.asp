<!--#include file="config.admin.asp"-->
<%
Dim Order
Set Order = New Class_KnifeCMS_Admin_Order
Set Order = Nothing
Class Class_KnifeCMS_Admin_Order

	Private Pv_ViewOrder
	Private Pv_OrderID,Pv_UserID,Pv_UserName,Pv_Confirm,Pv_Createtime
	Private Pv_IsTax,Pv_IsProtect,Pv_IsCanEdit,Pv_IsNeedPay,Pv_IsCanRefund,Pv_IsCanShip,Pv_IsCanReShip,Pv_IsCanFinish,Pv_IsCanInvalid
	Private Pv_Cost_Item,Pv_Cost_Freight,Pv_Cost_Protect,Pv_Cost_Tax,Pv_Pmt_Amount,Pv_Cost_Payment,Pv_Discount,Pv_Total_Amount,Pv_Final_Amount,Pv_Payed
	Private Pv_Tax_Company,Pv_Weight,Pv_ShippingID,Pv_Shipping,Pv_PaymentID,Pv_Payment,Pv_Currency,Pv_ItemNum,Pv_DeliveryCorpID
	Private Pv_ShippingArea,Pv_Ship_Name,Pv_Ship_Area,Pv_Ship_Address,Pv_Ship_Zip,Pv_Ship_Tel,Pv_Ship_Email,Pv_Ship_Mobile,Pv_Ship_Time,Pv_Mark_Text,Pv_Ship_AreaGrade,Pv_Ship_AreaArray
	Private Pv_Status,Pv_PayStatus,Pv_ShipStatus,Pv_PaymentData,Pv_ShippingData
	Private Pv_KeywordsType
	Private Pv_BillID,Pv_Bank,Pv_Account,Pv_PayAccount,Pv_Money,Pv_PayCost,Pv_CurMoney,Pv_PayType,Pv_PaymentName,Pv_OperaterID,Pv_IP,Pv_TBegin,Pv_TEnd,Pv_Remarks,Pv_Disabled,Pv_TradeNo
	Private Pv_TReady,Pv_TSent,Pv_TReceived
	Private Pv_Logi_ID,Pv_Logi_Name,Pv_Logi_NO '物流公司ID,物流公司名称,物流单号
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "order.lang.asp"
		IsContentExist = False
		Pv_IsCanEdit   = False'是否可以编辑,当"订单状态"为非"活动订单"（即Status>1） 时
		                      '             或"支付状态"为非"未支付"（即PayStatus>0）时
							  '             或"发货状态"为非"未发货"（即ShipStatus>0）时
							  '             或在回收站（即Recycle<>0）时 不可以编辑
		Pv_IsNeedPay     = False  '是否可以支付: 当"已支付的金额"小于"订单总金额"时（即Final_Amount>Payed）可以支付
		                          '              当"支付状态"为"部分退款"或"全额退款"时不可以支付
		Pv_IsCanRefund   = False  '是否可以退款: 当已经支付过并且支付金额大于零（即Payed>0）时
		                          '              
		Pv_IsCanShip     = False
		Pv_IsCanReShip   = False
		Pv_IsCanFinish   = False '是否可以完成: 当"订单状态"为"活动订单"(即Status=1)时可以编辑为完成状态
		Pv_IsCanInvalid  = False '是否可以作废: 当"未支付"并且为"未发货"时可以编辑为作废状态
		Header()
		If Ctl = "order" Or Ctl = "order_recycle" Then
			Pv_OrderID = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","orderid"),"[^0-9]",""),16)
			Select Case Action
				Case "view"
					If SubAction = "detail" Then
						Call GetOrderDetailData()
						Pv_Shipping = KnifeCMS.DB.GetFieldByID(DBTable_Delivery,"DeliveryName",Pv_ShippingID)
						Pv_Payment  = KnifeCMS.DB.GetFieldByID(DBTable_Payments,"PaymentName",Pv_PaymentID)
						If IsContentExist Then OrderDetail : Else Admin.ShowMessage Lang_Order_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
					Else
						Call OrderGetSearchCondition()
						Pv_ViewOrder = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","order"))
						Pv_ViewOrder = KnifeCMS.IIF(Pv_ViewOrder=0,1,Pv_ViewOrder)
						Call OrderView()
					End If
				Case "add","edit"
					If SubAction="save" Then
						Pv_IsCanEdit = CheckIsCanEdit
						If Pv_IsCanEdit Then OrderDoSave : Else Admin.ShowMessage Lang_Order_CannotEdit,Array("javascript:WinClose();",Lang_WinClose),0
					Else
						If Action = "add" Then
							Call OrderAdd()
						ElseIf Action = "edit" Then
							If SubAction = "revert" Then
								Call OrderOtherEdit()
							ElseIf SubAction = "ordergoodsdelete" Then
								Call OrderGoodsDelete()
							Else
								Call GetOrderDetailData()
								If IsContentExist Then
									Pv_IsCanEdit = CheckIsCanEdit
									If Pv_IsCanEdit Then OrderEdit : Else Admin.ShowMessage Lang_Order_CannotEdit,Array("javascript:WinClose();",Lang_WinClose),0
								Else
									Admin.ShowMessage Lang_Order_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
								End If
							End If
						End IF
					End IF
				Case "del"  : Call OrderDel()
				Case "orderpay"
					If SubAction="save" Then
						Call GetOrderDetailData()
						If IsContentExist Then
							If Pv_IsNeedPay Then OrderPaySave : Else Admin.ShowMessage Lang_Order_Cue(14),Array("javascript:WinClose();",Lang_WinClose),0
						Else
							Admin.ShowMessage Lang_Order_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
						End If
					Else
						Call GetOrderDetailData()
						If IsContentExist Then
							If Pv_IsNeedPay Then OrderPay : Else Admin.ShowMessage Lang_Order_Cue(14),Array("javascript:WinClose();",Lang_WinClose),0
						Else
							Admin.ShowMessage Lang_Order_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
						End If
					End If
				Case "orderrefund"
					If SubAction="save" Then
						Call GetOrderDetailData()
						If IsContentExist Then
							If Pv_IsCanRefund Then OrderRefundSave : Else Admin.ShowMessage Lang_Order_Cue(15),Array("javascript:WinClose();",Lang_WinClose),0
						Else
							Admin.ShowMessage Lang_Order_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
						End If
					Else
						Call GetOrderDetailData()
						If IsContentExist Then
							If Pv_IsCanRefund Then OrderRefund : Else Admin.ShowMessage Lang_Order_Cue(15),Array("javascript:WinClose();",Lang_WinClose),0
						Else
							Admin.ShowMessage Lang_Order_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
						End If
					End If
				Case "ordership"
					If SubAction="save" Then
						Call GetOrderDetailData()
						If IsContentExist Then
							If Pv_IsCanShip Then OrderShipSave : Else Admin.ShowMessage Lang_Order_Cue(16),Array("javascript:WinClose();",Lang_WinClose),0
						Else
							Admin.ShowMessage Lang_Order_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
						End If
					Else
						Call GetOrderDetailData()
						If IsContentExist Then
							If Pv_IsCanShip Then OrderShip : Else Admin.ShowMessage Lang_Order_Cue(16),Array("javascript:WinClose();",Lang_WinClose),0
						Else
							Admin.ShowMessage Lang_Order_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
						End If
					End If
				Case "orderreship"
					If SubAction="save" Then
						Call GetOrderDetailData()
						If IsContentExist Then
							If Pv_IsCanReShip Then OrderReShipSave : Else Admin.ShowMessage Lang_Order_Cue(17),Array("javascript:WinClose();",Lang_WinClose),0
						Else
							Admin.ShowMessage Lang_Order_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
						End If
					Else
						Call GetOrderDetailData()
						If IsContentExist Then
							If Pv_IsCanReShip Then OrderReShip : Else Admin.ShowMessage Lang_Order_Cue(17),Array("javascript:WinClose();",Lang_WinClose),0
						Else
							Admin.ShowMessage Lang_Order_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
						End If
					End If
				Case "orderfinish"
					Call GetOrderDetailData()
					If IsContentExist Then
						If Pv_IsCanFinish Then OrderFinishSave : Else Admin.ShowMessage Lang_Order_Cue(18),Array("javascript:WinClose();",Lang_WinClose),0
					Else
						Admin.ShowMessage Lang_Order_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
					End If
				Case "orderinvalid"
					Call GetOrderDetailData()
					If IsContentExist Then
						If Pv_IsCanFinish Then OrderInvalidSave : Else Admin.ShowMessage Lang_Order_Cue(19),Array("javascript:WinClose();",Lang_WinClose),0
					Else
						Admin.ShowMessage Lang_Order_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
					End If
			End Select
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	'检测订单是否可以编辑
	Private Function CheckIsCanEdit()
		Dim Fn_Rs,Fn_IsCanEdit
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Order,Array("ID:"& ID),"")
		If Not(Fn_Rs.Eof) Then
			If KnifeCMS.Data.CLng(Fn_Rs("Status"))>1 Or KnifeCMS.Data.CLng(Fn_Rs("PayStatus"))>0 Or KnifeCMS.Data.CLng(Fn_Rs("ShipStatus"))>0 Or KnifeCMS.Data.CLng(Fn_Rs("Recycle"))<>0 Then
				Fn_IsCanEdit = False
			Else
				Fn_IsCanEdit = True
			End If
		Else
			Fn_IsCanEdit = False
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		CheckIsCanEdit = Fn_IsCanEdit
	End Function
	
	Public Function GetBankCountAsOption(ByVal BV_Condition, ByVal BV_OtherCondition)
		Dim Fn_Rs,Fn_Temp,Fn_ReturnString
		Fn_ReturnString = "<option value=""0"">"& Lang_UseExistBankAccount &"</option>"
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_BankAccount &":Bank,AccountOwner,AccountNO",BV_Condition,BV_OtherCondition)
		Do While Not Fn_Rs.Eof
			Fn_Temp = Fn_Rs(0)
			If Not(KnifeCMS.Data.IsNul(Fn_Rs(1))) Then Fn_Temp = Fn_Temp &"["& Fn_Rs(1) &"]"
			Fn_Temp = Fn_Temp & Fn_Rs(2)
			Fn_ReturnString= Fn_ReturnString & "<option bank="""& Fn_Rs(0) &""" ower="""& Fn_Rs(1) &""" no="""& Fn_Rs(2) &""" value="""& Fn_Temp &""" >"& Fn_Temp &"</option>"
			Fn_Rs.Movenext
		Loop
	    KnifeCMS.DB.CloseRs Fn_Rs
		GetBankCountAsOption = Fn_ReturnString
	End Function
	
	Private Sub GetOrderDetailData()
		Dim Fn_Temp
		If KnifeCMS.Data.IsNul(Pv_OrderID) Then Fn_Temp=Array("ID:"& ID) : Else Fn_Temp=Array("OrderID:"& Pv_OrderID)
		Set Rs = KnifeCMS.DB.GetRecord(DBTable_Order,Fn_Temp,"")
		If Not(Rs.Eof) Then
			IsContentExist = True
			SysID           = Rs("SysID")
			Pv_OrderID      = Rs("OrderID")
			Pv_UserID       = KnifeCMS.Data.CLng(Rs("UserID"))
			Pv_Createtime   = Rs("Createtime")
			Pv_Status       = KnifeCMS.Data.CLng(Rs("Status"))
			Pv_PayStatus    = KnifeCMS.Data.CLng(Rs("PayStatus"))
			Pv_ShipStatus   = KnifeCMS.Data.CLng(Rs("ShipStatus"))
			Pv_IsTax        = KnifeCMS.Data.CLng(Rs("IsTax"))
			Pv_IsProtect    = KnifeCMS.Data.CLng(Rs("IsProtect"))
			Pv_Cost_Item    = KnifeCMS.Data.FormatCurrency(Rs("Cost_Item"))    '商品总金额
			Pv_Cost_Freight = KnifeCMS.Data.FormatCurrency(Rs("Cost_Freight")) '配送费用
			Pv_Cost_Protect = KnifeCMS.Data.FormatCurrency(Rs("Cost_Protect")) '保价费
			Pv_Cost_Tax     = KnifeCMS.Data.FormatCurrency(Rs("Cost_Tax"))     '税金
			Pv_Pmt_Amount   = KnifeCMS.Data.FormatCurrency(Rs("Pmt_Amount"))   '促销优惠金额
			Pv_Cost_Payment = KnifeCMS.Data.FormatCurrency(Rs("Cost_Payment")) '支付手续费
			Pv_Discount     = KnifeCMS.Data.FormatDouble(Rs("Discount"))       '订单折扣或涨价
			Pv_Total_Amount = KnifeCMS.Data.FormatDouble(Rs("Total_Amount"))   '订单总金额(订单本来的总金额=商品总金额+配送费用+物流保价费+税金-促销优惠金额+订单折扣或涨价)
			Pv_Final_Amount = KnifeCMS.Data.FormatDouble(Rs("Final_Amount"))   '订单总金额(订单最终的总金额=商品总金额+配送费用+物流保价费+税金-促销优惠金额+订单折扣或涨价+支付手续费)
			Pv_Payed        = KnifeCMS.Data.FormatDouble(Rs("Payed"))
			Pv_Tax_Company  = KnifeCMS.Data.HTMLDecode(Rs("Tax_Company"))      '发票抬头
			Pv_Weight       = Rs("Weight")       '总重量
			
			Pv_ShippingID   = Rs("ShippingID")   '配送方式ID
			Pv_PaymentID    = Rs("PaymentID")    '配送方式ID
			Pv_Currency     = Rs("Currency")     '货币
			Pv_ShippingArea = Rs("ShippingArea") '配送地区
			Pv_Ship_Name    = Rs("Ship_Name")    '收货人姓名
			Pv_Ship_Email   = Rs("Ship_Email")   '收货人Email
			Pv_Ship_Area    = Rs("Ship_Area")    '收货地区
			Pv_Ship_Address = KnifeCMS.Data.HTMLDecode(Rs("Ship_Address")) '详细地址
			Pv_Ship_Zip     = Rs("Ship_Zip")     '邮政编码
			Pv_Ship_Tel     = Rs("Ship_Tel")     '联系电话
			Pv_Ship_Mobile  = Rs("Ship_Mobile")  '联系手机
			Pv_Ship_Time    = Rs("Ship_Time")    '送货日期-时间段
			Pv_Mark_Text    = Rs("Mark_Text")    '订单附言
		Else
			IsContentExist = False
		End IF
		KnifeCMS.DB.CloseRs Rs
		'If 1=Pv_Status And (Pv_Final_Amount - Pv_Payed)>0 And (Pv_PayStatus<>4 And Pv_PayStatus<>5) Then Pv_IsNeedPay=True : Else Pv_IsNeedPay=False
		'部分退款/全额退款后还可以继续支付(已完成或以作废订单:不可以继续支付)
		If 1=Pv_Status And (Pv_Final_Amount - Pv_Payed)>0 Then Pv_IsNeedPay=True : Else Pv_IsNeedPay=False
		If 1=Pv_Status And Pv_Payed>0  Then Pv_IsCanRefund=True : Else Pv_IsCanRefund=False
		If 1=Pv_Status And Pv_ShipStatus<>1 Then Pv_IsCanShip=True
		If 1=Pv_Status And (1=Pv_ShipStatus Or 2=Pv_ShipStatus Or 3=Pv_ShipStatus) Then Pv_IsCanReShip=True
		If 1=Pv_Status Then Pv_IsCanFinish=True
		If 1=Pv_Status And 0=Pv_PayStatus And 0=Pv_ShipStatus Then Pv_IsCanInvalid=True
		If Pv_UserID>0 Then Pv_UserName = KnifeCMS.DB.GetFieldByID(DBTable_Members,"Username",Pv_UserID)
		
	End Sub
	
	Private Function OrderGetSearchCondition()
		Pv_Status     = KnifeCMS.GetForm("both","status[]")
		Pv_PaymentID  = KnifeCMS.GetForm("both","paymentid[]")
		Pv_PayStatus  = KnifeCMS.GetForm("both","paystatus[]")
		Pv_ShippingID = KnifeCMS.GetForm("both","shippingid[]")
		Pv_ShipStatus = KnifeCMS.GetForm("both","shipstatus[]")
		Pv_KeywordsType = KnifeCMS.GetForm("both","keywordstype")
	End Function
	Private Function OrderSearchSql()
		Dim Fn_TempSql
		Pv_Status     = ParseSearchData(Pv_Status)
		Pv_PaymentID  = ParseSearchData(Pv_PaymentID)
		Pv_PayStatus  = ParseSearchData(Pv_PayStatus)
		Pv_ShippingID = ParseSearchData(Pv_ShippingID)
		Pv_ShipStatus = ParseSearchData(Pv_ShipStatus)
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(KnifeCMS.Data.IsNul(Pv_Status) Or Pv_Status="any",""," AND a.Status IN ("& Pv_Status &")" )
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(KnifeCMS.Data.IsNul(Pv_PaymentID) Or Pv_PaymentID="any",""," AND a.PaymentID IN ("& Pv_PaymentID &")" )
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(KnifeCMS.Data.IsNul(Pv_PayStatus) Or Pv_PayStatus="any",""," AND a.PayStatus IN ("& Pv_PayStatus &")" )
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(KnifeCMS.Data.IsNul(Pv_ShippingID) Or Pv_ShippingID="any",""," AND a.ShippingID IN ("& Pv_ShippingID &")" )
		Fn_TempSql = Fn_TempSql & KnifeCMS.IIF(KnifeCMS.Data.IsNul(Pv_ShipStatus) Or Pv_ShipStatus="any",""," AND a.ShipStatus IN ("& Pv_ShipStatus &")" )
		
		Select Case Pv_KeywordsType
			Case "orderid"
				Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Admin.Search.KeyWords,"[^0-9]",""),16)
				Fn_TempSql = Fn_TempSql & " AND a.OrderID LIKE '%"& Admin.Search.KeyWords &"%'"
			Case "goodsbn"
				Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Admin.Search.KeyWords,"[^a-zA-Z0-9]",""),20)
				Fn_TempSql = Fn_TempSql & " AND a.OrderID IN (SELECT OrderID FROM ["& DBTable_OrderGoods &"] WHERE GoodsBN='"& Admin.Search.KeyWords &"')"
			Case "goodsname"
				'Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Admin.Search.KeyWords,"[^a-zA-Z0-9]",""),20)
				Fn_TempSql = Fn_TempSql & " AND a.OrderID IN (SELECT OrderID FROM ["& DBTable_OrderGoods &"] WHERE GoodsName LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%' OR PacksName LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%')"
			Case "memberid"
				Fn_TempSql = Fn_TempSql & " AND a.UserID="& KnifeCMS.Data.CLng(Admin.Search.KeyWords) &""
			Case "membername"
				Fn_TempSql = Fn_TempSql & " AND a.UserID IN (SELECT ID FROM ["& DBTable_Members &"] WHERE Username LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%')"
			Case "ship_name"
				Fn_TempSql = Fn_TempSql & " AND a.Ship_Name LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%'"
			Case "ship_address"
				Fn_TempSql = Fn_TempSql & " AND a.Ship_Address LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%'"
			Case "ship_tel"
				Fn_TempSql = Fn_TempSql & " AND a.Ship_Tel LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%'"
			Case "mark_text"
				Fn_TempSql = Fn_TempSql & " AND a.Mark_Text LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%'"
		End Select
		OrderSearchSql = Fn_TempSql
	End Function
	Private Function ParseSearchData(ByVal BV_Data)
		Dim Fn_ii,Fn_ReturnString,Fn_TempArray,Fn_Temp
		Fn_TempArray    = Split(BV_Data,",")
		Fn_ReturnString = ""
		For Fn_ii=0 To Ubound(Fn_TempArray)
			If Fn_TempArray(Fn_ii)="any" Then
				Fn_ReturnString="any"
				Exit For
			Else
				Fn_Temp = KnifeCMS.Data.Clng(Fn_TempArray(Fn_ii))
				If Fn_Temp>=0 Then
					If Fn_ReturnString="" Then Fn_ReturnString = Fn_Temp : Else Fn_ReturnString = Fn_ReturnString &","& Fn_Temp
				End If
			End If
		Next
		ParseSearchData = Fn_ReturnString
	End Function
	
	Private Function OrderList()
		Dim Fn_TempSql,Fn_ColumnNum
			Fn_TempSql = "SELECT a.ID,a.OrderID,a.Total_Amount,a.Cost_Payment,a.Final_Amount,a.Status,a.PayStatus,a.ShipStatus,a.Payment,a.Shipping,d.Username,a.Ship_Name,a.Createtime FROM ["& DBTable_Order &"] a LEFT JOIN ["& DBTable_Members &"] d ON (d.ID=a.UserID)"
			Fn_ColumnNum = 13
			Operation = "<span class=""sysBtn""><a href=""javascript:;"" onClick=""ShowModal('?ctl="&Ctl&"&act=view&subact=detail&id={$ID}',960,620); ShowModalReload();""><b class=""icon icon_view""></b>"& Lang_DoView &"</a></span>"
			Operation = Operation & "<span class=""sysBtn""><a href=""javascript:;"" onClick=""ShowModal('?ctl="&Ctl&"&act=edit&id={$ID}',960,620); ShowModalReload();""><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		If SubCtl = "recycle" Then
			Fn_TempSql = Fn_TempSql & " WHERE a.Recycle=1 ORDER BY a.ID DESC"
			Operation = ""
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,"")
		Else
			Fn_TempSql = Fn_TempSql & " WHERE a.Recycle=0 "
			If SubAction = "search" Then

				Select Case DB_Type
				Case 0
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.Createtime>=#"& Admin.Search.StartDate &"# ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.Createtime<=#"& Admin.Search.EndDate &"# ","")
				Case 1
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.Createtime>='"& Admin.Search.StartDate &"' ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.Createtime<='"& Admin.Search.EndDate &"' ","")
				End Select
				
				tempCondition = tempCondition & OrderSearchSql
				PageUrlPara = PageUrlPara & "&startdate="& Server.URLEncode(Admin.Search.StartDate) &"&enddate="& Server.URLEncode(Admin.Search.EndDate) &"&keywordstype="& Pv_KeywordsType &"&keywords="& Server.URLEncode(Admin.Search.KeyWords)&"&status[]="& Pv_Status &"&paymentid[]="& Pv_PaymentID &"&paystatus[]="& Pv_PayStatus &"&shippingid[]="& Pv_ShippingID &"&shipstatus[]="& Pv_ShipStatus &""
				Fn_TempSql = Fn_TempSql & tempCondition
			End If
			Fn_TempSql = Fn_TempSql &" ORDER BY a.ID DESC"
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
		End If
	End Function
	
	'编辑/查看订单时的商品列表
	Private Function OrderGoodsList(ByVal BV_IsDo,ByVal BV_OrderID)
		Dim Fn_Rs,Fn_ii,Fn_ID,Fn_OrderID,Fn_TempString,Fn_JSON,Fn_PacksHTML,Fn_IsPacks
		Dim Fn_Config
		Dim Fn_PacksID,Fn_PacksName
		Dim GoodsID,Fn_GoodsBN,Fn_GoodsName,Fn_GoodsAttrValue
		Dim Fn_Name,Fn_Price,Fn_Nums,Fn_Amount
		If KnifeCMS.Data.IsNul(BV_OrderID) Then Fn_OrderID = KnifeCMS.DB.GetFieldByID(DBTable_Order,"OrderID",ID) : Else Fn_OrderID = BV_OrderID
		'Fn_OrderID = KnifeCMS.DB.GetFieldByID(DBTable_Order,"OrderID",ID)
		If KnifeCMS.Data.IsNul(Fn_OrderID) Then
			Fn_TempString = "<tr class=""data""><td colspan=""6"">"& Lang_Order_Cue(11) &"</td></tr>"
		Else
			Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_OrderGoods,Array("OrderID:"& Fn_OrderID),"ORDER BY ID ASC")
			Do While Not(Fn_Rs.Eof)
				Fn_ID        = Fn_Rs("ID")
				Fn_Config    = Fn_Rs("Config")
				Fn_PacksID   = KnifeCMS.Data.CLng(Fn_Rs("PacksID"))
				Fn_PacksName = KnifeCMS.Data.HTMLDecode(Fn_Rs("PacksName"))
				GoodsID      = Fn_Rs("GoodsID")
				Fn_GoodsBN   = Fn_Rs("GoodsBN")
				Fn_GoodsName      = KnifeCMS.Data.HTMLDecode(Fn_Rs("GoodsName"))
				Fn_GoodsAttrValue = KnifeCMS.Data.HTMLDecode(Fn_Rs("GoodsAttrValue"))
				'Fn_Name    = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_PacksName),Fn_GoodsName,Fn_PacksName)
				Fn_Price   = KnifeCMS.Data.FormatCurrency(Fn_Rs("Price"))
				Fn_Nums    = Fn_Rs("Nums")
				Fn_Amount  = KnifeCMS.Data.FormatCurrency(Fn_Rs("Amount"))
				
				If Not(KnifeCMS.Data.IsNul(Fn_GoodsAttrValue)) Then Fn_GoodsName = Fn_GoodsName &"<span class=yellow>("& Fn_GoodsAttrValue &")</span>"
				If Fn_PacksID>0 Then Fn_IsPacks = True : Else Fn_IsPacks = False
				'Echo Fn_Config &"<br><br><br>"
				If Fn_IsPacks Then
					Set Fn_JSON  = KnifeCMS.JSON.Parse(Fn_Config)
					Fn_GoodsAttrValue = KnifeCMS.Data.UnEscape(Fn_JSON.goodsattrvalue)
					If Not(KnifeCMS.Data.IsNul(Fn_GoodsAttrValue)) Then Fn_GoodsName = KnifeCMS.Data.UnEscape(Fn_JSON.goodsname) &"<span class=yellow>("& Fn_GoodsAttrValue &")</span>"
					Fn_PacksHTML = "<span class=""red"">["& Lang_ThePack &"]"& Fn_PacksName &"</span>"
					Fn_PacksHTML = Fn_PacksHTML & "<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""listTable"">"
					Fn_PacksHTML = Fn_PacksHTML & "<tr class=""data""><td>"& Fn_JSON.goodsbn &"</td><td>"& Fn_GoodsName &"</td></tr>"
					
					For Fn_ii=0 To Fn_JSON.selectgoods.length-1
						Fn_GoodsName = KnifeCMS.Data.UnEscape(Fn_JSON.selectgoods.get(Fn_ii).goodsname)
						Fn_PacksHTML = Fn_PacksHTML & "<tr class=""data""><td width=""10%""><div style=""width:150px;"">"& Fn_JSON.selectgoods.get(Fn_ii).goodsbn &"</div></td><td width=""90%""><div style=""width:380px;"">"& Fn_GoodsName &"</div></td></tr>"
					Next
					Fn_PacksHTML  = Fn_PacksHTML & "</table>"
					Fn_TempString = Fn_TempString & "<tr class=""data""><td colspan=""2"">"& Fn_PacksHTML &"</td><td>"& MoneySb & Fn_Price &"</td><td>"& Fn_Nums &"</td><td>"& MoneySb & Fn_Amount &"</td>"
					If BV_IsDo Then Fn_TempString = Fn_TempString & "<td><span class=""sysBtn""><a href=""?ctl=order&act=edit&subact=ordergoodsdelete&id="&ID&"&orderid="& Fn_OrderID &"&itemid="& Fn_ID &""">"& Lang_DoDel &"</a></span></td>"
					Fn_TempString = Fn_TempString & "</tr>"
				Else
				    Fn_PacksHTML  = ""
					Fn_TempString = Fn_TempString &"<tr class=""data""><td>"& Fn_GoodsBN &"</td><td>"& Fn_GoodsName &"</td><td>"& MoneySb & Fn_Price &"</td><td>"& Fn_Nums &"</td><td>"& MoneySb & Fn_Amount &"</td>"
					If BV_IsDo Then Fn_TempString = Fn_TempString & "<td><span class=""sysBtn m3""><a href=""?ctl=order&act=edit&subact=ordergoodsdelete&id="&ID&"&orderid="& Fn_OrderID &"&itemid="& Fn_ID &""">"& Lang_DoDel &"</a></span></td>"
					Fn_TempString = Fn_TempString & "</tr>"
				End If
				Fn_Rs.Movenext()
			Loop
			KnifeCMS.DB.CloseRs Fn_Rs'关闭并释放指定数据集
		End If
		Echo Fn_TempString
	End Function
	
	Private Function OrderShipBillList(ByVal BV_Type,ByVal BV_OrderID)
		Dim Fn_Rs,Fn_ReturnString,Fn_Type
		If BV_Type="shipbill" Then
			Fn_Type = 1
		Else
			Fn_Type = 0
		End If
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_ShipBill &":TBegin,BillID,Logi_NO,DeliveryName,Ship_Name",Array("Type:"& Fn_Type,"OrderID:"& BV_OrderID),"ORDER BY ID ASC")
		Do While Not(Fn_Rs.Eof)
			Fn_ReturnString = Fn_ReturnString & "<tr class=data><td>"& Fn_Rs(0) &"</td><td>"& Fn_Rs(1) &"</td><td>"& Fn_Rs(2) &"</td><td>"& Fn_Rs(3) &"</td><td>"& Fn_Rs(4) &"</td></tr>"
			Fn_Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		OrderShipBillList = Fn_ReturnString
	End Function
	
	Private Function OrderPaymentBillList(ByVal BV_OrderID)
		Dim Fn_Rs,Fn_ReturnString
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_PaymentBill &":TEnd,Money,PaymentName,Status",Array("OrderID:"& BV_OrderID),"ORDER BY ID ASC")
		Do While Not(Fn_Rs.Eof)
			Fn_ReturnString = Fn_ReturnString & "<tr class=data><td>"& Fn_Rs(0) &"</td><td>"& Fn_Rs(1) &"</td><td>"& Fn_Rs(2) &"</td><td>"& KnifeCMS.IIF(Fn_Rs(3)="success",Lang_Success,Lang_Fail) &"</td></tr>"
			Fn_Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		OrderPaymentBillList = Fn_ReturnString
	End Function
	
	Private Function OrderRefundBillList(ByVal BV_OrderID)
		Dim Fn_Rs,Fn_ReturnString
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_RefundBill &":TSent,Money,PaymentName,Status",Array("OrderID:"& BV_OrderID),"ORDER BY ID ASC")
		Do While Not(Fn_Rs.Eof)
			Fn_ReturnString = Fn_ReturnString & "<tr class=data><td>"& Fn_Rs(0) &"</td><td>"& Fn_Rs(1) &"</td><td>"& Fn_Rs(2) &"</td><td>"& KnifeCMS.IIF(Fn_Rs(3)="success",Lang_Success,Lang_Fail) &"</td></tr>"
			Fn_Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		OrderRefundBillList = Fn_ReturnString
	End Function
	
	Private Function OrderLogList(ByVal BV_OrderID)
		Dim Fn_Rs,Fn_ii,Fn_ReturnString
		Fn_ii = 0
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_OrderLog &":AddTime,OperaterName,BeHavior,Result,LogText",Array("OrderID:"& BV_OrderID),"ORDER BY ID ASC")
		Do While Not(Fn_Rs.Eof)
			Fn_ii = Fn_ii + 1
			Fn_ReturnString = Fn_ReturnString & "<tr class=data><td>"& Fn_ii &"</td><td>"& Fn_Rs(0) &"</td><td>"& Fn_Rs(1) &"</td><td>"& Fn_Rs(2) &"</td><td>"& Fn_Rs(3) &"</td><td>"& Fn_Rs(4) &"</td></tr>"
			Fn_Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		OrderLogList = Fn_ReturnString
	End Function
	
	'订单编辑保存
	Private Function OrderDoSave()
		Dim Fn_ReturnString,Fn_Temp,Fn_TempArray
		Dim Fn_Error(10)
		    Fn_Error(0) = "{""errorinfo"":""order_exist""}"
			Fn_Error(1) = "{""errorinfo"":""region_is_error""}"
			Fn_Error(2) = "{""errorinfo"":""address_is_null""}"
			Fn_Error(3) = "{""errorinfo"":""name_is_null""}"
			Fn_Error(4) = "{""errorinfo"":""tel_is_error""}"
			Fn_Error(5) = "{""errorinfo"":""shippingid_not_exist""}"
			Fn_Error(6) = "{""errorinfo"":""shipping_is_error""}"
			Fn_Error(7) = ""
			
		'收货人信息
		for each Fn_Temp in Request.Form("ship_area")
			Pv_Ship_Area = trim(Fn_Temp)
		next
		Pv_Ship_Area     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Pv_Ship_Area,"[^0-9,]",""),50)
		Pv_Ship_Area     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Pv_Ship_Area,"([,]+?)",","),50)
		Pv_Ship_Address  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ship_address")),250)
		Pv_Ship_Zip      = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ship_zip"),"[^0-9\-]",""),20)
		Pv_Ship_Name     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ship_name")),50)
		Pv_Ship_Tel      = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ship_tel"),"[^0-9\-,]",""),50)
		Pv_Ship_Mobile   = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ship_mobile"),"[^0-9,]",""),50)
		Pv_Ship_Email    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ship_email")),60)
		If Not(KnifeCMS.Data.IsEmail(Pv_Ship_Email)) Then Pv_Ship_Email=""
		Pv_Mark_Text  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","mark_text")),250)
		
		If Len(Pv_Ship_Area)<3 Then 
			ErrMsg = ErrMsg & Fn_Error(1)
		Else
			Fn_TempArray      = Split(Pv_Ship_Area,",")
			Pv_Ship_AreaGrade = Ubound(Fn_TempArray)-1
			If Pv_Ship_AreaGrade<1 Then ErrMsg = ErrMsg & Fn_Error(1)
		End If
		Pv_ShippingArea = KnifeCMS.Data.Left(OS.GetRegionByPath(Pv_Ship_Area),250) '配送地区
		If KnifeCMS.Data.IsNul(Pv_Ship_Address) Then ErrMsg = ErrMsg & Fn_Error(2)
		If KnifeCMS.Data.IsNul(Pv_Ship_Name) Then ErrMsg = ErrMsg & Fn_Error(3)
		If Len(Pv_Ship_Tel)<5 And Len(Pv_Ship_Mobile)<5 Then ErrMsg = ErrMsg & Fn_Error(4)

		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,Array("order.asp?ctl=order&act=edit&id="&ID&"",Lang_Goback),0 : Exit Function

		'================
		Pv_OrderID      = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","orderid"),"[^0-9]",""),16)
		
		Fn_TempArray    = GetOrderItemNumAndCost(Pv_OrderID)
		Pv_ItemNum      = KnifeCMS.Data.CLng(Fn_TempArray(0))
		Pv_ShippingID   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","shippingid"))  '配送方式ID
		Pv_PaymentID    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","paymentid"))  '支付方式ID
		Pv_Tax_Company  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","tax_company")),100)      '发票抬头
		Pv_Ship_Time    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(Trim(KnifeCMS.GetForm("post","ship_time"))),100)  '送货日期-时间段
		Pv_Mark_Text    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","mark_text")),250)'订单附言
		
		Pv_Weight       = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","weight"))        '商品总重量
		Pv_Cost_Item    = KnifeCMS.Data.FormatDouble(Fn_TempArray(1))
		Pv_Cost_Freight = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","cost_freight"))  '配送费用
		Pv_Cost_Protect = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","cost_protect"))  '物流保价费
		Pv_Cost_Tax     = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","cost_tax"))      '税金
		Pv_Pmt_Amount   = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","pmt_amount"))    '促销优惠金额
		Pv_Cost_Payment = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","cost_payment"))  '支付手续费
		Pv_Discount     = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","discount"))      '订单折扣或涨价
		Pv_Total_Amount = Pv_Cost_Item + Pv_Cost_Freight + Pv_Cost_Protect + Pv_Cost_Tax - Pv_Pmt_Amount + Pv_Discount
		Pv_Final_Amount = Pv_Cost_Item + Pv_Cost_Freight + Pv_Cost_Protect + Pv_Cost_Tax - Pv_Pmt_Amount + Pv_Discount + Pv_Cost_Payment
		'==取得配送方式名称
		Pv_Shipping = KnifeCMS.DB.GetFieldByID(DBTable_Delivery,"DeliveryName",Pv_ShippingID)
		Pv_Payment  = KnifeCMS.DB.GetFieldByID(DBTable_Payments,"PaymentName",Pv_PaymentID)
		
		Result = KnifeCMS.DB.UpdateRecord(DBTable_Order,Array("Acttime:"& SysTime,"ItemNum:"& Pv_ItemNum,"Ship_Area:"& Pv_Ship_Area,"Ship_Address:"& Pv_Ship_Address,"Ship_Zip:"& Pv_Ship_Zip,"Ship_Name:"& Pv_Ship_Name,"Ship_Tel:"& Pv_Ship_Tel,"Ship_Mobile:"& Pv_Ship_Mobile,"Ship_Email:"& Pv_Ship_Email,"Ship_Time:"& Pv_Ship_Time,"Mark_Text:"& Pv_Mark_Text,"ShippingID:"& Pv_ShippingID,"Shipping:"& Pv_Shipping,"ShippingArea:"& Pv_ShippingArea,"PaymentID:"& Pv_PaymentID,"Payment:"& Pv_Payment,"Tax_Company:"& Pv_Tax_Company,"Weight:"& Pv_Weight,"Cost_Item:"& Pv_Cost_Item,"Cost_Freight:"& Pv_Cost_Freight,"Cost_Protect:"& Pv_Cost_Protect,"Cost_Tax:"& Pv_Cost_Tax,"Pmt_Amount:"& Pv_Pmt_Amount,"Cost_Payment:"& Pv_Cost_Payment,"Discount:"& Pv_Discount,"Total_Amount:"& Pv_Total_Amount,"Final_Amount:"& Pv_Final_Amount),Array("ID:"& ID,"SysID:"& SysID,"OrderID:"& Pv_OrderID))
		Msg = Lang_Order_Cue(2) &"<br>[OrderID:"& Pv_OrderID &"]<br>[ID:"& ID &"]"
		If Result Then Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
	End Function
	
	'以数组方式返回订单商品个数、商品总金额、所有商品总重量
	Private Function GetOrderItemNumAndCost(ByVal BV_OrderID)
		Dim Fn_Rs,Fn_ReturnString(3)
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_OrderGoods &":sum(ItemNum*Nums),sum(Amount),sum(Weight*Nums)",Array("OrderID:"& BV_OrderID),"")
		If Not(Fn_Rs.Eof) Then
			Fn_ReturnString(0) = Fn_Rs(0)
			Fn_ReturnString(1) = Fn_Rs(1)
			Fn_ReturnString(2) = Fn_Rs(2)
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		GetOrderItemNumAndCost = Fn_ReturnString
	End Function
	
	Private Function OrderOtherEdit()
		Call OrderSubDo()
	End Function
	
	Private Function OrderDel()
		Call OrderSubDo()
	End Function
	
	'删除/彻底删除/还原
	Private Sub OrderSubDo()
		Dim Fn_ArrID,Fn_i,Fn_OrderID
		ID       = KnifeCMS.GetForm("post","InputName")
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			Result     = 0
			Fn_OrderID = ""
			If ID > 0 Then
				Set Rs = KnifeCMS.DB.GetRecord(DBTable_Order&":OrderID",Array("ID:"&ID),"")
				If Not(Rs.Eof) Then Fn_OrderID=Rs(0)
				KnifeCMS.DB.CloseRs Rs
				If Not(KnifeCMS.Data.IsNul(Fn_OrderID)) Then
					If Action = "del" Then
						If SubAction = "completely" Then
							Result = KnifeCMS.DB.DeleteRecord(DBTable_Order,Array("ID:"&ID))
							If 1=Result Then Result = KnifeCMS.DB.DeleteRecord(DBTable_OrderGoods,Array("OrderID:"& Fn_OrderID))
							If 1=Result Then Msg    = Msg & Lang_Order_Cue(4) &"{[orderid="& Fn_OrderID &"]}<br>"
						Else
							Result = KnifeCMS.DB.UpdateRecord(DBTable_Order,Array("Recycle:1"),Array("ID:"&ID))
							If 1=Result Then Msg = Msg & Lang_Order_Cue(3) &"{[orderid="& Fn_OrderID &"]}<br>"
						End If
					ElseIf Action = "edit" Then
						If SubAction = "revert" Then
							Result = KnifeCMS.DB.UpdateRecord(DBTable_Order,Array("Recycle:0"),Array("ID:"&ID))
							If 1=Result Then Msg = Msg & Lang_Order_Cue(5) &"{[orderid="& Fn_OrderID &"]}<br>"
						End If
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Sub
	
	'编辑订单界面-删除订单的商品，成功返回ok
	Public Function OrderGoodsDelete()
		Dim Fn_ii,Fn_Rs,Fn_Result,Fn_ItemID,Fn_OrderID,Fn_TempArray,Fn_TempShipFee,Fn_IsTax,Fn_TaxRate,Fn_ShippingID
		Dim Fn_Ship_Area,Fn_Ship_AreaArray,Fn_Ship_AreaGrade
		Fn_ItemID  = KnifeCMS.Data.CLng(KnifeCMS.GetForm("get","itemid"))
		Fn_OrderID = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("get","orderid"),"[^0-9]",""),16)
		Fn_Result  = KnifeCMS.DB.DeleteRecord(DBTable_OrderGoods,Array("ID:"& Fn_ItemID,"OrderID:"& Fn_OrderID))
		If Fn_Result And Fn_ItemID>0 Then
			'====计算订单商品数、商品总金额、总重量====
			Fn_TempArray    = GetOrderItemNumAndCost(Fn_OrderID)
			Pv_ItemNum      = KnifeCMS.Data.CLng(Fn_TempArray(0))
			Pv_Cost_Item    = KnifeCMS.Data.FormatDouble(Fn_TempArray(1))
			Pv_Weight       = KnifeCMS.Data.FormatDouble(Fn_TempArray(2))
			'==========================================
			Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Order &":Ship_Area,ShippingID,IsTax",Array("OrderID:"& Fn_OrderID),"")
			If Not(Fn_Rs.Eof) Then
				Fn_Ship_Area  = Fn_Rs(0)
				Fn_ShippingID = Fn_Rs(1)
				Fn_IsTax      = Fn_Rs(2)
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
			'====计算税金====
			'Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT Site_TaxRate FROM ["&DBTable_SystemSet&"]")
'			If Not(Fn_Rs.Eof) Then
'				Fn_TaxRate  = KnifeCMS.Data.FormatDouble(Fn_Rs(0))
'			End If
			Fn_TaxRate = KnifeCMS.Data.FormatDouble(SiteTaxRate) * 1/100
			KnifeCMS.DB.CloseRs Fn_Rs
			Pv_Cost_Tax = Pv_Cost_Item * Fn_TaxRate
			'=====计算配送费用、物流保价费====
			Fn_TempArray      = Split(Fn_Ship_Area,",")
			Fn_Ship_AreaGrade = Ubound(Fn_TempArray)-1
			ReDim Fn_Ship_AreaArray(Fn_Ship_AreaGrade)
			for Fn_ii=0 To Fn_Ship_AreaGrade
				if Fn_ii=0 Then
					Fn_Ship_AreaArray(0) = ","& Fn_TempArray(Fn_ii+1) &","
				else
					Fn_Ship_AreaArray(Fn_ii) = Fn_Ship_AreaArray(Fn_ii-1) & Fn_TempArray(Fn_ii+1) &","
				end if
			Next
			Fn_TempShipFee  = OS.GetShippingFee(Fn_ShippingID,Pv_Cost_Item,Pv_Weight,Fn_Ship_AreaArray,Fn_Ship_AreaGrade)
			Pv_Cost_Freight = Fn_TempShipFee(1)
			Pv_Cost_Protect = Fn_TempShipFee(3)
			'==================================
			'更新订单的总重量、订单商品数、商品总金额、配送费用、物流保价费、税金
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Order,Array("Weight:"& Pv_Weight,"ItemNum:"& Pv_ItemNum,"Cost_Item:"& Pv_Cost_Item,"Cost_Freight:"& Pv_Cost_Freight,"Cost_Protect:"& Pv_Cost_Protect,"Cost_Tax:"& Pv_Cost_Tax),Array("OrderID:"& Fn_OrderID))
			'计算订单总金额
			KnifeCMS.DB.Execute("UPDATE ["& DBTable_Order &"] SET Total_Amount=Cost_Item+Cost_Freight+Cost_Protect+Cost_Tax-Pmt_Amount+Discount")
			KnifeCMS.DB.Execute("UPDATE ["& DBTable_Order &"] SET Final_Amount=Total_Amount+Cost_Payment")
			Msg = Lang_Order_Cue(12) &"[ID="& Fn_ItemID &",OrderID="& Fn_OrderID &"]"
		Else
			Msg = Lang_Order_Cue(13) &"[ID="& Fn_ItemID &",OrderID="& Fn_OrderID &"]"
		End If
		Admin.ShowMessage Msg,Array("order.asp?ctl=order&act=edit&id="&ID&"",Lang_Goback),1
	End Function
	
	'订单付款创建收款单
	Private Function OrderPaySave()
		'退过款的订单不能再支付
		Dim Fn_TempMoney
		Dim Fn_Error(4)
		    Fn_Error(0) = Lang_Order_Cue(20)'"订单号传递错误."
			Fn_Error(1) = Lang_Order_Cue(21)'"收款金额不能小于或为零."
			Fn_Error(2) = Lang_Order_Cue(22)'"请选择付款类型."
			Fn_Error(3) = Lang_Order_Cue(23)'"请选择支付方式."
		Pv_BillID      = OS.CreateBillID("paymentbill")
		Pv_Bank        = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","bank")),250)
		Pv_Account     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","account")),250)
		Pv_PayAccount  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","payaccount")),250)
		Pv_Currency    = "CNY"
		Pv_Money       = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","money"))
		
		Pv_PayCost     = 0
		Pv_CurMoney    = Pv_Money
		Pv_PayType     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","paytype"),"[^0-9a-z]",""),10)
		Pv_PaymentID   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","paymentid"))
		Pv_PaymentName = KnifeCMS.DB.GetFieldByID(DBTable_Payments,"PaymentName",Pv_PaymentID)
		Pv_OperaterID  = Admin.SysUserID
		Pv_IP          = Admin.SysUserIP
		Pv_TBegin      = SysTime
		Pv_TEnd        = SysTime
		Pv_Status      = "success"
		Pv_Remarks     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","remarks")),1000)
		Pv_Disabled    = 0
		Pv_TradeNo     = ""
		
		If KnifeCMS.Data.IsNul(Pv_OrderID) Then ErrMsg = Fn_Error(0) &"<br>"
		If Not(Pv_Money>0) Then ErrMsg = ErrMsg & Fn_Error(1) &"<br>"
		If Not(Len(Pv_PayType)>1) Then ErrMsg = ErrMsg & Fn_Error(2) &"<br>"
		If Not(Pv_PaymentID>0) Or KnifeCMS.Data.IsNul(Pv_PaymentName) Then ErrMsg = ErrMsg & Fn_Error(3) &"<br>"
		
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act="& Action &"&orderid="& Pv_OrderID &"",0 : Exit Function
		
		Fn_TempMoney = Pv_Final_Amount - Pv_Payed
		If Fn_TempMoney < Pv_Money Then
			Pv_Money     = Fn_Temp
			Pv_PayStatus = 1
		ElseIf Fn_TempMoney = Pv_Money Then
			Pv_PayStatus = 1
		Else
			Pv_PayStatus = 3
		End If
		
		'保存银行帐号信息
		If Not(KnifeCMS.Data.IsNul(Pv_Account)) Then
			If Not(KnifeCMS.DB.CheckIsRecordExistByField(DBTable_BankAccount,Array("Bank:"& Pv_Bank,"AccountNO:"& Pv_Account))) Then
				Call KnifeCMS.DB.AddRecord(DBTable_BankAccount,Array("Bank:"& Pv_Bank,"AccountOwner:","AccountNO:"& Pv_Account,"AddTime:"& SysTime))
			End If
		End If
		
		Result = KnifeCMS.DB.AddRecord(DBTable_PaymentBill,Array("SysID:"& SysID,"BillID:"& Pv_BillID,"OrderID:"& Pv_OrderID,"UserID:"& Pv_UserID,"Bank:"& Pv_Bank,"Account:"& Pv_Account,"PayAccount:"& Pv_PayAccount,"Currency:"& Pv_Currency,"Money:"& Pv_Money,"PayCost:"& Pv_PayCost,"CurMoney:"& Pv_CurMoney,"PayType:"& Pv_PayType,"PaymentID:"& Pv_PaymentID,"PaymentName:"& Pv_PaymentName,"OperaterID:"& Pv_OperaterID,"IP:"& Pv_IP,"TBegin:"& Pv_TBegin,"TEnd:"& Pv_TEnd,"Status:"& Pv_Status,"Remarks:"& Pv_Remarks,"Disabled:"& Pv_Disabled,"TradeNo:"& Pv_TradeNo,"Recycle:0"))
		If Result Then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Order,"PayStatus="& Pv_PayStatus &",Payed=Payed+"& Pv_Money &"",Array("OrderID:"& Pv_OrderID))
			'=======创建日志=========
			Dim Fn_LogText,Fn_BeHavior
			Fn_LogText  = Lang_OrderLogText(1) & "{["& Lang_PaymentBillID &":"& Pv_BillID &"]["& Lang_PaymentMoney &":"& MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Money) &"]}"     '"订单支付"
			Fn_BeHavior = Lang_OrderLogBehavior(1) '"付款"
			Call KnifeCMS.DB.AddRecord(DBTable_OrderLog,Array("OrderID:"& Pv_OrderID,"UserID:NULL","SysUserID:"& Admin.SysUserID,"OperaterName:"& Admin.SysUsername,"LogText:" & Fn_LogText,"AddTime:"& SysTime,"BeHavior:"& Fn_BeHavior,"Result:success"))
		End If
		If Result Then Msg = Lang_PaySuccess &"[orderid:"& Pv_OrderID &",billid:"& Pv_BillID &"]"
		If Result Then Admin.ReloadTopDialog : Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
	End Function
	
	'订单退款创建退款单
	Private Function OrderRefundSave()
		'退过款的订单不能再支付
		Dim Fn_TempMoney
		Dim Fn_Error(5)
		    Fn_Error(0) = Lang_Order_Cue(20)'"订单号传递错误."
			Fn_Error(1) = Lang_Order_Cue(24)'"退款金额不能小于或为零."
			Fn_Error(2) = Lang_Order_Cue(25)'"请选择退款类型."
			Fn_Error(3) = Lang_Order_Cue(26)'"请选择退款方式."
			Fn_Error(4) = Lang_Order_Cue(27)'"退款金额不能超过已支付金额"
		Pv_BillID      = OS.CreateBillID("refundbill")
		Pv_Bank        = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","bank")),250)
		Pv_Account     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","account")),250)
		Pv_PayAccount  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","payaccount")),250)
		Pv_Currency    = "CNY"
		Pv_Money       = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","money"))
		Pv_PayType     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","paytype"),"[^0-9a-z]",""),10)
		Pv_PaymentID   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","paymentid"))
		Pv_PaymentName = KnifeCMS.DB.GetFieldByID(DBTable_Payments,"PaymentName",Pv_PaymentID)
		Pv_OperaterID  = Admin.SysUserID
		Pv_IP          = Admin.SysUserIP
		Pv_TReady      = SysTime
		Pv_TSent       = SysTime
		Pv_Status      = "success"
		Pv_Remarks     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","remarks")),1000)
		Pv_Disabled    = 0
		
		If KnifeCMS.Data.IsNul(Pv_OrderID) Then ErrMsg = Fn_Error(0) &"<br>"
		If Not(Pv_Money>0) Then ErrMsg = ErrMsg & Fn_Error(1) &"<br>"
		If Not(Len(Pv_PayType)>1) Then ErrMsg = ErrMsg & Fn_Error(2) &"<br>"
		If Not(Pv_PaymentID>0) Or KnifeCMS.Data.IsNul(Pv_PaymentName) Then ErrMsg = ErrMsg & Fn_Error(3) &"<br>"
		If Pv_Money > Pv_Payed Then ErrMsg = ErrMsg & Fn_Error(3) &"<br>"
		
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act="& Action &"&orderid="& Pv_OrderID &"",0 : Exit Function
		
		Fn_TempMoney = Pv_Payed - Pv_Money
		If Fn_TempMoney > 0 Then
			Pv_PayStatus = 4 '部分退款
		Else
			Pv_PayStatus = 5 '全额退款
		End If
		
		'保存银行帐号信息
		If Not(KnifeCMS.Data.IsNul(Pv_Account)) Then
			If Not(KnifeCMS.DB.CheckIsRecordExistByField(DBTable_BankAccount,Array("Bank:"& Pv_Bank,"AccountNO:"& Pv_Account))) Then
				Call KnifeCMS.DB.AddRecord(DBTable_BankAccount,Array("Bank:"& Pv_Bank,"AccountOwner:","AccountNO:"& Pv_Account,"AddTime:"& SysTime))
			End If
		End If
		
		Result = KnifeCMS.DB.AddRecord(DBTable_RefundBill,Array("SysID:"& SysID,"BillID:"& Pv_BillID,"OrderID:"& Pv_OrderID,"UserID:"& Pv_UserID,"Bank:"& Pv_Bank,"Account:"& Pv_Account,"PayAccount:"& Pv_PayAccount,"Currency:"& Pv_Currency,"Money:"& Pv_Money,"PayType:"& Pv_PayType,"PaymentID:"& Pv_PaymentID,"PaymentName:"& Pv_PaymentName,"OperaterID:"& Pv_OperaterID,"IP:"& Pv_IP,"TReady:"& Pv_TReady,"TSent:"& Pv_TSent,"Status:"& Pv_Status,"Remarks:"& Pv_Remarks,"Disabled:"& Pv_Disabled,"Recycle:0"))
		If Result Then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Order,"PayStatus="& Pv_PayStatus &",Payed=Payed-"& Pv_Money &"",Array("OrderID:"& Pv_OrderID))
			'=======创建日志=========
			Dim Fn_LogText,Fn_BeHavior
			Fn_LogText  = Lang_OrderLogText(2)     & "{["& Lang_RefundBillID &":"& Pv_BillID &"]["& Lang_RefundMoney &":"& MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Money) &"]}" '"订单退款"
			Fn_BeHavior = Lang_OrderLogBehavior(2) '"退款"
			Call KnifeCMS.DB.AddRecord(DBTable_OrderLog,Array("OrderID:"& Pv_OrderID,"UserID:NULL","SysUserID:"& Admin.SysUserID,"OperaterName:"& Admin.SysUsername,"LogText:"& Fn_LogText,"AddTime:"& SysTime,"BeHavior:"& Fn_BeHavior,"Result:success"))
		End If
		If Result Then Msg = Lang_RefundSuccess &"[orderid:"& Pv_OrderID &",billid:"& Pv_BillID &"]"
		If Result Then Admin.ReloadTopDialog : Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
	End Function
	
	'检查套装商品库存是否足够
	Private Function CheckPacksGoodsStoreIsEnough(ByVal BV_Config,ByVal BV_Nums)
		Dim Fn_ii,Fn_JSON,Fn_ReturnString,Fn_IsEnough
		Fn_IsEnough = True
		BV_Nums     = KnifeCMS.Data.CLng(BV_Nums)
		Set Fn_JSON = KnifeCMS.JSON.Parse(BV_Config)
		If BV_Nums > KnifeCMS.Data.CLng(KnifeCMS.DB.GetFieldByID(DBTable_Goods,"Store",Fn_JSON.goodsid)) Then
			Fn_ReturnString = Fn_ReturnString & "["& Lang_ThePack &":"& KnifeCMS.Data.UnEscape(Fn_JSON.packsname) &"|"& Lang_TheGoods &":"& KnifeCMS.Data.UnEscape(Fn_JSON.goodsname) &"]"& Lang_Understock &""
			Fn_IsEnough = False
		End If
		For Fn_ii=0 To Fn_JSON.selectgoods.length-1
			If BV_Nums > KnifeCMS.Data.CLng(KnifeCMS.DB.GetFieldByID(DBTable_Goods,"Store",Fn_JSON.selectgoods.get(Fn_ii).goodsid)) Then
				Fn_ReturnString = Fn_ReturnString & "["& Lang_ThePack &":"& KnifeCMS.Data.UnEscape(Fn_JSON.packsname) &"|"& Lang_TheGoods &":"& KnifeCMS.Data.UnEscape(Fn_JSON.selectgoods.get(Fn_ii).goodsname) &"]"& Lang_Understock &""
				Fn_IsEnough = False
			End If
		Next
		Set Fn_JSON = Nothing
		CheckPacksGoodsStoreIsEnough = Array(Fn_IsEnough,Fn_ReturnString)
	End Function
	
	'更新套装商品库存
	Private Function UpdatePacksGoodsStore(ByVal BV_Config,ByVal BV_Nums)
		Dim Fn_ii,Fn_JSON,Fn_GoodsID
		BV_Nums     = KnifeCMS.Data.CLng(BV_Nums)
		Set Fn_JSON = KnifeCMS.JSON.Parse(BV_Config)
		Fn_GoodsID = OpenWBs.Data.CLng(Fn_JSON.goodsid)
		If Fn_GoodsID>0 Then
			Call KnifeCMS.DB.UpdateRecord(DBTable_Goods,"Store=Store-"& BV_Nums &"",Array("ID:"& Fn_GoodsID))
		End If
		For Fn_ii=0 To Fn_JSON.selectgoods.length-1
			Fn_GoodsID = OpenWBs.Data.CLng(Fn_JSON.selectgoods.get(Fn_ii).goodsid)
			If Fn_GoodsID>0 Then
				Call KnifeCMS.DB.UpdateRecord(DBTable_Goods,"Store=Store-"& BV_Nums &"",Array("ID:"& Fn_GoodsID))
			End If
		Next
		Set Fn_JSON = Nothing
	End Function
	
	'订单发货创建发货单
	Private Function OrderShipSave()
		Dim Fn_ii,Fn_Rs,Fn_Temp,Fn_TempArray,Fn_IsOrderGoodsExit,Fn_NumDiff,Fn_IsCanCreateShipBill
		Dim Fn_Config,Fn_PacksID,Fn_PacksName,Fn_GoodsID,Fn_GoodsBN,Fn_GoodsName,Fn_Nums,Fn_Delivery_Status
		Dim Fn_Error(10)
		    Fn_Error(0) = Lang_Order_Cue(20)'"订单号传递错误."
			Fn_Error(1) = Lang_Order_Cue(28)'"发货单号生成错误."
			Fn_Error(2) = Lang_Order_Cue(29)'"收货地区错误."
			Fn_Error(3) = Lang_Order_Cue(30)'"必须填写详细地址."
			Fn_Error(4) = Lang_Order_Cue(31)'"必须填写收货人姓名."
			Fn_Error(5) = Lang_Order_Cue(32)'"联系手机与联系电话必须填写其中一个."
			Fn_Error(6) = Lang_Order_Cue(33)'"发货商品数据传递错误."
			Fn_Error(7) = Lang_Order_Cue(34)'"必须选择配送方式."
			Fn_Error(8) = Lang_Order_Cue(35)'"必须选择物流公司."
		Fn_IsCanCreateShipBill = False
		Pv_BillID              = OS.CreateBillID("shipbill")
		Pv_Remarks             = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","remarks")),250)
		
		If KnifeCMS.Data.IsNul(Pv_BillID)  Then ErrMsg = ErrMsg & Fn_Error(1) &"<br>"
		If KnifeCMS.Data.IsNul(Pv_OrderID) Then ErrMsg = ErrMsg & Fn_Error(0) &"<br>"
		
		'收货人信息
		for each Fn_Temp in Request.Form("ship_area")
			Pv_Ship_Area = trim(Fn_Temp)
		next
		Pv_Ship_Area     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Pv_Ship_Area,"[^0-9,]",""),50)
		Pv_Ship_Area     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Pv_Ship_Area,"([,]+?)",","),50)
		Pv_Ship_Address  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ship_address")),250)
		Pv_Ship_Zip      = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ship_zip"),"[^0-9\-]",""),20)
		Pv_Ship_Name     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ship_name")),50)
		Pv_Ship_Tel      = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ship_tel"),"[^0-9\-,]",""),50)
		Pv_Ship_Mobile   = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ship_mobile"),"[^0-9,]",""),50)
		Pv_Ship_Email    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ship_email")),60)
		If Not(KnifeCMS.Data.IsEmail(Pv_Ship_Email)) Then Pv_Ship_Email=""
		If Len(Pv_Ship_Area)<3 Then
			ErrMsg = ErrMsg & Fn_Error(2)
		Else
			Fn_TempArray      = Split(Pv_Ship_Area,",")
			Pv_Ship_AreaGrade = Ubound(Fn_TempArray)-1
			If Pv_Ship_AreaGrade<1 Then ErrMsg = ErrMsg & Fn_Error(2)
		End If
		Pv_ShippingArea = KnifeCMS.Data.Left(OS.GetRegionByPath(Pv_Ship_Area),250) '配送地区
		If KnifeCMS.Data.IsNul(Pv_Ship_Address) Then ErrMsg = ErrMsg & Fn_Error(3)
		If KnifeCMS.Data.IsNul(Pv_Ship_Name) Then ErrMsg = ErrMsg & Fn_Error(4)
		If Len(Pv_Ship_Tel)<11 And Len(Pv_Ship_Mobile)<8 Then ErrMsg = ErrMsg & Fn_Error(5)
		'================
		'检查此次发货的商品数据是否匹配
		Dim Fn_OrderGoodsIDArray,Fn_OrderGoodsID,Fn_SendNumsArray,Fn_SendNums
		Fn_OrderGoodsIDArray = Split(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ordergoodsid"),"[^0-9,]",""),",")
		Fn_SendNumsArray     = Split(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","sendnums"),"[^0-9,]",""),",")
		If Ubound(Fn_OrderGoodsIDArray)<>Ubound(Fn_SendNumsArray) Then ErrMsg = ErrMsg & Fn_Error(6)
		
		'================
		Pv_Cost_Freight = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","cost_freight"))  '配送费用
		Pv_Cost_Protect = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","cost_protect"))  '物流保价费
		Pv_Money        = Pv_Cost_Freight
		Pv_IsProtect    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","isprotect"))
		If Pv_IsProtect<>1 Then Pv_Cost_Freight=0
		Pv_Cost_Protect = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","money"))
		Pv_ShippingID   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","shippingid"))  '配送方式ID
		Pv_Shipping     = KnifeCMS.DB.GetFieldByID(DBTable_Delivery,"DeliveryName",Pv_ShippingID)
		Pv_Logi_ID      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","logi_id"))     '物流公司ID
		Pv_Logi_Name    = KnifeCMS.DB.GetFieldByID(DBTable_DeliveryCorp,"CorpName",Pv_Logi_ID)
		Pv_Logi_NO      = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","logi_no"),"[^0-9a-zA-Z\-]",""),50) '物流单号
		If Not(Pv_ShippingID>0) Or KnifeCMS.Data.IsNul(Pv_Shipping) Then ErrMsg = ErrMsg & Fn_Error(7)
		If Not(Pv_Logi_ID>0) Or KnifeCMS.Data.IsNul(Pv_Logi_Name) Then ErrMsg = ErrMsg & Fn_Error(8)
		'================
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act="& Action &"&orderid="& Pv_OrderID &"",0 : Exit Function
		Pv_OperaterID  = Admin.SysUserID
		'检查发货的商品是否合法存在并且库存足够
		Fn_IsCanCreateShipBill = False
		For Fn_ii=0 To Ubound(Fn_OrderGoodsIDArray)
			Fn_OrderGoodsID = KnifeCMS.Data.CLng(Fn_OrderGoodsIDArray(Fn_ii))
			Fn_SendNums     = KnifeCMS.Data.CLng(Fn_SendNumsArray(Fn_ii))
			If Fn_SendNums > 0 Then
				'检查发货的商品是否合法存在
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_OrderGoods,Array("ID:"& Fn_OrderGoodsID,"OrderID:"& Pv_OrderID),"")
				If Not(Fn_Rs.Eof) Then
					Fn_IsOrderGoodsExit = True
					Fn_Config     = Fn_Rs("Config")  
					Fn_PacksID    = KnifeCMS.Data.CLng(Fn_Rs("PacksID"))
					Fn_PacksName  = Fn_Rs("PacksName")
					Fn_GoodsID    = Fn_Rs("GoodsID")    
					Fn_GoodsBN    = Fn_Rs("GoodsBN")    
					Fn_GoodsName  = Fn_Rs("GoodsName")    
					Fn_Nums       = KnifeCMS.Data.CLng(Fn_Rs("Nums"))
					Fn_NumDiff    = Fn_Nums-KnifeCMS.Data.CLng(Fn_Rs("SendNums"))
				Else
					Fn_IsOrderGoodsExit    = False
				End IF
				KnifeCMS.DB.CloseRs Fn_Rs
				'检查发货数量是否超出可发货的数量.
				If Fn_SendNums > Fn_NumDiff Then
					If Fn_PacksID > 0 Then
						ErrMsg = ErrMsg & "["& Lang_ThePack &":"& Fn_PacksName &"]"& Lang_Order_Cue(36) &""
					Else
						ErrMsg = ErrMsg & "["& Lang_TheGoods &":"& Fn_GoodsName &"]"& Lang_Order_Cue(36) &""
					End If
					Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act="& Action &"&orderid="& Pv_OrderID &"",0 : Exit Function
				End If
				If Fn_IsOrderGoodsExit Then
					If Fn_PacksID > 0 Then
					'如果是套装则检查套装商品库存是否足够
						Fn_TempArray = CheckPacksGoodsStoreIsEnough(Fn_Config,Fn_SendNums)
						If Fn_TempArray(0)=False Then
							ErrMsg = ErrMsg & Fn_TempArray(1)
							Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act="& Action &"&orderid="& Pv_OrderID &"",0 : Exit Function
						Else
							Fn_IsCanCreateShipBill = True
						End If
					Else
					'如果是单独商品则商品库存是否足够
						If Fn_SendNums > KnifeCMS.Data.CLng(KnifeCMS.DB.GetFieldByID(DBTable_Goods,"Store",Fn_GoodsID)) Then
							ErrMsg = ErrMsg & "["& Lang_TheGoods &":"& Fn_GoodsName &"]"& Lang_Understock &"."
							Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act="& Action &"&orderid="& Pv_OrderID &"",0 : Exit Function
						Else
							Fn_IsCanCreateShipBill = True
						End If
					End If
				Else
					ErrMsg = ErrMsg & "[OrderGoodsID:"& Fn_OrderGoodsID &"]"& Lang_Order_Cue(37) &""
					Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act="& Action &"&orderid="& Pv_OrderID &"",0 : Exit Function
					Fn_IsCanCreateShipBill = False
				End If
			End IF
		Next
		If Fn_IsCanCreateShipBill = False Then
			ErrMsg = ErrMsg & Lang_Order_Cue(38) '"发货商品无效或发货数量错误."
			Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act="& Action &"&orderid="& Pv_OrderID &"",0 : Exit Function
		End If
		'保存发货单商品
		For Fn_ii=0 To Ubound(Fn_OrderGoodsIDArray)
			Fn_NumDiff      = 0
			Fn_OrderGoodsID = KnifeCMS.Data.CLng(Fn_OrderGoodsIDArray(Fn_ii))
			Fn_SendNums     = KnifeCMS.Data.CLng(Fn_SendNumsArray(Fn_ii))
			If Fn_SendNums > 0 Then
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_OrderGoods,Array("ID:"& Fn_OrderGoodsID,"OrderID:"& Pv_OrderID),"")
				If Not(Fn_Rs.Eof) Then
					Fn_IsOrderGoodsExit = True
					Fn_Config     = Fn_Rs("Config")  
					Fn_PacksID    = KnifeCMS.Data.CLng(Fn_Rs("PacksID"))
					Fn_PacksName  = Fn_Rs("PacksName")
					Fn_GoodsID    = Fn_Rs("GoodsID")    
					Fn_GoodsBN    = Fn_Rs("GoodsBN")    
					Fn_GoodsName  = Fn_Rs("GoodsName")    
					Fn_Nums       = KnifeCMS.Data.CLng(Fn_Rs("Nums"))
					Fn_NumDiff    = Fn_Nums-KnifeCMS.Data.CLng(Fn_Rs("SendNums"))
				Else
					Fn_IsOrderGoodsExit = False
				End IF
				KnifeCMS.DB.CloseRs Fn_Rs
				If Fn_SendNums>=Fn_NumDiff And Fn_SendNums>0 Then
					Fn_SendNums = Fn_NumDiff
					Fn_Delivery_Status = 1
				Else
					Fn_Delivery_Status = 2
				End If
				If Fn_IsOrderGoodsExit And Fn_NumDiff>0 Then
					Call KnifeCMS.DB.AddRecord(DBTable_ShipItem,Array("ShipBillID:"& Pv_BillID,"OrderID:"& Pv_OrderID,"OrderGoodsID:"& Fn_OrderGoodsID,"GoodsType:1","Config:"& Fn_Config,"PacksID:"& Fn_PacksID,"PacksName:"& Fn_PacksName,"GoodsID:"& Fn_GoodsID,"GoodsBN:"& Fn_GoodsBN,"GoodsName:"& Fn_GoodsName,"Nums:"& Fn_SendNums))
					Call KnifeCMS.DB.UpdateRecord(DBTable_OrderGoods,"Delivery_Status="& Fn_Delivery_Status &",SendNums=SendNums+"& Fn_SendNums &"",Array("ID:"& Fn_OrderGoodsID,"OrderID:"& Pv_OrderID))
					'更新商品库存
					If Fn_PacksID>0 Then
						Call UpdatePacksGoodsStore(Fn_Config,Fn_SendNums)
					Else
						Call KnifeCMS.DB.UpdateRecord(DBTable_Goods,"Store=Store-"& Fn_SendNums &"",Array("ID:"& Fn_GoodsID))
					End If
					Fn_IsCanCreateShipBill = True
				End If
			End If
		Next
		Result = KnifeCMS.DB.AddRecord(DBTable_ShipBill,Array("SysID:"& SysID,"BillID:"& Pv_BillID,"OrderID:"& Pv_OrderID,"UserID:"& Pv_UserID,"Money:"& Pv_Money,"Type:1","IsProtect:"& Pv_IsProtect,"Cost_Protect:"& Pv_Cost_Protect,"DeliveryID:"& Pv_ShippingID,"DeliveryName:"& Pv_Shipping,"Logi_ID:"& Pv_Logi_ID,"Logi_Name:"& Pv_Logi_Name,"Logi_NO:"& Pv_Logi_NO,"Ship_Name:"& Pv_Ship_Name,"Ship_Area:"& Pv_Ship_Area &":"& Pv_ShippingArea,"Ship_Address:"& Pv_Ship_Address,"Ship_Zip:"& Pv_Ship_Zip,"Ship_Tel:"& Pv_Ship_Tel,"Ship_Mobile:"& Pv_Ship_Mobile,"Ship_Email:"& Pv_Ship_Email,"Ship_Time:"& Pv_Ship_Time,"TBegin:"& SysTime,"OperaterID:"& Pv_OperaterID,"Status:progress","Remarks:"& Pv_Remarks,"Disabled:0","Recycle:0"))
		'=======创建日志=========
		If Result Then
			Dim Fn_LogText,Fn_BeHavior
			Fn_LogText  = Lang_OrderLogText(3) & "{["& Lang_ShipBillID &":"& Pv_BillID &"]}"     '"订单发货"
			Fn_BeHavior = Lang_OrderLogBehavior(3) '"发货"
			Call KnifeCMS.DB.AddRecord(DBTable_OrderLog,Array("OrderID:"& Pv_OrderID,"UserID:NULL","SysUserID:"& Admin.SysUserID,"OperaterName:"& Admin.SysUsername,"LogText:" & Fn_LogText,"AddTime:"& SysTime,"BeHavior:" & Fn_BeHavior,"Result:success"))
		End If
		'判断订单的发货状态
		Pv_ShipStatus = Admin.CheckOrderShipStatus(Pv_OrderID)
		If Result Then Call KnifeCMS.DB.UpdateRecord(DBTable_Order,Array("ShipStatus:"& Pv_ShipStatus),Array("OrderID:"& Pv_OrderID))
		If Result Then Msg = Lang_ShipSuccess &"[orderid:"& Pv_OrderID &",billid:"& Pv_BillID &"]"
		If Result Then Admin.ReloadTopDialog : Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
	End Function
	
	'订单退货创建退货单
	Private Function OrderReShipSave()
		Dim Fn_ii,Fn_Rs,Fn_Temp,Fn_TempArray,Fn_IsOrderGoodsExit,Fn_NumDiff,Fn_IsCanCreateReShipBill,Fn_SendNums,Fn_Reason
		Dim Fn_Config,Fn_PacksID,Fn_PacksName,Fn_GoodsID,Fn_GoodsBN,Fn_GoodsName,Fn_Nums,Fn_Delivery_Status
		Dim Fn_Error(10)
		    Fn_Error(0) = Lang_Order_Cue(20)'"订单号传递错误."
			Fn_Error(1) = Lang_Order_Cue(39)'"退货单号生成错误."
			Fn_Error(2) = Lang_Order_Cue(40)'"退货地区错误."
			Fn_Error(3) = Lang_Order_Cue(30)'"必须填写详细地址."
			Fn_Error(4) = Lang_Order_Cue(41)'"必须填写退货人姓名."
			Fn_Error(5) = Lang_Order_Cue(32)'"联系手机与联系电话必须填写其中一个."
			Fn_Error(6) = Lang_Order_Cue(42)'"退货商品数据传递错误."
			Fn_Error(7) = Lang_Order_Cue(34)'"必须选择配送方式."
			Fn_Error(8) = Lang_Order_Cue(35)'"必须选择物流公司."
		Fn_IsCanCreateReShipBill = False
		Pv_BillID                = OS.CreateBillID("shipbill")
		Pv_Remarks               = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","remarks")),250)
		Fn_Reason                = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","reason")),250)
		
		If KnifeCMS.Data.IsNul(Pv_OrderID) Then ErrMsg = ErrMsg & Fn_Error(0) &"<br>"
		If KnifeCMS.Data.IsNul(Pv_BillID)  Then ErrMsg = ErrMsg & Fn_Error(1) &"<br>"
		
		'退货人信息
		for each Fn_Temp in Request.Form("ship_area")
			Pv_Ship_Area = trim(Fn_Temp)
		next
		Pv_Ship_Area     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Pv_Ship_Area,"[^0-9,]",""),50)
		Pv_Ship_Area     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Pv_Ship_Area,"([,]+?)",","),50)
		Pv_Ship_Address  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ship_address")),250)
		Pv_Ship_Zip      = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ship_zip"),"[^0-9\-]",""),20)
		Pv_Ship_Name     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ship_name")),50)
		Pv_Ship_Tel      = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ship_tel"),"[^0-9\-,]",""),50)
		Pv_Ship_Mobile   = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ship_mobile"),"[^0-9,]",""),50)
		Pv_Ship_Email    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ship_email")),60)
		If Not(KnifeCMS.Data.IsEmail(Pv_Ship_Email)) Then Pv_Ship_Email=""
		If Len(Pv_Ship_Area)<3 Then
			ErrMsg = ErrMsg & Fn_Error(2)
		Else
			Fn_TempArray      = Split(Pv_Ship_Area,",")
			Pv_Ship_AreaGrade = Ubound(Fn_TempArray)-1
			If Pv_Ship_AreaGrade<1 Then ErrMsg = ErrMsg & Fn_Error(2)
		End If
		Pv_ShippingArea = KnifeCMS.Data.Left(OS.GetRegionByPath(Pv_Ship_Area),250) '配送地区
		If KnifeCMS.Data.IsNul(Pv_Ship_Address) Then ErrMsg = ErrMsg & Fn_Error(3)
		If KnifeCMS.Data.IsNul(Pv_Ship_Name) Then ErrMsg = ErrMsg & Fn_Error(4)
		If Len(Pv_Ship_Tel)<11 And Len(Pv_Ship_Mobile)<8 Then ErrMsg = ErrMsg & Fn_Error(5)
		'================
		'检查此次发货的商品数据是否匹配
		Dim Fn_OrderGoodsIDArray,Fn_OrderGoodsID,Fn_ReturnNumsArray,Fn_ReturnNums
		Fn_OrderGoodsIDArray = Split(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ordergoodsid"),"[^0-9,]",""),",")
		Fn_ReturnNumsArray     = Split(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","returnnums"),"[^0-9,]",""),",")
		If Ubound(Fn_OrderGoodsIDArray)<>Ubound(Fn_ReturnNumsArray ) Then ErrMsg = ErrMsg & Fn_Error(6)
		
		'================
		Pv_Cost_Freight = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","cost_freight"))  '配送费用
		Pv_Money        = Pv_Cost_Freight
		Pv_IsProtect    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","isprotect"))
		Pv_ShippingID   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","shippingid"))  '配送方式ID
		Pv_Shipping     = KnifeCMS.DB.GetFieldByID(DBTable_Delivery,"DeliveryName",Pv_ShippingID)
		Pv_Logi_ID      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","logi_id"))     '物流公司ID
		Pv_Logi_Name    = KnifeCMS.DB.GetFieldByID(DBTable_DeliveryCorp,"CorpName",Pv_Logi_ID)
		Pv_Logi_NO      = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","logi_no"),"[^0-9a-zA-Z\-]",""),50) '物流单号
		If Not(Pv_ShippingID>0) Or KnifeCMS.Data.IsNul(Pv_Shipping) Then ErrMsg = ErrMsg & Fn_Error(7)
		If Not(Pv_Logi_ID>0) Or KnifeCMS.Data.IsNul(Pv_Logi_Name) Then ErrMsg = ErrMsg & Fn_Error(8)
		'================
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act="& Action &"&orderid="& Pv_OrderID &"",0 : Exit Function
		Pv_OperaterID  = Admin.SysUserID
		'检查退货的商品是否合法
		Fn_IsCanCreateReShipBill = False
		For Fn_ii=0 To Ubound(Fn_OrderGoodsIDArray)
			Fn_OrderGoodsID = KnifeCMS.Data.CLng(Fn_OrderGoodsIDArray(Fn_ii))
			Fn_ReturnNums   = KnifeCMS.Data.CLng(Fn_ReturnNumsArray(Fn_ii))
			If Fn_ReturnNums > 0 Then
				'检查退货的商品是否合法存在
				If KnifeCMS.DB.CheckIsRecordExistByField(DBTable_OrderGoods,Array("ID:"& Fn_OrderGoodsID,"OrderID:"& Pv_OrderID)) Then
					'如果退货数量超过已发货数量 则提示错误
					If Fn_ReturnNums > KnifeCMS.Data.CLng(KnifeCMS.DB.GetFieldByArrayField(DBTable_OrderGoods,"SendNums",Array("ID:"& Fn_OrderGoodsID,"OrderID:"& Pv_OrderID))) Then
						ErrMsg = ErrMsg & Lang_Order_Cue(43)
						Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act="& Action &"&orderid="& Pv_OrderID &"",0 : Exit Function
					Else
						Fn_IsCanCreateReShipBill = True
					End If
				Else
					Fn_IsCanCreateReShipBill = False
				End If
			End IF
		Next
		If Fn_IsCanCreateReShipBill = False Then
			ErrMsg = ErrMsg & Lang_Order_Cue(44)
			Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act="& Action &"&orderid="& Pv_OrderID &"",0 : Exit Function
		End If
		'保存退货单商品
		Fn_IsCanCreateReShipBill = False
		For Fn_ii=0 To Ubound(Fn_OrderGoodsIDArray)
			Fn_OrderGoodsID = KnifeCMS.Data.CLng(Fn_OrderGoodsIDArray(Fn_ii))
			Fn_ReturnNums   = KnifeCMS.Data.CLng(Fn_ReturnNumsArray(Fn_ii))
			If Fn_ReturnNums > 0 Then
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_OrderGoods,Array("ID:"& Fn_OrderGoodsID,"OrderID:"& Pv_OrderID),"")
				If Not(Fn_Rs.Eof) Then
					Fn_IsOrderGoodsExit = True
					Fn_Config     = Fn_Rs("Config")  
					Fn_PacksID    = Fn_Rs("PacksID")
					Fn_PacksName  = Fn_Rs("PacksName")
					Fn_GoodsID    = Fn_Rs("GoodsID")    
					Fn_GoodsBN    = Fn_Rs("GoodsBN")    
					Fn_GoodsName  = Fn_Rs("GoodsName")    
					Fn_SendNums   = KnifeCMS.Data.CLng(Fn_Rs("SendNums"))
				Else
					Fn_IsOrderGoodsExit = False
				End IF
				KnifeCMS.DB.CloseRs Fn_Rs
				If Fn_SendNums=Fn_ReturnNums Then
					Fn_Delivery_Status = 4 '已退货
				Else
					Fn_Delivery_Status = 3 '部分退货
				End If
				If Fn_IsOrderGoodsExit Then
					Call KnifeCMS.DB.AddRecord(DBTable_ShipItem,Array("ShipBillID:"& Pv_BillID,"OrderID:"& Pv_OrderID,"OrderGoodsID:"& Fn_OrderGoodsID,"GoodsType:1","Config:"& Fn_Config,"PacksID:"& Fn_PacksID,"PacksName:"& Fn_PacksName,"GoodsID:"& Fn_GoodsID,"GoodsBN:"& Fn_GoodsBN,"GoodsName:"& Fn_GoodsName,"Nums:"& Fn_ReturnNums))
					Call KnifeCMS.DB.UpdateRecord(DBTable_OrderGoods,"Delivery_Status="& Fn_Delivery_Status &",SendNums=SendNums-"& Fn_ReturnNums &"",Array("ID:"& Fn_OrderGoodsID,"OrderID:"& Pv_OrderID))
					Fn_IsCanCreateReShipBill = True
				End If
			End If
		Next
		If Fn_IsCanCreateReShipBill Then
			Result = KnifeCMS.DB.AddRecord(DBTable_ShipBill,Array("SysID:"& SysID,"BillID:"& Pv_BillID,"OrderID:"& Pv_OrderID,"UserID:"& Pv_UserID,"Money:"& Pv_Money,"Type:0","IsProtect:"& Pv_IsProtect,"Cost_Protect:"& Pv_Cost_Protect,"DeliveryID:"& Pv_ShippingID,"DeliveryName:"& Pv_Shipping,"Logi_ID:"& Pv_Logi_ID,"Logi_Name:"& Pv_Logi_Name,"Logi_NO:"& Pv_Logi_NO,"Ship_Name:"& Pv_Ship_Name,"Ship_Area:"& Pv_Ship_Area &":"& Pv_ShippingArea,"Ship_Address:"& Pv_Ship_Address,"Ship_Zip:"& Pv_Ship_Zip,"Ship_Tel:"& Pv_Ship_Tel,"Ship_Mobile:"& Pv_Ship_Mobile,"Ship_Email:"& Pv_Ship_Email,"Ship_Time:"& Pv_Ship_Time,"TBegin:"& SysTime,"OperaterID:"& Pv_OperaterID,"Status:progress","Reason:"& Fn_Reason,"Remarks:"& Pv_Remarks,"Disabled:0","Recycle:0"))
			'=======创建日志=========
			If Result Then
				Dim Fn_LogText,Fn_BeHavior
				Fn_LogText  = Lang_OrderLogText(4) & "{["& Lang_ReShipBillID &":"& Pv_BillID &"]}"     '"订单退货"
				Fn_BeHavior = Lang_OrderLogBehavior(4) '"退货"
				Call KnifeCMS.DB.AddRecord(DBTable_OrderLog,Array("OrderID:"& Pv_OrderID,"UserID:NULL","SysUserID:"& Admin.SysUserID,"OperaterName:"& Admin.SysUsername,"LogText:" & Fn_LogText,"AddTime:"& SysTime,"BeHavior:" & Fn_BeHavior,"Result:success"))
			End If
			'判断订单的发货状态
			Pv_ShipStatus = Admin.CheckOrderShipStatus(Pv_OrderID)
			Call KnifeCMS.DB.UpdateRecord(DBTable_Order,Array("ShipStatus:"& Pv_ShipStatus),Array("OrderID:"& Pv_OrderID))
			If Result Then Msg = Lang_ReShipSuccess &"[orderid:"& Pv_OrderID &",billid:"& Pv_BillID &"]"
			If Result Then Admin.ReloadTopDialog : Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		Else
			Msg = Lang_Order_Cue(45)'"无法创建退货单."
			Admin.ReloadTopDialog : Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		End If
	End Function
	
	Private Function OrderFinishSave()
		If Not(KnifeCMS.Data.IsNul(Pv_OrderID)) Then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Order,Array("Status:3"),Array("OrderID:"& Pv_OrderID))
			If Result Then
				Msg = ""& Lang_OperateSuccess &":"& Lang_Order_Cue(46) &""
				'=======创建日志=========
				Dim Fn_LogText,Fn_BeHavior
				Fn_LogText  = Lang_OrderLogText(5)
				Fn_BeHavior = Lang_OrderLogBehavior(5)
				Call KnifeCMS.DB.AddRecord(DBTable_OrderLog,Array("OrderID:"& Pv_OrderID,"UserID:NULL","SysUserID:"& Admin.SysUserID,"OperaterName:"& Admin.SysUsername,"LogText:" & Fn_LogText,"AddTime:"& SysTime,"BeHavior:"& Fn_BeHavior,"Result:success"))
			Else
				Msg = ""& Lang_OperateFail &":"& Lang_Order_Cue(46) &""
			End If
		Else
			Msg = Lang_Order_Cue(7)'"订单不存在."
		End If
		Admin.ReloadTopDialog : Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
	End Function
	
	Private Function OrderInvalidSave()
		If Not(KnifeCMS.Data.IsNul(Pv_OrderID)) Then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Order,Array("Status:2"),Array("OrderID:"& Pv_OrderID))
			If Result Then
				Msg = ""& Lang_OperateSuccess &":"& Lang_Order_Cue(47) &""
				Dim Fn_LogText,Fn_BeHavior
				Fn_LogText  = Lang_OrderLogText(6)
				Fn_BeHavior = Lang_OrderLogBehavior(6)
				Call KnifeCMS.DB.AddRecord(DBTable_OrderLog,Array("OrderID:"& Pv_OrderID,"UserID:NULL","SysUserID:"& Admin.SysUserID,"OperaterName:"& Admin.SysUsername,"LogText:" & Fn_LogText,"AddTime:"& SysTime,"BeHavior:"& Fn_BeHavior,"Result:success"))
			Else
				Msg = ""& Lang_OperateFail &":"& Lang_Order_Cue(47) &""
			End If
		Else
			Msg = Lang_Order_Cue(7)'"订单不存在."
		End If
		Admin.ReloadTopDialog : Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
	End Function
	
	Private Function OrderView()
%>
	<%=Admin.Calendar(Array("","",""))%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
        <ul class="TabsStyle1">
           <li <% Echo KnifeCMS.IIF(Pv_ViewOrder=1,"class=""current""","")%> onclick="Admin.Goods.GetOrder(1)"><%=Lang_Order(1)%></li>
           <li <% Echo KnifeCMS.IIF(Pv_ViewOrder=2,"class=""current""","")%> onclick="Admin.Goods.GetOrder(2)"><%=Lang_Order(2)%></li>
           <li <% Echo KnifeCMS.IIF(Pv_ViewOrder=3,"class=""current""","")%> onclick="Admin.Goods.GetOrder(3)"><%=Lang_Order(3)%></li>
           <li <% Echo KnifeCMS.IIF(Pv_ViewOrder=4,"class=""current""","")%> onclick="Admin.Goods.GetOrder(4)"><%=Lang_Order(4)%></li>
           <li <% Echo KnifeCMS.IIF(Pv_ViewOrder=5,"class=""current""","")%> onclick="Admin.Goods.GetOrder(5)"><%=Lang_Order(5)%></li>
           <li <% Echo KnifeCMS.IIF(Pv_ViewOrder=6,"class=""current""","")%> onclick="Admin.Goods.GetOrder(6)"><%=Lang_Order(6)%></li>
           <li <% Echo KnifeCMS.IIF(Pv_ViewOrder=7,"class=""current""","")%> onclick="Admin.Goods.GetOrder(7)"><%=Lang_Order(7)%></li>
           <li class="none"><a href="javascript:ShowHelpMsg(600,300,'<%=Lang_Order_Cue(48)%>',Js_HelpMsg[9]);"><b class="icon icon_help"></b><%=Lang_Order_Cue(48)%></a></li>
        </ul>
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <% If SubCtl = "recycle" Then %>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:revert','')"><b class="icon icon_revert"></b><% Echo Lang_DoRevert %></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  <% Else %>
		  <td class="tdcell" style="display:none;">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
                  <span class="sysBtn"><a href="javascript:ShowModal('?ctl=<% Echo Ctl %>&act=add',940,600);ShowModalReload();"><b class="icon icon_add"></b><% Echo Lang_DoAdd %></a></span>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','')"><b class="icon icon_recycle"></b><% Echo Lang_DoDel%></button>
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
          <td class="tdcell">
            <table border="0" cellpadding="2" cellspacing="0">
            <tr><td class="upcell">
                <div class="align-center grey">
                    <% Echo Lang_DistrictDoSearch %><!--搜索操作区-->
                    <span class="AdvancedSearch"><a href="javascript:Admin.AdvancedSearch.Open(600,484,'AdvancedSearch')" ><% Echo Lang_AdvancedSearch %></a></span>
                </div>
                <DIV id="AdvancedSearch" style="display:none;">
                    <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                    <div class="diainbox">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                      <tr><td class="titletd"><%=Lang_Order(10)%></td><td class="infotd">
                          <select name="status[]" id="status[]" multiple="multiple" size="3" style="width:200px;">
                          <% Echo OS.CreateOption(Array("any:"& Lang_AnyStatus &"","1:"& Lang_ActiveOrder &"","2:"& Lang_HaveDead &"","3:"& Lang_HaveFinished &""),Split(Pv_Status,",")) %>
                          </select>
                          </td>
                      </tr>
                      <tr><td class="titletd"><%=Lang_Payment%>/<%=Lang_PayStatus(6)%>:</td><td class="infotd">
                          <table border="0" cellpadding="0" cellspacing="0"><tr>
                          <td class="pr5">
                          <div class="mb3 grey"><%=Lang_Payment%></div>
                          <% 
						  Dim Fn_PaymentString : Fn_PaymentString = Admin.GetListAsArrayList(DBTable_Payments &":ID,PaymentName","Disabled=0 ","ORDER BY OrderNum ASC,ID ASC")
						  If KnifeCMS.Data.IsNul(Fn_PaymentString) Then
							  Execute("Pv_PaymentData = Array(""any:"& Lang_AnyPayment &""")")
						  Else
						      Execute("Pv_PaymentData = Array(""any:"& Lang_AnyPayment &""","& Fn_PaymentString &")")
						  End If
						  %>
                          <select name="paymentid[]" id="paymentid[]" multiple="multiple" size="5" style="width:200px;">
                          <% Echo OS.CreateOption(Pv_PaymentData,Split(Pv_PaymentID,",")) %>
                          </select>
                          </td>
                          <td>
                          <div class="mb3 grey"><%=Lang_PayStatus(6)%></div>
                          <select name="paystatus[]" id="paystatus[]" multiple="multiple" size="5" style="width:200px;">
                          <%
						  Echo OS.CreateOption(Array("any:"& Lang_AnyPayStatus &"","0:"& Lang_PayStatus(0) &"","1:"& Lang_PayStatus(1) &"","2:"& Lang_PayStatus(2) &"","3:"& Lang_PayStatus(3) &"","4:"& Lang_PayStatus(4) &"","5:"& Lang_PayStatus(5) &""),Split(Pv_PayStatus,","))
						  %>
                          </select>
                          </td>
                          </tr></table>
                          </td>
                      </tr>
                      <tr><td class="titletd"><%=Lang_Delivery%>/<%=Lang_ShipStatus(5)%>:</td><td class="infotd">
                          <table border="0" cellpadding="0" cellspacing="0"><tr>
                          <td class="pr5">
                          <div class="mb3 grey"><%=Lang_Delivery%></div>
                          <%
						  Dim Fn_DeliveryString : Fn_DeliveryString = Admin.GetListAsArrayList(DBTable_Delivery &":ID,DeliveryName","Disabled=0 ","ORDER BY OrderNum ASC,ID ASC")
						  If KnifeCMS.Data.IsNul(Fn_DeliveryString) Then
							  Execute("Pv_ShippingData = Array(""any:"& Lang_AnyDelivery &""")")
						  Else
							  Execute("Pv_ShippingData = Array(""any:"& Lang_AnyDelivery &""","& Fn_DeliveryString &")")
						  End If
						  %>
                          <select name="shippingid[]" id="shippingid[]" multiple="multiple" size="5" style="width:200px;">
                          <% Echo OS.CreateOption(Pv_ShippingData,Split(Pv_ShippingID,",")) %>
                          </select>
                          </td>
                          <td>
                          <div class="mb3 grey"><%=Lang_ShipStatus(5)%></div>
                          <select name="shipstatus[]" id="shipstatus[]" multiple="multiple" size="5" style="width:200px;">
                          <% Echo OS.CreateOption(Array("any:"& Lang_AnyStatus &"","0:"& Lang_ShipStatus(0) &"","1:"& Lang_ShipStatus(1) &"","2:"& Lang_ShipStatus(2) &"","3:"& Lang_ShipStatus(3) &"","4:"& Lang_ShipStatus(4) &""),Split(Pv_ShipStatus,",")) %>
                          </select>
                          </td>
                          </tr></table>
                          </td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_Keywords %>:</td>
                          <td class="infotd">
                          <select name="keywordstype">
                          <% Echo OS.CreateOption(Array("orderid:"& Lang_OrderID &"","goodsbn:"& Lang_GoodsBN &"","goodsname:"& Lang_GoodsName &"","memberid:"& Lang_MemberID &"","membername:"& Lang_MemberName &"","ship_name:"& Lang_Ship_Name &"","ship_address:"& Lang_Ship_Address &"","ship_tel:"& Lang_Ship_Tel &"","mark_text:"& Lang_Mark_Text &""),Split(Pv_KeywordsType,",")) %>
                          </select>
                          <input type="text" class="TxtClass" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:306px;" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_StartDate %>:</td>
                          <td class="infotd"><input type="text" class="TxtSer dateStart" id="startdate" name="startdate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' value="<%=Admin.Search.StartDate%>" style="width:200px;" /></td>
                      <tr><td class="titletd"><% Echo Lang_EndDate %>:</td>
                          <td class="infotd"><input type="text" class="TxtSer dateEnd" id="enddate" name="enddate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' value="<%=Admin.Search.EndDate%>" style="width:200px;" /></td>
                      </tr>
                      </table>
                      </div>
                    <% Echo AdvancedSearchBtnline %>
                    </form>
                </DIV>
                </td></tr>
            <tr><td class="downcell">
                <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                <table border="0" cellpadding="0" cellspacing="0"><tr>
                <td><span><% Echo Lang_Keywords %>:</span></td>
                <td class="pl3">
                <select name="keywordstype">
                <% Echo OS.CreateOption(Array("orderid:"& Lang_OrderID &"","goodsbn:"& Lang_GoodsBN &"","goodsname:"& Lang_GoodsName &"","memberid:"& Lang_MemberID &"","membername:"& Lang_MemberName &"","ship_name:"& Lang_Ship_Name &"","ship_address:"& Lang_Ship_Address &"","ship_tel:"& Lang_Ship_Tel &"","mark_text:"& Lang_Mark_Text &""),Split(Pv_KeywordsType,",")) %>
                </select>
                </td>
                <td class="pl3"><input type="text" class="TxtClass" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:150px;" /></td>
                <td class="pl3"><button type="submit" class="sysBtn" ><b class="icon icon_search"></b><% Echo Lang_Btn_Search %></button></td>
                </tr></table>
                </form>
            </td></tr>
            </table>
          </td>
		  <% End If %>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th><div><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
			<th><div><% Echo Lang_Order_ListTableColLine(1) %><!--订单号--></div></th>
            <th><div><% Echo Lang_Order_ListTableColLine(2) %><!--订单总金额--></div></th>
            <th><div><% Echo Lang_Order_ListTableColLine(22) %><!--手续费--></div></th>
            <th><div><% Echo Lang_Order_ListTableColLine(23) %><!--须支付总额--></div></th>
            <th><div><% Echo Lang_Order_ListTableColLine(4) %><!--订单状态--></div></th>
            <th><div><% Echo Lang_Order_ListTableColLine(5) %><!--付款状态--></div></th>
            <th><div><% Echo Lang_Order_ListTableColLine(6) %><!--发货状态--></div></th>
            <th><div><% Echo Lang_Order_ListTableColLine(7) %><!--付款方式--></div></th>
            <th><div><% Echo Lang_Order_ListTableColLine(8) %><!--配送方式--></div></th>
            <th><div><% Echo Lang_Order_ListTableColLine(9) %><!--下单会员--></div></th>
            <th><div><% Echo Lang_Order_ListTableColLine(3) %><!--收货人--></div></th>
            <th><div><% Echo Lang_Order_ListTableColLine(10) %><!--下单时间--></div></th>
			<th><div><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% OrderList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
    <script type="text/javascript">
	KnifeCMS.SetSelectIndexNumFontWeight(0,KnifeCMS.$id("status[]"));
	KnifeCMS.SetSelectIndexNumFontWeight(0,KnifeCMS.$id("paymentid[]"));
	KnifeCMS.SetSelectIndexNumFontWeight(0,KnifeCMS.$id("paystatus[]"));
	KnifeCMS.SetSelectIndexNumFontWeight(0,KnifeCMS.$id("shippingid[]"));
	KnifeCMS.SetSelectIndexNumFontWeight(0,KnifeCMS.$id("shipstatus[]"));
	var OrderStatus= ["","<%=Lang_ActiveOrder%>","<%=Lang_HaveDead%>","<%=Lang_HaveFinished%>"];
	var PayStatus  = Lang_Js_PayStatus;
	var ShipStatus = ["<%=Lang_ShipStatus(0)%>","<%=Lang_ShipStatus(1)%>","<%=Lang_ShipStatus(2)%>","<%=Lang_ShipStatus(3)%>","<%=Lang_ShipStatus(4)%>"];
	TrBgChange("listForm","InputName","oliver","data","select");
	if(objExist(document.getElementById("keywords"))){ReplaceText(document.getElementById("keywords").value,1)}
	function ChangeTdText(){
		var tds=document.getElementById("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			var IsEdit = true;
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td0"){
					TempHTML='<a href="?ctl=order&act=view&subact=detail&id='+tds[i].title+'">'+tds[i].innerHTML+'</a>';
					tds[i].innerHTML = TempHTML;
				}
				if(tds[i].id=="td1"){
					TempHTML='<a href="?ctl=order&act=view&subact=detail&id='+tds[i-1].title+'">'+tds[i].innerHTML+'</a>';
					tds[i].innerHTML = TempHTML;
				}
				if(tds[i].id=="td2"){
					TempHTML=KnifeCMS.FormatCurrency(tds[i].innerHTML);
					tds[i].innerHTML = '<% Echo MoneySb %>'+TempHTML
				}
				if(tds[i].id=="td3"){
					TempHTML=KnifeCMS.FormatCurrency(tds[i].innerHTML);
					tds[i].innerHTML = '<% Echo MoneySb %>'+TempHTML
				}
				if(tds[i].id=="td4"){
					TempHTML=KnifeCMS.FormatCurrency(tds[i].innerHTML);
					tds[i].innerHTML = "<span class='redh'>"+'<% Echo MoneySb %>'+TempHTML+"</span>"
				}
				if(tds[i].id=="td5"){
					TempHTML=tds[i].innerHTML;//定单状态{1:active(活动定单),2:dead(已作废),3:finish(已完成)}
					if(parseInt(TempHTML)>1){IsEdit=false;}
					tds[i].innerHTML = OrderStatus[parseInt(TempHTML)]
				}
				if(tds[i].id=="td6"){
					TempHTML=tds[i].innerHTML;//支付状态{0:未支付,1:已支付,2:处理中,3:部分付款,4:部分退款,5:全额退款}
					if(parseInt(TempHTML)>0){IsEdit=false;}
					tds[i].innerHTML = PayStatus[parseInt(TempHTML)]
				}
				if(tds[i].id=="td7"){
					TempHTML=tds[i].innerHTML;//发货状态{0:未发货,1:已发货,2:部分发货,3:部分退货,4:已退货}
					if(parseInt(TempHTML)>0){IsEdit=false;}
					tds[i].innerHTML = ShipStatus[parseInt(TempHTML)]
				}
				if(tds[i].id=="td8"){
					if(IsEdit==true){
						TempHTML=tds[i-7].innerHTML;//加粗
						tds[i-7].innerHTML = "<b>"+TempHTML+"</b>"
					}
				}
				if(tds[i].id=="Operate"){
					if(IsEdit==false){
						var span = tds[i].getElementsByTagName("span");
						if(span.length==2){tds[i].removeChild(span[1]);}
						IsEdit=true;
					}
				}
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
	Private Function OrderAdd()
%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
          <div class="wbox">
            <div class="inbox">
                您所使用的软件版本暂未开放此功能.
            </div>
          </div>
      </DIV>
    </DIV>
<%
	End Function
	Private Function OrderEdit()
%>
    <script language="javascript" src="../data/javascript/region.json.js"></script>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
          <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
          <div class="obox">
            <div class="tit"><%=Lang_Order(12)%></div>
            <div class="inbox">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                  <tr><td class="titletd"><%=Lang_OrderID%>:</td><td class="infotd"><% Echo Pv_OrderID %></td><td class="titletd"><%=Lang_Order(13)%>:</td><td class="infotd"><% Echo Pv_Createtime %></td></tr>
                  <tr><td class="titletd"><%=Lang_Order(14)%><!--商品总金额-->:</td><td class="infotd">
                      <span><% Echo Pv_Cost_Item %></span>
                      <input type="text" class="TxtClass" disabled="disabled" name="cost_item" id="cost_item" style="display:none;width:150px;" size="10" value="<% Echo Pv_Cost_Item %>" />
                      </td>
                      <td class="titletd"><%=Lang_Order(15)%><!--配送方式-->:</td><td class="infotd">
                      <select name="shippingid">
					  <% Echo Admin.GetDeliveryAsOption(0,Pv_ShippingID) %>
                      </select>
                      </td></tr>
                      
                  <tr><td class="titletd"><%=Lang_Order(16)%><!--配送费用-->:</td><td class="infotd">
                      <input type="text" class="TxtClass" name="cost_freight" id="cost_freight" style="width:150px;" size="10" value="<% Echo Pv_Cost_Freight %>" /></td>
                      <td class="titletd"><%=Lang_Payment%>:</td><td class="infotd">
                      <select name="paymentid">
					  <% Echo Admin.GetPaymentsAsOption(0,Pv_PaymentID) %>
                      </select>
                      </td></tr>
                      
                  <tr><td class="titletd"><%=Lang_Order(17)%><!--保价费-->:</td><td class="infotd">
                      <input type="text" class="TxtClass" name="cost_protect" id="cost_protect" style="width:150px;" size="10" value="<% Echo Pv_Cost_Protect %>" /></td>
                      <td class="titletd"><%=Lang_Order(18)%><!--总重量-->:</td><td class="infotd">
                      <input type="text" class="TxtClass" name="weight" id="weight" style="width:150px;" size="10" value="<% Echo Pv_Weight %>" /></td></tr>
                      
                  <tr><td class="titletd"><%=Lang_Order(19)%><!--税金-->:</td><td class="infotd">
                      <input type="text" class="TxtClass" name="cost_tax" id="cost_tax" style="width:150px;" size="10" value="<% Echo Pv_Cost_Tax %>" /></td>
                      <td class="titletd"><%=Lang_Order(20)%><!--发票抬头-->:</td><td class="infotd">
                      <input type="text" class="TxtClass" name="tax_company" id="tax_company" style="width:260px;" value="<% Echo Pv_Tax_Company %>"  /></td></tr>
                      
                  <tr><td class="titletd"><%=Lang_Order(21)%><!--促销优惠金额-->:</td><td class="infotd">
                      <input type="text" class="TxtClass" name="pmt_amount" id="pmt_amount" style="width:150px;" size="10" value="<% Echo Pv_Pmt_Amount %>" /></td>
                      <td class="titletd"><%=Lang_Order(22)%><!--支付币别-->:</td><td class="infotd">
                      <% Echo Pv_Currency %></td></tr>
                      
                  <tr><td class="titletd"><%=Lang_Order(23)%><!--支付手续费-->:</td><td class="infotd">
                      <input type="text" class="TxtClass" name="cost_payment" id="cost_payment" style="width:150px;" size="10" value="<% Echo Pv_Cost_Payment %>" /></td>
                      <td class="titletd"><%=Lang_Order(24)%><!--订单折扣或涨价-->:</td><td class="infotd">
                      <input type="text" class="TxtClass" name="discount" id="discount" style="width:60px;" size="10" value="<% Echo Pv_Discount %>" /><br />
                      <label class="Normal"><%=Lang_Order(25)%><!--要便宜99元，则输入"-99"；要加99元，则输入"99"。--></label></td></tr>
                      
                  <tr><td class="titletd"><%=Lang_Order(26)%><!--订单总金额-->:</td><td class="infotd" colspan="3">
                      <span><% Echo KnifeCMS.Data.FormatCurrency(Pv_Total_Amount) %></span>
                      <input type="hidden" class="TxtClass" name="total_amount" id="total_amount" style="width:150px;" size="12" value="<% Echo Pv_Total_Amount %>" /></td></tr>
                  <tr><td class="titletd"><%=Lang_Order_ListTableColLine(23)%><!--须支付总额-->:</td><td class="infotd" colspan="3">
                      <span class="font14 redh"><% Echo KnifeCMS.Data.FormatCurrency(Pv_Final_Amount) %></span>
                      <input type="hidden" class="TxtClass" name="final_amount" id="final_amount" style="width:150px;" size="12" value="<% Echo Pv_Final_Amount %>" /></td></tr>
                </table>
            </div>
            <div class="tit"><%=Lang_Order(27)%><!--商品信息--></div>
            <div class="inbox">
                <table id="goodslist" border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
                 <tbody>
                  <tr class="top">
                  <th width="8%"><div style="width:150px;"><% Echo Lang_Order_ListTableColLine(11) %><!--商品编号--></div></th>
                  <th width="62%"><div style="width:260px;"><% Echo Lang_Order_ListTableColLine(12) %><!--商品名称--></div></th>
                  <th width="10%"><div style="width:80px; "><% Echo Lang_Order_ListTableColLine(14) %><!--价格--></div></th>
                  <th width="10%"><div style="width:60px; "><% Echo Lang_Order_ListTableColLine(15) %><!--购买数量--></div></th>
                  <th width="10%"><div style="width:80px; "><% Echo Lang_Order_ListTableColLine(16) %><!--金额小计--></div></th>
                  <th width="10%"><div style="width:60px;"><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
                  </tr>
                  <% Call OrderGoodsList(1,"") %>
                 </tbody>
                </table>
                <script type="text/javascript">
                TrBgChange("goodslist","oliver","data");
                </script>
            </div>
            <div class="tit"><%=Lang_Order(28)%><!--收货人信息--></div>
            <div class="inbox">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                  <tr><td class="titletd"><%=Lang_Ship_Name%>:</td><td class="infotd">
				      <input type="text" class="TxtClass" name="ship_name" id="ship_name" style="width:150px;" value="<% Echo Pv_Ship_Name %>" /></td>
                      <td class="titletd"><%=Lang_Ship_Email%>:</td><td class="infotd">
					  <input type="text" class="TxtClass" name="ship_email" id="ship_email" style="width:150px;" value="<% Echo Pv_Ship_Email %>" /></td></tr>
                  <tr><td class="titletd"><%=Lang_Ship_Area%>:</td><td class="infotd">
				      <span id="ship_area"></span>
					  <script type="text/javascript">
                      CreateAddressAsSelect(KnifeCMS.$id("ship_area"),",","<% Echo Pv_Ship_Area %>","ship_area",false);
                      </script></td>
                      <td class="titletd"><%=Lang_Ship_Tel%>:</td><td class="infotd">
					  <input type="text" class="TxtClass" name="ship_tel" id="ship_tel" style="width:150px;" value="<% Echo Pv_Ship_Tel %>" /></td></tr>
                  <tr><td class="titletd"><%=Lang_Ship_Zip%>:</td><td class="infotd">
				      <input type="text" class="TxtClass" name="ship_zip" id="ship_zip" style="width:60px;" value="<% Echo Pv_Ship_Zip %>" /></td>
                      <td class="titletd"><%=Lang_Ship_Mobile%>:</td><td class="infotd">
					  <input type="text" class="TxtClass" name="ship_mobile" id="ship_mobile" style="width:150px;" value="<% Echo Pv_Ship_Mobile %>" /></td></tr>
                  <tr><td class="titletd"><%=Lang_Ship_Address%>:</td><td class="infotd" colspan="3">
				      <input type="text" class="TxtClass" name="ship_address" id="ship_address" style="width:300px;" value="<% Echo Pv_Ship_Address %>" /></td></tr>
                </table>
            </div>
            <div class="inbox">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                  <tr><td class="titletd"><%=Lang_Ship_Time%>:</td><td class="infotd">
				      <input type="text" class="TxtClass" name="ship_time" id="ship_time" style="width:150px;" value="<% Echo Pv_Ship_Time %>" /></td></tr>
                  <tr><td class="titletd"><%=Lang_Mark_Text%>:</td><td class="infotd">
				      <textarea class="mini" name="mark_text" value="" style="width:360px; height:60px;"><% Echo KnifeCMS.Data.HTMLDecode(Pv_Mark_Text) %></textarea></td></tr>
                </table>
            </div>
            
          </div>
          <input type="hidden" name="id" value="<% Echo ID %>" />
          <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
          <input type="hidden" name="orderid" id="orderid" value="<% Echo Pv_OrderID %>" />
          <div class="bottomSaveline" id="SubmitButtoms" style="text-align:center;">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Goods.SaveAttributeCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
          </div>
          </form>
      </DIV>
    </DIV>
<%
	End Function
	Private Function OrderDetail()
%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
          
          <div class="obox">
            <div class="fr pb10">
            <span class="mr10">
            <button type="button" class="sysBtn" <% Echo KnifeCMS.IIF(Pv_IsNeedPay,"","disabled=""disabled""") %> onClick="ShowModal('?ctl=<%=Ctl%>&act=orderpay&orderid=<%=Pv_OrderID%>',800,520);ShowModalReload();"><span class="pl5 pr5"><% Echo Lang_DoPay %></span></button><button type="button" class="sysBtn" <% Echo KnifeCMS.IIF(Pv_IsCanShip,"","disabled=""disabled""") %> onClick="ShowModal('?ctl=<%=Ctl%>&act=ordership&orderid=<%=Pv_OrderID%>',960,620);ShowModalReload();"><span class="pl5 pr5"><% Echo Lang_DoShip %></span></button><button type="button" class="sysBtn" <% Echo KnifeCMS.IIF(Pv_IsCanFinish,"","disabled=""disabled""") %> onClick="ShowModal('?ctl=<%=Ctl%>&act=orderfinish&orderid=<%=Pv_OrderID%>',800,520);ShowModalReload();"><span class="pl5 pr5"><% Echo Lang_DoFinish %></span></button>
            </span>
            <span class="mr10">
            <button type="button" class="sysBtn" <% Echo KnifeCMS.IIF(Pv_IsCanRefund,"","disabled=""disabled""") %> onClick="ShowModal('?ctl=<%=Ctl%>&act=orderrefund&orderid=<%=Pv_OrderID%>',800,520);ShowModalReload();"><span class="pl5 pr5"><% Echo Lang_DoRefund %></span></button><button type="button" class="sysBtn" <% Echo KnifeCMS.IIF(Pv_IsCanReShip,"","disabled=""disabled""") %> onClick="ShowModal('?ctl=<%=Ctl%>&act=orderreship&orderid=<%=Pv_OrderID%>',960,620);ShowModalReload();"><span class="pl5 pr5"><% Echo Lang_DoReShip %></span></button>
            </span>
            <span>
            <button type="button" class="sysBtn" <% Echo KnifeCMS.IIF(Pv_IsCanInvalid,"","disabled=""disabled""") %> onClick="ShowModal('?ctl=<%=Ctl%>&act=orderinvalid&orderid=<%=Pv_OrderID%>',800,520);ShowModalReload();"><span class="pl5 pr5"><% Echo Lang_DoInvalid %></span></button>
            </span>
            
            </div>
            <div class="tit"><%=Lang_OrderID%>:<% Echo Pv_OrderID %></div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td valign="top">
                <div class="inbox mr5">
                    <div class="tit"><%=Lang_Order(12)%></div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                    <tr><td class="titletd"><%=Lang_Order(47)%><!--商品总额-->:</td><td class="infotd"><% Echo MoneySb & Pv_Cost_Item %></td></tr>
                    <tr><td class="titletd"><%=Lang_Order(48)%><!--配送费用-->:</td><td class="infotd"><% Echo MoneySb & Pv_Cost_Freight %></td></tr>
                    <tr><td class="titletd"><%=Lang_Order(17)%><!--保价费-->:</td><td class="infotd"><% Echo MoneySb & Pv_Cost_Protect %></td></tr>
                    <tr><td class="titletd"><%=Lang_Order(19)%><!--税金-->:</td><td class="infotd"><% Echo MoneySb & Pv_Cost_Tax %></td></tr>
                    <tr><td class="titletd"><%=Lang_Order(21)%><!--促销优惠金额-->:</td><td class="infotd"><% Echo MoneySb & Pv_Pmt_Amount %></td></tr>
                    <tr><td class="titletd"><%=Lang_Order(24)%><!--订单折扣或涨价-->:</td><td class="infotd">
					<%
					If Pv_Discount<>0 Then
						Echo KnifeCMS.IIF(Mid(Pv_Discount,1,1)="-",MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Discount) & "("& Lang_Order(91) &")",MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Discount) & "("& Lang_Order(92) &")")
					Else
						Echo MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Discount)
					End If
					%>
                    </td></tr>
                    <tr><td class="titletd"><%=Lang_Order(26)%><!--订单总金额-->:</td><td class="infotd"><% Echo MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Total_Amount) %></td></tr>
                     <tr><td class="titletd"><%=Lang_Order(23)%><!--支付手续费-->:</td><td class="infotd"><% Echo MoneySb & Pv_Cost_Payment %></td></tr>
                    <tr><td class="titletd"><%=Lang_Order_ListTableColLine(23)%><!--需支付总额-->:</td><td class="infotd"><b class="font14 redh"><% Echo MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Final_Amount) %></b></td></tr>
                    <tr><td class="titletd"><%=Lang_HavePayedMoney%>:</td><td class="infotd"><b class="font14 green"><% Echo MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Payed) %></b></td></tr>
                    <tr><td><div style="width:98px;"></div></td><td><div style="width:100px;"></div></td></tr>
                    </table>
                </div>
                </td>
                <td valign="top">
                <div class="inbox mr5">
                    <div class="tit"><%=Lang_Order(49)%><!--订单其他信息--></div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                    <tr><td class="titletd"><%=Lang_Order(46)%><!--下单会员-->:</td><td class="infotd"><% Echo Pv_UserName %></td></tr>
                    <tr><td class="titletd"><%=Lang_Order(13)%><!--下单时间-->:</td><td class="infotd"><% Echo Pv_Createtime %></td></tr>
                    <tr><td class="titletd"><%=Lang_Order(50)%><!--商品重量-->:</td><td class="infotd"><% Echo Pv_Weight %> g</td></tr>
                    <tr><td class="titletd"><%=Lang_Order(15)%><!--配送方式-->:</td><td class="infotd"><% Echo Pv_Shipping %></td></tr>
                    <tr><td class="titletd"><%=Lang_Order(51)%><!--配送保价-->:</td><td class="infotd"><% Echo KnifeCMS.IIF(Pv_IsProtect=1,Lang_Yes,Lang_No) %></td></tr>
                    <tr><td class="titletd"><%=Lang_Order(52)%><!--是否开票-->:</td><td class="infotd"><% Echo KnifeCMS.IIF(Pv_IsTax=1,Lang_Yes,Lang_No) %></td></tr>
                    <tr><td class="titletd"><%=Lang_Payment%>:</td><td class="infotd"><% Echo Pv_Payment %></td></tr>
                    <tr><td class="titletd"><%=Lang_Order(20)%><!--发票抬头-->:</td><td class="infotd"><% Echo Pv_Tax_Company %></td></tr>
                    <tr><td class="titletd"><%=Lang_Mark_Text%>:</td><td class="infotd"><% Echo Pv_Mark_Text %></td></tr>
                    <tr><td><div style="width:98px;"></div></td><td><div style="width:100px;"></div></td></tr>
                    </table>
                </div>
                </td>
                <td valign="top">
                <div class="inbox">
                    <div class="tit"><%=Lang_Order(28)%><!--收货人信息--></div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                    <tr><td class="titletd"><%=Lang_Ship_Time%>:</td><td class="infotd"><% Echo Pv_Ship_Time %></td></tr>
                    <tr><td class="titletd"><%=Lang_Ship_Name%><!--收货人姓名-->:</td><td class="infotd"><% Echo Pv_Ship_Name %></td></tr>
                    <tr><td class="titletd"><%=Lang_Ship_Tel%><!--电话-->:</td><td class="infotd"><% Echo Pv_Ship_Tel %></td></tr>
                    <tr><td class="titletd"><%=Lang_Ship_Mobile%><!--手机-->:</td><td class="infotd"><% Echo Pv_Ship_Mobile %></td></tr>
                    <tr><td class="titletd"><%=Lang_Ship_Area%><!--地区-->:</td><td class="infotd"><% Echo Pv_ShippingArea %></td></tr>
                    <tr><td class="titletd"><%=Lang_Ship_Address%><!--地址-->:</td><td class="infotd"><% Echo Pv_Ship_Address  %></td></tr>
                    <tr><td class="titletd"><%=Lang_Ship_Zip%><!--邮编-->:</td><td class="infotd"><% Echo Pv_Ship_Zip %></td></tr>
                    <tr><td class="titletd"><%=Lang_Ship_Email%><!--Email-->:</td><td class="infotd"><% Echo Pv_Ship_Email %></td></tr>
                    <tr><td><div style="width:98px;"></div></td><td><div style="width:136px;"></div></td></tr>
                    </table>
                </div>
                </td>
                <td></td>
              </tr>
            </table>
            <div class="inbox">
                <div class="tit"><%=Lang_Order(27)%><!--商品信息--></div>
                <table id="goodslist" border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
                 <tbody>
                  <tr class="top">
                  <th width="8%" ><div style="width:150px;"><% Echo Lang_Order_ListTableColLine(11) %><!--商品编号--></div></th>
                  <th width="62%"><div style="width:360px;"><% Echo Lang_Order_ListTableColLine(12) %><!--商品名称--></div></th>
                  <th width="10%"><div style="width:80px; "><% Echo Lang_Order_ListTableColLine(14) %><!--价格--></div></th>
                  <th width="10%"><div style="width:60px; "><% Echo Lang_Order_ListTableColLine(15) %><!--购买数量--></div></th>
                  <th width="10%"><div style="width:80px; "><% Echo Lang_Order_ListTableColLine(16) %><!--金额小计--></div></th>
                  </tr>
                  <% Call OrderGoodsList(0,"") %>
                 </tbody>
                </table>
                <script type="text/javascript">
                TrBgChange("goodslist","oliver","data");
                </script>
            </div>
          </div>
          <div class="obox">
            <div class="tit"><%=Lang_Order(53)%><!--收退款记录--></div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td valign="top" width="50%">
                <div class="inbox">
                    <div class="tit"><%=Lang_Order(54)%><!--收款单据列表--></div>
                    <table id="goodslist" border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
                      <tbody>
                        <tr class="top">
                        <th width="30%"><div style="width:100px;"><%=Lang_Order(55)%><!--单据日期--></div></th>
                        <th width="20%"><div style="width:60px; "><%=Lang_PaymentMoney%></div></th>
                        <th width="30%"><div style="width:100px;"><%=Lang_Payment%></div></th>
                        <th width="20%"><div style="width:60px; "><%=Lang_Status%><!--状态--></div></th>
                        </tr>
                        <% Echo OrderPaymentBillList(Pv_OrderID) %>
                      </tbody>
                    </table>
                </div>
                </td>
                <td valign="top" width="50%">
                <div class="inbox">
                    <div class="tit"><%=Lang_Order(56)%><!--退款单据列表--></div>
                    <table id="goodslist" border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
                      <tbody>
                        <tr class="top">
                        <th width="30%"><div style="width:100px;"><%=Lang_Order(55)%><!--单据日期--></div></th>
                        <th width="20%"><div style="width:60px; "><%=Lang_RefundMoney%></div></th>
                        <th width="30%"><div style="width:100px;"><%=Lang_Order(57)%><!--退款方式--></div></th>
                        <th width="20%"><div style="width:60px; "><%=Lang_Status%><!--状态--></div></th>
                        </tr>
                        <% Echo OrderRefundBillList(Pv_OrderID) %>
                      </tbody>
                    </table>
                </div>
                </td>
              </tr>
            </table>
          </div>
          <div class="obox">
            <div class="tit"><%=Lang_Order(58)%><!--收发货记录--></div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td valign="top" width="50%">
                <div class="inbox">
                    <div class="tit"><%=Lang_Order(59)%><!--发货单据列表--></div>
                    <table id="goodslist" border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
                      <tbody>
                        <tr class="top">
                        <th width="22%"><div style="width:60px;"><%=Lang_Order(55)%><!--单据日期--></div></th>
                        <th width="24%"><div style="width:60px;"><%=Lang_ShipBillID%></div></th>
                        <th width="20%"><div style="width:60px;"><%=Lang_Logi_NO%><!--物流单号--></div></th>
                        <th width="20%"><div style="width:60px;"><%=Lang_Order(15)%><!--配送方式--></div></th>
                        <th width="14%"><div style="width:60px;"><%=Lang_Order(60)%><!--收货人--></div></th>
                        </tr>
                        <% Echo OrderShipBillList("shipbill",Pv_OrderID) %>
                      </tbody>
                    </table>
                </div>
                </td>
                <td valign="top" width="50%">
                <div class="inbox">
                    <div class="tit"><%=Lang_Order(61)%><!--退货单据列表--></div>
                    <table id="goodslist" border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
                      <tbody>
                        <tr class="top">
                        <th width="22%"><div style="width:60px;"><%=Lang_Order(55)%><!--单据日期--></div></th>
                        <th width="24%"><div style="width:60px;"><%=Lang_ReShipBillID%></div></th>
                        <th width="20%"><div style="width:60px;"><%=Lang_Logi_NO%><!--物流单号--></div></th>
                        <th width="20%"><div style="width:60px;"><%=Lang_Order(15)%><!--配送方式--></div></th>
                        <th width="14%"><div style="width:60px;"><%=Lang_Order(62)%><!--退货人--></div></th>
                        </tr>
                        <% Echo OrderShipBillList("reshipbill",Pv_OrderID) %>
                      </tbody>
                    </table>
                </div>
                </td>
              </tr>
            </table>
          </div>
          <div class="obox">
            <div class="tit"><%=Lang_Order(63)%><!--订单日志--></div>
            <table id="goodslist" border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
              <tbody>
                <tr class="top">
                <th width="6% "><div style="width:40px; "><%=Lang_Order(64)%><!--序号--></div></th>
                <th width="20%"><div style="width:130px;"><%=Lang_Order(65)%><!--时间--></div></th>
                <th width="10%"><div style="width:100px;"><%=Lang_Order(66)%><!--操作人--></div></th>
                <th width="10%"><div style="width:100px;"><%=Lang_Order(67)%><!--行为--></div></th>
                <th width="10%"><div style="width:100px;"><%=Lang_Order(68)%><!--结果--></div></th>
                <th width="44%"><div style="width:100px;"><%=Lang_Remarks%></div></th>
                </tr>
                <% Echo OrderLogList(Pv_OrderID) %>
              </tbody>
            </table>
          </div>
      </DIV>
    </DIV>
    <form name="FormForReload" style="display:none;" method="post"></form>
<%
	End Function
	Private Function OrderPay()
%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
          <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
          <div class="obox">
            <div class="inbox mr5">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><%=Lang_OrderID%>:</td><td class="infotd"><% Echo Pv_OrderID %></td>
                    <td class="titletd"><%=Lang_Order(13)%>:</td><td class="infotd"><% Echo Pv_Createtime %></td>
                </tr>
                <tr><td class="titletd"><%=Lang_Order_ListTableColLine(23)%><!--需支付总额-->:</td><td class="infotd"><b class="font14 redh"><% Echo MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Final_Amount) %></b></td>
                    <td class="titletd"><%=Lang_HavePayedMoney%>:</td><td class="infotd"><% Echo MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Payed) %></td>
                </tr>
                <tr><td class="titletd"><%=Lang_Order(69)%><!--收款银行-->:</td><td class="infotd" colspan="3">
                    <input type="text" class="TxtClass" name="bank" id="bank" value="" >
                    <select name="selectaccount" id="selectaccount">
                    <% Echo GetBankCountAsOption("","") %>
                    </select>
                    </td></tr>
                <tr><td class="titletd"><%=Lang_Order(70)%><!--收款帐号-->:</td><td class="infotd" colspan="3">
                    <input type="text" class="TxtClass" name="account" id="account" value="" ><label id="d_account"></label>
                    </td></tr>
                <tr><td class="titletd"><%=Lang_Order(71)%><!--收款金额-->:</td><td class="infotd" colspan="3"><input type="text" class="TxtClass" name="money" id="money" value="<% Echo KnifeCMS.Data.FormatCurrency(Pv_Final_Amount-Pv_Payed) %>" ></td></tr>
                <tr><td class="titletd"><%=Lang_Order(72)%><!--付款类型-->:</td><td class="infotd" colspan="3">
                    <select name="paytype" id="paytype">
                    <% Echo OS.CreateOption(Array("0:"& Lang_PleaseSelect &"","alipay:"& Lang_Payment_Alipay(0) &"","offline:"& Lang_Payment_OffLine(0) &"","cod:"& Lang_Payment_COD(0)),Split(0,",")) %>
                    </select><label id="d_paytype"></label>
                    </td></tr>
                <tr><td class="titletd"><%=Lang_Payment%>:</td><td class="infotd" colspan="3">
                    <select id="paymentid" name="paymentid"><% Echo Admin.GetPaymentsAsOption(0,0) %></select><label id="d_paymentid"></label>
                    </td></tr>
                <tr><td class="titletd"><%=Lang_Order(73)%><!--付款人-->:</td><td class="infotd" colspan="3"><input type="text" class="TxtClass" name="payaccount" id="payaccount" value="" ></td></tr>
                
                <tr><td class="titletd"><%=Lang_Order(52)%><!--是否开票-->:</td><td class="infotd"><% Echo KnifeCMS.IIF(Pv_IsTax=1,Lang_Yes,Lang_No) %></td>
                    <td class="titletd"><%=Lang_Order(19)%><!--税金-->:</td><td class="infotd"><% Echo MoneySb & Pv_Cost_Tax %></td>
                </tr>
                <tr><td class="titletd"><%=Lang_Order(20)%><!--发票抬头-->:</td><td class="infotd" colspan="3"><% Echo Pv_Tax_Company %></td></tr>
                <tr><td class="titletd"><%=Lang_Order(74)%><!--收款单备注-->:</td><td class="infotd" colspan="3"><textarea id="remarks" name="remarks" class="remark"></textarea></td></tr>
                
                </table>
            </div>
          </div>
          <input type="hidden" name="id" value="<% Echo ID %>" />
          <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
          <input type="hidden" name="orderid" id="orderid" value="<% Echo Pv_OrderID %>" />
          <div class="bottomSaveline" id="SubmitButtoms" style="text-align:center;">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Order.SaveOrderPayCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
          </div>
          </form>
      </DIV>
    </DIV>
    <script type="text/javascript">
	SelectAccount("selectaccount","bank","account");
    </script>
<%
	End Function
	Private Function OrderRefund()
%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
          <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
          <div class="obox">
            <div class="inbox mr5">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><%=Lang_OrderID%>:</td><td class="infotd"><% Echo Pv_OrderID %></td>
                    <td class="titletd"><%=Lang_Order(13)%>:</td><td class="infotd"><% Echo Pv_Createtime %></td>
                </tr>
                <tr><td class="titletd"><%=Lang_Order(26)%><!--订单总金额-->:</td><td class="infotd"><b class="font14 redh"><% Echo MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Total_Amount) %></b></td>
                    <td class="titletd"><%=Lang_HavePayedMoney%>:</td><td class="infotd"><% Echo MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Payed) %></td>
                </tr>
                <tr><td class="titletd"><%=Lang_Order(75)%><!--退款银行-->:</td><td class="infotd" colspan="3">
                    <input type="text" class="TxtClass" name="bank" id="bank" value="" >
                    <select name="selectaccount" id="selectaccount">
                    <% Echo GetBankCountAsOption("","") %>
                    </select>
                    </td></tr>
                <tr><td class="titletd"><%=Lang_Order(76)%><!--退款帐号-->:</td><td class="infotd" colspan="3"><input type="text" class="TxtClass" name="account" id="account" value="" ></td></tr>
                <tr><td class="titletd"><%=Lang_RefundMoney%>:</td><td class="infotd" colspan="3"><input type="text" class="TxtClass" name="money" id="money" value="<% Echo KnifeCMS.Data.FormatCurrency(Pv_Payed) %>" ></td></tr>
                <tr><td class="titletd"><%=Lang_Order(77)%><!--退款类型-->:</td><td class="infotd" colspan="3">
                    <select name="paytype" id="paytype">
                    <% Echo OS.CreateOption(Array("0:"& Lang_PleaseSelect &"","alipay:"& Lang_Payment_Alipay(0) &"","offline:"& Lang_Payment_OffLine(0) &"","cod:"& Lang_Payment_COD(0)),Split(0,",")) %>
                    </select>
                    <label id="d_paytype"></label>
                    </td></tr>
                <tr><td class="titletd"><%=Lang_Order(78)%><!--退付方式-->:</td><td class="infotd" colspan="3">
                    <select name="paymentid" id="paymentid"><% Echo Admin.GetPaymentsAsOption(0,0) %></select><label id="d_paymentid"></label>
                    </td></tr>
                <tr><td class="titletd"><%=Lang_Order(79)%><!--收款人-->:</td><td class="infotd" colspan="3"><input type="text" class="TxtClass" name="payaccount" id="payaccount" value="" ></td></tr>
                <tr><td class="titletd"><%=Lang_Order(80)%><!--退款单备注-->:</td><td class="infotd" colspan="3"><textarea id="remarks" name="remarks" class="remark"></textarea></td></tr>
                
                </table>
            </div>
          </div>
          <input type="hidden" name="id" value="<% Echo ID %>" />
          <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
          <input type="hidden" name="orderid" id="orderid" value="<% Echo Pv_OrderID %>" />
          <div class="bottomSaveline" id="SubmitButtoms" style="text-align:center;">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Order.SaveOrderRefundCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
          </div>
          </form>
      </DIV>
    </DIV>
    <script type="text/javascript">
	SelectAccount("selectaccount","bank","account");
    </script>
<%
	End Function
	Private Function OrderShip()
%>
    <script language="javascript" src="../data/javascript/region.json.js"></script>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
          <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
          <div class="obox">
            <div class="inbox">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><%=Lang_OrderID%>:</td><td class="infotd"><% Echo Pv_OrderID %></td><td class="titletd"><%=Lang_Order(13)%>:</td><td class="infotd"><% Echo Pv_Createtime %></td></tr>
                <tr><td class="titletd"><%=Lang_Order(15)%><!--配送方式-->:</td><td class="infotd">
                    <select name="shippingid" id="shippingid">
					<% Echo Admin.GetDeliveryAsOption(0,Pv_ShippingID) %>
                    </select><label id="d_shippingid"></label></td>
                    <td class="titletd"><%=Lang_Order(48)%><!--配送费用-->:</td><td class="infotd"><% Echo Pv_Cost_Freight %></td>
                </tr>
                <tr><td class="titletd"><%=Lang_Order(81)%><!--是否保价-->:</td><td class="infotd" colspan="3"><% Echo KnifeCMS.IIF(Pv_IsProtect=1,Lang_Yes,Lang_No) %></td></tr>
                <tr><td class="titletd"><%=Lang_DeliveryCorp%><!--物流公司-->:</td><td class="infotd">
                    <select name="logi_id" id="logi_id">
                    <% Echo Admin.GetDeliveryCorpAsOption(0,Pv_DeliveryCorpID) %>
                    </select><label id="d_logi_id"></label></td>
                    <td class="titletd"><%=Lang_Logi_NO%><!--物流单号-->:</td><td class="infotd"><input type="text" class="TxtClass" name="logi_no" id="logi_no" style="width:150px;" value="" ></td>
                    </tr>
                <tr><td class="titletd"><%=Lang_Order(82)%><!--物流费用-->:</td><td class="infotd">
                    <input type="text" class="TxtClass" name="cost_freight" id="cost_freight" style="width:150px;" value="<% Echo Pv_Cost_Freight %>" ></td>
                    <td class="titletd"><%=Lang_Order(81)%><!--是否保价-->:</td><td class="infotd">是<input type="radio" name="isprotect" <% Echo KnifeCMS.IIF(Pv_IsProtect=1,"checked=""checked""","") %> value="1"> 否<input type="radio" name="isprotect" <% Echo KnifeCMS.IIF(Pv_IsProtect=0,"checked=""checked""","") %> value="0"></td>
                    </tr>
                <tr><td class="titletd"><%=Lang_Order(17)%><!--保价费-->:</td><td class="infotd" colspan="3">
                    <input type="text" class="TxtClass" name="cost_protect" id="cost_protect" style="width:150px;" value="<% Echo Pv_Cost_Protect %>" ></td>
                    </tr>
                <tr class="Separated"><td class="titletd" colspan="2"><div></div></td><td class="infotd" colspan="2"><div></div></td></tr>
                <tr><td class="titletd"><%=Lang_Ship_Name%>:</td><td class="infotd">
                    <input type="text" class="TxtClass" name="ship_name" id="ship_name" style="width:150px;" value="<% Echo Pv_Ship_Name %>" /><label id="d_ship_name"></label></td>
                    <td class="titletd"><%=Lang_Ship_Email%>:</td><td class="infotd">
                    <input type="text" class="TxtClass" name="ship_email" id="ship_email" style="width:150px;" value="<% Echo Pv_Ship_Email %>" /></td></tr>
                <tr><td class="titletd"><%=Lang_Ship_Mobile%>:</td><td class="infotd">
                    <input type="text" class="TxtClass" name="ship_mobile" id="ship_mobile" style="width:150px;" value="<% Echo Pv_Ship_Mobile %>" /></td>
                    <td class="titletd"><%=Lang_Ship_Tel%>:</td><td class="infotd">
                    <input type="text" class="TxtClass" name="ship_tel" id="ship_tel" style="width:150px;" value="<% Echo Pv_Ship_Tel %>" /></td></tr>
                <tr><td class="titletd"><%=Lang_Ship_Zip%>:</td><td class="infotd" colspan="3">
                    <input type="text" class="TxtClass" name="ship_zip" id="ship_zip" style="width:60px;" value="<% Echo Pv_Ship_Zip %>" /></td>
                    </tr>
                <tr><td class="titletd"><%=Lang_Ship_Area%>:</td><td class="infotd" colspan="3">
                    <span id="ship_area"></span>
                    <script type="text/javascript">
                    CreateAddressAsSelect(KnifeCMS.$id("ship_area"),",","<% Echo Pv_Ship_Area %>","ship_area",false);
                    </script></td></tr>
                <tr><td class="titletd"><%=Lang_Ship_Address%>:</td><td class="infotd" colspan="3">
                    <input type="text" class="TxtClass" name="ship_address" id="ship_address" style="width:300px;" value="<% Echo Pv_Ship_Address %>" /><label id="d_ship_address"></label></td></tr>
                <tr><td class="titletd"><%=Lang_Remarks%>:</td><td class="infotd" colspan="3"><textarea id="remarks" name="remarks" class="remark"></textarea></td></tr>
              </table>
            </div>
            <div class="inbox">
                <table id="goodslist" border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
                 <tbody>
                  <tr class="top">
                  <th width="8%" ><div style="width:154px;"><% Echo Lang_Order_ListTableColLine(11) %><!--商品编号--></div></th>
                  <th width="62%"><div style="width:260px;"><% Echo Lang_Order_ListTableColLine(12) %><!--商品名称--></div></th>
                  <th width="2%" ><div style="width:84px; "><% Echo Lang_Order_ListTableColLine(17) %><!--库存--></div></th>
                  <th width="10%"><div style="width:60px; "><% Echo Lang_Order_ListTableColLine(18) %><!--购买数量--></div></th>
                  <th width="10%"><div style="width:68px; "><% Echo Lang_Order_ListTableColLine(19) %><!--已发货数量--></div></th>
                  <th width="10%"><div style="width:80px; "><% Echo Lang_Order_ListTableColLine(20) %><!--此单发货数量--></div></th>
                  </tr>
                  <% Call Admin.OrderGoodsList(Pv_OrderID,"ship") %>
                 </tbody>
                </table>
                <script type="text/javascript">
                TrBgChange("goodslist","oliver","data");
                </script>
            </div>
          </div>
          <input type="hidden" name="id" value="<% Echo ID %>" />
          <input type="hidden" name="orderid" id="orderid" value="<% Echo Pv_OrderID %>" />
          <div class="bottomSaveline" id="SubmitButtoms" style="text-align:center;">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Order.SaveOrderShipCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
          </div>
          </form>
      </DIV>
    </DIV>
    <script type="text/javascript">
	SelectAccount("selectaccount","bank","account");
    </script>
<%
	End Function
	Private Function OrderReShip()
%>
    <script language="javascript" src="../data/javascript/region.json.js"></script>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
          <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
          <div class="obox">
            <div class="inbox">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><%=Lang_OrderID%>:</td><td class="infotd"><% Echo Pv_OrderID %></td><td class="titletd"><%=Lang_Order(13)%>:</td><td class="infotd"><% Echo Pv_Createtime %></td></tr>
                <tr>
                    <td class="titletd"><%=Lang_Order(84)%><!--退货原因-->:</td><td class="infotd">
                    <select name="reason" id="reason">
                    <% Echo OS.CreateOption(Array(""& Lang_Order(85) &":"& Lang_Order(85) &"",""& Lang_Order(86) &":"& Lang_Order(86) &"",""& Lang_Order(87) &":"& Lang_Order(87) &""),Split("","")) %>
                    </select></td>
                    <td class="titletd"><%=Lang_Order(15)%><!--配送方式-->:</td><td class="infotd">
                    <select name="shippingid" id="shippingid">
					<% Echo Admin.GetDeliveryAsOption(0,Pv_ShippingID) %>
                    </select><label id="d_shippingid"></label></td>
                </tr>
                <tr><td class="titletd"><%=Lang_DeliveryCorp%><!--物流公司-->:</td><td class="infotd">
                    <select name="logi_id" id="logi_id">
                    <% Echo Admin.GetDeliveryCorpAsOption(0,Pv_DeliveryCorpID) %>
                    </select><label id="d_logi_id"></label></td>
                    <td class="titletd"><%=Lang_Logi_NO%><!--物流单号-->:</td><td class="infotd"><input type="text" class="TxtClass" name="logi_no" id="logi_no" style="width:150px;" value="" ></td>
                    </tr>
                <tr><td class="titletd"><%=Lang_Order(82)%><!--物流费用-->:</td><td class="infotd">
                    <input type="text" class="TxtClass" name="cost_freight" id="cost_freight
                    " style="width:150px;" value="<% Echo Pv_Cost_Freight %>" ></td>
                    <td class="titletd"><%=Lang_Order(83)%><!--实际保价-->:</td><td class="infotd"><%=Lang_Yes%><!--是--><input type="radio" name="isprotect" <% Echo KnifeCMS.IIF(Pv_IsProtect=1,"checked=""checked""","") %> value="1"> ><%=Lang_No%><!--否--><input type="radio" name="isprotect" <% Echo KnifeCMS.IIF(Pv_IsProtect=0,"checked=""checked""","") %> value="0"></td>
                    </tr>
                <tr class="Separated"><td class="titletd" colspan="2"><div></div></td><td class="infotd" colspan="2"><div></div></td></tr>
                <tr><td class="titletd"><%=Lang_Order(88)%><!--退货人姓名-->:</td><td class="infotd">
                    <input type="text" class="TxtClass" name="ship_name" id="ship_name" style="width:150px;" value="<% Echo Pv_Ship_Name %>" /><label id="d_ship_name"></label></td>
                    <td class="titletd"><%=Lang_Order(89)%><!--退货人Email-->:</td><td class="infotd">
                    <input type="text" class="TxtClass" name="ship_email" id="ship_email" style="width:150px;" value="<% Echo Pv_Ship_Email %>" /></td></tr>
                <tr><td class="titletd"><%=Lang_Ship_Mobile%>:</td><td class="infotd">
                    <input type="text" class="TxtClass" name="ship_mobile" id="ship_mobile" style="width:150px;" value="<% Echo Pv_Ship_Mobile %>" /></td>
                    <td class="titletd"><%=Lang_Ship_Tel%>:</td><td class="infotd">
                    <input type="text" class="TxtClass" name="ship_tel" id="ship_tel" style="width:150px;" value="<% Echo Pv_Ship_Tel %>" /></td></tr>
                <tr><td class="titletd"><%=Lang_Ship_Zip%>:</td><td class="infotd" colspan="3">
                    <input type="text" class="TxtClass" name="ship_zip" id="ship_zip" style="width:60px;" value="<% Echo Pv_Ship_Zip %>" /></td>
                    </tr>
                <tr><td class="titletd"><%=Lang_Order(90)%><!--退货地区-->:</td><td class="infotd" colspan="3">
                    <span id="ship_area"></span>
                    <script type="text/javascript">
                    CreateAddressAsSelect(KnifeCMS.$id("ship_area"),",","<% Echo Pv_Ship_Area %>","ship_area",false);
                    </script></td></tr>
                <tr><td class="titletd"><%=Lang_Ship_Address%>:</td><td class="infotd" colspan="3">
                    <input type="text" class="TxtClass" name="ship_address" id="ship_address" style="width:300px;" value="<% Echo Pv_Ship_Address %>" /><label id="d_ship_address"></label></td></tr>
                <tr><td class="titletd"><%=Lang_Remarks%>:</td><td class="infotd" colspan="3"><textarea id="remarks" name="remarks" class="remark"></textarea></td></tr>
              </table>
            </div>
            <div class="inbox">
                <table id="goodslist" border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
                 <tbody>
                  <tr class="top">
                  <th width="8%" ><div style="width:154px;"><% Echo Lang_Order_ListTableColLine(11) %><!--商品编号--></div></th>
                  <th width="62%"><div style="width:260px;"><% Echo Lang_Order_ListTableColLine(12) %><!--商品名称--></div></th>
                  <th width="10%"><div style="width:60px; "><% Echo Lang_Order_ListTableColLine(18) %><!--购买数量--></div></th>
                  <th width="10%"><div style="width:68px; "><% Echo Lang_Order_ListTableColLine(19) %><!--已发货数量--></div></th>
                  <th width="10%"><div style="width:80px; "><% Echo Lang_Order_ListTableColLine(21) %><!--此单退货数量--></div></th>
                  </tr>
                  <% Call Admin.OrderGoodsList(Pv_OrderID,"reship") %>
                 </tbody>
                </table>
                <script type="text/javascript">
                TrBgChange("goodslist","oliver","data");
                </script>
            </div>
          </div>
          <input type="hidden" name="id" value="<% Echo ID %>" />
          <input type="hidden" name="orderid" id="orderid" value="<% Echo Pv_OrderID %>" />
          <div class="bottomSaveline" id="SubmitButtoms" style="text-align:center;">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Order.SaveOrderReShipCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
          </div>
          </form>
      </DIV>
    </DIV>
    <script type="text/javascript">
	SelectAccount("selectaccount","bank","account");
    </script>
<%
	End Function
End Class
%>