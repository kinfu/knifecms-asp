<!--#include file="config.admin.asp"-->
<%
Dim GoodsModel
Set GoodsModel = New Class_KnifeCMS_Admin_GoodsModel
Set GoodsModel = Nothing
Class Class_KnifeCMS_Admin_GoodsModel

	Private GoodsModelID,GoodsModelName,OrderNum,Remarks,AddTime
	Private AttrName,AttrOrder,IsLinked,AttrType,AttrInputType,AttrValues
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "goods.lang.asp"
		IsContentExist = False
		GoodsModelID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","goodsmodelid"))
		Header()
		If Ctl = "goodsmodel" Then
			Select Case Action
				Case "view"
					GoodsModelView
				Case "add","edit"
					If SubAction="save" Then
						GoodsModelDoSave
					Else
						If Action = "add" Then
							GoodsModelDo
						ElseIf Action = "edit" Then
							Set Rs = KnifeCMS.DB.GetRecord(DBTable_GoodsModel&":SysID,ModelName,OrderNum,Remarks",Array("ID:"&ID),"")
							If Not(Rs.Eof) Then
								IsContentExist = True
								SysID          = Rs("SysID")
								GoodsModelName = Rs("ModelName")
								OrderNum       = Rs("OrderNum")
								Remarks        = Rs("Remarks")
							End IF
							KnifeCMS.DB.CloseRs Rs
							If IsContentExist Then GoodsModelDo : Else Admin.ShowMessage Lang_GoodsModel_Cue(7),"goback",0
						End IF
					End IF
				Case "del"  : GoodsModelDel
			End Select
		ElseIf Ctl = "attribute" Then
			Select Case Action
				Case "view"
					AttributeView
				Case "add","edit"
					If SubAction="save" Then
						AttributeDoSave
					Else
						If Action = "add" Then
							AttributeDo
						ElseIf Action = "edit" Then
							Set Rs = KnifeCMS.DB.GetRecord(DBTable_GoodsModelAttribute&":AttrName,GoodsModelID,AttrOrder,IsLinked,AttrType,AttrInputType,AttrValues",Array("ID:"&ID),"")
							If Not(Rs.Eof) Then
								IsContentExist = True
								AttrName     = Rs("AttrName")
								GoodsModelID = Rs("GoodsModelID")
								AttrOrder    = Rs("AttrOrder")
								IsLinked     = Rs("IsLinked")
								AttrType     = Rs("AttrType")
								AttrInputType= Rs("AttrInputType")
								AttrValues   = Rs("AttrValues")
							End IF
							KnifeCMS.DB.CloseRs Rs
							If IsContentExist Then AttributeDo : Else Admin.ShowMessage Lang_Attribute_Cue(7),"goback",0
						End IF
					End IF
				Case "del"  : AttributeDel
			End Select
		End If
		Footer()
	End Sub
	
	'$:商品类型列表
	Private Function GoodsModelList()
		Dim Fn_TempSql
		Fn_TempSql="SELECT a.ID,a.OrderNum,a.ModelName,(select count(*) From ["& DBTable_GoodsModelAttribute &"] b where b.GoodsModelID=a.ID),a.Remarks FROM ["& DBTable_GoodsModel &"] a ORDER BY a.OrderNum ASC,a.ID ASC"
		Operation = "<span class=""sysBtn""><a href=""javascript:;"" onClick=""ShowModal('?ctl="&Ctl&"&act=edit&id={$ID}',840,450); ShowModalReload();"" ><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		Operation = Operation & "<span class=""sysBtn""><a href=""javascript:;"" onClick=""ShowModal('?ctl=attribute&act=view&goodsmodelid={$ID}',920,560); ShowModalReload();""><b class=""icon icon_view""></b>"& Lang_Attribute(0) &"</a></span>"
		Call Admin.DataList(Fn_TempSql,20,5,Operation,PageUrlPara)
	End Function
	
	'$:商品类型属性列表
	Private Function AttributeList()
		Dim Fn_TempSql
		Fn_TempSql = "SELECT a.ID,a.AttrName,b.ModelName,a.AttrOrder,a.IsLinked,a.AttrType,a.AttrInputType,a.AttrValues,a.AddTime FROM ["& DBTable_GoodsModelAttribute &"] a LEFT JOIN ["& DBTable_GoodsModel &"] b ON (b.ID=a.GoodsModelID) WHERE a.GoodsModelID="&GoodsModelID&" ORDER BY a.AttrOrder ASC,a.ID ASC"
		Operation = "<span class=""sysBtn""><a href=""javascript:;"" onClick=""ShowModal('?ctl="&Ctl&"&act=edit&id={$ID}',840,480); ShowModalReload();"" ><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		Call Admin.DataList(Fn_TempSql,20,9,Operation,PageUrlPara)
	End Function
	
	Function GoodsModelDoSave()
		'获取数据并处理数据
		GoodsModelName = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","GoodsModelName")),250)
		OrderNum       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","OrderNum"))
		Remarks        = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Remarks")),250)
		'数据合法性检测并处理数据
		If KnifeCMS.Data.IsNul(GoodsModelName) Then ErrMsg =Lang_GoodsModel_Cue(8) &"<br>"
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		'开始保存数据操作
		IF Action="add" Then
			Result = KnifeCMS.DB.AddRecord(DBTable_GoodsModel,Array("SysID:"&SysID,"ModelName:"& GoodsModelName,"OrderNum:"& OrderNum,"Remarks:"& Remarks,"AddTime:"& SysTime))
			ID  = Admin.GetIDBySysID(DBTable_GoodsModel,SysID)
			Msg = Lang_GoodsModel_Cue(1) &"<br>["& Lang_GoodsModel(1) &":"& GoodsModelName &"]<br>[ID:"& ID &"]"
			If Result Then Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		ElseIf Action="edit" then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_GoodsModel,Array("ModelName:"& GoodsModelName,"OrderNum:"& OrderNum,"Remarks:"& Remarks),Array("ID:"&ID))
			Msg = Lang_GoodsModel_Cue(2) &"<br>["& Lang_GoodsModel(1) &":"& GoodsModelName &"]<br>[ID:"& ID &"]"
			If Result Then Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		End IF
	End Function
	
	Private Function AttributeDoSave()
		Dim Fn_Arr,Fn_i
		'获取数据并处理数据
		AttrName      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","AttrName")),250)
		AttrOrder     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","AttrOrder"))
		IsLinked      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","IsLinked"))
		AttrType      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","AttrType"))
		AttrInputType = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","AttrInputType"))
		AttrValues    = KnifeCMS.GetForm("post","AttrValues")
		If Not(KnifeCMS.Data.IsNul(AttrValues)) Then
			AttrValues=Replace(AttrValues,chr(13),"\n")
			Fn_Arr    =KnifeCMS.Data.ClearSameDatas(split(AttrValues,"\n"),0)
			AttrValues=""
			For Fn_i=0 to Ubound(Fn_Arr)
				If Not(KnifeCMS.Data.IsNul(Fn_Arr(Fn_i))) Then AttrValues = KnifeCMS.IIF(KnifeCMS.Data.IsNul(AttrValues), Trim(Fn_Arr(Fn_i)), Trim(AttrValues) &"\n"& Trim(Fn_Arr(Fn_i)))
			Next
		End If
		'数据合法性检测并处理数据
		If KnifeCMS.Data.IsNul(AttrName)     Then ErrMsg = Lang_Attribute_Cue(8) &"<br>"
		If GoodsModelID < 1                 Then ErrMsg = ErrMsg & Lang_Attribute_Cue(9) &"<br>"
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		'开始保存数据操作
		IF Action="add" Then
			Result = KnifeCMS.DB.AddRecord(DBTable_GoodsModelAttribute,Array("SysID:"&SysID,"GoodsModelID:"& GoodsModelID,"AttrName:"& AttrName,"AttrOrder:"& AttrOrder,"IsLinked:"&IsLinked,"AttrType:"& AttrType,"AttrInputType:"& AttrInputType,"AttrValues:"& AttrValues,"AddTime:"& SysTime))
			ID  = Admin.GetIDBySysID(DBTable_GoodsModelAttribute,SysID)
			Msg = Lang_Attribute_Cue(1) &"<br>["& Lang_Attribute(1) &":"& AttrName &"]<br>["& Lang_Attribute(2) &"ID:"& GoodsModelID &"]<br>[ID:"& ID &"]"
			If Result Then Admin.ReloadTopDialog : Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		ElseIf Action="edit" then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_GoodsModelAttribute,Array("GoodsModelID:"& GoodsModelID,"AttrName:"& AttrName,"AttrOrder:"& AttrOrder,"IsLinked:"&IsLinked,"AttrType:"& AttrType,"AttrInputType:"& AttrInputType,"AttrValues:"& AttrValues),Array("ID:"&ID))
			Msg = Lang_Attribute_Cue(2) &"<br>["& Lang_Attribute(1) &":"& AttrName &"]<br>["& Lang_Attribute(2) &"ID:"& GoodsModelID &"]<br>[ID:"& ID &"]"
			If Result Then Admin.ReloadTopDialog : Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		End IF
	End Function
	
	Private Function GoodsModelDel()
		ID = KnifeCMS.GetForm("post","InputName")
		If KnifeCMS.Data.IsNul(Replace(ID,",","")) Then Exit Function
		Dim Fn_ArrID,Fn_ID,Fn_i,Fn_Rs
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			If ID > 0 Then
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_GoodsModel&":ModelName",Array("ID:"&ID),"")
				If Not(Fn_Rs.Eof) Then GoodsModelName=Fn_Rs(0)
				KnifeCMS.DB.CloseRs Fn_Rs
				Result = KnifeCMS.DB.Execute("DELETE FROM ["&DBTable_GoodsAttr&"] WHERE AttrID IN (SELECT ID FROM ["&DBTable_GoodsModelAttribute&"] WHERE GoodsModelID = "&ID&")")
				Result = KnifeCMS.DB.DeleteRecord(DBTable_GoodsModelAttribute,Array("GoodsModelID:"&ID))
				Result = KnifeCMS.DB.DeleteRecord(DBTable_GoodsModel,Array("ID:"&ID))
				Msg    = Msg & Lang_GoodsModel_Cue(3) &"{"& GoodsModelName &"[ID="& ID &"]}<br>"
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Function
	
	Private Function AttributeDel()
		ID = KnifeCMS.GetForm("post","InputName")
		If KnifeCMS.Data.IsNul(Replace(ID,",","")) Then Exit Function
		Dim Fn_ArrID,Fn_ID,Fn_i,Fn_Rs
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			If ID > 0 Then
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_GoodsModelAttribute&":AttrName",Array("ID:"&ID),"")
				If Not(Fn_Rs.Eof) Then AttrName=Fn_Rs(0)
				KnifeCMS.DB.CloseRs Fn_Rs
				Result = KnifeCMS.DB.DeleteRecord(DBTable_GoodsAttr,Array("AttrID:"&ID))
				Result = KnifeCMS.DB.DeleteRecord(DBTable_GoodsModelAttribute,Array("ID:"&ID))
				Msg    = Msg & Lang_Attribute_Cue(3) &"{"& AttrName &"[ID="& ID &"]}<br>"
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Function
	
	Private Function GoodsModelView()
%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <span class="sysBtn"><a href="javascript:ShowModal('?ctl=<% Echo Ctl %>&act=add',840,450);ShowModalReload();"><b class="icon icon_write"></b><% Echo Lang_DoAdd %></a></span>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','<% Echo Lang_GoodsModel_Cue(10) & Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDel%></button>
				  </td></tr>
			  </table>
		  </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<% Echo  KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th width="1%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
			<th width="5%"><div style="width:32px;"><% Echo Lang_GoodsModel_ListTableColLine(0) %><!--排序--></div></th>
            <th width="20%"><div style="width:200px;"><% Echo Lang_GoodsModel_ListTableColLine(1) %><!--商品类型名称--></div></th>
            <th width="5%"><div style="width:60px"><% Echo Lang_GoodsModel_ListTableColLine(2) %><!--属性数量--></div></th>
            <th width="40%"><div style="width:250px"><% Echo Lang_ListTableColLine_Remarks %><!--备注--></div></th>
			<th><div style="width:60px;"><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% GoodsModelList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
	<script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	</script>
<%
	End Function
	Private Function AttributeView()
%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <span class="sysBtn"><a href="javascript:ShowModal('?ctl=<% Echo Ctl %>&act=add&goodsmodelid=<% Echo GoodsModelID %>',840,480);ShowModalReload();"><b class="icon icon_write"></b><% Echo Lang_DoAdd %></a></span>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','<% Echo Lang_Attribute_Cue(11) & Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDel%></button>
				  </td></tr>
			  </table>
		  </td>
          
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<% Echo  KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th width="1%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %></div></th><!--系统ID-->
			<th><div style="width:100px;"><% Echo Lang_Attribute_ListTableColLine(0) %><!--属性名称--></div></th>
            <th><div style="width:80px"><% Echo Lang_Attribute_ListTableColLine(1) %><!--所属商品类型--></div></th>
            <th><div style="width:30px"><% Echo Lang_Attribute_ListTableColLine(2) %><!--排序--></div></th>
            <th><div style="width:60px;"><% Echo Lang_Attribute_ListTableColLine(3) %><!--是否关联--></div></th>
            <th><div style="width:60px;"><% Echo Lang_Attribute_ListTableColLine(4) %><!--是否可选--></div></th>
            <th><div style="width:62px;"><% Echo Lang_Attribute_ListTableColLine(5) %><!--录入方式--></div></th>
            <th><div style="width:80px;"><% Echo Lang_Attribute_ListTableColLine(6) %><!--可选值列表--></div></th>
            <th width="13%"><div style="width:60px;"><% Echo Lang_ListTableColLine_AddTime %><!--添加时间--></div></th>
			<th><div style="width:60px;"><% Echo Lang_ListTableColLine_Do %></div></th><!--操作-->
			</tr>
			<% AttributeList() %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
    <form name="FormForReload" style="display:none;" method="post"></form>
	<script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td4" && tds[i].innerHTML=="1"){
					tds[i].innerHTML=Lang_Js_Yes;
				}
				if(tds[i].id=="td4" && tds[i].innerHTML=="0"){
					tds[i].innerHTML=Lang_Js_No;
				}
				if(tds[i].id=="td5" && tds[i].innerHTML=="0"){
					tds[i].innerHTML=Js_Goods[16];
				}
				if(tds[i].id=="td5" && tds[i].innerHTML=="1"){
					tds[i].innerHTML=Js_Goods[17]//'单选属性';
				}
				if(tds[i].id=="td5" && tds[i].innerHTML=="2"){
					tds[i].innerHTML=Js_Goods[18];
				}
				if(tds[i].id=="td6" && tds[i].innerHTML=="0"){
					tds[i].innerHTML=Js_Goods[19];
				}
				if(tds[i].id=="td6" && tds[i].innerHTML=="1"){
					tds[i].innerHTML=Js_Goods[20];
				}
				if(tds[i].id=="td6" && tds[i].innerHTML=="2"){
					tds[i].innerHTML=Js_Goods[21];
				}
				if(tds[i].id=="td7"){
					TempHTML=tds[i].innerHTML;
					re = /\\n/g;
					tds[i].innerHTML=TempHTML.replace(re,'<br>')
				}
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
	Private Function GoodsModelDo
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
          <div class="inbox">
            <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable" id="Tabs_1">
              <tbody>
               <tr><td class="titletd"><% Echo Lang_GoodsModel(1) %><!--商品类型名称-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="GoodsModelName" name="GoodsModelName" value="<% Echo GoodsModelName%>" />
                   <label id="d_GoodsModelName"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_GoodsModel(2) %><!--排序-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="OrderNum" name="OrderNum" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9]*/g','maxLength:5'))" style="width:50px;" value="<% Echo OrderNum%>" />
                   <label class="Normal"><% Echo Lang_GoodsModel_Cue(9) %><!--数字越小越靠前--></label>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_GoodsModel(3) %><!--备注-->:</td>
                   <td class="infotd">
                   <textarea class="remark" name="Remarks" id="Remarks"><% Echo Remarks%></textarea>
                   </td>
               </tr>
              </tbody>
            </table>
            <input type="hidden" name="id" value="<% Echo ID %>" />
            <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
            <div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Goods.SaveGoodsModelCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
            </div>
            </form>
          </div>
        </div>
      </DIV>
    </DIV>
<%
	End Function
	Private Function AttributeDo
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
          <div class="inbox">
            <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable" id="Tabs_1">
              <tbody>
               <tr><td class="titletd"><% Echo Lang_Attribute(1) %><!--属性名称-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="AttrName" name="AttrName" value="<% Echo AttrName%>" />
                   <label id="d_AttrName"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Attribute(2) %><!--所属商品类型-->:</td>
                   <td class="infotd">
                   <select id="GoodsModelID" name="GoodsModelID" style="font-size:13px;">
                   <% Echo Admin.GetGoodsModelAsOption(0,GoodsModelID) %>
                   </select>
                   <label class="Normal" id="d_GoodsModelID"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Attribute(3) %><!--排序-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="AttrOrder" name="AttrOrder" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9]*/g','maxLength:5'))" style="width:50px;" value="<% Echo AttrOrder%>" />
                   <label class="Normal"><% Echo Lang_Attribute_Cue(10) %><!--数字越小越靠前--></label>
                   </td>
               </tr>
                <tr><td class="titletd"><% Echo Lang_Attribute(4) %><!--相同属性值的商品是否关联？--></td>
                   <td class="infotd">
                   <input type="radio" name="IsLinked" value="1" <% Echo Admin.Input_Checked("1",IsLinked)%> /><% Echo Lang_Yes %><!--是-->
                   <input type="radio" name="IsLinked" value="0" <% Echo Admin.Input_Checked("0",IsLinked)%> /><% Echo Lang_No %><!--否-->
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Attribute(5) %><!--属性是否可选-->:</td>
                   <td class="infotd">
                   <input type="radio" value="0" name="AttrType" <% Echo Admin.Input_Checked("0",AttrType)%> /><% Echo Lang_Attribute(8) %><!--唯一不可选-->
                   <input type="radio" value="1" name="AttrType" <% Echo Admin.Input_Checked("1",AttrType)%> /><% Echo Lang_Attribute(9) %><!--单选属性-->
                   <input type="radio" value="2" name="AttrType" <% Echo Admin.Input_Checked("2",AttrType)%> /><% Echo Lang_Attribute(10) %><!--复选属性-->
                   <label id="d_AttrType"></label>
                   <span><a href="javascript:;" onclick="ShowHelpMsg('600','300','<% Echo Lang_Attribute(5) %>',Js_Goods_HelpMsg[2])"><b class="icon icon_help"></b></a></span>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Attribute(6) %><!--该属性值的录入方式-->:</td>
                   <td class="infotd">
                   <input type="radio" name="AttrInputType" id="AttrInputType1" onclick='SetInputDisabled(1,"AttrInputType1","AttrValues")' value="0" <% Echo Admin.Input_Checked("0",AttrInputType)%> /><% Echo Lang_Attribute(11) %><!--手工录入-->
                   <input type="radio" name="AttrInputType" id="AttrInputType2" onclick='SetInputDisabled(0,"AttrInputType2","AttrValues")' value="1" <% Echo Admin.Input_Checked("1",AttrInputType)%> /><% Echo Lang_Attribute(12) %><!--从下面的列表中选择（一行代表一个可选值）-->
                   <input type="radio" name="AttrInputType" id="AttrInputType3" onclick='SetInputDisabled(1,"AttrInputType3","AttrValues")' value="2" <% Echo Admin.Input_Checked("2",AttrInputType)%> /><% Echo Lang_Attribute(13) %><!--多行文本框--><br />
                   <label id="d_AttrInputType"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Attribute(7) %><!--可选值列表-->:</td>
                   <td class="infotd">
                   <% if AttrInputType = 1 Then %>
                   <textarea class="info" name="AttrValues" id="AttrValues"><% Echo Replace(AttrValues,"\n",chr(13))%></textarea>
                   <% else %>
                   <textarea class="info" name="AttrValues" id="AttrValues" disabled="disabled" ><% Echo Replace(AttrValues,"\n",chr(13))%></textarea>
                   <% End if %>
                   </td>
               </tr>
              </tbody>
            </table>
            <input type="hidden" name="id" value="<% Echo ID %>" />
            <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
            <div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Goods.SaveAttributeCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
            </div>
            </form>
          </div>
        </div>
      </DIV>
    </DIV>
<%
	End Function
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
End Class
%>