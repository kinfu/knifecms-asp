<!--#include file="config.admin.asp"-->
<%
Dim Comment
Set Comment = New Class_KnifeCMS_Admin_Comment
Set Comment = Nothing
Class Class_KnifeCMS_Admin_Comment

	Private CommentName,TitleField
	Private DBTable_Content,S_TieDepth,Pv_IsConsult
	Private SubSystem,subSysName
	Private Pv_UserID,Pv_Username,Pv_ContentID,Pv_CommentID,Pv_IP,Pv_CommentContent
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "comment.lang.asp"
		
		S_TieDepth   = 4
		Pv_IsConsult = False
		Header()
		
		Pv_UserID     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","userid"))
		Pv_Username   = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("both","username")),24)
		Pv_ContentID  = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","contentid"))
		Pv_CommentID  = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","commentid"))
		Pv_IP         = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","ip"),"[^0-9.]",""),15)
		
		C_TempArr=Split(Ctl,"_")
		If Ubound(C_TempArr)<1 Then Admin.ShowMessage Lang_IllegalSysPra,"",0 : Die ""
		SubSystem = KnifeCMS.Data.Left(KnifeCMS.Data.FiltNotLetter(C_TempArr(0)),50)
		If KnifeCMS.Data.IsNul(SubSystem) Then Admin.ShowMessage Lang_IllegalSysPra,"",0 : Die ""
		Select Case SubSystem
			Case "goods"
				If C_TempArr(1) = "consult" Then
					Pv_IsConsult    = True
					S_TieDepth      = 2
					DBTable         = DBTable_GoodsConsult
				ElseIf C_TempArr(1) = "comment" Then
					S_TieDepth      = 2
					DBTable         = DBTable_GoodsComment
				Else 
				    Admin.ShowMessage Lang_IllegalSysPra,"",0 : Die ""
				End If
				DBTable_Content = TablePre&"Goods"
				TitleField      = "GoodsName"
			Case Else
				'内容模型
				C_TempArr=Split(Ctl,"_")
				If Ubound(C_TempArr)<1 Then Admin.ShowMessage Lang_IllegalSysPra,"",0 : Die ""
				SubSystem = KnifeCMS.Data.Left(KnifeCMS.Data.FiltNotLetter(C_TempArr(1)),50)
				If KnifeCMS.Data.IsNul(SubSystem) Then Admin.ShowMessage Lang_IllegalSysPra,"",0 : Die ""
				'检查内容模型是否存在
				Set Rs = KnifeCMS.DB.GetRecord(TablePre&"ContentSubSystem",Array("System:"&SubSystem),"")
				If Rs.Eof Then ErrMsg = Lang_IllegalSysPra&"<br>"
				KnifeCMS.DB.CloseRs Rs
				If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"",0 : Die ""
				DBTable         = TablePre &"Content_"& SubSystem &"_Comment"
				DBTable_Content = TablePre &"Content_"& SubSystem
				TitleField      = "Title"
				subSysName=Admin.SystemCtlName
		End Select
		
		Select Case Action
			Case "view" : CommentView
			Case "add"
				If SubAction="save" Then
					CommentDoSave
				Else
					Pv_CommentContent = KnifeCMS.DB.GetFieldByID(DBTable,"Content",ID)
					CommentDo
				End If
			Case "edit"
				CommentEdit
			Case "del"       : CommentDel
		End Select
		Footer()
	End Sub
	
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function CommentList()
		Dim Temp_Sql
		Select Case DB_Type
		Case 0
			Temp_Sql = "SELECT a.ID,a.UserID,b.Username,b.HeadImg,a.ContentID,c."& TitleField &",a.Content,a.AddTime,a.Disabled,a.ChildNum,a.IP FROM (["& DBTable &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.UserID)) LEFT JOIN ["& DBTable_Content &"] c ON (c.ID=a.ContentID) WHERE 1=1 "
		Case 1
			Temp_Sql = "SELECT a.ID,a.UserID,b.Username,b.HeadImg,a.ContentID,c."& TitleField &",a.Content,a.AddTime,a.Disabled,a.ChildNum,a.IP FROM ["& DBTable &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.UserID) LEFT JOIN ["& DBTable_Content &"] c ON (c.ID=a.ContentID) WHERE 1=1 "
		End Select
			'If SubAction = "search" Then
				If Pv_IsConsult And Not(Pv_CommentID>0) Then tempCondition = " AND a.Depth=1 "
				tempCondition = tempCondition & KnifeCMS.IIF(Pv_UserID  > 0    ," AND a.UserID="& Pv_UserID &" "," ")
				tempCondition = tempCondition & KnifeCMS.IIF(Pv_Username <>""  ," AND b.Username='"& Pv_Username &"' "," ")
				tempCondition = tempCondition & KnifeCMS.IIF(Pv_ContentID  > 0 ," AND a.ContentID="& Pv_ContentID &" "," ")
				tempCondition = tempCondition & KnifeCMS.IIF(Pv_CommentID  > 0 ," AND a.P_ID="& Pv_CommentID &" "," ")
				tempCondition = tempCondition & KnifeCMS.IIF(Pv_IP <>""        ," AND a.IP='"& Pv_IP &"' "," ")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.KeyWords <>""," AND a.Content like '%"& Admin.Search.KeyWords &"%' "," ")
				Select Case DB_Type
				Case 0
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.AddTime>=#"& Admin.Search.StartDate &"# ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.AddTime<=#"& Admin.Search.EndDate &"# ","")
				Case 1
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.AddTime>='"& Admin.Search.StartDate &"' ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.AddTime<='"& Admin.Search.EndDate &"' ","")
				End Select
				PageUrlPara = "userid="& Pv_UserID &"&username="& Server.URLEncode(Pv_Username) &"&contentid="& Pv_ContentID &"&commentid="& Pv_CommentID &"&ip="& Server.URLEncode(Pv_IP) &"&startdate="& Server.URLEncode(Admin.Search.StartDate) &"&enddate="& Server.URLEncode(Admin.Search.EndDate) &"&keywords="& Server.URLEncode(Admin.Search.KeyWords) &""
				Temp_Sql = Temp_Sql & tempCondition
			'End If
			Temp_Sql  = Temp_Sql &" ORDER BY a.ID DESC"
			Operation = Operation &"<span class=""sysBtn""><a href=""javascript:;"" onClick=""javascript:ShowModal('?ctl="&Ctl&"&act=add&id={$ID}',720,460);ShowModalReload();""><b class=""icon icon_writemsg""></b>"& Lang_DoReply &"</a></span>"
			Call Admin.DataList(Temp_Sql,20,11,Operation,PageUrlPara)
	End Function
	
	Private Function CommentDoSave()
		Dim Fn_Rs,Fn_Result,Fn_ID,Fn_Path,Fn_Depth,Fn_ContentID,Fn_IP
		Pv_CommentContent = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","commenttextarea")),1000)
		Fn_IP             = KnifeCMS.GetClientIP()
		If KnifeCMS.Data.IsNul(Pv_CommentContent) Then Admin.ShowMessage Lang_IllegalSysPra,Array("javascript:WinClose();",Lang_WinClose),0 : Exit Function
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable&":Path,Depth,ContentID",Array("ID:"&ID),"")
		If Not(Fn_Rs.Eof) Then
			Fn_Path      = Fn_Rs(0)
			Fn_Depth     = Fn_Rs(1)
			Fn_ContentID = Fn_Rs(2)
		Else
			Admin.ShowMessage Lang_IllegalSysPra,Array("javascript:WinClose();",Lang_WinClose),0 : Exit Function
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		If Fn_Depth+1 > S_TieDepth Then Admin.ShowMessage Lang_Comment_Cue(9),Array("javascript:WinClose();",Lang_WinClose),0 : Exit Function
		
		Fn_Result = KnifeCMS.DB.AddRecord(DBTable,Array("SysID:"& SysID,"P_ID:"& ID,"Path:"& Fn_Path ,"Depth:"& Fn_Depth+1 ,"ChildNum:0","ContentID:"& Fn_ContentID, "Content:"& Pv_CommentContent,"UserID:"& Admin.UserID,"Username:"& Admin.Username,"HeadImg:NULL" ,"AddTime:"& SysTime,"UpdateTime:"& SysTime,"IP:"& Fn_IP,"Disabled:0"))
		Call KnifeCMS.DB.UpdateRecord(DBTable,Array("ChildNum:ChildNum+1","UpdateTime:"& SysTime),Array("ID:"&ID))
		Fn_ID = Admin.GetIDBySysID(DBTable,SysID)
		Fn_Path = Fn_Path &""& Fn_ID &","
		Call KnifeCMS.DB.UpdateRecord(DBTable,Array("Path:"& Fn_Path),Array("ID:"&Fn_ID))
		Msg = Lang_Comment_Cue(1) &"<br>[ContentID:"& Fn_ContentID &"]<br>[P_ID:"& ID &"]"
		If Fn_Result Then
			Admin.ReloadTopDialog : Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		End If
	End Function
	
	Private Function CommentEdit()
		Dim Fn_Rs,Fn_ArrID,Fn_i,Fn_Disabled,Fn_Msg
		If SubAction="checked" Then 
			Fn_Disabled=0
			Fn_Msg = Lang_Comment_Cue(10)
		ElseIf SubAction="unchecked" Then 
			Fn_Disabled=1
			Fn_Msg = Lang_Comment_Cue(11)
		Else
			Admin.ShowMessage Lang_IllegalSysPra,"",0 : Exit Function
		End If
		ID = KnifeCMS.GetForm("post","InputName")
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			Result = KnifeCMS.DB.UpdateRecord(DBTable,Array("Disabled:"& Fn_Disabled),Array("ID:"&ID))
			If Result Then Msg = Msg & Fn_Msg &"[CommentID="& ID &"]<br>"
		Next
		Admin.ShowMessage Msg,Url,1
	End Function
	
	Private Function CommentDel()
		ID = KnifeCMS.GetForm("post","InputName")
		Dim Fn_Rs,Fn_ArrID,Fn_i,Fn_P_ID,Fn_Path,Fn_ContentID,Fn_ContentTitle
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			Fn_Path = ""
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable&":P_ID,Path,ContentID,ChildNum",Array("ID:"&ID),"")
			If Not(Fn_Rs.Eof) Then
				Fn_P_ID       = KnifeCMS.Data.CLng(Fn_Rs(0))
				Fn_Path       = Fn_Rs(1)
				Fn_ContentID  = Fn_Rs(2)
				Fn_ContentTitle = KnifeCMS.DB.GetFieldByID(DBTable_Content,TitleField,Fn_ContentID)
				If Fn_P_ID > 0 Then Call KnifeCMS.DB.UpdateRecord(DBTable,Array("ChildNum:ChildNum-1"),Array("ID:"&Fn_P_ID))
				Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable &"] WHERE Path LIKE '"& Fn_Path &"%'")
				If Result Then
					If Not(Fn_P_ID>0) Then
						'评论量减1(回复不算)
						If SubSystem<>"goods" Then
							Call  KnifeCMS.DB.UpdateRecord(DBTable_Content,Array("Comments:Comments-1"),Array("ID:"& Fn_ContentID))
						End If
					End If
					Msg = Msg & Lang_Comment_Cue(4) &"{"& Fn_ContentTitle &"[CommentID="& ID &"]}<br>"
				End If
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
		Next
		Admin.ShowMessage Msg,Url,1
	End Function
	
	Private Function CommentView()
%>
	<%=Admin.Calendar(Array("","",""))%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
          <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:checked','<% Echo Lang_DoCheckedConfirm %>')"><b class="icon icon_checked"></b><% Echo Lang_DoChecked%></button>
                  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:unchecked','<% Echo Lang_DoUnCheckedConfirm %>')"><b class="icon icon_unchecked"></b><% Echo Lang_DoUnChecked%></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
          <td class="tdcell">
            <table border="0" cellpadding="2" cellspacing="0">
            <tr><td class="upcell">
                <div class="align-center grey">
                    <% Echo Lang_DistrictDoSearch %><!--搜索操作区-->
                    <span class="AdvancedSearch"><a href="javascript:Admin.AdvancedSearch.Open(460,360,'AdvancedSearch')" ><% Echo Lang_AdvancedSearch %></a></span>
                </div>
                <DIV id="AdvancedSearch" style="display:none;">
                    <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                    <div class="diainbox">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                      <tr><td class="titletd"><% Echo Lang_Comment(1) %></td>
                          <td class="infotd"><input type="text" class="TxtSer" name="userid" style="width:220px;" value="<% Echo Pv_UserID %>" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_Comment(2) %></td>
                          <td class="infotd"><input type="text" class="TxtSer" name="username" style="width:220px;" value="<% Echo Pv_Username %>" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_Comment(4) %></td>
                          <td class="infotd"><input type="text" class="TxtSer" name="contentid" style="width:220px;" value="<% Echo Pv_ContentID %>" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_Keywords %></td>
                          <td class="infotd"><input type="text" class="TxtClass" id="keywords" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:220px;" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_Comment(11) %></td>
                          <td class="infotd"><input type="text" class="TxtSer" name="commentid" style="width:220px;" value="<% Echo Pv_CommentID %>" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_StartDate %>:</td>
                          <td class="infotd">
                          <input type="text" class="TxtSer dateStart" id="startdate" name="startdate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:200px;" value="<%=Admin.Search.StartDate%>" />
                          </td>
                      <tr><td class="titletd"><% Echo Lang_EndDate %>:</td>
                          <td class="infotd">
                          <input type="text" class="TxtSer dateEnd" id="enddate" name="enddate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:200px;" value="<%=Admin.Search.EndDate%>" />
                          </td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_Comment(10) %></td>
                          <td class="infotd"><input type="text" class="TxtSer" name="ip" id="ip" style="width:220px;" value="<% Echo Pv_IP %>" /></td>
                      </tr>
                      </table>
                      </div>
                      <% Echo AdvancedSearchBtnline %>
                    </form>
                </DIV>
                </td></tr>
            <tr><td class="downcell">
                <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                <table border="0" cellpadding="0" cellspacing="0"><tr>
                <td><span><% Echo Lang_Keywords %>:</span></td>
                <td class="pl3"><input type="text" class="TxtClass" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:220px;" /></td>
                <td class="pl3"><button type="submit" class="sysBtn" ><b class="icon icon_search"></b><% Echo Lang_Btn_Search %></button></td>
                </tr></table>
                </form>
            </td></tr>
            </table>
          </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th><div><% Echo Lang_ListTableColLine_ID %></div></th><!--主键ID-->
            <th><div><% Echo Lang_Comment(1) %></div></th><!--会员ID-->
			<th><div><% Echo Lang_Comment(2) %></div></th><!--会员帐号-->
            <th><div><% Echo Lang_Comment(3) %></div></th><!--会员头像-->
            <th><div><% Echo Lang_Comment(4) %></div></th><!--内容ID-->
            <th><div><% Echo Lang_Comment(5) %></div></th><!--内容标题-->
            <th><div><% Echo Lang_Comment(6) %></div></th><!--详细内容-->
            <th><div><% Echo Lang_Comment(7) %></div></th><!--发表时间-->
            <th><div><% Echo Lang_Comment(8) %></div></th><!--审核-->
            <th><div><% Echo Lang_Comment(9) %></div></th><!--回复数-->
            <th><div><% Echo Lang_Comment(10) %></div></th><!--IP-->
			<th><div><% Echo Lang_ListTableColLine_Do %></div></th><!--操作-->
			</tr>
            <% CommentList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
    <form name="FormForReload" style="display:none;" method="post"></form>
    <script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	if(objExist(document.getElementById("keywords"))){ReplaceText(document.getElementById("keywords").value,1)}
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var TempHTML;
		var _ScreenWidth = ScreenWidth();
		if(_ScreenWidth>1440){_ScreenWidth=_ScreenWidth-200;}else{_ScreenWidth=_ScreenWidth-100;}
		var _ScreenHeight = ScreenHeight()-100;
		if(tds!=null){
			var ContentID,TempID;
			var url;
			var SubSystem  = "<%=SubSystem%>";
			var SystemPath = "<%=SystemPath%>";
			var Ctl        = "<%=Ctl%>";
			var Action     = "<%=Action%>";
			var IsChecked;
			for(var i=1;i<tds.length;i++){
				ContentID = "";
				if(tds[i].id=="td3" && tds[i].innerHTML!=""){
					TempHTML = '<span class="sImg"><a href="../?user/'+ tds[i-2].title +'<%=FileSuffix%>" target="_blank"><img src="'+ unescape(tds[i].innerHTML) +'" /></a></span>';
					tds[i].innerHTML=TempHTML;
				}else if(tds[i].id=="td4" && tds[i].innerHTML!=""){
					TempHTML = '<a href="?ctl='+Ctl+'&act='+Action+'&contentid='+ tds[i].title +'" title="'+Lang_Js[2]+'">'+ tds[i].innerHTML +'</a></span>';
					tds[i].innerHTML=TempHTML;
				}else if(tds[i].id=="td5" && tds[i].innerHTML!=""){
					ContentID = tds[i-1].title;
					if(SubSystem=="goods"){
						url=SystemPath+"?shop/"+ContentID+"/index<%=FileSuffix%>";
					}else{
						url=SystemPath+"?content/"+SubSystem+"/"+ContentID+"/index<%=FileSuffix%>";
					}
					TempHTML = '<a href="'+url+'" title="'+Lang_Js[1]+'" target="_blank" >'+ unescape(tds[i].innerHTML) +'</a></span>';
					tds[i].innerHTML=TempHTML;
				}else if(tds[i].id=="td8" && tds[i].innerHTML!=""){
					IsChecked = tds[i].innerHTML;
					if(IsChecked=="1"){TempHTML="<span class=red>"+Lang_Js[5]+"</span>";}else{TempHTML="<span class=green>"+Lang_Js[6]+"</span>";}
					tds[i].innerHTML=TempHTML;
				}else if(tds[i].id=="td9" && tds[i].innerHTML!=""){
					ContentID = tds[i-9].innerHTML;
					TempHTML = '<a href="javascript:ShowModal(\'?ctl='+Ctl+'&act='+Action+'&commentid='+ ContentID +'\','+_ScreenWidth+','+_ScreenHeight+');ShowModalReload();" title="'+Lang_Js[3]+'">'+ unescape(tds[i].innerHTML) + Lang_Js[8] +'</a></span>';
					tds[i].innerHTML=TempHTML;
				}
			}
		}
	}
	ChangeTdText();
    </script>
<%
	End Function
	Function CommentDo()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
          <div class="inbox">
            <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable" id="Tabs_1">
              <tbody>
               <tr><td class="titletd"><% Echo Lang_Comment(6) %><!--评论/咨询内容-->:</td>
                   <td class="infotd">
                   <div style="height:106px; width:485px;"><% Echo Pv_CommentContent %></div>
                   <label id="d_BrandName"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Comment(12) %><!--回复-->:</td>
                   <td class="infotd">
                   <textarea class="text" name="commenttextarea" id="commenttextarea" style="width:485px; height:106px;"></textarea>
                   <label id="d_commenttextarea"></label>
                   </td>
               </tr>
              </tbody>
            </table>
            <input type="hidden" name="id" value="<%=ID%>" />
            <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
            <div class="bottomSaveline" id="SubmitButtoms">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Comment.SaveCommentCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
            </div>
            </form>
          </div>
        </div>
      </DIV>
    </DIV>
<%
	End Function
End Class
%>