<!--#include file="config.admin.asp"-->
<%
Dim [Class]
Set [Class] = New Class_KnifeCMS_Admin_Class
Set [Class] = Nothing
Class Class_KnifeCMS_Admin_Class

	Private ClassName,OldParentClass,ParentClass,ClassOrder,ClassHide,ClassType,Remarks,ClassDepth
	Private GoodsModelID
	Private SeoTitle,SeoUrl,MetaKeywords,MetaDescription
	Private subSys,subSysName,allowDepth,tagLine,editContentURL,editContentName
	Private LogEventPre
	Private Pv_CacheType,Pv_TemplateSet,Pv_CategoryTemplate,Pv_ContentTemplate,Pv_TemplateInherit,Pv_Icon,Pv_Pic
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "class.lang.asp"
		ClassType = 0
		Pv_TemplateSet = 0
		ClassOrder = 999
		Header()
		C_TempArr=Split(Ctl,"_")
		If Ubound(C_TempArr)<1 Then Admin.ShowMessage Lang_IllegalSysPra,"",0 : Die ""
		subSys = KnifeCMS.Data.Left(KnifeCMS.Data.FiltNotLetter(C_TempArr(0)),50)
		If KnifeCMS.Data.IsNul(subSys) Then Admin.ShowMessage Lang_IllegalSysPra,"",0 : Die ""
		
		Select Case subSys
			Case "vote"
				allowDepth=1
				DBTable=TablePre&"VoteClass"
				subSysName=Admin.SystemCtlName
				Pv_CacheType = "vote"
			Case "goods"
				allowDepth=3
				DBTable=TablePre&"GoodsClass"
				subSysName=Admin.SystemCtlName
				Pv_CacheType = "goods"
			Case "goodsvirtual"
				allowDepth=2
				DBTable=TablePre&"GoodsVirtualClass"
				editContentURL  = "goods.asp?ctl=goodsinvirtualclass&act=view"
				editContentName = Lang_Class_GoodsManage
				subSysName=Admin.SystemCtlName
			Case Else
				'内容模型
				allowDepth=3
				subSys    = OS.GetSubSystem(Ctl)
				If subSys="" Then Admin.ShowMessage Lang_IllegalSysPra,"",0 : Die ""
				'检查内容模型是否存在
				If IsContentSubSystemExist(subSys)=False Then
					ErrMsg = Lang_IllegalSysPra&"<br>"
				End If
				If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"",0 : Die ""
				DBTable=TablePre&"Content_"&subSys&"_Class"
				subSysName=Admin.SystemCtlName
				Pv_CacheType = "content"
		End Select
		
		Select Case Action
			Case "view"
				Call ClassView()
			Case "add","edit"
				If SubAction="save" Then
					Call ClassDoSave()
				Else
					ParentClass = KnifeCMS.Data.CLng(KnifeCMS.GetForm("get","parid"))
					If Action = "edit" Then
						Set Rs = KnifeCMS.DB.GetRecord(DBTable&":ClassName,ParentClassID,ClassOrder,ClassHide,ClassType,Icon,Pic,TemplateSet,CategoryTemplate,ContentTemplate,SeoTitle,SeoUrl,MetaKeywords,MetaDescription,Remarks",Array("ID:"&ID),"")
						If Not(Rs.Eof) Then
							ClassName   = KnifeCMS.Data.HTMLDecode(Rs("ClassName"))
							ParentClass = Rs("ParentClassID")
							ClassOrder  = Rs("ClassOrder")
							ClassHide   = KnifeCMS.Data.CLng(Rs("ClassHide"))
							ClassType   = KnifeCMS.Data.CLng(Rs("ClassType"))
							ClassName   = KnifeCMS.Data.HTMLDecode(Rs("ClassName"))
							Pv_Icon     = KnifeCMS.Data.URLDecode(Rs("Icon"))
							Pv_Pic      = KnifeCMS.Data.URLDecode(Rs("Pic"))
							Pv_TemplateSet      = KnifeCMS.Data.CLng(Rs("TemplateSet"))
							Pv_CategoryTemplate = Rs("CategoryTemplate")
							Pv_ContentTemplate  = Rs("ContentTemplate")
							SeoTitle        = Rs("SeoTitle")
							SeoUrl          = Rs("SeoUrl")
							MetaKeywords    = Rs("MetaKeywords")
							MetaDescription = Rs("MetaDescription")
							Remarks         = Rs("Remarks")
						Else
							Admin.ShowMessage Lang_ClassCue(7),"?ctl="& Ctl &"&act=view",0 : Die ""
						End If
						KnifeCMS.DB.CloseRs Rs
						ClassHide   = KnifeCMS.IIF(ClassHide=1,1,0)
						Pv_TemplateInherit = KnifeCMS.IIF(Pv_TemplateSet=0,1,0)
						Call ClassDo()
					ElseIf Action = "add" Then
						Dim Temp_ParID : Temp_ParID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","parid"))
						ParentClass = KnifeCMS.IIF(Temp_ParID>=1,Temp_ParID,0)
						SeoUrl = "index"
						Pv_TemplateInherit = KnifeCMS.IIF(Pv_TemplateSet=0,1,0)
						Call ClassDo()
					End If
				End If
			Case "batchedit" : If SubAction="save" Then SaveBatchClassEdit
			Case "del"       : ClassDel
		End Select
		Footer()
	End Sub
	
	' $ ClassList(ByVal BV_ParID, ByVal DB_Table, ByVal BV_Depth, ByVal BV_EditURL, ByVal BV_EditName)
	'   BV_ParID: 父级分类ID
	'   DB_Table: 分类数据库表
	'   BV_Depth: 分类级数
	'   BV_EditURL: 内容管理URL
	'   BV_EditName: 内容管理名称
	Private Function ClassList(ByVal BV_ParID, ByVal DB_Table, ByVal BV_Depth, ByVal BV_EditURL, ByVal BV_EditName)
		Dim TempRs,Temp_N,TempLastID
		Dim TempID,TempClassName,TempClassDepth,TempClassChildNum,TempClassOrder,TempClassHide,TempClassType,TempTemplateInherit,TempRemarks
		BV_ParID=KnifeCMS.Data.CLng(BV_ParID)
		DB_Table=CStr(DB_Table)
		BV_EditURL=CStr(BV_EditURL)
		BV_EditName=CStr(BV_EditName)
		BV_Depth=KnifeCMS.Data.Int(BV_Depth)
		If BV_Depth < 1 Then BV_Depth = 1
		Set TempRs = KnifeCMS.DB.GetRecord(DB_Table&":ID,ClassName,ClassDepth,ClassChildNum,ClassOrder,ClassHide,ClassType,TemplateSet,CategoryTemplate,ContentTemplate",Array("ParentClassID:"&BV_ParID),"ORDER BY ClassDepth,ClassOrder ASC")
		If TempRs.bof and TempRs.eof then
			If BV_EditURL <> "" Then
				Echo "<tr class=solid><td colspan=7>none...</td></tr>"
			Else
				Echo "<tr class=solid><td colspan=6>none...</td></tr>"
			End If
		Else
			TempRs.movelast
			TempLastID=TempRs(0)
			TempRs.movefirst
			do while not TempRs.eof'-----------------------------------------------------------
				TempID            = KnifeCMS.Data.CLng(TempRs("ID"))
				TempClassName     = KnifeCMS.Data.HTMLDecode(TempRs("ClassName"))
				TempClassDepth    = KnifeCMS.Data.CLng(TempRs("ClassDepth"))
				TempClassChildNum = KnifeCMS.Data.CLng(TempRs("ClassChildNum"))
				TempClassOrder    = KnifeCMS.Data.CLng(TempRs("ClassOrder"))
				TempClassHide     = KnifeCMS.Data.CLng(TempRs("ClassHide"))
				TempClassType     = KnifeCMS.Data.CLng(TempRs("ClassType"))
				Pv_TemplateSet    = KnifeCMS.Data.CLng(TempRs("TemplateSet"))
				
				Echo "<tr class=solid>"
				Echo "<td class=firstl>"
				Echo "<input type=""hidden"" name=""ClassID"" value="""&TempID&""" />"
				Echo "<input type=""text"" class=""TxtClass"" name=""Order"" style=""width:40px;"" value="""&TempClassOrder&""" />"
				Echo "</td>"
				Echo "<td id="&TempClassDepth&":"&TempClassChildNum&" class=imgText>"
				If TempClassDepth > 1 Then
					for Temp_N=2 to TempClassDepth
						if Temp_N=2 then
						Echo"&nbsp;&nbsp;&nbsp;&nbsp;"
						else
						Echo"<img src=""image/tree_l.gif"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
						end if
					next
					if TempLastID=TempID then
					Echo"<img src=""image/tree_b.gif"">"
					else
					Echo"<img src=""image/tree_m.gif"">"
					end if
				End If
				Echo "<input type=""text"" class=""TxtClass"" style=""width:120px;"" name=""ClassName"" "&KnifeCMS.IIF(TempClassHide,"style=""color:#888;""","")&" value="""&TempClassName&"""/></td>"
				Echo"<td>"&KnifeCMS.IIF(TempClassType,"<img class=""pointer"" ctl="""& Ctl &""" id="""& TempID &""" value="""& TempClassType &""" onclick=""Admin.Class.SetIsPageContent(this)"" src=""image/icon_yes.png"">","<img class=""pointer"" ctl="""& Ctl &""" id="""& TempID &""" value="""& TempClassType &"""  onclick=""Admin.Class.SetIsPageContent(this)"" src=""image/icon_yes_gray.png"">")&"</td>"
				Echo"<td>"&KnifeCMS.IIF(TempClassHide,"<img class=""pointer"" ctl="""& Ctl &""" id="""& TempID &""" value="""& TempClassHide &""" onclick=""Admin.Class.SetIsShow(this)"" src=""image/icon_yes_gray.png"">","<img class=""pointer"" ctl="""& Ctl &""" id="""& TempID &""" onclick=""Admin.Class.SetIsShow(this)"" value="""& TempClassHide &""" src=""image/icon_yes.png"">")&"</td>"
				Echo"<td>"
				If BV_Depth >= TempClassDepth+1 Then 
				  Echo"<span class=""sysBtn""><a href=""?ctl="& Ctl &"&act=add&parid="&TempID&""" target=""_self""><b class=""icon icon_write""></b>"& Lang_Class(0) &"</a></span>"
				End If
				Echo"<span class=""sysBtn""><a href=""?ctl="& Ctl &"&act=edit&id="&TempID&""" target=""_self""><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
				Echo"<span class=""sysBtn""><a href=""?ctl="& Ctl &"&act=del&id="&TempID&""" onclick=""return(confirm('"&  Lang_DoDelConfirm &"'))"" target=""_self""><b class=""icon icon_del""></b>"& Lang_DoDel &"</a></span>"
				Echo"</td>"
				If BV_EditURL <> "" Then
				  Echo"<td><span class=""sysBtn""><a href=""javascript:;"" onClick=""ShowModal('"&BV_EditURL&"&id="&TempID&"',920,560); return false;""><b class=""icon icon_edit""></b>"&BV_EditName&"</a></span></td>"
				End If
				Echo"<td>"& KnifeCMS.IIF(Pv_TemplateSet=0,Lang_ClassCue(18),"") &"</td>"
				Echo"</TR>"
				if TempClassChildNum > 0 then
				Call ClassList(TempID,DB_Table,BV_Depth,BV_EditURL,BV_EditName)
				end if
				TempRs.movenext
			loop'------------------------------------------------------------------------
		end if
		KnifeCMS.DB.CloseRs TempRs
	End Function
	
	Private Function ClassDoSave()

		'获取数据并处理数据
		ClassName       = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ClassName")),100)
		ParentClass     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","ParentClass"))'编辑后的父级分类ID
		ClassOrder      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","ClassOrder"))
		ClassHide       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","ClassHide"))
		ClassType       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","ClassType"))
		'图片
		Pv_Icon         = KnifeCMS.Data.Left(KnifeCMS.Data.URLEncode(KnifeCMS.GetForm("post","Icon")),250)
		Pv_Pic          = KnifeCMS.Data.Left(KnifeCMS.Data.URLEncode(KnifeCMS.GetForm("post","Pic")),250)
		
		Pv_TemplateInherit = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","TemplateInherit"))
		If Pv_TemplateInherit=1 Then
			Pv_TemplateSet = 0
		Else
			Pv_TemplateSet = 1
			Pv_CategoryTemplate = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","CategoryTemplate")),50)
			Pv_ContentTemplate  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ContentTemplate")),50)
		End If
		
		Remarks         = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","TextContent")),100000)
		
		ClassHide       = KnifeCMS.IIF(ClassHide=1,1,0)
		
		'SEO优化内容
		SeoTitle            = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","SeoTitle")),250)        : If KnifeCMS.Data.IsNul(SeoTitle) Then SeoTitle=ClassName
		SeoUrl              = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","SeoUrl")),250)          : If KnifeCMS.Data.IsNul(SeoUrl)   Then SeoUrl="index"
		MetaKeywords        = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","MetaKeywords")),250)
		MetaDescription     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","MetaDescription")),250)
		
		'获取原来父分类的ID
		OldParentClass=0
		Set Rs = KnifeCMS.DB.GetRecord(DBTable&":ParentClassID",Array("ID:"& ID),"")
		If Not(Rs.Eof) Then
			OldParentClass=KnifeCMS.Data.CLng(Rs(0))
		End If
		Set Rs=nothing
		
		'获取上级分类的级别深度
		Set Rs = KnifeCMS.DB.GetRecord(DBTable&":ClassDepth",Array("ID:"&ParentClass),"")
		If Not(Rs.Eof) Then ClassDepth=Rs(0)+1 : Else ClassDepth=1 : End if
		Set Rs=nothing
		'数据合法性检测
		If Trim(Replace(ClassName,"&nbsp;",""))="" Then ErrMsg = Lang_ClassCue(4) &"<br>"
		'检查是否超出允许的分类级数 allowDepth
		if ClassDepth > allowDepth Then ErrMsg = Replace(Lang_ClassCue(10),"{$:allowDepth}",allowDepth) &"<br>"
		
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		'开始保存数据操作
		If Action="add" then
			Result = KnifeCMS.DB.AddRecord(DBTable,Array("ClassName:"& ClassName,"ClassDepth:"& ClassDepth,"ParentClassID:"& ParentClass,"ClassOrder:"& ClassOrder,"ClassChildNum:0","ClassHide:"& ClassHide,"ClassType:"& ClassType,"Icon:"& Pv_Icon,"Pic:"& Pv_Pic,"TemplateSet:"& Pv_TemplateSet,"CategoryTemplate:"& Pv_CategoryTemplate,"ContentTemplate:"& Pv_ContentTemplate,"SeoTitle:"& SeoTitle,"SeoUrl:"& SeoUrl,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription,"Remarks:"& Remarks,"Recycle:0"))
			'执行对父分类的子分类数加1
			Result = KnifeCMS.DB.UpdateRecord(DBTable,"ClassChildNum=ClassChildNum+1",Array("ID:"&ParentClass))
			ID = KnifeCMS.DB.GetFieldByMaxID(DBTable,"ID")
			
			'更新缓存
			If IsCache Then
				Print "<div class=""creating_cache"">"
				Call Admin.CreateCache_Init()
				Select Case Pv_CacheType
				Case "vote"    : Call Admin.CreateStaticHTML_Vote(True,ID,0)
				Case "goods"   : Call Admin.CreateStaticHTML_Shop(True,ID,0)
				Case "content" : Call Admin.CreateStaticHTML_ContentSystem(subSys,True,ID,0)
				End Select
				Call Admin.CreateCache_Terminate()
				Print "</div>"
			End If
			
			Msg    = Lang_ClassCue(0) &"{"& ClassName &"["& Admin.SystemCtlName &"]}"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		ElseIf Action="edit" then
			'若是移到自己的分类下则报错
			MoveClassCheck ID,0,ParentClass,DBTable
			'更新分类
			Result = KnifeCMS.DB.UpdateRecord(DBTable,Array("ClassName:"& ClassName,"ClassDepth:"& ClassDepth,"ParentClassID:"& ParentClass,"ClassOrder:"& ClassOrder,"ClassHide:"& ClassHide,"ClassType:"& ClassType,"Icon:"& Pv_Icon,"Pic:"& Pv_Pic,"TemplateSet:"& Pv_TemplateSet,"CategoryTemplate:"& Pv_CategoryTemplate,"ContentTemplate:"& Pv_ContentTemplate,"SeoTitle:"& SeoTitle,"SeoUrl:"& SeoUrl,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription,"Remarks:"& Remarks),Array("ID:"& ID))
			'执行对父分类的子分类数加1
			if ParentClass<>0 then Result = KnifeCMS.DB.UpdateRecord(DBTable,"ClassChildNum=ClassChildNum+1",Array("ID:"&ParentClass))
			'执行对原来父分类的子分类数减1
			if OldParentClass>0 then Result = KnifeCMS.DB.UpdateRecord(DBTable,"ClassChildNum=ClassChildNum-1",Array("ID:"&OldParentClass))
			'修正其下各个分类的深度
			SubClassDepthEdit ID,ClassDepth,DBTable
			
			'更新缓存
			If IsCache Then
				Print "<div class=""creating_cache"">"
				Call Admin.CreateCache_Init()
				Select Case Pv_CacheType
				Case "vote"    : Call Admin.CreateStaticHTML_Vote(True,ID,0)
				Case "goods"   : Call Admin.CreateStaticHTML_Shop(True,ID,0)
				Case "content" : Call Admin.CreateStaticHTML_ContentSystem(subSys,True,ID,0)
				End Select
				Call Admin.CreateCache_Terminate()
				Print "</div>"
			End If
			
			Msg    = Lang_ClassCue(1) &"{"& ClassName &"["& Admin.SystemCtlName &"][ID="&ID&"]}"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		Else
			Admin.ShowMessage "act:error","?ctl="& Ctl &"&act=view",0
		End If
	End Function
	
	'$:批量执行编辑操作
	Private Function SaveBatchClassEdit()
		Dim Fn_ID,Fn_ArrID,Fn_Order,Fn_ArrOrder,Fn_ClassName,Fn_ArrClassName,Fn_i
		Fn_i = 1
		For Each Fn_ID In Request.Form("ClassID")
			Fn_ID = KnifeCMS.Data.CLng(Fn_ID)
			Fn_ArrID = KnifeCMS.IIF( Fn_i=1 , Fn_ID , Fn_ArrID &","& Fn_ID)
			Fn_i = Fn_i+1
		Next
		Fn_i = 1
		For Each Fn_Order In Request.Form("Order")
			Fn_Order    = KnifeCMS.Data.CLng(Fn_Order)
			Fn_ArrOrder = KnifeCMS.IIF( Fn_i=1 , Fn_Order , Fn_ArrOrder &","& Fn_Order)
			Fn_i = Fn_i+1
		Next
		Fn_i = 1
		For Each Fn_ClassName In Request.Form("ClassName")
			Fn_ClassName    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(Fn_ClassName),100)
			Fn_ArrClassName = KnifeCMS.IIF( Fn_i=1 , Fn_ClassName , Fn_ArrClassName &","& Fn_ClassName)
			Fn_i = Fn_i+1
		Next
		'Echo Fn_ArrID &"<br>"& Fn_ArrOrder &"<br>"& Fn_ArrClassName &"<br>"
		Fn_ArrID        = Split(Fn_ArrID,",")
		Fn_ArrOrder     = Split(Fn_ArrOrder,",")
		Fn_ArrClassName = Split(Fn_ArrClassName,",")
		'Echo Ubound(Fn_ArrID) &"-"& Ubound(Fn_ArrOrder) &"-"& Ubound(Fn_ArrClassName)
		If Ubound(Fn_ArrID) <> Ubound(Fn_ArrOrder) Or Ubound(Fn_ArrOrder) <> Ubound(Fn_ArrClassName) Then Admin.ShowMessage Lang_ClassCue(16),"?ctl="& Ctl &"&act=view",0 : Exit Function
		For Fn_i=0 To Ubound(Fn_ArrID)
			Fn_ID        = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			Fn_Order     = KnifeCMS.Data.CLng(Fn_ArrOrder(Fn_i))
			Fn_ClassName = CStr(KnifeCMS.Data.Left(Fn_ArrClassName(Fn_i),100))
			Result = KnifeCMS.DB.UpdateRecord(DBTable,Array("ClassName:"&Fn_ClassName,"ClassOrder:"&Fn_Order),Array("ID:"&Fn_ID))
			Msg    = Msg & "["& Admin.SystemCtlName &"]"& Lang_ClassCue(1) &"{"& Fn_ClassName &"[ID="& Fn_ID  &"]}<br>"
		Next
		Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
	End Function
	
	'$编辑分类时若是移到自己的分类下则报错
	Private Sub MoveClassCheck(ByVal ClassID, ByVal tmpParentClassID, ByVal ParentClassID, ByVal DB_Table)
		Dim TempRs
		Dim tmpClassChildNum : tmpClassChildNum = 0
		Dim tmpClassID       : tmpClassID       = 0
		ClassID          = KnifeCMS.Data.CLng(ClassID)
		tmpParentClassID = KnifeCMS.Data.CLng(tmpParentClassID)
		ParentClassID    = KnifeCMS.Data.CLng(ParentClassID)
		'若是把自己作为上一级分类则报错
		'Echo "ClassID:"&ClassID&"--ParentClassID:"&ParentClassID &"<Br>"
		If ID = ParentClassID Then Admin.ShowMessage Lang_ClassCue(12),"?ctl="& Ctl &"&act=view",0 : Die ""
		If ClassID = 0 And tmpParentClassID > 0 Then
			Set TempRs = KnifeCMS.DB.GetRecord(DBTable&":ID,ClassChildNum",Array("ParentClassID:"&tmpParentClassID),"ORDER BY ClassOrder ASC")
		Else
			Set TempRs = KnifeCMS.DB.GetRecord(DBTable&":ID,ClassChildNum",Array("ID:"&ClassID),"")
		End if
		Do While Not TempRs.Eof
			tmpClassID=KnifeCMS.Data.CLng(TempRs(0))
			tmpClassChildNum=KnifeCMS.Data.CLng(TempRs(1))
			'若是移到自己的分类下则报错
			If ParentClassID = tmpClassID Then Admin.ShowMessage Lang_ClassCue(13),"goback",0 : Die ""
			If tmpClassChildNum > 0       Then MoveClassCheck 0,tmpClassID,ParentClassID,DB_Table
			TempRs.Movenext
		Loop
		KnifeCMS.DB.CloseRs TempRs
	End Sub
	
	Private Function SubClassDepthEdit(ByVal BV_ID,ByVal BV_ClassDepth,ByVal DB_Table)
		Dim TempRs,sSql,tmpClassID,tmpClassChildNum,tmpClassDepth
		tmpClassDepth=BV_ClassDepth+1
		BV_ID = KnifeCMS.Data.CLng(BV_ID)
		Set TempRs = KnifeCMS.DB.GetRecord(DB_Table&":ID,ClassChildNum",Array("ParentClassID:"&BV_ID),"")
		Do While Not TempRs.Eof
			tmpClassID=TempRs(0)
			tmpClassChildNum=TempRs(1)
			Call KnifeCMS.DB.UpdateRecord(DB_Table,Array("ClassDepth:"&BV_ClassDepth+1),Array("ID:"&tmpClassID))
			If tmpClassChildNum > 0 Then SubClassDepthEdit tmpClassID,tmpClassDepth,DB_Table
			TempRs.Movenext
		loop
		KnifeCMS.DB.CloseRs TempRs
	End Function
	
	
	Private Function ClassDel()
		ParentClass=0
		If ID<1 Then Exit Function
		'检查该分类下是否有子分类
		Set Rs = KnifeCMS.DB.GetRecord(DBTable&":ParentClassID,ClassChildNum,ClassName",Array("ID:"&ID),"")
		If Rs.bof and Rs.eof then
			ErrMsg = Lang_ClassCue(7)
		Else
			ParentClass=Rs(0)
			ClassName = Rs(2)
			'该分类下有子分类,请先删除子分类.
			If Rs(1) > 0 then ErrMsg = ErrMsg & Lang_ClassCue(14)
		End If
		KnifeCMS.DB.CloseRs Rs
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		Result = KnifeCMS.DB.DeleteRecord(DBTable,Array("ID:"&ID))
		Result = KnifeCMS.DB.UpdateRecord(DBTable,"ClassChildNum=ClassChildNum-1",Array("ID:"&ParentClass))
		Msg    = Lang_ClassCue(2) &"{"& ClassName &"["& Admin.SystemCtlName &"][ID="& ID &"]}"
		If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
	End Function
	
	Function ClassView()
%>
	<style type="text/css">body{ padding-bottom:37px;}</style>
	<DIV id="MainFrame">
		<div class="tagLine"><%=NavigationLine%></div>
		<DIV class="cbody">
		  <div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td class="tdcell"><span class="sysBtn"><a href="?ctl=<%=Ctl%>&act=add" ><b class="icon icon_add"></b><%=Lang_DoAdd%></a></span></td>
		  <td class="tdcell"><span class="illustration"><b><%=Lang_SystemCue%>:</b><%=Lang_ClassCue(15)%></span></td>
		  </tr>
		  </table>
		  </div>
		  <div class="tabledata">
		  <form name="SaveForm" id="SaveForm" action="?ctl=<%=Ctl%>&act=batchedit&subact=save" method="post">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th width="60px"><div style="width:60px;"><%=Lang_Class(3)%></div></th><!--排序-->
			<th width="350px"><div style="width:350px;"><%=Lang_Class(1)%></div></th><!--分类名称-->
            <th width="120px"><div style="width:120px;"><%=Lang_Class(8)%></div></th><!--优先显示单页-->
            <th width="60px"><div style="width:60px;"><%=Lang_Class(4)%></div></th><!--隐藏-->
			<th width="210px"><div style="width:210px;"><%=Lang_ListTableColLine_Do%></div></th><!--操作-->
			<% If not(KnifeCMS.Data.IsNul(editContentURL)) Then Echo "<th width=""90px"">"& editContentName &"</th>" %>
			<th><%=Lang_Class(14)%></th><!--备注-->
			</tr>
			<% ClassList 0,DBTable,allowDepth,editContentURL,editContentName%>
		   </tbody>
		  </table>
		  <div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
			  <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Class.BatchEdit()"><b class="icon icon_save"></b><%=Lang_Btn_BatchEdit%></button>
			  <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><%=Lang_Btn_Reset%></button>
		  </div>
		  </form>
		  </div>
		</DIV>
	</DIV>
<%
	End Function
	Function ClassDo()
%>
	<DIV id="MainFrame">
		<div class="tagLine"><%=NavigationLine%></div>
		<DIV class="cbody">
			<div class="wbox">
				<form name="SaveForm" action="?ctl=<%=Ctl%>&act=<%=Action%>&subact=save" method="post">
                <div class="inbox">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                  <tbody>
				   <tr><td class="titletd"><span class="required">*</span><%=Lang_Class(1)%></td><td class="infotd">
					   <input type="text" class="TxtClass" name="ClassName" id="ClassName" value="<%=ClassName%>" onblur="KnifeCMS.onblur(this,'maxLength:250')" />
					   <label class="Normal"  id="d_ClassName"></label>
					   </td></tr>
				   <tr><td class="titletd"><%=Lang_Class(2)%></td><td class="infotd">
					   <input type="hidden" name="OldParentClass" value="<%=ParentClass%>" />
					   <select name="ParentClass" id="ParentClass" style="font-size:13px;" /><%=Admin.ShowParentClass(0,ParentClass,DBTable,allowDepth)%></select>
					   <label class="Normal" id="d_ParentClass">*</label>
					   </td></tr>
                   <% If subSys = "goods" Then%>
                   <tr><td class="titletd"><%=Lang_Class(13)%></td><td class="infotd">
					   <input type="hidden" name="OldParentClass" value="<%=ParentClass%>" />
					   <select id="GoodsModel" name="GoodsModel" style="font-size:13px;">
					   <%=Admin.GetGoodsModelAsOption(0,GoodsModelID)%>
                       </select>
					   <label class="Normal" id="d_ParentClass">*</label>
					   </td></tr>
                   <% End If%>
                   <tr><td class="titletd"><%=Lang_Class(8)%></td><td class="infotd">
                       <input type="checkbox" value="1" onclick="ShowCheckbox(this,'1:1','TextContent_Area')" <%=Admin.InputChecked(ClassType)%> name="ClassType" />
                       <label class="Normal"><%=Lang_ClassCue(17)%></label>
                       </td>
                   </tr>
                  </tbody>
                  <tbody id="TextContent_Area" <% If ClassType <> 1 Then Echo "style=""display:none;"""%>>
                   <tr><td class="titletd"><%=Lang_Class(5)%><!--备注--></td><td class="infotd">
                       <textarea class="TextContent" id="TextContent" name="TextContent"><%=KnifeCMS.Data.HTMLDecode(Remarks)%></textarea>
                       <label class="Normal"></label>
                       </td>
                   </tr>
                   </tbody>
                   <tbody>
                   <tr><td class="titletd"><%=Lang_Class(14)%></td><td class="infotd">
					   <input type="checkbox" value="1" <%=Admin.InputChecked(Pv_TemplateInherit)%> name="TemplateInherit" id="TemplateInherit" />
					   <label class="Normal"><%=Lang_ClassCue(18)%></label>
                       <div id="TemplateSet" <% If Pv_TemplateInherit = 1 Then Echo "style=""display:none;"""%>>
                       <div class="inbox" style="display:inline-block; margin-top:5px;">
                        <table border="0" cellpadding="0" cellspacing="0" class="infoTable">
                          <tbody>
                          <tr><td class="titletd"><span class="required">*</span><%=Lang_Class(15)%></td>
                               <td class="infotd">
                               <%=Admin.SelectTemplateFile("CategoryTemplate","",Pv_CategoryTemplate)%>
                               <span class="mt5"><label class="Normal" id="d_CategoryTemplate"></label></span>
                               </td>
                          </tr>
                          <tr><td class="titletd"><span class="required">*</span><%=Lang_Class(16)%></td>
                               <td class="infotd">
                               <%=Admin.SelectTemplateFile("ContentTemplate","",Pv_ContentTemplate)%>
                               <span class="mt5"><label class="Normal" id="d_ContentTemplate"></label></span>
                               </td>
                          </tr>
                          </tbody>
                        </table>
                       </div>
                       </div>
					   </td>
                   </tr>
                   <tr><td class="titletd"><%=Lang_Class(17)%></td><td class="infotd">
					   <a href="javascript:;" id="a_PicSet"><%=Lang_Class(20)%></a>
                       <div id="PicSet" <% If KnifeCMS.Data.IsNul(Pv_Icon) And KnifeCMS.Data.IsNul(Pv_Pic) Then Echo "style=""display:none;"""%>>
                       <div class="inbox" style="display:inline-block; margin-top:5px;">
                        <table border="0" cellpadding="0" cellspacing="0" class="infoTable">
                          <tbody>
                            <tr><td class="titletd" style="width:auto; padding-left:10px;"><%=Lang_Class(18)%></td>
                                <td class="infotd">
                                <div class="image_grid">
                                    <table border="0" cellpadding="0" cellspacing="0"><tr>
                                    <td valign="top" style="padding:0px;">
                                        <div class="imageBox" id="IconBox">
                                        <%
                                        If not(KnifeCMS.Data.IsNul(Pv_Icon)) Then
                                            Echo "<img class=""img"" src="""& Pv_Icon &""" />"
                                        Else
                                            Echo "<img class=""img"" src=""image/image.jpg"" />"
                                        End If
                                        %>
                                        </div>
                                    <td valign="top" style="padding:0px 0px 0px 6px;">
                                        <input type="text" class="TxtClass slong" id="Icon" name="Icon" onblur="Admin.ChangeImageUrl('IconBox','Icon','img','d_Icon')" style="width:314px; border:1px dashed #ccc;" value="<%=Pv_Icon%>"/>
                                        <div style="padding:5px 0px 0px 0px;">
                                        <span class="sysBtn" id="UploadImage"><a onclick="UploadImg('1','Icon');Admin.ImageUrlChange('IconBox','Icon','','d_Icon')" href="javascript:void(0)"><b class="icon icon_add"></b><%=Lang_Btn_SelectPics%></a></span>
                                        </div>
                                        <label class="Normal" id="d_Icon"></label>
                                    </td>
                                    </tr></table>
                                </div>
                                </td>
                            </tr>
                            <tr><td class="titletd" style="width:auto; padding-left:10px;"><%=Lang_Class(19)%></td>
                                <td class="infotd">
                                <div class="image_grid">
                                    <table border="0" cellpadding="0" cellspacing="0"><tr>
                                    <td valign="top" style="padding:0px;">
                                        <div class="imageBox" id="PicBox">
                                        <%
                                        If Not(KnifeCMS.Data.IsNul(Pv_Pic)) Then
                                            Echo "<img class=""img"" src="""& Pv_Pic &""" />"
                                        Else
                                            Echo "<img class=""img"" src=""image/image.jpg"" />"
                                        End If
                                        %>
                                        </div>
                                    <td valign="top" style="padding:0px 0px 0px 6px;">
                                        <input type="text" class="TxtClass slong" id="Pic" name="Pic" onblur="Admin.ChangeImageUrl('PicBox','Pic','img','d_Pic')" style="width:314px; border:1px dashed #ccc;" value="<%=Pv_Pic%>"/>
                                        <div style="padding:5px 0px 0px 0px;">
                                        <span class="sysBtn" id="UploadImage"><a onclick="UploadImg('1','Pic');Admin.ImageUrlChange('PicBox','Pic','','d_Pic')" href="javascript:void(0)"><b class="icon icon_add"></b><%=Lang_Btn_SelectPics%></a></span>
                                        </div>
                                        <label class="Normal" id="d_Pic"></label>
                                    </td>
                                    </tr></table>
                                </div>
                                </td>
                            </tr>
                          </tbody>
                        </table>
                       </div>
                       </div>
                       
                       
					   </td>
                   </tr>
				 </tbody>
				</table>
                </div>
                <div class="inbox">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                  <tbody>
                     <tr><td class="titletd" valign="top"><%=Lang_SeoTitle%></td><td class="infotd">
                         <input type="text" class="TxtClass long" name="SeoTitle" id="SeoTitle" maxlength="250" value="<%=KnifeCMS.Data.ReplaceString(SeoTitle,"&nbsp;",Chr(32))%>"/> <label class="Normal" id="d_SeoTitle"><%=Lang_SeoTitleCue%></label>
                         </td></tr>
                     <tr><td class="titletd" valign="top"><%=Lang_SeoUrl%></td><td class="infotd">
                         <input type="text" class="TxtClass readonly" name="url_pre" readonly="readonly" style="width:368px;" value="<%=SiteURL & SystemPath & "?content/"& subSys &"/category/"& KnifeCMS.IIF(ID>0,ID,"{id}") &"/"%>"/>
                         <input type="text" class="TxtClass" name="SeoUrl" id="SeoUrl" maxlength="250"  style="width:160px;" value="<%=SeoUrl%>"/>
                         <input type="text" class="TxtClass readonly" name="url_suffix" readonly="readonly" style="width:40px;" value="<%=FileSuffix%>"/>
                         <label class="Normal" id="d_SeoUrl"><%=Lang_SeoUrlCue%></label>
                         </td></tr>
                     <tr><td class="titletd" valign="top"><%=Lang_MetaKeywords%></td><td class="infotd">
                         <input type="text" class="TxtClass long" name="MetaKeywords" id="MetaKeywords" maxlength="250" value="<%=KnifeCMS.Data.ReplaceString(MetaKeywords,"&nbsp;",Chr(32))%>"/> <label class="Normal" id="d_MetaKeywords"><%=Lang_MetaKeywordsCue%></label>
                         </td></tr>
                     <tr><td  class="titletd" valign="top"><%=Lang_MetaDescription%></td><td class="infotd">
                         <textarea class="metadescription" name="MetaDescription" id="MetaDescription" maxlength="250" onblur="KnifeCMS.onblur(this,'maxLength:250')" ><%=KnifeCMS.Data.ReplaceString(MetaDescription,"&nbsp;",Chr(32))%></textarea>
                         <label class="Normal" id="d_MetaDescription"><%=Lang_MetaDescriptionCue%></label>
                         </td>
                     </tr>
                  </tbody>
                  </table>
                </div>
                
                <div class="inbox">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
				<tbody>
                  <tr><td class="titletd"><%=Lang_Class(3)%></td><td class="infotd">
					  <input type="text" class="TxtClass" id="ClassOrder" name="ClassOrder" onblur="KnifeCMS.onblur(this,'int')" style="width:50px;" value="<%=ClassOrder%>" />
					  <label class="Normal"><%=Lang_ClassCue(5)%></label>
					  </td>
                  </tr>
                  <tr><td class="titletd"><%=Lang_Class(9)%></td><td class="infotd">
					  <input type="checkbox" value="1" <%=Admin.InputChecked(ClassHide)%> name="ClassHide" />
					  <label class="Normal"><%=Lang_ClassCue(6)%></label>
					  </td>
                  </tr>
                </tbody>
				</table>
                </div>
                
				<div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
				<input type="hidden" name="id" value="<%=ID%>" />
				<input type="hidden" name="sysid" id="sysid" value="<%=SysID%>" />
				<button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Class.SaveClassCheck()"><b class="icon icon_save"></b><%=Lang_Btn_Submit%></button>
				<button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><%=Lang_Btn_Reset%></button>
				</div>
				</form>
			</div>
		</DIV>
	</DIV>
    <script type="text/javascript" charset="utf-8">
	window.UEDITOR_HOME_URL = "<%=SystemPath%>plugins/ueditor/";
	</script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_config.asp"></script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_api.js"></script>
    <script type="text/javascript">
	var OSWebEditor=UE.getEditor('TextContent');
	//模版设置
	$("input[name='TemplateInherit']").click(function(){
		if($(this).attr("checked")==true){
			$("#TemplateSet").hide("fast");
		}else{
			$("#TemplateSet").show("fast");
		}
	});
	//标识图片
	$("#a_PicSet").click(function(){
		var $picSet = $("#PicSet");
		if($picSet.css("display")=="none"){
			$picSet.show("fast");
			$(this).htm("<%=Lang_Class(21)%>");
		}else{
			$picSet.hide("fast");
			$(this).htm("<%=Lang_Class(20)%>");
		}
	});
	if($("#PicSet").css("display")=="none"){
		$("#a_PicSet").htm("<%=Lang_Class(20)%>");
	}else{
		$("#a_PicSet").htm("<%=Lang_Class(21)%>");
	}
	</script>
<%
	End Function
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
End Class
%>