<!--#include file="config.admin.asp"-->
<%
Dim SystemUser
Set SystemUser = New Class_KnifeCMS_Admin_SystemUser
Set SystemUser = Nothing
Class Class_KnifeCMS_Admin_SystemUser
	Private Pv_MemberID
	Private Username,RealName,Password,RePassword,Email,GroupID,UserDisable,LoginTimes,LastLoginTime,Remarks,Degree
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "sysuser.lang.asp"
		IsContentExist = False
		Header()
		If Ctl = "sysuser" Or Ctl = "sysuser_recycle" Then
			Select Case Action
				Case "view"
					Call SystemUserView()
				Case "add"
					If SubAction="save" Then
						SystemUserDoSave
					Else
						SystemUserDo
					End If
				Case "edit"
					If SubAction="save" Then
						Call GetSystemUserDetailData()
						If IsContentExist Then SystemUserDoSave : Else Admin.ShowMessage Lang_SysUser(6),"goback",0
					Else
						If SubAction = "revert" Or SubAction = "forbid" Or SubAction = "enabled" Then
							Call SystemUserEditDo()
						Else
							Call GetSystemUserDetailData()
							If IsContentExist Then
								Call SysUserDoAuthorityCheck(Degree)
								Call SystemUserDo()
							Else
								Admin.ShowMessage Lang_SysUser(6),"goback",0
							End If
						End If
					End IF
				Case "del"  : Call SystemUserDelDo()
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"?ctl="& Ctl &"&act=view",1
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function GetSystemUserDetailData()
		Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT a.Username,a.RealName,a.Password,a.Email,a.GroupID,a.MemberID,a.UserDisable,a.Remarks,b.Degree FROM ["&DBTable_SysUser &"] a LEFT JOIN ["&TablePre&"SysUserGroup] b ON (a.GroupID = b.ID) WHERE a.ID="&ID)
		If Not(Rs.Eof) Then
			IsContentExist = true
			Username  = Rs(0)
			RealName  = Rs(1)
			Email     = Rs(3)
			GroupID   = Rs(4)
			Pv_MemberID = Rs(5)
			UserDisable=Rs(6)
			Remarks   = Rs(7)
			Degree    = Rs(8)
		Else
			IsContentExist = false
		End If
		KnifeCMS.DB.CloseRs Rs
	End Function
	
	Private Function SystemUserList()
		Dim Fn_TempSql,Fn_ColumnNum
		Select Case DB_Type
		Case 0
		Fn_TempSql = "SELECT a.ID,a.Username,a.RealName,a.Email,b.GroupName,a.UserDisable,c.Username,a.LoginTimes,a.LastLoginTime,a.IP,a.Remarks FROM (["& DBTable_SysUser &"] a LEFT JOIN ["& DBTable_SysUserGroup &"] b ON (a.GroupID=b.ID)) LEFT JOIN ["& DBTable_Members &"] c ON (a.MemberID=c.ID)"
		Case 1
		Fn_TempSql = "SELECT a.ID,a.Username,a.RealName,a.Email,b.GroupName,a.UserDisable,c.Username,a.LoginTimes,a.LastLoginTime,a.IP,a.Remarks FROM ["& DBTable_SysUser &"] a LEFT JOIN ["& DBTable_SysUserGroup &"] b ON (a.GroupID=b.ID) LEFT JOIN ["& DBTable_Members &"] c ON (a.MemberID=c.ID)"
		End Select
			Fn_ColumnNum = 11
		If SubCtl = "recycle" Then
			Fn_TempSql = Fn_TempSql & " WHERE a.Recycle=1 ORDER BY b.Degree DESC,a.ID ASC"
		Else
			Fn_TempSql = Fn_TempSql & " WHERE a.Recycle=0 ORDER BY b.Degree DESC,a.ID ASC"
			Operation  = "<span class=""sysBtn""><a href=""?ctl="& Ctl &"&act=edit&id={$ID}"" ><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		End If
		Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
	End Function
	
	Private Function SystemUserDoSave()
		Dim TempGroupID,Fn_IsNeedChangePassword,Fn_IsFounder
		'获取数据并检查数据正确性
		Username   = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","Username"),50)
		RealName   = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","RealName"),50)
		Password   = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","Password"),20)
		RePassword = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","RePassword"),20)
		Email      = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","Email"),50)
		Remarks    = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","Remarks"),250)
		GroupID    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","GroupID"))
		Pv_MemberID= KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","MemberID"))
		UserDisable= KnifeCMS.Data.Left(KnifeCMS.Data.Int(KnifeCMS.GetForm("post","UserDisable")),1)
		If Username=""                      Then ErrMsg = Lang_SysUserCue(0) &"<br>"
		If RealName=""                      Then ErrMsg = ErrMsg & Lang_SysUserCue(1) &"<br>"
		If Not(KnifeCMS.Data.IsEmail(Email)) Then ErrMsg = ErrMsg & Lang_SysUserCue(4) &"<br>"
		'判断前台会员是否存在
		If Pv_MemberID>0 Then
			If Not(KnifeCMS.DB.CheckIsRecordExist(DBTable_Members,Array("ID:"& Pv_MemberID),"")) Then
				ErrMsg = ErrMsg & Lang_SysUserCue(12) &"<br>"
			End If
		Else
		    ErrMsg = ErrMsg & Lang_SysUserCue(12) &"<br>"
		End If
		'开始保存数据操作
		If Action="add" then
			If Len(Password)<6                  Then ErrMsg = ErrMsg & Lang_SysUserCue(2)&"<br>"
			If Password<>RePassword             Then ErrMsg = ErrMsg & Lang_SysUserCue(3)&"<br>"
			'如果前台会员已经被其他系统用户使用则提示错误.
			If KnifeCMS.DB.CheckIsRecordExist(DBTable_SysUser,Array("MemberID:"& Pv_MemberID),"") Then
				ErrMsg = ErrMsg & Lang_SysUserCue(15) &"<br>"
			End If
			If GroupID < 1  Then ErrMsg = ErrMsg & Lang_SysUserCue(5)&"<br>"
			If ErrMsg <> "" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
			Password = KnifeCMS.ParsePassWord(Password)
			'1.检查是否存在用户帐号相同的用户
			Set Rs = KnifeCMS.DB.GetRecord(DBTable_SysUser &":ID",Array("Username:"&Username),"")
			If Not(Rs.Eof and Rs.Bof) Then ErrMsg = Lang_SysUserCue(6) &":"& Username &"<br>"
			KnifeCMS.DB.CloseRs Rs
			If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
			'2.检查是否超越权限
			Degree = 0
			Set Rs = KnifeCMS.DB.GetRecord(TablePre&"SysUserGroup:Degree",Array("ID:"&GroupID),"")
			If Not(Rs.Eof) Then Degree=Rs(0)
			KnifeCMS.DB.CloseRs Rs
			Call SysUserDoAuthorityCheck(Degree)
			'3.保存数据
			Result = KnifeCMS.DB.AddRecord(DBTable_SysUser,Array("SysID:"& SysID,"Username:"&Username,"RealName:"&RealName,"Password:"&Password,"Email:"&Email,"GroupID:"&GroupID,"MemberID:"& Pv_MemberID,"UserDisable:"&UserDisable,"LoginTimes:0","RegisterTime:"&SysTime,"LastLoginTime:"&SysTime,"Remarks:"&Remarks,"Recycle:0"))
			
			If Result Then Call KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("SysUserID:"& KnifeCMS.DB.GetFieldByField(DBTable_SysUser,"ID","SysID",SysID)),Array("ID:"& Pv_MemberID))
			Msg    = Lang_SysUser(1) &"{"& Username &"}"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		Else
			'如果前台会员已经被其他系统用户使用则提示错误.
			If KnifeCMS.DB.CheckIsRecordExist(DBTable_SysUser,Array("MemberID:"& Pv_MemberID)," AND ID<>"& ID &"") Then
				ErrMsg = ErrMsg & Lang_SysUserCue(15) &"<br>"
			End If
			'如果密码留空则不修改密码
			If KnifeCMS.Data.IsNul(Password) Then
				Fn_IsNeedChangePassword = false
			Else
				Fn_IsNeedChangePassword = true
				If Len(Password)<6                  Then ErrMsg = ErrMsg & Lang_SysUserCue(2)&"<br>"
				If Password<>RePassword             Then ErrMsg = ErrMsg & Lang_SysUserCue(3)&"<br>"
			End If
			If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
			Password = KnifeCMS.ParsePassWord(Password)
			'2.检查是否越权编辑(不能编辑系统用户组等级比自己高的用户)
			Degree       = 0
			Fn_IsFounder = 0
			'取得当前编辑的用户的用户组ID和等级
			Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT b.ID,b.Degree,b.Founder FROM ["& DBTable_SysUser &"] a LEFT JOIN ["& DBTable_SysUserGroup &"] b ON (a.GroupID = b.ID) WHERE a.ID="&ID)
			If Not(Rs.Eof) Then
				TempGroupID  = Rs(0)
				Degree       = Rs(1)
				Fn_IsFounder = KnifeCMS.Data.CLng(Rs(2))
			End If
			KnifeCMS.DB.CloseRs Rs
			Call SysUserDoAuthorityCheck(Degree)
			'如果自身不是创始人的话则无权操作创始人组
			If KnifeCMS.Data.CLng(KnifeCMS.DB.GetFieldByField(DBTable_SysUserGroup,"Founder","ID",Admin.SysUserGroupID))=0 AND Fn_IsFounder=1 Then
				Admin.ShowMessage Lang_SysUserCue(17) &"{[ID="& ID &"]}",Url,1 : Exit Function
			End If
			'检查是否选择用户组(不能调整自己的系统用户组等级,创始人组不能修改系统用户组等级)
			If Admin.SysUserID=ID Then
				GroupID = TempGroupID
			End If
			If TempGroupID=1 Then
				GroupID = 1
			End If
			If GroupID < 1 AND TempGroupID<>1 Then ErrMsg = ErrMsg & Lang_SysUserCue(5)&"<br>"
			If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
			'3.检查是否存在用户帐号相同的其他用户
			Set Rs = KnifeCMS.DB.GetRecord(DBTable_SysUser,Array("Username:"&Username),"AND ID<>"&ID)
			If Not(Rs.Eof and Rs.Bof) Then ErrMsg = Lang_SysUserCue(6) &":"& Username &"<br>"
			KnifeCMS.DB.CloseRs Rs
			If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
			'4.保存数据
			If Fn_IsNeedChangePassword Then
				Result = KnifeCMS.DB.UpdateRecord(DBTable_SysUser,Array("Username:"&Username,"RealName:"&RealName,"Password:"&Password,"Email:"&Email,"GroupID:"& GroupID,"MemberID:"& Pv_MemberID,"UserDisable:"&UserDisable,"Remarks:"&Remarks),Array("ID:"&ID))
			Else
				Result = KnifeCMS.DB.UpdateRecord(DBTable_SysUser,Array("Username:"&Username,"RealName:"&RealName,"Email:"&Email,"GroupID:"& GroupID,"MemberID:"& Pv_MemberID,"UserDisable:"&UserDisable,"Remarks:"&Remarks),Array("ID:"&ID))
			End If
			If Result Then Call KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("SysUserID:"& ID),Array("ID:"& Pv_MemberID))
			Msg    = Lang_SysUser(2) &"{"& Username &"[ID="& ID &"]}"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		End If
	End Function
	
	Private Function SystemUserDelDo()
		SystemUserSubDo
	End Function
	
	Private Function SystemUserEditDo()
		SystemUserSubDo
	End Function
	
	Private Sub SystemUserSubDo()
		ID = KnifeCMS.GetForm("post","InputName")
		Dim ArrID : ArrID = Split(ID,",")
		Dim Fn_IsFounder
		For i=0 To Ubound(ArrID)
			ID = KnifeCMS.Data.Int(ArrID(i))
			Result    = 0
			If ID>0 Then
				If ID = Admin.SysUserID Then Admin.ShowMessage Lang_SysUserCue(8),Url,0 : Die ""
				'1.检查是否越权
				Degree       = 0
				Fn_IsFounder = 0
				Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT a.Username,b.Degree,b.Founder FROM ["& DBTable_SysUser &"] a LEFT JOIN ["& DBTable_SysUserGroup &"] b ON (a.GroupID = b.ID) WHERE a.ID="&ID)
				If Not(Rs.Eof) Then
					Username     = Rs(0)
					Degree       = Rs(1)
					Fn_IsFounder = KnifeCMS.Data.CLng(Rs(2))
				End If
				KnifeCMS.DB.CloseRs Rs
				Call SysUserDoAuthorityCheck(Degree)
				'如果自身不是创始人的话则无权操作创始人组
				If KnifeCMS.Data.CLng(KnifeCMS.DB.GetFieldByField(DBTable_SysUserGroup,"Founder","ID",Admin.SysUserGroupID))=0 AND Fn_IsFounder=1 Then
					Admin.ShowMessage Lang_SysUserCue(17) &"{"& Username &"[ID="& ID &"]}",Url,1 : Exit Sub
				Else
					'2.开始执行操作
					If Action = "del" Then
						If SubAction = "completely" Then
							Result = KnifeCMS.DB.DeleteRecord(DBTable_SysUser,Array("ID:"&ID))
							Msg    = Lang_SysUser(4)
						Else
							Result = KnifeCMS.DB.UpdateRecord(DBTable_SysUser,Array("Recycle:1"),Array("ID:"&ID))
							Msg    = Lang_SysUser(3)
						End If
					ElseIf Action = "edit" Then
						If SubAction = "revert" Then
							Result = KnifeCMS.DB.UpdateRecord(DBTable_SysUser,Array("Recycle:0"),Array("ID:"&ID))
							Msg    = Lang_SysUserCue(9)
						ElseIf SubAction = "forbid" Then
							Result = KnifeCMS.DB.UpdateRecord(DBTable_SysUser,Array("UserDisable:1"),Array("ID:"&ID))
							Msg    = Lang_SysUserCue(10)
						ElseIf SubAction = "enabled" Then
							Result = KnifeCMS.DB.UpdateRecord(DBTable_SysUser,Array("UserDisable:0"),Array("ID:"&ID))
							Msg    = Lang_SysUserCue(11)
						End If
					End If
				End If
			End If
			If Result And Username <> "" Then Admin.ShowMessage Msg &"{"& Username &"[ID="& ID &"]}",Url,1
		Next
	End Sub
	
	'$ 检查是否超越权限(不能添加、编辑、删除比您所在用户组级别更高的用户)
	Private Function SysUserDoAuthorityCheck(ByVal BeDo_SysUserDegree)
		If BeDo_SysUserDegree > Admin.SysUserDegree Then Admin.ShowMessage Lang_SysUserCue(7),"goback",0 : Die ""
	End Function
	
	'$ 获取您可以添加编辑的用户所属的用户组下拉列表
	Private Function GetSysUserGroup(ByVal SelectID,ByVal ListType)
		if ListType=0 then C_TempStr = C_TempStr & "<option value=0 selected>选择用户组...</option>"
		Set Rs = KnifeCMS.DB.GetRecord(DBTable_SysUserGroup &":ID,GroupName,Degree",Array(KnifeCMS.IIF(GroupID=1 AND Action="edit","Founder:1","Founder:0")),KnifeCMS.IIF(Admin.SysUserDegree>0,"AND Degree<="& Admin.SysUserDegree &" ","")&"ORDER BY Degree DESC")
		Do While Not Rs.eof
			C_TempStr = C_TempStr & "<option value='"&Rs(0)&"'"
			If SelectID>0 And SelectID=Rs(0) Then C_TempStr = C_TempStr & " selected"
			C_TempStr = C_TempStr & ">"
			C_TempStr = C_TempStr & Trim(Rs(1)) & "</option>"
			Rs.movenext
		Loop
		KnifeCMS.DB.CloseRs Rs
		Echo C_TempStr
	End function
	
	Private Function SystemUserView()
	%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
          <% If SubCtl = "recycle" Then %>
          <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:revert','')"><b class="icon icon_revert"></b><% Echo Lang_DoRevert %></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  <% Else %>
          <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=add" ><b class="icon icon_userAdd"></b><% echo Lang_DoAdd %></a></span>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_recycle"></b><% Echo Lang_DoDel%></button>
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:forbid','<% Echo Lang_DoForbidSysUserConfirm %>')"><b class="icon icon_userForbid"></b><% Echo Lang_DoForbid %></button>
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:enabled','<% Echo Lang_DoEnabledSysUserConfirm %>')"><b class="icon icon_userEnabled"></b><% Echo Lang_DoEnabled %></button>
				  </td></tr>
			  </table>
		  </td>
          <% End If %>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></th>
			<th><div><% Echo Lang_ListTableColLine_ID %></div></th>
			<th><div><% Echo Lang_SysUser_ColLine(3) %></div></th><!--用户帐号-->
			<th><div><% Echo Lang_SysUser_ColLine(4) %></div></th><!--真实姓名-->
			<th><div><% Echo Lang_SysUser_ColLine(5) %></div></th><!--电子邮件-->
			<th><div><% Echo Lang_SysUser_ColLine(1) %></div></th><!--用户组-->
			<th><div><% Echo Lang_SysUser_ColLine(6) %></div></th><!--状态-->
            <th><div><% Echo Lang_SysUser_ColLine(9) %></div></th><!--前台会员-->
			<th><div><% Echo Lang_SysUser_ColLine(7) %></div></th><!--登陆次数-->
			<th><div><% Echo Lang_SysUser_ColLine(8) %></div></th><!--上次登陆时间-->
            <th><div><% Echo Lang_SysUser_ColLine(10) %></div></th><!--上次登陆IP-->
			<th><div><% Echo Lang_ListTableColLine_Remarks %></div></th><!--备注-->
			<th><div><% Echo Lang_ListTableColLine_Do %></div></th><!--操作-->
			</tr>
			<% SystemUserList %>
		   </tbody>
		  </table>
		  </form>
		</div>
      </DIV>
	</DIV>
	<script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var content;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td5" && tds[i].innerHTML==1){
					tds[i].innerHTML='<b class=\"icon icon_userForbid\" title=\"<% Echo Lang_DoForbid %>\"></b>';
				}
				if(tds[i].id=="td5" && tds[i].innerHTML==0){
					tds[i].innerHTML='<b class=\"icon icon_userEnabled\" title=\"<% Echo Lang_Normal %>\"></b>';
				}
				if(tds[i].id=="td8"){
					content=tds[i].innerHTML;
					tds[i].innerHTML="<div class=\"height22 line-height22 overhid\">"+content+"</div>";
				}
			}
		}
	}
	ChangeTdText();
	</script>
	<%
	End Function
	
	'添加系统用户界面
	Private Function SystemUserDo()
	%>
	<DIV id="MainFrame">
	  <div id="wrap">
		<div class="tagLine"><% Echo NavigationLine %></div>
		<DIV class="cbody">
		  <div class="wbox">
			<form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
			  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
			  <tbody>
                 <tr><td class="titletd"><span class="required">*</span><% Echo Lang_SysUser(15) %>:</td>
					 <td class="infotd">
					 <input type="text" class="TxtClass" name="Username" id="Username" maxLength="50" value="<% Echo Username %>" style="width:160px;"/>
					 <label class="Normal" id="d_Username"></label>
					 </td>
                 </tr>
				 <tr><td class="titletd"><span class="required">*</span><% Echo Lang_SysUser(16) %>:</td>
					 <td class="infotd">
					 <input type="text" class="TxtClass"name="RealName" id="RealName" maxLength="50" value="<% Echo RealName %>" style="width:160px;"/>
					 <label class="Normal" id="d_RealName"></label>
					 </td></tr>
				 <tr><td class="titletd"><span class="required">*</span><% Echo Lang_SysUser(18) %>:</td>
					 <td class="infotd">
					 <input type="Password" class="TxtClass" name="Password" id="Password" maxLength="20" value="" style="width:160px;" />
					 <label class="Normal" id="d_Password"><% Echo Lang_SysUserCue(13) & KnifeCMS.IIF(Action="edit",Lang_SysUserCue(16),"")%></label>
					 </td></tr>
				 <tr><td class="titletd"><span class="required">*</span><% Echo Lang_SysUser(19) %>:</td>
					 <td class="infotd">
					 <input type="Password" class="TxtClass" name="RePassword" id="RePassword" maxLength="20"  value="" style="width:160px;" />
					 <label class="Normal" id="d_RePassword"></label>
					 </td></tr>
				 <tr><td class="titletd"><span class="required">*</span><% Echo Lang_SysUser(17) %>:</td>
					 <td class="infotd">
					 <input type="text" class="TxtClass" name="Email" id="Email" maxLength="50" value="<% Echo Email %>" style="width:160px;" />
					 <label class="Normal" id="d_Email"></label>
					 </td></tr>
				 <tr><td class="titletd"><span class="required">*</span><% Echo Lang_SysUser(20) %></td>
					 <td class="infotd">
					 <select name="GroupID" id="GroupID" <% Echo KnifeCMS.IIF((GroupID=1 AND Action="edit") Or Admin.SysUserID=ID,"disabled=""disabled""","")%> style="font-size:13px;">
					 <% Echo GetSysUserGroup(GroupID,0)%>
					 </select>
					 <label class="Normal" id="d_GroupID"></label>
					 </td></tr>
                 <tr><td class="titletd"><span class="required">*</span><% Echo Lang_SysUser(14) %>:</td>
					 <td class="infotd">
					 <input type="text" class="TxtClass" name="MemberID" id="MemberID" maxLength="10" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9]*/g'))" value="<% Echo Pv_MemberID %>" style="width:60px;"/>
                     <label class="Normal" id="d_MemberID"></label>
					 <label class="Normal"><%=Lang_SysUserCue(14)%></label>
					 </td>
                 </tr>
                 <tr><td class="titletd"><% Echo Lang_SysUser(21) %></td>
					 <td class="infotd"><input type="checkbox" name="UserDisable" id="UserDisable" value="1" <% Echo Admin.InputChecked(UserDisable)%> />
					 </td></tr>
				 <tr><td class="titletd"><% Echo Lang_SysUser(22) %></td>
					 <td class="infotd"><textarea class="remark" name="Remarks" id="Remarks"><% Echo Remarks%></textarea>
					 </td>
				 </tr>
			   </tbody>
			  </table>
			<div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
			  <input type="hidden" name="id" value="<% Echo ID %>" />
              <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
			  <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.SysUser.SaveUserCheck('<%=Action%>')"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
			  <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
			</div>
			</form>
		  </div>
		</DIV>
	  </div>
	</DIV>
	<%
	End Function
End Class
%>
