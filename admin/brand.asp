<!--#include file="config.admin.asp"-->
<%
Dim Brand
Set Brand = New Class_KnifeCMS_Admin_Brand
Set Brand = Nothing
Class Class_KnifeCMS_Admin_Brand

	Private BrandName,BrandLogo,BrandUrl,OrderNum,Abstract,Intro
	Private SeoTitle,SeoUrl,MetaKeywords,MetaDescription
	Private GetAbstract
	Private DBTable_Goods
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "brand.lang.asp"
		DBTable                     = TablePre&"Brand"
		DBTable_Goods               = TablePre&"Goods"
		Header()
		Select Case Action
			Case "view"
				BrandView
			Case "add","edit"
				If SubAction="save" Then
					BrandDoSave
				Else
					If Action = "add" Then
						BrandDo
					ElseIf Action = "edit" Then
						Set Rs = KnifeCMS.DB.GetRecord(DBTable&":SysID,BrandName,BrandLogo,BrandUrl,OrderNum,Abstract,Intro,MetaKeywords,MetaDescription",Array("ID:"&ID),"")
						If Not(Rs.Eof) Then
							SysID     = Rs("SysID")
							BrandName = Rs("BrandName")
							BrandLogo = Rs("BrandLogo")
							BrandUrl  = Rs("BrandUrl")
							OrderNum  = Rs("OrderNum")
							Abstract  = Rs("Abstract")
							Intro     = Rs("Intro")
							MetaKeywords    = Rs("MetaKeywords")
							MetaDescription = Rs("MetaDescription")
						Else
							Admin.ShowMessage Lang_Brand_Cue(7),"goback",0 : Die ""
						End IF
						KnifeCMS.DB.CloseRs Rs
						BrandDo
					End IF
				End IF
			Case "del"  : BrandDel
		End Select
		Footer()
	End Sub
	
	Private Function BrandList()
		Dim Fn_TempSql,Fn_ColumnNum
		Fn_TempSql = "SELECT a.ID,a.OrderNum,a.BrandName,(SELECT COUNT(*) FROM ["&DBTable_Goods&"] b WHERE b.BrandID=a.ID),a.BrandLogo,a.BrandUrl FROM ["&DBTable&"] a "
		If SubCtl = "recycle" Then
			Fn_TempSql = Fn_TempSql & "WHERE Recycle=1 ORDER BY a.OrderNum ASC , a.ID DESC"
		Else
			Fn_TempSql = Fn_TempSql & "WHERE Recycle=0 ORDER BY a.OrderNum ASC , a.ID DESC"
			Operation = "<span class=""sysBtn""><a href=""goods.asp?ctl=goods&act=view&subact=search&brandid={$ID}""><b class=""icon icon_view""></b>"& Lang_Brand(7) &"</a></span>"
			Operation = Operation & "<span class=""sysBtn""><a href=""javascript:void(0);"" onClick=""ShowModal('brand.asp?ctl="&Ctl&"&act=edit&id={$ID}',940,600);ShowModalReload();""><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		End If
		Fn_ColumnNum = 6
		Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
	End Function
	
	Private Function BrandDoSave()
		'获取数据并处理数据
		BrandName   = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","BrandName")),250)
		OrderNum    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","OrderNum"))
		BrandLogo   = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","BrandLogo")),250)
		BrandUrl    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","BrandUrl")),250)
		GetAbstract = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","GetAbstract"))
		Intro       = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","TextContent")),100000)
		If GetAbstract Then
		  Abstract = KnifeCMS.Data.Left(Data.HtmlTextTreat(Intro,false,false,false),250)
		Else
		  Abstract = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Abstract")),250)
		End If
		MetaKeywords    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","MetaKeywords")),250)
		MetaDescription = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","MetaDescription")),250)
		'数据合法性检测并处理数据
		If KnifeCMS.Data.IsNul(BrandName) Then ErrMsg = Lang_Brand_Cue(8) &"<br>"
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		IF Action="add" Then
			Result = KnifeCMS.DB.AddRecord(DBTable,Array("SysID:"&SysID,"BrandName:"& BrandName,"BrandLogo:"& BrandLogo,"BrandUrl:"& BrandUrl,"OrderNum:"& OrderNum,"Abstract:"& Abstract,"Intro:"& Intro,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription,"Recycle:0"))
			ID  = Admin.GetIDBySysID(DBTable,SysID)
			Msg = Lang_Brand_Cue(1) &"<br>["& Lang_Brand(1) &":"& BrandName &"]<br>[ID:"& ID &"]"
			If Result Then Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		ElseIf Action="edit" then
			Result = KnifeCMS.DB.UpdateRecord(DBTable,Array("BrandName:"& BrandName,"BrandLogo:"& BrandLogo,"BrandUrl:"& BrandUrl,"OrderNum:"& OrderNum,"Abstract:"& Abstract,"Intro:"& Intro,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription),Array("ID:"&ID))
			Msg = Lang_Brand_Cue(2) &"<br>["& Lang_Brand(1) &":"& BrandName &"]<br>[ID:"& ID &"]"
			If Result Then Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		End IF
	End Function
	
	Private Function BrandDel()
		ID = KnifeCMS.GetForm("post","InputName")
		If KnifeCMS.Data.IsNul(Replace(ID,",","")) Then Exit Function
		Dim Fn_ArrID,Fn_ID,Fn_i,Fn_Rs
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			If ID > 0 Then
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable&":BrandName",Array("ID:"&ID),"")
				If Not(Fn_Rs.Eof) Then BrandName=Fn_Rs(0)
				KnifeCMS.DB.CloseRs Fn_Rs
				'开始执行操作
				If Action = "del" Then
					If SubAction = "completely" Then
						Result = KnifeCMS.DB.DeleteRecord(DBTable,Array("ID:"&ID))
						Msg    = Msg & Lang_Brand_Cue(4) &"{"& BrandName &"[ID="& ID &"]}<br>"
					Else
						Result = KnifeCMS.DB.UpdateRecord(DBTable,Array("Recycle:1"),Array("ID:"&ID))
						Msg    = Msg & Lang_Brand_Cue(3) &"{"& BrandName &"[ID="& ID &"]}<br>"
					End If
				ElseIf Action = "edit" Then
					If SubAction = "revert" Then
						Result = KnifeCMS.DB.UpdateRecord(DBTable,Array("Recycle:0"),Array("ID:"&ID))
						Msg    = Msg & Lang_Brand_Cue(5) &"{"& BrandName &"[ID="& ID &"]}<br>"
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Function
	
	Private Function BrandView()
%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <% If SubCtl = "recycle" Then %>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:revert','')"><b class="icon icon_revert"></b><% Echo Lang_DoRevert %></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely %></button>
				  </td></tr>
			  </table>
		  </td>
		  <% Else %>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
                  <span class="sysBtn"><a href="javascript:ShowModal('?ctl=<% Echo Ctl %>&act=add',940,600);ShowModalReload();"><b class="icon icon_add"></b><% Echo Lang_DoAdd %></a></span>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','')"><b class="icon icon_recycle"></b><% Echo Lang_DoDel%></button>
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  <% End If %>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th width="1%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
			<th width="5%"><div style="width:32px;"><% Echo Lang_Brand_ListTableColLine(0) %><!--排序--></div></th>
            <th width="25%"><div style="width:150px;"><% Echo Lang_Brand_ListTableColLine(1) %><!--品牌名称--></div></th>
            <th width="5%"><div style="width:60px;"><% Echo Lang_Brand_ListTableColLine(2) %><!--商品数量--></div></th>
            <th width="15%"><div style="width:100px"><% Echo Lang_Brand_ListTableColLine(3) %><!--品牌LOGO--></div></th>
            <th width="25%"><div style="width:200px"><% Echo Lang_Brand_ListTableColLine(4) %><!--品牌URL--></div></th>
			<th><div style="width:60px;"><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% BrandList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
	<script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td4" && tds[i].innerHTML!=""){
					TempHTML = "<img class=\"BrandLogo\" src='"+ unescape(tds[i].innerHTML) +"' />";
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td5" && tds[i].innerHTML!=""){
					url=tds[i].innerHTML;
					tds[i].innerHTML="";
					tds[i].innerHTML="<a href=\""+url+"\" target=\"_blank\">"+url+"</a>"
				}
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
	Private Function BrandDo()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
            <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable" id="Tabs_1">
              <tbody>
               <tr><td class="titletd"><% Echo Lang_Brand(1) %><!--品牌名称-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="BrandName" name="BrandName" value="<%=BrandName%>" />
                   <label id="d_BrandName"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Brand(2) %><!--排序-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="OrderNum" name="OrderNum" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9]*/g','maxLength:5'))" style="width:50px;" value="<%=OrderNum%>" />
                   <label class="Normal"><% Echo Lang_Brand_Cue(9) %><!--数字越小越靠前--></label>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Brand(3) %><!--品牌网址-->:</td>
                   <td class="infotd"><input type="text" class="TxtClass long" id="BrandUrl" name="BrandUrl" value="<%=BrandUrl%>" /><label id="d_BrandUrl"></label></td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Brand(4) %><!--品牌LOGO-->:</td>
                   <td class="infotd">
                   <span id="BrandLogoBox" style="margin-right:5px;"><% if not(KnifeCMS.Data.IsNul(BrandLogo)) Then Response.Write("<img src="""&BrandLogo&""" class=""BrandLogo"" />") %></span>
                   <span class="sysBtn"><a href="javascript:void(0)" onclick="UploadLogo()"><b class="icon icon_add"></b>上传LOGO...</a></span>
                   <input type="hidden" class="TxtClass long" id="BrandLogo" name="BrandLogo" value="<%=BrandLogo%>" />
                   <label id="d_BrandLogo"></label></td>
               </tr>
               <tr><td class="titletd" valign="top"><% Echo Lang_Brand(5) %><!--摘要介绍-->:</td>
                   <td class="infotd">
                   <textarea class="abstract" id="Abstract" name="Abstract"><%=Abstract%></textarea><input type="checkbox" id="GetAbstract" name="GetAbstract" onclick='SetInputDisabled(1,"GetAbstract","Abstract")' value="1"/><font class="explain"><% Echo Lang_Brand_Cue(10) %><!--自动获取详细介绍的前200个字作为摘要介绍.--></font>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Brand(6) %><!--详细介绍-->:</td>
                   <td class="infotd">
                   <textarea class="TextContent" id="TextContent" name="TextContent"><%=KnifeCMS.Data.HTMLDecode(Intro)%></textarea>
                   <label class="Normal"></label>
                   </td>
               </tr>
              </tbody>
            </table>
            </div>
            <div class="inbox" style="display:none;">
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
              <tbody>
                 <tr><td class="titletd" valign="top"><% Echo Lang_SeoTitle %></td><td class="infotd">
                     <input type="text" class="TxtClass long" name="SeoTitle" id="SeoTitle" maxlength="250" value="<%=KnifeCMS.Data.ReplaceString(SeoTitle,"&nbsp;",Chr(32))%>"/> <label class="Normal" id="d_SeoTitle"><% Echo Lang_SeoTitleCue %></label>
                     </td></tr>
                 <tr><td class="titletd" valign="top"><% Echo Lang_SeoUrl %></td><td class="infotd">
                     <input type="text" class="TxtClass readonly" name="url_pre" readonly="readonly" style="width:368px;" value="<% Echo SiteURL & SystemPath & "?brand/"& KnifeCMS.IIF(ID>0,ID,"{id}") &"/" %>"/>
                     <input type="text" class="TxtClass" name="SeoUrl" id="SeoUrl" maxlength="250"  style="width:160px;" value="<%=SeoUrl%>"/>
                     <input type="text" class="TxtClass readonly" name="url_suffix" readonly="readonly" style="width:40px;" value="<% Echo FileSuffix %>"/>
                     <label class="Normal" id="d_SeoUrl"><% Echo Lang_SeoUrlCue %></label>
                     </td></tr>
                 <tr><td class="titletd" valign="top"><% Echo Lang_MetaKeywords %></td><td class="infotd">
                     <input type="text" class="TxtClass long" name="MetaKeywords" id="MetaKeywords" maxlength="250" value="<%=KnifeCMS.Data.ReplaceString(MetaKeywords,"&nbsp;",Chr(32))%>"/> <label class="Normal" id="d_MetaKeywords"><% Echo Lang_MetaKeywordsCue %></label>
                     </td></tr>
                 <tr><td  class="titletd" valign="top"><% Echo Lang_MetaDescription %></td><td class="infotd">
                     <textarea class="metadescription" name="MetaDescription" id="MetaDescription" maxlength="250" onblur="KnifeCMS.onblur(this,'maxLength:250')" ><%=KnifeCMS.Data.ReplaceString(MetaDescription,"&nbsp;",Chr(32))%></textarea>
                     <label class="Normal" id="d_MetaDescription"><% Echo Lang_MetaDescriptionCue %></label>
                     </td>
                 </tr>
              </tbody>
              </table>
            </div>
            <input type="hidden" name="id" value="<%=ID%>" />
            <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
            <div class="bottomSaveline" id="SubmitButtoms">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Goods.SaveBrandCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
            </div>
            </form>
        </div>
      </DIV>
    </DIV>
    <script type="text/javascript" charset="utf-8">
	window.UEDITOR_HOME_URL = "<%=SystemPath%>plugins/ueditor/";
	</script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_config.asp"></script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_api.js"></script>
    <script type="text/javascript">
	var OSWebEditor=UE.getEditor('TextContent');
	</script>
	<script type="text/javascript">
	//$:上传品牌Logo
	function UploadLogo(){
		var NewWindow;
		var TargetArea=document.getElementById("BrandLogoBox");
		var Input=document.getElementById("BrandLogo");
		if(TargetArea!=null && Input!=null){
			NewWindow=window.showModalDialog('fileupload.asp?ctl=file_image&act=view&insertnum=1&inserttype=url',window,'dialogWidth:740px;dialogHeight:550px;center:Yes;help:no;resizable:yes;scroll:yes;');
			if(NewWindow!=null){
				TargetArea.innerHTML="";
				var img=document.createElement("img");
				img.src=unescape(NewWindow);
				img.className="BrandLogo";
				TargetArea.appendChild(img);
				Input.value=unescape(NewWindow);
				}
			}else{
				alert("Alert: UploadLogo(){ Can't Found Tag Width id=BrandLogoBox Or id=BrandLogo! }");
		}
	}
    </script>
<%
	End Function
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
End Class
%>