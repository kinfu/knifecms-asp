<!--#include file="config.admin.asp"-->
<%
Dim Ads
Set Ads = New Class_KnifeCMS_Admin_Ads
Set Ads = Nothing
Class Class_KnifeCMS_Admin_Ads

	Private AdsDefault,AdsGroupName,Identify,StartTime,EndTime,Width,Height,OrderNum,Remarks,Disabled
	Private AdsGroupID,AdsTitle,AdsUrl,AdsTxt,ImageUrl
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "ads.lang.asp"
		AdsGroupID = KnifeCMS.Data.CLng(KnifeCMS.GetForm("both","adsgroupid"))
		Header()
		If Ctl="adsgroup" Or Ctl="adsgroup_recycle" Then
			Select Case Action
				Case "view"
					AdsGroupView
				Case "add","edit"
					If SubAction="save" Then
						AdsGroupDoSave
					Else
						If Action = "add" Then
							AdsGroupDo
						ElseIf Action = "edit" Then
							If SubAction = "revert" Or SubAction = "disabled" Or SubAction = "undisabled" Then
								AdsGroupOtherEdit
							Else
								Set Rs = KnifeCMS.DB.GetRecord(DBTable_Ads_Group&":AdsDefault,AdsGroupName,Identify,StartTime,EndTime,Width,Height,OrderNum,Remarks",Array("ID:"&ID),"")
								If Not(Rs.Eof) Then
									AdsDefault   = Rs("AdsDefault")
									AdsGroupName = Rs("AdsGroupName")
									Identify     = Rs("Identify")
									StartTime    = Rs("StartTime")
									EndTime      = Rs("EndTime")
									Width        = Rs("Width")
									Height       = Rs("Height")
									OrderNum     = Rs("OrderNum")
									Remarks      = Rs("Remarks")
								Else
									Admin.ShowMessage Lang_Ads_Cue(7),"goback",0 : Die ""
								End IF
								KnifeCMS.DB.CloseRs Rs
								AdsGroupDo
							End IF
						End IF
					End IF
				Case "del"  : AdsGroupDel
			End Select
		ElseIf Ctl="adsdetail" then
			Select Case Action
				Case "view"
					AdsDetailView
				Case "add","edit"
					If SubAction="save" Then
						AdsDetailDoSave
					Else
						If Action = "add" Then
							AdsDetailDo
						ElseIf Action = "edit" Then
							If SubAction = "revert" Or SubAction = "disabled" Or SubAction = "undisabled" Then
								AdsDetailOtherEdit
							Else
								Set Rs = KnifeCMS.DB.GetRecord(DBTable_Ads_Detail&":AdsGroupID,AdsTitle,AdsUrl,AdsTxt,ImageConfig,OrderNum",Array("ID:"&ID),"")
								If Not(Rs.Eof) Then
									AdsGroupID = Rs("AdsGroupID")
									AdsTitle   = Rs("AdsTitle")
									AdsUrl     = Rs("AdsUrl")
									AdsTxt     = Rs("AdsTxt")
									ImageUrl   = Rs("ImageConfig")
									OrderNum   = Rs("OrderNum")
								Else
									Admin.ShowMessage Lang_Ads_Detail_Cue(7),"goback",0 : Die ""
								End IF
								KnifeCMS.DB.CloseRs Rs
								AdsDetailDo
							End IF
						End IF
					End IF
				Case "del"  : AdsDetailDel
			End Select
		Else
		    Admin.ShowMessage Lang_IllegalSysPra,"goback",0
		End If
		Footer()
	End Sub
	
	Private Function AdsGroupList()
		Dim Fn_TempSql,Fn_ColumnNum
		Select Case DB_Type
		Case 0
		Fn_TempSql = "SELECT a.ID,a.AdsGroupName,a.Identify,a.AdsDefault,IIF(a.Recycle=0,a.Disabled,a.Recycle),a.StartTime,a.EndTime,a.Width,a.Height,a.OrderNum FROM ["&DBTable_Ads_Group&"] a "
		Case 1
		Fn_TempSql = "SELECT a.ID,a.AdsGroupName,a.Identify,a.AdsDefault,CASE WHEN a.Recycle=0 THEN a.Disabled ELSE a.Recycle END,a.StartTime,a.EndTime,a.Width,a.Height,a.OrderNum FROM ["&DBTable_Ads_Group&"] a "
		End Select
		If SubCtl = "recycle" Then
			Fn_TempSql = Fn_TempSql & "WHERE Recycle=1 ORDER BY a.OrderNum ASC , a.ID ASC"
		Else
			Fn_TempSql = Fn_TempSql & "WHERE Recycle=0 ORDER BY a.OrderNum ASC , a.ID ASC"
			Operation = "<span class=""sysBtn""><a href=""javascript:;"" onClick=""ShowModal('public.asp?ctl=adsdetail&act=view&adsgroupid={$ID}',1024,700);""><b class=""icon icon_view""></b>"& Lang_Ads_ListTableColLine(10) &"</a></span>"
			Operation = Operation & "<span class=""sysBtn""><a href=""public.asp?ctl="&Ctl&"&act=edit&id={$ID}""><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		End If
		Fn_ColumnNum = 10
		Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
	End Function
	
	Private Function AdsGroupDoSave()
		Dim Fn_Rs,Fn_Value,Fn_Check : Fn_Check=True
		'获取数据并处理数据
		AdsGroupName = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","AdsGroupName")),100)
		Identify     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","Identify"),"[^0-9a-zA-Z-_]",""),20)
		StartTime    = KnifeCMS.Data.FormatDateTime(KnifeCMS.GetForm("post","StartTime"),0)
		EndTime      = KnifeCMS.Data.FormatDateTime(KnifeCMS.GetForm("post","EndTime"),0)
		Width        = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Width"))
		Height       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Height"))
		OrderNum     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","OrderNum"))
		Remarks      = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","Remarks"),10000)
		'数据合法性检测并处理数据
		If KnifeCMS.Data.IsNul(AdsGroupName) Then ErrMsg = ErrMsg & Lang_Ads_Cue(8) &"<br>"
		If KnifeCMS.Data.IsNul(StartTime)    Then ErrMsg = ErrMsg & Lang_Ads_Cue(10) &"<br>"
		If KnifeCMS.Data.IsNul(EndTime)      Then ErrMsg = ErrMsg & Lang_Ads_Cue(11) &"<br>"
		If DateDiff("d",StartTime,EndTime)<0 Then ErrMsg = ErrMsg & Lang_Ads_Cue(15) &"<br>"
		If Not(Width>0)                     Then ErrMsg = ErrMsg & Lang_Ads_Cue(12) &"<br>"
		If Not(Height>0)                    Then ErrMsg = ErrMsg & Lang_Ads_Cue(13) &"<br>"
		IF Action="add" Then
			'检查是否存在标识一样的广告位
			Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Ads_Group&":AdsGroupName",Array("Identify:"&Identify),"")
			If Not(Fn_Rs.Eof) Then Fn_Check=False
			KnifeCMS.DB.CloseRs Fn_Rs
			If KnifeCMS.Data.IsNul(Identify) Then ErrMsg = ErrMsg & Lang_Ads_Cue(9) &"<br>"
			If Not(Fn_Check) Then ErrMsg = ErrMsg & Lang_Ads_Cue(14) &"<br>"
			If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
			Result = KnifeCMS.DB.AddRecord(DBTable_Ads_Group,Array("SysID:"&SysID,"AdsDefault:0","AdsGroupName:"& AdsGroupName,"Identify:"& Identify,"StartTime:"& StartTime,"EndTime:"& EndTime,"Width:"& Width,"Height:"& Height,"OrderNum:"& OrderNum,"Disabled:0","Remarks:"& Remarks,"Recycle:0"))
			ID  = Admin.GetIDBySysID(DBTable_Ads_Group,SysID)
			Msg = Lang_Ads_Cue(1) &"["& AdsGroupName &"][ID:"& ID &"]"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		ElseIf Action="edit" then
			'检查广告位是否存在
			Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Ads_Group&":AdsDefault",Array("ID:"&ID),"")
			If Not(Fn_Rs.Eof) Then
				AdsDefault = Fn_Rs(0)
			Else
				Fn_Check=False
			End If
			KnifeCMS.DB.CloseRs Fn_Rs
			AdsDefault = KnifeCMS.Data.CLng(AdsDefault)
			If AdsDefault=0 And KnifeCMS.Data.IsNul(Identify) Then ErrMsg = ErrMsg & Lang_Ads_Cue(9) &"<br>"
			If Not(Fn_Check) Then ErrMsg = ErrMsg & Lang_IllegalSysPra &"<br>"
			'检查是否存在标识一样的广告位
			Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Ads_Group&":AdsGroupName",Array("Identify:"&Identify),"AND ID<>"&ID&"")
			If Not(Fn_Rs.Eof) Then Fn_Check=False
			KnifeCMS.DB.CloseRs Fn_Rs
			If Not(Fn_Check) Then ErrMsg = ErrMsg & Lang_Ads_Cue(14) &"<br>"
			If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
			If AdsDefault = 1 Then
				Fn_Value = Array("AdsGroupName:"& AdsGroupName,"StartTime:"& StartTime,"EndTime:"& EndTime,"Width:"& Width,"Height:"& Height,"OrderNum:"& OrderNum,"Remarks:"& Remarks)
			Else
				Fn_Value = Array("AdsGroupName:"& AdsGroupName,"Identify:"& Identify,"StartTime:"& StartTime,"EndTime:"& EndTime,"Width:"& Width,"Height:"& Height,"OrderNum:"& OrderNum,"Remarks:"& Remarks)
			End If
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Ads_Group,Fn_Value,Array("ID:"&ID))
			Msg = Lang_Ads_Cue(2) &"["& AdsGroupName &"][ID:"& ID &"]"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		End IF
	End Function
	
	Private Function AdsGroupDel()
		AdsGroupSubDo
	End Function
	Private  Function AdsGroupOtherEdit()
		AdsGroupSubDo
	End Function
	
	Private Function AdsGroupSubDo()
		ID = KnifeCMS.GetForm("post","InputName")
		If KnifeCMS.Data.IsNul(Replace(ID,",","")) Then Exit Function
		Dim Fn_ArrID,Fn_ID,Fn_i,Fn_Rs,Fn_Check
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			If ID > 0 Then
				Fn_Check = False
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Ads_Group&":AdsDefault,AdsGroupName",Array("ID:"&ID),"")
				If Not(Fn_Rs.Eof) Then AdsDefault=Fn_Rs(0) : AdsGroupName=Fn_Rs(1) : Fn_Check = True
				KnifeCMS.DB.CloseRs Fn_Rs
				AdsDefault = KnifeCMS.Data.CLng(AdsDefault)
				If Fn_Check Then
					'开始执行操作
					If Action = "del" Then
						If SubAction = "completely" Then
							If AdsDefault = 0 Then
								Result = KnifeCMS.DB.DeleteRecord(DBTable_Ads_Group,Array("ID:"&ID))
								Result = KnifeCMS.DB.DeleteRecord(DBTable_Ads_Detail,Array("AdsGroupID:"&ID))
								Msg    = Msg & Lang_Ads_Cue(4) &"{"& AdsGroupName &"[ID="& ID &"]}<br>"
							Else
								Msg    = Msg & Lang_Ads_Cue(16) &"{"& AdsGroupName &"[ID="& ID &"]}<br>"
							End If
						Else
							Result = KnifeCMS.DB.UpdateRecord(DBTable_Ads_Group,Array("Recycle:1"),Array("ID:"&ID))
							Msg    = Msg & Lang_Ads_Cue(3) &"{"& AdsGroupName &"[ID="& ID &"]}<br>"
						End If
					ElseIf Action = "edit" Then
						If SubAction = "revert" Then
							Result = KnifeCMS.DB.UpdateRecord(DBTable_Ads_Group,Array("Recycle:0"),Array("ID:"&ID))
							Msg    = Msg & Lang_Ads_Cue(5) &"{"& AdsGroupName &"[ID="& ID &"]}<br>"
						ElseIf SubAction = "undisabled" Then
							Result = KnifeCMS.DB.UpdateRecord(DBTable_Ads_Group,Array("Disabled:0"),Array("ID:"&ID))
							Msg    = Msg & Lang_Ads_Cue(17) &"{"& AdsGroupName &"[ID="& ID &"]}<br>"
						ElseIf SubAction = "disabled" Then
							Result = KnifeCMS.DB.UpdateRecord(DBTable_Ads_Group,Array("Disabled:1"),Array("ID:"&ID))
							Msg    = Msg & Lang_Ads_Cue(18) &"{"& AdsGroupName &"[ID="& ID &"]}<br>"
						End If
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Function
	
	Private Function AdsDetailList()
		Dim Fn_TempSql,Fn_ColumnNum
		Fn_TempSql = "SELECT a.ID,a.AdsTitle,a.AdsUrl,a.ImageConfig,a.AdsTxt,a.OrderNum FROM ["&DBTable_Ads_Detail&"] a WHERE AdsGroupID="&AdsGroupID&" AND "
		If SubCtl = "recycle" Then
			Fn_TempSql = Fn_TempSql & "Recycle=1 ORDER BY a.OrderNum ASC , a.ID ASC"
		Else
			Fn_TempSql = Fn_TempSql & "Recycle=0 ORDER BY a.OrderNum ASC , a.ID ASC"
			Operation = Operation & "<span class=""sysBtn""><a href=""javascript:ShowModal('public.asp?ctl="&Ctl&"&act=edit&id={$ID}',1000,620);ShowModalReload();""><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		End If
		Fn_ColumnNum = 6
		Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
	End Function
	
	Private Function AdsDetailDoSave()
		Dim Fn_Rs,Fn_Check : Fn_Check=True
		'获取数据并处理数据
		AdsTitle    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","AdsTitle")),250)
		AdsUrl      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","AdsUrl")),250)
		AdsTxt      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","TextContent")),100000)
		ImageUrl    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ImageUrl")),250)
		OrderNum    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","OrderNum"))
		'数据合法性检测并处理数据
		If KnifeCMS.Data.IsNul(ImageUrl) Then ErrMsg = ErrMsg & Lang_Ads_Detail_Cue(8) &"<br>"
		If Not(AdsGroupID>0) Then ErrMsg = ErrMsg & Lang_Ads_Detail_Cue(9) &"<br>"
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		'检查广告位是否存在
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Ads_Group&":AdsGroupName",Array("ID:"&AdsGroupID),"")
		If Fn_Rs.Eof Then Fn_Check=False
		KnifeCMS.DB.CloseRs Fn_Rs
		If Not(Fn_Check) Then ErrMsg = ErrMsg & Lang_Ads_Detail_Cue(9) &"<br>"
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		IF Action="add" Then
			SysID = KnifeCMS.CreateSysID
			Result = KnifeCMS.DB.AddRecord(DBTable_Ads_Detail,Array("SysID:"&SysID,"AdsGroupID:"&AdsGroupID,"AdsTitle:"& AdsTitle,"AdsUrl:"& AdsUrl,"AdsTxt:"& AdsTxt,"ImageConfig:"& ImageUrl,"OrderNum:"& OrderNum,"Disabled:0","Recycle:0"))
			ID  = Admin.GetIDBySysID(DBTable_Ads_Detail,SysID)
			Msg = Lang_Ads_Detail_Cue(1) &"["& AdsTitle &"][ID:"& ID &"]"
			If Result Then Admin.ReloadTopDialog : Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		ElseIf Action="edit" then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Ads_Detail,Array("AdsTitle:"& AdsTitle,"AdsUrl:"& AdsUrl,"AdsTxt:"& AdsTxt,"ImageConfig:"& ImageUrl,"OrderNum:"& OrderNum),Array("ID:"&ID))
			Msg = Lang_Ads_Detail_Cue(2) &"["& AdsTitle&"][ID:"& ID &"]"
			If Result Then Admin.ReloadTopDialog : Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		End IF
	End Function
	
	Private Function AdsDetailDel()
		AdsDetailSubDo
	End Function
	
	Private Function AdsDetailSubDo()
		ID = KnifeCMS.GetForm("post","InputName")
		If KnifeCMS.Data.IsNul(Replace(ID,",","")) Then Exit Function
		Dim Fn_ArrID,Fn_ID,Fn_i,Fn_Rs,Fn_Check
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			If ID > 0 Then
				Fn_Check = False
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Ads_Detail&":AdsTitle",Array("ID:"&ID),"")
				If Not(Fn_Rs.Eof) Then AdsTitle=Fn_Rs(0) : Fn_Check = True
				KnifeCMS.DB.CloseRs Fn_Rs
				If Fn_Check Then
					'开始执行操作
					If Action = "del" Then
						Result = KnifeCMS.DB.DeleteRecord(DBTable_Ads_Detail,Array("ID:"&ID))
						Msg    = Msg & Lang_Ads_Detail_Cue(3) &"{"& AdsGroupName &"[ID="& ID &"]}<br>"
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Function
	
	Private Function AdsGroupView()
%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <% If SubCtl = "recycle" Then %>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:revert','')"><b class="icon icon_revert"></b><% Echo Lang_DoRevert %></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  <% Else %>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
                  <span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=add"><b class="icon icon_add"></b><% Echo Lang_DoAdd & Lang_Ads(1) %></a></span>
				  </td></tr>
			  </table>
		  </td>
          <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:undisabled','')"><b class="icon icon_start"></b><% Echo Lang_DoOnRuning %></button>
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:disabled','')"><b class="icon icon_stop"></b><% Echo Lang_DoStopRuning %></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','')"><b class="icon icon_recycle"></b><% Echo Lang_DoDel%></button>
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  <% End If %>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th width="1%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
            <th width="20%"><div style="width:100px;"><% Echo Lang_Ads_ListTableColLine(1) %><!--广告位名称--></div></th>
			<th width="12%"><div style="width:100px;"><% Echo Lang_Ads_ListTableColLine(2) %><!--标识--></div></th>
            <th width="6%"><div style="width:56px;"><% Echo Lang_Ads_ListTableColLine(3) %><!--系统默认--></div></th>
            <th width="5%"><div style="width:52px"><% Echo Lang_Ads_ListTableColLine(4) %><!--状态--></div></th>
            <th width="13%"><div style="width:100px;"><% Echo Lang_Ads_ListTableColLine(5) %><!--开始时间--></div></th>
            <th width="13%"><div style="width:100px"><% Echo Lang_Ads_ListTableColLine(6) %><!--结束时间--></div></th>
            <th width="3%"><div style="width:30px"><% Echo Lang_Ads_ListTableColLine(7) %><!--宽--></div></th>
            <th width="3%"><div style="width:30px"><% Echo Lang_Ads_ListTableColLine(8) %><!--高--></div></th>
            <th width="3%"><div style="width:30px"><% Echo Lang_Ads_ListTableColLine(9) %><!--排序--></div></th>
			<th><div style="width:60px;"><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% AdsGroupList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
	<script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td3"){
					TempHTML = tds[i].innerHTML;
					if(TempHTML=="0"){tds[i].innerHTML="<span style=\"color:#888;\">"+Lang_Js_No+"</span>";}else if(TempHTML=="1"){ tds[i].innerHTML=Lang_Js_Yes;}
				}
				if(tds[i].id=="td4"){
					TempHTML = tds[i].innerHTML;
					if(TempHTML=="0"){tds[i].innerHTML="<span style=\"color:#048E08;\">"+Lang_Js_OnRunning+"</span>";}else if(TempHTML=="1"){ tds[i].innerHTML="<span style=\"color:#CD2E03;\">"+Lang_Js_HaveStopRunning+"</span>"}
				}
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
	Private Function AdsGroupDo()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
          <div class="inbox">
            <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable" id="Tabs_1">
              <tbody>
               <tr><td class="titletd"><%=Lang_Ads(2) %><!--广告位名称-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="AdsGroupName" name="AdsGroupName" value="<%=AdsGroupName%>" />
                   <label id="d_AdsGroupName"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_Ads(3) %><!--广告标识-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="Identify" name="Identify" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9a-zA-Z-_]*/g','maxLength:20'))" value="<%=Identify%>" />
                   <label class="Normal"><%=Lang_Ads_Cue(19) %><!--"英文数字-_"的组合,如: index_ads_01--></label>
                   <div><label class="Normal" id="d_Identify"></label></div>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_Ads(4) %><!--开始时间-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtSer dateStart" id="StartTime" name="StartTime" readonly="readonly" style="width:200px;" value="<%=StartTime%>" />
                   <label id="d_StartTime"></label>
                   <%=Admin.Calendar(Array("StartTime","StartTime","%Y-%m-%d %H:%M:%S"))%>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_Ads(5) %><!--结束时间-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtSer dateEnd" id="EndTime" name="EndTime" readonly="readonly" style="width:200px;" value="<%=EndTime%>" />
                   <label id="d_EndTime"></label>
                   <%=Admin.Calendar(Array("EndTime","EndTime","%Y-%m-%d %H:%M:%S"))%>
                   </td>
               </tr>
               <tr><td class="titletd" valign="top"><% Echo Lang_Ads(6) %><!--广告位宽度-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="Width" name="Width" style="width:60px;" value="<%=Width%>" /><label class="ml3 Normal">px</label>
                   <label id="d_Width"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Ads(7) %><!--广告位高度-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="Height" name="Height" style="width:60px;" value="<%=Height%>" /><label class="ml3 Normal">px</label>
                   <label id="d_Height"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_Ads(8) %><!--排序-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="OrderNum" name="OrderNum" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9]*/g','maxLength:5'))" style="width:50px;" value="<%=OrderNum%>" />
                   <label class="Normal"><% Echo Lang_Ads(10) %><!--数字越小越靠前--></label>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Ads(9) %><!--备注-->:</td>
                   <td class="infotd">
                   <textarea class="remark" id="Remarks" name="Remarks"><%=Remarks%></textarea> 
                   <label class="Normal"></label>
                   </td>
               </tr>
              </tbody>
            </table>
            <input type="hidden" name="id" value="<%=ID%>" />
            <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
            <div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Ads.SaveAdsGroupCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
              <button type="button" class="sysSaveBtn" id="ReSetBtn" onClick="if( confirm('<% Echo Lang_DoConfirmLeave %>') ){CloseWithOpenNewWin('?ctl=<% Echo Ctl %>&act=view');}"><b class="icon icon_goback"></b><% Echo Lang_Goback %></button>
            </div>
            </form>
          </div>
        </div>
      </DIV>
    </DIV>
<%
	End Function
	Private Function AdsDetailView()
%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
                  <span class="sysBtn"><a href="javascript:ShowModal('?ctl=<% Echo Ctl %>&act=add&adsgroupid=<%=AdsGroupID%>',1000,620);ShowModalReload();"><b class="icon icon_add"></b><% Echo Lang_DoAdd & Lang_Ads(0) %></a></span>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_recycle"></b><% Echo Lang_DoDel%></button>
				  </td></tr>
			  </table>
		  </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th width="1%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
            <th width="15%"><div style="width:150px;"><% Echo Lang_Ads_ListTableColLine(11) %><!--广告标题--></div></th>
			<th width="15%"><div style="width:150px;"><% Echo Lang_Ads_ListTableColLine(12) %><!--广告链接地址--></div></th>
            <th width="28%"><div style="width:200px;"><% Echo Lang_Ads_ListTableColLine(13) %><!--广告图片--></div></th>
            <th width="36%"><div style="width:150px"><% Echo Lang_Ads_ListTableColLine(14) %><!--广告简介--></div></th>
            <th width="5%"><div style="width:50px"><% Echo Lang_Ads_ListTableColLine(15) %><!--排序--></div></th>
			<th><div style="width:60px;"><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% AdsDetailList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
    <form name="FormForReload" style="display:none;" method="post"></form>
	<script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td2"){ TempHTML = tds[i].innerHTML; tds[i].innerHTML="<a href=\""+TempHTML+"\">"+TempHTML+"</a>";}
				if(tds[i].id=="td3"){ TempHTML = tds[i].innerHTML; tds[i].innerHTML="<a href=\"javascript:;\" onclick=\"Admin.Ads.ImageView_GetUrl(this)\"><img src=\""+TempHTML+"\" class=\"img_list_s\"></a>";}
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
	Private Function AdsDetailDo()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
          <div class="inbox">
            <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
            <input type="hidden" id="adsgroupID" name="adsgroupID" value="<%=AdsGroupID%>" />
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable" id="Tabs_1">
              <tbody>
               <tr><td class="titletd"><%=Lang_Ads(11) %><!--广告标题-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="AdsTitle" name="AdsTitle" style="width:360px;" value="<%=AdsTitle%>" />
                   <label id="d_AdsTitle"></label> 
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_Ads(14) %><!--广告图片URL-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="ImageUrl" name="ImageUrl" style="width:360px;" onblur="Admin.Ads.AdsImageUrlChange()" value="<%=ImageUrl%>" /><span class="ml5 sysBtn"><a href="javascript:;" onclick="Admin.Ads.SelectAdsImage()"><b class="icon icon_add"></b><%=Lang_Btn_SelectPics%></a></span>
                   <div><label id="d_ImageUrl"></label></div>
                   <div id="AdsImageBox" class="mt5"><% if Not(KnifeCMS.Data.IsNul(ImageUrl)) Then Response.Write("<img src="""&ImageUrl&""" class=""AdsImage"" />") %></div>
                   
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_Ads(12) %><!--广告链接地址-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="AdsUrl" name="AdsUrl" style="width:360px;" value="<%=AdsUrl%>" />
                   <label id="d_AdsUrl"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_Ads(13) %><!--广告简介-->:</td>
                   <td class="infotd">
                   <textarea class="TextContent" id="TextContent" name="TextContent"><%=KnifeCMS.Data.HTMLDecode(AdsTxt)%></textarea>
                   <label id="d_AdsTxt"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_Ads(8) %><!--排序-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="OrderNum" name="OrderNum" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9]*/g','maxLength:5'))" style="width:50px;" value="<%=OrderNum%>" />
                   <label class="Normal"><% Echo Lang_Ads(10) %><!--数字越小越靠前--></label>
                   </td>
               </tr>
              </tbody>
            </table>
            <input type="hidden" name="id" value="<%=ID%>" />
            <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
            <div class="bottomSaveline" id="SubmitButtoms">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Ads.SaveAdsDetailCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
              <button type="button" class="sysSaveBtn" id="ReSetBtn" onClick="WinClose()"><b class="icon icon_close"></b><% Echo Lang_Btn_Close %></button>
            </div>
            </form>
          </div>
        </div>
      </DIV>
    </DIV>
    <script type="text/javascript" charset="utf-8">
	window.UEDITOR_HOME_URL = "<%=SystemPath%>plugins/ueditor/";
	</script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_config.asp"></script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_api.js"></script>
    <script type="text/javascript">
	var OSWebEditor=UE.getEditor('TextContent');
	</script>
<%
	End Function
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
End Class
%>