<!--#include file="config.admin.asp"-->
<%
Dim ShipBill
Set ShipBill = New Class_KnifeCMS_Admin_ShipBill
Set ShipBill = Nothing
Class Class_KnifeCMS_Admin_ShipBill

	Private Pv_OrderID,Pv_UserID,Pv_Currency,Pv_Status,Pv_ShippingID,Pv_IsShipBill,Pv_IsReShipBill
	Private Pv_Username,Pv_SysUsername
	Private Pv_Cost_Protect
	Private Pv_BillID,Pv_Money,Pv_IsProtect,Pv_DeliveryName,Pv_Logi_Name,Pv_Logi_NO,Pv_Ship_Name,Pv_Ship_Area,Pv_Ship_Address,Pv_Ship_Zip,Pv_Ship_Tel,Pv_Ship_Email,Pv_Ship_Mobile,Pv_Ship_Time,Pv_OperaterID,Pv_TBegin,Pv_TEnd,Pv_Reason,Pv_Remarks,Pv_Disabled
	Private Pv_ShippingData,Pv_DeliveryCorpData,Pv_DeliveryCorpID
	Private Pv_KeywordsType,Pv_PriceFrom,Pv_PriceTo
	Private Pv_SqlShipType,Pv_LanguageFile
	
	Private Sub Class_Initialize()
		If Ctl = "reshipbill" Then 
			Pv_IsShipBill   = False
			Pv_IsReShipBill = True
			Pv_SqlShipType  = " Type=0 "
			Pv_LanguageFile = "reshipbill.lang.asp"
		Else
			Pv_IsShipBill   = True
			Pv_IsReShipBill = False
			Pv_SqlShipType = " Type=1 "
			Pv_LanguageFile = "shipbill.lang.asp"
		End If
		Admin.IncludeLanguageFile Pv_LanguageFile
		IsContentExist = False
		Header()
		If Ctl = "shipbill" Or Ctl = "reshipbill" Then
			Pv_BillID = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","billid"),"[^0-9]",""),16)
			Select Case Action
				Case "view"
					If SubAction = "detail" Then
						Call GetShipBillDetailData()
						If IsContentExist Then ShipBillDetailView : Else Admin.ShowMessage Lang_ShipBill_Cue(7),Array("javascript:WinClose();",Lang_WinClose),0
					Else
						Call ShipBillGetSearchCondition()
						Call ShipBillView()
					End If
				Case "del"
				    Call ShipBillDel()
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"?ctl="& Ctl &"&act=view",1
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function GetShipBillDetailData()
		Set Rs = KnifeCMS.DB.GetRecord(DBTable_ShipBill,Array("ID:"& ID),"")
		If Not(Rs.Eof) Then
			IsContentExist = True
			SysID           = Rs("SysID")
			Pv_BillID       = Rs("BillID")
			Pv_OrderID      = Rs("OrderID")
			Pv_Money        = Rs("Money")
			Pv_IsProtect    = Rs("IsProtect")
			Pv_Cost_Protect = Rs("Cost_Protect")
			Pv_DeliveryName = Rs("DeliveryName")
			Pv_Logi_Name    = Rs("Logi_Name")
			Pv_Logi_NO      = Rs("Logi_NO")
			Pv_Ship_Name    = Rs("Ship_Name")
			Pv_Ship_Area    = Rs("Ship_Area")
			Pv_Ship_Address = Rs("Ship_Address")
			Pv_Ship_Zip     = Rs("Ship_Zip")
			Pv_Ship_Tel     = Rs("Ship_Tel")
			Pv_Ship_Mobile  = Rs("Ship_Mobile")
			Pv_Ship_Email   = Rs("Ship_Email")
			Pv_Ship_Time    = Rs("Ship_Time")
			Pv_UserID       = KnifeCMS.Data.CLng(Rs("UserID"))
			Pv_OperaterID   = KnifeCMS.Data.CLng(Rs("OperaterID"))
			Pv_TBegin       = Rs("TBegin")
			Pv_TEnd         = Rs("TEnd")
			Pv_Status       = Rs("Status")
			Pv_Reason       = Rs("Reason")
			Pv_Remarks      = Rs("Remarks")
			Pv_Disabled     = Rs("Disabled")
		Else
			IsContentExist = False
		End If
		KnifeCMS.DB.CloseRs Rs
		If Pv_UserID>0 Then Pv_Username = KnifeCMS.DB.GetFieldByID(DBTable_Members,"Username",Pv_UserID)
		If Pv_OperaterID>0 Then Pv_SysUsername = KnifeCMS.DB.GetFieldByID(DBTable_SysUser,"Username",Pv_OperaterID)
	End Function
	
	Private Function ShipBillGoodsList(ByVal BV_Type)
		Dim Fn_Rs,Fn_ii,Fn_TempString,Fn_JSON,Fn_PacksHTML
		Dim Fn_Config,Fn_PacksID,Fn_PacksName,Fn_GoodsBN,Fn_GoodsName,Fn_GoodsAttrValue,Fn_Nums
		If KnifeCMS.Data.IsNul(Pv_BillID) Then
			Fn_TempString = "<tr class=""data""><td colspan=""6"">"& Lang_HaveNoneGoods &"</td></tr>"
		Else
			Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_ShipItem,Array("ShipBillID:"& Pv_BillID),"ORDER BY ID ASC")
			Do While Not(Fn_Rs.Eof)
				Fn_Config         = Fn_Rs("Config")
				Fn_PacksID        = KnifeCMS.Data.CLng(Fn_Rs("PacksID"))
				Fn_PacksName      = KnifeCMS.Data.HTMLDecode(Fn_Rs("PacksName"))
				Fn_Nums           = KnifeCMS.Data.CLng(Fn_Rs("Nums"))     '发货/退货数量
				
				Set Fn_JSON       = KnifeCMS.JSON.Parse(Fn_Config)
				Fn_GoodsName      = KnifeCMS.Data.UnEscape(Fn_JSON.goodsname)
				Fn_GoodsAttrValue = KnifeCMS.Data.UnEscape(Fn_JSON.goodsattrvalue)
				If Not(KnifeCMS.Data.IsNul(Fn_GoodsAttrValue)) Then Fn_GoodsName = Fn_GoodsName &"<span class=yellow>("& Fn_GoodsAttrValue &")</span>"
				If Fn_PacksID>0 Then
					Fn_PacksHTML = "<span class=""packsname"">[套装]"& Fn_PacksName &"</span>"
					Fn_PacksHTML = Fn_PacksHTML & "<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""listTable"">"
					Fn_PacksHTML = Fn_PacksHTML & "<tr class=""data""><td>"& Fn_JSON.goodsbn &"</td><td>"& Fn_GoodsName &"</td></tr>"
					For Fn_ii=0 To Fn_JSON.selectgoods.length-1
						Fn_GoodsName = KnifeCMS.Data.UnEscape(Fn_JSON.selectgoods.get(Fn_ii).goodsname)
						Fn_PacksHTML = Fn_PacksHTML & "<tr class=""data""><td width=""5%""><div style=""width:150px;"">"& Fn_JSON.selectgoods.get(Fn_ii).goodsbn &"</div></td><td width=""90%""><div style=""width:380px;"">"& Fn_GoodsName &"</div></td></tr>"
					Next
					Fn_PacksHTML  = Fn_PacksHTML & "</table>"
					Fn_TempString = Fn_TempString & "<tr class=""data""><td colspan=""2"">"& Fn_PacksHTML &"</td><td>"& Fn_Nums &"</td></tr>"
				Else
					Fn_GoodsBN        = Fn_JSON.goodsbn
					Fn_TempString = Fn_TempString &"<tr class=""data""><td>"& Fn_GoodsBN &"</td><td>"& Fn_GoodsName &"</td><td>"& Fn_Nums &"</td></tr>"
				End If
				Set Fn_JSON  = Nothing
				Fn_Rs.Movenext()
			Loop
			KnifeCMS.DB.CloseRs Fn_Rs '关闭并释放指定数据集
		End If
		Echo Fn_TempString
	End Function
	
	Private Function ShipBillGetSearchCondition()
		Pv_PriceFrom = KnifeCMS.GetForm("both","PriceFrom") : If Not(KnifeCMS.Data.IsNul(Pv_PriceFrom)) Then Pv_PriceFrom = KnifeCMS.Data.FormatDouble(Pv_PriceFrom)
		Pv_PriceTo   = KnifeCMS.GetForm("both","PriceTo")   : If Not(KnifeCMS.Data.IsNul(Pv_PriceTo))   Then Pv_PriceTo   = KnifeCMS.Data.FormatDouble(Pv_PriceTo)
		Pv_ShippingID     = KnifeCMS.GetForm("both","shippingid[]")
		Pv_DeliveryCorpID = KnifeCMS.GetForm("both","deliverycorpid[]")
		Pv_KeywordsType   = KnifeCMS.GetForm("both","keywordstype")
	End Function
	Private Function ShipBillSearchSql()
		Dim Fn_TempSql
		Pv_ShippingID     = ParseSearchData(Pv_ShippingID)
		Pv_DeliveryCorpID = ParseSearchData(Pv_DeliveryCorpID)
		Pv_PriceFrom = KnifeCMS.Data.FormatDouble(Pv_PriceFrom)
		Pv_PriceTo   = KnifeCMS.Data.FormatDouble(Pv_PriceTo)
		Fn_TempSql  = Fn_TempSql & KnifeCMS.IIF(KnifeCMS.Data.IsNul(Pv_ShippingID) Or Pv_ShippingID="any",""," AND a.DeliveryID IN ("& Pv_ShippingID &")" )
		Fn_TempSql  = Fn_TempSql & KnifeCMS.IIF(KnifeCMS.Data.IsNul(Pv_DeliveryCorpID) Or Pv_DeliveryCorpID="any",""," AND a.Logi_ID IN ("& Pv_DeliveryCorpID &")" )
		Fn_TempSql  = Fn_TempSql & KnifeCMS.IIF(Pv_PriceFrom > 0 ," AND isnull(a.Money,0)>="& Pv_PriceFrom &" "," ")
		Fn_TempSql  = Fn_TempSql & KnifeCMS.IIF(Pv_PriceTo   > 0 ," AND isnull(a.Money,0)<="& Pv_PriceTo &" "  ," ")
		Select Case Pv_KeywordsType
			Case "billid"
				Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Admin.Search.KeyWords,"[^a-zA-Z0-9]",""),20)
				Fn_TempSql = Fn_TempSql & " AND a.BillID LIKE '%"& Admin.Search.KeyWords &"%'"
			Case "orderid"
				Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Admin.Search.KeyWords,"[^0-9]",""),20)
				Fn_TempSql = Fn_TempSql & " AND a.OrderID LIKE '%"& Admin.Search.KeyWords &"%'"
			Case "ship_name"
				Fn_TempSql = Fn_TempSql & " AND a.Ship_Name LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%'"
			Case "deliveryname"
				Fn_TempSql = Fn_TempSql & " AND a.DeliveryName LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%'"
			Case "logi_name"
				Fn_TempSql = Fn_TempSql & " AND a.Logi_Name LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%'"
			Case "logi_no"
				Fn_TempSql = Fn_TempSql & " AND a.Logi_NO LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%'"
			Case "membername"
				Fn_TempSql = Fn_TempSql & " AND a.UserID IN (SELECT ID FROM ["& DBTable_Members &"] WHERE Username LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%')"
			Case "operater"
				Fn_TempSql = Fn_TempSql & " AND a.OperaterID IN (SELECT ID FROM ["& DBTable_SysUser &"] WHERE Username LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%')"
		End Select
		ShipBillSearchSql = Fn_TempSql
	End Function
	Private Function ParseSearchData(ByVal BV_Data)
		Dim Fn_ii,Fn_ReturnString,Fn_TempArray,Fn_Temp
		Fn_TempArray    = Split(BV_Data,",")
		Fn_ReturnString = ""
		For Fn_ii=0 To Ubound(Fn_TempArray)
			If Fn_TempArray(Fn_ii)="any" Then
				Fn_ReturnString="any"
				Exit For
			Else
				Fn_Temp = KnifeCMS.Data.Clng(Fn_TempArray(Fn_ii))
				If Fn_Temp>=0 Then
					If Fn_ReturnString="" Then Fn_ReturnString = Fn_Temp : Else Fn_ReturnString = Fn_ReturnString &","& Fn_Temp
				End If
			End If
		Next
		ParseSearchData = Fn_ReturnString
	End Function
	
	Private Function ShipBillList()
		Dim Fn_TempSql,Fn_ColumnNum
		Select Case DB_Type
		Case 0
		Fn_TempSql = "SELECT a.ID,a.BillID,a.OrderID,a.Money,a.IsProtect,a.Ship_Name,a.DeliveryName,a.Logi_Name,a.Logi_NO,b.Username,a.TBegin,c.Username FROM (["& DBTable_ShipBill &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.UserID)) LEFT JOIN ["& DBTable_SysUser &"] c ON (c.ID=a.OperaterID)"
		Case 1
		Fn_TempSql = "SELECT a.ID,a.BillID,a.OrderID,a.Money,a.IsProtect,a.Ship_Name,a.DeliveryName,a.Logi_Name,a.Logi_NO,b.Username,a.TBegin,c.Username FROM ["& DBTable_ShipBill &"] a LEFT JOIN ["& DBTable_Members &"] b ON (b.ID=a.UserID) LEFT JOIN ["& DBTable_SysUser &"] c ON (c.ID=a.OperaterID)"
		End Select
			Fn_ColumnNum = 12
			Operation = Operation & "<span class=""sysBtn""><a href=""javascript:;"" onClick=""ShowModal('?ctl="&Ctl&"&act=view&subact=detail&id={$ID}',800,480);""><b class=""icon icon_view""></b>"& Lang_DoView &"</a></span>"
		If SubCtl = "recycle" Then
			Fn_TempSql = Fn_TempSql & " WHERE a.Recycle=1 AND "& Pv_SqlShipType &" ORDER BY a.ID DESC"
			Operation = ""
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,"")
		Else
			Fn_TempSql = Fn_TempSql & " WHERE a.Recycle=0 AND "& Pv_SqlShipType &" "
			If SubAction = "search" Then
			
				Select Case DB_Type
				Case 0
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.TBegin>=#"& Admin.Search.StartDate &"# ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.TBegin<=#"& Admin.Search.EndDate &"# ","")
				Case 1
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.TBegin>='"& Admin.Search.StartDate &"' ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.TBegin<='"& Admin.Search.EndDate &"' ","")
				End Select
				
				tempCondition = tempCondition & ShipBillSearchSql
				PageUrlPara = PageUrlPara & "&startdate="& Server.URLEncode(Admin.Search.StartDate) &"&enddate="& Server.URLEncode(Admin.Search.EndDate) &"&keywords="& Server.URLEncode(Admin.Search.KeyWords)&"&keywordstype="& Pv_KeywordsType &"&PriceFrom="& Pv_PriceFrom &"&PriceTo="& Pv_PriceTo &"&shippingid[]="& Pv_ShippingID &"&deliverycorpid[]="& Pv_DeliveryCorpID &""
				Fn_TempSql = Fn_TempSql & tempCondition
			End If
			Fn_TempSql = Fn_TempSql &" ORDER BY a.ID DESC"
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
		End If
	End Function
	
	Private Function ShipBillDel()
		Call ShipBillSubDo()
	End Function
	
	'删除/彻底删除/还原
	Private Sub ShipBillSubDo()
		Dim Fn_ArrID,Fn_i,Fn_BillID
		ID       = KnifeCMS.GetForm("post","InputName")
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			Result     = 0
			Fn_BillID  = ""
			If ID > 0 Then
				Fn_BillID = KnifeCMS.DB.GetFieldByID(DBTable_ShipBill,"BillID",ID)
				If Not(KnifeCMS.Data.IsNul(Fn_BillID)) Then
					If Action = "del" Then
						If SubAction = "completely" Then
							Result = KnifeCMS.DB.DeleteRecord(DBTable_ShipBill,Array("ID:"&ID))
							If 1=Result Then Msg = Msg & Lang_ShipBill_Cue(4) &"{[billid="& Fn_BillID &"]}<br>"
						End If
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Sub
	
	Private Function ShipBillView()
%>
	<%=Admin.Calendar(Array("","",""))%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
          <td class="tdcell">
            <table border="0" cellpadding="2" cellspacing="0">
            <tr><td class="upcell">
                <div class="align-center grey">
                    <% Echo Lang_DistrictDoSearch %><!--搜索操作区-->
                    <span class="AdvancedSearch"><a href="javascript:Admin.AdvancedSearch.Open(600,355,'AdvancedSearch')" ><% Echo Lang_AdvancedSearch %></a></span>
                </div>
                <DIV id="AdvancedSearch" style="display:none;">
                    <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                    <div class="diainbox">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                      <tr><td class="titletd"><% Echo Lang_Keywords %>:</td>
                          <td class="infotd">
                          <select name="keywordstype">
                          <% Echo OS.CreateOption(Array("billid:"& Lang_ShipBill_ListTableColLine(1),"orderid:"& Lang_ShipBill_ListTableColLine(2),"ship_name:"& Lang_ShipBill_ListTableColLine(5),"deliveryname:"& Lang_ShipBill_ListTableColLine(6),"logi_name:"& Lang_ShipBill_ListTableColLine(7),"logi_no:"& Lang_ShipBill_ListTableColLine(8),"membername:"& Lang_ShipBill_ListTableColLine(9),"operater:"& Lang_ShipBill_ListTableColLine(11)),Split(Pv_KeywordsType,",")) %>
                          </select>
                          <input type="text" class="TxtClass" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:306px;" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_ShipBill_ListTableColLine(6) %>/<% Echo Lang_ShipBill_ListTableColLine(7) %></td><td class="infotd">
                          <table border="0" cellpadding="0" cellspacing="0"><tr>
                          <td class="pr5">
                          <div class="mb3 grey"><% Echo Lang_ShipBill_ListTableColLine(6) %><!--配送方式--></div>
                          <%
						  Dim Fn_DeliveryString : Fn_DeliveryString = Admin.GetListAsArrayList(DBTable_Delivery &":ID,DeliveryName","Disabled=0 ","ORDER BY OrderNum ASC,ID ASC")
						  If KnifeCMS.Data.IsNul(Fn_DeliveryString) Then
							  Execute("Pv_ShippingData = Array(""any:"& Lang_AnyShipping &""")")
						  Else
							  Execute("Pv_ShippingData = Array(""any:"& Lang_AnyShipping &""","& Fn_DeliveryString &")")
						  End If
						  %>
                          <select name="shippingid[]" id="shippingid[]" multiple="multiple" size="5" style="width:200px;">
                          <% Echo OS.CreateOption(Pv_ShippingData,Split(Pv_ShippingID,",")) %>
                          </select>
                          </td>
                          <td>
                          <div class="mb3 grey"><% Echo Lang_ShipBill_ListTableColLine(7) %><!--物流公司--></div>
                          <% 
						  Dim Fn_DeliveryCorpString : Fn_DeliveryCorpString = Admin.GetListAsArrayList(DBTable_DeliveryCorp &":ID,CorpName","Disabled=0 ","ORDER BY OrderNum ASC,ID ASC")
						  If KnifeCMS.Data.IsNul(Fn_DeliveryCorpString) Then
							  Execute("Pv_DeliveryCorpData = Array(""any:"& Lang_AnyDeliveryCorp &""")")
						  Else
						      Execute("Pv_DeliveryCorpData = Array(""any:"& Lang_AnyDeliveryCorp &""","& Fn_DeliveryCorpString &")")
						  End If
						  %>
                          <select name="deliverycorpid[]" id="deliverycorpid[]" multiple="multiple" size="5" style="width:200px;">
                          <% Echo OS.CreateOption(Pv_DeliveryCorpData,Split(Pv_DeliveryCorpID,",")) %>
                          </select>
                          </td>
                          </tr></table>
                          </td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_ShipBill_ListTableColLine(3) %><!--支付金额-->:</td>
                            <td class="infotd"><% Echo Lang_From %><!--从--> <input type="text" id="PriceFrom" name="PriceFrom" class="TxtClass" style="width:51px;" value="<% Echo Pv_PriceFrom %>" /> <% Echo Lang_FromTo %><!--到--> <input type="text" id="PriceTo" name="PriceTo" class="TxtClass" style="width:51px;" value="<% Echo Pv_PriceTo %>" /></td></tr>
                      <tr><td class="titletd"><% Echo Lang_StartDate %>:</td>
                          <td class="infotd"><input type="text" class="TxtSer dateStart" id="startdate" name="startdate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' value="<%=Admin.Search.StartDate%>" style="width:200px;" /></td>
                      <tr><td class="titletd"><% Echo Lang_EndDate %>:</td>
                          <td class="infotd"><input type="text" class="TxtSer dateEnd" id="enddate" name="enddate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' value="<%=Admin.Search.EndDate%>" style="width:200px;" /></td>
                      </tr>
                      <tr><td class="titletd"></td>
                          <td class="infotd"><input type="checkbox" name="explicitsearch" value="1" /><% Echo Lang_ExplicitSearch %></td>
                      </tr>
                      </table>
                    </div>
                    <% Echo AdvancedSearchBtnline %>
                    </form>
                </DIV>
                </td></tr>
            <tr><td class="downcell">
                <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                <table border="0" cellpadding="0" cellspacing="0"><tr>
                <td><span><% Echo Lang_Keywords %>:</span></td>
                <td class="pl3">
                <select name="keywordstype">
                <% Echo OS.CreateOption(Array("billid:"& Lang_ShipBill_ListTableColLine(1),"orderid:"& Lang_ShipBill_ListTableColLine(2),"ship_name:"& Lang_ShipBill_ListTableColLine(5),"deliveryname:"& Lang_ShipBill_ListTableColLine(6),"logi_name:"& Lang_ShipBill_ListTableColLine(7),"logi_no:"& Lang_ShipBill_ListTableColLine(8),"membername:"& Lang_ShipBill_ListTableColLine(9),"operater:"& Lang_ShipBill_ListTableColLine(11)),Split(Pv_KeywordsType,",")) %>
                </select>
                </td>
                <td class="pl3"><input type="text" class="TxtClass" name="keywords" id="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:150px;" /></td>
                <td class="pl3"><button type="submit" class="sysBtn" ><b class="icon icon_search"></b><% Echo Lang_Btn_Search %></button></td>
                <td class="pl3"><input type="checkbox" name="explicitsearch" value="1" /><% Echo Lang_ExplicitSearch %></td>
                </tr></table>
                </form>
            </td></tr>
            </table>
          </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th><div><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
			<th><div><% Echo Lang_ShipBill_ListTableColLine(1) %><!--发货单号--></div></th>
            <th><div><% Echo Lang_ShipBill_ListTableColLine(2) %><!--订单号--></div></th>
            <th><div><% Echo Lang_ShipBill_ListTableColLine(3) %><!--物流费用--></div></th>
            <th><div><% Echo Lang_ShipBill_ListTableColLine(4) %><!--是否保价--></div></th>
            <th><div><% Echo Lang_ShipBill_ListTableColLine(5) %><!--收货人--></div></th>
            <th><div><% Echo Lang_ShipBill_ListTableColLine(6) %><!--配送方式--></div></th>
            <th><div><% Echo Lang_ShipBill_ListTableColLine(7) %><!--物流公司--></div></th>
            <th><div><% Echo Lang_ShipBill_ListTableColLine(8) %><!--物流单号--></div></th>
            <th><div><% Echo Lang_ShipBill_ListTableColLine(9) %><!--会员用户名--></div></th>
            <th><div><% Echo Lang_ShipBill_ListTableColLine(10)%><!--单据创建时间--></div></th>
            <th><div><% Echo Lang_ShipBill_ListTableColLine(11)%><!--操作员--></div></th>
			<th><div><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% ShipBillList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
    <script type="text/javascript">
	KnifeCMS.SetSelectIndexNumFontWeight(0,KnifeCMS.$id("shippingid[]"));
	KnifeCMS.SetSelectIndexNumFontWeight(0,KnifeCMS.$id("deliverycorpid[]"));
	TrBgChange("listForm","InputName","oliver","data","select");
	if(objExist(document.getElementById("keywords"))){ReplaceText(document.getElementById("keywords").value,1)}
	function ChangeTdText(){
		var tds=document.getElementById("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			var IsEdit = true;
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td3"){
					TempHTML=KnifeCMS.FormatCurrency(tds[i].innerHTML);
					tds[i].innerHTML = '<% Echo MoneySb %>'+TempHTML
				}
				if(tds[i].id=="td4"){
					TempHTML=parseInt(tds[i].innerHTML);
					if(TempHTML==1){tds[i].innerHTML = '<%=Lang_Yes%>'}else if(TempHTML==0){tds[i].innerHTML = '<%=Lang_No%>'}
				}
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
	Private Function ShipBillDetailView()
%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
          <div class="obox">
            <div class="tit"><% Echo Lang_ShipBill(1) %><!--发货单信息--></div>
            <div class="inbox">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><% Echo Lang_ShipBill(2) %><!--发/退货单号-->:</td><td class="infotd"><% Echo Pv_BillID %></td>
                    <td class="titletd"><% Echo Lang_ShipBill(3) %><!--订单号-->:</td><td class="infotd"><% Echo Pv_OrderID %></td>
                </tr>
                <tr>
                    <td class="titletd"><% Echo Lang_ShipBill(4) %><!--配送方式-->:</td><td class="infotd"><% Echo Pv_DeliveryName %></td>
                    <td class="titletd"><% Echo Lang_ShipBill(5) %><!--物流公司-->:</td><td class="infotd"><% Echo Pv_Logi_Name %></td>
                </tr>
                <tr>
                    <td class="titletd"><% Echo Lang_ShipBill(6) %><!--物流费用-->:</td><td class="infotd"><% Echo MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Money) %></td>
                    <td class="titletd"><% Echo Lang_ShipBill(7) %><!--物流单号-->:</td><td class="infotd"><% Echo Pv_Logi_NO %></td>
                </tr>
                <tr>
                    <td class="titletd"><% Echo Lang_ShipBill(8) %><!--实际保价-->:</td><td class="infotd" <% If Pv_IsReShipBill Then %>colspan="3"<% End If %>><% Echo KnifeCMS.IIF(Pv_IsProtect=1,Lang_Yes,Lang_No) %></td>
                    <% If Pv_IsShipBill Then %><td class="titletd"><% Echo Lang_ShipBill(9) %><!--保价费用-->:</td><td class="infotd"><% Echo MoneySb & KnifeCMS.Data.FormatCurrency(Pv_Cost_Protect) %></td><% End If %>
                </tr>
                <tr class="Separated"><td class="titletd" colspan="2"><div></div></td><td class="infotd" colspan="2"><div></div></td></tr>
                <tr><td class="titletd"><% Echo Lang_ShipBill(10) %><!--发/退货人姓名-->:</td><td class="infotd"><% Echo Pv_Ship_Name %></td>
                    <td class="titletd"><% Echo Lang_ShipBill(11) %><!--发/退货人Email-->:</td><td class="infotd"><% Echo Pv_Ship_Email %></td></tr>
                <tr><td class="titletd"><% Echo Lang_ShipBill(12) %><!--联系手机-->:</td><td class="infotd"><% Echo Pv_Ship_Mobile %></td>
                    <td class="titletd"><% Echo Lang_ShipBill(13) %><!--联系电话-->:</td><td class="infotd"><% Echo Pv_Ship_Tel %></td></tr>
                <tr><td class="titletd"><% Echo Lang_ShipBill(14) %><!--发/退货地区-->:</td><td class="infotd"><% Echo KnifeCMS.Data.CRight(Pv_Ship_Area,":") %></td>
                    <td class="titletd"><% Echo Lang_ShipBill(15) %><!--邮政编码-->:</td><td class="infotd"><% Echo Pv_Ship_Zip %></td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_ShipBill(16) %><!--详细地址-->:</td><td class="infotd" colspan="3"><% Echo Pv_Ship_Address %></td></tr>
                <tr class="Separated"><td class="titletd" colspan="2"><div></div></td><td class="infotd" colspan="2"><div></div></td></tr>
                <% If Pv_IsReShipBill Then %><tr><td class="titletd"><% Echo Lang_ShipBill(17) %><!--退货原因-->:</td><td class="infotd" colspan="3"><% Echo Pv_Reason %></td></tr><% End If %>
                <tr><td class="titletd"><% Echo Lang_ShipBill(18) %><!--备注-->:</td><td class="infotd" colspan="3"><% Echo Pv_Remarks %></td></tr>
              </table>
            </div>
            <div class="inbox">
                <table id="goodslist" border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
                 <tbody>
                  <tr class="top">
                  <th width="8%" ><div style="width:154px;"><% Echo Lang_ShipBill_ListTableColLine(12) %><!--商品编号--></div></th>
                  <th width="82%"><div style="width:260px;"><% Echo Lang_ShipBill_ListTableColLine(13) %><!--商品名称--></div></th>
                  <th width="10%"><div style="width:68px; "><% Echo Lang_ShipBill_ListTableColLine(14) %><!--发货数量--></div></th>
                  </tr>
                  <% Call ShipBillGoodsList("shipbill") %>
                 </tbody>
                </table>
                <script type="text/javascript">
                TrBgChange("goodslist","oliver","data");
                </script>
            </div>
          </div>
      </DIV>
    </DIV>
<%
	End Function
End Class
%>
	