<!--#include file="config.admin.asp"-->
<%
Dim IndexPage
Set IndexPage = New Class_KnifeCMS_Admin_Index
Set IndexPage = Nothing

Class Class_KnifeCMS_Admin_Index
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "index.lang.asp"
		Call Init()
	End Sub
	Private Sub Class_Terminate()
		Call ClassObjectClose()
	End Sub
	Private Sub Init()
%>
<html>
<head id="container" name="container">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<title><%=HeadTitle%></title>
<link rel="stylesheet" type="text/css" href="skin/default/css/comm.css" />
<link rel="stylesheet" type="text/css" href="skin/default/css/skin.css" id="cssfile" />
<script type="text/javascript" src="js/index.js"></script>
</head>
<body style="overflow:visible;">
<table cellspacing="0" cellpadding="0" width="100%" height="100%" id="abody">
  <tr>
    <td class="getuserdata" colspan="2" >
      <div class="topdata">
          <div class="top_right">
            <div class="tophelp">
            <a href="sysusers.asp?ctl=sysuser&act=edit&id=<%=Admin.SysUserID%>" target="mainFrame"><font class="import"><%=Admin.SysUsername%>[<%=Admin.SysUserGroupName%>]</font> (<%=Lang_Index(1)%>)</a> | <a href="../" target="_blank"><%=Lang_Index(2)%></a> | <a href="http://www.knifecms.com/?content/help/category/2/index.htm" target="_blank"><%=Lang_Index(3)%></a> | <a href="ajax.login.asp?ctl=sysuserlogin&act=loginout" ><%=Lang_Index(4)%></a></div>
            <div class="top_right_note"></div>
          </div>
          <div class="refresh"><a href="javascript:frames['mainFrame'].location.reload();"><%=Lang_Index(5)%></a></div>
          <div class="top_left">
              <div class="switch">
              <div class="topmenu" id="topmenu">
                  <ul>
                  <li class="current"><a onClick="javascript:MainmenuSelect(1)" href="main.asp?ctl=systeminfo&act=view" target="mainFrame"><h3><%=Lang_Menu_SystemSet%></h3></a></li>
                  <li><a onClick="javascript:MainmenuSelect(2)" href="content.subsystem.asp?ctl=contentsubsystem&act=view" target="mainFrame"><h3><%=Lang_Menu_ContentSystem%></h3></a></li>
                  <li><a onClick="javascript:MainmenuSelect(3)" href="navigator.asp?ctl=navigator&act=view" target="mainFrame"><h3><%=Lang_Menu_VisionSystem%></h3></a></li>
                  <li><a onClick="javascript:MainmenuSelect(4)" href="guestbook.asp?ctl=guestbook&act=view" target="mainFrame"><h3><%=Lang_Menu_AssistSystem%></h3></a></li>
                  <li <% If CloseModuleShop Then Echo "style=""display:none;""" %>><a onClick="javascript:MainmenuSelect(5)" href="goods.asp?ctl=goods&act=view" target="mainFrame"><h3><%=Lang_Menu_ShopSystem%></h3></a></li>
                  <!--li <% If CloseModuleShop Then Echo "style=""display:none;""" %>><a onClick="javascript:MainmenuSelect(6)" href="order.asp?ctl=order&act=view&all" target="mainFrame"><h3><%=Lang_Menu_OrderSystem%></h3></a></li-->
                  <li><a onClick="javascript:MainmenuSelect(6)" href="user.asp?ctl=user&act=view" target="mainFrame"><h3><%=Lang_Menu_MemberSystem%></h3></a></li>
                  <li><a onClick="javascript:MainmenuSelect(7)" href="public.asp?ctl=adsgroup&act=view" target="mainFrame"><h3><%=Lang_Menu_AdSystem%></h3></a></li>
                  <li><a onClick="javascript:MainmenuSelect(8)" href="weixin.asp?ctl=weixin&act=view" target="mainFrame"><h3><%=Lang_Menu_WeiXin%></h3></a></li>
                  </ul>
              </div>
            </div>
          </div>
      </div>
      <div class="show_leftmenu" id="show_leftmenu"><a href="javascript:showleftmenu();" class="arrowleft" id="showmenu" ></a></div>
    </td>
  </tr>
  <tr>
     <td class="skinleft" id="skinleft">
     <DIV id="leftPanel" class="leftcontent">
      <table class="fdbody" id="fdbody" cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
        <tr>
           <td class="Leftbody" style="height:100%;">
           <div class="leftmenu" id="leftmenu_1">
               <h3 class="mmenu"><span><%=Lang_Menu_SystemSet%></span></h3>
               <ul class="m_menu">
               <li><a href="main.asp?ctl=systeminfo&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_SystemInfo%></a></li>
               <li><a href="systemset.asp?ctl=systemset&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_SystemBaseSet%></a></li>
               <li><a href="region.asp?ctl=regionset&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_DistrictSet%></a></li>
               <li><a href="cache.asp?ctl=cache&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_Cache%></a></li>
               </ul>
               
               <h4 class="mmenu"><span class="expand" onClick="ExpandShrink(this,'SysEManage')"><%=Lang_Menu_SystemEManage%></span></h4>
               <ul class="m_menu" id="SysEManage">
               <li><a href="kfc.systembackup.asp?ctl=systembackup&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_SystemBackup%></a></li>
               <%=PrintUploadFileManageMenu()%>
               </ul>
               
               <h4 class="mmenu"><span class="expand" onClick="ExpandShrink(this,'SysUser')"><%=Lang_Menu_SysUserManage %></span></h4>
               <ul class="m_menu" id="SysUser">
               <li><a href="sysusergroups.asp?ctl=sysusergroup&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_SysUserGroups %></a></li>
               <li><a href="sysusers.asp?ctl=sysuser&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_SysUsers %></a></li>
               <li><a href="sysusers.asp?ctl=sysuser_recycle&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_SysUsersRecycle %></a></li>
               </ul>
               
               <h4 class="mmenu"><span class="shrink" onClick="ExpandShrink(this,'SysLog')"><% Echo Lang_Menu_SysLog %></span></h4>
               <ul class="m_menu" id="SysLog" style="display:none;">
               <li><a href="syslog.asp?ctl=syslog&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_SysLogView %></a></li>
               <li><a href="syslog.asp?ctl=syslog_recycle&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_SysLogRecycle %></a></li>
               </ul>
           </div>
           <!--leftmenu_2-->
           <div class="leftmenu" id="leftmenu_2" style="display:none;">
               <h3 class="mmenu"><span><%=Lang_Menu_ContentSystem%></span></h3>
               <ul class="m_menu">
               <li><a href="content.subsystem.asp?ctl=contentsubsystem&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_SubSystemManage%></a></li>
               <li><a href="fieldset.asp?ctl=fieldset&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_FieldSet%></a></li>
               <li><a href="cmodule.asp?ctl=cmodule&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_CModule%></a></li>
               </ul>
               <%
			   Dim TempSys,TempSysName
			   Set Rs = KnifeCMS.DB.GetRecord(DBTable_ContentSubSystem &":System,SystemName","","ORDER BY Orders ASC")
               Do While Not(Rs.Eof)
			   i=i+1
			   TempSys     = "content_"&Rs(0)
			   TempSysName = Rs(1)
			   %>
               <h4 class="mmenu"><span <% Echo KnifeCMS.IIF(i=1,"class=expand","class=shrink") %> onClick="ExpandShrink(this,'<% Echo TempSys %>')"><% Echo TempSysName %></span></h4>
               <ul class="m_menu" id="<% Echo TempSys %>" <% Echo KnifeCMS.IIF(i=1,"style=''","style='display:none'") %>>
               <li><a href="content.asp?ctl=<% Echo TempSys %>&act=add" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_AddNewContent%></a></li>
               <li><a href="content.asp?ctl=<% Echo TempSys %>&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_ContentMange%></a></li>
               <li><a href="class.asp?ctl=<% Echo TempSys %>_class&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_ContentCategory%></a></li>
               <li><a href="content.asp?ctl=<% Echo TempSys %>_drafts&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_ContentDraft%></a></li>
               <li><a href="comment.asp?ctl=<% Echo TempSys %>_comment&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_ContentComment%></a></li>
               <li><a href="content.asp?ctl=<% Echo TempSys %>_recycle&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_ContentRecycle%></a></li>
               </ul>
               <%
               Rs.Movenext()
               Loop
               KnifeCMS.DB.CloseRs Rs 
               %>
           </div>
           <!--leftmenu_3-->
           <div class="leftmenu" id="leftmenu_3" style="display:none;">
               <h3 class="mmenu"><span><%=Lang_Menu_VisionSystem%></span></h3>
               <ul class="m_menu">
               <li><a href="navigator.asp?ctl=navigator&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_WebsiteNavigation%></a></li>
               <li><a href="template.asp?ctl=template&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_Template%></a></li>
               </ul>
               <h4 class="mmenu"><span class="expand" onClick="ExpandShrink(this,'Design')"><%=Lang_Menu_VisualDesign%></span></h4>
               <ul class="m_menu" id="Design">
               <li><a href="vision/index.asp?ctl=&act=list" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_PageList%></a></li>
               </ul>
           </div>
           <!--leftmenu_4-->
           <div class="leftmenu" id="leftmenu_4" style="display:none;">
               <h3 class="mmenu"><span><%=Lang_Menu_AssistSystem%></span></h3>
               <ul class="m_menu">
               <li><a href="module/index.asp?ctl=&act=list" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_ModuleAdmin%></a></li>
               <li><a href="kfc.onlineservice.asp?ctl=onlineservice&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_OnlineService%></a></li>
               <li><a href="guestbook.asp?ctl=guestbook&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_Guestbook%></a></li>
               <li><a href="link.asp?ctl=link&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_Link%></a></li>
               </ul>
               <h4 class="mmenu"><span class="expand" onClick="ExpandShrink(this,'Collection')"><%=Lang_Menu_CollectSystem%></span></h4>
               <ul class="m_menu" id="Collection">
               <li><a href="kfc.collection.asp?ctl=collection&act=add" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_AddNewCollection%></a></li>
               <li><a href="kfc.collection.asp?ctl=collection&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_CollectionManage%></a></li>
               <li><a href="kfc.collection.asp?ctl=collection_result&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_CollectionResultManage%></a></li>
               </ul>
               <h4 class="mmenu" <% If CloseModuleVote Then Echo "style=""display:none;""" %>><span class="expand" onClick="ExpandShrink(this,'Vote')"><%=Lang_Menu_VoteStstem%></span></h4>
               <ul class="m_menu" id="Vote" <% If CloseModuleVote Then Echo "style=""display:none;""" %>>
               <li><a href="vote.asp?ctl=vote&act=add" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_AddNewVote%></a></li>
               <li><a href="vote.asp?ctl=vote&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_VoteManage%></a></li>
               <li><a href="class.asp?ctl=vote_class&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_VoteCategory%></a></li>
               <li><a href="vote.asp?ctl=vote_recycle&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_VoteRecycle%></a></li>
               </ul>
           </div>
           <!--leftmenu_5-->
           <div class="leftmenu" id="leftmenu_5" style="display:none;">
               <h3 class="mmenu"><span><%=Lang_Menu_GoodsSystem%></span></h3>
               <ul class="m_menu">
               <li><a href="goods.asp?ctl=goods&act=add" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_AddNewGoods%></a></li>
               <li><a href="goods.asp?ctl=goods&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_GoodsManage%></a></li>
               <li><a href="goods.asp?ctl=goods_recycle&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_GoodsRecycle%></a></li>
               <li><a href="goodsfieldset.asp?ctl=goodsfieldset&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_GoodsFieldSet%></a></li>
               </ul>
               <h4 class="mmenu"><span class="shrink" onClick="ExpandShrink(this,'GoodsComment')"><%=Lang_Menu_GoodsCommentAndConsult%></span></h4>
               <ul class="m_menu" id="GoodsComment" style="display:none;">
               <li><a href="comment.asp?ctl=goods_consult&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_GoodsConsult%></a></li>
               <li><a href="comment.asp?ctl=goods_comment&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_GoodsComment%></a></li>
               </ul>
               <h4 class="mmenu"><span class="shrink" onClick="ExpandShrink(this,'GoodsClass')"><%=Lang_Menu_GoodsModelAndCategory%></span></h4>
               <ul class="m_menu" id="GoodsClass" style="display:none;">
               <li><a href="goodsmodel.asp?ctl=goodsmodel&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_GoodsModel%></a></li>
               <li><a href="class.asp?ctl=goods_class&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_GoodsCategory%></a></li>
               <li><a href="class.asp?ctl=goodsvirtual_class&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_GoodsVirtualCategory%></a></li>
               </ul>
               <h4 class="mmenu"><span class="shrink" onClick="ExpandShrink(this,'PaymentConfig')"><%=Lang_Menu_PaymentAndDelivery%></span></h4>
               <ul class="m_menu" id="PaymentConfig" style="display:none;">
               <li><a href="payment.asp?ctl=paymentconfig&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_PaymentManage%></a></li>
               <li><a href="delivery.asp?ctl=deliveryconfig&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_DeliveryManage%></a></li>
               <li><a href="deliverycorp.asp?ctl=deliverycorp&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_DeliveryCorp%></a></li>
               </ul>
               <h4 class="mmenu"><span class="shrink" onClick="ExpandShrink(this,'Brand')"><%=Lang_Menu_BrandSystem%></span></h4>
               <ul class="m_menu" id="Brand" style="display:none;">
               <li><a href="brand.asp?ctl=brand&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_BrandManage%></a></li>
               <li><a href="brand.asp?ctl=brand_recycle&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_BrandRecycle%></a></li>
               </ul>
               <h4 class="mmenu"><span class="shrink" onClick="ExpandShrink(this,'Order')"><%=Lang_Menu_OrderSystem%></span></h4>
               <ul class="m_menu" id="Order" style="display:none;">
               <li><a href="order.asp?ctl=order&act=add" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_AddNewOrder%></a></li>
               <li><a href="order.asp?ctl=order&act=view&all" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_OrderManage%></a></li>
               <li><a href="order.asp?ctl=order&act=view&subact=search&status[]=1&shipstatus[]=0&paystatus[]=0" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_OrderUnShipUnPay%></a></li>
               <li><a href="order.asp?ctl=order&act=view&subact=search&status[]=1&shipstatus[]=1&paystatus[]=0" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_OrderShipedUnPay%></a></li>
               <li><a href="order.asp?ctl=order&act=view&subact=search&status[]=1&shipstatus[]=0&paystatus[]=1" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_OrderUnShipPay%></a></li>
               <li><a href="order.asp?ctl=order&act=view&subact=search&status[]=1&shipstatus[]=4&paystatus[]=1,3,4" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_OrderReShipUnRefund%></a></li>
               <li><a href="order.asp?ctl=order&act=view&subact=search&status[]=2" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_OrderInvalid%></a></li>
               <li><a href="order.asp?ctl=order&act=view&subact=search&status[]=3" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_OrderFinish%></a></li>
               <li><a href="order.asp?ctl=order_recycle&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_OrderRecycle%></a></li>
               </ul>
               <h4 class="mmenu"><span class="shrink" onClick="ExpandShrink(this,'Payment')"><%=Lang_Menu_BillManage%></span></h4>
               <ul class="m_menu" id="Payment" style="display:none;">
               <li><a href="paymentbill.asp?ctl=paymentbill&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_PaymentBill%></a></li>
               <li><a href="refundbill.asp?ctl=refundbill&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_RefundBill%></a></li>
               <li><a href="shipbill.asp?ctl=shipbill&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_ShipBill%></a></li>
               <li><a href="shipbill.asp?ctl=reshipbill&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_ReshipBill%></a></li>
               </ul>
               <h4 class="mmenu"><span class="shrink" onClick="ExpandShrink(this,'PayTradeLog')"><%=Lang_Menu_TradeRecord%></span></h4>
               <ul class="m_menu" id="PayTradeLog" style="display:none;">
               <li><a href="paytradelog.asp?ctl=paytradelog&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_PaytradeRecord%></a></li>
               </ul>
           </div>
           <!--leftmenu_6-->
           <div class="leftmenu" id="leftmenu_6" style="display:none;">
               <h3 class="mmenu"><span><%=Lang_Menu_MemberSystem%></span></h3>
               <ul class="m_menu">
               <li><a href="user.asp?ctl=user&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_MemberManage%></a></li>
               <li><a href="user.asp?ctl=user_recycle&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_MemberRecycle%></a></li>
               <li><a href="twriter.asp?ctl=twriter&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_MemberTwriter%></a></li>
               <li><a href="userguestbook.asp?ctl=userguestbook&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_MemberGuestbook%></a></li>
               </ul>
           </div>
           <!--leftmenu_7-->
           <div class="leftmenu" id="leftmenu_7" style="display:none;">
               <h3 class="mmenu"><span><%=Lang_Menu_AdSystem%></span></h3>
               <ul class="m_menu">
               <li><a href="public.asp?ctl=adsgroup&act=add" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_AddNewAd%></a></li>
               <li><a href="public.asp?ctl=adsgroup&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_AdManage%></a></li>
               <li><a href="public.asp?ctl=adsgroup_recycle&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_AdRecycle%></a></li>
               </ul>
           </div>
           <!--leftmenu_8-->
           <div class="leftmenu" id="leftmenu_8" style="display:none;">
               <h3 class="mmenu"><span><%=Lang_Menu_WeiXin%></span></h3>
               <ul class="m_menu">
               <li><a href="weixin.asp?ctl=weixin&act=add" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_AddNewWeiXin%></a></li>
               <li><a href="weixin.asp?ctl=weixin&act=view" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_WeiXinManage%></a></li>
               </ul>
               <h4 class="mmenu"><span class="shrink" onClick="ExpandShrink(this,'WeXinMenu')"><%=Lang_Menu_WeiXinMenu%></span></h4>
               <ul class="m_menu" id="WeXinMenu" style="display:none;">
               <li><a href="weixin.asp?ctl=weixin&act=menu" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_WeiXinMenuConfig%></a></li>
               </ul>
               <h4 class="mmenu"><span class="shrink" onClick="ExpandShrink(this,'WeXinMenu')"><%=Lang_Menu_WeiXinMsg%></span></h4>
               <ul class="m_menu" id="WeXinMenu" style="display:none;">
               <li><a href="weixin.asp?ctl=weixin&act=msg" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_WeiXinMsgManage%></a></li>
               </ul>
               <h4 class="mmenu"><span class="shrink" onClick="ExpandShrink(this,'WeXinMenu')"><%=Lang_Menu_WeiXinUser%></span></h4>
               <ul class="m_menu" id="WeXinMenu" style="display:none;">
               <li><a href="weixin.asp?ctl=weixin&act=user" target="mainFrame"><input type="button" class="leftlimenu" disabled><%=Lang_Menu_WeiXinUserManage%></a></li>
               </ul>
           </div>
           </td>
        </tr>
      </table>
      </DIV>
     </td>
     <td class="skinright">
      <div id="mainFrameContainer" class="maincontent">
         <iframe src="main.asp?ctl=systeminfo&act=view" name="mainFrame" id="mainFrame" class="mainFrame" frameborder="no" scrolling="auto" style="width:100%;height:100%;" onload="autoFixIfm()" hidefocus/></iframe>
	  </div>
     </td>
  </tr>
</table>
<script type="text/javascript">
menuSelect();
</script>
</body>
</html>
<%
	End Sub
	
	Private Function PrintUploadFileManageMenu()

		Dim Fn_Html(4)
		Fn_Html(0) = "<li><a href=""kfc.filemanage.asp?ctl=file_image&act=view"" target=""mainFrame""><input type=""button"" class=""leftlimenu"" disabled>"& Lang_Menu_FileManage &"</a></li>"
		Fn_Html(1) = "<li><a href=""kfc.filemanage.asp?ctl=file_file&act=view"" target=""mainFrame""><input type=""button"" class=""leftlimenu"" disabled>"& Lang_Menu_FileManage &"</a></li>"
		Fn_Html(2) = "<li><a href=""kfc.filemanage.asp?ctl=file_flash&act=view"" target=""mainFrame""><input type=""button"" class=""leftlimenu"" disabled>"& Lang_Menu_FileManage &"</a></li>"
		Fn_Html(3) = "<li><a href=""kfc.filemanage.asp?ctl=file_music&act=view"" target=""mainFrame""><input type=""button"" class=""leftlimenu"" disabled>"& Lang_Menu_FileManage &"</a></li>"
		Fn_Html(4) = "<li><a href=""kfc.filemanage.asp?ctl=file_media&act=view"" target=""mainFrame""><input type=""button"" class=""leftlimenu"" disabled>"& Lang_Menu_FileManage &"</a></li>"
		
		If Admin.PermissionCheck(Array("file_image","view"))=True Then PrintUploadFileManageMenu = Fn_Html(0) : Exit Function
		If Admin.PermissionCheck(Array("file_file","view"))=True  Then PrintUploadFileManageMenu = Fn_Html(1) : Exit Function
		If Admin.PermissionCheck(Array("file_flash","view"))=True Then PrintUploadFileManageMenu = Fn_Html(2) : Exit Function
		If Admin.PermissionCheck(Array("file_music","view"))=True Then PrintUploadFileManageMenu = Fn_Html(3) : Exit Function
		If Admin.PermissionCheck(Array("file_media","view"))=True Then PrintUploadFileManageMenu = Fn_Html(4) : Exit Function
		
	End Function
End Class
%>
