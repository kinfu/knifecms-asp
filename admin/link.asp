<!--#include file="config.admin.asp"-->
<%
Dim Link
Set Link = New Class_KnifeCMS_Admin_Link
Set Link = Nothing
Class Class_KnifeCMS_Admin_Link

	Private Pv_Result
	Private Pv_LinkName,Pv_LinkLogo,Pv_LinkURL,Pv_Remarks,Pv_Recommend,Pv_Checked,Pv_OrderNum,Pv_AddTime,Pv_Recycle
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "link.lang.asp"
		Header()
		If Ctl="link" Then
			Select Case Action
				Case "view"
					Call LinkView()
				Case "add"
					If SubAction="save" Then
						Call LinkDoSave()
					Else
						Pv_OrderNum = 1000
						Call LinkDo()
					End If
				Case "edit"
					If SubAction="save" Then
						Call LinkDoSave()
					Else
						Call GetLinkDetail()
						If IsContentExist Then Call LinkDo() : Else Admin.ShowMessage Lang_Link(13),"goback",0
					End If
				Case "del"
					Call LinkDel()
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"goback",0
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Sub GetLinkDetail()
		Set Rs = KnifeCMS.DB.GetRecord(DBTable_Link,Array("ID:"&ID),"")
		If Not(Rs.Eof) Then
			IsContentExist = True
			Pv_LinkName    = Rs("LinkName")
			Pv_LinkLogo    = Rs("LinkLogo")
			Pv_LinkURL     = Rs("LinkURL")
			Pv_Recommend   = Rs("Recommend")
			Pv_OrderNum    = Rs("OrderNum")
			Pv_Remarks     = Rs("Remarks")
		Else
			IsContentExist = False
		End IF
		KnifeCMS.DB.CloseRs Rs
	End Sub
	
	Private Function LinkList()
		Dim Fn_TempSql,Fn_ColumnNum
		Fn_TempSql   = "SELECT a.ID,a.LinkName,a.LinkLogo,a.LinkURL,a.Hits,a.Recommend,a.OrderNum,a.Remarks FROM ["& DBTable_Link &"] a"
		Fn_ColumnNum = 8
		Fn_TempSql   = Fn_TempSql & " WHERE a.Recycle=0 ORDER BY a.ID DESC"
		Operation    = "<span class=""sysBtn""><a href=""?ctl="& Ctl &"&act=edit&id={$ID}"" ><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		Call Admin.DataList(Fn_TempSql,50,Fn_ColumnNum,Operation,PageUrlPara)
	End Function
	
	Private Function LinkDoSave()
		'获取数据并处理数据
		Pv_LinkName  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","LinkName")),250)
		Pv_LinkLogo  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","LinkLogo")),250)
		Pv_LinkURL   = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","LinkURL")),250)
		Pv_Remarks   = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Remarks")),250)
		Pv_Recommend = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Recommend"))
		Pv_OrderNum  = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","OrderNum"))
		'数据合法性检测并处理数据
		If KnifeCMS.Data.IsNul(Pv_LinkName) Then ErrMsg = Lang_Link(9) &"<br>"
		If KnifeCMS.Data.IsNul(Pv_LinkURL) Then ErrMsg = Lang_Link(10) &"<br>"
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,Lang_GoBack,0 : Exit Function
		IF Action="add" Then
			Result = KnifeCMS.DB.AddRecord(DBTable_Link,Array("SysID:"&SysID,"LinkName:"& Pv_LinkName,"Category:1","LinkLogo:"& Pv_LinkLogo,"LinkURL:"& Pv_LinkURL,"Remarks:"& Pv_Remarks,"Recommend:"& Pv_Recommend,"Checked:1","Hits:0","OrderNum:"& Pv_OrderNum,"AddTime:"& SysTime,"Recycle:0"))
			ID  = Admin.GetIDBySysID(DBTable_Link,SysID)
			Msg = Lang_Link(11) &"<br>[ID:"& ID &"]["& Lang_Link(2) &":"& Pv_LinkName &"]"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		ElseIf Action="edit" then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Link,Array("LinkName:"& Pv_LinkName,"LinkLogo:"& Pv_LinkLogo,"LinkURL:"& Pv_LinkURL,"Remarks:"& Pv_Remarks,"Recommend:"& Pv_Recommend,"OrderNum:"& Pv_OrderNum),Array("ID:"&ID))
			Msg = Lang_Link(12) &"<br>[ID:"& ID &"]["& Lang_Link(2) &":"& Pv_LinkName &"]"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		End IF
	End Function
	
	Private Function LinkDel()
		ID = KnifeCMS.GetForm("post","InputName")
		If KnifeCMS.Data.IsNul(Replace(ID,",","")) Then Exit Function
		Dim Fn_ArrID,Fn_ID,Fn_i
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			If ID > 0 Then
				Pv_LinkName = KnifeCMS.DB.GetFieldByID(DBTable_Link,"LinkName",ID)
				'开始执行操作
				If Action = "del" Then
					Result = KnifeCMS.DB.DeleteRecord(DBTable_Link,Array("ID:"&ID))
					If Result Then
						Msg = Msg & Lang_Link(15) &"[ID:"& ID &"]["& Lang_Link(2) &":"& Pv_LinkName &"]<br>"
					Else
						Msg = Msg & Lang_Link(16) &"[ID:"& ID &"]["& Lang_Link(2) &":"& Pv_LinkName &"]<br>"
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Function
	
    Function LinkView()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
          <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=add" ><b class="icon icon_add"></b><% echo Lang_DoAdd %></a></span>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%=KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></th>
			<th width="5%" ><div style="width:52px;" ><% Echo Lang_ListTableColLine_ID %></div></th>
			<th width="25%"><div style="width:100px;"><% Echo Lang_Link(2) %></div></th>
			<th width="8%"><div style="width:80px;"><% Echo Lang_Link(3) %></div></th>
			<th width="25%"><div style="width:150px;"><% Echo Lang_Link(4) %></div></th>
            <th width="5%" ><div style="width:52px;" ><% Echo Lang_Link(14) %></div></th>
			<th width="5%" ><div style="width:52px;" ><% Echo Lang_Link(6) %></div></th>
            <th width="5%" ><div style="width:30px;" ><% Echo Lang_Link(7) %></div></th>
            <th width="25%"><div style="width:80px;" ><% Echo Lang_Link(5) %></div></th>
			<th><div style="width:60px;"><% Echo Lang_ListTableColLine_Do %></div></th>
			</tr>
			<% Call LinkList() %>
		   </tbody>
		  </table>
		  </form>
		</div>
      </DIV>
    </DIV>
    <script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var fn_temp;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td0"){
					fn_temp=tds[i].innerHTML;
					tds[i].innerHTML='<a href="?ctl=<%=Ctl%>&act=edit&id='+fn_temp+'">'+fn_temp+'</a>';
				}
				if(tds[i].id=="td2" && tds[i].innerHTML!=""){
					TempHTML = "<span class=\"sImg\"><a href=javascript:;><img src='"+ unescape(tds[i].innerHTML) +"' /></a></span>";
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td3"){
					fn_temp=tds[i].innerHTML;
					tds[i].innerHTML="<a href=\""+fn_temp+"\" target=_blank>"+fn_temp+"</a>";
				}
				if(tds[i].id=="td5"){
					fn_label = "recommend";
					fn_id    = tds[i-5].title;
					img = document.createElement("img");
					img.setAttribute("class","pointer");
					if(parseInt(tds[i].innerHTML)==1){
						img.src="image/icon_yes.png";
						img.setAttribute("value",1);
					}else{
						img.src="image/icon_yes_gray.png";
						img.setAttribute("value",0);
					}
					img.setAttribute("id",fn_id);
					img.setAttribute("label",fn_label);
					img.onclick=function(){SetLinkLabel(this)}
					tds[i].title="";
					tds[i].innerHTML="";
					tds[i].appendChild(img);
				}
			}
		}
	}
	ChangeTdText();
	var SetLinkLabel = function(_this){
		var id,label,value,imgsrc,imgnewsrc,newvalue;
		id        = _this.getAttribute("id");
		label     = _this.getAttribute("label");
		imgsrc    = _this.src;
		value     = parseInt( _this.getAttribute("value"));
		_this.src = "image/loading.gif";
		label = _this.getAttribute("label");
		if(value>0){
			newvalue     = 0;
			imgnewsrc = "image/icon_yes_gray.png";
		}else{
			newvalue     = 1;
			imgnewsrc = "image/icon_yes.png";
		}
		var ajaxString = "label="+label+"&value="+newvalue;
		var ajaxUrl    = "ajax.get.asp?ctl=link&act=edit&id="+id+"&subact=setlabel&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		if(ajaxSta=="ok"){
			_this.setAttribute("value",newvalue);
			_this.src = imgnewsrc;
		}else{
			if(ajaxSta.indexOf(Lang_NoPermissions)>0){alert(Lang_NoPermissions);}
			_this.src = imgsrc;
		}
	}
	</script>
<%
    End Function
	
	'添加界面
	Private Function LinkDo()
	%>
	<DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
          <form name="SaveForm" action="?ctl=<%=Ctl%>&act=<%=Action%>&subact=save" method="post">
          <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
            <tbody>
               <tr><td class="titletd"><span class="required">*</span><%=Lang_Link(2)%>:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" name="LinkName" id="LinkName" maxLength="250" value="<% Echo Pv_LinkName %>"/>
                   <label class="Normal" id="d_LinkName"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><span class="required">*</span><%=Lang_Link(4)%>:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass long" name="LinkURL" id="LinkURL" maxLength="250" value="<% Echo Pv_LinkURL %>"/>
                   <label class="Normal" id="d_LinkURL"></label>
                   </td></tr>
               <tr><td class="titletd"><%=Lang_Link(3)%>:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass long" id="LinkLogo" name="LinkLogo" onblur="Admin.ImageUrlChange('LinkLogoBox','LinkLogo','','d_LinkLogo')" value="<%=Trim(KnifeCMS.Data.URLDecode(Pv_LinkLogo))%>" />
                   <span class="sysBtn"><a href="javascript:void(0)" onclick="UploadImg('1','LinkLogo');Admin.ImageUrlChange('LinkLogoBox','LinkLogo','','d_LinkLogo')"><b class="icon icon_add"></b>上传LOGO...</a></span>
                   <div id="LinkLogoBox" class="LinkLogoBox" style="margin-right:5px;">
				   <% If Not(KnifeCMS.Data.IsNul(Pv_LinkLogo)) Then Response.Write("<img src="""& Trim(KnifeCMS.Data.URLDecode(Pv_LinkLogo)) &""" />") %>
                   </div>
                   <label id="d_LinkLogo" style="display:none;"></label>
                   </td></tr>
            </tbody>
            </table>
          </div>
          <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
            <tbody>
               <tr><td class="titletd"><%=Lang_Link(5)%>:</td>
                   <td class="infotd">
                   <textarea class="abstract" id="Remarks" name="Remarks"><%=Pv_Remarks%></textarea>
                   <label class="Normal" id="d_Remarks"></label>
                   </td></tr>
               <tr><td class="titletd"><%=Lang_Link(6)%>:</td>
                   <td class="infotd">
                   <input id="Recommend" name="Recommend" type="checkbox" <%=Admin.InputChecked(Pv_Recommend)%> value="1"><%=Lang_Recommend%>
                   <label class="Normal" id="d_Recommend"></label>
                   </td></tr>
               <tr><td class="titletd"><%=Lang_Link(7)%>:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" name="OrderNum" id="OrderNum" maxlength="10" value="<%=Pv_OrderNum%>" style="width:60px;"/>
                   <label class="Normal" id="d_OrderNum"></label>
                   </td>
               </tr>
             </tbody>
            </table>
          </div>
          <div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
            <input type="hidden" name="id" value="<% Echo ID %>" />
            <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
            <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="SaveLinkCheck('<%=Action%>')"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
            <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
          </div>
          </form>
        </div>
      </DIV>
	</DIV>
    <script type="text/javascript">
    var SaveLinkCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("LinkName","d_LinkName",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("LinkURL","d_LinkURL",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit()
		}
	}
    </script>
	<%
	End Function
End Class
%>
