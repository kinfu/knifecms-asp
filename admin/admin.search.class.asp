<%
'**
'后台搜索操作类
'filename: admin.search.class.asp
'copyright: KFC
'**
Class Class_KnifeCMS_Admin_Search
	Private S_ExplicitSearch,S_ClassID,S_KeyWords,S_StartDate,S_EndDate
	Private S_CollectionID
	Private S_GoodsType,S_BrandID
	
	Private Sub Class_Initialize()
	End Sub
	
	Public Property Let ExplicitSearch(ByVal BV_Str)
		S_ExplicitSearch = BV_Str
	End Property
	Public Property Get ExplicitSearch()
		ExplicitSearch = S_ExplicitSearch
	End Property
	
	Public Property Let ClassID(ByVal BV_Str)
		S_ClassID = BV_Str
	End Property
	Public Property Get ClassID()
		ClassID = S_ClassID
	End Property
	
	Public Property Let KeyWords(ByVal BV_Str)
		S_KeyWords = BV_Str
	End Property
	Public Property Get KeyWords()
		KeyWords = S_KeyWords
	End Property
	
	Public Property Let StartDate(ByVal BV_Str)
		S_StartDate = BV_Str
	End Property
	Public Property Get StartDate()
		StartDate = S_StartDate
	End Property
	
	Public Property Let EndDate(ByVal BV_Str)
		S_EndDate = BV_Str
	End Property
	Public Property Get EndDate()
		EndDate = S_EndDate
	End Property
	
	Public Property Let CollectionID(ByVal BV_Str)
		S_CollectionID = BV_Str
	End Property
	Public Property Get CollectionID()
		CollectionID = S_CollectionID
	End Property
	
	Public Property Let GoodsType(ByVal BV_Str)
		S_GoodsType = BV_Str
	End Property
	Public Property Get GoodsType()
		GoodsType= S_GoodsType
	End Property
	
	Public Property Let BrandID(ByVal BV_Str)
		S_BrandID = BV_Str
	End Property
	Public Property Get BrandID()
		BrandID= S_BrandID
	End Property
	
	Private Sub Class_Terminate()
	End Sub
End Class
%>