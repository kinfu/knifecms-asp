<!--#include file="config.admin.asp"-->
<%
Dim Delivery
Set Delivery = New Class_KnifeCMS_Admin_Delivery
Set Delivery = Nothing
Class Class_KnifeCMS_Admin_Delivery

	Private Pv_Rs,Pv_TempHTML,Pv_i
	Private DeliveryName,Config,Config_ItemNum,Expressions,Intro,Price,DeliveryType,Gateway,Protect,ProtectRate,ProtectMinPrice,OrderNum,HasCod,MinPrice,Disabled,CorpID
	Private AreaFeeSet,FirstUnit,ContinueUnit,FirstPrice,ContinuePrice,SetAreaDefaultFee,Area,AreaGroupID,AreaFirstPrice,AreaContinuePrice
	Private Area_i,AreaGroupID_i,AreaFirstPrice_i,AreaContinuePrice_i
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "delivery.lang.asp"
		Config_ItemNum = 0
		HasCod     = 0
		Protect    = 0
		AreaFeeSet = 0
		Header()
		If Ctl="deliveryconfig" Then
			Select Case Action
				Case "view"
					DeliveryView
				Case "add","edit"
					If SubAction="save" Then
						DeliveryDoSave
					Else
						If Action = "add" Then
							DeliveryDo
						ElseIf Action = "edit" Then
							If SubAction = "disabled" Or SubAction = "undisabled" Then
									DeliveryOtherEdit
							Else
								Set Rs = KnifeCMS.DB.GetRecord(DBTable_Delivery,Array("ID:"&ID),"")
								If Not(Rs.Eof) Then
									DeliveryName   = Rs("DeliveryName")
									Config         = Rs("Config")
									Expressions    = Rs("Expressions")
									Intro          = Rs("Intro")
									Protect        = Rs("Protect")
									ProtectRate    = Rs("ProtectRate")
									ProtectMinPrice= Rs("ProtectMinPrice")
									OrderNum       = Rs("OrderNum")
									HasCod         = Rs("HasCod")
									FirstPrice     = KnifeCMS.Data.FormatDouble(KnifeCMS.GetConfigParameter(Config,"FirstPrice"))
									FirstUnit      = KnifeCMS.Data.CLng(KnifeCMS.GetConfigParameter(Config,"FirstUnit"))
									ContinuePrice  = KnifeCMS.Data.FormatDouble(KnifeCMS.GetConfigParameter(Config,"ContinuePrice"))
									ContinueUnit   = KnifeCMS.Data.CLng(KnifeCMS.GetConfigParameter(Config,"ContinueUnit"))
									AreaFeeSet     = KnifeCMS.Data.CLng(KnifeCMS.GetConfigParameter(Config,"AreaFeeSet"))
									SetAreaDefaultFee = KnifeCMS.Data.CLng(KnifeCMS.GetConfigParameter(Config,"SetAreaDefaultFee"))
								Else
									Admin.ShowMessage Lang_Delivery_Cue(7),"goback",0 : Die ""
								End IF
								KnifeCMS.DB.CloseRs Rs
								DeliveryDo
							End IF
						End IF
					End IF
				Case "del"  : DeliveryDel
			End Select
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function DeliveryList()
		Dim Fn_TempSql,Fn_ColumnNum
		Fn_ColumnNum = 6
		Fn_TempSql   = "SELECT a.ID,a.DeliveryName,a.Disabled,a.Protect,a.HasCod,a.OrderNum FROM ["&DBTable_Delivery&"] a "
		Fn_TempSql   = Fn_TempSql & "ORDER BY a.Disabled ASC, a.OrderNum ASC, a.ID ASC"
		Operation    = "<span class=""sysBtn""><a href=""?ctl="&Ctl&"&act=edit&id={$ID}""><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
	End Function
	
	Private Function DeliveryDoSave()
		'获取并处理数据
		DeliveryName    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","DeliveryName")),100)
		HasCod          = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","HasCod"))
		FirstUnit       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","FirstUnit"))
		ContinueUnit    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","ContinueUnit"))
		Protect         = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Protect"))
		ProtectRate     = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","ProtectRate"))/100
		ProtectMinPrice = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","ProtectMinPrice"))
		AreaFeeSet      = KnifeCMS.IIF(KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","AreaFeeSet"))<>2,1,2)
		SetAreaDefaultFee = KnifeCMS.IIF(KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","SetAreaDefaultFee"))<>1,0,1)
		FirstPrice      = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","FirstPrice"))
		ContinuePrice   = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","ContinuePrice"))
		OrderNum        = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","OrderNum"))
		Intro           = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","TextContent")),10000)
		CorpID          = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","CorpID"))
		If AreaFeeSet=2 Then
			FirstPrice    = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","AreaDefaultFirstPrice"))
			ContinuePrice = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","AreaDefaultContinuePrice"))
			Pv_i=1
			for each Area_i in Request.Form("Area[]")
				Area_i = KnifeCMS.Data.RegReplace(Trim(Area_i),"[':""|]","")
				Area = KnifeCMS.IIF(Pv_i=1,KnifeCMS.Data.HTMLEncode(Area_i),Area &"|"& KnifeCMS.Data.HTMLEncode(Area_i))
				Pv_i=Pv_i+1
			next
			Area=Split(Area,"|")
			Pv_i=1
			for each AreaGroupID_i in Request.Form("AreaGroupID[]")
				AreaGroupID_i = KnifeCMS.Data.RegReplace(Trim(AreaGroupID_i),"[^0-9a-z,\[\]\{\}"":]","")
				AreaGroupID = KnifeCMS.IIF(Pv_i=1,AreaGroupID_i,AreaGroupID &"|"& AreaGroupID_i)
				Pv_i=Pv_i+1
			next
			AreaGroupID=Split(AreaGroupID,"|")
			Pv_i=1
			for each AreaFirstPrice_i in Request.Form("AreaFirstPrice[]")
				AreaFirstPrice_i = KnifeCMS.Data.FormatDouble(AreaFirstPrice_i)
				AreaFirstPrice = KnifeCMS.IIF(Pv_i=1,AreaFirstPrice_i,AreaFirstPrice &"|"& AreaFirstPrice_i)
				Pv_i=Pv_i+1
			next
			AreaFirstPrice=Split(AreaFirstPrice,"|")
			Pv_i=1
			for each AreaContinuePrice_i in Request.Form("AreaContinuePrice[]")
				AreaContinuePrice_i = KnifeCMS.Data.FormatDouble(AreaContinuePrice_i)
				AreaContinuePrice = KnifeCMS.IIF(Pv_i=1,AreaContinuePrice_i,AreaContinuePrice &"|"& AreaContinuePrice_i)
				Pv_i=Pv_i+1
			next
			AreaContinuePrice=Split(AreaContinuePrice,"|")
		End If
		
		Config_ItemNum = 7
		Expressions    = FirstPrice &"+(({w-"& FirstUnit &"})/"& ContinueUnit &")*"& ContinuePrice &""
		Config         = "a:"& Config_ItemNum &":{s:10:""FirstPrice"";s:"&Len(FirstPrice)&":"""&FirstPrice&""";s:9:""FirstUnit"";s:"&Len(FirstUnit)&":"""&FirstUnit&""";s:13:""ContinuePrice"";s:"&Len(ContinuePrice)&":"""&ContinuePrice&""";s:12:""ContinueUnit"";s:"&Len(ContinueUnit)&":"""&ContinueUnit&""";s:11:""Expressions"";s:"&Len(Expressions)&":"""&Expressions&""";s:10:""AreaFeeSet"";s:"&Len(AreaFeeSet)&":"""&AreaFeeSet&""";s:19:""SetAreaDefaultFee"";s:"&Len(SetAreaDefaultFee)&":"""&SetAreaDefaultFee&""";}"
		
		'数据合法性检测并处理数据
		If KnifeCMS.Data.IsNul(DeliveryName)  Then ErrMsg = ErrMsg & Lang_Delivery_Cue(8) &"<br>"
		If AreaFeeSet=2 Then
			If Ubound(Area) < 0 Or (Ubound(Area)<>Ubound(AreaGroupID) Or Ubound(AreaGroupID)<>Ubound(AreaFirstPrice) Or Ubound(AreaFirstPrice)<>Ubound(AreaContinuePrice)) Then 
			  ErrMsg = ErrMsg & Lang_WrongData &"<br>"
			End If
		End If
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		
		IF Action="add" Then
			Result = KnifeCMS.DB.AddRecord(DBTable_Delivery,Array("SysID:"&SysID,"DeliveryName:"& DeliveryName,"Config:"& Config,"Expressions:"& Expressions,"Intro:"& Intro,"Price:0","DeliveryType:1","Gateway:1","Protect:"& Protect,"ProtectRate:"& ProtectRate,"ProtectMinPrice:"& ProtectMinPrice,"HasCod:"& HasCod,"CorpID:"& CorpID,"OrderNum:"& OrderNum,"Disabled:0"))
			ID = Admin.GetIDBySysID(DBTable_Delivery,SysID)
			'插入指定配送地区和费用数据
			If AreaFeeSet = 2 Then
				Config_ItemNum = 3
				For Pv_i=0 To Ubound(Area)
					If Not(KnifeCMS.Data.IsNul(Area(Pv_i))) Then
						Expressions    = AreaFirstPrice(Pv_i) &"+(({w-"& FirstUnit &"})/"& ContinueUnit &")*"& AreaContinuePrice(Pv_i)
						Config         = "a:"& Config_ItemNum &":{s:10:""FirstPrice"";s:"&Len(AreaFirstPrice(Pv_i))&":"""&AreaFirstPrice(Pv_i)&""";s:13:""ContinuePrice"";s:"&Len(AreaContinuePrice(Pv_i))&":"""&AreaContinuePrice(Pv_i)&""";s:11:""Expressions"";s:"&Len(Expressions)&":"""&Expressions&""";}"
						Call KnifeCMS.DB.AddRecord(DBTable_Delivery_Area,Array("DeliveryID:"& ID,"AreaID:0","Price:0","HasCod:"& HasCod,"AreaName_Group:"& Area(Pv_i),"AreaID_Group:"& AreaGroupID(Pv_i),"Config:"& Config,"Expressions:"& Expressions,"OrderNum:"& OrderNum))
					End If
				Next
			End If
			Msg = Lang_Delivery_Cue(1) &"["& DeliveryName &"]"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		ElseIf Action="edit" Then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Delivery,Array("DeliveryName:"& DeliveryName,"Config:"& Config,"Expressions:"& Expressions,"Intro:"& Intro,"Price:0","DeliveryType:1","Gateway:1","Protect:"& Protect,"ProtectRate:"& ProtectRate,"ProtectMinPrice:"& ProtectMinPrice,"HasCod:"& HasCod,"CorpID:"& CorpID,"OrderNum:"& OrderNum),Array("ID:"&ID))
			Call KnifeCMS.DB.DeleteRecord(DBTable_Delivery_Area,Array("DeliveryID:"&ID))
			'插入指定配送地区和费用数据
			If AreaFeeSet = 2 Then
				Config_ItemNum = 3
				For Pv_i=0 To Ubound(Area)
					If Not(KnifeCMS.Data.IsNul(Area(Pv_i))) Then
						Expressions    = AreaFirstPrice(Pv_i) &"+(({w-"& FirstUnit &"})/"& ContinueUnit &")*"& AreaContinuePrice(Pv_i)
						Config         = "a:"& Config_ItemNum &":{s:10:""FirstPrice"";s:"&Len(AreaFirstPrice(Pv_i))&":"""&AreaFirstPrice(Pv_i)&""";s:13:""ContinuePrice"";s:"&Len(AreaContinuePrice(Pv_i))&":"""&AreaContinuePrice(Pv_i)&""";s:11:""Expressions"";s:"&Len(Expressions)&":"""&Expressions&""";}"
						Call KnifeCMS.DB.AddRecord(DBTable_Delivery_Area,Array("DeliveryID:"& ID,"AreaID:0","Price:0","HasCod:"& HasCod,"AreaName_Group:"& Area(Pv_i),"AreaID_Group:"& AreaGroupID(Pv_i),"Config:"& Config,"Expressions:"& Expressions,"OrderNum:"& OrderNum))
					End If
				Next
			End If
			Msg = Lang_Delivery_Cue(2) & "{"& DeliveryName &"[ID="& ID &"]}"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		End IF
	End Function
	
	Private Function DeliveryDel()
		DeliverySubDo
	End Function
	Private  Function DeliveryOtherEdit()
		DeliverySubDo
	End Function
	
	Private Function DeliverySubDo()
		ID = KnifeCMS.GetForm("post","InputName")
		If KnifeCMS.Data.IsNul(Replace(ID,",","")) Then Exit Function
		Dim Fn_ArrID,Fn_ID,Fn_i,Fn_Rs
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			If ID > 0 Then
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Delivery&":DeliveryName",Array("ID:"&ID),"")
				If Not(Fn_Rs.Eof) Then DeliveryName=Fn_Rs(0)
				KnifeCMS.DB.CloseRs Fn_Rs
				'开始执行操作
				If Action = "del" Then
					Result = KnifeCMS.DB.DeleteRecord(DBTable_Delivery,Array("ID:"&ID))
					Result = KnifeCMS.DB.DeleteRecord(DBTable_Delivery_Area,Array("DeliveryID:"&ID))
					Msg    = Msg & Lang_Delivery_Cue(4) &"{"& DeliveryName &"[ID="& ID &"]}<br>"
				ElseIf Action = "edit" Then
					If SubAction = "undisabled" Then
						Result = KnifeCMS.DB.UpdateRecord(DBTable_Delivery,Array("Disabled:0"),Array("ID:"&ID))
						Msg    = Msg & Lang_Delivery_Cue(5) &"{"& DeliveryName &"[ID="& ID &"]}<br>"
					ElseIf SubAction = "disabled" Then
						Result = KnifeCMS.DB.UpdateRecord(DBTable_Delivery,Array("Disabled:1"),Array("ID:"&ID))
						Msg    = Msg & Lang_Delivery_Cue(6) &"{"& DeliveryName &"[ID="& ID &"]}<br>"
					End If
				End If
			End If
		Next
		If Not(KnifeCMS.Data.IsNul(Msg)) Then Admin.ShowMessage Msg,Url,1
	End Function
	
	Private Function WeightUnitOptions(ByVal BV_Value)
		Dim Fn_Array : Fn_Array = Array("500","1000","1200","2000","2500","5000","10000","20000","50000")
		Dim Fn_Array_Lang : Fn_Array_Lang = Array("0.5kg","1.0kg","1.2kg","2.0kg","2.5kg","5.0kg","10kg","20kg","50kg")
		Dim Fn_i
		Pv_TempHTML = ""
		BV_Value    = KnifeCMS.Data.CLng(BV_Value)
		For Fn_i=0 To Ubound(Fn_Array)
			Pv_TempHTML = Pv_TempHTML & "<option "
			If KnifeCMS.Data.CLng(Fn_Array(Fn_i)) = KnifeCMS.Data.CLng(BV_Value) Then
				Pv_TempHTML = Pv_TempHTML & "selected"
			End If
			Pv_TempHTML = Pv_TempHTML & " value="&Fn_Array(Fn_i)&">"&Fn_Array_Lang(Fn_i)&"</option>"
		Next
		WeightUnitOptions = Pv_TempHTML
	End Function
	
	Private Function DeliveryView()
%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table bDelivery="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td class="tdcell">
			  <table bDelivery="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
                  <span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=add"><b class="icon icon_add"></b><% Echo Lang_DoAdd&Lang_Delivery(0) %></a></span>
				  </td></tr>
			  </table>
		  </td>
          <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onclick="Operate('listForm','InputName','edit:undisabled','')"><b class="icon icon_start"></b><% Echo Lang_DoOnUse %></button>
				  <button type="button" class="sysBtn" onclick="Operate('listForm','InputName','edit:disabled','')"><b class="icon icon_stop"></b><% Echo Lang_DoStopUse %></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table bDelivery="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onclick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th width="1%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
			<th width="20%"><div style="width:200px;"><% Echo Lang_Delivery_ListTableColLine(0) %><!--配送方式--></div></th>
            <th width="10%"><div style="width:100px;"><% Echo Lang_Delivery_ListTableColLine(1) %><!--状态--></div></th>
            <th width="5%"><div style="width:60px;"><% Echo Lang_Delivery_ListTableColLine(2) %><!--物流保价--></div></th>
            <th width="5%"><div style="width:60px;"><% Echo Lang_Delivery_ListTableColLine(3) %><!--货到付款--></div></th>
            <th width="5%"><div style="width:60px;"><% Echo Lang_Delivery_ListTableColLine(4) %><!--排序--></div></th>
			<th><div style="width:60px;"><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% DeliveryList %>
		   </tbody>
		  </table>
		  </form>
		</div>
      </DIV>
	</DIV>
    <script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td2"){
					TempHTML = tds[i].innerHTML;
					if(TempHTML=="0"){tds[i].innerHTML="<span style=\"color:#048E08;\">"+Lang_Js_OnUse+"</span>";}else if(TempHTML=="1"){ tds[i].innerHTML="<span style=\"color:#CD2E03;\">"+Lang_Js_HaveStopUse+"</span>"}
				}
				if(tds[i].id=="td3" || tds[i].id=="td4"){
					TempHTML = tds[i].innerHTML;
					if(TempHTML=="1"){tds[i].innerHTML="是";}else if(TempHTML=="0"){tds[i].innerHTML="否";}
				}
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
	Private Function DeliveryDo()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
          <div class="inbox">
            <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable mb5">
              <tbody>
               <tr><td class="titletd"><% Echo Lang_Delivery(1) %><!--配送方式名称-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="DeliveryName" name="DeliveryName" style="width:200px;" value="<% Echo DeliveryName %>" />
                   <label id="d_DeliveryName"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Delivery(2) %><!--类型-->:</td>
                   <td class="infotd">
                   <input type="radio" name="HasCod" value="0" <% Echo Admin.Input_Checked(HasCod,"0") %> /><span class="mr20"><% Echo Lang_Delivery(3) %><!--先收款后发货--></span>
                   <input type="radio" name="HasCod" value="1" <% Echo Admin.Input_Checked(HasCod,"1") %> /><span class=""><% Echo Lang_Delivery(4) %><!--货到付款--></span>
                   <label class="Normal"><% Echo Lang_Delivery(5) %><!--支持货到付款后顾客可以选择所有选择支付方式--></label>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Delivery(6) %><!--重量设置-->:</td>
                   <td class="infotd">
                   <span><% Echo Lang_Delivery(7) %><!--首重重量--></span>
                   <SELECT name="FirstUnit" id="FirstUnit">
                   <option value="0"><% Echo Lang_PleaseSelect %></option>
                   <% Echo WeightUnitOptions(FirstUnit) %>
                   </SELECT>
                   <label id="d_FirstUnit"></label>
                   <span class="ml10"><% Echo Lang_Delivery(8) %><!--续重单位--></span>
                   <SELECT name="ContinueUnit" id="ContinueUnit">
                   <option value="0"><% Echo Lang_PleaseSelect %></option>
                   <% Echo WeightUnitOptions(ContinueUnit) %>
                   </SELECT>
                   <label id="d_ContinueUnit"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Delivery(9) %><!--保价-->:</td>
                   <td class="infotd">
                   <input type="checkbox" name="Protect" onclick="ShowCheckbox(this,'1:1','ProtectSetArea')" value="1" <% Echo Admin.Input_Checked(Protect,"1") %> /><% Echo Lang_Delivery(10) %><!--支持物流保价-->
                   <div class="mt3" id="ProtectSetArea" <% If Protect = "1" then Echo "style=""display:;""" Else Echo "style=""display:none;""" : End If %> >
                   <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                   <tr><td class="titletd" style="width:100px;"><% Echo Lang_Delivery(11) %><!--费率-->:</td><td class="infotd"><input type="text" class="TxtClass" name="ProtectRate" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g','maxLength:9'))" style="width:60px;" value="<% Echo ProtectRate*100 %>" />%</td></tr>
                   <tr><td class="titletd" style="width:100px;"><% Echo Lang_Delivery(12) %><!--最低保价费-->:</td><td class="infotd"><input type="text" class="TxtClass" name="ProtectMinPrice" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g','maxLength:9'))" style="width:60px;" value="<% Echo ProtectMinPrice %>" /></td></tr>
                   </table>
                   </div>
                   </td>
               </tr>
               <tr><td class="titletd"><% Echo Lang_Delivery(13) %><!--配送地区费用设置-->:</td>
                   <td class="infotd">
                   <input type="radio" value="1" name="AreaFeeSet" onclick="ShowOrHidden('AreaFeeSet_1:AreaFeeSet_2','1:0')" <% Echo Admin.Input_Checked(AreaFeeSet,"1") %> /><span class="mr20"><% Echo Lang_Delivery(14) %><!--统一设置--></span>
                   <input type="radio" value="2" name="AreaFeeSet" onclick="ShowOrHidden('AreaFeeSet_1:AreaFeeSet_2','0:1')" <% Echo Admin.Input_Checked(AreaFeeSet,"2") %> /><span><% Echo Lang_Delivery(15) %><!--指定配送地区和费用--></span>
                   <label id="d_AreaFeeSet"></label>
                   </td>
               </tr>
               </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable" id="AreaFeeSet_1" <% If AreaFeeSet = "1" then Echo "style=""display:;""" Else Echo "style=""display:none;""" : End If %>>
              <tbody>
                <tr><td class="titletd"><% Echo Lang_Delivery(16) %><!--配送费用-->:</td><td class="infotd">
                    <span><% Echo Lang_Delivery(17) %><!--首重费用--></span><input type="text" class="TxtClass Short" name="FirstPrice" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g','maxLength:9'))" value="<% Echo FirstPrice %>" /><span class="ml20"><% Echo Lang_Delivery(18) %><!--续重费用--></span><input type="text" class="TxtClass Short" name="ContinuePrice" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g','maxLength:9'))" value="<% Echo ContinuePrice %>" />
                    </td>
                </tr>
              </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable" id="AreaFeeSet_2" <% If AreaFeeSet = "2" then Echo "style=""display:;""" Else Echo "style=""display:none;""" : End If %>>
              <tbody>
                <tr><td class="titletd">&nbsp;</td><td class="infotd">
                   <input type="checkbox" name="SetAreaDefaultFee" onclick="ShowCheckbox(this,'1:1','DefaultFee')" value="1" <% Echo Admin.Input_Checked(SetAreaDefaultFee,"1") %> /><% Echo Lang_Delivery(19) %><!--启用默认费用-->
                   <label class="Normal"><% Echo Lang_Delivery(20) %><!--注意：未启用默认费用时，不在指定配送地区的顾客不能使用本配送方式下订单--></label>
                   <div class="wbox mt3" id="DefaultFee" <% If SetAreaDefaultFee = "1" then Echo "style=""display:;""" Else Echo "style=""display:none;""" : End If %>>
                   <span><% Echo Lang_Delivery(17) %><!--首重费用--></span><input type="text" class="TxtClass Short" name="AreaDefaultFirstPrice" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g','maxLength:9'))" value="<% Echo FirstPrice %>" /><span class="ml20"><% Echo Lang_Delivery(18) %><!--续重费用--></span><input type="text" class="TxtClass Short" name="AreaDefaultContinuePrice" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g','maxLength:9'))" value="<% Echo ContinuePrice %>" />
                   </div>
                   </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_Delivery(21) %><!--指定配送地区-->:</td><td class="infotd">
                       <label id="d_Area[]"></label>
                       <div id="AreaFees">
                           <%
                           If Action="edit" And ID > 0 Then
                            Pv_TempHTML = ""
							Pv_i        = 0
                            Set Pv_Rs = KnifeCMS.DB.GetRecord(DBTable_Delivery_Area,Array("DeliveryID:"&ID),"")
                            Do While Not(Pv_Rs.Eof)
                                Config      = Pv_Rs("Config")
                                Area        = Pv_Rs("AreaName_Group")
                                AreaGroupID = Pv_Rs("AreaID_Group")
                                AreaFirstPrice    = KnifeCMS.Data.FormatDouble(KnifeCMS.GetConfigParameter(Config,"FirstPrice"))
                                AreaContinuePrice = KnifeCMS.Data.FormatDouble(KnifeCMS.GetConfigParameter(Config,"ContinuePrice"))
                                
                                Pv_TempHTML = Pv_TempHTML & "<div class=""inbox""><a class=""reduce"" onclick=""Admin.Delivery.DeleteDeliveryAreaFee(this)""; href=""javascript:;""><img src=""image/reduce.gif""></a>"
                                Pv_TempHTML = Pv_TempHTML & "<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""infoTable"">"
                                Pv_TempHTML = Pv_TempHTML & "<tr><td class=""titletd"">"& Lang_Delivery(22) &"<!--配送地区-->:</td><td class=""infotd""><input type=""text"" class=""TxtClass mr5"" readonly=""readonly"" name=""Area[]"" order=""e"&Pv_i&""" style=""width:300px;"" value="""& Area &""" /><input type=""hidden"" order=""e"&Pv_i&""" name=""AreaGroupID[]"" value='"& AreaGroupID &"' /><span class=""sysBtn""><a href=""javascript:;"" onclick=""Admin.Delivery.SelectDeliveryArea(this)""><b class=""icon icon_edit""></b>"& Lang_Btn_Select &"</a></span></td></tr>"
                                Pv_TempHTML = Pv_TempHTML & "<tr><td class=""titletd"">"& Lang_Delivery(23) &"<!--费用-->:</td><td class=""infotd""><span>"& Lang_Delivery(17) &"<!--首重费用--></span><input type=""text"" class=""TxtClass Short"" name=""AreaFirstPrice[]"" onblur=""KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g','maxLength:9'))"" value="""& AreaFirstPrice &""" /><span class=""ml20"">"& Lang_Delivery(18) &"<!--续重费用--></span><input type=""text"" class=""TxtClass Short"" name=""AreaContinuePrice[]"" onblur=""KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g','maxLength:9'))"" value="""& AreaContinuePrice &""" />"
                                Pv_TempHTML = Pv_TempHTML & "</td></tr>"
                                Pv_TempHTML = Pv_TempHTML & "</table>"
                                Pv_TempHTML = Pv_TempHTML & "</div>"
								Pv_i = Pv_i + 1
                            Pv_Rs.Movenext()
                            Loop
                            KnifeCMS.DB.CloseRs Pv_Rs
                            Echo Pv_TempHTML
						   ElseIf Action="add" Then
						   %>
                           <div class="inbox">
                             <a class="reduce" onclick="Admin.Delivery.DeleteDeliveryAreaFee(this)"; href="javascript:;"><img src="image/reduce.gif"></a>
                             <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                               <tr><td class="titletd"><% Echo Lang_Delivery(22) %><!--配送地区-->:</td><td class="infotd"><input type="text" class="TxtClass mr5" readonly="readonly" name="Area[]" order="a0" style="width:300px;" value="" /><input type="hidden" order="a0" name="AreaGroupID[]" value="" /><span class="sysBtn"><a href="javascript:;" onclick="Admin.Delivery.SelectDeliveryArea(this)"><b class="icon icon_edit"></b><% Echo Lang_Btn_Select %></a></span></td></tr>
                               <tr><td class="titletd"><% Echo Lang_Delivery(23) %><!--费用-->:</td><td class="infotd"><span><% Echo Lang_Delivery(17) %><!--首重费用--></span><input type="text" class="TxtClass Short" name="AreaFirstPrice[]" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g','maxLength:9'))" value="0" /><span class="ml20"><% Echo Lang_Delivery(18) %><!--续重费用--></span><input type="text" class="TxtClass Short" name="AreaContinuePrice[]" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g','maxLength:9'))" value="0" />
                                   </td></tr>
                             </table>
                           </div>
						   <%
                           End If
                           %>
                       </div>
                       <div class="pt5 pb5"><span class="sysBtn"><a href="javascript:;" onclick="Admin.Delivery.AddDeliveryAreaFee()"><b class="icon icon_add"></b><% Echo Lang_Delivery(24) %><!--为指定的地区设置运费--></a></span></div>
                   </td>
                </tr>
              </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable mt5">
              <tbody>
               <tr><td class="titletd"><% Echo Lang_Delivery(25) %><!--排序-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="OrderNum" name="OrderNum" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9]*/g','maxLength:5'))" style="width:60px;" value="<% Echo OrderNum %>" />
                   <label class="Normal"><% Echo Lang_Delivery(26) %><!--数字越小越靠前-->.</label>
                   </td>
               </tr>
               
               <tr><td class="titletd"><% Echo Lang_Delivery(27) %><!--说明-->:</td>
                   <td class="infotd">
                   <!--<div id="treejsonvaluec"></div>
                   <div id="treejsonvalue"></div>-->
                   <textarea class="TextContent" id="TextContent" name="TextContent"><%=KnifeCMS.Data.HTMLDecode(Intro)%></textarea>
                   <label class="Normal"></label>
                   
                   </td>
               </tr>
               
              </tbody>
            </table>
            <input type="hidden" name="id" value="<%=ID%>" />
            <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
            <div class="bottomSaveline" id="SubmitButtoms">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Delivery.SaveDeliveryCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
            </div>
            </form>
          </div>
        </div>
      </DIV>
    </DIV>
    <script type="text/javascript" charset="utf-8">
	window.UEDITOR_HOME_URL = "<%=SystemPath%>plugins/ueditor/";
	</script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_config.asp"></script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_api.js"></script>
    <script type="text/javascript">
	var OSWebEditor=UE.getEditor('TextContent');
	</script>
	<!--<script type="text/javascript">
    cwsEditor(750,200);
    </script>-->
    <script type="text/javascript" src="../data/javascript/region.json.js"></script>

<%
	End Function
End Class
%>