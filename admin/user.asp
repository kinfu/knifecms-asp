<!--#include file="config.admin.asp"-->
<%
Dim User
Set User = New Class_KnifeCMS_Admin_User
Set User = Nothing
Class Class_KnifeCMS_Admin_User

	Private Pv_UserID,Pv_Username,Pv_Password,Pv_Email,Pv_BigHead,Pv_HeadImg,Pv_SysUserID,Pv_SysUsername,Pv_Grade,Pv_Points,Pv_RegTime,Pv_LastLoginTime,Pv_Remarks,Pv_Disable,Pv_Recycle
	Private Pv_RealName,Pv_Gender,Pv_HomeTown,Pv_MySite,Pv_Birthday,Pv_Mobile,Pv_Telephone,Pv_QQ,Pv_Height,Pv_Weight,Pv_MaritalStatus,Pv_BloodType,Pv_Religion,Pv_Smoke,Pv_Drink,Pv_FavTravel,Pv_FavLiving,Pv_FavWork,Pv_FavPartner,Pv_FavCar
	Private Pv_KeywordsType,Pv_PointFrom,Pv_PointTo
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "user.lang.asp"
		IsContentExist = False
		Header()
		If Ctl="user" Or Ctl="user_recycle" Then
			Select Case Action
				Case "view"
					If SubAction="detail" Then
						Call GetUserDetailData()
						If IsContentExist Then UserDetail : Else Admin.ShowMessage Lang_User_Cue(0),"goback",0
					Else
						Call UserGetSearchCondition()
						Call UserView()
					End If
					
				Case "add","edit"
					If SubAction="save" Then
						Call GetUserDetailData()
						If IsContentExist Then UserDoSave : Else Admin.ShowMessage Lang_User_Cue(0),"goback",0
					Else
						If Action = "add" Then
							Call UserDo()
						ElseIf Action = "edit" Then
							If SubAction = "revert" Or SubAction = "forbid" Or SubAction = "enabled" Then
								Call UserOtherEdit()
							Else
								Call GetUserDetailData()
								If IsContentExist Then UserDo : Else Admin.ShowMessage Lang_User_Cue(0),"goback",0
							End If
						End IF
					End IF
				Case "del"  : UserDel
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"?ctl="& Ctl &"&act=view",1
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function  GetUserDetailData()
		Set Rs = KnifeCMS.DB.GetRecord(DBTable_Members,Array("ID:"&ID),"")
		If Not(Rs.Eof) Then
			IsContentExist = True
			Pv_UserID     = Rs("ID")
			Pv_Username   = Rs("Username")
			Pv_Password   = Rs("Password")
			Pv_Email      = Rs("Email")
			Pv_BigHead    = KnifeCMS.Data.URLDecode(Rs("BigHead"))
			Pv_HeadImg    = KnifeCMS.Data.URLDecode(Rs("HeadImg"))
			Pv_SysUserID  = Rs("SysUserID")
			Pv_Grade      = Rs("Grade")
			Pv_Points     = Rs("Points")
			Pv_RegTime    = Rs("RegTime")
			Pv_LastLoginTime   = Rs("LastLoginTime")
			Pv_Remarks    = Rs("Remarks")
			Pv_Disable    = Rs("Disable")
			Pv_Recycle    = Rs("Recycle")
		Else
			IsContentExist = False
		End IF
		KnifeCMS.DB.CloseRs Rs
		If IsContentExist Then
			Set Rs = KnifeCMS.DB.GetRecord(DBTable_Members_Info,Array("UserID:"& Pv_UserID),"")
			If Not(Rs.Eof) Then
				Pv_RealName   = Rs("RealName")
				Pv_Gender     = Rs("Gender")
				Pv_HomeTown   = Rs("HomeTown")
				Pv_MySite     = Rs("MySite")
				Pv_Birthday   = Rs("Birthday")
				Pv_Mobile     = Rs("Mobile")
				Pv_Telephone  = Rs("Telephone")
				Pv_QQ         = Rs("QQ")
				Pv_Height     = Rs("Height")
				Pv_Weight     = Rs("Weight")
				Pv_MaritalStatus = Rs("MaritalStatus")
				Pv_BloodType  = Rs("BloodType")
				Pv_Religion   = Rs("Religion")
				Pv_Smoke      = Rs("Smoke")
				Pv_Drink      = Rs("Drink")
				Pv_FavTravel  = Rs("FavTravel")
				Pv_FavLiving  = Rs("FavLiving")
				Pv_FavWork    = Rs("FavWork")
				Pv_FavPartner = Rs("FavPartner")
				Pv_FavCar     = Rs("FavCar")
			End IF
			KnifeCMS.DB.CloseRs Rs
		End If
	End Function
	
	Private Function UserGetSearchCondition()
		Pv_PointFrom = KnifeCMS.GetForm("both","PointFrom") : If Not(KnifeCMS.Data.IsNul(Pv_PointFrom)) Then Pv_PointFrom = KnifeCMS.Data.CLng(Pv_PointFrom)
		Pv_PointTo   = KnifeCMS.GetForm("both","PointTo")   : If Not(KnifeCMS.Data.IsNul(Pv_PointTo))   Then Pv_PointTo   = KnifeCMS.Data.CLng(Pv_PointTo)
		Pv_KeywordsType   = KnifeCMS.GetForm("both","keywordstype")
	End Function
	Private Function UserSearchSql()
		Dim Fn_TempSql
		Pv_PointFrom = KnifeCMS.Data.CLng(Pv_PointFrom)
		Pv_PointTo   = KnifeCMS.Data.CLng(Pv_PointTo)
		Fn_TempSql  = Fn_TempSql & KnifeCMS.IIF(Pv_PointFrom > 0 ," AND isnull(a.Points,0)>="& Pv_PointFrom &" "," ")
		Fn_TempSql  = Fn_TempSql & KnifeCMS.IIF(Pv_PointTo   > 0 ," AND isnull(a.Points,0)<="& Pv_PointTo &" "  ," ")
		Select Case Pv_KeywordsType
			Case "username"
				Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords),50)
				Fn_TempSql = Fn_TempSql & " AND a.Username LIKE '%"& Admin.Search.KeyWords &"%'"
			Case "email"
				Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords),50)
				Fn_TempSql = Fn_TempSql & " AND a.Email LIKE '%"& Admin.Search.KeyWords &"%'"
		End Select
		UserSearchSql = Fn_TempSql
	End Function
	
	Private Function UserList()
		Dim Fn_TempSql,Fn_ColumnNum
			Fn_TempSql = "SELECT a.ID,a.Username,a.BigHead,a.HeadImg,c.RealName,c.Gender,a.Email,a.Points,c.Mobile,a.Disable,a.LoginTimes,a.LastLoginTime,a.IP FROM ["& DBTable_Members &"] a LEFT JOIN ["& DBTable_Members_Info &"] c ON (c.UserID=a.ID)"
			Fn_ColumnNum = 13
			Operation = Operation & "<span class=""sysBtn""><a href=""javascript:;"" onClick=""ShowModal('?ctl="& Ctl &"&act=view&subact=detail&id={$ID}',800,600);""><b class=""icon icon_view""></b>"& Lang_DoView &"</a></span><span class=""sysBtn""><a href=""javascript:;"" onClick=""ShowModal('?ctl="& Ctl &"&act=edit&subact=detail&id={$ID}',800,600);ShowModalReload();""><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span><br/><span class=""sysBtn""><a href=""order.asp?ctl=order&act=view&subact=search&keywordstype=memberid&keywords={$ID}""><b class=""icon icon_view""></b>"& Lang_User_ListTableColLine(12) &"</a></span><span class=""sysBtn""><a href=""comment.asp?ctl=goods_consult&act=view&subact=search&userid={$ID}""><b class=""icon icon_view""></b>"& Lang_User_ListTableColLine(13) &"</a></span><span class=""sysBtn""><a href=""comment.asp?ctl=goods_comment&act=view&subact=search&userid={$ID}""><b class=""icon icon_view""></b>"& Lang_User_ListTableColLine(14) &"</a></span>"
		If SubCtl = "recycle" Then
			Fn_TempSql = Fn_TempSql & " WHERE a.Recycle=1 ORDER BY a.ID DESC"
			Operation = ""
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,"")
		Else
			Fn_TempSql = Fn_TempSql & " WHERE a.Recycle=0 "
			If SubAction = "search" Then
			
				Select Case DB_Type
				Case 0
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.RegTime>=#"& Admin.Search.StartDate &"# ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.RegTime<=#"& Admin.Search.EndDate &"# ","")
				Case 1
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.RegTime>='"& Admin.Search.StartDate &"' ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.RegTime<='"& Admin.Search.EndDate &"' ","")
				End Select
				  
				tempCondition = tempCondition & UserSearchSql
				PageUrlPara = PageUrlPara & "&startdate="& Server.URLEncode(Admin.Search.StartDate) &"&enddate="& Server.URLEncode(Admin.Search.EndDate) &"&keywords="& Server.URLEncode(Admin.Search.KeyWords)&"&keywordstype="& Pv_KeywordsType &"&PointFrom="& Pv_PointFrom &"&PointTo="& Pv_PointTo &""
				Fn_TempSql = Fn_TempSql & tempCondition
			End If
			Fn_TempSql = Fn_TempSql &" ORDER BY a.ID DESC"
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
		End If
	End Function
	'在调用此方法前已经进行过检测过会员是否存在,存在的情况下才能调用此方法
	Private Function UserDoSave()
		Dim Fn_Temp,Fn_Result
		Pv_Email      = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","email")),50)
		Pv_RealName   = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","realname")),50)
		Pv_Gender     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","gender"))
		for each Fn_Temp in Request.Form("myhometown")
			Pv_HomeTown = trim(Fn_Temp)
		next
		Pv_HomeTown   = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Pv_HomeTown,"[^0-9,]",""),50)
		for each Fn_Temp in Request.Form("mysite")
			Pv_MySite = trim(Fn_Temp)
		next
		Pv_MySite     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Pv_MySite,"[^0-9,]",""),50)
		Pv_Birthday   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","birthday_year")) &"-"& KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","birthday_month")) &"-"& KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","birthday_day"))
		
		Pv_Mobile     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","mobile"),"[^0-9,]",""),50)
		Pv_Telephone  = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","telephone"),"[^0-9\-,]",""),50)
		Pv_QQ         = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","qq"),"[^0-9,]",""),50)
		Pv_Height     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","height"))
		Pv_Weight     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","weight"))
		Pv_MaritalStatus = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","maritalstatus"))
		Pv_BloodType  = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","bloodtype"),"[^a-zA-Z]",""),5)
		Pv_Religion   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","religion"))
		Pv_Smoke      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","smoke"))
		Pv_Drink      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","drink"))
		Pv_FavTravel  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","favtravel")),250)
		Pv_FavLiving  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","favliving")),250)
		Pv_FavWork    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","favwork")),250)
		Pv_FavPartner = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","favpartner")),250)
		Pv_FavCar     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","favcar")),250)
		If KnifeCMS.Data.IsNul(Pv_Email) Then
			ErrMsg = Lang_User_Cue(9)
		Else
			If KnifeCMS.Data.IsEmail(Pv_Email) Then
				If KnifeCMS.DB.CheckIsRecordExist(DBTable_Members,Array("Email:"& Pv_Email)," AND ID<>"& ID &" ") Then
					ErrMsg = Lang_User_Cue(11)
				End If
			Else
				ErrMsg = Lang_User_Cue(10)
			End If
		End If
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act="& Action &"&id="& ID &"",0 : Exit Function
		Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("Email:"& Pv_Email),Array("ID:"& ID))
		If Fn_Result Then
			If Not(KnifeCMS.DB.CheckIsRecordExistByField(DBTable_Members_Info,Array("UserID:"& ID))) Then
				Fn_Result = KnifeCMS.DB.AddRecord(DBTable_Members_Info,Array("UserID:"& ID,"RealName:"& Pv_RealName,"Gender:"& Pv_Gender,"HomeTown:"& Pv_HomeTown,"MySite:"& Pv_MySite,"Birthday:"& Pv_Birthday,"Mobile:"& Pv_Mobile,"Telephone:"& Pv_Telephone,"QQ:"& Pv_QQ,"Height:"& Pv_Height,"Weight:"& Pv_Weight,"MaritalStatus:"& Pv_MaritalStatus,"BloodType:"& Pv_BloodType,"Religion:"& Pv_Religion,"Smoke:"& Pv_Smoke,"Drink:"& Pv_Drink,"FavTravel:"& Pv_FavTravel,"FavLiving:"& Pv_FavLiving,"FavWork:"& Pv_FavWork,"FavPartner:"& Pv_FavPartner,"FavCar:"& Pv_FavCar))
			Else
				Fn_Result = KnifeCMS.DB.UpdateRecord(DBTable_Members_Info,Array("RealName:"& Pv_RealName,"Gender:"& Pv_Gender,"HomeTown:"& Pv_HomeTown,"MySite:"& Pv_MySite,"Birthday:"& Pv_Birthday,"Mobile:"& Pv_Mobile,"Telephone:"& Pv_Telephone,"QQ:"& Pv_QQ,"Height:"& Pv_Height,"Weight:"& Pv_Weight,"MaritalStatus:"& Pv_MaritalStatus,"BloodType:"& Pv_BloodType,"Religion:"& Pv_Religion,"Smoke:"& Pv_Smoke,"Drink:"& Pv_Drink,"FavTravel:"& Pv_FavTravel,"FavLiving:"& Pv_FavLiving,"FavWork:"& Pv_FavWork,"FavPartner:"& Pv_FavPartner,"FavCar:"& Pv_FavCar),Array("UserID:"& ID))
			End If
		End If
		If Fn_Result Then
			Msg = Msg & Lang_User_Cue(8) &"{"& Pv_Username &"[ID="& ID &"]}"
			Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		Else
			Msg = Msg & Lang_User_Cue(12) &"{"& Pv_Username &"[ID="& ID &"]}"
			Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),0
		End If
	End Function
	
	Private Function UserDel()
		Call UserSubDo()
	End Function
	
	Private Function UserOtherEdit()
		 Call UserSubDo()
	End Function
	
	Private Sub UserSubDo()
		ID = KnifeCMS.GetForm("post","InputName")
		Dim Fn_ArrID,Fn_i,Fn_CanDel
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			Result      = 0
			Pv_Username = ""
			If ID > 0 Then
				Pv_SysUserID = 0
				Set Rs = KnifeCMS.DB.GetRecord(DBTable_Members &":Username,SysUserID",Array("ID:"& ID),"")
				If Not(Rs.Eof) Then
					IsContentExist = True
					Pv_Username    = Rs(0)
					Pv_SysUserID   = KnifeCMS.Data.CLng(Rs(1))
				Else
					IsContentExist = False
				End If
				KnifeCMS.DB.CloseRs Rs
				If IsContentExist Then
					'开始执行操作
					If Action = "del" Then
						If SubAction = "completely" Then
							Fn_CanDel = True
							If Pv_SysUserID>0 Then
								Set Rs = KnifeCMS.DB.GetRecord(DBTable_SysUser &":Username",Array("ID:"& Pv_SysUserID),"")
								If Not(Rs.Eof) Then
									Fn_CanDel      = False
									Pv_SysUsername = Rs(0)
								End If
							End If
							If Fn_CanDel Then
								Result = KnifeCMS.DB.DeleteRecord(DBTable_Members,Array("ID:"& ID))
								Result = KnifeCMS.DB.DeleteRecord(DBTable_Members_Info,Array("UserID:"& ID))
								Result = KnifeCMS.DB.DeleteRecord(DBTable_Members_Addrs,Array("UserID:"& ID))
								Result = KnifeCMS.DB.DeleteRecord(DBTable_Members_Feeds,Array("MemberID:"& ID))
								Result = KnifeCMS.DB.DeleteRecord(DBTable_Members_Feeds,Array("UserID:"& ID))
								Result = KnifeCMS.DB.DeleteRecord(DBTable_Members_GuestBook,Array("MemberID:"& ID))
								Result = KnifeCMS.DB.DeleteRecord(DBTable_Members_GuestBook,Array("UserID:"& ID))
								Result = KnifeCMS.DB.DeleteRecord(DBTable_Friend,Array("UserID:"& ID))
								Result = KnifeCMS.DB.DeleteRecord(DBTable_Friend,Array("FriendID:"& ID))
								Result = KnifeCMS.DB.DeleteRecord(DBTable_FriendGroup,Array("UserID:"& ID))
								Msg    = Msg & Lang_User_Cue(4) &"{"& Pv_Username &"[ID="& ID &"]}<br>"
							Else
								Msg    = Msg & KnifeCMS.Data.ReplaceString(KnifeCMS.Data.ReplaceString(Lang_User_Cue(14),"{$:sysusername}",Pv_SysUsername),"{$:username}",Pv_Username) &"{"& Pv_Username &"[ID="& ID &"]}<br>"
							End If
						Else
							Result = KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("Recycle:1"),Array("ID:"& ID))
							Msg    = Msg & Lang_User_Cue(3) &"{"& Pv_Username &"[ID="& ID &"]}<br>"
						End If
					ElseIf Action = "edit" Then
						If SubAction = "forbid" Then
							Result = KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("Disable:1"),Array("ID:"& ID))
							Msg    = Msg & Lang_User_Cue(6) &"{"& Pv_Username &"[ID="& ID &"]}<br>"
						ElseIf SubAction = "enabled" Then
							Result = KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("Disable:0"),Array("ID:"& ID))
							Msg    = Msg & Lang_User_Cue(7) &"{"& Pv_Username &"[ID="& ID &"]}<br>"
						ElseIf SubAction = "revert" Then
							Result = KnifeCMS.DB.UpdateRecord(DBTable_Members,Array("Recycle:0"),Array("ID:"& ID))
							Msg    = Msg & Lang_User_Cue(5) &"{"& Pv_Username &"[ID="& ID &"]}<br>"
						End If
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Sub
	
	Private Function UserView()
%>
	<%=Admin.Calendar(Array("","",""))%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table bUser="0" cellpadding="0" cellspacing="0">
		  <tr>
          <% If SubCtl = "recycle" Then %>
          <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:revert','')"><b class="icon icon_revert"></b><% Echo Lang_DoRevert %></button>
				  </td></tr>
			  </table>
		  </td>
          <td class="tdcell">
			  <table bUser="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
          <% Else %>
          <td class="tdcell">
              <table bUser="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','')"><b class="icon icon_recycle"></b><% Echo Lang_DoDel%></button>
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
          <td class="tdcell">
              <table border="0" cellpadding="2" cellspacing="0">
              <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
              <tr><td class="downcell">
                  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:forbid','<% Echo Lang_DoForbidUserConfirm %>')"><b class="icon icon_userForbid"></b><% Echo Lang_DoForbid %></button>
                  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:enabled','<% Echo Lang_DoEnabledUserConfirm %>')"><b class="icon icon_userEnabled"></b><% Echo Lang_DoEnabled %></button>
                  </td></tr>
              </table>
          </td>
          <td class="tdcell">
            <table border="0" cellpadding="2" cellspacing="0">
            <tr><td class="upcell">
                <div class="align-center grey">
                    <% Echo Lang_DistrictDoSearch %><!--搜索操作区-->
                    <span class="AdvancedSearch"><a href="javascript:Admin.AdvancedSearch.Open(600,324,'AdvancedSearch')" ><% Echo Lang_AdvancedSearch %></a></span>
                </div>
                <DIV id="AdvancedSearch" style="display:none;">
                    <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                    <div class="diainbox">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                      <tr><td class="titletd"><% Echo Lang_Keywords %>:</td>
                          <td class="infotd">
                          <select name="keywordstype">
						  <% Echo OS.CreateOption(Array("username:"& Lang_User_ListTableColLine(1),"email:"& Lang_User_ListTableColLine(5)),Split(Pv_KeywordsType,",")) %>
                          </select>
                          <input type="text" class="TxtClass" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:306px;" />
                          </td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_User_ListTableColLine(6) %><!--积分-->:</td>
                    <td class="infotd"><% Echo Lang_From %><!--从--> <input type="text" id="PointFrom" name="PointFrom" class="TxtClass" style="width:51px;" value="<% Echo Pv_PointFrom %>" /> <% Echo Lang_FromTo %><!--到--> <input type="text" id="PointTo" name="PointTo" class="TxtClass" style="width:51px;" value="<% Echo Pv_PointTo %>" /></td></tr>
                      <tr><td class="titletd"><% Echo Lang_StartDate %>:</td>
                          <td class="infotd"><input type="text" class="TxtSer dateStart" id="startdate" name="startdate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' value="<%=Admin.Search.StartDate%>" style="width:200px;" /></td>
                      <tr><td class="titletd"><% Echo Lang_EndDate %>:</td>
                          <td class="infotd"><input type="text" class="TxtSer dateEnd" id="enddate" name="enddate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' value="<%=Admin.Search.EndDate%>" style="width:200px;" /></td>
                      </tr>
                      </table>
                      </div>
                    <% Echo AdvancedSearchBtnline %>
                    </form>
                </DIV>
                </td></tr>
            <tr><td class="downcell">
                <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                <table border="0" cellpadding="0" cellspacing="0"><tr>
                <td><span><% Echo Lang_Keywords %>:</span></td>
                <td class="pl3">
                <select name="keywordstype">
                <% Echo OS.CreateOption(Array("username:"& Lang_User_ListTableColLine(1),"email:"& Lang_User_ListTableColLine(5)),Split(Pv_KeywordsType,",")) %>
                </select>
                </td>
                <td class="pl3"><input type="text" class="TxtClass" name="keywords" id="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:150px;" /></td>
                <td class="pl3"><button type="submit" class="sysBtn" ><b class="icon icon_search"></b><% Echo Lang_Btn_Search %></button></td>
                </tr></table>
                </form>
            </td></tr>
            </table>
          </td>
          <% End If %>
          <td class="tdcell"><div style="width:180px;"><span class="illustration"><b><% Echo Lang_SystemCue %>:</b><% Echo Lang_User_Cue(13) %></span></div></td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table bUser="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
            <th><div><% Echo Lang_User_ListTableColLine(0) %><!--会员ID--></div></th>
			<th><div><% Echo Lang_User_ListTableColLine(1) %><!--用户名--></div></th>
            <th><div><% Echo Lang_User_ListTableColLine(18)%><!--大头像--></div></th>
            <th><div><% Echo Lang_User_ListTableColLine(15)%><!--小头像--></div></th>
            <th><div><% Echo Lang_User_ListTableColLine(3) %><!--姓名--></div></th>
            <th><div><% Echo Lang_User_ListTableColLine(4) %><!--性别--></div></th>
            <th><div><% Echo Lang_User_ListTableColLine(5) %><!--Email--></div></th>
            <th><div><% Echo Lang_User_ListTableColLine(6) %><!--积分--></div></th>
            <th><div><% Echo Lang_User_ListTableColLine(7) %><!--手机号码--></div></th>
            <th><div><% Echo Lang_User_ListTableColLine(11)%><!--状态--></div></th>
            <th><div><% Echo Lang_User_ListTableColLine(16) %><!--登录次数--></div></th>
            <th><div><% Echo Lang_User_ListTableColLine(9) %><!--上次登录时间--></div></th>
            <th><div><% Echo Lang_User_ListTableColLine(17) %><!--上次登录IP--></div></th>
			<th><div><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% UserList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
    <script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	if(objExist(document.getElementById("keywords"))){ReplaceText(document.getElementById("keywords").value,1)}
	function ChangeTdText(){
		var tds=document.getElementById("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			var IsEdit = true;
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td0"){
					TempHTML = '<a href="javascript:;" onClick="ShowModal(\'?ctl=<%=Ctl%>&act=edit&subact=detail&id='+tds[i].innerHTML+'\',800,600);ShowModalReload();" title="<%=Lang_DoEdit%>">'+ tds[i].innerHTML +'</a>';
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td1"){
					TempHTML = '<a href="javascript:;" onClick="ShowModal(\'?ctl=<%=Ctl%>&act=view&subact=detail&id='+tds[i-1].title+'\',800,600);ShowModalReload();" title="<%=Lang_DoView%>">'+ unescape(tds[i].innerHTML) +'</a>';
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td2" && tds[i].innerHTML!=""){
					TempHTML = '<span class="sImg"><a href="../?user/'+ tds[i-2].title +'<%=FileSuffix%>" title="<%=Lang_DoView%>" target=_blank><img src='+ unescape(tds[i].innerHTML) +' /></a></span>';
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td3" && tds[i].innerHTML!=""){
					TempHTML = '<span class="sImg"><a href="../?user/'+ tds[i-3].title +'<%=FileSuffix%>" title="<%=Lang_DoView%>" target=_blank><img src='+ unescape(tds[i].innerHTML) +' /></a></span>';
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td5"){
					if(parseInt(tds[i].innerHTML)==1){
						TempHTML="<%=Lang_Male %>";
					}else if(parseInt(tds[i].innerHTML)==2){
						TempHTML="<%=Lang_Female %>";
					}else{
						TempHTML="<%=Lang_Secret %>";
					}
					tds[i].innerHTML = TempHTML;
				}
				if(tds[i].id=="td6"){
					if(KnifeCMS.IsNull(tds[i].innerHTML)){
						tds[i].innerHTML = "0";
					}
				}
				if(tds[i].id=="td9"){
					if(parseInt(tds[i].innerHTML)==1){
						tds[i].innerHTML='<b class=\"icon icon_userForbid\" title=\"<% Echo Lang_DoForbid %>\"></b>';
					}else{
						tds[i].innerHTML='<b class=\"icon icon_userEnabled\" title=\"<% Echo Lang_Normal %>\"></b>';
					}
				}
				
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
	Private Function UserDetail()
%>
    <script language="javascript" src="../data/javascript/region.json.js"></script>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
          <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
          <div class="obox">
            <div class="tit"><% Echo Lang_User(1) %><!--会员信息--></div>
            <div class="inbox">
              <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
              <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                  <tr><td class="titletd"><% Echo Lang_User(2) %><!--用户名-->:</td><td class="infotd"><% Echo Pv_Username %></td>
                  </tr>
                  <tr><td class="titletd"><% Echo Lang_User(3) %><!--Email-->:</td><td class="infotd"><% Echo Pv_Email %></td>
                  </tr>
                  <tr><td class="titletd"></td><td class="infotd"></td>
                  </tr>
                </table>
                
              </td>
              <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                  <tr><td class="titletd"><% Echo Lang_User(4) %><!--状态-->:</td><td class="infotd"><% Echo KnifeCMS.IIF(Pv_Disable=1,Lang_DoForbid,Lang_Normal) %></td>
                  </tr>
                  <tr><td class="titletd"><% Echo Lang_User(5) %><!--注册时间-->:</td><td class="infotd"><% Echo Pv_RegTime %></td>
                  </tr>
                  <tr><td class="titletd"><% Echo Lang_User(6) %><!--上次登录时间-->:</td><td class="infotd"><% Echo Pv_LastLoginTime %></td>
                  </tr>
                </table>
              </td>
              </tr></table>
              
              
            </div>
            <div class="inbox">
              <div class="tit"><% Echo Lang_User(7) %><!--基本信息--></div>
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><% Echo Lang_User(8) %><!--真实姓名-->:</td><td class="infotd"><% Echo Pv_RealName %></td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(9) %><!--性 别-->:</td><td class="infotd"><select name="gender"><% Echo OS.CreateOption(Array("0:"& Lang_Gender(0) &"","1:"& Lang_Gender(1) &"","2:"& Lang_Gender(2) &""),Split(Pv_Gender,",")) %></select></td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(10) %><!--生 日-->:</td><td class="infotd">
                    <span id="mybirthday"></span>
                    <script type="text/javascript">
                    CreateDate(KnifeCMS.$id("mybirthday"),"<% Echo Pv_Birthday %>","birthday");
                    </script>
                    <label class="Normal" id="d_birthday"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(11) %><!--家 乡-->:</td><td class="infotd">
                    <span id="address_ht"></span>
                    <script type="text/javascript">
					CreateAddressAsSelect(KnifeCMS.$id("address_ht"),",","<% Echo Pv_HomeTown %>","hometown",false);
					</script>
                    <label class="Normal" id="d_myhometown"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(12) %><!--目前所在地-->:</td><td class="infotd">
                    <span id="address_mysite"></span>
					<script type="text/javascript">
                    CreateAddressAsSelect(KnifeCMS.$id("address_mysite"),",","<% Echo Pv_MySite %>","mysite",false);
                    </script>
                    <label class="Normal" id="d_mysite"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(13) %><!--婚姻状态-->:</td><td class="infotd">
				    <span id="mymaritalstatus"></span>
					<script type="text/javascript">
                    CreateSelect(KnifeCMS.$id("mymaritalstatus"),"name:maritalstatus;id:maritalstatus","0:<% Echo Lang_MaritalStatus(0) %>;1:<% Echo Lang_MaritalStatus(1) %>;2:<% Echo Lang_MaritalStatus(1) %>","<% Echo Pv_MaritalStatus %>");
                    </script>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(14) %><!--身 高-->:</td><td class="infotd"><% Echo Pv_Height %> cm</td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(15) %><!--体 重-->:</td><td class="infotd"><% Echo Pv_Weight %> kg</td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(16) %><!--血 型-->:</td><td class="infotd">
                    <span id="mybloodtype"></span>
					<script type="text/javascript">
                    CreateSelect(KnifeCMS.$id("mybloodtype"),"name:bloodtype;id:bloodtype",":<% Echo Lang_PleaseSelect %>;A:<% Echo Lang_BloodType(0) %>;B:<% Echo Lang_BloodType(1) %>;O:<% Echo Lang_BloodType(2) %>;AB:<% Echo Lang_BloodType(3) %>;other:<% Echo Lang_Other %>","<% Echo Pv_BloodType %>");
                    </script>
                    <label class="Normal" id="d_bloodtype"></label>
                    </td>
                </tr>
              </table>
            </div>
            <div class="inbox">
              <div class="tit"><% Echo Lang_User(17) %><!--联系方式--></div>
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><% Echo Lang_User(18) %><!--QQ号码-->:</td><td class="infotd"><% Echo Pv_QQ %></td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(19) %><!--手机号码-->:</td><td class="infotd"><% Echo Pv_Mobile %></td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(20) %><!--联系电话-->:</td><td class="infotd"><% Echo Pv_Telephone %></td>
                </tr>
              </table>
            </div>
            <div class="inbox">
              <div class="tit"><% Echo Lang_User(21) %><!--其他信息--></div>
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><% Echo Lang_User(22) %><!--吸 烟-->:</td><td class="infotd">
                    <span id="mysmoke"></span>
					<script type="text/javascript">
                    CreateSelect(KnifeCMS.$id("mysmoke"),"name:smoke;id:smoke","0:<% Echo Lang_PleaseSelect %>;1:<% Echo Lang_Smoke(0) %>;2:<% Echo Lang_Smoke(1) %>;3:<% Echo Lang_Smoke(2) %>;4:<% Echo Lang_Smoke(3) %>","<% Echo Pv_Smoke %>");
                    </script>
                    <label class="Normal" id="d_smoke"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(23) %><!--喝 酒-->:</td><td class="infotd">
                    <span id="mydrink"></span>
					<script type="text/javascript">
                    CreateSelect(KnifeCMS.$id("mydrink"),"name:drink;id:drink","0:<% Echo Lang_PleaseSelect %>;1:<% Echo Lang_Drink(0) %>;2:<% Echo Lang_Drink(1) %>;3:<% Echo Lang_Drink(2) %>;4:<% Echo Lang_Drink(3) %>","<% Echo Pv_Drink %>");
                    </script>
                    <label class="Normal" id="d_drink"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(24) %><!--宗教信仰-->:</td><td class="infotd">
				    <span id="myreligion"></span>
					<script type="text/javascript">
                    CreateSelect(KnifeCMS.$id("myreligion"),"name:religion;id:religion","0:<% Echo Lang_PleaseSelect %>;1:<% Echo Lang_Religion(0) %>;2:<% Echo Lang_Religion(1) %>;3:<% Echo Lang_Religion(2) %>;4:<% Echo Lang_Religion(3) %>;5:<% Echo Lang_Religion(4) %>;6:<% Echo Lang_Religion(5) %>;7:<% Echo Lang_Other  %>","<% Echo Pv_Religion %>");
                    </script>
                    <label class="Normal" id="d_religion"></label>
                    </td>
                </tr>
              </table>
            </div>
            <div class="inbox">
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><% Echo Lang_User(25) %><!--梦想的旅游地-->:</td><td class="infotd">
                    <% Echo Pv_FavTravel %>
                    <label class="Normal" id="d_favtravel"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(26) %><!--梦想的居住地-->:</td><td class="infotd">
                    <% Echo Pv_FavLiving %>
                    <label class="Normal" id="d_favliving"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(27) %><!--梦想的工作-->:</td><td class="infotd">
				    <% Echo Pv_FavWork %>
                    <label class="Normal" id="d_favwork"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(28) %><!--梦想的伴侣-->:</td><td class="infotd">
				    <% Echo Pv_FavPartner %>
                    <label class="Normal" id="d_favpartner"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(29) %><!--梦想的汽车-->:</td><td class="infotd">
				    <% Echo Pv_FavCar %>
                    <label class="Normal" id="d_favcar"></label>
                    </td>
                </tr>
              </table>
            </div>
          </div>
          </form>
      </DIV>
    </DIV>
<%
	End Function
	Private Function UserDo()
%>
    <script language="javascript" src="../data/javascript/region.json.js"></script>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
          <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
          <div class="obox">
            <div class="tit"><% Echo Lang_User(1) %><!--会员信息--></div>
            <div class="inbox">
              <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
              <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                  <tr><td class="titletd"><% Echo Lang_User(2) %><!--用户名-->:</td><td class="infotd"><% Echo Pv_Username %></td>
                  </tr>
                  <tr><td class="titletd"><% Echo Lang_User(3) %><!--Email-->:</td><td class="infotd"><input type="text" class="TxtClass" id="email" name="email" value="<% Echo Pv_Email %>" /></td>
                  </tr>
                  <tr><td class="titletd"></td><td class="infotd"><button type="button" class="sysBtn" onClick="Admin.User.EditPassword('<% Echo ID %>')"><b class="icon icon_edit"></b><% Echo Lang_DoEditPassword %></button></td>
                  </tr>
                </table>
                
              </td>
              <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                  <tr><td class="titletd"><% Echo Lang_User(4) %><!--状态-->:</td><td class="infotd"><% Echo KnifeCMS.IIF(Pv_Disable=1,Lang_DoForbid,Lang_Normal) %></td>
                  </tr>
                  <tr><td class="titletd"><% Echo Lang_User(5) %><!--注册时间-->:</td><td class="infotd"><% Echo Pv_RegTime %></td>
                  </tr>
                  <tr><td class="titletd"><% Echo Lang_User(6) %><!--上次登录时间-->:</td><td class="infotd"><% Echo Pv_LastLoginTime %></td>
                  </tr>
                </table>
              </td>
              </tr></table>
              
              
            </div>
            <div class="inbox">
              <div class="tit"><% Echo Lang_User(7) %><!--基本信息--></div>
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><% Echo Lang_User(8) %><!--真实姓名-->:</td><td class="infotd"><input type="text" class="TxtClass" id="realname" name="realname" value="<% Echo Pv_RealName %>" /></td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(9) %><!--性 别-->:</td><td class="infotd"><select name="gender"><% Echo OS.CreateOption(Array("0:"& Lang_Gender(0) &"","1:"& Lang_Gender(1) &"","2:"& Lang_Gender(2) &""),Split(Pv_Gender,",")) %></select></td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(10) %><!--生 日-->:</td><td class="infotd">
                    <span id="mybirthday"></span>
                    <script type="text/javascript">
                    CreateDate(KnifeCMS.$id("mybirthday"),"<% Echo Pv_Birthday %>","birthday");
                    </script>
                    <label class="Normal" id="d_birthday"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(11) %><!--家 乡-->:</td><td class="infotd">
                    <span id="address_ht"></span>
                    <script type="text/javascript">
					CreateAddressAsSelect(KnifeCMS.$id("address_ht"),",","<% Echo Pv_HomeTown %>","hometown",false);
					</script>
                    <label class="Normal" id="d_myhometown"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(12) %><!--目前所在地-->:</td><td class="infotd">
                    <span id="address_mysite"></span>
					<script type="text/javascript">
                    CreateAddressAsSelect(KnifeCMS.$id("address_mysite"),",","<% Echo Pv_MySite %>","mysite",false);
                    </script>
                    <label class="Normal" id="d_mysite"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(13) %><!--婚姻状态-->:</td><td class="infotd">
				    <span id="mymaritalstatus"></span>
					<script type="text/javascript">
                    CreateSelect(KnifeCMS.$id("mymaritalstatus"),"name:maritalstatus;id:maritalstatus","0:<% Echo Lang_MaritalStatus(0) %>;1:<% Echo Lang_MaritalStatus(1) %>;2:<% Echo Lang_MaritalStatus(1) %>","<% Echo Pv_MaritalStatus %>");
                    </script>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(14) %><!--身 高-->:</td><td class="infotd"><input type="text" class="TxtClass" name="height" id="height" value="<% Echo Pv_Height %>" style="width:60px;" /> cm</td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(15) %><!--体 重-->:</td><td class="infotd"><input type="text" class="TxtClass" name="weight" id="weight" value="<% Echo Pv_Weight %>" style="width:60px;" /> kg</td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(16) %><!--血 型-->:</td><td class="infotd">
                    <span id="mybloodtype"></span>
					<script type="text/javascript">
                    CreateSelect(KnifeCMS.$id("mybloodtype"),"name:bloodtype;id:bloodtype",":<% Echo Lang_PleaseSelect %>;A:<% Echo Lang_BloodType(0) %>;B:<% Echo Lang_BloodType(1) %>;O:<% Echo Lang_BloodType(2) %>;AB:<% Echo Lang_BloodType(3) %>;other:<% Echo Lang_Other %>","<% Echo Pv_BloodType %>");
                    </script>
                    <label class="Normal" id="d_bloodtype"></label>
                    </td>
                </tr>
              </table>
            </div>
            <div class="inbox">
              <div class="tit"><% Echo Lang_User(17) %><!--联系方式--></div>
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><% Echo Lang_User(18) %><!--QQ号码-->:</td><td class="infotd"><input type="text" class="TxtClass" name="qq" id="qq" value="<% Echo Pv_QQ %>" style="width:260px;" /></td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(19) %><!--手机号码-->:</td><td class="infotd"><input type="text" class="TxtClass" name="mobile" id="mobile" value="<% Echo Pv_Mobile %>" style="width:260px;" /></td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(20) %><!--联系电话-->:</td><td class="infotd"><input type="text" class="TxtClass" name="telephone" id="telephone" value="<% Echo Pv_Telephone %>" style="width:260px;" /></td>
                </tr>
              </table>
            </div>
            <div class="inbox">
              <div class="tit"><% Echo Lang_User(21) %><!--其他信息--></div>
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><% Echo Lang_User(22) %><!--吸 烟-->:</td><td class="infotd">
                    <span id="mysmoke"></span>
					<script type="text/javascript">
                    CreateSelect(KnifeCMS.$id("mysmoke"),"name:smoke;id:smoke","0:<% Echo Lang_PleaseSelect %>;1:<% Echo Lang_Smoke(0) %>;2:<% Echo Lang_Smoke(1) %>;3:<% Echo Lang_Smoke(2) %>;4:<% Echo Lang_Smoke(3) %>","<% Echo Pv_Smoke %>");
                    </script>
                    <label class="Normal" id="d_smoke"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(23) %><!--喝 酒-->:</td><td class="infotd">
                    <span id="mydrink"></span>
					<script type="text/javascript">
                    CreateSelect(KnifeCMS.$id("mydrink"),"name:drink;id:drink","0:<% Echo Lang_PleaseSelect %>;1:<% Echo Lang_Drink(0) %>;2:<% Echo Lang_Drink(1) %>;3:<% Echo Lang_Drink(2) %>;4:<% Echo Lang_Drink(3) %>","<% Echo Pv_Drink %>");
                    </script>
                    <label class="Normal" id="d_drink"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(24) %><!--宗教信仰-->:</td><td class="infotd">
				    <span id="myreligion"></span>
					<script type="text/javascript">
                    CreateSelect(KnifeCMS.$id("myreligion"),"name:religion;id:religion","0:<% Echo Lang_PleaseSelect %>;1:<% Echo Lang_Religion(0) %>;2:<% Echo Lang_Religion(1) %>;3:<% Echo Lang_Religion(2) %>;4:<% Echo Lang_Religion(3) %>;5:<% Echo Lang_Religion(4) %>;6:<% Echo Lang_Religion(5) %>;7:<% Echo Lang_Other  %>","<% Echo Pv_Religion %>");
                    </script>
                    <label class="Normal" id="d_religion"></label>
                    </td>
                </tr>
              </table>
            </div>
            <div class="inbox">
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><% Echo Lang_User(25) %><!--梦想的旅游地-->:</td><td class="infotd">
                    <input type="text" class="TxtClass" name="favtravel" id="favtravel" value="<% Echo Pv_FavTravel %>" style="width:360px;" />
                    <label class="Normal" id="d_favtravel"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(26) %><!--梦想的居住地-->:</td><td class="infotd">
                    <input type="text" class="TxtClass" name="favliving" id="favliving" value="<% Echo Pv_FavLiving %>" style="width:360px;" />
                    <label class="Normal" id="d_favliving"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(27) %><!--梦想的工作-->:</td><td class="infotd">
				    <input type="text" class="TxtClass" name="favwork" id="favwork" value="<% Echo Pv_FavWork %>" style="width:360px;" />
                    <label class="Normal" id="d_favwork"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(28) %><!--梦想的伴侣-->:</td><td class="infotd">
				    <input type="text" class="TxtClass" name="favpartner" id="favpartner" value="<% Echo Pv_FavPartner %>" style="width:360px;" />
                    <label class="Normal" id="d_favpartner"></label>
                    </td>
                </tr>
                <tr><td class="titletd"><% Echo Lang_User(29) %><!--梦想的汽车-->:</td><td class="infotd">
				    <input type="text" class="TxtClass" name="favcar" id="favcar" value="<% Echo Pv_FavCar %>" style="width:360px;" />
                    <label class="Normal" id="d_favcar"></label>
                    </td>
                </tr>
              </table>
            </div>
            
          </div>
          <input type="hidden" name="id" value="<% Echo ID %>" />
          <div class="bottomSaveline" id="SubmitButtoms">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.User.SaveUserCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
          </div>
          </form>
      </DIV>
    </DIV>
<%
	End Function
End Class
%>