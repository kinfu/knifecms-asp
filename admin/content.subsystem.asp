<!--#include file="config.admin.asp"-->
<%
Dim ContentSubSystem
Set ContentSubSystem = New Class_KnifeCMS_Admin_ContentSubSystem
Set ContentSubSystem = Nothing
Class Class_KnifeCMS_Admin_ContentSubSystem

	Private SysDefault,ContentSubSystem,ContentSubSystemName,ContentSubSystemOrders
	Private Pv_IndexTemplate,Pv_CategoryTemplate,Pv_ContentTemplate,Pv_Config
	Private SeoTitle,SeoUrl,MetaKeywords,MetaDescription
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "content.lang.asp"
		Header()
		If Ctl<>"contentsubsystem" Then
			Admin.ShowMessage Lang_IllegalSysPra,"goback",0
		Else
			Select Case Action
				Case "view"
					Call ContentSubSystemView()
					Echo Admin.UCSString()
				Case "add"
					If SubAction="save" Then
						Call ContentSubSystemDoSave()
					Else
						Call ContentSubSystemDo()
					End If
				Case "edit"
					Call GetContentSubSystemDetailData()
					If SubAction="save" Then
						If IsContentExist Then ContentSubSystemDoSave : Else Admin.ShowMessage Lang_SubSystemCue(7),"",0
					Else
						If IsContentExist Then ContentSubSystemDo : Else Admin.ShowMessage Lang_SubSystemCue(7),"",0
					End If
				Case "del"  : ContentSubSystemDel
			End Select
		End If
		Footer()
	End Sub
	
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub

	Private Function GetContentSubSystemDetailData()
		Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT System,SystemName,Orders,Template,CategoryTemplate,ContentTemplate,SeoTitle,SeoUrl,MetaKeywords,MetaDescription FROM ["& TablePre &"ContentSubSystem] WHERE ID="& ID &"")
		If Not(Rs.Eof) Then
			IsContentExist            = true
			ContentSubSystem         = Rs("System")
			ContentSubSystemName     = Rs("SystemName")
			ContentSubSystemOrders   = Rs("Orders")
			
			Pv_IndexTemplate    = Rs("Template")
			Pv_CategoryTemplate = Rs("CategoryTemplate")
			Pv_ContentTemplate  = Rs("ContentTemplate")
			
			SeoTitle         = Rs("SeoTitle")
			SeoUrl           = Rs("SeoUrl")
			MetaKeywords     = Rs("MetaKeywords")
			MetaDescription  = Rs("MetaDescription")
		Else
			IsContentExist = false
		End If
		KnifeCMS.DB.CloseRs Rs
	End Function
	
	Function ContentSubSystemDoSave()
		Dim TempMaxID,TempMaxOrders,TempContentSubSystem,TempTable
		Dim Fn_SQL
		'获取数据并检查数据正确性
		ContentSubSystem         = LCase(KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","ContentSubSystem"),"[^0-9a-zA-Z]",""),20))
		ContentSubSystemName     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ContentSubSystemName")),45)
		ContentSubSystemOrders   = KnifeCMS.Data.Left(KnifeCMS.Data.Int(KnifeCMS.GetForm("post","ContentSubSystemOrders")),4)
		
		'模板文件
		Pv_IndexTemplate    = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","IndexTemplate"),50)
		Pv_CategoryTemplate = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","CategoryTemplate"),50)
		Pv_ContentTemplate  = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","ContentTemplate"),50)
		
		'SEO优化内容
		SeoTitle            = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","SeoTitle")),250)        : If KnifeCMS.Data.IsNul(SeoTitle) Then SeoTitle=ContentSubSystemName
		SeoUrl              = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","SeoUrl")),250)          : If KnifeCMS.Data.IsNul(SeoUrl)   Then SeoUrl="index"
		MetaKeywords        = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","MetaKeywords")),250)
		MetaDescription     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","MetaDescription")),250)
		
		If ContentSubSystem    ="" Then ErrMsg = Lang_SubSystemCue(3)&"<br>"
		If ContentSubSystemName="" Then ErrMsg = ErrMsg & Lang_SubSystemCue(4)&"<br>"
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"content.subsystem.asp?ctl=contentsubsystem&act=add",0 : Exit Function
		'开始保存数据操作
		If Action="add" then
			
			'判断是否已经存在有相同名称的系统
			Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& TablePre &"ContentSubSystem] WHERE System='"& ContentSubSystem &"'")
			If Not(Rs.Eof) Then ErrMsg = Lang_SubSystemCue(10)&"<br>"
			KnifeCMS.DB.CloseRs Rs
			Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& TablePre &"ContentSubSystem] WHERE SystemName='"& ContentSubSystemName &"'")
			If Not(Rs.Eof) Then ErrMsg = Lang_SubSystemCue(11)&"<br>"
			KnifeCMS.DB.CloseRs Rs
			If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"content.subsystem.asp?ctl=contentsubsystem&act=add",0 : Exit Function
			Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT max(ID),max(Orders) FROM ["& TablePre &"SystemCtlAction]")
			If Not(Rs.Eof) Then
				TempMaxID = Rs(0)
				TempMaxOrders = Rs(1)
			End If
			KnifeCMS.DB.CloseRs Rs
			
			If TempMaxID > 0 And  TempMaxOrders > 0 Then
				Select Case DB_Type
				Case 0
					'1.创建主内容表
					TempTable = TablePre & "Content_" & ContentSubSystem
					Fn_SQL = "CREATE TABLE ["&TempTable&"] ("
					Fn_SQL = Fn_SQL & "[ID] integer IDENTITY (1,1) PRIMARY KEY NOT NULL ,"
					Fn_SQL = Fn_SQL & "[SysID] text (32) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassID] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Title] text (250) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[BriefTitle] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[TitleFontColor] text (7) NULL ,"
					Fn_SQL = Fn_SQL & "[TitleFontType] number NULL ,"
					Fn_SQL = Fn_SQL & "[BriefTitleFontColor] text (7) NULL ,"
					Fn_SQL = Fn_SQL & "[BriefTitleFontType] number NULL ,"
					Fn_SQL = Fn_SQL & "[From] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[Author] text (50) NULL ,"
					Fn_SQL = Fn_SQL & "[PublicTime] date NULL ,"
					Fn_SQL = Fn_SQL & "[Thumbnail] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[UseLinkUrl] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[LinkUrl] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[Tags] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[PaginationType] number NULL ,"
					Fn_SQL = Fn_SQL & "[Abstract] memo NULL ,"
					Fn_SQL = Fn_SQL & "[Content] memo NULL ,"
					Fn_SQL = Fn_SQL & "[SlidesFirstImg] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[SlidesImgs] memo NULL ,"
					Fn_SQL = Fn_SQL & "[Hits] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Comments] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[TopLine] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[OnTop] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Hot] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Elite] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[CommentSet] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Openness] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Password] text (50) NULL ,"
					Fn_SQL = Fn_SQL & "[AddTime] date NOT NULL ,"
					Fn_SQL = Fn_SQL & "[EditTime] date NULL ,"
					Fn_SQL = Fn_SQL & "[Status] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Diggs] number NULL ,"
					Fn_SQL = Fn_SQL & "[LinkVotes] memo NULL ,"
					Fn_SQL = Fn_SQL & "[LinkContents] memo NULL ,"
					Fn_SQL = Fn_SQL & "[SeoTitle] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[SeoUrl] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[MetaKeywords] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[MetaDescription] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[Recycle] number NOT NULL"
					Fn_SQL = Fn_SQL & ")"
					Result = KnifeCMS.DB.Execute(Fn_SQL)
					
					'2.创建分类表
					TempTable = TablePre & "Content_" & ContentSubSystem &"_Class"
					Fn_SQL = "CREATE TABLE ["&TempTable&"] ("
					Fn_SQL = Fn_SQL & "[ID] integer IDENTITY (1,1) PRIMARY KEY NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassName] text (100) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassDepth] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ParentClassID] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassOrder] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassChildNum] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassHide] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassType] number NULL ,"
					Fn_SQL = Fn_SQL & "[ClassUrl] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[ClassLogo] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[Icon] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[Pic] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[TemplateSet] number NULL ,"
					Fn_SQL = Fn_SQL & "[CategoryTemplate] text (50) NULL ,"
					Fn_SQL = Fn_SQL & "[ContentTemplate] text (50) NULL ,"
					Fn_SQL = Fn_SQL & "[SeoTitle] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[SeoUrl] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[MetaKeywords] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[MetaDescription] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[Remarks] memo NULL ,"
					Fn_SQL = Fn_SQL & "[Recycle] number NULL"
					Fn_SQL = Fn_SQL & ")"
					If Result Then Result = KnifeCMS.DB.Execute(Fn_SQL)
					
					'3.创建评论表
					TempTable = TablePre & "Content_" & ContentSubSystem &"_Comment"
					Fn_SQL = "CREATE TABLE ["&TempTable&"] ("
					Fn_SQL = Fn_SQL & "[ID] integer IDENTITY (1,1) PRIMARY KEY NOT NULL ,"
					Fn_SQL = Fn_SQL & "[SysID] text (32) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[P_ID] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Path] text (50) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Depth] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ChildNum] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ContentID] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Content] memo NOT NULL ,"
					Fn_SQL = Fn_SQL & "[UserID] number NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Username] text (50) NULL ,"
					Fn_SQL = Fn_SQL & "[HeadImg] text (250) NULL ,"
					Fn_SQL = Fn_SQL & "[AddTime] date NOT NULL ,"
					Fn_SQL = Fn_SQL & "[UpdateTime] date NOT NULL ,"
					Fn_SQL = Fn_SQL & "[IP] text (64) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Disabled] number NOT NULL"
					Fn_SQL = Fn_SQL & ")"
					If Result Then Result = KnifeCMS.DB.Execute(Fn_SQL)
					
				Case 1
					'1.创建主内容表
					TempTable = TablePre & "Content_" & ContentSubSystem
					Fn_SQL = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].["&TempTable&"]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].["&TempTable&"] "
					Fn_SQL = Fn_SQL & "CREATE TABLE [dbo].["&TempTable&"] ("
					Fn_SQL = Fn_SQL & "[ID] [int] IDENTITY (1, 1) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[SysID] [varchar] (32) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassID] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Title] [nvarchar] (250) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[BriefTitle] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[TitleFontColor] [nvarchar] (7) NULL ,"
					Fn_SQL = Fn_SQL & "[TitleFontType] [int] NULL ,"
					Fn_SQL = Fn_SQL & "[BriefTitleFontColor] [nvarchar] (7) NULL ,"
					Fn_SQL = Fn_SQL & "[BriefTitleFontType] [int] NULL ,"
					Fn_SQL = Fn_SQL & "[From] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[Author] [nvarchar] (50) NULL ,"
					Fn_SQL = Fn_SQL & "[PublicTime] [datetime] NULL ,"
					Fn_SQL = Fn_SQL & "[Thumbnail] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[UseLinkUrl] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[LinkUrl] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[Tags] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[PaginationType] [int] NULL ,"
					Fn_SQL = Fn_SQL & "[Abstract] [ntext] NULL ,"
					Fn_SQL = Fn_SQL & "[Content] [ntext] NULL ,"
					Fn_SQL = Fn_SQL & "[SlidesFirstImg] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[SlidesImgs] [ntext] NULL ,"
					Fn_SQL = Fn_SQL & "[Hits] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Comments] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[TopLine] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[OnTop] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Hot] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Elite] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[CommentSet] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Openness] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Password] [nvarchar] (50) NULL ,"
					Fn_SQL = Fn_SQL & "[AddTime] [datetime] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[EditTime] [datetime] NULL ,"
					Fn_SQL = Fn_SQL & "[Status] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Diggs] [int] NULL ,"
					Fn_SQL = Fn_SQL & "[LinkVotes] [ntext] NULL ,"
					Fn_SQL = Fn_SQL & "[LinkContents] [ntext] NULL ,"
					Fn_SQL = Fn_SQL & "[SeoTitle] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[SeoUrl] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[MetaKeywords] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[MetaDescription] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[Recycle] [int] NOT NULL"
					Fn_SQL = Fn_SQL & ")"
					Fn_SQL = Fn_SQL & "ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]"
					Fn_SQL = Fn_SQL & " ALTER TABLE [dbo].["&TempTable&"] WITH NOCHECK ADD CONSTRAINT [PK_"&TempTable&"] PRIMARY KEY CLUSTERED ( [ID] ) ON [PRIMARY] "
					Result = KnifeCMS.DB.Execute(Fn_SQL)
					
					
					'2.创建分类表
					TempTable = TablePre & "Content_" & ContentSubSystem &"_Class"
					Fn_SQL = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].["&TempTable&"]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].["&TempTable&"] "
					Fn_SQL = Fn_SQL & "CREATE TABLE [dbo].["&TempTable&"] ("
					Fn_SQL = Fn_SQL & "[ID] [int] IDENTITY (1, 1) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassName] [nvarchar] (100) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassDepth] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ParentClassID] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassOrder] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassChildNum] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassHide] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ClassType] [int] NULL ,"
					Fn_SQL = Fn_SQL & "[ClassUrl] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[ClassLogo] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[Icon] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[Pic] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[TemplateSet] [int] NULL ,"
					Fn_SQL = Fn_SQL & "[CategoryTemplate] [nvarchar] (50) NULL ,"
					Fn_SQL = Fn_SQL & "[ContentTemplate] [nvarchar] (50) NULL ,"
					Fn_SQL = Fn_SQL & "[SeoTitle] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[SeoUrl] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[MetaKeywords] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[MetaDescription] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[Remarks] [ntext] NULL ,"
					Fn_SQL = Fn_SQL & "[Recycle] [int] NULL"
					Fn_SQL = Fn_SQL & ")"
					Fn_SQL = Fn_SQL & "ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]"
					Fn_SQL = Fn_SQL & " ALTER TABLE [dbo].["&TempTable&"] WITH NOCHECK ADD CONSTRAINT [PK_"&TempTable&"] PRIMARY KEY  CLUSTERED ([ID]) ON [PRIMARY] "
					If Result Then Result = KnifeCMS.DB.Execute(Fn_SQL)
					
					'3.创建评论表
					TempTable = TablePre & "Content_" & ContentSubSystem &"_Comment"
					Fn_SQL = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].["&TempTable&"]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].["&TempTable&"] "
					Fn_SQL = Fn_SQL & "CREATE TABLE [dbo].["&TempTable&"] ("
					Fn_SQL = Fn_SQL & "[ID] [int] IDENTITY (1, 1) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[SysID] [varchar] (32) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[P_ID] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Path] [varchar] (50) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Depth] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ChildNum] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[ContentID] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Content] [ntext] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[UserID] [int] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Username] [nvarchar] (50) NULL ,"
					Fn_SQL = Fn_SQL & "[HeadImg] [nvarchar] (250) NULL ,"
					Fn_SQL = Fn_SQL & "[AddTime] [datetime] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[UpdateTime] [datetime] NOT NULL ,"
					Fn_SQL = Fn_SQL & "[IP] [varchar] (64) NOT NULL ,"
					Fn_SQL = Fn_SQL & "[Disabled] [int] NOT NULL"
					Fn_SQL = Fn_SQL & ")"
					Fn_SQL = Fn_SQL & "ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]"
					Fn_SQL = Fn_SQL & " ALTER TABLE [dbo].["&TempTable&"] WITH NOCHECK ADD CONSTRAINT [PK_"&TempTable&"] PRIMARY KEY  CLUSTERED ([ID]) ON [PRIMARY] "
					If Result Then Result = KnifeCMS.DB.Execute(Fn_SQL)
				End Select
				
				If Result Then
					'2.添加 功能系统及操作对照 给系统创始人组添加权限
					
					'内容
					TempContentSubSystem = "content_"& ContentSubSystem
					Call KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SystemCtlAction &"] WHERE [SystemCtl]='"& TempContentSubSystem &"'")
					Call KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SysUserPermission &"] WHERE [SystemCtl]='"& TempContentSubSystem &"'")
					
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+1,"SystemCtl:"&TempContentSubSystem,"SystemAction:view","SystemCtlName:"&ContentSubSystemName,"SystemActionName:"&Lang_ContentSubSystemView,"Orders:"&TempMaxOrders+1))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','view',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+2,"SystemCtl:"&TempContentSubSystem,"SystemAction:add","SystemCtlName:"&ContentSubSystemName,"SystemActionName:"&Lang_ContentSubSystemAdd,"Orders:"&TempMaxOrders+1))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','add',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+3,"SystemCtl:"&TempContentSubSystem,"SystemAction:edit","SystemCtlName:"&ContentSubSystemName,"SystemActionName:"&Lang_ContentSubSystemEdit,"Orders:"&TempMaxOrders+1))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','edit',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+4,"SystemCtl:"&TempContentSubSystem,"SystemAction:del","SystemCtlName:"&ContentSubSystemName,"SystemActionName:"&Lang_ContentSubSystemDel,"Orders:"&TempMaxOrders+1))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','del',1)")
					
					'草稿
					TempContentSubSystem = "content_"& ContentSubSystem &"_drafts"
					Call KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SystemCtlAction &"] WHERE [SystemCtl]='"& TempContentSubSystem &"'")
					Call KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SysUserPermission &"] WHERE [SystemCtl]='"& TempContentSubSystem &"'")
					
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+5,"SystemCtl:"&TempContentSubSystem,"SystemAction:view","SystemCtlName:"&ContentSubSystemName&Lang_Drafts,"SystemActionName:"&Lang_ContentSubSystemView,"Orders:"&TempMaxOrders+2))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','view',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+6,"SystemCtl:"&TempContentSubSystem,"SystemAction:edit","SystemCtlName:"&ContentSubSystemName&Lang_Drafts,"SystemActionName:"&Lang_ContentSubSystemEdit,"Orders:"&TempMaxOrders+2))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','edit',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+7,"SystemCtl:"&TempContentSubSystem,"SystemAction:del","SystemCtlName:"&ContentSubSystemName&Lang_Drafts,"SystemActionName:"&Lang_ContentSubSystemDel,"Orders:"&TempMaxOrders+2))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','del',1)")
					'回收站
					TempContentSubSystem = "content_"& ContentSubSystem &"_recycle"
					Call KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SystemCtlAction &"] WHERE [SystemCtl]='"& TempContentSubSystem &"'")
					Call KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SysUserPermission &"] WHERE [SystemCtl]='"& TempContentSubSystem &"'")
					
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+8,"SystemCtl:"&TempContentSubSystem,"SystemAction:view","SystemCtlName:"&ContentSubSystemName&Lang_Recycle,"SystemActionName:"&Lang_ContentSubSystemView,"Orders:"&TempMaxOrders+3))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','view',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+9,"SystemCtl:"&TempContentSubSystem,"SystemAction:edit","SystemCtlName:"&ContentSubSystemName&Lang_Recycle,"SystemActionName:"&Lang_ContentSubSystemRevert,"Orders:"&TempMaxOrders+3))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','edit',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+10,"SystemCtl:"&TempContentSubSystem,"SystemAction:del","SystemCtlName:"&ContentSubSystemName&Lang_Recycle,"SystemActionName:"&Lang_ContentSubSystemDel,"Orders:"&TempMaxOrders+3))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','del',1)")
					'分类
					TempContentSubSystem = "content_"& ContentSubSystem &"_class"
					Call KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SystemCtlAction &"] WHERE [SystemCtl]='"& TempContentSubSystem &"'")
					Call KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SysUserPermission &"] WHERE [SystemCtl]='"& TempContentSubSystem &"'")
					
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+11,"SystemCtl:"&TempContentSubSystem,"SystemAction:view","SystemCtlName:"&ContentSubSystemName&Lang_Class,"SystemActionName:"&Lang_ClassView,"Orders:"&TempMaxOrders+4))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','view',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+12,"SystemCtl:"&TempContentSubSystem,"SystemAction:add","SystemCtlName:"&ContentSubSystemName&Lang_Class,"SystemActionName:"&Lang_ClassAdd,"Orders:"&TempMaxOrders+4))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','add',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+13,"SystemCtl:"&TempContentSubSystem,"SystemAction:edit","SystemCtlName:"&ContentSubSystemName&Lang_Class,"SystemActionName:"&Lang_ClassEdit,"Orders:"&TempMaxOrders+4))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','edit',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+14,"SystemCtl:"&TempContentSubSystem,"SystemAction:batchedit","SystemCtlName:"&ContentSubSystemName&Lang_Class,"SystemActionName:"&Lang_ClassBatchEdit,"Orders:"&TempMaxOrders+4))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','batchedit',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+15,"SystemCtl:"&TempContentSubSystem,"SystemAction:del","SystemCtlName:"&ContentSubSystemName&Lang_Class,"SystemActionName:"&Lang_ClassDel,"Orders:"&TempMaxOrders+4))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','del',1)")
					'评论
					TempContentSubSystem = "content_"& ContentSubSystem &"_comment"
					Call KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SystemCtlAction &"] WHERE [SystemCtl]='"& TempContentSubSystem &"'")
					Call KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SysUserPermission &"] WHERE [SystemCtl]='"& TempContentSubSystem &"'")
					
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+16,"SystemCtl:"&TempContentSubSystem,"SystemAction:view","SystemCtlName:"&ContentSubSystemName&Lang_Comment,"SystemActionName:"&Lang_CommentView,"Orders:"&TempMaxOrders+4))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','view',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+17,"SystemCtl:"&TempContentSubSystem,"SystemAction:add","SystemCtlName:"&ContentSubSystemName&Lang_Comment,"SystemActionName:"&Lang_CommentAdd,"Orders:"&TempMaxOrders+4))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','add',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+18,"SystemCtl:"&TempContentSubSystem,"SystemAction:edit","SystemCtlName:"&ContentSubSystemName&Lang_Comment,"SystemActionName:"&Lang_CommentEdit,"Orders:"&TempMaxOrders+4))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','edit',1)")
					Call KnifeCMS.DB.AddRecord(DBTable_SystemCtlAction,Array("ID:"&TempMaxID+19,"SystemCtl:"&TempContentSubSystem,"SystemAction:del","SystemCtlName:"&ContentSubSystemName&Lang_Comment,"SystemActionName:"&Lang_CommentDel,"Orders:"&TempMaxOrders+4))
					Call KnifeCMS.DB.Execute("INSERT INTO ["& DBTable_SysUserPermission &"] (GroupID,SystemCtl,SystemAction,IsAllow) VALUES (1,'"& TempContentSubSystem &"','del',1)")
					
					'3.添加 模型
					Result = KnifeCMS.DB.AddRecord(TablePre&"ContentSubSystem",Array("SysID:"& SysID,"System:"& ContentSubSystem,"SysDefault:0","SystemName:"& ContentSubSystemName,"CreateTime:"& SysTime,"Orders:"& ContentSubSystemOrders,"Template:"& Pv_IndexTemplate,"CategoryTemplate:"& Pv_CategoryTemplate,"ContentTemplate:"& Pv_ContentTemplate,"Config:"& Pv_Config,"SeoTitle:"& SeoTitle,"SeoUrl:"& SeoUrl,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription))
					'更新缓存
					
					Msg    = Lang_SubSystemCue(5) &"{"& ContentSubSystemName &"[ID="& ID &"]}"&"<br>"
					If Result Then Admin.ShowMessage Msg&"<b class=blue>"& Lang_Cue_AddSubSystemFlush &"</b><br>",Array("javascript:window.top.location='index.asp?ctl=systemindex&act=view';",Lang_Fresh),1
				End If
			End If
		Else
			Result = KnifeCMS.DB.UpdateRecord(TablePre &"ContentSubSystem",Array("SystemName:"& ContentSubSystemName,"Orders:"& ContentSubSystemOrders,"Template:"& Pv_IndexTemplate,"CategoryTemplate:"& Pv_CategoryTemplate,"ContentTemplate:"& Pv_ContentTemplate,"Config:"& Pv_Config,"SeoTitle:"& SeoTitle,"SeoUrl:"& SeoUrl,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription),Array("ID:"&ID))
			
			ContentSubSystem = KnifeCMS.DB.GetFieldByID(TablePre &"ContentSubSystem","System",ID)
			'更新缓存
			Msg    = Lang_SubSystemCue(6) &"{"& ContentSubSystemName &"[ID="& ID &"]}"
			If Result Then Admin.ShowMessage Msg,Array("javascript:window.top.location='index.asp?ctl=systemindex&act=view';",Lang_Fresh),1
		End If
	End Function
	
	Function ContentSubSystemDel()
		Dim TempTable
		Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT SysDefault,System,SystemName FROM ["& TablePre &"ContentSubSystem] WHERE ID="& ID &"")
		If Not(Rs.Eof) Then
			SysDefault           = KnifeCMS.Data.CLng(Rs(0))
			ContentSubSystem     = Rs(1)
			ContentSubSystemName = Rs(2)
			Result = 1
		Else
			ErrMsg = Lang_SubSystemCue(7) &"<br>"
		End If
		KnifeCMS.DB.CloseRs Rs
		If SysDefault = 1 Then ErrMsg = ErrMsg & Lang_SubSystemCue(13) & "<br>"
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"content.subsystem.asp?ctl=contentsubsystem&act=view",0 : Exit Function
		If Result And ContentSubSystem<>"" Then
			'1.删除数据表
			Select Case DB_Type
			Case 0
				TempTable = TablePre & "Content_" & ContentSubSystem
				Sql       = "drop table "& TempTable &""
				Result    = KnifeCMS.DB.Execute(Sql)
				TempTable = TablePre & "Content_" & ContentSubSystem &"_Class"
				Sql       = "drop table "& TempTable &""
				Result    = KnifeCMS.DB.Execute(Sql)
				TempTable = TablePre & "Content_" & ContentSubSystem &"_Comment"
				Sql       = "drop table "& TempTable &""
				Result    = KnifeCMS.DB.Execute(Sql)
			Case 1
				TempTable = TablePre & "Content_" & ContentSubSystem
				Sql =       " if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].["&TempTable&"]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].["&TempTable&"] "
				TempTable = TablePre & "Content_" & ContentSubSystem &"_Class"
				Sql = Sql & " if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].["&TempTable&"]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].["&TempTable&"] "
				TempTable = TablePre & "Content_" & ContentSubSystem &"_Comment"
				Sql = Sql & " if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].["&TempTable&"]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].["&TempTable&"] "
				Result = KnifeCMS.DB.Execute(Sql)
			End Select
			'2.删除 功能系统及操作对照
			Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SystemCtlAction &"] WHERE [SystemCtl]='content_"& ContentSubSystem &"'")
			Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SysUserPermission &"] WHERE [SystemCtl]='content_"& ContentSubSystem &"'")
			
			Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SystemCtlAction &"] WHERE [SystemCtl]='content_"& ContentSubSystem &"_drafts'")
			Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SysUserPermission &"] WHERE [SystemCtl]='content_"& ContentSubSystem &"_drafts'")

			Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SystemCtlAction &"] WHERE [SystemCtl]='content_"& ContentSubSystem &"_recycle'")
			Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SysUserPermission &"] WHERE [SystemCtl]='content_"& ContentSubSystem &"_recycle'")

			Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SystemCtlAction &"] WHERE [SystemCtl]='content_"& ContentSubSystem &"_class'")
			Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SysUserPermission &"] WHERE [SystemCtl]='content_"& ContentSubSystem &"_class'")
			
			Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SystemCtlAction &"] WHERE [SystemCtl]='content_"& ContentSubSystem &"_comment'")
			Result = KnifeCMS.DB.Execute("DELETE FROM ["& DBTable_SysUserPermission &"] WHERE [SystemCtl]='content_"& ContentSubSystem &"_comment'")
			
			'3.删除 模型
			Result = KnifeCMS.DB.Execute("DELETE FROM ["& TablePre &"ContentSubSystem] WHERE [ID]="& ID &"")
			
			'4.删除 自定义字段
			Result = KnifeCMS.DB.Execute("DELETE FROM ["& TablePre &"FieldSet] WHERE [System]='"& ContentSubSystem &"'")
			
			Msg    = Lang_SubSystemCue(12) &"{"& ContentSubSystemName &"[System="& ContentSubSystem &"][ID="& ID &"]}"
			If Result Then Admin.ShowMessage Msg,Array("javascript:window.top.location='index.asp?ctl=systemindex&act=view';",Lang_Fresh),1
			
		End If
	End Function

	
	Function ContentSubSystemView()
	%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <span class="sysBtn"><a href="content.subsystem.asp?ctl=contentsubsystem&act=add" ><b class="icon icon_userAdd"></b><% Echo Lang_CreateNewSubSystem %></a></span>
				  </td></tr>
			  </table>
		  </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <div class="inbox" style="zoom:1; overflow:auto; padding:20px;">
				<%
				   Dim TempSys,TempSysName,TempCreateTime
				   Set Rs = KnifeCMS.DB.GetRecord(TablePre&"ContentSubSystem:ID,SysDefault,System,SystemName,CreateTime","","ORDER BY Orders ASC")
				   Do While Not(Rs.Eof)
				   ID = Rs(0)
				   SysDefault = KnifeCMS.Data.CLng(Rs(1))
				   TempSys     = "content_"&Rs(2)
				   TempSysName = Rs(3)
				   TempCreateTime = Rs(4)
				%>
				<div class="subsystem">
				  <div class="imgdiv"></div>
				  <p class="name"><a href="content.asp?ctl=<% Echo TempSys %>&act=view"><% Echo TempSysName %></a></p>
				  <p class="pline">
					 <span class="sysBtn"><a href="content.subsystem.asp?ctl=contentsubsystem&act=edit&id=<% Echo ID %>"><b class="icon icon_edit"></b><% Echo Lang_SubSystemSet %></a></span>
					 <% If SysDefault<>1 Then %>
					 <span class="sysBtn"><a href="content.subsystem.asp?ctl=contentsubsystem&act=del&id=<% Echo ID %>" onclick="return confirm('<% Echo Lang_UninstallSubSystemConfirm %>');"><b class="icon icon_del"></b><% Echo Lang_UninstallSubSystem %></a></span>
					 <% End If %>
				  </p>
				  <p class="pline pt3"><span class="time"><% Echo Lang_CreateTime &":"& TempCreateTime %></span></p>
				</div>
				<%
				   Rs.Movenext()
				   Loop
				   KnifeCMS.DB.CloseRs Rs 
				%>
			</div>
		</div>
		</DIV>
	</DIV>
	<%
	End Function
	Function ContentSubSystemDo()
	%>
	<DIV id="MainFrame">
	  <div id="wrap">
		<div class="tagLine"><% Echo NavigationLine %></div>
		<DIV class="cbody">
		  <div class="wbox">
			<form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
            <div class="inbox">
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
              <tbody>
                 <tr><td class="titletd"><span class="required">*</span><% Echo Lang_SubSystem(2) %></td>
                     <td class="infotd">
                     <input type="text" class="TxtClass"name="ContentSubSystemName" id="ContentSubSystemName" maxLength="45" value="<% Echo ContentSubSystemName %>" style="width:260px;"/>
                     <span class="mt5"><label class="Normal" id="d_ContentSubSystemName"><% Echo Lang_SubSystemCue(2) %></label></span>
                     </td></tr>
                 <tr><td class="titletd"><span class="required">*</span><% Echo Lang_SubSystem(1) %></td>
                     <td class="infotd">
                     <input type="text" class="TxtClass" name="ContentSubSystem" id="ContentSubSystem" maxLength="50" onblur="KnifeCMS.onblur(this,'letter')" value="<% Echo ContentSubSystem %>" style="width:120px;" <% If Action="edit" Then Echo "readonly=""readonly""" %>/>
                     <span class="mt5"><label class="Normal" id="d_ContentSubSystem"><% Echo Lang_SubSystemCue(1) %></label></span>
                     </td></tr>
                 <tr><td class="titletd"><% Echo Lang_SubSystem(3) %></td>
                     <td class="infotd">
                     <input type="text" class="TxtClass" name="ContentSubSystemOrders" id="ContentSubSystemOrders" maxLength="50" onblur="KnifeCMS.onblur(this,'int')" value="<% Echo ContentSubSystemOrders %>" style="width:80px;" />
                     <label class="Normal" id="d_ContentSubSystemOrders"><% Echo Lang_SubSystemCue(8) %></label>
                     </td></tr>
               </tbody>
              </table>
            </div>
            
            <div class="inbox">
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
              <tbody>
                 <tr><td class="titletd"><% Echo Lang_SubSystem(5) %></td>
                     <td class="infotd">
                     <%=Admin.SelectTemplateFile("IndexTemplate","content.subsys.htm",Pv_IndexTemplate)%>
                     <span class="mt5"><label class="Normal" id="d_IndexTemplate"><%=Lang_SubSystemCue(14)%></label></span>
                     </td>
                 </tr>
                 <tr><td class="titletd"><% Echo Lang_SubSystem(6) %></td>
                     <td class="infotd">
                     <%=Admin.SelectTemplateFile("CategoryTemplate","content.subsys.category.htm",Pv_CategoryTemplate)%>
                     <span class="mt5"><label class="Normal" id="d_CategoryTemplate"><%=Lang_SubSystemCue(15)%></label></span>
                     </td>
                 </tr>
                 <tr><td class="titletd"><% Echo Lang_SubSystem(7) %></td>
                     <td class="infotd">
                     <%=Admin.SelectTemplateFile("ContentTemplate","content.subsys.detail.htm",Pv_ContentTemplate)%>
                     <span class="mt5"><label class="Normal" id="d_ContentTemplate"><%=Lang_SubSystemCue(16)%></label></span>
                     </td>
                 </tr>
               </tbody>
              </table>
            </div>
            
            <div class="inbox">
              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
              <tbody>
                 <tr><td class="titletd" valign="top"><% Echo Lang_SeoTitle %></td><td class="infotd">
                     <input type="text" class="TxtClass long" name="SeoTitle" id="SeoTitle" maxlength="250" value="<%=KnifeCMS.Data.ReplaceString(SeoTitle,"&nbsp;",Chr(32))%>"/> <label class="Normal" id="d_SeoTitle"><% Echo Lang_SeoTitleCue_0 %></label>
                     </td></tr>
                 <tr><td class="titletd" valign="top"><% Echo Lang_SeoUrl %></td><td class="infotd">
                     <input type="text" class="TxtClass readonly" name="url_pre" readonly="readonly" style="width:280px;" value="<% Echo SiteURL & SystemPath & "?content/"& KnifeCMS.IIF(Not(KnifeCMS.Data.IsNul(ContentSubSystem)),ContentSubSystem,Lang_SubSystem(1)) &"/" %>"/>
                     <input type="text" class="TxtClass" name="SeoUrl" id="SeoUrl" maxlength="250"  style="width:200px;" value="<%=SeoUrl%>"/>
                     <input type="text" class="TxtClass readonly" name="url_suffix" readonly="readonly" style="width:40px;" value="<% Echo FileSuffix %>"/>
                     <label class="Normal" id="d_SeoUrl"><% Echo Lang_SeoUrlCue_0 %></label>
                     </td></tr>
                 <tr><td class="titletd" valign="top"><% Echo Lang_MetaKeywords %></td><td class="infotd">
                     <input type="text" class="TxtClass long" name="MetaKeywords" id="MetaKeywords" maxlength="250" value="<%=KnifeCMS.Data.ReplaceString(MetaKeywords,"&nbsp;",Chr(32))%>"/> <label class="Normal" id="d_MetaKeywords"><% Echo Lang_MetaKeywordsCue_0 %></label>
                     </td></tr>
                 <tr><td  class="titletd" valign="top"><% Echo Lang_MetaDescription %></td><td class="infotd">
                     <textarea class="metadescription" name="MetaDescription" id="MetaDescription" maxlength="250" onblur="KnifeCMS.onblur(this,'maxLength:250')" ><%=KnifeCMS.Data.ReplaceString(MetaDescription,"&nbsp;",Chr(32))%></textarea>
                     <label class="Normal" id="d_MetaDescription"><% Echo Lang_MetaDescriptionCue_0 %></label>
                     </td>
                 </tr>
              </tbody>
              </table>
            </div>
			<div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
			  <input type="hidden" name="id" value="<% Echo ID %>" />
			  <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
			  <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Content.SaveSubSystemCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
			  <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
			</div>
			</form>
		  </div>
		</DIV>
	  </div>
	</DIV>
	<%
	End Function
End Class
	%>