<!--#include file="config.admin.asp"-->
<%
Dim Vote
Set Vote = New Class_KnifeCMS_Admin_Vote
Set Vote = Nothing
Class Class_KnifeCMS_Admin_Vote
	Private VoteTitle,VoteMode,VoteMode_More,VoteOption,VoteOptions,VoteDescription,ResultDescribe
	Private SeoTitle,SeoUrl,MetaKeywords,MetaDescription
	Private VoteOptionNum

	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "vote.lang.asp"
		IsContentExist = False
		Header()
		If Ctl="vote" Or Ctl = "vote_recycle" Then
			Select Case Action
				Case "view" : Call VoteView()
				Case "add","edit"
					If SubAction="save" Then
						Call VoteDoSave()
					Else
						If Action = "edit" Then
							If SubAction = "revert" Then
								Call VoteOtherEdit()
							Else
								Set Rs = KnifeCMS.DB.GetRecord(TablePre&"Vote:ClassID,VoteTitle,VoteMode,VoteMode_More,Description,ResultDescribe,SeoTitle,SeoUrl,MetaKeywords,MetaDescription",Array("ID:"&ID),"")
								If Not(Rs.Eof) Then
									IsContentExist  = True
									ClassID         = Rs("ClassID")
									VoteTitle       = Rs("VoteTitle")
									VoteMode        = Rs("VoteMode")
									VoteMode_More   = KnifeCMS.Data.CLng(Rs("VoteMode_More"))
									VoteDescription = Rs("Description")
									ResultDescribe  = Rs("ResultDescribe")
									SeoTitle        = Rs("SeoTitle")
									SeoUrl          = Rs("SeoUrl")
									MetaKeywords    = Rs("MetaKeywords")
									MetaDescription = Rs("MetaDescription")
								End If
								KnifeCMS.DB.CloseRs Rs
								If IsContentExist Then Call VoteDo() : Else Admin.ShowMessage Lang_VoteCue(7),"goback",0
							End If
						ElseIf Action = "add" Then
							If SeoUrl  = "" Then SeoUrl="index"
							Call VoteDo()
						End If
					End If
				Case "del"  : VoteDel
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"goback",0
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Function VoteList()
		Dim Temp_Sql
		If Ctl="vote_recycle" Then
		  Temp_Sql = "SELECT a.ID,a.VoteTitle,b.ClassName,a.VoteMode,a.Hits,a.Joins,a.AddTime FROM "&TablePre&"Vote a LEFT JOIN "&TablePre&"VoteClass b ON (a.ClassID=b.ID) WHERE a.Recycle=1 "
		Else
		  Temp_Sql = "SELECT a.ID,a.VoteTitle,b.ClassName,a.VoteMode,a.Hits,a.Joins,a.AddTime FROM "&TablePre&"Vote a LEFT JOIN "&TablePre&"VoteClass b ON (a.ClassID=b.ID) WHERE a.Recycle=0 "
		  If SubAction = "search" Then
			  Admin.Search.ClassID = KnifeCMS.IIF(Admin.Search.ClassID="",0,Admin.Search.ClassID)
			  If Admin.Search.ClassID > 0 Then Admin.GetChildClass TablePre&"VoteClass",Admin.Search.ClassID
			  tempCondition = KnifeCMS.IIF(Admin.Search.ClassID > 0," AND ClassID IN("&Admin.Search.ClassID&Admin.ChildClass&") "," ")
			  tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.KeyWords<>""," AND VoteTitle like '%"& Admin.Search.KeyWords &"%' "," ")
			  PageUrlPara = "classid="& Admin.Search.ClassID &"&keywords="& Server.URLEncode(Admin.Search.KeyWords) &""
		  End If
		  Temp_Sql = Temp_Sql & tempCondition
		  Operation="<span class=""sysBtn""><a href=""?ctl="& Ctl &"&act=edit&id={$ID}"" ><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		End If
		Temp_Sql = Temp_Sql & " ORDER BY a.ID DESC"
		Call Admin.DataList(Temp_Sql,20,7,Operation,PageUrlPara)
	End Function
	
	Function VoteDoSave()
		'获取数据并检查数据正确性
		VoteTitle       = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","VoteTitle")),250)
		VoteDescription = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","VoteDescription")),1000)
		VoteMode        = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","VoteMode"))
		VoteMode_More   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","VoteMode_More"))
		ResultDescribe  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","ResultDescribe")),1000)
		
		'SEO优化内容
		SeoTitle            = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","SeoTitle")),250)        : If KnifeCMS.Data.IsNul(SeoTitle) Then SeoTitle=VoteTitle
		SeoUrl              = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","SeoUrl")),250)
		MetaKeywords        = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","MetaKeywords")),250)
		MetaDescription     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","MetaDescription")),250)
		
		'数据合法性检测
		VoteOptionNum = 0
		Dim Options_i
		for each Options_i in Request.Form("VoteOption")
			If Trim(Replace(Options_i,"&nbsp;",""))<>"" Then VoteOptionNum = VoteOptionNum+1
		next
		If VoteTitle = ""          Then ErrMsg = Lang_VoteCue(3) &"<br>"
		If ClassID   < 1           Then ErrMsg = ErrMsg & Lang_VoteCue(11) &"<br>"
		If VoteMode  = 2     And VoteMode_More < 2  Then ErrMsg = ErrMsg & Lang_VoteCue(4) &"<br>"
		If Action    = "add" Then
			If VoteOptionNum < 1   Then ErrMsg = ErrMsg & Lang_VoteCue(5) &"<br>"
			if VoteOptionNum < 2   Then ErrMsg = ErrMsg & Lang_VoteCue(6) &"<br>"
			if VoteOptionNum > 100 Then ErrMsg = ErrMsg & Lang_VoteCue(12) &"<br>"
		End If
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		'开始保存数据操作
		If Action="add" then
			Result = KnifeCMS.DB.AddRecord(DBTable_Vote,Array("SysID:"&SysID,"ClassID:"&ClassID,"VoteTitle:"&VoteTitle,"VoteMode:"&VoteMode,"VoteMode_More:"&VoteMode_More,"Description:"&VoteDescription,"ResultDescribe:"&ResultDescribe,"Hits:0","Joins:0","SeoTitle:"& SeoTitle,"SeoUrl:"& SeoUrl,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription,"AddTime:"&SysTime,"Recycle:0"))
			ID = Admin.GetIDBySysID(DBTable_Vote,SysID)
			If ID < 1 Then Admin.ShowMessage Lang_VoteCue(10),"goback",0 : Exit Function
			'For i=0 To UBound(C_TempArr)
			for each Options_i in Request.Form("VoteOption")
				C_TempStr = KnifeCMS.Data.Left(Trim(Replace(Options_i,"&nbsp;"," ")),1000)
				IF Not(KnifeCMS.Data.IsNul(C_TempStr)) Then
				Result = KnifeCMS.DB.AddRecord(TablePre&"VoteOption",Array("VoteID:"&ID,"Content:"&C_TempStr,"VoteNum:0","AddTime:"&SysTime))
				End IF
			Next
			
			'更新缓存
			If IsCache Then
				Print "<div class=""creating_cache"">"
				Call Admin.CreateCache_Init()
				Call Admin.CreateStaticHTML_Vote(False,ID,0)
				Call Admin.CreateCache_Terminate()
				Print "</div>"
			End If
			
			Msg    = Lang_VoteCue(8) &"{"& VoteTitle &"[ID="& ID &"]}"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		ElseIf Action="edit" Then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Vote,Array("ClassID:"&ClassID,"VoteTitle:"&VoteTitle,"VoteMode:"&VoteMode,"VoteMode_More:"&VoteMode_More,"Description:"&VoteDescription,"ResultDescribe:"&ResultDescribe,"SeoTitle:"& SeoTitle,"SeoUrl:"& SeoUrl,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription,"EditTime:"&SysTime),Array("ID:"&ID))
			'更新缓存
			If IsCache Then
				Print "<div class=""creating_cache"">"
				Call Admin.CreateCache_Init()
				Call Admin.CreateStaticHTML_Vote(False,ID,0)
				Call Admin.CreateCache_Terminate()
				Print "</div>"
			End If
			Msg    = Lang_VoteCue(9) &"{"& VoteTitle &"[ID="& ID &"]}"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		End If
	End Function
	
	Function VoteDel()
		VoteSubDo
	End Function
	
	Function VoteOtherEdit()
		VoteSubDo
	End Function
	
	Sub VoteSubDo()
		ID = KnifeCMS.GetForm("post","InputName")
		Dim ArrID : ArrID = Split(ID,",")
		For i=0 To Ubound(ArrID)
			ID = KnifeCMS.Data.Int(ArrID(i))
			Result    = 0
			If ID > 0 Then
				Set Rs = KnifeCMS.DB.GetRecord(TablePre&"Vote:VoteTitle",Array("ID:"&ID),"")
				If Not(Rs.Eof) Then VoteTitle=Rs(0)
				KnifeCMS.DB.CloseRs Rs
				'开始执行操作
				If Action = "del" Then
					If SubAction = "completely" Then
						Result = KnifeCMS.DB.DeleteRecord(DBTable_Vote,Array("ID:"&ID))
						Result = KnifeCMS.DB.DeleteRecord(TablePre&"VoteOption",Array("VoteID:"&ID))
						Msg    = Lang_VoteCue(2)
						If Result Then Admin.ShowMessage Msg &"{"& VoteTitle &"[ID="& ID &"]}",Url,1
					Else
						Result = KnifeCMS.DB.UpdateRecord(DBTable_Vote,Array("Recycle:1"),Array("ID:"&ID))
						Msg    = Lang_VoteCue(1)
						If Result Then Admin.ShowMessage Msg &"{"& VoteTitle &"[ID="& ID &"]}",Url,1
					End If
				ElseIf Action = "edit" Then
					If SubAction = "revert" Then
						Result = KnifeCMS.DB.UpdateRecord(DBTable_Vote,Array("Recycle:0"),Array("ID:"&ID))
						Msg    = Lang_VoteCue(0)
						If Result Then Admin.ShowMessage Msg &"{"& VoteTitle &"[ID="& ID &"]}",Url,1
					End If
				End If
			End If
		Next
	End Sub
	
	Private Function VoteView()
		If Ctl="vote_recycle" Then
		%>
		<DIV id="MainFrame">
		  <div class="tagLine"><% Echo NavigationLine %></div>
		  <DIV class="cbody">
			<div class="toparea">
			  <table border="0" cellpadding="0" cellspacing="0">
			  <tr>
			  <td class="tdcell">
				  <table border="0" cellpadding="2" cellspacing="0">
				  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
				  <tr><td class="downcell">
					  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:revert','')"><b class="icon icon_revert"></b><% Echo Lang_DoRevert %></button>
					  </td></tr>
				  </table>
			  </td>
			  <td class="tdcell">
				  <table border="0" cellpadding="2" cellspacing="0">
				  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
				  <tr><td class="downcell">
					  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
					  </td></tr>
				  </table>
			  </td>
			  </tr>
			  </table>
			</div>
			<div class="tabledata">
			<form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
			  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
			   <tbody>
				<tr class="top">
				<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
				<th width="3%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %></div></th><!--系统ID-->
				<th width="26%"><div style="width:260px;"><% Echo Lang_ListTableColLine_Title %></div></th><!--标题-->
				<th width="5%"><div style="width:100px;"><% Echo Lang_ListTableColLine_Class %></div></th><!--所属分类-->
				<th width="5%"><div style="width:60px;"><% Echo Lang_ListTableColLine_VoteMode %></div></th><!--投票方式-->
				<th width="5%"><div style="width:50px;"><% Echo Lang_ListTableColLine_Hits %></div></th><!--浏览量-->
				<th width="5%"><div style="width:50px;"><% Echo Lang_ListTableColLine_Joins %></div></th><!--参与量-->
				<th width="10%"><div style="width:135px;"><% Echo Lang_ListTableColLine_AddTime %></div></th><!--添加时间-->
				<th><div style="width:80px;"><% Echo Lang_ListTableColLine_Do %></div></th><!--操作-->
				</tr>
				<% VoteList %>
			   </tbody>
			  </table>
			</form>
			</div>
		  </DIV>
		</DIV>
		<script type="text/javascript">
		TrBgChange("listForm","InputName","oliver","data","select");
		</script>
		<% Else %>
		<DIV id="MainFrame">
		  <div class="tagLine"><% Echo NavigationLine %></div>
		  <DIV class="cbody">
			<div class="toparea">
			  <table border="0" cellpadding="0" cellspacing="0">
			  <tr>
			  <td class="tdcell">
				  <table border="0" cellpadding="2" cellspacing="0">
				  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
				  <tr><td class="downcell">
					  <span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=add" ><b class="icon icon_add"></b><% echo Lang_DoAdd %></a></span>
					  </td></tr>
				  </table>
			  </td>
			  <td class="tdcell">
				  <table border="0" cellpadding="2" cellspacing="0">
				  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
				  <tr><td class="downcell">
					  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','')"><b class="icon icon_recycle"></b><% Echo Lang_DoDel%></button>
					  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
					  </td></tr>
				  </table>
			  </td>
			  <td class="tdcell">
				<table border="0" cellpadding="2" cellspacing="0">
				<tr><td class="upcell">
					<div class="align-center grey">
						<% Echo Lang_DistrictDoSearch %><!--搜索操作区-->
						<span class="AdvancedSearch"><a href="javascript:Admin.AdvancedSearch.Open(600,320,'AdvancedSearch')" ><% Echo Lang_AdvancedSearch %></a></span>
					</div>
					<DIV id="AdvancedSearch" style="display:none;">
						<form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
						<div class="diainbox mb5">
						  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
						  <tr><td class="titletd"><% Echo Lang_ClassSelect %></td>
									<td class="infotd"><select id="classid" name="classid"><%=Admin.ShowClass(1,Admin.Search.ClassID,TablePre&"VoteClass")%></select></td>
						  </tr>
						  <tr><td class="titletd"><% Echo Lang_Keywords %></td>
							  <td class="infotd"><input type="text" class="TxtClass" id="keywords" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:280px;" /></td>
						  </tr>
						  </table>
						</div>
						<% Echo AdvancedSearchBtnline %>
						</form>
					</DIV>
					</td></tr>
				<tr><td class="downcell">
					<form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
					<table border="0" cellpadding="0" cellspacing="0"><tr>
					<td><span><% Echo Lang_Keywords %>:</span></td>
					<td class="pl3"><input type="text" class="TxtClass" id="keywords" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:220px;" /></td>
					<td class="pl3"><button type="submit" class="sysBtn" ><b class="icon icon_search"></b><% Echo Lang_Btn_Search %></button></td>
					</tr></table>
					</form>
				</td></tr>
				</table>
			  </td>
			  </tr>
			  </table>
			</div>
			<div class="tabledata">
			  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
			  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
			   <tbody>
				<tr class="top">
				<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
				<th width="3%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %></div></th><!--系统ID-->
				<th width="35%"><div style="width:250px;"><% Echo Lang_ListTableColLine_Title %></div></th><!--标题-->
				<th width="5%"><div style="width:100px;"><% Echo Lang_ListTableColLine_Class %></div></th><!--所属分类-->
				<th width="5%"><div style="width:60px;"><% Echo Lang_ListTableColLine_VoteMode %></div></th><!--投票方式-->
				<th width="5%"><div style="width:50px;"><% Echo Lang_ListTableColLine_Hits %></div></th><!--浏览量-->
				<th width="5%"><div style="width:50px;"><% Echo Lang_ListTableColLine_Joins %></div></th><!--参与量-->
				<th width="10%"><div style="width:135px;"><% Echo Lang_ListTableColLine_AddTime %></div></th><!--添加时间-->
				<th><div style="width:80px;"><% Echo Lang_ListTableColLine_Do %></div></th><!--操作-->
				</tr>
				<% VoteList %>
			   </tbody>
			  </table>
			  <% If SubAction="getitems" Then Echo GetSelectItemsBtnLine %>
			  </form>
			</div>
			</DIV>
		</DIV>
		<script type="text/javascript">
		TrBgChange("listForm","InputName","oliver","data","select");
		function ChangeTdText(){
			var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
			var content;
			if(tds!=null){
				for(var i=1;i<tds.length;i++){
					if(tds[i].id=="td3" && tds[i].innerHTML==1){
						tds[i].innerHTML=Lang_Js_RadioSelect;
					}
					if(tds[i].id=="td3" && tds[i].innerHTML==2){
						tds[i].innerHTML=Lang_Js_CheckBoxSelect;
					}
				}
			}
		}
		ChangeTdText();
		ReplaceText(document.getElementById("keywords").value,1);
		</script>
		<% End If %>
		<%
	End Function
	
	Private Function VoteDo()
		%>
		<style type="text/css">body{ padding-bottom:37px;}</style>
		<DIV id="MainFrame">
		  <div class="tagLine"><% Echo NavigationLine %></div>
		  <DIV class="cbody">
			<div class="wbox">
				<div class="inbox">
				  <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
					 <div class="inbox">
                     <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable" id="Tabs_1">
					   <tbody>
						 <tr><td class="titletd"><% Echo Lang_Vote(1) %></td>
							 <td class="infotd">
							 <input type="hidden" name="VoteID" value="<%=ID%>" />
							 <input type="text" class="TxtClass long" id="VoteTitle" name="VoteTitle" onblur="Admin.AutoSeoTitle(KnifeCMS.$id('VoteTitle'),KnifeCMS.$id('TempTitle'),KnifeCMS.$id('SeoTitle'));" value="<%=KnifeCMS.Data.HTMLDecode(VoteTitle)%>" /><label class="Normal" id="d_VoteTitle"></label>
                             <input type="hidden" id="TempTitle" name="TempTitle" value="<%=KnifeCMS.Data.HTMLDecode(VoteTitle)%>"/></td>
						 </tr>
						 <tr><td class="titletd"><% Echo Lang_Vote(2) %></td>
							 <td class="infotd"><select id="ClassID" name="ClassID"  style="font-size:13px;"><%=Admin.ShowClass(0,ClassID,TablePre&"VoteClass")%></select><label class="Normal" id="d_ClassID"></label></td>
						 </tr>
						 <tr><td class="titletd" valign="top"><% Echo Lang_Vote(3) %></td>
							 <td class="infotd"><textarea class="abstract" id="VoteDescription" name="VoteDescription"><%=VoteDescription%></textarea><label class="Normal" id="d_VoteDescription"></label></td>
						 </tr>
						 <tr><td class="titletd" valign="top"></td><td class="infotd"><label class="Normal" id="d_VoteOptions"></label></td></tr>
					   </tbody> 
					   <%
						  Echo "<tbody id=""VoteOptions"">"
						  If ID > 0 And Action="edit" Then
							  Set Rs = KnifeCMS.DB.GetRecord(TablePre&"VoteOption:Content",Array("VoteID:"&ID),"")
							  IF Rs.bof and Rs.eof Then
								Echo "<tr><td class=""titletd"">"&  Lang_Vote(4) &"</td><td class=""infotd"">"
								Echo ""&Lang_VoteCue(13)&""
								Echo "</td></tr>"&vbCrLf
							  Else
								VoteOptionNum=0
								Do While Not Rs.Eof
								VoteOption=Rs(0)
								'VoteOption=Replace(VoteOption,"%2C",",")
								Echo "<tr id=""Vote_"& VoteOptionNum+1 &"""><td class=""titletd"">"&  Lang_Vote(4) &""& VoteOptionNum+1 &"</td><td class=""infotd"">"
								Echo "<input type=""text"" class=""TxtClass long"" id=""VoteOption_"& VoteOptionNum+1 &""" name=""VoteOption"" disabled=""disabled"" value="""& VoteOption &""" />"
								Echo "</td></tr>"&vbCrLf
								VoteOptionNum=VoteOptionNum+1
								Rs.movenext
								Loop
							  End IF
							  Set	Rs=Nothing
						  Else
							  For i=1 To 5
							  Echo "<tr id=""Vote_"&i&"""><td class=""titletd"">"&  Lang_Vote(4) &""&i&"</td><td class=""infotd"">"
							  Echo "<input type=""text"" class=""TxtClass long"" id=""VoteOption_"&i&""" name=""VoteOption"" value="""" />"
							  Echo "<a href=""javascript:void(0)"" onclick=""DeleteCurrentRow(this);ChangeVoteMode_More();"" class=""reduce""><img src=""Image/reduce.gif"" /></a>"
							  Echo "<label class=""Normal""></label>"
							  Echo "</td></tr>"&vbCrLf
							  Next
							  Echo "</tbody>"&vbCrLf
							  Echo "<tbody>"&vbCrLf
							  Echo "<tr><td class=""titletd"">.</td>"
							  Echo "<td class=""infotd""><a  href=""javascript:AddVoteOptions('VoteOptions',1)"" class=""add_01"">"&  Lang_Vote(5) &"</a> <a  href=""javascript:AddVoteOptions('VoteOptions',3)"" class=""add_01"" style=""margin-left:30px;"">"&  Lang_Vote(6) &"</a>"
							  Echo "</td>"
							  Echo "</tr>"
							  Echo "</tbody>"&vbCrLf
						  End If
						 %>
					   <tbody>
						 <tr><td class="titletd"><% Echo Lang_Vote(7) %></td>
							 <td class="infotd">
								 <% if KnifeCMS.Data.Int(VoteMode) = 1 then
								 Echo "<input type=""radio"" id=""VoteMode_1"" name=""VoteMode"" checked=""checked"" onclick=""SetInputDisabled(1,'VoteMode_1','VoteMode_More')"" value=""1"" />"&Lang_RadioSelect
								 Echo "<input type=""radio"" id=""VoteMode_2"" name=""VoteMode"" onclick=""SetInputDisabled(0,'VoteMode_2','VoteMode_More')"" value=""2"" />"&Lang_CheckBoxSelect
								 Echo "<select id=""VoteMode_More"" name=""VoteMode_More"" disabled=""disabled"">"
								 else
								 Echo "<input type=""radio"" id=""VoteMode_1""  name=""VoteMode"" onclick=""SetInputDisabled(1,'VoteMode_1','VoteMode_More')"" value=""1"" />"&Lang_RadioSelect
								 Echo "<input type=""radio"" id=""VoteMode_2"" name=""VoteMode"" checked=""checked"" onclick=""SetInputDisabled(0,'VoteMode_2','VoteMode_More')"" value=""2"" />"&Lang_CheckBoxSelect
								 Echo "<span id=""VoteMode_MoreArea"">"
								 Echo "<select id=""VoteMode_More"" name=""VoteMode_More"">"
								 end if
								 %>
								 <%
								 IF KnifeCMS.Data.Int(VoteOptionNum)<1 Then VoteOptionNum=5
								 For i=2 to VoteOptionNum
								   IF ( i <= VoteOptionNum And VoteMode_More=i ) or ( i=VoteOptionNum And VoteMode_More<1 ) Then
								   Echo "<option value="&i&" selected=""selected"">"&Lang_Vote(11)&i&Lang_Vote(12)&"</option>"
								   Else
								   Echo "<option value="&i&">"&Lang_Vote(11)&i&Lang_Vote(12)&"</option>"
								   End IF
								 Next
								 Echo "</select>"
								 Echo "</span>"
								 %>
							 </td>
						 </tr>
						 <tr><td class="titletd"><% Echo Lang_Vote(10) %></td>
							 <td class="infotd"><textarea class="abstract" id="ResultDescribe" name="ResultDescribe"><%=ResultDescribe%></textarea>
								 <label class="Normal"></label>
							 </td>
						 </tr>
					   </tbody>
					 </table>
                     </div>
                     <div class="inbox">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                      <tbody>
                         <tr><td class="titletd" valign="top"><% Echo Lang_SeoTitle %></td><td class="infotd">
                             <input type="text" class="TxtClass long" name="SeoTitle" id="SeoTitle" maxlength="250" value="<%=KnifeCMS.Data.ReplaceString(SeoTitle,"&nbsp;",Chr(32))%>"/> <label class="Normal" id="d_SeoTitle"><% Echo Lang_SeoTitleCue %></label>
                             </td></tr>
                         <tr><td class="titletd" valign="top"><% Echo Lang_SeoUrl %></td><td class="infotd">
                             <input type="text" class="TxtClass readonly" name="url_pre" readonly="readonly" style="width:288px;" value="<% Echo SiteURL & SystemPath & "?vote/"& KnifeCMS.IIF(ID>0,ID,"{id}") &"/" %>"/>
                             <input type="text" class="TxtClass" name="SeoUrl" id="SeoUrl" maxlength="250"  style="width:160px;" value="<%=SeoUrl%>"/>
                             <input type="text" class="TxtClass readonly" name="url_suffix" readonly="readonly" style="width:40px;" value="<% Echo FileSuffix %>"/>
                             <label class="Normal" id="d_SeoUrl"><% Echo Lang_SeoUrlCue %></label>
                             </td></tr>
                         <tr><td class="titletd" valign="top"><% Echo Lang_MetaKeywords %></td><td class="infotd">
                             <input type="text" class="TxtClass long" name="MetaKeywords" id="MetaKeywords" maxlength="250" value="<%=KnifeCMS.Data.ReplaceString(MetaKeywords,"&nbsp;",Chr(32))%>"/> <label class="Normal" id="d_MetaKeywords"><% Echo Lang_MetaKeywordsCue %></label>
                             </td></tr>
                         <tr><td  class="titletd" valign="top"><% Echo Lang_MetaDescription %></td><td class="infotd">
                             <textarea class="metadescription" name="MetaDescription" id="MetaDescription" maxlength="250" onblur="KnifeCMS.onblur(this,'maxLength:250')" ><%=KnifeCMS.Data.ReplaceString(MetaDescription,"&nbsp;",Chr(32))%></textarea>
                             <label class="Normal" id="d_MetaDescription"><% Echo Lang_MetaDescriptionCue %></label>
                             </td>
                         </tr>
                      </tbody>
                      </table>
                    </div>
					<div class="bottomSaveline" id="SubmitButtoms" style="text-align:left;">
					  <input type="hidden" name="id" value="<% Echo ID %>" />
					  <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
					  <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Vote.SaveVoteCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
					  <!--<button type="submit" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn"><b class="icon icon_save"></b>Save</button>-->
					  <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
					</div>
				</form>
				</div>
			<div class="illustration"><SPAN><B><% Echo Lang_SystemCue %>:</B><% Echo Lang_VoteCue(14) %></SPAN></div>
			</div>
		  </DIV>
		</DIV>
        <script type="text/javascript">
		Admin.AutoSeoTitle(KnifeCMS.$id('VoteTitle'),KnifeCMS.$id('TempTitle'),KnifeCMS.$id('SeoTitle'));
		</script>
		<%
	End Function
		
End Class
%>