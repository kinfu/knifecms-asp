<!--#include file="config.admin.asp"-->
<%
Dim ClassContent
Set ClassContent = New Class_KnifeCMS_Admin_Content
Set ClassContent = Nothing
Class Class_KnifeCMS_Admin_Content
	Private Pv_Rs,Pv_ii,Pv_jj,Pv_IsSystemExit,Pv_System,Pv_PaginationLabel
	Private Pv_ContentID,Title,TitleFontColor,TitleFontType,BriefTitle,BriefTitleFontColor,BriefTitleFontType
	Private Author,From,PublicTime,Thumbnail,UseFirstImg,LinkUrl,UseLinkUrl,Tags,GetAbstract,Abstract,PaginationType,SaveRemotePic,TextContent,Hits,TopLine,OnTop,Hot,Elite,CommentSet,Openness,Password,RePassword,SlidesFirstImg,SlidesImgs,LinkVotes,LinkContents
	Private SeoTitle,SeoUrl,MetaKeywords,MetaDescription
	Private ContentStatus
	Private ViewType
	Private TempImg
	Private Pv_Field,Pv_FieldName,Pv_FieldType,Pv_NotNull,Pv_FieldOptions,Pv_FieldTips,Pv_OrderNum,Pv_Recycle
	Private Pv_FieldValue,Pv_Array,Pv_Array2,Pv_Dictionary

	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "content.lang.asp"
		Pv_PaginationLabel= "<!--[nextpage]-->"
		Set Pv_Dictionary = Server.CreateObject("Scripting.Dictionary")
		IsContentExist    = False
		Pv_IsSystemExit   = False
		C_TempArr=Split(Ctl,"_")
		If Ubound(C_TempArr)>=1 Then
			Pv_System = KnifeCMS.Data.Left(KnifeCMS.Data.FiltNotLetter(C_TempArr(1)),50)
			If Not(KnifeCMS.Data.IsNul(Pv_System)) Then
				'检查系统是否存在
				Pv_IsSystemExit = KnifeCMS.DB.CheckIsRecordExist(DBTable_ContentSubSystem,Array("System:"& Pv_System),"")
				If Ubound(C_TempArr)>=2 Then ViewType = C_TempArr(2)
			End If
		End If
		Header()
		If Pv_IsSystemExit Then
			DBTable       = TablePre&"Content_"&Pv_System
			DBTable_Class = TablePre&"Content_"&Pv_System&"_Class"
			Pv_ContentID  = ID
			Select Case Action
				Case "view" : ContentView
				Case "add","edit"
					If SubAction="save" Then
						ContentDoSave
					Else
						If Action = "add" Then
							CommentSet = 0
							Openness   = 0
							If ID>0 Then Call GetContentDetail()
							ID         = 0
							SysID      = KnifeCMS.CreateSysID
							If SeoUrl  = "" Then SeoUrl="index"
							Call ContentDo()
						ElseIf Action = "edit" Then
							If SubAction = "revert" Or SubAction = "public" Then
								ContentOtherEdit
							Else
								Call GetContentDetail()
								If IsContentExist Then Call ContentDo() : Else Admin.ShowMessage Lang_ContentCue(7),"goback",0
							End If
						End IF
					 End IF 
				Case "del"  : ContentDel
			End Select
		Else
			Admin.ShowMessage Lang_SubSystemCue(7),"goback",0
		End If
		Footer()
	End Sub
	
	Private Sub Class_Terminate()
		Set Pv_Dictionary = Nothing
		ClassObjectClose
	End Sub
	
	Private Sub GetContentDetail()
		Set Rs = KnifeCMS.DB.GetRecord(DBTable,Array("ID:"& Pv_ContentID),"")
		If Not(Rs.Eof) Then
			IsContentExist = True
			ClassID        = Rs("ClassID")
			Title          = Rs("Title")
			TitleFontColor = Rs("TitleFontColor")
			TitleFontType  = Rs("TitleFontType")
			Author         = Rs("Author")
			From           = Rs("From")
			PublicTime     = Rs("PublicTime")
			Thumbnail      = Rs("Thumbnail")
			SlidesFirstImg = Rs("SlidesFirstImg")
			SlidesImgs     = Rs("SlidesImgs")
			UseLinkUrl     = Rs("UseLinkUrl")
			LinkUrl        = Rs("LinkUrl")
			Tags           = Rs("Tags")
			Abstract       = Rs("Abstract")
			PaginationType = Rs("PaginationType")
			TextContent    = Rs("Content")
			Hits           = Rs("Hits")
			TopLine        = Rs("TopLine")
			OnTop          = Rs("OnTop")
			Hot            = Rs("Hot")
			Elite          = Rs("Elite")
			CommentSet     = Rs("CommentSet")
			Openness       = Rs("Openness")
			Password       = Rs("Password")
			ContentStatus  = Rs("Status")
			LinkVotes      = Rs("LinkVotes")
			LinkContents   = Rs("LinkContents")
			SeoTitle       = Rs("SeoTitle")
			SeoUrl         = Rs("SeoUrl")
			MetaKeywords   = Rs("MetaKeywords")
			MetaDescription= Rs("MetaDescription")
		Else
			IsContentExist = False
		End IF
		KnifeCMS.DB.CloseRs Rs
		If KnifeCMS.Data.IsNul(LinkUrl) Then LinkUrl="http://"
		
		'DataArrayDictionary.Add BV_ContentSubSystem,Fn_String
	End Sub
	
	Private Function ContentList()
		Dim Temp_Sql
		PageUrlPara = "acttype="& ActionType
		If ViewType = "recycle" Then
			Temp_Sql = "SELECT a.ID,a.Title,b.ClassName,a.Thumbnail,a.Hits,a.Comments,a.CommentSet,a.TopLine,a.OnTop,a.Hot,a.Elite,a.AddTime FROM ["&DBTable&"] a LEFT JOIN "&DBTable_Class&" b ON (b.ID=a.ClassID) WHERE a.Recycle=1"
			Temp_Sql = Temp_Sql &" ORDER BY a.ID DESC"
			Call Admin.DataList(Temp_Sql,20,12,Operation,"")
		Else
			ContentStatus = KnifeCMS.IIF(ViewType="drafts",0,1)
			Temp_Sql = "SELECT a.ID,a.Title,b.ClassName,a.Thumbnail,a.Hits,a.Comments,a.CommentSet,a.TopLine,a.OnTop,a.Hot,a.Elite,a.AddTime FROM ["&DBTable&"] a LEFT JOIN "&DBTable_Class&" b ON (b.ID=a.ClassID) WHERE a.Recycle=0 And Status="& ContentStatus &""
			If SubAction = "search" Then
				Admin.Search.ClassID = KnifeCMS.IIF(Admin.Search.ClassID="",0,Admin.Search.ClassID)
				If Admin.Search.ClassID > 0 Then Admin.GetChildClass DBTable_Class,Admin.Search.ClassID
				tempCondition =                 KnifeCMS.IIF(Admin.Search.ClassID  > 0 ," AND ClassID IN("&Admin.Search.ClassID&Admin.ChildClass&") "," ")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.KeyWords <>""," AND Title like '%"& Admin.Search.KeyWords &"%' "," ")
				Select Case DB_Type
				Case 0
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND AddTime>=#"& Admin.Search.StartDate &"# ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND AddTime<=#"& Admin.Search.EndDate &"# ","")
				Case 1
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND AddTime>='"& Admin.Search.StartDate &"' ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND AddTime<='"& Admin.Search.EndDate &"' ","")
				End Select
				
				PageUrlPara = PageUrlPara &"&classid="& Admin.Search.ClassID &"&startdate="& Server.URLEncode(Admin.Search.StartDate) &"&enddate="& Server.URLEncode(Admin.Search.EndDate) &"&keywords="& Server.URLEncode(Admin.Search.KeyWords) &""
				Temp_Sql = Temp_Sql & tempCondition
			End If
			Temp_Sql = Temp_Sql &" ORDER BY a.ID DESC"
			Operation="<span class=""sysBtn""><a href=""?ctl="&Ctl&"&act=edit&id={$ID}"" ><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
			Operation=Operation&"<span class=""sysBtn""><a href=""?ctl=content_"& Pv_System &"&act=add&id={$ID}"" ><b class=""icon icon_copy""></b>"& Lang_DoCopy &"</a></span>"
			Call Admin.DataList(Temp_Sql,20,12,Operation,PageUrlPara)
		End If
	End Function
	
	Private Function ContentDoSave()
		Server.ScriptTimeOut = 900
		'获取数据并处理数据
		Title               = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Title")),250)
		TitleFontColor      = KnifeCMS.Data.Color(KnifeCMS.GetForm("post","TitleFontColor"))
		TitleFontType       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","TitleFontType"))

		Author              = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Author")),50)
		From                = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","From")),250)
		PublicTime          = KnifeCMS.Data.FormatDateTime(KnifeCMS.GetForm("post","PublicTime"),0) : If KnifeCMS.Data.IsNul(PublicTime) Then PublicTime=SysTime
		
		LinkUrl             = KnifeCMS.Data.Left(KnifeCMS.Data.URLEncode(KnifeCMS.GetForm("post","LinkUrl")),250)
		UseLinkUrl          = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","UseLinkUrl"))
		Tags                = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Tags")),250)
		
		PaginationType      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","PaginationType"))'分页模式
		SaveRemotePic       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","SaveRemotePic"))'详细内容图片采集
		TextContent         = KnifeCMS.GetForm("post","TextContent")'详细内容
		
		'内容摘要
		GetAbstract         = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","GetAbstract"))
		Abstract            = KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(KnifeCMS.GetForm("post","Abstract")),500)
		If GetAbstract=1 Then Abstract=KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(TextContent),500)
		
		'图片
		Thumbnail           = KnifeCMS.Data.Left(KnifeCMS.Data.URLEncode(KnifeCMS.GetForm("post","Thumbnail")),250)
		UseFirstImg         = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","UseFirstImg"))
		SlidesFirstImg      = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","SlidesImg[]FirstImg"),250)
		SlidesImgs          = KnifeCMS.Data.Left(KnifeCMS.GetForm("post","SlidesImg[]"),2500)
		
		Dim Fn_Temp
		'On Error Resume Next
		for each Fn_Temp in Request.Form("LinkVotesID[]")
			Fn_Temp   = KnifeCMS.Data.CLng(Fn_Temp)
			If Fn_Temp>0 Then LinkVotes = KnifeCMS.IIF(KnifeCMS.Data.IsNul(LinkVotes),Fn_Temp,LinkVotes &","& Fn_Temp)
		next
		for each Fn_Temp in Request.Form("LinkContentsID[]")
			Fn_Temp   = KnifeCMS.Data.CLng(Fn_Temp)
			If Fn_Temp>0 Then LinkContents = KnifeCMS.IIF(KnifeCMS.Data.IsNul(LinkContents),Fn_Temp,LinkContents &","& Fn_Temp)
		next
		'If Err.Number<>0 Then Err.Clear
		
		Hits                = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Hits"))
		TopLine             = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","TopLine"))'头条
		OnTop               = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","OnTop"))'固顶
		Hot                 = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Hot"))'热门
		Elite               = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Elite"))'精华
		CommentSet          = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","CommentSet"))
		Openness            = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Openness"))'评论设置
		Password            = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","Password")),50)
		ContentStatus       = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Status"))'文章状态 已发布/草稿
		'数据合法性检测并处理数据
		LinkUrl  = KnifeCMS.IIF(UseLinkUrl=0,"",LinkUrl)
		Openness = KnifeCMS.IIF(Openness>2 Or Openness<0,0,Openness)
		If ClassID < 1                             Then ErrMsg = Lang_ContentCue(16) &"<br>"
		If KnifeCMS.Data.IsNul(Title)               Then ErrMsg = ErrMsg & Lang_ContentCue(17) &"<br>"
		If Openness = 2 And KnifeCMS.Data.IsNul(Password) Then ErrMsg = ErrMsg & Lang_ContentCue(18) &"<br>"
		If Openness = 2 And Len(Password) > 50     Then ErrMsg = ErrMsg & Lang_ContentCue(19) &"<br>"
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		
		If SaveRemotePic=1 Then
			Echo "<dl class=Collecting><dt>"& Lang_CollectCue(1) &"</dt><dd>" : Response.Flush()
			TextContent = KnifeCMS.Http.ReplaceRemoteUrl(TextContent,1,0,0,0)
			Echo "<br>"& Lang_CollectCue(2) &"</dd></dl>"
		End If
		'采用详细内容的第一张图片作为缩略图
		If UseFirstImg=1 Then Thumbnail=KnifeCMS.GetFirstImgURL(TextContent)

		'SEO优化内容
		SeoTitle            = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","SeoTitle")),250)        : If SeoTitle = "" Then SeoTitle=Title
		SeoUrl              = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","SeoUrl")),250)          : If SeoUrl = "" Then SeoUrl="index"
		MetaKeywords        = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","MetaKeywords")),250)    : If MetaKeywords = "" Then MetaKeywords=Tags
		MetaDescription     = KnifeCMS.Data.ParseMetaDescription(KnifeCMS.GetForm("post","MetaDescription"))
		If KnifeCMS.Data.IsNul(MetaDescription) Then MetaDescription=Abstract
		If KnifeCMS.Data.IsNul(MetaDescription) Then MetaDescription=KnifeCMS.Data.ParseMetaDescription(TextContent)
		
		Pv_PaginationLabel = Pv_PaginationLabel
		If PaginationType=0 Then
		'不分页
			TextContent = Replace(TextContent,Pv_PaginationLabel,"")
		ElseIf PaginationType=1 Then
		'手动分页
			
		ElseIf PaginationType=2 Then
		'系统自动分页
			TextContent = Replace(TextContent,Pv_PaginationLabel,"")
			Dim Fn_ContentLength : Fn_ContentLength=Len(TextContent)
			Dim Fn_Page_Size : Fn_Page_Size = 5000 '以多少个字符为一页
				
			If Fn_ContentLength>Fn_Page_Size Then
				Dim Fn_Page_i : Fn_Page_i = Int(Fn_ContentLength/Fn_Page_Size)
					If Fn_Page_i=(Fn_ContentLength/Fn_Page_Size) Then Fn_Page_i=Fn_Page_i-1
				Dim Fn_Page_j
				Dim Fn_Page_Temp
				For Fn_Page_j=0 To Fn_Page_i
					If KnifeCMS.Data.IsNul(Fn_Page_Temp) Then
						Fn_Page_Temp = KnifeCMS.Data.CutStr(TextContent,1,Fn_Page_Size)
					Else
						Fn_Page_Temp = Fn_Page_Temp & Pv_PaginationLabel & KnifeCMS.Data.CutStr(TextContent,Fn_Page_j*Fn_Page_Size+1,Fn_Page_Size)
					End If
				Next
				TextContent = Fn_Page_Temp
			End If
		End If
		
		TextContent = KnifeCMS.Data.EditorContentEncode(TextContent)
		
		'开始保存数据操作
		If Action="add" then
			Result = KnifeCMS.DB.AddRecord(DBTable,Array("SysID:"& SysID,"ClassID:"& ClassID,"Title:"& Title,"TitleFontColor:"& TitleFontColor,"TitleFontType:"& TitleFontType,"BriefTitle:"& BriefTitle,"BriefTitleFontColor:"& BriefTitleFontColor,"BriefTitleFontType:"& BriefTitleFontType,"Author:"& Author,"From:"& From,"PublicTime:"& PublicTime,"Thumbnail:"& Thumbnail,"UseLinkUrl:"& UseLinkUrl,"LinkUrl:"& LinkUrl,"Tags:"& Tags,"PaginationType:"& PaginationType,"Abstract:"& Abstract,"Content:"& TextContent,"Hits:"& Hits,"Comments:0","TopLine:"& TopLine,"OnTop:"& OnTop,"Hot:"& Hot,"Elite:"& Elite,"CommentSet:"& CommentSet,"Openness:"& Openness,"Password:"& KnifeCMS.IIF(int(Openness)=2,Password,""),"SlidesFirstImg:"& SlidesFirstImg,"SlidesImgs:"& SlidesImgs,"Status:"& ContentStatus,"AddTime:"& SysTime,"Diggs:0","LinkVotes:"& LinkVotes,"LinkContents:"& LinkContents,"SeoTitle:"& SeoTitle,"SeoUrl:"& SeoUrl,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription,"Recycle:0"))
			ID = Admin.GetIDBySysID(DBTable,SysID)
			'添加自定义字段数据
			If Result And ID>0 Then Call SaveFieldSetValue()
			
			'更新缓存
			If IsCache Then
				Print "<div class=""creating_cache"">"
				Call Admin.CreateCache_Init()
				Call Admin.CreateStaticHTML_ContentSystem(Pv_System ,False,ID,0)
				Call Admin.CreateCache_Terminate()
				Print "</div>"
			End If
			
			Msg    = Lang_ContentCue(1) &"{"& Title &"[ID="& ID &"]}"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		Else
			Result = KnifeCMS.DB.UpdateRecord(DBTable,Array("ClassID:"&ClassID,"Title:"&Title,"TitleFontColor:"&TitleFontColor,"TitleFontType:"&TitleFontType,"BriefTitle:"&BriefTitle,"BriefTitleFontColor:"&BriefTitleFontColor,"BriefTitleFontType:"&BriefTitleFontType,"Author:"&Author,"From:"&From,"PublicTime:"&PublicTime,"Thumbnail:"&Thumbnail,"UseLinkUrl:"&UseLinkUrl,"LinkUrl:"&LinkUrl,"Tags:"&Tags,"PaginationType:"&PaginationType,"Abstract:"&Abstract,"Content:"&TextContent,"Hits:"&Hits,"Comments:0","TopLine:"&TopLine,"OnTop:"&OnTop,"Hot:"&Hot,"Elite:"&Elite,"CommentSet:"&CommentSet,"Openness:"&Openness,"Password:"&KnifeCMS.IIF(int(Openness)=2,Password,""),"SlidesFirstImg:"&SlidesFirstImg,"SlidesImgs:"&SlidesImgs,"Status:"&ContentStatus,"EditTime:"&SysTime,"LinkVotes:"&LinkVotes,"LinkContents:"&LinkContents,"SeoTitle:"& SeoTitle,"SeoUrl:"& SeoUrl,"MetaKeywords:"& MetaKeywords,"MetaDescription:"& MetaDescription),Array("ID:"&ID))
			'更新自定义字段数据
			If Result And ID>0 Then Call SaveFieldSetValue()
			
			'更新缓存
			If IsCache Then
				Print "<div class=""creating_cache"">"
				Call Admin.CreateCache_Init()
				Call Admin.CreateStaticHTML_ContentSystem(Pv_System ,False,ID,0)
				Call Admin.CreateCache_Terminate()
				Print "</div>"
			End If
			
			Msg    = Lang_ContentCue(2) &"{"& Title &"[ID="& ID &"]}"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		End If
	End Function
	
	'保存自定义表单值
	Public Function SaveFieldSetValue()
		Dim Fn_Rs
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_FieldSet &"] WHERE System='"& Pv_System &"' AND Recycle=0 ORDER BY OrderNum ASC")
	    Do While Not(Fn_Rs.Eof)
			Pv_Field      = Fn_Rs("Field")
			Pv_FieldType  = Fn_Rs("FieldType")
			Pv_FieldValue = Admin.ParseFieldSetValue(Pv_FieldType,Pv_Field)
			Call KnifeCMS.DB.UpdateRecord(DBTable,Array(Pv_Field &":"& Pv_FieldValue),Array("ID:"&ID))
			Fn_Rs.MoveNext
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
	End Function
	
	'**插入关联内容
	Private Sub AddLinkItems(ByVal DB_Table, ByVal BV_ID, ByVal BV_LinkID)
		If Not(KnifeCMS.Data.IsNul(BV_LinkID)) and BV_ID > 0 Then
			C_TempArr = Split(BV_LinkID,",")
			For i=0 to ubound(C_TempArr)
				C_TempID = KnifeCMS.Data.CLng(C_TempArr(i))
				If C_TempID > 0 Then Call KnifeCMS.DB.AddRecord(DB_Table,Array("ContentID:"&ID,"LinkID:"&C_TempID))
			Next
		End If
	End Sub
	
	Private Function GetLinkVotes(ByVal BV_ID)
		BV_ID = KnifeCMS.Data.ParseID(BV_ID)
		If KnifeCMS.Data.IsNul(BV_ID) Then Exit Function
		C_TempHtml = ""
		Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,VoteTitle FROM ["& DBTable_Vote &"] WHERE ID IN("& BV_ID &")")
		Do While Not(Rs.Eof)
			C_TempHtml = C_TempHtml &"<tr class=""data""><td><a onclick=""DeleteCurrentRow(this)"" href=""#""><b class=""icon icon_del""></b></a></td><td><input type=""hidden"" name=""LinkVotesID[]"" value="""&Rs(0)&""">"&Rs(0)&"</td><td>"&Rs(1)&"</td></tr>"
			Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs(Rs)
		Echo C_TempHtml
	End Function
	
	Private Function GetLinkContents(ByVal BV_ID)
		BV_ID = KnifeCMS.Data.ParseID(BV_ID)
		If KnifeCMS.Data.IsNul(BV_ID) Then Exit Function
		C_TempHtml = ""
		Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,Title FROM ["&DBTable&"] WHERE ID IN("& BV_ID &")")
		Do While Not(Rs.Eof)
			C_TempHtml = C_TempHtml &"<tr class=""data""><td><a onclick=""DeleteCurrentRow(this)"" href=""#""><b class=""icon icon_del""></b></a></td><td><input type=""hidden"" name=""LinkContentsID[]"" value="""&Rs(0)&""">"&Rs(0)&"</td><td>"&Rs(1)&"</td></tr>"
			Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs(Rs)
		Echo C_TempHtml
	End Function
	
	Private Function ContentDel()
		ContentSubDo
	End Function
	
	Private Function ContentOtherEdit()
		ContentSubDo
	End Function
	
	Private Sub ContentSubDo()
		ID = KnifeCMS.GetForm("post","InputName")
		Dim ArrID : ArrID = Split(ID,",")
		For i=0 To Ubound(ArrID)
			ID = KnifeCMS.Data.Int(ArrID(i))
			Result    = 0
			If ID > 0 Then
				Set Rs = KnifeCMS.DB.GetRecord(DBTable&":Title",Array("ID:"&ID),"")
				If Not(Rs.Eof) Then Title=Rs(0)
				KnifeCMS.DB.CloseRs Rs
				'开始执行操作
				If Action = "del" Then
					If SubAction = "completely" Then
						Result = KnifeCMS.DB.DeleteRecord(DBTable,Array("ID:"&ID))
						Msg    = Msg & Lang_ContentCue(4) &"{"& Title &"[ID="& ID &"]}<br>"
					Else
						Result = KnifeCMS.DB.UpdateRecord(DBTable,Array("Recycle:1"),Array("ID:"&ID))
						Msg    = Msg & Lang_ContentCue(3) &"{"& Title &"[ID="& ID &"]}<br>"
					End If
				ElseIf Action = "edit" Then
					If SubAction = "public" Then
						Result = KnifeCMS.DB.UpdateRecord(DBTable,Array("Status:1"),Array("ID:"&ID))
						Msg    = Msg & Lang_ContentCue(5) &"{"& Title &"[ID="& ID &"]}<br>"
					ElseIf SubAction = "revert" Then
						Result = KnifeCMS.DB.UpdateRecord(DBTable,Array("Recycle:0"),Array("ID:"&ID))
						Msg    = Msg & Lang_ContentCue(6) &"{"& Title &"[ID="& ID &"]}<br>"
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Sub
	
	Private Function ContentView()
	%>
	<%=Admin.Calendar(Array("","",""))%>
    <DIV id="MainFrame">
	  <div class="tagLine"><%=NavigationLine%></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <% If ViewType = "recycle" Then%>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><%=Lang_DistrictDoEdit%><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:revert','')"><b class="icon icon_revert"></b><%=Lang_DoRevert%></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><%=Lang_DistrictDoDel%><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<%=Lang_DoDelConfirm%>')"><b class="icon icon_del"></b><%=Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  <% Else%>
		  <% If ViewType = "drafts" Then%>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><%=Lang_DistrictDoAdd%><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <span class="sysBtn"><a href="?ctl=content_<%=Pv_System%>&act=add" ><b class="icon icon_write"></b><%=Lang_DoAdd%></a></span>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><%=Lang_DistrictDoEdit%><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:public','')"><b class="icon icon_import"></b><%=Lang_DoPublic%></button><!--发布-->
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><%=Lang_DistrictDoDel%><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','')"><b class="icon icon_recycle"></b><%=Lang_DoDel%></button>
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<%=Lang_DoDelConfirm%>')"><b class="icon icon_del"></b><%=Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  <% Else%>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><%=Lang_DistrictDoAdd%><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <span class="sysBtn"><a href="?ctl=<%=Ctl%>&act=add" ><b class="icon icon_write"></b><%=Lang_DoAdd%></a></span>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><%=Lang_DistrictDoDel%><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','')"><b class="icon icon_recycle"></b><%=Lang_DoDel%></button>
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<%=Lang_DoDelConfirm%>')"><b class="icon icon_del"></b><%=Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			<table border="0" cellpadding="2" cellspacing="0">
			<tr><td class="upcell">
				<div class="align-center grey">
					<%=Lang_DistrictDoSearch%><!--搜索操作区-->
					<span class="AdvancedSearch"><a href="javascript:Admin.AdvancedSearch.Open(600,280,'AdvancedSearch')" ><%=Lang_AdvancedSearch%></a></span>
				</div>
				<DIV id="AdvancedSearch" style="display:none;">
					<form name="SerachForm" action="?ctl=<%=Ctl%>&act=<%=Action%>&subact=search&acttype=<%=ActionType%>" method="post" style="margin:0px">
					<div class="diainbox">
					  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
					  <tr><td class="titletd"><%=Lang_Keywords%></td>
						  <td class="infotd"><input type="text" class="TxtClass" id="keywords" name="keywords" value="<%=Admin.Search.KeyWords%>" style="width:280px;" /></td>
					  </tr>
					  <tr><td class="titletd"><%=Lang_ClassSelect%></td>
								<td class="infotd"><select id="classid" name="classid"><%=Admin.ShowClass(1,Admin.Search.ClassID,DBTable_Class)%></select></td>
					  </tr>
					  <tr><td class="titletd"><%=Lang_StartDate%></td>
						  <td class="infotd">
                          <input type="text" class="TxtSer dateStart" id="startdate" name="startdate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:200px;" value="<%=Admin.Search.StartDate%>" />
                           
                          </td>
					  <tr><td class="titletd"><%=Lang_EndDate%></td>
						  <td class="infotd">
                          <input type="text" class="TxtSer dateEnd" id="enddate" name="enddate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:200px;" value="<%=Admin.Search.EndDate%>" />
                          
                          </td>
					  </tr>
					  </table>
					  <%=AdvancedSearchBtnline%>
                    </div>
					</form>
				</DIV>
				</td></tr>
			<tr><td class="downcell">
				<form name="SerachForm" action="?ctl=<%=Ctl%>&act=<%=Action%>&subact=search&acttype=<%=ActionType%>" method="post" style="margin:0px">
				<table border="0" cellpadding="0" cellspacing="0"><tr>
				<td><span><%=Lang_Keywords%>:</span></td>
				<td class="pl3"><input type="text" class="TxtClass" name="keywords" value="<%=Admin.Search.KeyWords%>" style="width:220px;" /></td>
				<td class="pl3"><button type="submit" class="sysBtn" ><b class="icon icon_search"></b><%=Lang_Btn_Search%></button></td>
				</tr></table>
				</form>
			</td></tr>
			</table>
		  </td>
		  <% End If%>
		  <% End If%>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<%=Ctl%>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<%=Lang_DoSelectAll%>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th width="1%"><div style="width:50px;"><%=Lang_ListTableColLine_ID%></div></th><!--系统ID-->
			<th width="30%"><div style="width:150px;"><%=Lang_Content(2)%></div></th><!--标题-->
			<th width="10%"><div style="width:60px;"><%=Lang_Content(1)%></div></th><!--所属分类-->
			<th width="5%"><div style="width:50px;"><%=Lang_Content(7)%></div></th><!--缩略图-->
			<th width="5%"><div style="width:40px;"><%=Lang_Content(14)%></div></th><!--浏览量-->
			<th width="5%"><div style="width:40px;"><%=Lang_ListTableColLine_Comments%></div></th><!--评论数-->
			<th width="5%"><div style="width:56px;"><%=Lang_ListTableColLine_CommentSet%></div></th><!--评论设置-->
            <th width="5%"><div style="width:28px;"><%=Lang_Content(18)%></div></th><!--头条-->
			<th width="5%"><div style="width:28px;"><%=Lang_Content(19)%></div></th><!--固顶-->
            <th width="5%"><div style="width:28px;"><%=Lang_Content(20)%></div></th><!--热门-->
            <th width="5%"><div style="width:28px;"><%=Lang_Content(21)%></div></th><!--精华-->
			<th width="15%"><div style="width:75px;"><%=Lang_ListTableColLine_AddTime%></div></th><!--添加时间-->
			<th><div style="width:60px;"><%=Lang_ListTableColLine_Do%></div></th><!--操作-->
			</tr>
			<% ContentList%>
		   </tbody>
		  </table>
		  <% If ActionType="getitems" Then Echo GetSelectItemsBtnLine%>
		  </form>
		</div>
		</DIV>
	</DIV>
	<script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	if(objExist(document.getElementById("keywords"))){ReplaceText(document.getElementById("keywords").value,1)}
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var content,TempHTML,ContentID,url;
		var subsystem  = "<%=Pv_System%>";
		var img,fn_id,fn_label;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td0"){
					TempHTML = '<a href="content.asp?ctl=content_<%=Pv_System%>&act=edit&id='+tds[i].innerHTML+'" title="<%=Lang_DoEdit%>">'+tds[i].innerHTML+'</a>';
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td1"){
					TempHTML = '<a href="<%=SystemPath%>?content/<%=Pv_System%>/'+tds[i-1].title+'/index<%=FileSuffix%>" title="<%=Lang_DoView%>" target="_blank">'+tds[i].innerHTML+'</a>';
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td3" && tds[i].innerHTML!=""){
					TempHTML = '<span class="sImg"><a href="<%=SystemPath%>?content/<%=Pv_System%>/'+tds[i-3].title+'/index<%=FileSuffix%>" title="<%=Lang_DoView%>" target="_blank"><img src="'+ unescape(tds[i].innerHTML) +'" /></a></span>';
					tds[i].innerHTML=TempHTML;
				}
				if(tds[i].id=="td6" && tds[i].innerHTML==0){
					tds[i].innerHTML=Lang_Js_Content[0];
				}
				if(tds[i].id=="td6" && tds[i].innerHTML==1){
					tds[i].innerHTML=Lang_Js_Content[1];
				}
				if(tds[i].id=="td6" && tds[i].innerHTML==2){
					tds[i].innerHTML=Lang_Js_Content[2];
				}
				if(tds[i].id=="td7" || tds[i].id=="td8" || tds[i].id=="td9" || tds[i].id=="td10"){
					if(tds[i].id=="td7"){
						fn_label = "topline";
						fn_id    = tds[i-7].title;
					}else if(tds[i].id=="td8"){
						fn_label = "ontop";
						fn_id    = tds[i-8].title;
					}else if(tds[i].id=="td9"){
						fn_label = "hot";
						fn_id    = tds[i-9].title;
					}else if(tds[i].id=="td10"){
						fn_label = "elite";
						fn_id    = tds[i-10].title;
					}
					img = document.createElement("img");
					img.setAttribute("class","pointer");
					if(parseInt(tds[i].innerHTML)==1){
						img.src="image/icon_yes.png";
						img.setAttribute("value",1);
					}else{
						img.src="image/icon_yes_gray.png";
						img.setAttribute("value",0);
					}
					img.setAttribute("id",fn_id);
					img.setAttribute("label",fn_label);
					img.onclick=function(){Admin.Content.SetContentLabel(this,subsystem)}
					tds[i].title="";
					tds[i].innerHTML="";
					tds[i].appendChild(img);
				}
			}
		}
	}
	ChangeTdText();
	</script>
	<%
	End Function
	Private Function ContentDo()
	Dim Fn_From_quot_exist   : Fn_From_quot_exist   = False
	Dim Fn_Author_quot_exist : Fn_Author_quot_exist = False
	If Instr(KnifeCMS.Data.HTMLDecode(From),"""")>0 Then Fn_From_quot_exist=True
	If Instr(KnifeCMS.Data.HTMLDecode(Author),"""")>0 Then Fn_Author_quot_exist=True
	%>
    <%
	Dim Fn_Html,Fn_EditorHtml,Fn_Temp,Fn_Temp2
	Dim Fn_Required,Fn_AttrNotNull
	
	Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_FieldSet &"] WHERE System='"& Pv_System &"' AND Recycle<>1 ORDER BY OrderNum ASC")
	Do While Not(Rs.Eof)
		
		Pv_Field       = KnifeCMS.Data.ParseRs(Rs("Field"))
		Pv_FieldName   = KnifeCMS.Data.ParseRs(Rs("FieldName"))
		Pv_FieldType   = KnifeCMS.Data.ParseRs(Rs("FieldType"))
		Pv_FieldOptions= KnifeCMS.Data.ParseRs(Rs("FieldOptions"))
		Pv_FieldTips   = KnifeCMS.Data.ParseRs(Rs("FieldTips"))
		
		Pv_FieldValue  = ""
		If Pv_ContentID>0 Then Pv_FieldValue = KnifeCMS.Data.ParseRs(KnifeCMS.DB.GetFieldByID(DBTable,Pv_Field,Pv_ContentID))
		
		If KnifeCMS.Data.CLng(Rs("NotNull"))>0 Then
			Fn_Required    = "<span class=""required"">*</span>"
			Fn_AttrNotNull = "notnull=""true"""
		Else
			Fn_Required    = ""
			Fn_AttrNotNull = "notnull=""false"""
		End If
		
		Select Case Pv_FieldType
		Case "text"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><input type=""text"" fieldset=1 class=""TxtClass long"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" maxlength=""250"" value="""& Pv_FieldValue &""" /><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
		Case "textarea"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><textarea type=""text"" fieldset=1 class=""info"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" maxlength=""250"">"& KnifeCMS.Data.TextareaDecode(Pv_FieldValue) &"</textarea><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
			
		Case "editor"
			Fn_EditorHtml = Fn_EditorHtml &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><textarea type=""TextContent"" fieldset=1 class=""info"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &">"& KnifeCMS.Data.TextareaEditorContentDecode(Pv_FieldValue) &"</textarea><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label><script type=""text/javascript"">var "& Pv_Field &"_Editor=UE.getEditor('"& Pv_Field &"');</script></td></tr>"
			
		Case "radio"
			Pv_Array = Split(Pv_FieldOptions,"|")
			Fn_Temp  = ""
			For Pv_ii=0 To Ubound(Pv_Array)
				Fn_Temp = Fn_Temp &"<input type=""radio"" fieldset=1 name="""& Pv_Field &""" "& Fn_AttrNotNull &" "& Admin.Input_Checked(Pv_Array(Pv_ii),Pv_FieldValue) &" value="""& Pv_Array(Pv_ii) &""">"& Pv_Array(Pv_ii)
			Next
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd"">"& Fn_Temp &"<label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
			
		Case "checkbox"
			Pv_Array  = Split(Pv_FieldOptions,"|")
			Pv_Array2 = Split(Pv_FieldValue,"|")
			Fn_Temp   = ""
			For Pv_ii=0 To Ubound(Pv_Array)
				Fn_Temp2 = ""
				For Pv_jj=0 To Ubound(Pv_Array2)
					If Pv_Array2(Pv_jj)=Pv_Array(Pv_ii) Then
						Fn_Temp2 = "checked=""checked="""
					End If
				Next
				Fn_Temp = Fn_Temp &"<input type=""checkbox"" fieldset=1 name="""& Pv_Field &""" "& Fn_AttrNotNull &" "& Fn_Temp2 &" value="""& Pv_Array(Pv_ii) &""">"& Pv_Array(Pv_ii)
			Next
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd"">"& Fn_Temp &"<label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
			
		Case "select"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><select fieldset=1 id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &"><option value="""">"& Lang_PleaseSelect &"</option>"& OS.CreateOption(Split(Pv_FieldOptions,"|"),Pv_FieldValue) &"</select><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
		Case "number"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><input type=""text"" fieldset=1 class=""TxtClass slong"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" maxlength=""250"" value="""& Pv_FieldValue &""" onblur=""KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g'))"" /><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
			
		Case "datetime"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><input type=""text"" fieldset=1 class=""dateTime slong"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" maxlength=""250"" value="""& Pv_FieldValue &""" />"
			Fn_Html = Fn_Html & Admin.Calendar(Array(Pv_Field,Pv_Field,"%Y-%m-%d %H:%M:%S"))
			Fn_Html = Fn_Html &"<label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
			
		Case "image"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd"">"
			Fn_Html = Fn_Html &"<div class=""image_grid""><table border=""0"" cellpadding=""0"" cellspacing=""0""><tr><td valign=""top"" style=""padding:0px;""><div class=""imageBox"" id="""& Pv_Field &"Box"">"

			If Not(KnifeCMS.Data.IsNul(Pv_FieldValue)) Then
				Fn_Html = Fn_Html &"<img class=""img"" src="""& Trim(KnifeCMS.Data.URLDecode(Pv_FieldValue))&""" />"
			Else
				Fn_Html = Fn_Html &"<img class=""img"" src=""image/image.jpg"" />"
			End If
            Fn_Html = Fn_Html &"</div></td><td valign=""top"" style=""padding:0px 0px 0px 6px;""><input type=""text"" fieldset=1 class=""TxtClass slong"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" onblur=""Admin.ChangeImageUrl('"& Pv_Field &"Box','"& Pv_Field &"','img','d_"& Pv_Field &"')"" style=""width:314px; border:1px dashed #ccc;"" maxlength=""250"" value="""& KnifeCMS.Data.URLDecode(Pv_FieldValue) &""" /><div style=""padding:5px 0px 0px 0px;""><span class=""sysBtn""><a onclick=""UploadImg('1','"& Pv_Field &"');Admin.ImageUrlChange('"& Pv_Field &"Box','"& Pv_Field &"','','d_"& Pv_Field &"')"" href=""javascript:void(0)""><b class=""icon icon_add""></b>"& Lang_Btn_SelectPics &"</a></span></div><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr></table></div>"
			Fn_Html = Fn_Html &"</td></tr>"
			
		Case "file"
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><input type=""text"" fieldset=1 class=""TxtClass long"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" maxlength=""250"" value="""& Pv_FieldValue &""" /><span class=""sysBtn""><a onclick=""UploadFile('1','"& Pv_Field &"');Admin.ImageUrlChange('"& Pv_Field &"Box','"& Pv_Field &"','','d_"& Pv_Field &"')"" href=""javascript:void(0)""><b class=""icon icon_add""></b>"& Lang_Btn_SelectFile &"</a></span><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
		Case Else
			Fn_Html = Fn_Html &"<tr><td class=""titletd"">"& Fn_Required & Pv_FieldName &"</td><td class=""infotd""><input type=""text"" fieldset=1 class=""TxtClass long"" id="""& Pv_Field &""" name="""& Pv_Field &""" "& Fn_AttrNotNull &" maxlength=""250"" value="""& Pv_FieldValue &""" /><label class=""Normal pl5"" id=""d_"& Pv_Field &""">"& Pv_FieldTips &"</label></td></tr>"
		End Select
	Rs.MoveNext
	Loop
	KnifeCMS.DB.CloseRs Rs
	%>
    <style type="text/css">
	.infoTable .titletd{ width:120px;}
    </style>
    <script language="javascript" type="text/javascript" src="../include/javascript/colorpicker.js"></script>
    <script type="text/javascript">
    function set_title_color(color) {
		$('#Title').css('color',color);
		$('#TitleFontColor').val(color);
	}
	window.UEDITOR_HOME_URL = "<%=SystemPath%>plugins/ueditor/";
	</script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_config.asp"></script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_api.js"></script>
	<DIV id="MainFrame">
	  <div class="tagLine"><%=NavigationLine%></div>
	  <DIV class="cbody">
		<div class="wbox" style="background:#fff;">
		<form name="SaveForm" id="SaveForm" action="?ctl=<%=Ctl%>&act=<%=Action%>&subact=save" method="post" onsubmit="return Admin.Content.SaveContentCheck(1)">
		<ul id="ContentTabs" class="TabsStyle" style="display:;">
		   <li class="current" onclick=TabSwitch("MainFrame","li","Tabs",1)><%=Lang_ContentNav(1)%></li>
           <li onclick=TabSwitch("MainFrame","li","Tabs",2)><%=Lang_ContentNav(2)%></li>
		   <li onclick=TabSwitch("MainFrame","li","Tabs",3)><%=Lang_ContentNav(5)%></li>
		</ul>
		<div id="Tabs_1" class="inbox">
            <div>
              <table border="0" cellpadding="0" cellspacing="0">
                <tbody>
                  <tr><td style="width:658px;" valign="top">
                      <div class="">
                       <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                       <tbody>
                       <tr><td class="titletd"><span class="required">*</span><%=Lang_Content(1)%></td>
                           <td class="infotd">
                               <select id="ClassID" name="ClassID"  style="font-size:13px;">
                               <%=Admin.ShowClass(0,ClassID,DBTable_Class)%>
                               </select>
                               <label class="Normal" id="d_ClassID"></label>
                           </td>
                       </tr>
                       <tr><td class="titletd"><span class="required">*</span><%=Lang_Content(2)%></td>
                           <td class="infotd">
                           <input type="text" class="TxtClass long" id="Title" name="Title" onblur="Admin.AutoSeoTitle(KnifeCMS.$id('Title'),KnifeCMS.$id('TempTitle'),KnifeCMS.$id('SeoTitle'));" maxlength="250" value="<%=KnifeCMS.Data.HTMLDecode(Title)%>" /><input type="hidden" id="TitleFontColor" name="TitleFontColor" value="<%=TitleFontColor%>" />
                           <input type="hidden" id="TempTitle" name="TempTitle" value="<%=KnifeCMS.Data.HTMLDecode(Title)%>"/>
                           <span class="colorpicker" onclick="colorpicker('colorpanel_title','set_title_color');" title="<%=Lang_TitleColor%>"></span>
                           <span class="colorpanel" id="colorpanel_title" style="position:absolute;z-index:99999999;"></span>
                           <script type="text/javascript">
						   set_title_color("<%=TitleFontColor%>");
                           </script>
                           <div style="padding-top:0px;">
                           <label class="Normal" id="d_Title"></label>
                           </div>
                           </td>
                       </tr>
                       </tbody>
                      </table>
                      </div>
                      
                      <!--自定义字段-->
					  <%
                      If Fn_Html<>"" Then
                      %>
                      <div class="pt5">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                      <tbody>
                      <%=Fn_Html%>
                      </tbody>
                      </table>
                      </div>
                      <%
                      End If
                      %>
            
                      <div class="pt5">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                      <tbody>
                      <tr><td class="titletd"><%=Lang_Content(7)%></td>
                          <td class="infotd">
                          <div class="image_grid">
                              <table border="0" cellpadding="0" cellspacing="0"><tr>
                              <td valign="top" style="padding:0px;">
                                  <div class="imageBox" id="ThumbnailBox">
                                  <%
								  If Not(KnifeCMS.Data.IsNul(Thumbnail)) Then
									  Echo "<img class=""img"" src="""& Trim(KnifeCMS.Data.URLDecode(Thumbnail))&""" />"
                                  Else
									  Echo "<img class=""img"" src=""image/image.jpg"" />"
                                  End If
								  %>
                                  </div>
                              </td>
                              <td valign="top" style="padding:0px 0px 0px 6px;">
                                  <input type="text" class="TxtClass slong" id="Thumbnail" name="Thumbnail" onblur="Admin.ChangeImageUrl('ThumbnailBox','Thumbnail','img','d_Thumbnail')" style="width:314px; border:1px dashed #ccc;" value="<%=KnifeCMS.Data.URLDecode(Thumbnail)%>"/>
                                  <div style="padding:5px 0px 0px 0px;">
                                  <span class="sysBtn" id="UploadImage"><a onclick="UploadImg('1','Thumbnail');Admin.ImageUrlChange('ThumbnailBox','Thumbnail','','d_Thumbnail')" href="javascript:void(0)"><b class="icon icon_add"></b><%=Lang_Btn_SelectPics%></a></span>
                                  <input type="checkbox" id="UseFirstImg" name="UseFirstImg" onclick='SetInputDisabled(1,"UseFirstImg","Thumbnail,UploadImage")' value="1"/><font class="explain"><%=Lang_ContentCue(10)%></font>
                                  </div>
                                  <label class="Normal" id="d_Thumbnail"></label>
                              </td>
                              </tr></table>
                          </div>
                          </td>
                      </tr>
                      <tr><td class="titletd" valign="top"><%=Lang_Content(10)%></td>
                          <td class="infotd">
                          <textarea class="abstract" id="Abstract" name="Abstract"><%=KnifeCMS.Data.TextareaDecode(Abstract)%></textarea>
                          <div style="background:#F9F9F9; border:1px solid #C0C0C0; width:398px; padding:1px 5px 1px 2px;"><input type="checkbox" id="GetAbstract" name="GetAbstract" onclick='if($(this).attr("checked")==true){$("#Abstract").hide("fast");}else{$("#Abstract").show("fast");}' value="1"/><font class="explain"><%=Lang_ContentCue(13)%></font></div>
                          </td>
                      </tr>
                      </tbody>
                      </table>
                      </div>
                      
                      </td>
                      <td valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody><tr>
                          <td style="width:260px;" valign="top">
                            <div class="uploadimgArea" style="margin-left:8px;">
                               <div class="top">
                                 <span class="fr sysBtn" style="margin:-3px -6px 0px 0px;"><a onclick="AddImgs('imgslist','SlidesImg[]')" href="#"><b class="icon icon_add"></b><%=Lang_Btn_AddPics%></a></span>
                                 <b><%=Lang_Content(30)%></b>
                               </div>
                               <div id="bigimg" class="bigimg"><table border="0" cellpadding="0" cellspacing="0"><tr><td><% If not(KnifeCMS.Data.IsNul(SlidesImgs)) Then Echo "<input type=""hidden"" name=""SlidesImg[]FirstImg"" value="""&SlidesFirstImg&""" /><img src="""&SlidesFirstImg&""">"%></td></tr></table></div>
                               <ul class="imgslist" id="imgslist">
                               <% If not(KnifeCMS.Data.IsNul(SlidesImgs)) Then
                                     SlidesImgs=Split(SlidesImgs,",")
                                     For i=0 to Ubound(SlidesImgs)
                                     TempImg=Trim(SlidesImgs(i))
                                     Echo "<li title="& Lang_Js_SetAsCover &"><input type=""hidden"" name=""SlidesImg[]"" value="""&TempImg&""" /><table border=""0"" cellpadding=""0"" cellspacing=""0""><tbody><tr><td class=""imgdiv""><a href=""javascript:;"" target=""_self""><img src="""&TempImg&"""></a></td></tr></tbody></table><p><a onclick=""deleteCurrentPic(this)"" href=""javascript:;"" target=""_self""><b class=""icon icon_del"" title="""& Lang_DelThisImage &"""></b></a><a onclick=""DiaWindowOpen_GetImgURL(this)"" title="""&TempImg&""" href=""javascript:;"" target=""_self""><b class=""icon icon_imglink"" title="""& Lang_GetImageUrl &"""></b></a></p></li>"
                                     Next
                                  End If
                               %>
                               </ul>
                            </div>
                          </td></tr>
                        </tbody>
                        </table>
                      </td>
                  </tr>
                </tbody>
              </table>
            </div>
            
            <!--编辑器-->
            <div class="pt5">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
            <tbody>
            <tr><td class="titletd"><%=Lang_Content(13)%></td><td class="infotd">
                <textarea class="TextContent" id="TextContent" name="TextContent"><%=KnifeCMS.Data.TextareaEditorContentDecode(TextContent)%></textarea>
                <label class="Normal" id="d_TextContent"></label>
                <script type="text/javascript">var OSWebEditor=UE.getEditor('TextContent');</script>
                </td>
            </tr>
            <tr><td class="titletd"><%=Lang_Content(11)%></td>
                <td class="infotd">
                <% Call SelectPaginationType("PaginationType",PaginationType)%>
                <label class="Normal"><%=Lang_ContentCue(14)%></label>
                </td>
            </tr>
            <tr><td class="titletd"><%=Lang_Content(12)%></td>
                <td class="infotd"><input type="checkbox" name="SaveRemotePic" value="1"/><%=Lang_ContentCue(15)%>
                   <span><a href="javascript:void(0);" onclick="ShowHelpMsg(400,160,'<%=Lang_Content(12)%>',Js_Content_HelpMsg[3])"><b class="icon icon_help"></b></a></span>
                </td>
            </tr>
            </table>
            </div>
            <!--自定义字段:编辑器-->
            <%
            If Fn_EditorHtml<>"" Then
            %>
            <div class="pt5">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
            <tbody>
            <%=Fn_EditorHtml%>
            </tbody>
            </table>
            </div>
            <%
            End If
            %>
		</div>
        
        <div id="Tabs_2" class="inbox" style="display:none;">
            <div>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tbody>
                   <tr><td class="titletd"><%=Lang_Content(9)%></td>
                       <td class="infotd">
                           <input type="text" class="TxtClass long" id="Tags" name="Tags" maxlength="250" value="<%=Tags%>"/>
                           <a href="javascript:void(0);" onclick="ShowHelpMsg(400,100,'<%=Lang_Content(9)%>',Js_Content_HelpMsg[2])"><b class="icon icon_help"></b></a>
                           <label class="Normal"><%=Lang_ContentCue(12)%></label>
                       </td>
                   </tr>
                   <tr><td class="titletd"><%=Lang_Content(4)%></td>
                       <td class="infotd">
                       <input type="text" class="TxtClass slong" id="Author" name="Author" maxlength="50" value=<%=KnifeCMS.IIF(Fn_Author_quot_exist,"'"& KnifeCMS.Data.HTMLDecode(Author) &"'",""""& KnifeCMS.Data.HTMLDecode(Author) &"""")%> />
                       <label class="Normal" id="d_Author"></label>
                       </td>
                   </tr>
                   <tr><td class="titletd"><%=Lang_Content(5)%></td>
                       <td class="infotd">
                       <input type="text" class="TxtClass long" id="From" name="From" maxlength="250" value=<%=KnifeCMS.IIF(Fn_From_quot_exist,"'"& KnifeCMS.Data.HTMLDecode(From) &"'",""""& KnifeCMS.Data.HTMLDecode(From) &"""")%> />
                       <label class="Normal" id="d_From"></label>
                       </td>
                   </tr>
                   <tr><td class="titletd"><%=Lang_Content(6)%></td>
                       <td class="infotd">
                       <input type="text" class="dateTime slong" id="PublicTime" name="PublicTime" value="<%=PublicTime%>" />
                       <label class="Normal" id="d_PublicTime"></label>
                       <%=Admin.Calendar(Array("PublicTime","PublicTime","%Y-%m-%d %H:%M:%S"))%>
                   </td></tr>
                   <tr><td class="titletd"><%=Lang_Content(8)%></td>
                       <td class="infotd"><input type="text" class="TxtClass slong" id="LinkUrl" name="LinkUrl" maxlength="250" <% If UseLinkUrl<>1 Then%>disabled="disabled"<% End If%> value="<%=KnifeCMS.Data.URLDecode(LinkUrl)%>"/>
                           <input type="checkbox" id="UseLinkUrl" name="UseLinkUrl" onclick='SetInputDisabled(0,"UseLinkUrl","LinkUrl")' <%=Admin.InputChecked(UseLinkUrl)%> value="1"/><font class="explain"><%=Lang_ContentCue(11)%></font> 
                           <span><a href="javascript:void(0);" onclick="ShowHelpMsg(400,100,'<%=Lang_Content(8)%>',Js_Content_HelpMsg[1])"><b class="icon icon_help"></b></a></span>
                           <label class="Normal" id="d_LinkUrl"></label>
                       </td>
                   </tr>
                </tbody>
                </table>
            </div>
            <!--系统默认-->
            <div class="pt5">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                 <tbody>
                 
                 <tr><td class="titletd"><%=Lang_Content(15)%></td>
                     <td class="infotd">
                     <input id="TopLine" name="TopLine" type="checkbox" <%=Admin.InputChecked(TopLine)%> value="1"><%=Lang_Content(18)%><!--头条-->
                     <input id="OnTop"  name="OnTop" type="checkbox" <%=Admin.InputChecked(OnTop)%> value="1"><%=Lang_Content(19)%><!--固顶-->
                     <input id="Hot" name="Hot" type="checkbox" <%=Admin.InputChecked(Hot)%> value="1"><%=Lang_Content(20)%><!--热门-->
                     <input id="Elite"  name="Elite" type="checkbox" <%=Admin.InputChecked(Elite)%> value="1"><%=Lang_Content(21)%><!--精华-->
                     <label class="Normal"></label>
                     </td>
                 </tr>
                 <% If ContentCommentSet=0 Then%>
                 <tr><td class="titletd"><%=Lang_Content(16)%></td>
                     <td class="infotd">
                     <input type="radio" value="0" name="CommentSet" <%=Admin.Input_Checked(CommentSet,0)%> /><%=Lang_Content(24)%><!--只允许会员评论-->
                     <input type="radio" value="1" name="CommentSet" <%=Admin.Input_Checked(CommentSet,1)%> /><%=Lang_Content(23)%><!--禁止评论-->
                     <span style="display:none;">
                     <input type="radio" value="2" name="CommentSet" <%=Admin.Input_Checked(CommentSet,2)%> /><%=Lang_Content(22)%><!--自由评论-->
                     </span>
                     
                     </td>
                 </tr>
                 <% End If%>
                 <tr style="display:none;"><td class="titletd" valign="top"><%=Lang_Content(17)%></td>
                     <td class="infotd">
                     <input type="radio" value="0" name="Openness" <%=Admin.Input_Checked(Openness,0)%> onClick="ShowOrHidden('SetPassword',0)"/><%=Lang_Content(25)%><!--完全公开-->
                     <input type="radio" value="1" name="Openness" <%=Admin.Input_Checked(Openness,1)%> onClick="ShowOrHidden('SetPassword',0)" /><%=Lang_Content(26)%><!--完全保密-->
                      <span style="display:none;">
                     <input type="radio" value="2" name="Openness" <%=Admin.Input_Checked(Openness,2)%> onClick="ShowOrHidden('SetPassword',1)" /><%=Lang_Content(27)%><!--查看需要密码-->
                     </span>
                     </td>
                 </tr>
                 <tr style="display:none;"><td class="titletd"></td>
                     <td class="infotd">
                         <div id="SetPassword" <% if Openness<>2 Then Response.Write"style=""display:none;"""%> >
                         <table class="infoTable" cellpadding="0" cellspacing="0">
                         <tr><td class="titletd"><%=Lang_Content(28)%><!--输入密码--></td><td class="infotd"><input type="text" class="TxtClass" id="Password" name="Password" value="<%=Password%>" /></td></tr>
                         </table>
                         <label class="Normal" id="d_Password"></label>
                         </div>
                     </td>
                 </tr>
                 <tr><td class="titletd"><%=Lang_Content(14)%></td>
                     <td class="infotd">
                        <input type="text" class="TxtClass" id="Hits" name="Hits" style="width:50px" value="<%=Hits%>"/>
                        <label class="Normal"></label>
                     </td>
                 </tr>
                 </tbody>
                </table>
            </div>
                      
            <!--SEO优化-->
            <div class="pt5">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tbody>
                   <tr><td class="titletd" valign="top"><%=Lang_SeoTitle%></td><td class="infotd">
                       <input type="text" class="TxtClass long fl mr5" name="SeoTitle" id="SeoTitle" maxlength="250" value="<%=KnifeCMS.Data.ReplaceString(SeoTitle,"&nbsp;",Chr(32))%>"/><div class="fl"><label class="Normal" id="d_SeoTitle"><%=Lang_SeoTitleCue%></label></div>
                       </td></tr>
                   <tr><td class="titletd" valign="top"><%=Lang_SeoUrl%></td><td class="infotd">
                       <input type="text" class="TxtClass readonly  fl mr5" name="url_pre" readonly="readonly" style="width:260px;" value="<%=SiteURL & SystemPath & "?content/"& Pv_System &"/"& KnifeCMS.IIF(ID>0,ID,"{id}") &"/"%>"/>
                       <input type="text" class="TxtClass fl mr5" name="SeoUrl" id="SeoUrl" maxlength="250"  style="width:200px;" value="<%=SeoUrl%>"/>
                       <input type="text" class="TxtClass readonly fl mr5" name="url_suffix" readonly="readonly" style="width:34px;" value="<%=FileSuffix%>"/>
                       <div class="fl"><label class="Normal" id="d_SeoUrl"><%=Lang_SeoUrlCue%></label></div>
                       </td></tr>
                   <tr><td class="titletd" valign="top"><%=Lang_MetaKeywords%></td><td class="infotd">
                       <input type="text" class="TxtClass long fl mr5" name="MetaKeywords" id="MetaKeywords" maxlength="250" value="<%=KnifeCMS.Data.ReplaceString(MetaKeywords,"&nbsp;",Chr(32))%>"/><div class="fl"><label class="Normal" id="d_MetaKeywords"><%=Lang_MetaKeywordsCue%></label></div>
                       </td></tr>
                   <tr><td  class="titletd" valign="top"><%=Lang_MetaDescription%></td><td class="infotd">
                       <textarea class="metadescription fl" name="MetaDescription" id="MetaDescription" maxlength="250" onblur="KnifeCMS.onblur(this,'maxLength:250')" style="margin-right:5px;" ><%=KnifeCMS.Data.ReplaceString(MetaDescription,"&nbsp;",Chr(32))%></textarea>
                       <div class="fl"><label class="Normal" id="d_MetaDescription"><%=Lang_MetaDescriptionCue%></label></div>
                       </td>
                   </tr>
                </tbody>
                </table>
            </div>   
        </div>
        
        <!--相关内容-->
		<div id="Tabs_3" style="display:none;">
			<div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
            <tbody>
            <tr><td class="titletd" valign="top"><%=Lang_Content(32)%></td><td class="infotd">
            <div class="pb5">
			  <span class="sysBtn"><a href="javascript:;" onClick="Admin.GetSelectItems('Link','LinkContents','LinkContentsID[]','content.asp?ctl=<%=Ctl%>&act=view&acttype=getitems')"><b class="icon icon_add"></b><%=Lang_Btn_AddLinkContent%></a></span>
			</div>
			<div class="tabledata">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
			   <tbody id="LinkContents">
				<tr class="top">
				<th width="2%"><div style="width:30px;"><%=Lang_ListTableColLine_Del%></div></th>
				<th width="3%"><div style="width:50px;"><%=Lang_ListTableColLine_ID%></div></th>
				<th width="95%"><div style="width:200px;"><%=Lang_ListTableColLine_Title%></div></th>
				</tr>
				<% If Action="edit" Then Call GetLinkContents(LinkContents)%>
			   </tbody>
			</table>
			</div>
            </td>
            </tr>
            </tbody>
            </table>
            </div>
            
            <% If CloseModuleVote<>1 Then%>
            <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
            <tbody>
            <tr><td class="titletd" valign="top"><%=Lang_Content(31)%></td><td class="infotd">
            <div class="pb5">
			  <span class="sysBtn"><a href="javascript:;" onClick="Admin.GetSelectItems('Link','LinkVotes','LinkVotesID[]','vote.asp?ctl=vote&act=view&acttype=getitems')"><b class="icon icon_add"></b><%=Lang_Btn_AddLinkVotes%></a></span>
			</div>
			<div class="tabledata">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
			   <tbody id="LinkVotes">
				<tr class="top">
				<th width="2%"><div style="width:30px;"><%=Lang_ListTableColLine_Del%></div></th>
				<th width="3%"><div style="width:50px;"><%=Lang_ListTableColLine_ID%></div></th>
				<th width="95%"><div style="width:200px;"><%=Lang_ListTableColLine_Title%></div></th>
				</tr>
				<% If Action="edit" Then Call GetLinkVotes(LinkVotes)%>
			   </tbody>
			</table>
			</div>
            </td>
            </tr>
            </tbody>
            </table>
            </div>
            <% End If%>
		</div>
		<input type="hidden" name="id" value="<%=ID%>" />
		<input type="hidden" name="sysid" id="sysid" value="<%=SysID%>" />
		<input type="hidden" id="Status" name="Status" value="<%=ContentStatus%>" />
		<div class="bottomSaveline" id="SubmitButtoms" style="text-align:left; padding-left:150px;">
		<button type="submit" class="sysSaveBtn" id="Publish" name="Publish" onclick="document.getElementById('Status').value=1;"><b class="icon icon_save"></b><%=Lang_ContentBtn(1)%></button>
		<button type="submit" class="sysSaveBtn" id="Draft" name="Draft" onclick="document.getElementById('Status').value=0;"><b class="icon icon_reset"></b><%=Lang_ContentBtn(2)%></button>
		<button  type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><%=Lang_Btn_Reset%></button>
		<button type="button" class="sysSaveBtn" id="ReSetBtn" onClick="if( confirm('<%=Lang_DoConfirmLeave%>') ){ CloseWithOpenNewWin('content.asp?ctl=<%=Ctl%>&act=view');}"><b class="icon icon_reset"></b><%=Lang_Btn_Close%></button>
		</div>
		</form>
		</div>
	  </DIV>
	</DIV>
	<script type="text/javascript">
	selectFirstPic("imgslist","SlidesImg[]");
	Admin.AutoSeoTitle(KnifeCMS.$id('Title'),KnifeCMS.$id('TempTitle'),KnifeCMS.$id('SeoTitle'));
	</script>
	<%
	End Function
End Class
%>