<!--#include file="config.admin.asp"-->
<%
Dim PayTradeLog
Set PayTradeLog = New Class_KnifeCMS_Admin_PayTradeLog
Set PayTradeLog = Nothing
Class Class_KnifeCMS_Admin_PayTradeLog

	Private Pv_OrderID,Pv_UserID,Pv_Currency,Pv_Status,Pv_PaymentID
	Private Pv_Username,Pv_SysUsername
	Private Pv_BillID,Pv_Bank,Pv_Account,Pv_PayAccount,Pv_Money,Pv_PayCost,Pv_CurMoney,Pv_PayType,Pv_PaymentName,Pv_OperaterID,Pv_IP,Pv_TBegin,Pv_TEnd,Pv_Remarks,Pv_Disabled,Pv_TradeNo
	Private Pv_PaymentData
	Private Pv_KeywordsType,Pv_PriceFrom,Pv_PriceTo
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "paytradelog.lang.asp"
		IsContentExist = False
		Header()
		If Ctl = "paytradelog" Then
			Pv_BillID = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("both","billid"),"[^0-9]",""),16)
			Select Case Action
				Case "view"
					Call PayTradeLogGetSearchCondition()
					If SubAction = "search" Then
						Call PayTradeLogSearchSql()
					End If
					Call PayTradeLogView()
				Case Else
					Admin.ShowMessage Lang_PraError,"?ctl="& Ctl &"&act=view",1
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"?ctl="& Ctl &"&act=view",1
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function PayTradeLogGetSearchCondition()
		Pv_PriceFrom = KnifeCMS.GetForm("both","PriceFrom") : If Not(KnifeCMS.Data.IsNul(Pv_PriceFrom)) Then Pv_PriceFrom = KnifeCMS.Data.FormatDouble(Pv_PriceFrom)
		Pv_PriceTo   = KnifeCMS.GetForm("both","PriceTo")   : If Not(KnifeCMS.Data.IsNul(Pv_PriceTo))   Then Pv_PriceTo   = KnifeCMS.Data.FormatDouble(Pv_PriceTo)
		Pv_PaymentID    = KnifeCMS.GetForm("both","paymentid[]")
		Pv_KeywordsType = KnifeCMS.GetForm("both","keywordstype")
	End Function
	Private Function PayTradeLogSearchSql()
		Dim Fn_TempSql
		Pv_PaymentID = ParseSearchData(Pv_PaymentID)
		Pv_PriceFrom = KnifeCMS.Data.FormatDouble(Pv_PriceFrom)
		Pv_PriceTo  = KnifeCMS.Data.FormatDouble(Pv_PriceTo)
		Fn_TempSql  = Fn_TempSql & KnifeCMS.IIF(KnifeCMS.Data.IsNul(Pv_PaymentID) Or Pv_PaymentID="any",""," AND a.PaymentID IN ("& Pv_PaymentID &")" )
		Fn_TempSql  = Fn_TempSql & KnifeCMS.IIF(Pv_PriceFrom > 0 ," AND isnull(a.TradeFee,0)>="& Pv_PriceFrom &" "," ")
		Fn_TempSql  = Fn_TempSql & KnifeCMS.IIF(Pv_PriceTo   > 0 ," AND isnull(a.TradeFee,0)<="& Pv_PriceTo &" "  ," ")
		Select Case Pv_KeywordsType
			Case "tradeno"
				Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Admin.Search.KeyWords,"[^0-9]",""),20)
				Fn_TempSql = Fn_TempSql & " AND a.TradeNo LIKE '%"& Admin.Search.KeyWords &"%'"
			Case "status"
				Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Admin.Search.KeyWords,"[^a-zA-Z]",""),20)
				Fn_TempSql = Fn_TempSql & " AND a.Status LIKE '%"& Admin.Search.KeyWords &"%'"
			Case "paymentname"
				Fn_TempSql = Fn_TempSql & " AND a.PaymentName LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%'"
			Case "orderid"
				Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Admin.Search.KeyWords,"[^0-9]",""),20)
				Fn_TempSql = Fn_TempSql & " AND a.OrderID LIKE '%"& Admin.Search.KeyWords &"%'"
			Case "memberid"
				Admin.Search.KeyWords = KnifeCMS.Data.CLng(Admin.Search.KeyWords)
				Fn_TempSql = Fn_TempSql & " AND a.MemberID="& Admin.Search.KeyWords &""
			Case "banktradeno"
				Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Admin.Search.KeyWords,"[^a-zA-Z0-9]",""),50)
				Fn_TempSql = Fn_TempSql & " AND a.BankTradeNo LIKE '%"& Admin.Search.KeyWords &"%'"
			Case "ip"
				Admin.Search.KeyWords = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(Admin.Search.KeyWords,"[^0-9.]",""),15)
				Fn_TempSql = Fn_TempSql & " AND a.IP LIKE '%"& Admin.Search.KeyWords &"%'"
			Case "remarks"
				Fn_TempSql = Fn_TempSql & " AND a.Remarks LIKE '%"& KnifeCMS.Data.HTMLEncode(Admin.Search.KeyWords) &"%'"
		End Select
		PayTradeLogSearchSql = Fn_TempSql
	End Function
	Private Function ParseSearchData(ByVal BV_Data)
		Dim Fn_ii,Fn_ReturnString,Fn_TempArray,Fn_Temp
		Fn_TempArray    = Split(BV_Data,",")
		Fn_ReturnString = ""
		For Fn_ii=0 To Ubound(Fn_TempArray)
			If Fn_TempArray(Fn_ii)="any" Then
				Fn_ReturnString="any"
				Exit For
			Else
				Fn_Temp = KnifeCMS.Data.Clng(Fn_TempArray(Fn_ii))
				If Fn_Temp>=0 Then
					If Fn_ReturnString="" Then Fn_ReturnString = Fn_Temp : Else Fn_ReturnString = Fn_ReturnString &","& Fn_Temp
				End If
			End If
		Next
		ParseSearchData = Fn_ReturnString
	End Function
	
	Private Function PayTradeLogList()
		Dim Fn_TempSql,Fn_ColumnNum
			Fn_TempSql = "SELECT a.ID,a.TradeType,a.TradeNo,a.Currency,a.TradeFee,a.Status,a.PaymentName,a.OrderID,a.StartTime,a.EndTime,a.MemberID,a.BankTradeNo,a.IP,a.Remarks FROM ["& DBTable_PayTradeLog &"] a"
			Fn_ColumnNum = 14
			Operation = ""
		If SubCtl = "recycle" Then
			Fn_TempSql = Fn_TempSql & " WHERE a.Recycle=1 ORDER BY a.ID DESC"
			Operation = ""
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,"")
		Else
			Fn_TempSql = Fn_TempSql & " WHERE a.Recycle=0 "
			If SubAction = "search" Then
			
				Select Case DB_Type
				Case 0
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.StartTime>=#"& Admin.Search.StartDate &"# ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.StartTime<=#"& Admin.Search.EndDate &"# ","")
				Case 1
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>""," AND a.StartTime>='"& Admin.Search.StartDate &"' ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>""," AND a.StartTime<='"& Admin.Search.EndDate &"' ","")
				End Select
				
				tempCondition = tempCondition & PayTradeLogSearchSql
				PageUrlPara = PageUrlPara & "&startdate="& Server.URLEncode(Admin.Search.StartDate) &"&enddate="& Server.URLEncode(Admin.Search.EndDate) &"&keywords="& Server.URLEncode(Admin.Search.KeyWords)&"&keywordstype="& Pv_KeywordsType &"&PriceFrom="& Pv_PriceFrom &"&PriceTo="& Pv_PriceTo &""
				Fn_TempSql = Fn_TempSql & tempCondition
			End If
			Fn_TempSql = Fn_TempSql &" ORDER BY a.ID DESC"
			Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
		End If
	End Function
	
	Private Function PayTradeLogView()
%>
	<%=Admin.Calendar(Array("","",""))%>
    <DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
          <td class="tdcell">
            <table border="0" cellpadding="2" cellspacing="0">
            <tr><td class="upcell">
                <div class="align-center grey">
                    <% Echo Lang_DistrictDoSearch %><!--搜索操作区-->
                    <span class="AdvancedSearch"><a href="javascript:Admin.AdvancedSearch.Open(600,324,'AdvancedSearch')" ><% Echo Lang_AdvancedSearch %></a></span>
                </div>
                <DIV id="AdvancedSearch" style="display:none;">
                    <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                    <div class="diainbox">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                      <tr><td class="titletd"><% Echo Lang_Keywords %>:</td>
                          <td class="infotd">
                          <select name="keywordstype">
						  <% Echo OS.CreateOption(Array("tradeno:"& Lang_PayTradeLog_ListTableColLine(1),"status:"& Lang_PayTradeLog_ListTableColLine(4),"paymentname:"& Lang_PayTradeLog_ListTableColLine(5),"orderid:"& Lang_PayTradeLog_ListTableColLine(6),"memberid:"& Lang_PayTradeLog_ListTableColLine(9),"banktradeno:"& Lang_PayTradeLog_ListTableColLine(10),"ip:"& Lang_PayTradeLog_ListTableColLine(11),"remarks:"& Lang_PayTradeLog_ListTableColLine(12)),Split(Pv_KeywordsType,",")) %>
                          </select>
                          <input type="text" class="TxtClass" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:306px;" /></td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_PayTradeLog_ListTableColLine(5) %><!--支付方式-->:</td><td class="infotd">
						  <% 
						  Dim Fn_PaymentString : Fn_PaymentString = Admin.GetListAsArrayList(DBTable_Payments &":ID,PaymentName","Disabled=0 ","ORDER BY OrderNum ASC,ID ASC")
						  If KnifeCMS.Data.IsNul(Fn_PaymentString) Then
							  Execute("Pv_PaymentData = Array(""any:"& Lang_AnyPayment &""")")
						  Else
						      Execute("Pv_PaymentData = Array(""any:"& Lang_AnyPayment &""","& Fn_PaymentString &")")
						  End If
						  %>
                          <select name="paymentid[]" id="paymentid[]" multiple="multiple" size="5" style="width:200px;">
                          <% Echo OS.CreateOption(Pv_PaymentData,Split(Pv_PaymentID,",")) %>
                          </select>
                          </td>
                      </tr>
                      <tr><td class="titletd"><% Echo Lang_PayTradeLog_ListTableColLine(3) %><!--交易金额-->:</td>
                            <td class="infotd"><% Echo Lang_From %><!--从--> <input type="text" id="PriceFrom" name="PriceFrom" class="TxtClass" style="width:51px;" value="<% Echo Pv_PriceFrom %>" /> <% Echo Lang_FromTo %><!--到--> <input type="text" id="PriceTo" name="PriceTo" class="TxtClass" style="width:51px;" value="<% Echo Pv_PriceTo %>" /></td></tr>
                      <tr><td class="titletd"><% Echo Lang_StartDate %>:</td>
                          <td class="infotd"><input type="text" class="TxtSer dateStart" id="startdate" name="startdate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' value="<%=Admin.Search.StartDate%>" style="width:200px;" /></td>
                      <tr><td class="titletd"><% Echo Lang_EndDate %>:</td>
                          <td class="infotd"><input type="text" class="TxtSer dateEnd" id="enddate" name="enddate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' value="<%=Admin.Search.EndDate%>" style="width:200px;" /></td>
                      </tr>
                      </table>
                      </div>
                    <% Echo AdvancedSearchBtnline %>
                    </form>
                </DIV>
                </td></tr>
            <tr><td class="downcell">
                <form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
                <table border="0" cellpadding="0" cellspacing="0"><tr>
                <td><span><% Echo Lang_Keywords %>:</span></td>
                <td class="pl3">
                <select name="keywordstype">
                <% Echo OS.CreateOption(Array("tradeno:"& Lang_PayTradeLog_ListTableColLine(1),"status:"& Lang_PayTradeLog_ListTableColLine(4),"paymentname:"& Lang_PayTradeLog_ListTableColLine(5),"orderid:"& Lang_PayTradeLog_ListTableColLine(6),"memberid:"& Lang_PayTradeLog_ListTableColLine(9),"banktradeno:"& Lang_PayTradeLog_ListTableColLine(10),"ip:"& Lang_PayTradeLog_ListTableColLine(11),"remarks:"& Lang_PayTradeLog_ListTableColLine(12)),Split(Pv_KeywordsType,",")) %>
                </select>
                </td>
                <td class="pl3"><input type="text" class="TxtClass" name="keywords" id="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:150px;" /></td>
                <td class="pl3"><button type="submit" class="sysBtn" ><b class="icon icon_search"></b><% Echo Lang_Btn_Search %></button></td>
                </tr></table>
                </form>
            </td></tr>
            </table>
          </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th><div><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
            <th><div><% Echo Lang_PayTradeLog_ListTableColLine(0) %><!--交易类型--></div></th>
			<th><div><% Echo Lang_PayTradeLog_ListTableColLine(1) %><!--交易单号--></div></th>
            <th><div><% Echo Lang_PayTradeLog_ListTableColLine(2) %><!--货币--></div></th>
            <th><div><% Echo Lang_PayTradeLog_ListTableColLine(3) %><!--交易金额--></div></th>
            <th><div><% Echo Lang_PayTradeLog_ListTableColLine(4) %><!--状态--></div></th>
            <th><div><% Echo Lang_PayTradeLog_ListTableColLine(5) %><!--支付方式--></div></th>
            <th><div><% Echo Lang_PayTradeLog_ListTableColLine(6) %><!--订单号--></div></th>
            <th><div><% Echo Lang_PayTradeLog_ListTableColLine(7) %><!--开始时间--></div></th>
            <th><div><% Echo Lang_PayTradeLog_ListTableColLine(8) %><!--完成时间--></div></th>
            <th><div><% Echo Lang_PayTradeLog_ListTableColLine(9) %><!--会员ID--></div></th>
            <th><div><% Echo Lang_PayTradeLog_ListTableColLine(10) %><!--银行交易号--></div></th>
            <th><div><% Echo Lang_PayTradeLog_ListTableColLine(11) %><!--IP--></div></th>
            <th><div><% Echo Lang_PayTradeLog_ListTableColLine(12) %><!--备注--></div></th>
			<th><div><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% PayTradeLogList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
    <script type="text/javascript">
	KnifeCMS.SetSelectIndexNumFontWeight(0,KnifeCMS.$id("paymentid[]"));
	TrBgChange("listForm","InputName","oliver","data","select");
	if(objExist(document.getElementById("keywords"))){ReplaceText(document.getElementById("keywords").value,1)}
	function ChangeTdText(){
		var tds=document.getElementById("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			var IsEdit = true;
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td1"){
					tds[i].innerHTML = KnifeCMS.PayTradeType(tds[i].innerHTML);
				}
				if(tds[i].id=="td4"){
					TempHTML=KnifeCMS.FormatCurrency(tds[i].innerHTML);
					tds[i].innerHTML = TempHTML
				}
				if(tds[i].id=="td5"){
					tds[i].innerHTML = KnifeCMS.PayTradeStatus(tds[i].innerHTML);
				}
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
End Class
%>
	