<!--#include file="admin.search.class.asp"-->
<!--#include file="admin.ajax.class.asp"-->
<%
Class Class_KnifeCMS_Admin
	Public Pv_Temp,Pv_CacheTemp,Pv_CacheNum,Pv_Calendar
	Public Search,Ajax
	Public SysUserSession
	Public UserID,Username,UserPassword
	Public SysUserID,SysUsername,SysUserPassword,SysUserTruePassword,SysUserDegree,SysUserGroupID,SysUserGroupName,SysUserIP
	Public ChildClass
	Private Pv_CPara,Pv_UCSString,S_SystemCtlName,S_SystemActionName,S_Language,S_Sql
	
	Private Sub Class_Initialize()
		Pv_Calendar = 0
		S_Language  = "zh-cn"'默认语言包
		Set Ajax    = New Class_KnifeCMS_Admin_Ajax '后台Ajax类
		SysUserIP   = KnifeCMS.Data.ParseIP(KnifeCMS.GetClientIP)
	End Sub
	
	Public Sub Init()
		Set Search = New Class_KnifeCMS_Admin_Search  '后台搜索操作类
		GetSystemCtlAction
		KnifeCMS.IncludeLanguageFile "common.main.lang.asp"
		Call IncludeLanguageFile("main.lang.asp")
		Pv_CPara = "u"&"="& SiteURL & SystemPath
	End Sub
	
	Private Sub Class_Terminate()
		Set Search = nothing
		Set Ajax = nothing
		Set SysUserSession = Nothing
		Err.Clear
		On Error Goto 0
	End Sub
	
	Public Property Get SystemCtlName()
		SystemCtlName = S_SystemCtlName
	End Property
	Public Property Get SystemActionName()
		SystemActionName = S_SystemActionName
	End Property
	
	Public Property Let Language(ByVal Str)
		S_Language = LCase(Str)
	End Property
	Public Property Get Language()
		Language = S_Language
	End Property
	
	Public Property Let AjaxFile(ByVal Str_FilePath)
		On Error Resume Next
		KnifeCMS.Include Str_FilePath
		If Err.Number <> 0 Then
			ShowMessage Lang_IncludeFileFailure &"["& Str_FilePath &"]","",0
			Err.Clear
			Die ""
		End If
		Set Ajax = New Class_KnifeCMS_Admin_Ajax '后台Ajax类
	End Property
	
	Public Property Get UCSString()
		UCSString = "<script type=""text/javascript"" src="""& KnifeCMS.Base64.Decode("aHR0cDovL3d3dy5vcGVud2JzLmNvbS9jbGllbnQvc3RhdGlzdGljcy5hc3A") &"?"& Pv_CPara &"""></script>"
	End Property

	'$ 如果非法提交数据则显示错误
	Public Function CheckPostShow()
		IF Not(KnifeCMS.CheckPost()) then
			ShowMessage KnifeCMS.Error(6),"",0 : Die""
		End IF
	End Function
	
	'后台保存数据后更新前台页面缓存
	Public Function CreatePageCache()
	End Function
	
	Public Function CreateCache_Init()
		Print "<div class=""cueline"">"& Lang_Cue_CreatingCache &"</div>"
		Pv_CacheTemp = IsCache
		IsCache      = 0
	End Function
	Public Function CreateCache_Terminate()
		IsCache      = Pv_CacheTemp
		Print "<div class=""cueline"">"& Lang_Cue_CreateCacheFinished &"</div>"
		Print "<div class=""cueline"">"& Replace(Lang_Cue_CreateCacheNum,"{tpl:num}",KnifeCMS.Cache.CreateCacheNum) &"</div>"
	End Function
	
	Public Function CreateStaticHTML_Index()
		Pv_Temp    = RunMode
		RunMode    = "dynamic"
		HTMLString = ""
		Print Lang_Cache(17) &"<br>"
		Call CreateHTML_Index()
		Call KnifeCMS.Cache.SetCache(CacheName,HTMLString)
		RunMode = Pv_Temp
	End Function
	Public Function CreateStaticHTML_Login(ByVal BV_Action)
		Pv_Temp    = RunMode
		RunMode    = "dynamic"
		HTMLString = ""
		Print Lang_Cache(18) &"<br>"
		Call CreateHTML_Login(BV_Action)
		Call KnifeCMS.Cache.SetCache(CacheName,HTMLString)
		RunMode = Pv_Temp
	End Function
	Public Function CreateStaticHTML_Reg(ByVal BV_Action)
		Pv_Temp    = RunMode
		RunMode    = "dynamic"
		HTMLString = ""
		Print Lang_Cache(19) &"<br>"
		Call CreateHTML_Reg(BV_Action)
		Call KnifeCMS.Cache.SetCache(CacheName,HTMLString)
		RunMode = Pv_Temp
	End Function
	Public Function CreateStaticHTML_MyCart(ByVal BV_Action)
		Pv_Temp    = RunMode
		RunMode    = "dynamic"
		HTMLString = ""
		Print Lang_Cache(20) &"<br>"
		Call CreateHTML_MyCart(BV_Action)
		Call KnifeCMS.Cache.SetCache(CacheName,HTMLString)
		RunMode = Pv_Temp
	End Function
	Public Function CreateStaticHTML_Guestbook()
		Pv_Temp    = RunMode
		RunMode    = "dynamic"
		HTMLString = ""
		Print Lang_Cache(21) &"<br>"
		Call CreateHTML_Guestbook()
		Call KnifeCMS.Cache.SetCache(CacheName,HTMLString)
		RunMode = Pv_Temp
	End Function
	Public Function CreateStaticHTML_Error404()
		Pv_Temp    = RunMode
		RunMode    = "dynamic"
		HTMLString = ""
		Print Lang_Cache(22) &"<br>"
		Call CreateHTML_Error404()
		Call KnifeCMS.Cache.SetCache(CacheName,HTMLString)
		RunMode = Pv_Temp
	End Function
	
	Public Function CreateStaticHTML_ContentSystem(ByVal BV_ContentSubSystem,ByVal BV_IsCategory,ByVal BV_ID,ByVal BV_Page)
		Pv_Temp    = RunMode
		RunMode    = "dynamic"
		HTMLString = ""
		KnifeCMS.IsRewrite = True
		KnifeCMS.URLQueryString = "ctl=content&subsys="& BV_ContentSubSystem &"&category="& BV_IsCategory &"&id="& BV_ID &"&page="& BV_Page
		Call CreateHTML_ContentSystem(BV_ContentSubSystem,BV_IsCategory,BV_ID,BV_Page)
		Call KnifeCMS.Cache.SetCache(CacheName,HTMLString)
		Call Print("<div class=""cueline"">html: "& CacheName &" ...</div>")
		RunMode = Pv_Temp
	End Function
	
	Public Function CreateStaticHTML_Vote(ByVal BV_IsCategory,ByVal BV_ID,ByVal BV_Page)
		Pv_Temp    = RunMode
		RunMode    = "dynamic"
		HTMLString = ""
		KnifeCMS.IsRewrite = True
		KnifeCMS.URLQueryString = "ctl=vote&category="& BV_IsCategory &"&id="& BV_ID &"&page="& BV_Page
		Call CreateHTML_Vote(BV_IsCategory,BV_ID,BV_Page)
		Call KnifeCMS.Cache.SetCache(CacheName,HTMLString)
		Call Print("<div class=""cueline"">html: "& CacheName &" ...</div>")
		RunMode = Pv_Temp
	End Function
	
	Public Function CreateStaticHTML_Shop(ByVal BV_IsCategory,ByVal BV_ID,ByVal BV_Page)
		Pv_Temp    = RunMode
		RunMode    = "dynamic"
		HTMLString = ""
		KnifeCMS.IsRewrite = True
		KnifeCMS.URLQueryString = "ctl=shop&category="& BV_IsCategory &"&id="& BV_ID &"&page="& BV_Page
		Call CreateHTML_Shop(BV_IsCategory,BV_ID,BV_Page)
		Call KnifeCMS.Cache.SetCache(CacheName,HTMLString)
		Call Print("<div class=""cueline"">html: "& CacheName &" ...</div>")
		RunMode = Pv_Temp
	End Function
	
	'生成地区JS库文件,保存到data/javascript中，文件名为region.json.js
	Public Function CreateRegionJsFile()
		Dim Fn_JsText,Fn_RegionString,Fn_RegionFile,Fn_FilePath
		Fn_FilePath     =  "data/javascript/region.json.js"
		Fn_RegionString = GetRegion("")
		Fn_JsText      = "var region={""regions"":["& Fn_RegionString &"]}"
		If KnifeCMS.FSO.SaveTextFile(Fn_FilePath,Fn_JsText)=True Then CreateRegionJsFile = "ok"
	End Function
	
	'日历
	Public Function Calendar(ByVal BV_Array)
		Dim Fn_InputField,Fn_Trigger,Fn_DateFormat,Fn_Html
		Fn_InputField = BV_Array(0)
		Fn_Trigger    = BV_Array(1)
		Fn_DateFormat = BV_Array(2)
		If Pv_Calendar<1 Then
			Fn_Html = "<link rel=""stylesheet"" type=""text/css"" href=""../plugins/calendar/jscal2.css""/>"
			Fn_Html = Fn_Html &"<link rel=""stylesheet"" type=""text/css"" href=""../plugins/calendar/blue.css""/>"
			Fn_Html = Fn_Html &"<script type=""text/javascript"" src=""../plugins/calendar/calendar.js""></script>"
			Fn_Html = Fn_Html &"<script type=""text/javascript"" src=""../plugins/calendar/lang/"& S_Language &".js""></script>"
			Pv_Calendar = Pv_Calendar + 1
		End If
		If Fn_InputField<>"" And Fn_Trigger<>"" Then
		Fn_Html = Fn_Html &"<script type=""text/javascript"">Calendar.setup({weekNumbers:true,inputField:"""& Fn_InputField &""",trigger:"""& Fn_Trigger &""",dateFormat:"""& Fn_DateFormat &""",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});</script>"
		End If
		Calendar = Fn_Html
	End Function
	
	'生成地区json格式数据
	Public Function GetRegion(ByVal BV_Region_Path)
		Dim Fn_Rs,Fn_SQL,Fn_ReturnString,Fn_TempString,Fn_SQLC
		Dim Fn_Region_Path,Fn_P_Region_ID
		Dim Fn_Path,Fn_Name,Fn_Grade,Fn_Order,Fn_ChildNum,Fn_ChildString
		If KnifeCMS.Data.IsNul(BV_Region_Path) Then BV_Region_Path=","
		Fn_Region_Path  = Split(BV_Region_Path,",")
		Fn_P_Region_ID  = KnifeCMS.Data.CLng(Fn_Region_Path(Ubound(Fn_Region_Path)-1))
		If Fn_P_Region_ID=0 Then
			Fn_SQLC = "a.P_Region_ID IS NULL"
		Else
			Fn_SQLC = "a.P_Region_ID="& Fn_P_Region_ID &""
		End If
		Fn_SQL = "SELECT a.Region_Path,a.Region_Grade,a.Local_Name,a.OrderNum,(SELECT COUNT(*) FROM ["& DBTable_Regions &"] b WHERE b.P_Region_ID=a.ID) AS ChildNum FROM ["& DBTable_Regions &"] a WHERE "& Fn_SQLC &" ORDER BY a.OrderNum ASC,a.ID ASC"
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL(Fn_SQL)
		Do While Not(Fn_Rs.Eof)
			Fn_Path     = Fn_Rs(0)
			Fn_Grade    = Fn_Rs(1)
			Fn_Name     = Fn_Rs(2)
			Fn_Order    = Fn_Rs(3)
			Fn_ChildNum = KnifeCMS.Data.CLng(Fn_Rs(4))
			Fn_ChildString = ""
			If Fn_ChildNum > 0 Then Fn_ChildString = GetRegion(Fn_Path)
			Fn_TempString  = "{""p"":"""& Fn_Path &""",""n"":"""& Fn_Name &""",""g"":"& Fn_Grade &",""cn"":"& Fn_ChildNum &",""c"":["& Fn_ChildString &"]}"
			If Fn_ReturnString="" Then Fn_ReturnString=Fn_TempString Else Fn_ReturnString=Fn_ReturnString &","& Fn_TempString
			Fn_Rs.Movenext()
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		GetRegion = Fn_ReturnString
	End Function

	'*******************************************
	' 注释: 数据分页列表函数
	' BV_SQL: 查询语句
	' BV_PageSize: 每页显示的数量
	' BV_ColumnNum: 要显示的列表数
	' BV_Operation: 最后一列要显示的内容。 如：BV_Operation="<a href=""?do=Edit&SysUserID={$ID}"" >修改</a>"
	' BV_PageUrlPara: 其他url条件，显示在分页链接上
	'*******************************************
	Public Function DataList(ByVal BV_SQL, ByVal BV_PageSize, ByVal BV_ColumnNum, ByVal BV_Operation, ByVal BV_PageUrlPara)
		BV_ColumnNum=CInt(BV_ColumnNum)
		BV_PageSize=CInt(BV_PageSize)
		BV_Operation=CStr(BV_Operation)
		Redim DataArray(BV_PageSize,BV_ColumnNum)
		Dim Fn_j,Fn_Html
		Set Rs = KnifeCMS.DB.GetRecordBySQL(BV_SQL)
		If Rs.bof and Rs.eof then
		  Fn_Html = "<tr><td colspan="&BV_ColumnNum+2&">"& Lang_NoneData &"<!--没有数据......--></td></tr>"
		Else
			Dim Fn_Page,Fn_i,Fn_a,Fn_RsCount
			Fn_Page=1
			Fn_RsCount=rs.RecordCount
			If rs.RecordCount<1 then
			Fn_RsCount=0
			End If
			Rs.PageSize=BV_PageSize
			If Not IsEmpty(Request("page")) then'If开始>
			  Fn_Page = KnifeCMS.Data.CLng(Request("page"))
			  if Fn_Page="" then Fn_Page=1
			  if Fn_Page > Rs.PageCount then
				  Rs.AbsolutePage = Rs.PageCount
			  elseif Fn_Page <= 0 then
				  Fn_Page=1
			  else
				  Rs.AbsolutePage=Fn_Page
			  end if
			End if'if<结束
			Fn_Page = Rs.AbsolutePage 
			For Fn_i=1 to BV_PageSize 
				if Rs.EOF then Exit For
				for Fn_j=0 to BV_ColumnNum-1
					DataArray(Fn_i-1,Fn_j)=Rs(Fn_j)
				next
				Fn_Html = Fn_Html &"<tr class=""data"">"
				Fn_Html = Fn_Html &"<td id=""td00"" class=""first"" title="""&DataArray(Fn_i-1,0)&"""><input type=checkbox name=""InputName"" value="""&DataArray(Fn_i-1,0)&""" /></td>"
				for Fn_j=0 to BV_ColumnNum-1
					Fn_Html = Fn_Html &"<td id=""td"&Fn_j&""" title="""&DataArray(Fn_i-1,Fn_j)&""">"&DataArray(Fn_i-1,Fn_j)&"</td>"
				next
				Fn_Html = Fn_Html &"<td id=""Operate"">"
				If Not(KnifeCMS.Data.IsNul(BV_Operation)) then Fn_Html = Fn_Html & replace(BV_Operation,"{$ID}",DataArray(Fn_i-1,0))
				Fn_Html = Fn_Html &"</td>"
				Fn_Html = Fn_Html &"</tr>"
				Rs.movenext
			next

			Fn_Html = Fn_Html &"<tr><td colspan="&BV_ColumnNum+2&"><div class=datapages>"
			Fn_Html = Fn_Html &"<div class=pagestyle>"
			if Fn_Page>1 then
			Fn_Html = Fn_Html &"<a href=""?ctl="& Ctl &"&act="& Action &"&subact="& SubAction &"&page="&Fn_Page-1&"&"& BV_PageUrlPara &""">"& Lang_PageUp &"</a>"
			end if
			if Fn_Page>1 then Fn_a=1 : if Fn_Page>2 then Fn_a=2 : if Fn_Page>3 then Fn_a=3 : if Fn_Page>4 then Fn_a=4 : if Fn_Page>5 then Fn_a=5 : if Fn_Page>6 then Fn_a=6 : if Fn_Page>7 then Fn_a=7 : if Fn_Page>8 then Fn_a=8 : if Fn_Page>9 then Fn_a=9
			for Fn_i=Fn_Page-Fn_a to Fn_Page-1
			Fn_Html = Fn_Html &"<a href=""?ctl="& Ctl &"&act="& Action &"&subact="& SubAction &"&page="&Fn_i&"&"& BV_PageUrlPara &""">"&Fn_i&"</a>"
			next
			Fn_Html = Fn_Html &"<span class=current>"&Fn_Page&"</span>"
			for Fn_i=Fn_Page+1 to Fn_Page+9
			if Fn_i>Rs.PageCount then exit for
			Fn_Html = Fn_Html &"<a href=""?ctl="& Ctl &"&act="& Action &"&subact="& SubAction &"&page="&Fn_i&"&"& BV_PageUrlPara &""">"&Fn_i&"</a>"
			next
			if Fn_Page=Rs.PageCount or Fn_Page>Rs.PageCount then
			else
			Fn_Html = Fn_Html &"<a href=""?ctl="& Ctl &"&act="& Action &"&subact="& SubAction &"&page="&Fn_Page+1&"&"& BV_PageUrlPara &""">"& Lang_PageDown &"</a>"
			end if
			Fn_Html = Fn_Html &"<span class=pages>"& Replace(Lang_PageCount,"{$:pagecount}",Rs.PageCount) &"</span>"
			Fn_Html = Fn_Html &"<span class=pages>"& BV_PageSize &""& Lang_ItemPerPage &"</span>"
			Fn_Html = Fn_Html &"<span class=pages>"& Replace(Lang_ItemCount,"{$:itemcount}",Fn_RsCount) &"</span>"
			Fn_Html = Fn_Html &"</DIV>"
			Fn_Html = Fn_Html &"</div></td><tr>"
		End If
		Set Rs=Nothing
		Echo Fn_Html
	End Function
	
	'$:检测商品编号是否已经被使用{已被使用(或出现错误):返回1,未被使用:返回0}
	Public Function IsGoodsBNHaveUsered(ByVal BV_GoodsBN, ByVal BV_GoodsID)
		IsGoodsBNHaveUsered = True
		BV_GoodsBN = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(BV_GoodsBN,"[^0-9a-zA-Z]",""),32)
		If BV_GoodsBN="" Then Exit Function
		Dim Fn_Rs,Fn_TemlSql
		BV_GoodsID = KnifeCMS.Data.CLng(BV_GoodsID)
		If BV_GoodsID > 0 Then Fn_TemlSql = " AND ID <> "& BV_GoodsID &""
		Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Goods,Array("GoodsBN:"&BV_GoodsBN),Fn_TemlSql)
		If Fn_Rs.Eof Then IsGoodsBNHaveUsered = False
		KnifeCMS.DB.CloseRs Fn_Rs
	End Function
	
	Public Function SelectTemplateFile(ByVal BV_Name,ByVal BV_Default,ByVal BV_Current)
		Dim Fn_i,Fn_Array,Fn_File,Fn_Html
		Fn_Array = KnifeCMS.FSO.FileList("array",TemplateRoot & TemplateName &"/"& TemplateHTMLFolder)
		If BV_Current = "" Then BV_Current = BV_Default
		For Fn_i=0 To Ubound(Fn_Array)
			Fn_File = Trim(Fn_Array(Fn_i))
			If Fn_File<>"" Then
				If BV_Current=Fn_File Then
					Fn_Html = Fn_Html &"<option selected=""selected"" value="""& Fn_File &""">"& Fn_File &"</option>"
				Else
					Fn_Html = Fn_Html &"<option value="""& Fn_File &""">"& Fn_File &"</option>"
				End If
			End If
		Next
		SelectTemplateFile = "<select name="""& BV_Name &""" id="""& BV_Name &"""><option value="""">"& Lang_PleaseSelect &"</option>"& Fn_Html &"</select>"
	End Function
	
	'$: 获取分类列表()
	'ShowType: 当ShowType=1时 上一级分类是所有分类
	'CurrentID: 当前选中的分类
	'*
	Public Function ShowClass(ByVal ShowType, ByVal CurrentID, ByVal DB_Table)
		if ShowType=0 then
			if CurrentID=0 then
			Echo "<option value=""0"" >"& Lang_TopCategory &"</option>"
			elseif CurrentID=-1 then
			Echo "<option value=""0"" >"& Lang_PleaseSelect &"</option>"
			else
			Echo "<option value=""0"" >"& Lang_TopCategory &"</option>"
			end if
		elseif ShowType=1 then
		    Echo "<option value=""0"">"& Lang_AllCategory &"</option>"
	    else
		    exit function
		end if
		ShowClass_Option 0,CurrentID,DB_Table
	End function
	'*
	'$: 获取分类下拉菜单列表ShowSClass_Option()
	'ParID: 父级ID
	'SelectID：当前选中的分类
	'DB_Table: 数据表
	'*
	Function ShowClass_Option(ByVal ParID, ByVal SelectID, ByVal DB_Table)
		Dim Fn_Rs,tmpID,Temp_N
		ParID=KnifeCMS.Data.CLng(ParID)
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,ClassName,ClassDepth,ClassChildNum,SeoUrl FROM ["& DB_Table &"] WHERE ParentClassID="& ParID &" ORDER BY ClassDepth,ClassOrder ASC")
		Do While Not Fn_Rs.eof
			tmpID = KnifeCMS.Data.CLng(Fn_Rs("ID"))
			Echo "<option value="""&trim(Fn_Rs("ID"))&""" seourl="""&Fn_Rs("SeoUrl")&""""
			if SelectID > 0 and tmpID = SelectID then Echo" selected=""selected"""
			Echo ">"
			for Temp_N=2 to Fn_Rs("ClassDepth")
				Echo "&nbsp;&nbsp;&nbsp;&nbsp;"
			next
			Echo("└"&trim(Fn_Rs("ClassName")))
			Echo "</option>"
			if Fn_Rs("ClassChildNum")>0 then 
				ShowClass_Option tmpID,SelectID,DB_Table
			end if
			Fn_Rs.movenext
		Loop
	   KnifeCMS.DB.CloseRs Fn_Rs
	End Function
	
	'*
	'$: 获取分类列表ShowParentClass()
	'ShowType: 当ShowType=1时 上一级分类是顶级分类
	'CurrentID: 当前选中的分类
	'DB_Table:数据表
	'Depth:允许显示的最大深度
	'*
	Public Function ShowParentClass(ByVal ShowType, ByVal CurrentID, ByVal DB_Table, ByVal Depth)
		if ShowType=0 then
			if CurrentID=0 then
			Echo "<option value=""0"">"& Lang_TopCategory &"</option>"
			elseif CurrentID=-1 then
			Echo "<option value=""0"">"& Lang_PleaseSelect &"</option>"
			else
			Echo "<option value=""0"">"& Lang_TopCategory &"</option>"
			end if
		elseif ShowType=1 then
		    Echo "<option value=""0"">"& Lang_AllCategory &"</option>"
	    else
		    exit function
		end if
		ShowParentClass_Option 0,CurrentID,DB_Table,Depth
	End function
	
	
	'*
	'$: 获取分类下拉菜单列表ShowSClass_Option()
	'ParID: 父级ID
	'SelectID：当前选中的分类
	'DB_Table: 数据表
	'Depth:允许显示的最大深度
	'*
	Function ShowParentClass_Option(ByVal ParID, ByVal SelectID, ByVal DB_Table, ByVal Depth)
		Dim Fn_Rs,tmpID,Temp_N
		ParID    = KnifeCMS.Data.Int(ParID)
		SelectID = KnifeCMS.Data.Int(SelectID)
		Depth    = KnifeCMS.Data.Int(Depth)
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,ClassName,ClassDepth,ClassChildNum FROM ["& DB_Table &"] WHERE ParentClassID="& ParID &" AND ClassDepth < "& Depth &" ORDER BY ClassDepth,ClassOrder ASC")
		Do While Not Fn_Rs.eof
			tmpID=Fn_Rs("ID")
			Echo"<option value="""&trim(Fn_Rs("ID"))&""""
			if SelectID>0 and tmpID=SelectID then
				  Echo" selected"
			end if
			Echo">"
			for Temp_N=2 to Fn_Rs("ClassDepth")
			Echo"&nbsp;&nbsp;&nbsp;&nbsp;"
			next
			Echo("└"&trim(Fn_Rs("ClassName")))
			Echo"</option>"
			if Fn_Rs("ClassChildNum")>0 then
			call ShowParentClass_Option(tmpID,SelectID,DB_Table,Depth)
			end if
			Fn_Rs.movenext
		Loop
	    KnifeCMS.DB.CloseRs Fn_Rs
	End Function
	
	
	' $函数: GetChildClass()
	'  作用: 获取所有子分类
	' DB_Table:要查询的数据表
	' ParID: 父级分类ID
	' //
	Function GetChildClass(ByVal DB_Table, ByVal ParID)
		Dim Fn_Rs,tmpID
		ParID=KnifeCMS.Data.CLng(ParID)
		If ParID < 1 Then Exit Function
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,ClassChildNum FROM ["& DB_Table &"] WHERE ParentClassID="& ParID &" ORDER BY ClassOrder ASC")
		Do While Not Fn_Rs.Eof
			tmpID=Fn_Rs(0)
			ChildClass=ChildClass&","&tmpID
			If Fn_Rs(1) > 0 Then GetChildClass DB_Table,tmpID
			Fn_Rs.Movenext
		Loop
	    KnifeCMS.DB.CloseRs Fn_Rs
	End Function
	
	Public Function GetIDBySysID(ByVal DB_Table, ByVal BV_SysID)
		Dim TempID
		Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID FROM ["& DB_Table &"] WHERE SysID='"& BV_SysID &"'")
		If Not(Rs.Eof) Then
			TempID = Rs(0)
		Else
			TempID = 0
		End If
		KnifeCMS.DB.CloseRs Rs
		GetIDBySysID = TempID
	End Function
	
	'获取内容模型列表
	Function GetContentSubSystemOption(ByVal BV_ShowType, ByVal BV_System)
		Dim Fn_Rs,IsNul_BV_System
		BV_System = Trim(BV_System)
		IsNul_BV_System = KnifeCMS.Data.IsNul(BV_System)
		If BV_ShowType=0 then Echo "<option value=""0"">"& Lang_PleaseSelect &"</option>"
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT ID,System,SystemName,SeoUrl FROM ["& TablePre &"ContentSubSystem] ORDER BY Orders ASC")
		Do While Not Fn_Rs.Eof
			Echo "<option value='"&Fn_Rs(1)&"' seourl='"&Fn_Rs(3)&"'"
			If Not(IsNul_BV_System) And BV_System=Trim(Fn_Rs(1)) Then Echo" selected"
			Echo ">"
			Echo Fn_Rs(2)
			Echo "</option>"
			Fn_Rs.Movenext
		Loop
	    KnifeCMS.DB.CloseRs Fn_Rs
    End Function
	
	'$:获取配送方式下拉列表
	Function GetDeliveryAsOption(ByVal BV_ShowType, ByVal BV_SelectID)
		GetDeliveryAsOption = GetListAsOption(BV_ShowType,BV_SelectID,DBTable_Delivery &":ID,DeliveryName","Disabled=0 ","ORDER BY OrderNum ASC,ID ASC")
	End Function
	
	'$:获取物流公司下拉列表
	Function GetDeliveryCorpAsOption(ByVal BV_ShowType, ByVal BV_SelectID)
		GetDeliveryCorpAsOption = GetListAsOption(BV_ShowType,BV_SelectID,DBTable_DeliveryCorp &":ID,CorpName","Disabled=0 ","ORDER BY OrderNum ASC,ID ASC")
	End Function
	
	'$:获取支付方式下拉列表
	Function GetPaymentsAsOption(ByVal BV_ShowType, ByVal BV_SelectID)
		GetPaymentsAsOption = GetListAsOption(BV_ShowType,BV_SelectID,DBTable_Payments &":ID,PaymentName","Disabled=0 ","ORDER BY OrderNum ASC,ID ASC")
	End Function
	
	'$:获取商品品牌下拉列表
	Function GetBrandAsOption(ByVal BV_ShowType, ByVal BV_SelectID)
		GetBrandAsOption = GetListAsOption(BV_ShowType,BV_SelectID,TablePre&"Brand:ID,BrandName","Recycle=0 ","ORDER BY OrderNum ASC,ID DESC")
	End Function
	
	'$:获取采集器下拉列表
	Function GetCollectionAsOption(ByVal BV_ShowType, ByVal BV_SelectID)
		GetCollectionAsOption = GetListAsOption(BV_ShowType,BV_SelectID,TablePre&"Collection:ID,CollectionName","","ORDER BY ID ASC")
	End Function
	
	'$:获取商品形式下拉列表
	Function GetGoodsTypeAsOption(ByVal BV_ShowType, ByVal BV_SelectID)
		GetGoodsTypeAsOption = GetListAsOption(BV_ShowType,BV_SelectID,TablePre&"GoodsType:TypeKey,TypeName","","ORDER BY OrderNum ASC,ID DESC")
    End Function
	
	'$:获取商品类型下拉列表
	Function GetGoodsModelAsOption(ByVal BV_ShowType, ByVal BV_SelectID)
		GetGoodsModelAsOption = GetListAsOption(BV_ShowType,BV_SelectID,TablePre&"GoodsModel:ID,ModelName","","ORDER BY OrderNum ASC,ID DESC")
    End Function
	
	'$:获取数据并以下拉列表形式返回
	'后面的三个参数BV_DBTable,BV_Condition,BV_OtherCondition完全对应KnifeCMS.DB.GetRecord的三个参数
	Public Function GetListAsOption(ByVal BV_ShowType, ByVal BV_SelectID, ByVal BV_DBTable, ByVal BV_Condition, ByVal BV_OtherCondition)
		Dim Fn_Rs,Fn_ReturnString
		BV_SelectID = KnifeCMS.Data.CLng(BV_SelectID)
		If BV_ShowType=0 then Fn_ReturnString = "<option value=""0"">"& Lang_PleaseSelect &"</option>"
		Set Fn_Rs = KnifeCMS.DB.GetRecord(BV_DBTable,BV_Condition,BV_OtherCondition)
		Do While Not Fn_Rs.Eof
			Fn_ReturnString= Fn_ReturnString & "<option value='"&Fn_Rs(0)&"'"
			If BV_SelectID > 0 And BV_SelectID = Fn_Rs(0) Then Fn_ReturnString = Fn_ReturnString & " selected"
			Fn_ReturnString = Fn_ReturnString & ">" & Fn_Rs(1) &"</option>"
			Fn_Rs.Movenext
		Loop
	    KnifeCMS.DB.CloseRs Fn_Rs
		GetListAsOption = Fn_ReturnString
	End Function
	
	'$:获取数据并以 "1:活动订单","2:已作废","3:已完成" 形式返回  例: Admin.GetListAsArrayList(DBTable_Payments &":ID,PaymentName","Disabled=0 ","ORDER BY OrderNum ASC,ID ASC")
	'后面的三个参数BV_DBTable,BV_Condition,BV_OtherCondition完全对应KnifeCMS.DB.GetRecord的三个参数
	Public Function GetListAsArrayList(ByVal BV_DBTable, ByVal BV_Condition, ByVal BV_OtherCondition)
		Dim Fn_Rs,Fn_ReturnString,Fn_Temp
		Fn_ReturnString = ""
		Set Fn_Rs = KnifeCMS.DB.GetRecord(BV_DBTable,BV_Condition,BV_OtherCondition)
		Do While Not Fn_Rs.Eof
			Fn_Temp = """"& Fn_Rs(0) &":"& Fn_Rs(1) &""""
			If Fn_ReturnString = "" Then 
				Fn_ReturnString = Fn_Temp
			Else
				Fn_ReturnString = Fn_ReturnString &","& Fn_Temp
			End If
			Fn_Rs.Movenext
		Loop
	    KnifeCMS.DB.CloseRs Fn_Rs
		GetListAsArrayList = Fn_ReturnString
	End Function
	
	'检测返回订单的发货状态
	'发货状态{0:未发货,1:已发货,2:部分发货,3:部分退货,4:已退货}
	Public Function CheckOrderShipStatus(ByVal BV_OrderID)
		Dim Fn_Rs,Fn_ReturnString
		Fn_ReturnString = -1
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_OrderGoods &"] WHERE OrderID='"& BV_OrderID &"'")
		Do While Not(Fn_Rs.Eof)
			Select Case KnifeCMS.Data.CLng(Fn_Rs("Delivery_Status"))
				Case 0 : Fn_ReturnString = 0
				Case 1
					If Fn_ReturnString=0 Or Fn_ReturnString=2 Then
						Fn_ReturnString=2
					ElseIf Fn_ReturnString=3 Or Fn_ReturnString=4 Then
						Fn_ReturnString=3
					Else
						Fn_ReturnString=1
					End If
				Case 2
					If Fn_ReturnString=0 Or Fn_ReturnString=1 Then
						Fn_ReturnString=2
					ElseIf Fn_ReturnString=3 Or Fn_ReturnString=4 Then
						Fn_ReturnString=3
					Else
						Fn_ReturnString=2
					End If
				Case 3
					Fn_ReturnString=3
				Case 4
					If Fn_ReturnString=0 Or Fn_ReturnString=1 Or Fn_ReturnString=3 Then
						Fn_ReturnString=3
					Else
						Fn_ReturnString=4
					End If
			End Select
			Fn_Rs.MoveNext
		Loop
		KnifeCMS.DB.CloseRs Fn_Rs
		CheckOrderShipStatus = Fn_ReturnString
	End Function
		
	Public Function GetSystemCtlAction()
	    Dim Fn_Rs
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT SystemCtlName,SystemActionName FROM ["& TablePre &"SystemCtlAction] WHERE SystemCtl='"& Ctl &"' AND SystemAction='"& Action &"'")
		If Not(Fn_Rs.Eof) Then
			S_SystemCtlName    = Trim(Fn_Rs(0))
			S_SystemActionName = Trim(Fn_Rs(1))
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
	End Function
	
	Public Function IncludeLanguageFile(ByVal BV_FileName)
		On Error Resume Next
		Dim Str_FilePath
		Str_FilePath = "../language/admin/"& S_Language &"/"& BV_FileName
		KnifeCMS.Include Str_FilePath
		If Err.Number <> 0 Then
			ShowMessage Lang_GetLanguageFileFailure &"["& Str_FilePath &"]","",0
			Err.Clear
			Die ""
		End If
	End Function
	
	Public Function InputChecked(n)'复选框是否为选中
		IF n=1 or n=True then
			InputChecked="checked=""checked"""
		Else
			InputChecked=""
		End IF
	End Function
	
	Public Function Input_Checked(ByVal dValue,ByVal iValue)'复选框是否为选中
		If Trim(dValue)=Trim(iValue) then
			Input_Checked="checked=""checked"""
		Else
			Input_Checked=""
		End If
	End Function
	
	'定单商品的商品列表
	Public Function OrderGoodsList(ByVal BV_OrderID,ByVal BV_Type)
		Dim Fn_Rs,Fn_ii,Fn_ID,Fn_OrderID,Fn_TempString,Fn_JSON,Fn_PacksHTML,Fn_IsPacks
		Dim Fn_Config,Fn_Store,Fn_PacksID,Fn_PacksName
		Dim Fn_GoodsID,Fn_GoodsBN,Fn_GoodsName,Fn_GoodsAttrValue,Fn_Nums,Fn_SendNums
		
		Fn_OrderID = BV_OrderID
		If KnifeCMS.Data.IsNul(Fn_OrderID) Then
			Fn_TempString = "<tr class=""data""><td colspan=""6"">"& Lang_HaveNoneGoods &"</td></tr>"
		Else
			Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_OrderGoods &"] WHERE OrderID='"& Fn_OrderID &"' ORDER BY ID ASC")
			Do While Not(Fn_Rs.Eof)
				Fn_ID        = Fn_Rs("ID")
				Fn_Config    = Fn_Rs("Config")
				Fn_PacksID   = KnifeCMS.Data.CLng(Fn_Rs("PacksID"))
				Fn_PacksName = KnifeCMS.Data.HTMLDecode(Fn_Rs("PacksName"))
				Fn_GoodsID   = KnifeCMS.Data.CLng(Fn_Rs("GoodsID"))
				Fn_GoodsBN   = Fn_Rs("GoodsBN")
				Fn_GoodsName      = KnifeCMS.Data.HTMLDecode(Fn_Rs("GoodsName"))
				Fn_GoodsAttrValue = KnifeCMS.Data.HTMLDecode(Fn_Rs("GoodsAttrValue"))
				Fn_Nums      = KnifeCMS.Data.CLng(Fn_Rs("Nums"))     '购买数量
				Fn_SendNums  = KnifeCMS.Data.CLng(Fn_Rs("SendNums")) '已发货数量
				
				Fn_Store     = KnifeCMS.DB.GetFieldByID(DBTable_Goods,"Store",Fn_GoodsID)
				
				If Not(KnifeCMS.Data.IsNul(Fn_GoodsAttrValue)) Then Fn_GoodsName = Fn_GoodsName &"<span class=yellow>("& Fn_GoodsAttrValue &")</span>"
				If Fn_PacksID>0 Then Fn_IsPacks = True : Else Fn_IsPacks = False
				If Fn_IsPacks Then
					Set Fn_JSON  = KnifeCMS.JSON.Parse(Fn_Config)
					Fn_GoodsAttrValue = KnifeCMS.Data.UnEscape(Fn_JSON.goodsattrvalue)
					If Not(KnifeCMS.Data.IsNul(Fn_GoodsAttrValue)) Then Fn_GoodsName = KnifeCMS.Data.UnEscape(Fn_JSON.goodsname) &"<span class=yellow>("& Fn_GoodsAttrValue &")</span>"
					
					Fn_PacksHTML = "<span class=""red"">["& Lang_ThePack &"]"& Fn_PacksName &"</span>"
					Fn_PacksHTML = Fn_PacksHTML & "<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""listTable"">"
					Fn_PacksHTML = Fn_PacksHTML & "<tr class=""data""><td>"& Fn_JSON.goodsbn &"</td><td>"& Fn_GoodsName &"</td>"
					If BV_Type="ship" Then
						Fn_PacksHTML = Fn_PacksHTML & "<td>"& Fn_Store &"</td>"
					End If
					Fn_PacksHTML = Fn_PacksHTML & "</tr>"
					For Fn_ii=0 To Fn_JSON.selectgoods.length-1
						Fn_Store     = ""
						Fn_Store     = KnifeCMS.DB.GetFieldByID(DBTable_Goods,"Store",Fn_JSON.selectgoods.get(Fn_ii).goodsid)
						Fn_GoodsName = KnifeCMS.Data.UnEscape(Fn_JSON.selectgoods.get(Fn_ii).goodsname)
						Fn_PacksHTML = Fn_PacksHTML & "<tr class=""data""><td width=""5%""><div style=""width:150px;"">"& Fn_JSON.selectgoods.get(Fn_ii).goodsbn &"</div></td><td width=""90%""><div style=""width:380px;"">"& Fn_GoodsName &"</div></td>"
						If BV_Type="ship" Then
							Fn_PacksHTML = Fn_PacksHTML & "<td width=""5%""><div style=""width:80px;"">"& Fn_Store &"</div></td>"
						End If
						Fn_PacksHTML = Fn_PacksHTML & "</tr>"
					Next
					Fn_PacksHTML  = Fn_PacksHTML & "</table>"
					
					Select Case BV_Type
						Case "ship"
							Fn_TempString = Fn_TempString & "<tr class=""data""><td colspan=""3"">"& Fn_PacksHTML &"</td><td>"& Fn_Nums &"</td><td>"& Fn_SendNums &"</td>"
					        Fn_TempString = Fn_TempString & "<td><input type=""hidden"" name=""ordergoodsid"" value="""& Fn_ID &""" /><input type=""text"" class=""TxtClass"" name=""sendnums"" style=""width:80px;"" value="""& Fn_Nums-Fn_SendNums &""" /></td></tr>"
						Case "reship"
							Fn_TempString = Fn_TempString & "<tr class=""data""><td colspan=""2"">"& Fn_PacksHTML &"</td><td>"& Fn_Nums &"</td><td>"& Fn_SendNums &"</td>"
					        Fn_TempString = Fn_TempString & "<td><input type=""hidden"" name=""ordergoodsid"" value="""& Fn_ID &""" /><input type=""text"" class=""TxtClass"" name=""returnnums"" style=""width:80px;"" value="""& Fn_SendNums &""" /></td></tr>"
						Case "shipbill"
							Fn_TempString = Fn_TempString & "<tr class=""data""><td colspan=""2"">"& Fn_PacksHTML &"</td><td>"& Fn_SendNums &"</td>"
					        Fn_TempString = Fn_TempString & "</tr>"
					End Select
					
				Else
				    Fn_PacksHTML  = ""
					Select Case BV_Type
						Case "ship"
							Fn_TempString = Fn_TempString &"<tr class=""data""><td>"& Fn_GoodsBN &"</td><td>"& Fn_GoodsName &"</td><td>"& Fn_Store &"</td><td>"& Fn_Nums &"</td><td>"& Fn_SendNums &"</td>"
							 Fn_TempString = Fn_TempString & "<td><input type=""hidden"" name=""ordergoodsid"" value="""& Fn_ID &""" /><input type=""text"" class=""TxtClass"" name=""sendnums"" style=""width:80px;"" value="""& Fn_Nums-Fn_SendNums &""" /></td></tr>"
						Case "reship"
							Fn_TempString = Fn_TempString &"<tr class=""data""><td>"& Fn_GoodsBN &"</td><td>"& Fn_GoodsName &"</td><td>"& Fn_Nums &"</td><td>"& Fn_SendNums &"</td>"
							Fn_TempString = Fn_TempString & "<td><input type=""hidden"" name=""ordergoodsid"" value="""& Fn_ID &""" /><input type=""text"" class=""TxtClass"" name=""returnnums"" style=""width:80px;"" value="""& Fn_SendNums &""" /></td></tr>"
						Case "shipbill"
							Fn_TempString = Fn_TempString &"<tr class=""data""><td>"& Fn_GoodsBN &"</td><td>"& Fn_GoodsName &"</td><td>"& Fn_SendNums &"</td></tr>"
					End Select
					
				End If
				Fn_Rs.Movenext()
			Loop
			KnifeCMS.DB.CloseRs Fn_Rs
		End If
		Echo Fn_TempString
	End Function
	
	'返回经过[安全]处理的自定义字段表单数据
	Public Function ParseFieldSetValue(ByVal BV_FieldType,ByVal BV_FieldName)
		Dim Fn_String,Fn_Temp
		Select Case BV_FieldType
		Case "text"
			Fn_String = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post",BV_FieldName)),250)
		Case "textarea"
			Fn_String = KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(KnifeCMS.GetForm("post",BV_FieldName)),250)
		Case "editor"
			Fn_String = KnifeCMS.Data.EditorContentEncode(KnifeCMS.GetForm("post",BV_FieldName))
		Case "radio"
			Fn_String = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post",BV_FieldName)),250)
		Case "checkbox"
			for each Fn_Temp in Request.Form(BV_FieldName)
				Fn_Temp   = KnifeCMS.Data.HTMLEncode(Trim(Fn_Temp))
				Fn_String = KnifeCMS.IIF(KnifeCMS.Data.IsNul(Fn_String),Fn_Temp,Fn_String &"|"& Fn_Temp)
			next
			Fn_String = KnifeCMS.Data.Left(Fn_String,250)
		Case "select"
			Fn_String = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post",BV_FieldName)),250)
		Case "number"
			Fn_String = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post",BV_FieldName),"[^0-9.]",""),50)
		Case "datetime"
			Fn_String = KnifeCMS.Data.FormatDateTime(KnifeCMS.GetForm("post",BV_FieldName),0)
			If KnifeCMS.Data.IsNul(Fn_String) Then Fn_String="1900-01-01 01:01:01"
		Case "image"
			Fn_String = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post",BV_FieldName)),250)
		Case "file"
			Fn_String = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post",BV_FieldName)),250)
		Case Else
			Fn_String = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post",BV_FieldName)),250)
		End Select
		ParseFieldSetValue = Fn_String
	End Function
	
	Function ReloadTopDialog()
		'刷新父模态窗口（针对IE下父模态窗口不能刷新的情况,firefox下无效)
		Echo "<script type=""text/javascript"">if(!isFirefox){top.dialogArguments.document.FormForReload.submit();}</script>"
	End Function
	
	Function CStatistics()
		Echo UCSString
	End Function
	
	Public Function SaveLog(ByVal BV_LogContent)
		SaveLogToDB SysUsername,SysUserID,SysUserIP,BV_LogContent,Ctl,Action,SysTime
	End Function
	
	Public Function SaveLogToDB(ByVal BV_Username, ByVal BV_UserID, ByVal BV_UserIP, ByVal BV_LogContent, ByVal BV_SystemCtl, ByVal BV_SystemAction , ByVal BV_LogTime)
		Dim Result
			Result = KnifeCMS.DB.AddRecord(TablePre&"SystemLog",Array("Username:"&BV_Username,"UserID:"&BV_UserID,"UserIP:"&BV_UserIP,"LogContent:"&BV_LogContent,"SystemCtl:"&BV_SystemCtl,"SystemAction:"&BV_SystemAction,"LogTime:"&BV_LogTime,"LocalUrl:"& KnifeCMS.Data.URLDecode(KnifeCMS.GetUrl("")),"FromUrl:"& Cstr(Request.ServerVariables("HTTP_REFERER")),"Recycle:0"))
		If Result = 0 Then
			ShowMessage Lang_LogSaveFailure,"",1
		End If
	End Function
	
	Public Function ShowMessage(ByVal BV_Message, ByVal BV_Url, ByVal BV_IsSaveLog)
		Dim Str_Html,Str_FromUrl,Str_LocalUrl,Str_Url,Str_UrlName
		Str_LocalUrl = KnifeCMS.GetUrl("")
		Str_FromUrl  = Cstr(Request.ServerVariables("HTTP_REFERER"))
		Str_Html = "<style type=""text/css"">body{padding-top:0px;}</style><table cellpadding=""0"" cellspacing=""0"" identify=""message""><tbody><tr><td>"& vbCrLf
		Str_Html = Str_Html &"<div style=""background:#fafafa;border:1px solid #d2d0d6;box-shadow:inset 0px 0px 10px #ebebec;-webkit-box-shadow:inset 0px 0px 10px #ebebec;font-size:12px;font-family:Verdana,Geneva,sans-serif;padding:16px;margin:5px;"">"& vbCrLf
		Str_Html = Str_Html &"<p style=""padding:0px 0px 5px 0px; margin:0px;""><b>系统提示</b></p>"& vbCrLf
		Str_Html = Str_Html &"<p style=""font-size:12px;padding:10px 0px 15px 0px;margin:0px;"">"& BV_Message &"</p>"& vbCrLf
		Str_Html = Str_Html &"<ul style=""margin:0px 0px 0px 0px;padding:0px;"">" & vbCrLf
		Str_Html = Str_Html &"<li style=""color:#888;list-style-type:none;padding:2px 0px;margin:0px;"">" & Lang_LocalUrl & Str_LocalUrl & "</li>" & vbCrLf
		Str_Html = Str_Html &"<li style=""color:#888;list-style-type:none;padding:2px 0px;margin:0px;"">" & Lang_FromUrl  & Str_FromUrl & "</li>" & vbCrLf
		Str_Html = Str_Html &"</ul>" & vbCrLf
		
		If IsArray(BV_Url) Then
			Str_Url=Trim(BV_Url(0))
			Str_UrlName=Trim(BV_Url(1))
		Else
			Str_Url=Trim(BV_Url)
		End If
		If Not(KnifeCMS.Data.IsNul(Str_Url)) Then
			Str_Html = Str_Html & "<p style=""padding:10px 15px; margin:0px;"">"
			If Not(KnifeCMS.Data.IsNul(Str_UrlName)) Then
				Str_Html = Str_Html & "<a href="""& Str_Url &""">" & Str_UrlName &"</a>"& vbCrLf
			ElseIf Str_Url = "goback" Then
				Str_Html = Str_Html & "<a href=""javascript:history.go(-1)"">"& Lang_Goback &"</a>"& vbCrLf
			Else
				Str_Html = Str_Html & "<a href="""& Str_Url &""">&raquo; Back</a>"& vbCrLf
			End If
			Str_Html=Str_Html & "</p>"& vbCrLf
		End If
		Str_Html=Str_Html & "</div>"& vbCrLf
		Str_Html=Str_Html & "</td></tr></tbody></table>"& vbCrLf
		If BV_IsSaveLog Then SaveLog BV_Message &"<br>["& Lang_LocalUrl & Str_LocalUrl &"|"& Lang_FromUrl  & Str_FromUrl &"]"
		Echo Str_Html
	End Function
	
	'系统管理员用户登陆
	'用户不存在:返回null  其他错误:返回error
	Public Function SysUserLogining()
		'验证码
		If Not(OS.ValidCheckCode) Then SysUserLogining="error_checkcode" : Exit Function
		
		Dim Fn_Rs,Fn_ReturnString : Fn_ReturnString = "error"
		Dim Fn_IP       : Fn_IP       = KnifeCMS.GetClientIP()
		Dim Fn_Username : Fn_Username = KnifeCMS.ParseUserName(KnifeCMS.GetForm("post","username"))
		Dim Fn_Password : Fn_Password = KnifeCMS.ParsePassWord(KnifeCMS.GetForm("post","password"))
		If KnifeCMS.Data.IsNul(Fn_Username) Or KnifeCMS.Data.IsNul(Fn_Password) Then SysUserLogining="null" : Exit Function
		Set Fn_Rs=KnifeCMS.DB.GetRecord(DBTable_SysUser &":ID,Username,Password,GroupID,UserDisable,Recycle",Array("Username:"& Fn_Username),"")
		If Fn_Rs.Eof Then
			Fn_ReturnString="null"
		Else
			If KnifeCMS.Data.CLng(Fn_Rs("Recycle"))=1 Or KnifeCMS.Data.CLng(Fn_Rs("UserDisable"))=1 Then
				Fn_ReturnString="disable"
				Call SaveLog(Fn_Username & Lang_LoginFail(1))
			Else
				If Fn_Password <> Fn_Rs("Password") Then
				    Fn_ReturnString = "errorpassword"
					Call SaveLog(Fn_Username & Lang_LoginFail(0))
				Else
					SysUserID       = Fn_Rs("ID")
					SysUsername     = Fn_Rs("Username")
					SysUserPassword = Fn_Rs("Password")
					SysUserGroupID  = Fn_Rs("GroupID")
					SysUserTruePassword = OS.UpdateTruePassword(DBTable_SysUser,SysUserID)
					'写入用户cookies
					If SysUserTruePassword <> False Then Fn_ReturnString = "ok" : Call SetSysUserCookies(SysUserID,SysUsername,SysUserTruePassword,SysUserGroupID)
				End If
			End If
		End If
		KnifeCMS.DB.CloseRs Fn_Rs
		If Fn_ReturnString = "ok" Then
			'更新登陆次数\登陆时间\IP
			Call KnifeCMS.DB.UpdateRecord(DBTable_SysUser,Array("LoginTimes=LoginTimes+1","LastLoginTime:"& SysTime,"IP:"& Fn_IP),Array("ID:"& SysUserID))
			'写入用户session
			Set SysUserSession=KnifeCMS.CreateXmlDoc("msxml2.FreeThreadedDOMDocument"& MsxmlVersion)
			SysUserSession.Loadxml "<?xml version=""1.0"" encoding=""gb2312""?><xml><userinfo userid="""& SysUserID &""" username="""& SysUsername &""" userpassword="""& SysUserPassword &""" truepassword="""& SysUserTruePassword &""" usergroupid="""& SysUserGroupID &""" cometime="""" activetime="""" statuserid=""0"" /></xml>"
			SysUserSession.documentElement.selectSingleNode("userinfo/@cometime").text=Now()
			SysUserSession.documentElement.selectSingleNode("userinfo/@activetime").text=DateAdd("s",-3600,Now())
			Call KnifeCMS.Session.SetSession("SysUserID",SysUserSession.xml)
			Call SaveLog(Lang_LoginSuccess)
		End If
		SysUserLogining = Fn_ReturnString
	End Function
	
	'管理员退出
	Public Sub SysUserLoginOut()
		SysUserID           = ""
		SysUsername         = ""
		SysUserTruePassword = ""
		SysUserGroupID      = ""
		KnifeCMS.Cookie.RemoveCookie("sysuser:id")
		KnifeCMS.Cookie.RemoveCookie("sysuser:name")
		KnifeCMS.Cookie.RemoveCookie("sysuser:pw")
		KnifeCMS.Session.RemoveSession("SysUserID")
		Admin.ShowMessage Lang_SysUser(12),Array("login.asp",Lang_Login),1
	End Sub
	
	Public Function SetSysUserCookies(ByVal BV_UserID, ByVal BV_Username, ByVal BV_Password, ByVal BV_GroupID)
		Call KnifeCMS.Cookie.SetCookie("sysuser:id",BV_UserID,KnifeCMS.Cookie.LoginCookieTime)
		Call KnifeCMS.Cookie.SetCookie("sysuser:name",BV_Username,KnifeCMS.Cookie.LoginCookieTime)
		Call KnifeCMS.Cookie.SetCookie("sysuser:pw",BV_Password,KnifeCMS.Cookie.LoginCookieTime)
		Call KnifeCMS.Cookie.SetCookie("sysuser:groupid",BV_GroupID,KnifeCMS.Cookie.LoginCookieTime)
	End Function
	
	Public Function SysUserCheck()
		Dim Fn_SQL
		SysUserID           = KnifeCMS.Data.CLng(KnifeCMS.Cookie.GetCookie("sysuser:id"))
		SysUsername         = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.Data.RegReplace(KnifeCMS.Cookie.GetCookie("sysuser:name"),"[\(\)]",""),"[\s]",""),50)
		SysUserTruePassword = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.Cookie.GetCookie("sysuser:pw"),"[^0-9a-zA-Z]",""),32)
		SysUserGroupID      = KnifeCMS.Data.CLng(KnifeCMS.Cookie.GetCookie("sysuser:groupid"))
		'判断Cookie记录是否有效
		If Not(SysUserID>0) Or KnifeCMS.Data.IsNul(SysUsername) Or KnifeCMS.Data.IsNul(SysUserTruePassword) Then
			ShowMessage Lang_NotLogin,Array("login.asp",Lang_Login),0 : die ""
		End If

		Select Case KnifeCMS.Data.CLng(KnifeCMS.SysConfig("AdminCookieType"))
		Case 0
		'Cookie+Session双保险模式
			Set SysUserSession=KnifeCMS.CreateXmlDoc("msxml2.FreeThreadedDOMDocument"& MsxmlVersion)
			'判断是否有该用户的session记录，没有session记录或session记录与cookie记录不一致则为非登陆状态{
			If Not SysUserSession.loadxml(KnifeCMS.Session.GetSession("SysUserID") & "") Then
				ShowMessage Lang_NotLogin,Array("login.asp",Lang_Login),0 : die ""
			End If
			If KnifeCMS.Data.CLng(SysUserSession.documentElement.selectSingleNode("userinfo/@userid").text)<>SysUserID Or SysUserSession.documentElement.selectSingleNode("userinfo/@username").text<>SysUsername Or SysUserSession.documentElement.selectSingleNode("userinfo/@truepassword").text<>SysUserTruePassword Then
				ShowMessage Lang_NotLogin,Array("login.asp",Lang_Login),0 : die ""
			End If
			'}判断完毕
		Case 1
		'Cookie保险模式
		End Select
		
		Select Case DB_Type
		Case 0
			Fn_SQL = "SELECT a.Username,a.TruePassword,a.UserDisable,a.Recycle,a.GroupID,b.GroupName,b.Degree,c.ID,c.Username FROM (["& DBTable_SysUser &"] a LEFT JOIN ["& DBTable_SysUserGroup &"] b ON (b.ID=a.GroupID)) LEFT JOIN ["& DBTable_Members &"] c ON (c.ID=a.MemberID) WHERE a.ID="& SysUserID &""
			
		Case 1
			Fn_SQL = "SELECT a.Username,a.TruePassword,a.UserDisable,a.Recycle,a.GroupID,b.GroupName,b.Degree,c.ID,c.Username FROM ["& DBTable_SysUser &"] a LEFT JOIN ["& DBTable_SysUserGroup &"] b ON (b.ID=a.GroupID) LEFT JOIN ["& DBTable_Members &"] c ON (c.ID=a.MemberID) WHERE a.ID="& SysUserID &""
		End Select
		Set Rs=KnifeCMS.DB.GetRecordBySQL(Fn_SQL)
		If Rs.eof then
			ShowMessage Lang_NotLogin,Array("login.asp",Lang_Login),0 : die ""
		Else
			if Rs(0)<>SysUsername or Rs(1)<>SysUserTruePassword then
				ShowMessage Lang_NotLogin,Array("login.asp",Lang_Login),0 : die ""
			end if
			if Rs(2)<>0 then '该用户已被禁用
				ShowMessage Lang_NotLogin,Array("login.asp",Lang_Login),0 : die ""
			end if
			if Rs(3)<>0 then '该用户已被删除
				ShowMessage Lang_NotLogin,Array("login.asp",Lang_Login),0 : die ""
			end if
			'取得系统用户组数据
			If KnifeCMS.data.IsNul(Rs("GroupName")) Then
				'非法的系统用户
				ShowMessage Lang_IllegalSysUser,Array("login.asp",Lang_Login),0 : die ""
			Else
				SysUserGroupID   = Rs("GroupID")
				SysUserGroupName = Rs("GroupName")
				SysUserDegree    = Rs("Degree")
			End If
			'取得唯一与配对的前台会员ID帐号密码
			If KnifeCMS.data.IsNul(Rs(8)) Then
				'非法的系统用户
				ShowMessage Lang_IllegalSysUser,Array("login.asp",Lang_Login),0 : die ""
			Else
				UserID       = Rs(7)
				Username     = Rs(8)
			End If
		End If
		Set Rs=nothing
		KnifeCMS.DB.CloseRs Rs
		Admin.SysUserPermissionCheck
	End Function
	
	Public Function SysUserPermissionCheck()
		Dim Temp_IsAllow : Temp_IsAllow = 0
		Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT IsAllow FROM ["& DBTable_SysUserPermission &"] WHERE GroupID="& SysUserGroupID &" AND SystemCtl='"& Ctl &"' AND SystemAction='"& Action &"'")
		If Not(Rs.Eof) Then
			If Rs(0) Then
				Temp_IsAllow = 1
			End If
		End If
		KnifeCMS.DB.CloseRs Rs
		If Temp_IsAllow = 0 Then
			If Right(KnifeCMS.GetUrl(""),9)="index.asp" Then
				Response.Redirect("index.asp?ctl=systemindex&act=view")
			Else
				ShowMessage Lang_NoPermissions,"goback",0 : Die ""
			End If
		End If
	End Function
	
	'权限检测
	'Admin.PermissionCheck(Array("systemset","view"))
	Public Function PermissionCheck(ByVal BV_Opt)
		Dim Fn_Rs,Fn_Ctl,Fn_Action,Fn_Result
		Fn_Ctl    = BV_Opt(0)
		Fn_Action = BV_Opt(1)
		If Not(SysUserGroupID>0) Or Fn_Ctl="" Or Fn_Action="" Then PermissionCheck = False : Exit Function
		Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT IsAllow FROM ["& DBTable_SysUserPermission &"] WHERE GroupID="& SysUserGroupID &" AND SystemCtl='"& Fn_Ctl &"' AND SystemAction='"& Fn_Action &"'")
		If Not(Fn_Rs.Eof) Then
			Fn_Result = True
		Else
			Fn_Result = False
		End If
		KnifeCMS.DB.CloseRs Rs
		PermissionCheck = Fn_Result
	End Function
	
	'显示操作菜单
	'Admin.PrintPermissionMenu(Array("systemset","view"),"系统设置")
	Public Function PrintPermissionMenu(ByVal BV_Opt,ByVal BV_String)
		If PermissionCheck(BV_Opt)=True Then Echo BV_String
	End Function
	
End Class


%>
