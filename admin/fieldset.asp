<!--#include file="config.admin.asp"-->
<%
Dim FieldSet
Set FieldSet = New Class_KnifeCMS_Admin_FieldSet
Set FieldSet = Nothing
Class Class_KnifeCMS_Admin_FieldSet

	Private Pv_Result
	Private Pv_Field,Pv_FieldName,Pv_FieldType,Pv_System,Pv_NotNull,Pv_FieldOptions,Pv_FieldTips,Pv_OrderNum,Pv_Recycle
	Private Pv_IsShowOptions,Pv_DataType,Pv_TempField
	
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "fieldset.lang.asp"
		Pv_IsShowOptions = False
		Header()
		If Ctl="fieldset" Then
			Select Case Action
				Case "view"
					Call FieldSetView()
				Case "add"
					If SubAction="save" Then
						Call FieldSetDoSave()
					Else
						Pv_OrderNum = 9999
						Call FieldSetDo()
					End If
				Case "edit"
					If SubAction="save" Then
						Call FieldSetDoSave()
					Else
						Call GetFieldSetDetail()
						If Pv_FieldType="radio" Or Pv_FieldType="checkbox" Or Pv_FieldType="select" Then
							Pv_IsShowOptions=True
						End If
						If IsContentExist Then Call FieldSetDo() : Else Admin.ShowMessage Lang_Field(13),"goback",0
					End If
				Case "del"
					Call FieldSetDel()
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"goback",0
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Sub GetFieldSetDetail()
		Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_FieldSet &"] WHERE ID="& ID &"")
		If Not(Rs.Eof) Then
			IsContentExist = True
			Pv_Field       = Rs("Field")
			Pv_FieldName   = Rs("FieldName")
			Pv_FieldType   = Rs("FieldType")
			Pv_System      = Rs("System")
			Pv_NotNull     = Rs("NotNull")
			Pv_FieldOptions= Rs("FieldOptions")
			Pv_FieldTips   = Rs("FieldTips")
			Pv_OrderNum    = Rs("OrderNum")
			Pv_Recycle     = Rs("Recycle")
		Else
			IsContentExist = False
		End IF
		KnifeCMS.DB.CloseRs Rs
	End Sub
	
	Private Function FieldSetList()
		Dim Fn_TempSql,Fn_ColumnNum
		Fn_TempSql   = "SELECT a.ID,a.Field,a.FieldName,b.SystemName,a.FieldType,a.NotNull,a.Field,a.OrderNum FROM ["& DBTable_FieldSet &"] a LEFT JOIN "& DBTable_ContentSubSystem &" b ON (b.System=a.System) WHERE a.Recycle=0"
		Fn_ColumnNum = 8
		Fn_TempSql   = Fn_TempSql & " ORDER BY a.ID DESC"
		Operation    = "<span class=""sysBtn""><a href=""?ctl="& Ctl &"&act=edit&id={$ID}"" ><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		Call Admin.DataList(Fn_TempSql,50,Fn_ColumnNum,Operation,PageUrlPara)
	End Function
	
	Private Function CreateFieldDataType(ByVal BV_FieldType)
		Dim Fn_Return
		Select Case DB_Type
		Case 0
			If BV_FieldType="editor" Then
				Fn_Return = "memo"
			ElseIf BV_FieldType="number" Then
				Fn_Return = "number"
			ElseIf BV_FieldType="datetime" Then
				Fn_Return = "date"
			ElseIf BV_FieldType="checkbox" Then
				Fn_Return = "memo"
			Else
				Fn_Return = "text (250)"
			End If
		Case 1
			If BV_FieldType="editor" Then
				Fn_Return = "[ntext]"
			ElseIf BV_FieldType="number" Then
				Fn_Return = "[int]"
			ElseIf BV_FieldType="datetime" Then
				Fn_Return = "[datetime]"
			ElseIf BV_FieldType="checkbox" Then
				Fn_Return = "[ntext]"
			Else
				Fn_Return = "[nvarchar] (250)"
			End If
		End Select
		CreateFieldDataType = Fn_Return
	End Function
	
	Private Function FieldSetDoSave()
		'获取数据并处理数据
		Pv_Field        = LCase(KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","Field"),"[^0-9a-zA-Z_]",""),30))
		Pv_FieldName    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","FieldName")),250)
		Pv_FieldType    = LCase(KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","FieldType"),"[^a-zA-Z]",""),30))
		Pv_System       = LCase(KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","System"),"[^0-9a-zA-Z]",""),30))
		
		Pv_FieldOptions = KnifeCMS.GetForm("post","FieldOptions")
		Pv_FieldOptions = Replace(Pv_FieldOptions,Chr(10),"|")
		Pv_FieldOptions = Replace(Pv_FieldOptions,Chr(13),"")
		Pv_FieldOptions = Replace(Pv_FieldOptions,"<br>","")
		Pv_FieldOptions = Replace(Pv_FieldOptions,"<br/>","")
		
		Pv_FieldTips    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","FieldTips")),250)
		
		Pv_NotNull      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","NotNull"))
		Pv_OrderNum     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","OrderNum"))
		'数据合法性检测并处理数据
		If KnifeCMS.Data.IsNul(Pv_Field)     Then ErrMsg = Lang_Field(9) &"<br>"
		If KnifeCMS.Data.IsNul(Pv_FieldName) Then ErrMsg = ErrMsg & Lang_Field(10) &"<br>"
		If Action="add" And KnifeCMS.Data.IsNul(Pv_System)    Then ErrMsg = ErrMsg & Lang_Field(11) &"<br>"
		If Action="add" And KnifeCMS.Data.IsNul(Pv_FieldType) Then ErrMsg = ErrMsg & Lang_Field(12) &"<br>"
		
		Pv_Field = KnifeCMS.DB.FieldPre & Pv_Field
		
		If Action="add" Then
			'判断字段是否已经存在
			If KnifeCMS.DB.CheckIsRecordExistByField(DBTable_FieldSet,Array("Field:"& Pv_Field,"System:"& Pv_System)) Then
				ErrMsg = ErrMsg & Lang_Field(23) &"<br>"
			End If
			If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act=add",0 : Exit Function
			'在对应的表中增加该字段
			Pv_DataType = CreateFieldDataType(Pv_FieldType)
			If KnifeCMS.DB.IsColumnExist(TablePre &"Content_"& Pv_System,Pv_Field) Then
				Result = 1
			Else
				Result = KnifeCMS.DB.AddColumn(TablePre &"Content_"& Pv_System,"["& Pv_Field &"] "& Pv_DataType &" NULL")
			End If
			'添加记录
			If Result Then
				Result = KnifeCMS.DB.AddRecord(DBTable_FieldSet,Array("Field:"& Pv_Field,"FieldName:"& Pv_FieldName,"FieldType:"& Pv_FieldType,"System:"& Pv_System,"NotNull:"& Pv_NotNull,"FieldOptions:"& Pv_FieldOptions,"FieldTips:"& Pv_FieldTips,"OrderNum:"& Pv_OrderNum,"Recycle:0"))
			End If
			'增加自定义字段结束，输出结果信息
			If Result Then
				ID  = KnifeCMS.DB.GetMaxID(DBTable_FieldSet)
				Msg = Lang_Field(13) &"<br>[ID:"& ID &"]["& Lang_Field(2) &":"& Pv_FieldName &"]"
				Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
			Else
				Msg = Lang_Field(14) &"<br>[ID:"& ID &"]["& Lang_Field(2) &":"& Pv_FieldName &"]"
				Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
			End If
		ElseIf Action="edit" Then
			If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"?ctl="& Ctl &"&act=add",0 : Exit Function
			Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_FieldSet &"] WHERE ID="& ID &"")
			If Not(Rs.Eof) Then
				IsContentExist = True
				Pv_TempField   = Rs("Field")
				Pv_FieldType   = Rs("FieldType")
				Pv_System      = Rs("System")
			Else
				IsContentExist = False
			End IF
			KnifeCMS.DB.CloseRs Rs
			If IsContentExist Then
				If Pv_TempField<>Pv_Field Then
					Select Case DB_Type
					Case 0
					'access:修改字段名
						Pv_DataType = CreateFieldDataType(Pv_FieldType)
						Call KnifeCMS.DB.AddColumn(TablePre &"Content_"& Pv_System,"["& Pv_Field &"] "& Pv_DataType &" NULL")
						Call KnifeCMS.DB.CopyColumnData(TablePre &"Content_"& Pv_System,Pv_TempField,Pv_Field)
						Call KnifeCMS.DB.DeleteColumn(TablePre &"Content_"& Pv_System,Pv_TempField)
					Case 1
					'mssql:修改字段名
						Call KnifeCMS.DB.EditColumn(TablePre &"Content_"& Pv_System,Pv_TempField,Pv_Field)
					End Select
				End If
				Result = KnifeCMS.DB.UpdateRecord(DBTable_FieldSet,Array("Field:"& Pv_Field,"FieldName:"& Pv_FieldName,"NotNull:"& Pv_NotNull,"FieldOptions:"& Pv_FieldOptions,"FieldTips:"& Pv_FieldTips,"OrderNum:"& Pv_OrderNum),Array("ID:"&ID))
			End If
			If Result Then
				Msg = Lang_Field(15) &"<br>[ID:"& ID &"]["& Lang_Field(2) &":"& Pv_FieldName &"]"
				If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
			Else
				Msg = Lang_Field(16) &"<br>[ID:"& ID &"]["& Lang_Field(2) &":"& Pv_FieldName &"]"
				If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
			End If
		End If
	End Function
	
	Private Function FieldSetDel()
		ID=KnifeCMS.GetForm("post","InputName")
		If KnifeCMS.Data.IsNul(Replace(ID,",","")) Then Exit Function
		Dim Fn_ArrID,Fn_ID,Fn_i,Fn_Rs
		Fn_ArrID=Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID=KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			If ID>0 Then
				Set Fn_Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_FieldSet &"] WHERE ID="& ID &"")
				If Not(Fn_Rs.Eof) Then
					IsContentExist = True
					Pv_Field       = Fn_Rs("Field")
				    Pv_FieldName   = Fn_Rs("FieldName")
					Pv_System      = Fn_Rs("System")
				Else
					IsContentExist = False
				End If
				KnifeCMS.DB.CloseRs Fn_Rs
				'开始执行操作
				If Action = "del" And IsContentExist Then
					Result = KnifeCMS.DB.DeleteRecord(DBTable_FieldSet,Array("ID:"&ID))
					If Result Then
						Call KnifeCMS.DB.DeleteColumn(TablePre &"Content_"& Pv_System,Pv_Field)
						Msg = Msg & Lang_Field(17) &"[ID:"& ID &"]["& Lang_Field(1) &":"& Pv_Field &"]["& Lang_Field(2) &":"& Pv_FieldName &"]<br>"
					Else
						Msg = Msg & Lang_Field(18) &"[ID:"& ID &"]["& Lang_Field(1) &":"& Pv_Field &"]["& Lang_Field(2) &":"& Pv_FieldName &"]<br>"
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Function
	
    Function FieldSetView()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
          <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=add" ><b class="icon icon_add"></b><% echo Lang_DoAdd %></a></span>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%=KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></th>
			<th><div><% Echo Lang_ListTableColLine_ID %></div></th>
			<th><div><% Echo Lang_Field(1) %></div></th>
			<th><div><% Echo Lang_Field(2) %></div></th>
            <th><div><% Echo Lang_Field(4) %></div></th>
			<th><div><% Echo Lang_Field(3) %></div></th>
			<th><div><% Echo Lang_Field(5) %></div></th>
            <th><div><% Echo Lang_Field(8) %></div></th>
            <th><div><% Echo Lang_Field(7) %></div></th>
			<th><div><% Echo Lang_ListTableColLine_Do %></div></th>
			</tr>
			<% Call FieldSetList() %>
		   </tbody>
		  </table>
		  </form>
		</div>
      </DIV>
    </DIV>
    <script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var fn_temp;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td0"){
					fn_temp=tds[i].innerHTML;
					tds[i].innerHTML='<a href="?ctl=<%=Ctl%>&act=edit&id='+tds[i].title+'" target=_self>'+fn_temp+'</a>';
				}
				if(tds[i].id=="td1"){
					fn_temp=tds[i].innerHTML;
					tds[i].innerHTML='<a href="?ctl=<%=Ctl%>&act=edit&id='+tds[i-1].title+'" target=_self>'+fn_temp+'</a>';
				}
				if(tds[i].id=="td3"){
					fn_temp=tds[i].innerHTML;
					//tds[i].innerHTML='<a href="?ctl=<%=Ctl%>&act=view&subact=search&system='+tds[i-1].innerHTML+'" target=_self>'+fn_temp+'</a>';
				}
				if(tds[i].id=="td5"){
					fn_temp=tds[i].innerHTML;
					if(parseInt(fn_temp)>0){
						tds[i].innerHTML="<%=Lang_MustFillIn%>";
					}else{
						tds[i].innerHTML="<%=Lang_CanNull%>";
					}
				}
				if(tds[i].id=="td6"){
					fn_temp=tds[i].innerHTML;
					tds[i].style.padding="5px";
					tds[i].style.color="#666666";
					tds[i].innerHTML='<%=Lang_Field(27)%><span class="templatelabel">[contentlist:'+fn_temp+']</span><br><%=Lang_Field(28)%><span class="templatelabel">{content:'+fn_temp+'}</span>';
				}
			}
		}
	}
	ChangeTdText();
	</script>
<%
    End Function
	
	'添加界面
	Private Function FieldSetDo()
	%>
	<DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
          <form name="SaveForm" action="?ctl=<%=Ctl%>&act=<%=Action%>&subact=save" method="post">
          <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
            <tbody>
               <tr><td class="titletd"><span class="required">*</span><%=Lang_Field(4)%>:</td>
                   <td class="infotd">
                   <select id="System" name="System" style="font-size:13px;" <%=KnifeCMS.IIF(Action="edit","disabled=""disabled""","")%>>
                   <% Admin.GetContentSubSystemOption 0,Pv_System %>
                   </select>
                   <label class="Normal" id="d_System"></label>
                   </td></tr>
               <tr><td class="titletd"><span class="required">*</span><%=Lang_Field(1)%>:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass readonly" name="Field_Pre" id="Field_Pre" readonly="readonly" style="margin:0px 2px 0px 0px; width:18px; text-align:center;" value="<%=KnifeCMS.DB.FieldPre%>"><input type="text" class="TxtClass" name="Field" id="Field" maxLength="30" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9a-zA-Z_]*/g','maxLength:30'))" style="width:223px;" value="<% Echo Mid(Pv_Field,3) %>"/>
                   <label class="Normal" id="d_Field"><%=Lang_Field(24)%></label>
                   </td>
               </tr>
               <tr><td class="titletd"><span class="required">*</span><%=Lang_Field(2)%>:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" name="FieldName" id="FieldName" maxLength="200"  style="width:250px;" value="<% Echo Pv_FieldName %>"/>
                   <label class="Normal" id="d_FieldName"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><span class="required">*</span><%=Lang_Field(3)%>:</td>
                   <td class="infotd">
                   <select name="FieldType" id="FieldType" onchange="Field_Setting(this);" style="font-size:13px;" <%=KnifeCMS.IIF(Action="edit","disabled=""disabled""","")%>>
                   <%=OS.CreateOption(Array("0:"& Lang_Field(22),"text:"& Lang_FieldType(1),"textarea:"& Lang_FieldType(2),"editor:"& Lang_FieldType(3),"radio:"& Lang_FieldType(4),"checkbox:"& Lang_FieldType(5),"select:"& Lang_FieldType(6),"number:"& Lang_FieldType(7),"datetime:"& Lang_FieldType(8),"image:"& Lang_FieldType(9),"file:"& Lang_FieldType(10)),Pv_FieldType)%>
                   </select>
                   <label class="Normal" id="d_FieldType"></label>
                   </td></tr>
               <tr><td class="titletd"><%=Lang_Field(5)%>:</td>
                   <td class="infotd">
                   <input type="checkbox" id="NotNull" name="NotNull" <%=Admin.InputChecked(Pv_NotNull)%> value="1"><%=Lang_Field(6)%>
                   <label class="Normal" id="d_Pv_NotNull"></label>
                   </td></tr>
            </tbody>
            </table>
          </div>
          <div class="inbox" id="FieldOptions_Grid" <% If Pv_IsShowOptions=False Then Echo"style=""display:none;""" %>>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
            <tbody>
               <tr><td class="titletd"><%=Lang_Field(20)%>:</td>
                   <td class="infotd">
                   <textarea class="info" name="FieldOptions" id="FieldOptions"><%=Replace(Pv_FieldOptions,"|",Chr(10))%></textarea>
                   <div><label class="Normal" id="d_FieldOptions"><%=Lang_Field(21)%></label></div>
                   </td>
               </tr>
             </tbody>
            </table>
          </div>
          <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
            <tbody>
               <tr><td class="titletd"><%=Lang_Field(25)%>:</td>
                   <td class="infotd">
                   <textarea class="mini" name="FieldTips" id="FieldTips" maxLength="250"><% Echo Pv_FieldTips %></textarea>
                   <label class="Normal" id="d_FieldTips"><%=Lang_Field(26)%></label>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_Field(7)%>:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" name="OrderNum" id="OrderNum" maxlength="10" value="<%=Pv_OrderNum%>" style="width:60px;"/>
                   <label class="Normal" id="d_OrderNum"></label>
                   </td>
               </tr>
             </tbody>
            </table>
          </div>
          <div class="bottomSaveline" id="SubmitButtoms" style="text-align:left; padding-left:160px;">
            <input type="hidden" name="id" value="<%=ID%>" />
            <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="SaveFieldSetCheck('<%=Action%>')"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
            <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
          </div>
          </form>
        </div>
      </DIV>
	</DIV>
    <script type="text/javascript">
    var SaveFieldSetCheck = function(){
		var id="<%=ID%>";
		var Check=true;
		var Action = "<%=Action%>";
		if(!Admin.isHaveSelect("System","d_System",Lang_Js_HaveNotSelect)){Check=false}
		if(!Admin.CheckItem("Field","d_Field",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("FieldName","d_FieldName",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.isHaveSelect("FieldType","d_FieldType",Lang_Js_HaveNotSelect)){Check=false}
		if(Check){
			var ajaxString  = "id="+id+"&Field=<%=KnifeCMS.DB.FieldPre%>"+KnifeCMS.$id("Field").value+"&System="+KnifeCMS.$id("System").options[KnifeCMS.$id("System").selectedIndex].value;
			var ajaxUrl     = "ajax.get.asp?ctl=<%=Ctl%>&act=view&subact=checkfield&r="+parseInt(Math.random()*1000);
			var ajaxSta     = posthttp(ajaxString,ajaxUrl);
			if(ajaxSta!="ok"){
				Check = false;
				KnifeCMS.$id("d_Field").className="d_err";
				KnifeCMS.$id("d_Field").innerHTML="<%=Lang_Field(23)%>";
			}
			if(Check){
				Admin.DataSubmiting();
				document.forms["SaveForm"].submit();
			}
		}
	}
	var Field_Setting = function(_this){
		var fieldtpye=_this.options[_this.selectedIndex].value;
		if(fieldtpye=="radio" || fieldtpye=="checkbox" || fieldtpye=="select"){
			KnifeCMS.$id("FieldOptions_Grid").style.display="";
		}else{
			KnifeCMS.$id("FieldOptions_Grid").style.display="none";
		}
	}
    </script>
	<%
	End Function
End Class
%>
