<!--#include file="config.admin.asp"-->
<%
Dim Payment
Set Payment = New Class_KnifeCMS_Admin_Payment
Set Payment = Nothing
Class Class_KnifeCMS_Admin_Payment

	Private Pv_Len,Pv_TempHTML
	Private PayType,PaymentName,Config,Config_ItemNum,PayMethod,Fee,PaymentDes,OrderNum
	Private SellerEmail,Partner,PrivateKey,InterFaceType
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "Payment.lang.asp"
		IsContentExist     = False
		Config_ItemNum    = 0
		PayMethod         = 1
		Header()
		If Ctl = "paymentconfig" Then
			Select Case Action
				Case "view"
					PaymentConfigView
				Case "add","edit"
					If SubAction="save" Then
						PaymentConfigDoSave
					Else
						If Action = "add" Then
							PaymentConfigDo
						ElseIf Action = "edit" Then
							If SubAction = "disabled" Or SubAction = "undisabled" Then
								PaymentConfigOtherEdit
							Else
								'Set Rs = KnifeCMS.DB.GetRecord(DBTable_Payments,Array("ID:"&ID),"")
								Set Rs = KnifeCMS.DB.GetRecordBySQL("SELECT * FROM ["& DBTable_Payments &"] WHERE ID="& ID &"")
								If Not(Rs.Eof) Then
									IsContentExist = True
									PayType       = Rs("PayType")
									PaymentName   = Rs("PaymentName")
									Config        = Rs("Config")
									PayMethod     = Rs("PayMethod")
									Fee           = Rs("Fee")
									PaymentDes    = Rs("PaymentDes")
									OrderNum      = Rs("OrderNum")
									PayMethod     = KnifeCMS.Data.CLng(PayMethod)
									Fee = KnifeCMS.Data.FormatDouble(KnifeCMS.IIF(PayMethod="1",Fee * 100,Fee))
									If PayType = "alipay" Then
										SellerEmail   = KnifeCMS.GetConfigParameter(Config,"SellerEmail")
										Partner       = KnifeCMS.GetConfigParameter(Config,"Partner")
										PrivateKey    = KnifeCMS.GetConfigParameter(Config,"PrivateKey")
										InterFaceType = KnifeCMS.GetConfigParameter(Config,"InterFaceType")
									End If
								End IF
								KnifeCMS.DB.CloseRs Rs
								If IsContentExist Then PaymentConfigDo : Else Admin.ShowMessage Lang_Payment_Cue(7),"goback",0
							End IF
						End IF
					End IF
				Case "del"  : PaymentConfigDel
			End Select
		Else
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function PaymentConfigList()
		Dim Fn_TempSql,Fn_ColumnNum
		Fn_ColumnNum = 6
		Select Case DB_Type
		Case 0
		Fn_TempSql   = "SELECT a.ID,a.PaymentName,a.PayMethod,IIF(a.PayMethod=1,a.Fee * 100,a.Fee),a.Disabled,a.OrderNum FROM ["& DBTable_Payments &"] a "
		Case 1
		Fn_TempSql   = "SELECT a.ID,a.PaymentName,a.PayMethod,CASE WHEN a.PayMethod=1 THEN a.Fee * 100 ELSE a.Fee END,a.Disabled,a.OrderNum FROM ["& DBTable_Payments &"] a "
		End Select
		Fn_TempSql   = Fn_TempSql & "ORDER BY a.Disabled ASC, a.OrderNum ASC, a.ID ASC"
		Operation    = "<span class=""sysBtn""><a href=""javascript:void(0);"" onClick=""ShowModal('payment.asp?ctl="&Ctl&"&act=edit&id={$ID}',960,600);ShowModalReload();""><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
	End Function
	
	Private Function PaymentConfigDoSave()
		'获取数据并处理数据
		PayType     = KnifeCMS.Data.Left(KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","PayType"),"[^0-9a-z]",""),10)
		PaymentName = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","PaymentName")),100)
		PayMethod   = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","PayMethod"))
		Fee         = KnifeCMS.Data.FormatDouble(KnifeCMS.GetForm("post","Fee"))
		PaymentDes  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","TextContent")),10000)
		OrderNum    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","OrderNum"))
		
		SellerEmail   = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","SellerEmail")),100)
		Partner       = KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","Partner"),"[^0-9a-zA-Z_-]","")
		PrivateKey    = KnifeCMS.Data.RegReplace(KnifeCMS.GetForm("post","PrivateKey"),"[^0-9a-zA-Z_-]","")
		InterFaceType = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","InterFaceType"))
		'die PayType
		If PayType     = "offline" then
			Config         = "a:0:{}"
		Else
			If PayType     = "alipay" Then Config_ItemNum = 3
			Config         = "a:"& Config_ItemNum &":{s:11:""SellerEmail"";s:"&Len(SellerEmail)&":"""&SellerEmail&""";s:9:""Partner"";s:"&Len(Partner)&":"""&Partner&""";s:10:""PrivateKey"";s:"&Len(PrivateKey)&":"""&PrivateKey&""";s:11:""InterFaceType"";s:"&Len(InterFaceType)&":"""&InterFaceType&""";}"
		End If
		
		'数据合法性检测并处理数据
		Fee = KnifeCMS.IIF(PayMethod = 1, Fee/100 ,Fee)
		If Action="add" And Len(PayType) < 2  Then ErrMsg = ErrMsg & Lang_Payment_Cue(9) &"<br>"
		If KnifeCMS.Data.IsNul(PaymentName)       Then ErrMsg = ErrMsg & Lang_Payment_Cue(8) &"<br>"
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		IF Action="add" Then
			Result = KnifeCMS.DB.AddRecord(DBTable_Payments,Array("PaymentName:"&PaymentName,"PayType:"&PayType,"Config:"&Config,"PayMethod:"&PayMethod,"Fee:"&Fee,"PaymentDes:"& PaymentDes,"OrderNum:"&OrderNum,"Disabled:0"))
			Msg = Lang_Payment_Cue(1) &"["& PaymentName &"]"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		ElseIf Action="edit" Then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_Payments,Array("PaymentName:"&PaymentName,"Config:"&Config,"PayMethod:"&PayMethod,"Fee:"&Fee,"PaymentDes:"& PaymentDes,"OrderNum:"&OrderNum),Array("ID:"&ID))
			Msg = Lang_Payment_Cue(2) &"{"& PaymentName &"[ID="& ID &"]}"
			If Result Then Admin.ShowMessage Msg,Array("javascript:WinClose();",Lang_WinClose),1
		End IF
	End Function
	
	Private Function PaymentConfigDel()
		PaymentConfigSubDo
	End Function
	Private  Function PaymentConfigOtherEdit()
		PaymentConfigSubDo
	End Function
	
	Private Function PaymentConfigSubDo()
		ID = KnifeCMS.GetForm("post","InputName")
		If KnifeCMS.Data.IsNul(Replace(ID,",","")) Then Exit Function
		Dim Fn_ArrID,Fn_ID,Fn_i,Fn_Rs
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			If ID > 0 Then
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_Payments&":PaymentName",Array("ID:"&ID),"")
				If Not(Fn_Rs.Eof) Then PaymentName=Fn_Rs(0)
				KnifeCMS.DB.CloseRs Fn_Rs
				'开始执行操作
				If Action = "del" Then
					Result = KnifeCMS.DB.DeleteRecord(DBTable_Payments,Array("ID:"&ID))
					Msg    = Msg & Lang_Payment_Cue(4) &"{"& PaymentName &"[ID="& ID &"]}<br>"
				ElseIf Action = "edit" Then
					If SubAction = "undisabled" Then
						Result = KnifeCMS.DB.UpdateRecord(DBTable_Payments,Array("Disabled:0"),Array("ID:"&ID))
						Msg    = Msg & Lang_Payment_Cue(5) &"{"& PaymentName &"[ID="& ID &"]}<br>"
					ElseIf SubAction = "disabled" Then
						Result = KnifeCMS.DB.UpdateRecord(DBTable_Payments,Array("Disabled:1"),Array("ID:"&ID))
						Msg    = Msg & Lang_Payment_Cue(6) &"{"& PaymentName &"[ID="& ID &"]}<br>"
					End If
				End If
			End If
		Next
		If Not(KnifeCMS.Data.IsNul(Msg)) Then Admin.ShowMessage Msg,Url,1
	End Function
	
	Private Function PaymentConfigView()
%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
                  <span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=add"><b class="icon icon_add"></b><% Echo Lang_DoAdd&Lang_Payment(0) %></a></span>
				  </td></tr>
			  </table>
		  </td>
          <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:undisabled','')"><b class="icon icon_start"></b><% Echo Lang_DoOnUse %></button>
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:disabled','')"><b class="icon icon_stop"></b><% Echo Lang_DoStopUse %></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely %></button>
				  </td></tr>
			  </table>
		  </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th width="1%"><div style="width:50px;"><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
			<th width="20%"><div style="width:200px;"><% Echo Lang_Payment_ListTableColLine(0) %><!--支付方式名称--></div></th>
            <th width="10%"><div style="width:100px;"><% Echo Lang_Payment_ListTableColLine(4) %><!--手续费--></div></th>
            <th width="10%"><div style="width:100px;"><% Echo Lang_Payment_ListTableColLine(3) %><!--手续费--></div></th>
            <th width="10%"><div style="width:100px;"><% Echo Lang_Payment_ListTableColLine(1) %><!--状态--></div></th>
            <th width="5%"><div style="width:50px;"><% Echo Lang_Payment_ListTableColLine(2) %><!--排序--></div></th>
			<th><div style="width:60px;"><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% PaymentConfigList %>
		   </tbody>
		  </table>
		  </form>
		</div>
		</DIV>
	</DIV>
    <script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td2"){
					TempHTML = tds[i].innerHTML;
					if(TempHTML=="1"){
						tds[i].innerHTML="<% Echo Lang_Payment(4) %>";//按比例收费
						tds[i+1].innerHTML=tds[i+1].innerHTML+"%";
					}else if(TempHTML=="2"){
						tds[i].innerHTML="<% Echo Lang_Payment(5) %>";//固定费用
					}
				}
				if(tds[i].id=="td3"){
					TempHTML = tds[i].innerHTML;
					if(TempHTML=="1"){tds[i].innerHTML=TempHTML+"%";}
				}
				if(tds[i].id=="td4"){
					TempHTML = tds[i].innerHTML;
					if(TempHTML=="0"){tds[i].innerHTML="<span style=\"color:#048E08;\">"+Lang_Js_OnUse+"</span>";}else if(TempHTML=="1"){ tds[i].innerHTML="<span style=\"color:#CD2E03;\">"+Lang_Js_HaveStopUse+"</span>"}
				}
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
	Private Function PaymentConfigDo()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
          <div class="inbox">
            <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable mb5">
              <tbody>
               <tr><td class="titletd"><% Echo Lang_Payment(0) %><!--支付方式-->:</td>
                   <td class="infotd">
                   <select onchange="Admin.Payment.GetPayment(this);" id="PayType" name="PayType" <% If Action = "edit" Then Echo "disabled" %>>
                   <% Echo OS.CreateOption(Array("0:"& Lang_PleaseSelect &"","alipay:"& Lang_Payment_Alipay(0) &"","offline:"& Lang_Payment_OffLine(0) &"","cod:"& Lang_Payment_COD(0)),Split(PayType,",")) %>
                   </select>
                   <input type="hidden" name="PayType" value="<% Echo PayType %>" />
                   <label id="d_PayType"></label>
                   </td>
               </tr>
               </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
              <tbody>
                <tr><td class="titletd"><% Echo Lang_Payment(1) %><!--支付方式名称-->:</td>
                    <td class="infotd"><input type="text" class="TxtClass" id="PaymentName" name="PaymentName" style="width:220px;" value="<% Echo PaymentName %>" /><label id="d_PaymentName"></label></td>
                </tr>
              </tbody>
            </table>
            <span id="payment_config">
            <%
			If PayType = "alipay" Then
				Pv_TempHTML = "<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""infoTable""><tbody>"
				Pv_TempHTML = Pv_TempHTML & "<tr><td class=""titletd"">"& Lang_Payment_Alipay(1) &"<!--支持交易货币-->:</td><td class=""infotd"">"& Lang_Payment_Alipay(2) &"<!--人民币--></td></tr>"
				Pv_TempHTML = Pv_TempHTML & "<tr><td class=""titletd"">"& Lang_Payment_Alipay(9) &"<!--签约支付宝账号-->:</td><td class=""infotd""><input type=""text"" class=""TxtClass"" id=""SellerEmail"" name=""SellerEmail"" style=""width:220px;"" value="""& SellerEmail &""" /><label id=""d_SellerEmail""></label></td></tr>"
				Pv_TempHTML = Pv_TempHTML & "<tr><td class=""titletd"">"& Lang_Payment_Alipay(3) &"<!--合作者身份-->:</td><td class=""infotd""><input type=""text"" class=""TxtClass"" id=""Partner"" name=""Partner"" style=""width:220px;"" value="""& Partner &""" /><label id=""d_Partner""></label></td></tr>"
                Pv_TempHTML = Pv_TempHTML & "<tr><td class=""titletd"">"& Lang_Payment_Alipay(4) &"<!--交易安全校验码-->:</td><td class=""infotd""><input type=""text"" class=""TxtClass"" id=""PrivateKey"" name=""PrivateKey"" style=""width:300px;"" value="""& PrivateKey &""" /><label id=""d_PrivateKey""></label></td></tr>"
                Pv_TempHTML = Pv_TempHTML & "<tr><td class=""titletd"">"& Lang_Payment_Alipay(5) &"<!--选择接口类型-->:</td><td class=""infotd"">"
				
				Pv_TempHTML = Pv_TempHTML & "<SELECT type=""select"" name=""InterFaceType"" >"
				Pv_TempHTML = Pv_TempHTML & OS.CreateOption(Array("0:"& Lang_Payment_Alipay(6) &"","1:"& Lang_Payment_Alipay(7) &"","2:"& Lang_Payment_Alipay(8) &""),Split(InterFaceType,","))
				Pv_TempHTML = Pv_TempHTML & "</SELECT>"
				
				Pv_TempHTML = Pv_TempHTML & "<label id=""d_InterFaceType""></label></td></tr>"
				Pv_TempHTML = Pv_TempHTML & "</tbody></table>"
				Echo Pv_TempHTML
			End If
			%>
            </span>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
              <tbody>
                <tr><td class="titletd"><% Echo Lang_Payment(3) %><!--支付手续费设置-->:</td>
                   <td class="infotd">
                   <INPUT type="radio" name="PayMethod" onclick="ShowOrHidden('PayMethod_1:PayMethod_2','1:0')" <% Echo Admin.Input_Checked(PayMethod,"1") %> value="1" /><% Echo Lang_Payment(4) %><!--按比例收费-->&nbsp;&nbsp;&nbsp;
                   <INPUT type="radio" name="PayMethod" onclick="ShowOrHidden('PayMethod_1:PayMethod_2','0:1')" <% Echo Admin.Input_Checked(PayMethod,"2") %> value="2" /><% Echo Lang_Payment(5) %><!--固定费用-->&nbsp;&nbsp;&nbsp;
                   <label id="d_PayMethod"></label>
                   <div class="mt5 pt5" style="border-top:1px solid #f5f5f5;">
                   <div id="PayMethod_1" <% If PayMethod = "1" then Echo "style=""display:;""" Else Echo "style=""display:none;""" : End If %>>
				   <% Echo Lang_Payment(6) %><!--费率-->：<INPUT type="text" class="TxtClass" name="Fee" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g','maxLength:8'))" style="width:60px;" value="<% If PayMethod = "1" then Echo Fee %>" />%<label class="ml10 Normal"><% Echo Lang_Payment(7) %><!--说明：顾客将支付订单总金额乘以此费率作为手续费.--></label></div>
                   <div id="PayMethod_2" <% If PayMethod = "2" then Echo "style=""display:;""" Else Echo "style=""display:none;""" : End If %>>
				   <% Echo Lang_Payment(8) %><!--金额-->：<INPUT type="text" class="TxtClass" name="Fee" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9.]*/g','maxLength:8'))" style="width:60px;" value="<% If PayMethod = "2" then Echo Fee %>" /><label class="ml10 Normal"><% Echo Lang_Payment(9) %><!--说明：顾客每笔订单需要支付的手续费.--></label></div>
                   </div>
                   </td>
                </tr>
              </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
              <tbody>
               <tr><td class="titletd"><% Echo Lang_Payment(10) %><!--排序-->:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="OrderNum" name="OrderNum" style="width:60px;" value="<% Echo OrderNum %>" />
                   <label class="Normal"><% Echo Lang_Payment(11) %><!--数字越小越靠前.--></label>
                   </td>
               </tr>
               
               <tr><td class="titletd"><% Echo Lang_Payment(12) %><!--说明-->:</td>
                   <td class="infotd">
                   <textarea class="TextContent" id="TextContent" name="TextContent"><%=KnifeCMS.Data.HTMLDecode(PaymentDes)%></textarea>
                   <label class="Normal"></label>
                   </td>
               </tr>
               
              </tbody>
            </table>
            <input type="hidden" name="id" value="<%=ID%>" />
            <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
            <div class="bottomSaveline" id="SubmitButtoms">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Payment.SavePaymentCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
            </div>
            </form>
          </div>
        </div>
      </DIV>
    </DIV>
    <script type="text/javascript" charset="utf-8">
	window.UEDITOR_HOME_URL = "<%=SystemPath%>plugins/ueditor/";
	</script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_config.asp"></script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_api.js"></script>
    <script type="text/javascript">
	var OSWebEditor=UE.getEditor('TextContent');
	</script>
	<!--<script type="text/javascript">
    cwsEditor(750,200);
    </script>-->
<%
	End Function
End Class
%>