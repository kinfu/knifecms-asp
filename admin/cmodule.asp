<!--#include file="config.admin.asp"-->
<%
Dim CModule
Set CModule = New Class_KnifeCMS_Admin_CModule
Set CModule = Nothing
Class Class_KnifeCMS_Admin_CModule

	Private Pv_Result
	Private Pv_CModuleName,Pv_CModuleLabel,Pv_Content,Pv_Remarks,Pv_OrderNum,Pv_Recycle
	Private Pv_LabelPre,Pv_LabelSuf
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "cmodule.lang.asp"
		Header()
		Pv_Recycle  = KnifeCMS.Data.CLng(Pv_Recycle)
		Pv_LabelPre = "{label:"
		Pv_LabelSuf = "}"
		If Ctl="cmodule" Then
			Select Case Action
				Case "view"
					Call CModuleView()
				Case "add"
					If SubAction="save" Then
						Call CModuleDoSave()
					Else
						Pv_OrderNum = 9999
						Call CModuleDo()
					End If
				Case "edit"
					If SubAction="save" Then
						Call CModuleDoSave()
					Else
						Call GetCModuleDetail()
						If IsContentExist Then Call CModuleDo() : Else Admin.ShowMessage Lang_CModule(13),"goback",0
					End If
				Case "del"
					Call CModuleDel()
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"goback",0
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Sub GetCModuleDetail()
		Set Rs = KnifeCMS.DB.GetRecord(DBTable_CModule,Array("ID:"&ID),"")
		If Not(Rs.Eof) Then
			IsContentExist = True
			Pv_CModuleName    = Rs("CModuleName")
			Pv_CModuleLabel   = Rs("CModuleLabel")
			Pv_Content     = Rs("Content")
			Pv_Remarks     = Rs("Remarks")
			Pv_OrderNum    = Rs("OrderNum")
			Pv_Recycle     = Rs("Recycle")
		Else
			IsContentExist = False
		End IF
		KnifeCMS.DB.CloseRs Rs
	End Sub
	
	Private Function CModuleList()
		Dim Fn_TempSql,Fn_ColumnNum
		Fn_TempSql   = "SELECT a.ID,a.CModuleName,a.CModuleLabel,a.Remarks,a.Recycle,a.OrderNum FROM ["& DBTable_CModule &"] a"
		Fn_ColumnNum = 6
		Fn_TempSql   = Fn_TempSql & " ORDER BY a.OrderNum ASC"
		Operation    = "<span class=""sysBtn""><a href=""?ctl="& Ctl &"&act=edit&id={$ID}"" ><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		Call Admin.DataList(Fn_TempSql,50,Fn_ColumnNum,Operation,PageUrlPara)
	End Function
	
	Private Function CModuleDoSave()
		'获取数据并处理数据
		Pv_CModuleName  = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","CModuleName")),250)
		Pv_CModuleLabel = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","CModuleLabel")),250)
		Pv_Content      = KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","TextContent"))
		Pv_Remarks      = KnifeCMS.Data.Left(KnifeCMS.Data.TextareaEncode(KnifeCMS.GetForm("post","Remarks")),250)
		Pv_Recycle      = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","Recycle"))
		Pv_OrderNum     = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","OrderNum"))
		'数据合法性检测并处理数据
		If KnifeCMS.Data.IsNul(Pv_CModuleName)  Then ErrMsg = Lang_CModule(9) &"<br>"
		If KnifeCMS.Data.IsNul(Pv_CModuleLabel) Then ErrMsg = Lang_CModule(10) &"<br>"
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		IF Action="add" Then
			Result = KnifeCMS.DB.AddRecord(DBTable_CModule,Array("CModuleName:"& Pv_CModuleName,"CModuleLabel:"& Pv_CModuleLabel,"Content:"& Pv_Content,"Remarks:"& Pv_Remarks,"OrderNum:"& Pv_OrderNum,"Recycle:"& Pv_Recycle))
			ID  = KnifeCMS.DB.GetMaxID(DBTable_CModule)
			Msg = Lang_CModule(11) &"<br>[ID:"& ID &"]["& Lang_CModule(1) &":"& Pv_CModuleName &"]"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		ElseIf Action="edit" then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_CModule,Array("CModuleName:"& Pv_CModuleName,"CModuleLabel:"& Pv_CModuleLabel,"Content:"& Pv_Content,"Remarks:"& Pv_Remarks,"OrderNum:"& Pv_OrderNum,"Recycle:"& Pv_Recycle),Array("ID:"&ID))
			Msg = Lang_CModule(12) &"<br>[ID:"& ID &"]["& Lang_CModule(1) &":"& Pv_CModuleName &"]"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		End IF
	End Function
	
	Private Function CModuleDel()
		ID = KnifeCMS.GetForm("post","InputName")
		If KnifeCMS.Data.IsNul(Replace(ID,",","")) Then Exit Function
		Dim Fn_ArrID,Fn_ID,Fn_i
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			If ID > 0 Then
				Pv_CModuleName = KnifeCMS.DB.GetFieldByID(DBTable_CModule,"CModuleName",ID)
				'开始执行操作
				If Action = "del" Then
					Result = KnifeCMS.DB.DeleteRecord(DBTable_CModule,Array("ID:"&ID))
					If Result Then
						Msg = Msg & Lang_CModule(15) &"[ID:"& ID &"]["& Lang_CModule(1) &":"& Pv_CModuleName &"]<br>"
					Else
						Msg = Msg & Lang_CModule(16) &"[ID:"& ID &"]["& Lang_CModule(1) &":"& Pv_CModuleName &"]<br>"
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,1
	End Function
	
    Function CModuleView()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
          <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=add" ><b class="icon icon_add"></b><% echo Lang_DoAdd %></a></span>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
				  </td></tr>
			  </table>
		  </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%=KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></th>
			<th><div><% Echo Lang_ListTableColLine_ID %></div></th>
			<th><div><% Echo Lang_CModule(1) %></div></th>
			<th><div><% Echo Lang_CModule(2) %></div></th>
			<th><div><% Echo Lang_CModule(4) %></div></th>
            <th><div><% Echo Lang_CModule(6) %></div></th>
            <th><div><% Echo Lang_CModule(5) %></div></th>
			<th><div><% Echo Lang_ListTableColLine_Do %></div></th>
			</tr>
			<% Call CModuleList() %>
		   </tbody>
		  </table>
		  </form>
		</div>
      </DIV>
    </DIV>
    <script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var fn_temp;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td0"){
					fn_temp=tds[i].innerHTML;
					tds[i].innerHTML='<a href="?ctl=<%=Ctl%>&act=edit&id='+fn_temp+'">'+fn_temp+'</a>';
				}
				if(tds[i].id=="td1"){
					fn_temp=tds[i].innerHTML;
					tds[i].innerHTML='<a href="?ctl=<%=Ctl%>&act=edit&id='+tds[i-1].title+'">'+fn_temp+'</a>';
				}
				if(tds[i].id=="td2"){
					fn_temp=tds[i].innerHTML;
					tds[i].innerHTML='<%=Pv_LabelPre%>'+fn_temp+'<%=Pv_LabelSuf%>';
				}
				if(tds[i].id=="td4"){
					fn_label = "recycle";
					fn_id    = tds[i-5].title;
					img = document.createElement("img");
					img.setAttribute("class","pointer");
					if(parseInt(tds[i].innerHTML)==0){
						img.src="image/icon_yes.png";
						img.setAttribute("value",0);
					}else{
						img.src="image/icon_yes_gray.png";
						img.setAttribute("value",1);
					}
					img.setAttribute("id",fn_id);
					img.setAttribute("label",fn_label);
					img.onclick=function(){SetCModuleLabel(this)}
					tds[i].title="";
					tds[i].innerHTML="";
					tds[i].appendChild(img);
				}
			}
		}
	}
	ChangeTdText();
	var SetCModuleLabel = function(_this){
		var id,label,value,imgsrc,imgnewsrc,newvalue;
		id        = _this.getAttribute("id");
		label     = _this.getAttribute("label");
		imgsrc    = _this.src;
		value     = parseInt( _this.getAttribute("value"));
		_this.src = "image/loading.gif";
		label = _this.getAttribute("label");
		if(value>0){
			newvalue     = 0;
			imgnewsrc = "image/icon_yes.png";
		}else{
			newvalue     = 1;
			imgnewsrc = "image/icon_yes_gray.png";
		}
		var ajaxString = "label="+label+"&value="+newvalue;
		var ajaxUrl    = "ajax.get.asp?ctl=cmodule&act=edit&id="+id+"&subact=setlabel&r="+parseInt(Math.random()*1000);
		var ajaxSta    = posthttp(ajaxString,ajaxUrl);//window.open(ajaxUrl);
		if(ajaxSta=="ok"){
			_this.setAttribute("value",newvalue);
			_this.src = imgnewsrc;
		}else{
			if(ajaxSta.indexOf(Lang_NoPermissions)>0){alert(Lang_NoPermissions);}
			_this.src = imgsrc;
		}
	}
	</script>
<%
    End Function
	
	'添加界面
	Private Function CModuleDo()
	%>
    <script type="text/javascript">
	window.UEDITOR_HOME_URL = "<%=SystemPath%>plugins/ueditor/";
	</script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_config.asp"></script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_api.js"></script>
	<DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
          <form name="SaveForm" action="?ctl=<%=Ctl%>&act=<%=Action%>&subact=save" method="post" onsubmit="return SaveCModuleCheck()">
          <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
            <tbody>
               <tr><td class="titletd"><span class="required">*</span><%=Lang_CModule(1)%>:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass slong" name="CModuleName" id="CModuleName" maxLength="250" style="width:330px;" value="<% Echo Pv_CModuleName %>"/>
                   <label class="Normal" id="d_CModuleName"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><span class="required">*</span><%=Lang_CModule(2)%>:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass readonly" name="Label_Pre" id="Label_Pre" readonly="readonly" style="width:48px; text-align:right;" value="<%=Pv_LabelPre%>"><input type="text" class="TxtClass slong" name="CModuleLabel" id="CModuleLabel" maxLength="100" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9a-zA-Z_]*/g','maxLength:100'))" style="border-left:none; border-right:none;" value="<% Echo Pv_CModuleLabel %>"/><input type="text" class="TxtClass readonly" name="Label_Suf" id="Field_Suf" readonly="readonly" style="margin:0px 2px 0px 0px; width:10px; text-align:center;" value="<%=Pv_LabelSuf%>">
                   <label class="Normal" id="d_CModuleLabel"><%=Lang_CModule(8)%></label>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_CModule(3)%></td>
                  <td class="infotd">
                  <textarea class="TextContent" id="TextContent" name="TextContent"><%=KnifeCMS.Data.HTMLDecode(Pv_Content)%></textarea>
                  <label class="Normal" id="d_TextContent"></label>
                  <script type="text/javascript">var OSWebEditor=UE.getEditor('TextContent');</script>
                  </td>
              </tr>
            </tbody>
            </table>
          </div>
          <div class="inbox">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
            <tbody>
               <tr><td class="titletd"><%=Lang_CModule(4)%>:</td>
                   <td class="infotd">
                   <textarea class="mini" id="Remarks" name="Remarks"><%=Pv_Remarks%></textarea>
                   <label class="Normal" id="d_Remarks"></label>
                   </td></tr>
               <tr><td class="titletd"><%=Lang_CModule(6)%>:</td>
                   <td class="infotd">
                   <input type="radio" name="Recycle" value="0" <%=Admin.Input_Checked(Pv_Recycle,0)%> /><label><%=Lang_UnHide%></label>
                   <input type="radio" name="Recycle" value="1" <%=Admin.Input_Checked(Pv_Recycle,1)%>/><label><%=Lang_Hide%></label>
                   <label class="Normal" id="d_Recycle"></label>
                   </td>
               </tr>
               <tr><td class="titletd"><%=Lang_CModule(5)%>:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" name="OrderNum" id="OrderNum" maxlength="10" value="<%=Pv_OrderNum%>" style="width:60px;"/>
                   <label class="Normal" id="d_OrderNum"></label>
                   </td></tr>
             </tbody>
            </table>
          </div>
          <div class="bottomSaveline" id="SubmitButtoms" style="text-align:left; padding-left:165px;">
            <input type="hidden" name="id" value="<% Echo ID %>" />
            <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
            <button type="submit" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
            <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
          </div>
          </form>
        </div>
      </DIV>
	</DIV>
    <script type="text/javascript">
    var SaveCModuleCheck = function(){
		var Check=true;
		if(!Admin.CheckItem("CModuleName","d_CModuleName",Lang_Js_ThisCannotEmpty)){Check=false};
		if(!Admin.CheckItem("CModuleLabel","d_CModuleLabel",Lang_Js_ThisCannotEmpty)){Check=false};
		if(Check){
			Admin.DataSubmiting();
		}
		return Check;
	}
    </script>
	<%
	End Function
End Class
%>
