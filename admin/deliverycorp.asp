<!--#include file="config.admin.asp"-->
<%
Dim DeliveryCorp
Set DeliveryCorp = New Class_KnifeCMS_Admin_DeliveryCorp
Set DeliveryCorp = Nothing
Class Class_KnifeCMS_Admin_DeliveryCorp

	Private Pv_Rs,Pv_i,Pv_TempHTML
	Private Pv_CorpName,Pv_CorpLogo,Pv_CorpURL,Pv_OrderNum,Pv_Intro
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "deliverycorp.lang.asp"
		IsContentExist = False
		If KnifeCMS.Data.IsNul(Pv_CorpURL) Then Pv_CorpURL="http://"
		Header()
		If Ctl="deliverycorp" Then
			Select Case Action
				Case "view"
					DeliveryCorpView
				Case "add","edit"
					If SubAction="save" Then
						DeliveryCorpDoSave
					Else
						If Action = "add" Then
							DeliveryCorpDo
						ElseIf Action = "edit" Then
							If SubAction = "disabled" Or SubAction = "undisabled" Then
								DeliveryCorpOtherEdit
							Else
								Set Rs = KnifeCMS.DB.GetRecord(DBTable_DeliveryCorp,Array("ID:"&ID),"")
								If Not(Rs.Eof) Then
									IsContentExist = True
									Pv_CorpName = Rs("CorpName")
									Pv_CorpLogo = Rs("CorpLogo")
									Pv_CorpURL  = Rs("CorpURL")
									Pv_OrderNum = Rs("OrderNum")
									Pv_Intro    = KnifeCMS.Data.HTMLDecode(Rs("Intro"))
								End IF
								KnifeCMS.DB.CloseRs Rs
								If IsContentExist Then DeliveryCorpDo : Else Admin.ShowMessage Lang_Delivery_Cue(7),"goback",0
							End IF
						End IF
					End IF
				Case "del"  : DeliveryCorpDel
			End Select
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function DeliveryCorpList()
		Dim Fn_TempSql,Fn_ColumnNum
		Fn_ColumnNum = 6
		Fn_TempSql   = "SELECT a.ID,a.OrderNum,a.CorpName,a.CorpLogo,a.CorpURL,a.Disabled FROM ["& DBTable_DeliveryCorp &"] a "
		Fn_TempSql   = Fn_TempSql & "ORDER BY a.Disabled ASC, a.OrderNum ASC, a.ID ASC"
		Operation    = "<span class=""sysBtn""><a href=""?ctl="&Ctl&"&act=edit&id={$ID}""><b class=""icon icon_edit""></b>"& Lang_DoEdit &"</a></span>"
		Call Admin.DataList(Fn_TempSql,20,Fn_ColumnNum,Operation,PageUrlPara)
	End Function
	
	Private Function DeliveryCorpDoSave()
		Dim Fn_Error(2)
		    Fn_Error(0) = Lang_DeliveryCorp_Cue(8) '物流公司名称不能为空.
		'获取数据并处理数据
		Pv_CorpName    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","CorpName")),250)
		Pv_OrderNum    = KnifeCMS.Data.CLng(KnifeCMS.GetForm("post","OrderNum"))
		Pv_CorpLogo    = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","CorpLogo")),250)
		Pv_CorpURL     = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","CorpURL")),250)
		Pv_Intro       = KnifeCMS.Data.Left(KnifeCMS.Data.HTMLEncode(KnifeCMS.GetForm("post","TextContent")),100000)
		'数据合法性检测并处理数据
		If KnifeCMS.Data.IsNul(Pv_CorpName) Then ErrMsg = Fn_Error(0) &"<br>"
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		IF Action="add" Then
			Result = KnifeCMS.DB.AddRecord(DBTable_DeliveryCorp,Array("SysID:"& SysID,"CorpType:","CorpName:"& Pv_CorpName,"CorpLogo:"& Pv_CorpLogo,"CorpURL:"& Pv_CorpURL,"OrderNum:"& Pv_OrderNum,"Intro:"& Pv_Intro,"Disabled:0"))
			ID  = Admin.GetIDBySysID(DBTable_DeliveryCorp,SysID)
			Msg = Lang_DeliveryCorp_Cue(1) &"<br>["& Lang_DeliveryCorp(1) &":"& Pv_CorpName &"]<br>[ID:"& ID &"]"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		ElseIf Action="edit" then
			Result = KnifeCMS.DB.UpdateRecord(DBTable_DeliveryCorp,Array("CorpName:"& Pv_CorpName,"CorpLogo:"& Pv_CorpLogo,"CorpURL:"& Pv_CorpURL,"OrderNum:"& Pv_OrderNum,"Intro:"& Pv_Intro),Array("ID:"&ID))
			Msg = Lang_DeliveryCorp_Cue(2) &"<br>["& Lang_DeliveryCorp(1) &":"& Pv_CorpName &"]<br>[ID:"& ID &"]"
			If Result Then Admin.ShowMessage Msg,"?ctl="& Ctl &"&act=view",1
		End IF
	End Function
	
	Private Function DeliveryCorpDel()
		DeliveryCorpSubDo
	End Function
	Private  Function DeliveryCorpOtherEdit()
		DeliveryCorpSubDo
	End Function
	
	Private Function DeliveryCorpSubDo()
		ID = KnifeCMS.GetForm("post","InputName")
		If KnifeCMS.Data.IsNul(Replace(ID,",","")) Then Exit Function
		Dim Fn_ArrID,Fn_ID,Fn_i,Fn_Rs
		Fn_ArrID = Split(ID,",")
		For Fn_i=0 To Ubound(Fn_ArrID)
			ID = KnifeCMS.Data.CLng(Fn_ArrID(Fn_i))
			If ID > 0 Then
				Set Fn_Rs = KnifeCMS.DB.GetRecord(DBTable_DeliveryCorp &":CorpName",Array("ID:"&ID),"")
				If Not(Fn_Rs.Eof) Then Pv_CorpName=Fn_Rs(0)
				KnifeCMS.DB.CloseRs Fn_Rs
				'开始执行操作
				If Action = "del" Then
					Result = KnifeCMS.DB.DeleteRecord(DBTable_DeliveryCorp,Array("ID:"&ID))
					If Result Then Msg = Msg & Lang_DeliveryCorp_Cue(4) &"{"& Pv_CorpName &"[ID="& ID &"]}<br>"
				ElseIf Action = "edit" Then
					If SubAction = "undisabled" Then
						Result = KnifeCMS.DB.UpdateRecord(DBTable_DeliveryCorp,Array("Disabled:0"),Array("ID:"&ID))
						If Result Then Msg = Msg & Lang_DeliveryCorp_Cue(5) &"{"& Pv_CorpName &"[ID="& ID &"]}<br>"
					ElseIf SubAction = "disabled" Then
						Result = KnifeCMS.DB.UpdateRecord(DBTable_DeliveryCorp,Array("Disabled:1"),Array("ID:"&ID))
						If Result Then Msg = Msg & Lang_DeliveryCorp_Cue(6) &"{"& Pv_CorpName &"[ID="& ID &"]}<br>"
					End If
				End If
			End If
		Next
		If Not(KnifeCMS.Data.IsNul(Msg)) Then Admin.ShowMessage Msg,Url,1
	End Function
	
	Private Function DeliveryCorpView()
%>
	<DIV id="MainFrame">
	  <div class="tagLine"><% Echo NavigationLine %></div>
	  <DIV class="cbody">
		<div class="toparea">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoAdd %><!--添加操作区--></div></td></tr>
			  <tr><td class="downcell">
                  <span class="sysBtn"><a href="?ctl=<% Echo Ctl %>&act=add"><b class="icon icon_add"></b><% Echo Lang_DoAdd & Lang_DeliveryCorp(0) %></a></span>
				  </td></tr>
			  </table>
		  </td>
          <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onclick="Operate('listForm','InputName','edit:undisabled','')"><b class="icon icon_start"></b><% Echo Lang_DoOnUse %></button>
				  <button type="button" class="sysBtn" onclick="Operate('listForm','InputName','edit:disabled','')"><b class="icon icon_stop"></b><% Echo Lang_DoStopUse %></button>
				  </td></tr>
			  </table>
		  </td>
		  <td class="tdcell">
			  <table border="0" cellpadding="2" cellspacing="0">
			  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
			  <tr><td class="downcell">
				  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely %></button>
				  </td></tr>
			  </table>
		  </td>
		  </tr>
		  </table>
		</div>
		<div class="tabledata">
		  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
		   <tbody>
			<tr class="top">
			<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onclick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
			<th width="1%" ><div style="width:50px; "><% Echo Lang_ListTableColLine_ID %><!--系统ID--></div></th>
            <th width="5%" ><div style="width:32px; "><% Echo Lang_DeliveryCorp_ListTableColLine(4) %><!--排序--></div></th>
			<th width="20%"><div style="width:150px;"><% Echo Lang_DeliveryCorp_ListTableColLine(0) %><!--物流公司--></div></th>
            <th width="15%" ><div style="width:150px; "><% Echo Lang_DeliveryCorp_ListTableColLine(1) %><!--Logo--></div></th>
            <th width="20%" ><div style="width:150px; "><% Echo Lang_DeliveryCorp_ListTableColLine(2) %><!--网址--></div></th>
            <th width="6%"><div style="width:80px;"><% Echo Lang_DeliveryCorp_ListTableColLine(3) %><!--状态--></div></th>
			<th><div style="width:60px;"><% Echo Lang_ListTableColLine_Do %><!--操作--></div></th>
			</tr>
			<% DeliveryCorpList %>
		   </tbody>
		  </table>
		  </form>
		</div>
      </DIV>
	</DIV>
    <script type="text/javascript">
	TrBgChange("listForm","InputName","oliver","data","select");
	function ChangeTdText(){
		var tds=KnifeCMS.$id("listForm").getElementsByTagName('td');
		var TempHTML;
		if(tds!=null){
			for(var i=1;i<tds.length;i++){
				if(tds[i].id=="td3" && tds[i].innerHTML!=""){
					TempHTML = "<img class=\"CorpLogo\" src='"+ unescape(tds[i].innerHTML) +"' />";
					tds[i].innerHTML=TempHTML;
				}else if(tds[i].id=="td5"){
					TempHTML = tds[i].innerHTML;
					if(TempHTML=="0"){tds[i].innerHTML="<span style=\"color:#048E08;\">"+Lang_Js_OnUse+"</span>";}else if(TempHTML=="1"){ tds[i].innerHTML="<span style=\"color:#CD2E03;\">"+Lang_Js_HaveStopUse+"</span>"}
				}
			}
		}
	}
	ChangeTdText();
	</script>
<%
	End Function
	Private Function DeliveryCorpDo()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <div class="wbox">
          <div class="inbox">
            <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save" method="post">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable mb5">
              <tbody>
               <tr><td class="titletd"><% Echo Lang_DeliveryCorp(1) %>:</td>
                   <td class="infotd">
                   <input type="text" class="TxtClass" id="CorpName" name="CorpName" style="width:200px;" value="<% Echo Pv_CorpName %>" />
                   <label id="d_CorpName"></label>
                   </td>
               </tr>
               </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
                <tr><td class="titletd"><% Echo Lang_DeliveryCorp(2) %><!--排序-->:</td>
                     <td class="infotd">
                     <input type="text" class="TxtClass" id="OrderNum" name="OrderNum" onblur="KnifeCMS.onblur(this,Array('replace','/[^0-9]*/g','maxLength:5'))" style="width:50px;" value="<% Echo Pv_OrderNum %>" />
                     <label class="Normal"><% Echo Lang_DeliveryCorp_Cue(9) %><!--数字越小越靠前--></label>
                     </td>
                 </tr>
                 <tr><td class="titletd"><% Echo Lang_DeliveryCorp(3) %><!--网址-->:</td>
                     <td class="infotd"><input type="text" class="TxtClass long" id="CorpURL" name="CorpURL" value="<% Echo Pv_CorpURL %>" /><label id="d_CorpURL"></label></td>
                 </tr>
                 <tr><td class="titletd"><% Echo Lang_DeliveryCorp(4) %><!--Logo-->:</td>
                     <td class="infotd">
                     <span id="CorpLogoBox" style="margin-right:5px;"><% if not(KnifeCMS.Data.IsNul(Pv_CorpLogo)) Then Response.Write("<img src="""& Pv_CorpLogo &""" class=""CorpLogo"" />") %></span>
                     <span class="sysBtn"><a href="javascript:void(0)" onclick="UploadLogo()"><b class="icon icon_add"></b>上传LOGO...</a></span>
                     <input type="hidden" class="TxtClass long" id="CorpLogo" name="CorpLogo" value="<% Echo Pv_CorpLogo %>" />
                     <label id="d_CorpLogo"></label></td>
                 </tr>
                 <tr><td class="titletd"><% Echo Lang_DeliveryCorp(5) %><!--详细介绍-->:</td>
                     <td class="infotd">
                     <textarea class="TextContent" id="TextContent" name="TextContent"><%=KnifeCMS.Data.HTMLDecode(Pv_Intro)%></textarea>
                     <label class="Normal"></label>
                     </td>
                 </tr>
            </table>
            <input type="hidden" name="id" value="<%=ID%>" />
            <input type="hidden" name="sysid" id="sysid" value="<% Echo SysID %>" />
            <div class="bottomSaveline" id="SubmitButtoms">
              <button type="button" class="sysSaveBtn" name="SubmitBtn" id="SubmitBtn" onclick="Admin.Delivery.SaveDeliveryCorpCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
              <button type="reset" class="sysSaveBtn" id="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
            </div>
            </form>
          </div>
        </div>
      </DIV>
    </DIV>
    <script type="text/javascript" charset="utf-8">
	window.UEDITOR_HOME_URL = "<%=SystemPath%>plugins/ueditor/";
	</script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_config.asp"></script>
    <script type="text/javascript" charset="UTF-8" src="../plugins/ueditor/editor_api.js"></script>
    <script type="text/javascript">
	var OSWebEditor=UE.getEditor('TextContent');
	</script>
	<script type="text/javascript">
    //cwsEditor(750,200);
	//$:上传品牌Logo
	function UploadLogo(){
		var NewWindow;
		var TargetArea=document.getElementById("CorpLogoBox");
		var Input=document.getElementById("CorpLogo");
		if(TargetArea!=null && Input!=null){
			NewWindow=window.showModalDialog('fileupload.asp?ctl=file_image&act=view&insertnum=1&inserttype=url',window,'dialogWidth:740px;dialogHeight:550px;center:Yes;help:no;resizable:yes;scroll:yes;');
			if(NewWindow!=null){
				TargetArea.innerHTML="";
				var img=document.createElement("img");
				img.src=unescape(NewWindow);
				img.className="CorpLogo";
				TargetArea.appendChild(img);
				Input.value=unescape(NewWindow);
				}
			}else{
				alert("Alert: UploadLogo(){ Can't Found Tag Width id=CorpLogoBox Or id=CorpLogo! }");
		}
	}
    </script>
<%
	End Function
End Class
%>