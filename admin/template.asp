<!--#include file="config.admin.asp"-->
<%
Dim Template
Set Template = New Class_KnifeCMS_Admin_Template
Set Template = Nothing
Class Class_KnifeCMS_Admin_Template

	Private Pv_RunFooter
	Private Pv_Result
	Private Pv_FilePath,Pv_Code,Pv_FileSuffix,Fn_TemplateName
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "template.lang.asp"
		Header()
		Pv_RunFooter = True
		If Ctl="template" Then
			Select Case Action
				Case "view"
					Call TemplateView()
				Case "add"
				Case "activate"
					Call TemplateActivate()
				Case "edit"
					If ActionType="templatefile" Then
						If SubAction="save" Then
							Call TemplateFileDoSave()
						ElseIf SubAction="list" Then
							Call TemplateFileListView()
						Else
							Pv_RunFooter = False
							Call TemplateFileDo()
							Echo "</body></html>"
						End If
					End If
				Case "del"
					Call TemplateDel()
			End Select
		Else
			Admin.ShowMessage Lang_PraError,"goback",0
		End If
		If Pv_RunFooter Then Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Private Function GetCurrentTemplate()
		On Error Resume Next
		Dim Fn_FSO,Fn_Folder,Fn_Item,Fn_String
		Dim Fn_FolderName,Fn_XmlFile
		Dim Fn_Tpl_Preview,Fn_Tpl_Name,Fn_Tpl_Author,Fn_Tpl_Demo,Fn_Tpl_Website,Fn_Tpl_Description
		Fn_FolderName = TemplateName
		
		Fn_String = Fn_String &"<table border=""0"" cellpadding=""0"" cellspacing=""0""><tr>"
        Fn_String = Fn_String &"<td valign=""top"">"
        Fn_String = Fn_String &"<div class=""template_pic"" id=""template_pic""><img src="""& SystemPath & TemplateRoot & Fn_FolderName &"/preview.png"" /></div>"
        Fn_String = Fn_String &"</td>"
        Fn_String = Fn_String &"<td valign=""top"">"
        Fn_String = Fn_String &"<div class=""template_info"">"
        Fn_String = Fn_String &"<div class=""template_foldername"">"& Fn_FolderName &"<a href=""../"" class=""ml10"" target=_blank>"& Lang_Template(19) &"</a></div>"
        Dim HasTemplate:HasTemplate=false
		If KnifeCMS.FSO.FileExists(Server.MapPath(SystemPath & TemplateRoot & Fn_FolderName &"/template.xml")) Then
			Set Fn_XmlFile = KnifeCMS.CreateXmlDoc("msxml2.FreeThreadedDOMDocument"& MsxmlVersion)
			Fn_XmlFile.Loadxml KnifeCMS.Read(SystemPath & TemplateRoot & Fn_FolderName &"/template.xml")
			Fn_Tpl_Name        = Fn_XmlFile.selectSingleNode("template").selectSingleNode("name").text
			Fn_Tpl_Author      = Fn_XmlFile.selectSingleNode("template").selectSingleNode("author").text
			Fn_Tpl_Demo        = Fn_XmlFile.selectSingleNode("template").selectSingleNode("demo").text
			Fn_Tpl_Website     = Fn_XmlFile.selectSingleNode("template").selectSingleNode("website").text
			Fn_Tpl_Description = Fn_XmlFile.selectSingleNode("template").selectSingleNode("description").text
			Set Fn_XmlFile = Nothing
			HasTemplate=true
		'判断是否dedecms模板
		ElseIf KnifeCMS.FSO.FileExists(Server.MapPath(SystemPath & TemplateRoot & Fn_FolderName &"/index.htm")) Then
			Dim IndexStr
			IndexStr = KnifeCMS.Read(SystemPath & TemplateRoot & Fn_FolderName &"/index.htm")
			If InStr(IndexStr,"{dede:")>0 Then
				Fn_Tpl_Name = "DeDeCMS模板"
				HasTemplate=true
			End If
		Else
			HasTemplate=false
		End If
		If HasTemplate=true Then
			Fn_String = Fn_String &"<div class=""template_name pt10"">"& Fn_Tpl_Name
			If Fn_Tpl_Demo<>"" Then
				Fn_String = Fn_String &"<span class=""demo""><a href="""& Fn_Tpl_Demo &""" title="""& Lang_Template(10) &""">"& Lang_Template(10) &"</a></span>"
			End If
			Fn_String = Fn_String &"</div>"
			If Fn_Tpl_Author<>"" Then
				Fn_String = Fn_String &"<div class=""template_author"">"& Lang_Template(8)
				If Fn_Tpl_Website<>"" Then
				Fn_String = Fn_String &"<span class=""author""><a href="""& Fn_Tpl_Website &""" title="""& Lang_Template(9) &""">"& Fn_Tpl_Author &"</a></span>"
				Else
				Fn_String = Fn_String &"<span class=""author"">"& Fn_Tpl_Author &"</span>"
				End If
				Fn_String = Fn_String &"</div>"
			End If
			If Fn_Tpl_Description<>"" Then
				Fn_String = Fn_String &"<div class=""description""><div class=""description_heading"">DESCRIPTION</div>"& Fn_Tpl_Description &"</div>"
			End If
		Else
			Fn_String = Fn_String &"<div class=""template_error_cue"">"& Lang_Template(11) &"</div>"
		End If
		Fn_String = Fn_String &"<div class=""action_line""><a href=""template.asp?ctl=template&act=edit&subact=list&acttype=templatefile&filepath="& TemplateRoot & Fn_FolderName &"/"">"& Lang_Template(4) &"</a></div>"
		Fn_String = Fn_String &"</div>"
		Fn_String = Fn_String &"</td>"
		Fn_String = Fn_String &"</tr></table>"
		GetCurrentTemplate = Fn_String
		If Err.Number <> 0 Then Err.Clear
	End Function
	
	Private Function GetTemplateList()
		On Error Resume Next
		Dim Fn_FSO,Fn_Folder,Fn_Item,Fn_String
		Dim Fn_i,Fn_FolderName,Fn_XmlFile
		Dim Fn_Tpl_Preview,Fn_Tpl_Name,Fn_Tpl_Author,Fn_Tpl_Demo,Fn_Tpl_Website,Fn_Tpl_Description
		Set Fn_FSO = KnifeCMS.FSO.ObjectFSO
		If Fn_FSO.FolderExists(Server.MapPath(SystemPath & TemplateRoot))=False Then
			Echo "ERROR_FILEPATH："& SystemPath & TemplateRoot
			Exit function
		End If
		Set Fn_Folder=Fn_FSO.getfolder(Server.MapPath(SystemPath & TemplateRoot))
		For Each Fn_Item in Fn_Folder.subfolders
			Fn_FolderName = Fn_Item.name
			If LCase(Fn_FolderName)<>LCase(TemplateName) Then
				Fn_i = Fn_i+1
				Fn_Tpl_Name        = ""
				Fn_Tpl_Author      = ""
				Fn_Tpl_Demo        = ""
				Fn_Tpl_Website     = ""
				Fn_Tpl_Description = ""
				Fn_String = Fn_String &"<div class=""template"">"
				Fn_String = Fn_String &"<div class=""template_pic""><img src="""& SystemPath & TemplateRoot & Fn_FolderName &"/preview.png"" /></div>"
				Fn_String = Fn_String &"<div class=""template_foldername""><a href=""template.asp?ctl=template&act=edit&subact=list&acttype=templatefile&filepath="& TemplateRoot & Fn_FolderName &"/"" title="""& Lang_Template(7) &""">"& Fn_FolderName &"</a><span class=""time"">DATE:"& Fn_Item.datelastmodified &"</span></div>"
				Dim HasTemplate:HasTemplate=false
				If KnifeCMS.FSO.FileExists(Server.MapPath(SystemPath & TemplateRoot & Fn_FolderName &"/template.xml")) Then
					
					Set Fn_XmlFile = KnifeCMS.CreateXmlDoc("msxml2.FreeThreadedDOMDocument"& MsxmlVersion)
					Fn_XmlFile.Loadxml KnifeCMS.Read(SystemPath & TemplateRoot & Fn_FolderName &"/template.xml")
					Fn_Tpl_Name        = Fn_XmlFile.selectSingleNode("template").selectSingleNode("name").text
					Fn_Tpl_Author      = Fn_XmlFile.selectSingleNode("template").selectSingleNode("author").text
					Fn_Tpl_Demo        = Fn_XmlFile.selectSingleNode("template").selectSingleNode("demo").text
					Fn_Tpl_Website     = Fn_XmlFile.selectSingleNode("template").selectSingleNode("website").text
					Fn_Tpl_Description = Fn_XmlFile.selectSingleNode("template").selectSingleNode("description").text
					Set Fn_XmlFile = Nothing
					HasTemplate=true
				'判断是否dedecms模板
				ElseIf KnifeCMS.FSO.FileExists(Server.MapPath(SystemPath & TemplateRoot & Fn_FolderName &"/index.htm")) Then
					Dim IndexStr
					IndexStr = KnifeCMS.Read(SystemPath & TemplateRoot & Fn_FolderName &"/index.htm")
					If InStr(IndexStr,"{dede:")>0 Then
						Fn_Tpl_Name = "DeDeCMS模板"
						HasTemplate=true
					End If
				Else
					HasTemplate=false
				End If
				If HasTemplate=true Then
					Fn_String = Fn_String &"<div class=""template_name"">"& Fn_Tpl_Name
					If Fn_Tpl_Demo<>"" Then
						Fn_String = Fn_String &"<span class=""demo""><a href="""& Fn_Tpl_Demo &""" title="""& Lang_Template(10) &""">"& Lang_Template(10) &"</a></span>"
					End If
					Fn_String = Fn_String &"</div>"
					If Fn_Tpl_Author<>"" Then
						Fn_String = Fn_String &"<div class=""template_author"">"& Lang_Template(8)
						If Fn_Tpl_Website<>"" Then
						Fn_String = Fn_String &"<span class=""author""><a href="""& Fn_Tpl_Website &""" title="""& Lang_Template(9) &""">"& Fn_Tpl_Author &"</a></span>"
						Else
						Fn_String = Fn_String &"<span class=""author"">"& Fn_Tpl_Author &"</span>"
						End If
						Fn_String = Fn_String &"</div>"
					End If
					Fn_String = Fn_String &"<div class=""description"" id=""template_"& Fn_i &"""><div class=""description_heading"">DESCRIPTION</div>"
					If Fn_Tpl_Description<>"" Then
						Fn_String = Fn_String & Fn_Tpl_Description
					Else
						Fn_String = Fn_String & Lang_Template(12)
					End If
					Fn_String = Fn_String &"</div>"
				Else
					Fn_String = Fn_String &"<div class=""template_error_cue"">"& Lang_Template(11) &"</div>"
				End If
				Fn_String = Fn_String &"<div class=""action_line""><a href=""template.asp?ctl=template&act=activate&templatename="& Fn_FolderName &""" class=""activate"">"& Lang_Template(3) &"</a>|<a href=""template.asp?ctl=template&act=edit&subact=list&acttype=templatefile&filepath="& TemplateRoot & Fn_FolderName &"/"">"& Lang_Template(4) &"</a>|<a onclick=""return confirm('"& Replace(Lang_Template(18),"{tpl:templatename}",Fn_FolderName) &"');"" href=""template.asp?ctl=template&act=del&templatename="& Fn_FolderName &""" class=""del"">"& Lang_Template(6) &"</a></div>"
				Fn_String = Fn_String &"</div>"
			End If
		Next
		Set Fn_Folder=Nothing
		Set Fn_FSO =Nothing
		GetTemplateList = Fn_String
		If Err.Number <> 0 Then Err.Clear
	End Function
	
	Private Function TemplateActivate()
		Fn_TemplateName = KnifeCMS.GetForm("get","templatename")
		If Fn_TemplateName="" Then ErrMsg=Lang_Template(13)
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		'保存配置到config.main.asp
		Pv_Code = KnifeCMS.Read(SystemPath & "config/config.main.asp")
		Pv_Code = KnifeCMS.Data.RegReplace(Pv_Code,"TemplateName([\s]*?)=([\s]*?)(""\S*"")","TemplateName = """& Fn_TemplateName &"""")
		If KnifeCMS.FSO.SaveTextFile("config/config.main.asp",Pv_Code)=True Then
			Admin.ShowMessage Replace(Lang_Template(14),"{ptl:templatename}",Fn_TemplateName),"?ctl="& Ctl &"&act=view",1
		Else
			Admin.ShowMessage Replace(Lang_Template(15),"{ptl:templatename}",Fn_TemplateName),"?ctl="& Ctl &"&act=view",1
		End If
	End Function
	
	Private Function CreateFolderTagLine(ByVal BV_Path)
		Dim Fn_Array,Fn_i,Fn_String,Fn_Folder,Fn_FolderPath
		Fn_Array = Split(BV_Path,"/")
		For Fn_i=0 To Ubound(Fn_Array)-1
			Fn_Folder     = Fn_Array(Fn_i)
			Fn_FolderPath = Fn_FolderPath & Fn_Folder &"/"
			If KnifeCMS.Data.IsNul(Fn_String) Then
				Fn_String = "<span class=""folderroot"">"& Lang_FilePath &"</span>"
			Else
				Fn_String = Fn_String &"<span class=""folder""><a href=""template.asp?ctl=template&act=edit&subact=list&acttype=templatefile&filepath="& Fn_FolderPath &""" target=""_self"">"& Fn_Folder &"</a></span><span class=""raquo"">/</span>"
			End If
		Next
		Fn_String = "<div class=""folderTagline"" id=""folderTagline"">"& Fn_String
		Fn_String = Fn_String &"</div>"
		CreateFolderTagLine = Fn_String
	End Function
	
	Private Function TemplateFileList()
		Dim Fn_Path : Fn_Path = KnifeCMS.GetForm("get","filepath")
		Call GetFiles(Fn_Path)
	End Function
	
	Private Function GetFiles(ByVal BV_FilePath)
		If Trim(BV_FilePath) = "" Or KnifeCMS.Data.IsNul(KnifeCMS.Data.ReplaceString(BV_FilePath,"/","")) Then Exit Function
		If TemplateRoot <> Left(BV_FilePath,Len(TemplateRoot)) Then Echo "error" : Exit Function
		If KnifeCMS.IsFSOInstalled=False then ErrMsg=Lang_ObjFSONotInstalled : Admin.ShowError(0)

		Dim Fn_FilePath,Fn_FSO,Fn_Folder,FileItem,Fn_FileNum,Fn_i,Fn_HTML
		Dim Fn_Time_Day,Fn_Time_Hour,Fn_Time_Second,Fn_DateDiff
		Fn_FileNum=0
		Fn_i=0
		Fn_FilePath = KnifeCMS.Data.FolderPath(BV_FilePath)
		Echo CreateFolderTagLine(Fn_FilePath)
		
		Set Fn_FSO = KnifeCMS.FSO.ObjectFSO
		If Fn_FSO.FolderExists(Server.MapPath(SystemPath & Fn_FilePath))=False Then
			Echo "ERROR_FILEPATH："& Fn_FilePath 
			Exit function
		End If
		Set Fn_Folder=Fn_FSO.getfolder(Server.MapPath(SystemPath&Fn_FilePath))
		Fn_FileNum=Fn_Folder.subfolders.Count + Fn_Folder.files.Count
		If int(Fn_FileNum) > 0 Then
			Fn_HTML = "<ul class=""templatefilelist"" id=""templatefilelist"">"
			for each FileItem in Fn_Folder.subfolders
				Fn_i=Fn_i+1
				Fn_HTML = Fn_HTML &"<li class=""folder"" id=""folder_"& Fn_i &"""><span class=""icon""></span><div class=""info""><div class=""tline""><span class=""name""><a href=""template.asp?ctl=template&act=edit&subact=list&acttype=templatefile&filepath="& BV_FilePath & FileItem.name &"/"" target=""_self"">"& FileItem.name &"</a></span></div><div class=""bline""><span class=""size"">" &KnifeCMS.Data.FormatCurrency(FileItem.size/1024) &"k</span><span class=""time"">"& FileItem.datelastmodified &"</span></div></div></li>"
			next
			for each FileItem in Fn_Folder.files
				Fn_i=Fn_i+1
				Pv_FileSuffix = KnifeCMS.Data.FileExName(FileItem.name)
				Fn_HTML = Fn_HTML &"<li class=""file"" id=""file_"& Fn_i &""">"
				
				If Pv_FileSuffix="jpg" Or Pv_FileSuffix="jpeg" Or Pv_FileSuffix="bmp" Or Pv_FileSuffix="gif" Or Pv_FileSuffix="png" Then
					Fn_HTML = Fn_HTML &"<span class=""img""><a href=""template.asp?ctl=template&act=edit&acttype=templatefile&filepath="& BV_FilePath & FileItem.name &""" target=""templateFrame""><img src="""& SystemPath & BV_FilePath & FileItem.name &"""/></a></span>"
				Else
					Fn_HTML = Fn_HTML &"<span class=""icon""></span>"
				End If
				
				Fn_HTML = Fn_HTML &"<div class=""info""><div class=""tline"">"
				If Pv_FileSuffix="htm" Or Pv_FileSuffix="js" Or Pv_FileSuffix="css" Or Pv_FileSuffix="jpg" Or Pv_FileSuffix="gif" Or Pv_FileSuffix="png" Then
					Fn_HTML = Fn_HTML &"<span class=""name""><a href=""template.asp?ctl=template&act=edit&acttype=templatefile&filepath="& BV_FilePath & FileItem.name &""" target=""templateFrame"">"& FileItem.name &"</a></span>"
				Else
					Fn_HTML = Fn_HTML &"<span class=""name"">"& FileItem.name &"</span>"
				End If
				Fn_HTML = Fn_HTML &"</div><div class=""bline""><span class=""size"">"& KnifeCMS.Data.FormatCurrency(FileItem.size/1024) &"k</span><span class=""time"">"& FileItem.datelastmodified &"</span></div></div></li>"
			next
			Fn_HTML = Fn_HTML &"</ul>"
		Else
			Fn_HTML = Fn_HTML &("<div class=""foldernone"">"& Lang_NoFileAndFolderExistInThisFolder &"</div>")
		End If
		Set Fn_Folder=Nothing
		Set Fn_FSO=Nothing
		Echo Fn_HTML
	End Function
	
	Private Function TemplateFileDoSave()
		Pv_FilePath   = KnifeCMS.GetForm("post","filepath")
		Pv_FileSuffix = KnifeCMS.Data.FileExName(Pv_FilePath)
		'判断是否存在文件
		If KnifeCMS.FSO.FileExists(Server.MapPath(SystemPath & Pv_FilePath))=False Then
			Echo "<DIV class=""cbody"">FILE_NOT_EXIST："& SystemPath & Pv_FilePath &"</DIV>"
			Exit Function
		End If
		'文件合法性判断
		If Pv_FileSuffix<>"html" And Pv_FileSuffix<>"js" And Pv_FileSuffix<>"css" Then
			Echo "<DIV class=""cbody"">ILLEGAL_FILE："& SystemPath & Pv_FilePath &"</DIV>"
			Exit Function
		End If
		Pv_Code = KnifeCMS.Data.TemplateCodeDecode(KnifeCMS.GetForm("post","code"))
		If KnifeCMS.FSO.SaveTextFile(Pv_FilePath,Pv_Code)=True Then
			Admin.ShowMessage Lang_SaveSuccess,Array(Url,Lang_GoBack),1
		Else
			Admin.ShowMessage Lang_SaveFail,Array(Url,Lang_GoBack),1
		End If
	End Function
	
	Private Function TemplateDel()
		Fn_TemplateName = KnifeCMS.GetForm("get","templatename")
		If Fn_TemplateName="" Then ErrMsg=Lang_Template(13)
		If ErrMsg<>"" Then Admin.ShowMessage ErrMsg,"goback",0 : Exit Function
		'删除整个模板文件夹
		If KnifeCMS.FSO.DeleteFolder(Server.MapPath(SystemPath & TemplateRoot & Fn_TemplateName))=True Then
			Admin.ShowMessage Replace(Lang_Template(16),"{ptl:templatename}",Fn_TemplateName),"?ctl="& Ctl &"&act=view",1
		Else
			Admin.ShowMessage Replace(Lang_Template(17),"{ptl:templatename}",Fn_TemplateName),"?ctl="& Ctl &"&act=view",1
		End If
	End Function
	
    Function TemplateView()
%>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
      <div class="template_gird">
          <div class="heading"><%=Lang_Template(1)%></div>
          <div class="current_template" id="current_template">
              <% Echo GetCurrentTemplate() %>
          </div>
          <div class="heading"><%=Lang_Template(2)%></div>
          <div class="template_list">
              <% Echo GetTemplateList() %>
          </div>
      </div>
      
      </DIV>
    </DIV>
    <script type="text/javascript">
	template_init();
    function template_init(){
		var template=document.getElementsByClassName("div","template");
		var current_template=document.getElementById("template_pic");
		current_template.onmouseover = function(){
			this.className = "template_pic template_hover";
		}
		current_template.onmouseout = function(){
			this.className = "template_pic";
		}
		for(var i=0;i<template.length;i++){
			template[i].onmouseover = function(){
				this.className = "template template_hover";
			}
			template[i].onmouseout = function(){
				this.className = "template";
			}
		}
	}
    </script>
<%
    End Function
	Function TemplateFileListView()
	%>
    <style type="text/css">
    body{ padding:20px 0px 0px 0px;}
    </style>
    <DIV id="MainFrame">
      <div class="tagLine"><% Echo NavigationLine %></div>
      <DIV class="cbody">
        <table border="0" cellpadding="0" cellspacing="0" style="width:100%"><tr>
        <td valign="top" style="width:10%">
          <input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" />
          <% Call TemplateFileList() %>
        </td>
        <td valign="top" width="80%">
        <div class="template_code_editor">
        <iframe src="template.asp?ctl=template&act=edit&acttype=templatefile&filepath=" name="templateFrame" id="templateFrame" class="templateFrame" frameborder="no" scrolling="auto" style="width:100%;height:100%;" hidefocus/></iframe>
        </div>
        </td>
        </tr></table>
      </DIV>
    </DIV>
    <script type="text/javascript">
	if(objExist(KnifeCMS.$id("templatefilelist"))){
		KnifeCMS.$id("templatefilelist").style.height = parseInt(webScreenHeight()-102) + "px";
	}
	document.getElementById("templateFrame").style.height = parseInt(webScreenHeight()-69) + "px";
    </script>
    <%
	End Function
	Function TemplateFileDo()
		Pv_FilePath   = KnifeCMS.GetForm("get","filepath")
		Pv_FileSuffix = KnifeCMS.Data.FileExName(Pv_FilePath)
		If Pv_FileSuffix="" Then Exit Function
		If KnifeCMS.FSO.FileExists(Server.MapPath(SystemPath & Pv_FilePath))=False Then
			Echo "<DIV class=""cbody"">FILE_NOT_EXIST："& SystemPath & Pv_FilePath &"</DIV>"
			Exit Function
		End If
	%>
    <style type="text/css">
    body{ padding:0px 0px 0px 0px;}
    </style>
    <DIV id="MainFrame">
      <DIV class="cbody">
        <div class="templatefilepath"><%=Pv_FilePath%></div>
        <% If Pv_FileSuffix="htm" Or Pv_FileSuffix="js" Or Pv_FileSuffix="css" Then %>
        <form name="SaveForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=save&acttype=<%=ActionType%>" method="post">
        <div class="template_code_editor">
            <textarea class="code" name="code" id="template_code_editor" style="width:99%"><%=KnifeCMS.Data.TemplateCodeEncode(KnifeCMS.Read(SystemPath & Pv_FilePath))%></textarea>
            <div class="bottomSaveline" id="SubmitButtoms">
            <input type="hidden" name="filepath" value="<%=Pv_FilePath%>" />
            <input type="hidden" name="url" value="<%=KnifeCMS.CurrentURL%>" />
            <button type="button" class="sysSaveBtn" id="SaveBtn" name="SaveBtn" onclick="SaveTemplateCodeCheck()"><b class="icon icon_save"></b><% Echo Lang_Btn_Submit %></button>
            <button type="reset" class="sysSaveBtn" id="ReSetBtn" name="ReSetBtn"><b class="icon icon_reset"></b><% Echo Lang_Btn_Reset %></button>
            </div>
        </div>
        </form>
        <% ElseIf Pv_FileSuffix="jpg" Or Pv_FileSuffix="jpeg" Or Pv_FileSuffix="bmp" Or Pv_FileSuffix="gif" Or Pv_FileSuffix="png" Then %>
        <img src="<%=SystemPath & Pv_FilePath%>" />
        <% End If %>
      </DIV>
    </DIV>
    <script type="text/javascript">
	if(objExist(KnifeCMS.$id("template_code_editor"))){
		KnifeCMS.$id("template_code_editor").style.height = parseInt(webScreenHeight()-83) + "px";
	}
	function SaveTemplateCodeCheck(){
		var Check=true;
		if(Check){
			Admin.DataSubmiting();
			document.forms["SaveForm"].submit();
		}
	}
	function TemplateDel(){
		
	}
    </script>
    <%
	End Function
End Class
%>
