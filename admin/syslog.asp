<!--#include file="config.admin.asp"-->
<%
Dim SysLog
Set SysLog = New Class_KnifeCMS_Admin_SysLog
Set SysLog = Nothing
Class Class_KnifeCMS_Admin_SysLog
	
	Private Pv_Username
	
	Private Sub Class_Initialize()
		Admin.IncludeLanguageFile "syslog.lang.asp"
		Header()
		If Ctl="syslog" Or Ctl="syslog_recycle" Then
			Select Case Action
				Case "view" : SysLogView
				Case "edit"
					If SubAction = "revert" Then SysLogOtherEdit
				Case "del"  : SysLogDel
			End Select
		End If
		Footer()
	End Sub
	Private Sub Class_Terminate()
		ClassObjectClose
	End Sub
	
	Function SysLogList()
		Dim Fn_TempSql,Fn_ColumnNum
		Fn_TempSql   = "SELECT ID,LogTime,Username,UserID,UserIP,SystemCtl,SystemAction,LogContent FROM ["& DBTable_SystemLog &"] "
		Fn_ColumnNum = 8
		If SubCtl = "recycle" Then
			Fn_TempSql = Fn_TempSql & "WHERE Recycle=1 ORDER BY ID DESC"
			Call Admin.DataList(Fn_TempSql,50,Fn_ColumnNum,Operation,PageUrlPara)
		Else
			Fn_TempSql = Fn_TempSql & "WHERE Recycle=0 "
			If SubAction = "search" Then
				tempCondition = KnifeCMS.IIF(Admin.Search.KeyWords<>"","AND LogContent like '%"& Admin.Search.KeyWords &"%' "," ")
				Select Case DB_Type
				Case 0
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>"","AND LogTime>=#"& Admin.Search.StartDate &"# ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>"","AND LogTime<=#"& Admin.Search.EndDate &"# ","")
				Case 1
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.StartDate<>"","AND LogTime>='"& Admin.Search.StartDate &"' ","")
				tempCondition = tempCondition & KnifeCMS.IIF(Admin.Search.EndDate  <>"","AND LogTime<='"& Admin.Search.EndDate &"' ","")
				End Select
				PageUrlPara = "startdate="& Server.URLEncode(Admin.Search.StartDate) &"&enddate="& Server.URLEncode(Admin.Search.EndDate) &"&keywords="& Server.URLEncode(Admin.Search.KeyWords) &""
				Fn_TempSql = Fn_TempSql & tempCondition
			End If
			Fn_TempSql = Fn_TempSql &" ORDER BY ID DESC"
			Call Admin.DataList(Fn_TempSql,50,Fn_ColumnNum,Operation,PageUrlPara)
		End If
	End Function
	
	Function SysLogDel()
		SysLogSubs
	End Function
	
	Function SysLogOtherEdit()
		SysLogSubs
	End Function
	
	Sub SysLogSubs()
		ID = KnifeCMS.GetForm("post","InputName")
		Dim ArrID : ArrID = Split(ID,",")
		For i=0 To Ubound(ArrID)
			ID = KnifeCMS.Data.Int(ArrID(i))
			Result    = 0
			If ID>0 Then
				'开始执行操作
				If Action = "del" Then
					If SubAction = "completely" Then
						Result = KnifeCMS.DB.DeleteRecord(TablePre&"SystemLog",Array("ID:"&ID))
						Msg    = Msg & Lang_SysLogCue(2) &"{"& Pv_Username &"[ID="& ID &"]}<br/>"
					Else
						Result = KnifeCMS.DB.UpdateRecord(TablePre&"SystemLog",Array("Recycle:1"),Array("ID:"&ID))
						Msg    = Msg & Lang_SysLogCue(1) &"{"& Pv_Username &"[ID="& ID &"]}<br/>"
					End If
				ElseIf Action = "edit" Then
					If SubAction = "revert" Then
						Result = KnifeCMS.DB.UpdateRecord(TablePre&"SystemLog",Array("Recycle:0"),Array("ID:"&ID))
						Msg    = Msg & Lang_SysLogCue(0) &"{"& Pv_Username &"[ID="& ID &"]}<br/>"
					End If
				End If
			End If
		Next
		Admin.ShowMessage Msg,Url,0
	End Sub
	
	Function SysLogView()
		If Ctl="syslog_recycle" Then
		%>
		<DIV id="MainFrame">
		  <div class="tagLine"><% Echo NavigationLine %></div>
		  <DIV class="cbody">
			<div class="toparea">
			  <table border="0" cellpadding="0" cellspacing="0">
			  <tr>
			  <td class="tdcell">
				  <table border="0" cellpadding="2" cellspacing="0">
				  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoEdit %><!--编辑操作区--></div></td></tr>
				  <tr><td class="downcell">
					  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','edit:revert','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_revert"></b><% Echo Lang_DoRevert %></button>
					  </td></tr>
				  </table>
			  </td>
			  <td class="tdcell">
				  <table border="0" cellpadding="2" cellspacing="0">
				  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
				  <tr><td class="downcell">
					  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
					  </td></tr>
				  </table>
			  </td>
			  </tr>
			  </table>
			</div>
			<div class="tabledata">
			  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
			  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
			   <tbody>
				<tr class="top">
				<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
				<th width="6%"><% Echo Lang_ListTableColLine_ID %></th><!--系统编号-->
				<th width="7%"><% Echo Lang_ListTableColLine_LogTime %></th><!--日志时间-->
				<th width="7%"><% Echo Lang_ListTableColLine_Username %></th><!--用户-->
				<th width="7%"><% Echo Lang_ListTableColLine_UserID %></th><!--用户ID-->
				<th width="7%"><% Echo Lang_ListTableColLine_UserIP %></th><!--来源IP-->
				<th width="7%"><% Echo Lang_ListTableColLine_SystemCtl %></th><!--功能系统-->
				<th width="7%"><% Echo Lang_ListTableColLine_SystemAction %></th><!--功能操作-->
				<th><% Echo Lang_ListTableColLine_LogContent %></th><!--日志内容-->
                <th></th>
				</tr>
				<% SysLogList %>
			   </tbody>
			  </table>
			  </form>
			</div>
			</DIV>
		</DIV>
		<script type="text/javascript">
		TrBgChange("listForm","InputName","oliver","data","select");
		</script>
		<% Else %>
        <%=Admin.Calendar(Array("","",""))%>
		<DIV id="MainFrame">
		  <div class="tagLine"><% Echo NavigationLine %></div>
		  <DIV class="cbody">
			<div class="toparea">
			  <table border="0" cellpadding="0" cellspacing="0">
			  <tr>
			  <td class="tdcell">
				  <table border="0" cellpadding="2" cellspacing="0">
				  <tr><td class="upcell"><div class="align-center grey"><% Echo Lang_DistrictDoDel %><!--删除操作区--></div></td></tr>
				  <tr><td class="downcell">
					  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_recycle"></b><% Echo Lang_DoDel%></button>
					  <button type="button" class="sysBtn" onClick="Operate('listForm','InputName','del:completely','<% Echo Lang_DoDelConfirm %>')"><b class="icon icon_del"></b><% Echo Lang_DoDelCompletely%></button>
					  </td></tr>
				  </table>
			  </td>
			  <td class="tdcell">
				<table border="0" cellpadding="2" cellspacing="0">
				<tr><td class="upcell">
					<div class="align-center grey">
						<% Echo Lang_DistrictDoSearch %><!--搜索操作区-->
						<span class="AdvancedSearch"><a href="javascript:Admin.AdvancedSearch.Open(600,320,'AdvancedSearch')" ><% Echo Lang_AdvancedSearch %></a></span>
					</div>
					<DIV id="AdvancedSearch" style="display:none;">
						<form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
						<div class="diainbox mb5">
						  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="infoTable">
						  <tr><td class="titletd"><% Echo Lang_StartDate %>:</td>
							  <td class="infotd"><input type="text" class="TxtSer dateStart" id="startdate" name="startdate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:200px;" value="<%=Admin.Search.StartDate%>" /></td>
						  <tr><td class="titletd"><% Echo Lang_EndDate %>:</td>
							  <td class="infotd"><input type="text" class="TxtSer dateEnd" id="enddate" name="enddate" readonly="readonly" onfocus='Calendar.setup({weekNumbers:true,inputField:this,trigger:this,dateFormat:"%Y-%m-%d %H:%M:%S",showTime:true,minuteStep:1,onSelect:function(){this.hide();}});' style="width:200px;" value="<%=Admin.Search.EndDate%>" /></td>
						  </tr>
						  <tr><td class="titletd"><% Echo Lang_ContentKeywords %></td>
							  <td class="infotd"><input type="text" class="TxtClass" id="keywords" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:280px;" /></td>
						  </tr>
						  </table>
						</div>
						<% Echo AdvancedSearchBtnline %>
						</form>
					</DIV>
					</td></tr>
				<tr><td class="downcell">
					<form name="SerachForm" action="?ctl=<% Echo Ctl %>&act=<% Echo Action %>&subact=search" method="post" style="margin:0px">
					<table border="0" cellpadding="0" cellspacing="0"><tr>
					<td><span><% Echo Lang_ContentKeywords %>:</span></td>
					<td class="pl3"><input type="text" class="TxtClass" id="keywords" name="keywords" value="<% Echo Admin.Search.KeyWords %>" style="width:220px;" /></td>
					<td class="pl3"><button type="submit" class="sysBtn" ><b class="icon icon_search"></b><% Echo Lang_Btn_Search %></button></td>
					</tr></table>
					</form>
				</td></tr>
				</table>
			  </td>
			  </tr>
			  </table>
			</div>
			<div class="tabledata">
			  <form name="listForm" id="listForm" method="post" action="?ctl=<% Echo Ctl %>&url=<%= KnifeCMS.Data.URLEncode(KnifeCMS.GetUrl(""))%>">
			  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listTable">
			   <tbody>
				<tr class="top">
				<th class="first"><div style="width:23px;"><input name="SelectAll" type="checkbox" onClick="CheckAll('listForm','SelectAll','InputName','oliver','data','select')" title="<% Echo Lang_DoSelectAll %>"><input type="hidden" name="act" value="" /><input type="hidden" name="subact" value="" /></div></th>
				<th width="6%"><% Echo Lang_ListTableColLine_ID %></th><!--系统编号-->
				<th width="7%"><% Echo Lang_ListTableColLine_LogTime %></th><!--日志时间-->
				<th width="7%"><% Echo Lang_ListTableColLine_Username %></th><!--用户-->
				<th width="7%"><% Echo Lang_ListTableColLine_UserID %></th><!--用户ID-->
				<th width="7%"><% Echo Lang_ListTableColLine_UserIP %></th><!--来源IP-->
				<th width="7%"><% Echo Lang_ListTableColLine_SystemCtl %></th><!--功能系统-->
				<th width="7%"><% Echo Lang_ListTableColLine_SystemAction %></th><!--功能操作-->
				<th><% Echo Lang_ListTableColLine_LogContent %></th><!--日志内容-->
                <th></th>
				</tr>
				<% SysLogList %>
			   </tbody>
			  </table>
			  </form>
			</div>
			</DIV>
		</DIV>
		<script type="text/javascript">
		TrBgChange("listForm","InputName","oliver","data","select");
		ReplaceText(document.getElementById("keywords").value,1);
		</script>
		<%
		End If
	End Function
	
End Class
%>